<?php

namespace app\models\common;

use yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $authKey
 * @property string $accessToken
 * @property string $role
 */

class User extends ActiveRecord implements IdentityInterface
{
    const ADMIN           = 'admin';
    const SUPER_ADMIN     = 'superAdmin';
    const CONTENT_MANAGER = 'contentManager';

    const SUPER_ADMIN_KEY     = 1;
    const ADMIN_KEY           = 2;
    const CONTENT_MANAGER_KEY = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'role'], 'required'],
            [['email', 'password', 'authKey', 'accessToken'], 'string', 'max' => 40],
            [['email'], 'email'],
            [['email'], 'unique', 'targetAttribute' => ['email'], 'message' => 'Email has already been taken.'],
            [['role'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'password' => 'Password',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'role' => 'Role',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['accessToken' => $token]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->authKey = sha1(Yii::$app->getSecurity()->generateRandomKey() . microtime());
    }

    /**
     * Generates access token
     */
    public function generateAccessToken()
    {
        $this->accessToken = sha1(Yii::$app->getSecurity()->generateRandomKey() . microtime());
    }

    /**
     * create password hash
     */
    private function passwordHash()
    {
        $this->password = sha1($this->password);
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === sha1($password);
    }

    /**
     * Create a new user
     */
    public function create()
    {
        $this->passwordHash();
        $this->generateAuthKey();
        $this->generateAccessToken();

        return $this->save();
    }
}
