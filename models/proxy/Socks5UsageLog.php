<?php

namespace app\models\proxy;

use Yii;

/**
 * This is the model class for table "socks5UsageLog".
 *
 * @property string $id
 * @property string $clientName
 * @property string $proxy
 * @property string $createdAt
 */
class Socks5UsageLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'socks5UsageLog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientName', 'proxy'], 'required'],
            [['createdAt'], 'safe'],
            [['proxy'], 'string', 'max' => 32],
            [['clientName'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clientName' => 'Client Name',
            'proxy' => 'Proxy',
            'createdAt' => 'Created At',
        ];
    }
}
