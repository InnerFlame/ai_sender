<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log".
 *
 * @property integer $id
 * @property integer $level
 * @property string $category
 * @property integer $log_time
 * @property string $prefix
 * @property string $message
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level', 'log_time'], 'integer'],
            [['prefix', 'message'], 'string'],
            [['category'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'level' => 'Level',
            'category' => 'Category',
            'log_time' => 'Log Time',
            'prefix' => 'Prefix',
            'message' => 'Message',
        ];
    }

    public static function getLogs($request)
    {
        $logs = self::find();

        if (!empty($request['dateFrom'])) {
            $logs->andWhere('log_time >= :dateFrom', [':dateFrom' => strtotime($request['dateFrom'] . ' 00:00:00')]);
        }

        if (!empty($request['dateTo'])) {
            $logs->andWhere('log_time <= :dateTo', [':dateTo' => strtotime($request['dateTo'] . ' 23:59:59')]);
        }

        return $logs->orderBy('id desc');
    }
}
