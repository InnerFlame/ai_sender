<?php

namespace app\models\rpc;

use Yii;

/**
 * This is the model class for table "rpclog".
 *
 * @property integer $id
 * @property string $country
 * @property string $data
 */
class LaunchedContainersInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'launchedContainersInfo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'data'], 'required'],
            [['country', 'data'], 'string'],
        ];
    }
}
