<?php

namespace app\models\rpc;

use Yii;

/**
 * This is the model class for table "rpclog".
 *
 * @property string $id
 * @property string $clientName
 * @property string $direction
 * @property string $method
 * @property string $params
 * @property string $createdAt
 */
class RpcLog extends \yii\db\ActiveRecord
{
    const INCOMING     = 'incoming';
    const METHOD_ERROR = 'error';
    const OUTGOING     = 'outgoing';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rpcLog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['direction', 'method'], 'required'],
            [['direction', 'params'], 'string'],
            [['createdAt'], 'safe'],
            [['clientName', 'method'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clientName' => 'Client Name',
            'direction' => 'Direction',
            'method' => 'Method',
            'params' => 'Params',
            'createdAt' => 'Created At',
        ];
    }
}
