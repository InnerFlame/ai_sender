<?php

namespace app\models\rpc;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\rpc\RpcLog;

/**
 * RpcLogSearch represents the model behind the search form about `app\models\RpcLog`.
 */
class RpcLogSearch extends RpcLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientName', 'method', 'direction'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RpcLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'clientName' => $this->clientName,
            'method'     => $this->method,
            'direction'  => $this->direction,
        ])->orderBy('id DESC');

        return $dataProvider;
    }
}
