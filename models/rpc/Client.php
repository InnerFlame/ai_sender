<?php

namespace app\models\rpc;

/**
 * @property integer $id
 * @property integer $profileId
 * @property string $name
 * @property array $status
 * @property string $type
 * @property string $target
 * @property string $timeSetStatus
 * @property string $country
 * @property integer $site
 * @property string $refreshingTime
 */
class Client extends \yii\db\ActiveRecord
{
    const STATUS_STARTED = 'started';
    const STATUS_LOGIN   = 'login';
    const STATUS_STOPPED = 'stopped';
    const STATUS_WAIT    = 'wait';

    const TYPE_COMMUNICATION   = 'communication';
    const TYPE_REGISTRATION    = 'registration';
    const TYPE_RE_UPLOAD_PHOTO = 'reUpload';
    const TYPE_OBSERVE         = 'observe';
    const TYPE_SEARCH          = 'search';
    const TYPE_FETCH           = 'fetch';

    /** @var array $types */
    public static $types = [
        self::TYPE_COMMUNICATION      => self::TYPE_COMMUNICATION,
        self::TYPE_REGISTRATION       => self::TYPE_REGISTRATION,
        self::TYPE_RE_UPLOAD_PHOTO    => self::TYPE_RE_UPLOAD_PHOTO,
        self::TYPE_OBSERVE            => self::TYPE_OBSERVE,
        self::TYPE_SEARCH             => self::TYPE_SEARCH,
        self::TYPE_FETCH              => self::TYPE_FETCH,
    ];

    /** @var array $statuses */
    public static $statuses = [
        self::STATUS_STARTED => self::STATUS_STARTED,
        self::STATUS_LOGIN   => self::STATUS_LOGIN,
        self::STATUS_STOPPED => self::STATUS_STOPPED,
        self::STATUS_WAIT    => self::STATUS_WAIT,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client';
    }

    public static function getClients(array $request)
    {
        $client = self::find();

        if (!empty($request['name'])) {
            $client->andWhere(['name' => $request['name']]);
        }

        if (!empty($request['status'])) {
            $client->andWhere(['status' => $request['status']]);
        }

        if (!empty($request['type'])) {
            $client->andWhere(['type' => $request['type']]);
        }

        if (!empty($request['country'])) {
            $client->andWhere(['country' => $request['country']]);
        }

        if (!empty($request['site'])) {
            $client->andWhere(['site' => $request['site']]);
        }

        return $client->all();
    }
}
