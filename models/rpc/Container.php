<?php

namespace app\models\rpc;

use app\config\Constant;

/**
 * @property integer $id
 * @property integer $profileId
 * @property string  $name
 * @property string   $status
 * @property string  $target
 * @property string  $type
 * @property string  $lastTimeActivity
 * @property string  $timeSetStatus
 * @property string  $country
 * @property integer  $site
 */
class Container extends \yii\db\ActiveRecord
{
    const TYPE_COMMUNICATION   = 'communication';
    const TYPE_REGISTRATION    = 'registration';
    const TYPE_RE_UPLOAD_PHOTO = 'reUpload';
    const TYPE_OBSERVE         = 'observe';
    const TYPE_SEARCH          = 'search';
    const TYPE_FETCH           = 'fetch';

    const STATUS_STARTED = 'started';
    const STATUS_STOPPED = 'stopped';
    const STATUS_WAIT    = 'wait';

    const LIVE_TIME          = 300;
    const LIVE_TIME_COMM     = 1800;
    const LIVE_TIME_REGISTER = 600;

    /** @var array $types */
    public static $types = [
        self::TYPE_COMMUNICATION      => self::TYPE_COMMUNICATION,
        self::TYPE_REGISTRATION       => self::TYPE_REGISTRATION,
        self::TYPE_RE_UPLOAD_PHOTO    => self::TYPE_RE_UPLOAD_PHOTO,
        self::TYPE_OBSERVE            => self::TYPE_OBSERVE,
        self::TYPE_SEARCH             => self::TYPE_SEARCH,
        self::TYPE_FETCH              => self::TYPE_FETCH,
    ];

    /** @var array $statuses */
    public static $statuses = [
        self::STATUS_STARTED => self::STATUS_STARTED,
        self::STATUS_STOPPED => self::STATUS_STOPPED,
        self::STATUS_WAIT    => self::STATUS_WAIT,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'container';
    }

    public static function getStarted(string $type, string $country, int $site)
    {
        return self::findAll([
            'target'  => Constant::TARGET_PHOENIX,
            'type'    => $type,
            'country' => $country,
            'site'    => $site,
            'status'  => self::STATUS_STARTED
        ]);
    }

    public static function getStartedCount(string $type, array $filters = [])
    {
        $containersCount =  self::find()
            ->where(['target'     => Constant::TARGET_PHOENIX])
            ->andWhere(['type'    => $type])
            ->andWhere(['or',
                ['status' => Container::STATUS_STARTED],
                ['status' => Container::STATUS_WAIT],
            ]);

        if (!empty($filters['country'])) {
            $containersCount->andWhere(['country' => $filters['country']]);
        }

        if (!empty($filters['site'])) {
            $containersCount->andWhere(['site' => $filters['site']]);
        }

        return $containersCount->count();
    }

    public static function updateLastTimeActivity(string $name, string $target)
    {
        $container = self::findOne(['name' => $name, 'target' => $target]);

        if ($container) {
            $container->lastTimeActivity = time();
            $container->save();
        }
    }

    public static function getStartedCountForSections()
    {
        $data = [];

        foreach (self::$types as $type) {
            $data[$type] = self::find()
                ->where(['or',
                    ['target'    => Constant::TARGET_PHOENIX],
                    ['target'    => Constant::TARGET_PHOENIX_WEB],
                ])
                ->andWhere(['type'   => $type])
                ->andWhere(['status' => Container::STATUS_STARTED])
                ->count();
        }

        return $data;
    }

    public static function getContainers(array $request)
    {
        $containers = self::find();

        if (!empty($request['name'])) {
            $containers->andWhere(['name' => $request['name']]);
        }

        if (!empty($request['status'])) {
            $containers->andWhere(['status' => $request['status']]);
        }

        if (!empty($request['type'])) {
            $containers->andWhere(['type' => $request['type']]);
        }

        if (!empty($request['country'])) {
            $containers->andWhere(['country' => $request['country']]);
        }

        if (!empty($request['site'])) {
            $containers->andWhere(['site' => $request['site']]);
        }

        return $containers->all();
    }
}
