<?php

namespace app\models\rpc;

use Yii;

/**
 * @property integer $id
 * @property string $country
 * @property string $data
 */
class LifeCycleContainersInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lifeCycleContainersInfo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'data'], 'required'],
            [['country', 'data'], 'string'],
        ];
    }
}
