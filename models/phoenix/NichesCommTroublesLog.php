<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "{{%nichesErrorLog}}".
 *
 * @property integer $id
 * @property string $niche
 * @property string $country
 * @property integer $profileType
 * @property string $message
 * @property string $createdAt
 */
class NichesCommTroublesLog extends \yii\db\ActiveRecord
{
    const PROFILE_TYPE_SENDER    = 1;
    const PROFILE_TYPE_RECIPIENT = 2;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return '{{%nichesCommTroublesLog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['niche', 'country', 'profileType'], 'required'],
            [['profileType'], 'integer'],
            [['createdAt'], 'safe'],
            [['niche', 'message'], 'string', 'max' => 255],
            [['country'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id'            => 'ID',
            'niche'         => 'Niche',
            'country'       => 'Country',
            'profileType'   => 'Profile Type',
            'message'       => 'Message',
            'createdAt'     => 'Created At',
        ];
    }

    /**
     * Log data to table
     *
     * @param string $niche
     * @param string $country
     * @param int $profileType
     * @param string $message
     */
    public static function log(string $niche, string $country, int $profileType, string $message = '')
    {
        $model = new self([
            'niche'         => $niche,
            'country'       => $country,
            'profileType'   => $profileType,
            'message'       => $message
        ]);

        $model->save();
    }

    public static function getLoggedData($niche, $count = false)
    {
        $records = self::find()
            ->where(['between', 'createdAt', date('Y-m-01 00:00:00'), date('Y-m-t 23:59:59')])
            ->andWhere(['niche' => $niche])
            ->orderBy(['createdAt' => SORT_DESC]);

        return $count ? $records->count() : $records->all();
    }

    public static function cleanLog()
    {
        self::deleteAll(['between', 'createdAt', date('Y-m-01 00:00:00'), date('Y-m-t 23:59:59')]);
    }
}
