<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "phoenixIncomingMessage".
 *
 * @property integer $id
 * @property integer $senderId
 * @property integer $recipientId
 * @property integer $step
 * @property integer $isUsed
 * @property string $text
 * @property string $subject
 * @property string $createdAt
 * @property string $messageId
 * @property integer $groupId
 * @property integer $schemaId
 * @property string $messageType
 * @property string $platformFromSent
 */
class IncomingMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'incomingMessage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['senderId', 'recipientId', 'step', 'isUsed', 'groupId', 'schemaId'], 'integer'],
            [['createdAt', 'text', 'subject', 'messageId', 'messageType', 'platformFromSent'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'senderId'    => 'Sender ID',
            'recipientId' => 'Recipient ID',
            'step'        => 'Step',
            'isUsed'      => 'Is Process',
            'createdAt'   => 'Created At',
            'groupId'     => 'Group Id',
            'messageType' => 'Message Type',
        ];
    }
}