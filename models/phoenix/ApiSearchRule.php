<?php

namespace app\models\phoenix;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "apiSearchRule".
 *
 * @property integer $id
 * @property string $country
 * @property integer $isActive
 * @property string $createdAt
 */
class ApiSearchRule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'apiSearchRule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'isActive'], 'required'],
            [['isActive'], 'integer'],
            [['createdAt'], 'safe'],
            [['country'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'country'   => 'Country',
            'isActive'  => 'Is Active',
            'createdAt' => 'Created At',
        ];
    }

    public static function getCountries()
    {
        return ArrayHelper::getColumn(self::findAll(['isActive' => 1]), 'country');
    }
}
