<?php

namespace app\models\phoenix;

use Yii;

//TODO: DEPRECATED MODEL - used in migration
/**
 * This is the model class for table "recipientProfileData".
 * @property integer $id
 * @property integer $recipientId
 * @property string $multiProfileId
 * @property string $createdAt
 */
class RecipientProfileData extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'recipientProfileData';
    }
}