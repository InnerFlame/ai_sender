<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "phoenixProfileMessage".
 *
 * @property integer $id
 * @property string $subject
 * @property string $body
 * @property string $language
 * @property string $country
 * @property integer $gender
 * @property integer $sexuality
 * @property integer $sendMode
 * @property integer $project
 * @property integer $isPaid
 * @property integer $isActive
 */
class ProfileMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profileMessage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body', 'gender', 'sexuality', 'project'], 'required'],
            [['body'], 'string'],
            [['gender', 'sexuality', 'sendMode', 'project', 'isPaid', 'isActive'], 'integer'],
            [['subject'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 5],
            [['country'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'Subject',
            'body' => 'Body',
            'language' => 'Language',
            'country' => 'Country',
            'gender' => 'Gender',
            'sexuality' => 'Sexuality',
            'sendMode' => 'Send Mode',
            'project' => 'Project',
            'isPaid' => 'Is Paid',
            'isActive' => 'Is Active',
        ];
    }
}