<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "phoenixReUploadPhotoScheduler".
 *
 * @property integer $id
 * @property integer $senderId
 * @property string $timeToReUpload
 * @property string $startReUpload
 * @property string $createdAt
 */
class ReUploadPhotoScheduler extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reUploadPhotoScheduler';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['senderId'], 'required'],
            [['senderId'], 'integer'],
            [['timeToReUpload', 'startReUpload', 'createdAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'senderId' => 'Sender ID',
            'timeToReUpload' => 'Time To Re Upload',
            'startReUpload' => 'Start Re Upload',
            'createdAt' => 'Created At',
        ];
    }
}
