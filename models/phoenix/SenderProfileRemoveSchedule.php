<?php

namespace app\models\phoenix;

use Yii;


/**
 * @property integer $id
 * @property integer $senderId
 * @property string $timeToRemove
 * @property string $startedRemoveAt
 * @property string $removedAt
 * @property string $createdAt
 * @property string $niche
 */
class SenderProfileRemoveSchedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'senderProfileRemoveSchedule';
    }

    public function getSenderProfile()
    {
        return $this->hasOne(SenderProfile::className(), ['id' => 'senderId']);
    }
}
