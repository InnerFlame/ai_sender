<?php

namespace app\models\phoenix;

use sender\communication\models\CommunicationSchedule;

/**
 * This is the model class for table "group".
 *
 * @property integer $id
 * @property integer $schemaId
 * @property integer $sendMode
 * @property integer $messageCount
 * @property string $title
 * @property string $startTimeOffset
 * @property string $endTimeOffset
 * @property integer $period
 * @property integer $isInitial
 * @property integer $percentWink
 * @property integer $percentAddToFriend
 * @property integer $percentBrowse
 * @property integer $typeSendActivity
 */
class Group extends \yii\db\ActiveRecord
{
    const TYPE_SEND_ACTIVITY_ALL          = 'all';
    const TYPE_SEND_ACTIVITY_RESTART      = 'restart';
    const TYPE_SEND_ACTIVITY_ZERO_RESTART = 'zeroRestart';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'schemaId', 'sendMode', 'messageCount', 'period', 'startTimeOffset', 'endTimeOffset'], 'required'],
            [['schemaId', 'sendMode', 'messageCount', 'period', 'startTimeOffset', 'endTimeOffset'], 'integer'],
            [['createdAt', 'isInitial', 'percentWink', 'percentAddToFriend', 'percentBrowse', 'typeSendActivity'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['percentWink', 'percentAddToFriend', 'percentBrowse'], 'compare', 'compareValue' => 100, 'operator' => '<=', 'type' => 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => 'ID',
            'schemaId'           => 'Schema',
            'sendMode'           => 'Send Mode',
            'messageCount'       => 'Message Count',
            'title'              => 'Title',
            'startTimeOffset'    => 'Start Time Offset',
            'endTimeOffset'      => 'End Time Offset',
            'period'             => 'Period',
            'isInitial'          => 'Is Initial',
            'typeSendActivity'   => 'Type Send Activity',
            'percentWink'        => 'Wink %',
            'percentAddToFriend' => 'Add To Friend %',
            'percentBrowse'      => 'Browse %',
        ];
    }

    public function getSchema()
    {
        return $this->hasOne(Schema::className(), ['id' => 'schemaId']);
    }


    public function afterDelete()
    {
        parent::afterDelete();

        CommunicationSchedule::deleteAll(['groupId' => $this->id, 'sendStartedAt' => '0000-00-00 00:00:00']);
    }
}
