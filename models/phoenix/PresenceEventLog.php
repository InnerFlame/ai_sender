<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "phoenixPresenceEventLog".
 *
 * @property string $id
 * @property string $clientName
 * @property string $action
 * @property string $createdAt
 */
class PresenceEventLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'presenceEventLog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action'], 'required'],
            [['createdAt'], 'safe'],
            [['clientName', 'action'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clientName' => 'Client Name',
            'action' => 'Action',
            'createdAt' => 'Created At',
        ];
    }
}
