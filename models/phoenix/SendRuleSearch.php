<?php

namespace app\models\phoenix;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SendRuleSearch represents the model behind the search form about `app\models\SendRule`.
 */
class SendRuleSearch extends SendRule
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'schemaId', 'site', 'isActive'], 'integer'],
            [['country', 'project'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params): ActiveDataProvider
    {
        $query = SendRule::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'          => $this->id,
            'schemaId'    => $this->schemaId,
            'site'        => $this->site,
            'country'     => $this->country,
            'isActive'    => $this->isActive,
            'project'     => $this->project
        ]);

        return $dataProvider;
    }
}
