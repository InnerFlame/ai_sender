<?php

namespace app\models\phoenix;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\phoenix\AnswerRateStat;
use yii\db\Expression;
use yii\db\Query;

/**
 * AnswerRateStatSearch represents the model behind the search form about `app\models\phoenix\AnswerRateStat`.
 */
class AnswerRateStatSearch extends AnswerRateStat
{
    public $restartedStatus;
    public $answersRate;

    public $sentAtFrom;
    public $sentAtTo;

    const RESTART_STATUS_TRUE = 1;
    const RESTART_STATUS_FALSE = 0;

    private $filteredSchemaData = [];
    private $filteredGroupsData = [];

    public $countInitialsWithoutGroup = 0;
    public $countAnsweredWithoutGroup = 0;
    public $countRestartWithoutGroup = 0;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'initials', 'answered', 'groupId', 'schemeId', 'restartNumber'], 'integer'],
            [['sentAtFrom', 'sentAtTo', 'country', 'restartedStatus', 'answersRate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AnswerRateStat::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ],
            'pagination' => [
                'pageSize' => 40,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $needToGroup = false;

        if (isset($this->sentAtFrom) && !empty($this->sentAtFrom)) {
            if(isset($this->sentAtTo) && !empty($this->sentAtTo)) {
                $from = $this->sentAtFrom   . ' 00:00:00';
                $to   = $this->sentAtTo     . ' 00:00:00';
                $query->where(['between', 'sentAt', $from, $to]);
            }
        }


        if (isset($this->restartedStatus) && $this->restartedStatus != "") {
            if($this->restartedStatus == self::RESTART_STATUS_FALSE) {
                $query->where(['or', ['restartNumber' => 0], ['restartNumber' => NULL]]);
           } else {
                $query->where(['and',
                    ['not', ['restartNumber' => 0]],
                    ['not', ['restartNumber' => null]]
                ]);
           }
        }

        if (isset($this->schemeId) && $this->schemeId != '') {
            $needToGroup = true;
        }

        if (isset($this->country) && !empty($this->country)) {
            $needToGroup = true;
        }

        if (isset($this->groupId) && !empty($this->groupId)) {
            $needToGroup = true;
        }

        if ($needToGroup) {
            $query->addGroupBy('country, schemeId, groupId');
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'            => $this->id,
            'initials'      => $this->initials,
            'answered'      => $this->answered,
            'groupId'       => $this->groupId,
            'schemeId'      => $this->schemeId,
            'restartNumber' => $this->restartNumber,
            'country'       => $this->country
        ]);


        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getSchemesFilterData()
    {
        $models = Schema::find();
        foreach ($models->batch(100) as $batchOfModels) {
            foreach ($batchOfModels as $model) {
                /**@var $model Schema */
                $this->filteredSchemaData[$model->id] = $model->title;
            }
        }

        return $this->filteredSchemaData;
    }

    /**
     * @param $idKey
     * @return mixed|string
     */
    public function getSchemeTitle($idKey)
    {
        if (empty($this->filteredSchemaData)) {
            $this->getSchemesFilterData();
        }

        return $this->filteredSchemaData[$idKey] ?? 'Undefined';
    }


    /**
     * @return array
     */
    public function getGroupsFilterData()
    {
        $models = Group::find();
        foreach ($models->batch(100) as $batchOfModels) {
            foreach ($batchOfModels as $model) {
                /**@var $model Group */
                $this->filteredGroupsData[$model->id] = $model->title;
            }
        }

        return $this->filteredGroupsData;
    }

    /**
     * @param $idKey
     * @return mixed|string
     */
    public function getGroupTitle($idKey)
    {
        if (empty($this->filteredGroupsData)) {
            $this->getGroupsFilterData();
        }

        return $this->filteredGroupsData[$idKey] ?? 'Undefined';
    }

    /**
     * @param $country
     * @param $date
     * @return int
     */
    public function getTotalInitialsCountForAllGroups($country, $date)
    {
        $query = (new Query())->select('SUM(initials) as sum')
            ->from(AnswerRateStat::tableName())
            ->where(['country' => $country])
            ->andWhere(['sentAt' => $date])
            ->one();
        $this->countInitialsWithoutGroup = $query['sum'];
        return $this->countInitialsWithoutGroup;
    }

    /**
     * @param $country
     * @param $date
     * @return int
     */
    public function getTotalAnsweredCountForAllGroups($country, $date)
    {
        $query = (new Query())->select('SUM(answered) as sum')
            ->from(AnswerRateStat::tableName())
            ->where(['country' => $country])
            ->andWhere(['sentAt' => $date])
            ->one();
        $this->countAnsweredWithoutGroup = $query['sum'];
        return $this->countAnsweredWithoutGroup;
    }

    /**
     * @param $country
     * @param $date
     * @return int
     */
    public function getTotalRestartCount($country, $date)
    {
        $query = (new Query())->select('SUM(restartNumber) as sum')
            ->from(AnswerRateStat::tableName())
            ->where(['country' => $country])
            ->andWhere(['sentAt' => $date])
            ->one();
        $this->countRestartWithoutGroup = $query['sum'];
        return $this->countRestartWithoutGroup;
    }

    /**
     * Get group condition
     * @return bool
     */
    public function getGroupByCountriesDataCondition()
    {
        return
            (!isset($this->groupId) || empty($this->groupId))
            &&
            (!isset($this->schemeId) || empty($this->schemeId));
    }
}
