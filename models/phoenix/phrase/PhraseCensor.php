<?php

namespace app\models\phoenix\phrase;

use Yii;

/**
 * This is the model class for table "phraseCensor".
 *
 * @property integer $id
 * @property string $text
 * @property integer $categoryId
 * @property integer $tagId
 * @property integer $staffId
 * @property string $createdAt
 */
class PhraseCensor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'phraseCensor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryId', 'tagId', 'staffId'], 'integer'],
            [['createdAt'], 'safe'],
            [['text'], 'string', 'max' => 30],
            [['text'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'text'       => Yii::t('app', 'Text'),
            'categoryId' => Yii::t('app', 'Category ID'),
            'tagId'      => Yii::t('app', 'Tag ID'),
            'staffId'    => Yii::t('app', 'Staff ID'),
            'createdAt'  => Yii::t('app', 'Created At'),
        ];
    }
}
