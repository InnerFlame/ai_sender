<?php

namespace app\models\phoenix\phrase;

use Yii;

/**
 * This is the model class for table "phraseTags".
 *
 * @property integer $id
 * @property string $tag
 */
class PhraseTags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'phraseTags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag'], 'string', 'max' => 30],
            [['tag'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'  => Yii::t('app', 'ID'),
            'tag' => Yii::t('app', 'Tag'),
        ];
    }
}
