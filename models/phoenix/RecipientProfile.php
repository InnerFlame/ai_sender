<?php

namespace app\models\phoenix;

/**
 * This is the model class for table "phoenixRecipientProfile".
 *
 * @property integer $id
 * @property string $remoteId
 * @property integer $isPaid
 * @property integer $sexuality
 * @property integer $gender
 * @property string $activationTime
 * @property integer $site
 * @property string $country
 * @property integer $age
 * @property string $createdAt
 * @property integer $online
 * @property string $lastOnlineDate
 * @property string $state
 * @property string $city
 * @property string $citySearch
 * @property string $stateSearch
 * @property integer $isWithPhoto
 * @property string $lastUsedPlatform
 * @property string $source
 * @property string $locale
 * @property string $screenname
 * @property string $registrationPlatform
 * @property integer $photoCount
 * @property string $onlineStatusExpireAt
 * @property string $updatedAt
 * @property string $birthday
 * @property string $multiProfileId
 * @property string $isExcludeByTopic
 * @property string $niche
 * @property integer $complained
 * @property integer $searchable
 * @property RecipientProfileData $recipientProfileData  (deprecated used in migration)
 */
class RecipientProfile extends \yii\db\ActiveRecord
{
    const WEB_SITE    = 'webSite';
    const MOB_SITE    = 'mobSite';
    const ANDROID_APP = 'androidApp';
    const IOS_APP     = 'iosApp';
    const NOT_SET     = 'notSet';
    const DEFAULT_LANG = 'en';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recipientProfile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['remoteId', 'isPaid'], 'required'],
            [['isPaid', 'sexuality', 'gender', 'site', 'age', 'online', 'complained', 'searchable'], 'integer'],
            [['activationTime', 'createdAt', 'lastOnlineDate', 'isWithPhoto'], 'safe'],
            [['remoteId', 'multiProfileId'], 'string', 'max' => 40],
            [['multiProfileId'], 'string', 'max' => 32],
            [['niche'], 'string', 'max' => 30],
            [['country'], 'string', 'max' => 3],
            [['state', 'stateSearch'], 'string', 'max' => 4],
            [['city', 'citySearch'], 'string', 'max' => 50],
            [['remoteId'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                   => 'ID',
            'remoteId'             => 'Remote ID',
            'isPaid'               => 'Is Paid',
            'sexuality'            => 'Sexuality',
            'gender'               => 'Gender',
            'activationTime'       => 'Activation Time',
            'site'                 => 'Site',
            'country'              => 'Country',
            'age'                  => 'Age',
            'createdAt'            => 'Created At',
            'online'               => 'Online',
            'lastOnlineDate'       => 'Last Online Date',
            'state'                => 'State',
            'city'                 => 'City',
            'citySearch'           => 'City Search',
            'stateSearch'          => 'State Search',
            'niche'                => 'Niche',
            'complained'           => 'Complained',
            'searchable'           => 'Searchable'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipientProfileData()
    {
        return $this->hasOne(RecipientProfileData::className(), ['recipientId' => 'id']);
    }

    public function getLanguage(): string
    {
        $realLanguage = $this->getLangFromLocale();
        $multiLanguages = $this->getMultiLanguagesList();

        if (in_array($realLanguage, $multiLanguages)) {
            return $realLanguage;
        }

        return $this->getDefaultLangByCountry();
    }

    /**
     * @return bool|string
     */
    public function getLangFromLocale()
    {
        $countryCodeDelimiterPosition = strpos($this->locale, '_');
        $langFromLocale = $countryCodeDelimiterPosition === false
            ? $this->locale : substr($this->locale, 0, $countryCodeDelimiterPosition);

        return strlen($langFromLocale) == 2 ? $langFromLocale : self::DEFAULT_LANG;
    }

    /**
     * @return string[]
     */
    public function getMultiLanguagesList(): array
    {
        $defaultLanguage = \Yii::$app->yaml->parseByKey('language', 'allowedLanguages');
        if (!empty($defaultLanguage[$this->country])) {
            return $defaultLanguage[$this->country];
        }

        return [];
    }

    /**
     * getting default language for recipient by country
     * see in config\language.yaml
     * @return string
     */
    public function getDefaultLangByCountry()
    {
        $defaultLanguage = \Yii::$app->yaml->parseByKey('language', 'defaultLanguage');
        if (!empty($defaultLanguage[$this->country])) {
            return $defaultLanguage[$this->country];
        }

        return self::DEFAULT_LANG;
    }

    /**
     * @param $siteId
     * @return mixed
     */
    public static function getProjectFromSiteHash($siteId)
    {
        $sites          = \Yii::$app->yaml->parse('site');
        $notValidSiteId = empty($siteId) || !isset($sites[$siteId]) || empty($sites[$siteId]);
        if ($notValidSiteId) {
            return null;
        }
        $hash = $sites[$siteId]['hash'];
        $projects = \Yii::$app->yaml->parse('projectBySiteId');

        foreach ($projects as $projectName => $siteHashes) {
            if (in_array($hash, $siteHashes)) {
                return $projectName;
            }
        }

        return  null;
    }

    /**
     * @param $siteId
     * @return null|string
     */
    public static function getSiteNameFromSiteId($siteId)
    {
        $sites          = \Yii::$app->yaml->parse('site');
        $notValidSiteId = empty($siteId) || !isset($sites[$siteId]) || empty($sites[$siteId]);
        if ($notValidSiteId) {
            return null;
        }

        return $sites[$siteId]['name'] ?? null;
    }

    /**
     * @return mixed|string
     */
    public function getRecipientNiche()
    {
        if (!is_null($this->niche)) {
            return $this->niche;
        }
        $recipientNiche = self::getNicheFromSite($this->site);
        if ($recipientNiche) {
            $this->niche = $recipientNiche;
            $this->save();
        }

        return $this->niche;
    }

    /**
     * @param int $recipientId
     * @return mixed|null|string
     */
    public static function getRecipientNicheById(int $recipientId)
    {
        $model = self::findOne($recipientId);
        if (!$model) {
            return null;
        }

        if (!is_null($model->niche)) {
            return $model->niche;
        }

        $recipientNiche = self::getNicheFromSite($model->site);
        if ($recipientNiche) {
            $model->niche = $recipientNiche;
            $model->save();
        }

        return $model->niche;
    }

    /**
     * @param $site
     * @return mixed
     * @internal param $hash
     */
    public static function getNicheFromSite($site)
    {
        $niches = \Yii::$app->yaml->parse('recipientNiches');
        $site   = self::getSiteNameFromSiteId($site);

        foreach ($niches as $niche => $data) {
            if (empty($data['sites']) || is_null($data['sites'])) {
                continue;
            }
            if (in_array($site, $data['sites'])) {
                return $niche;
            }
        }

        return null;
    }
}
