<?php

namespace app\models\phoenix;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "sentCommunicationLog".
 *
 * @property integer $id
 * @property integer $site
 * @property integer $step
 * @property string $country
 * @property integer $countComm
 * @property integer $dateSent
 * @property integer $createdAt
 */
class SentCommunicationLog extends ActiveRecord
{
    public static function tableName()
    {
        return 'sentCommunicationLog';
    }
}