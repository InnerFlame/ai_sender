<?php

namespace app\models\phoenix;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SendExclusionsRulesSearch represents the model behind the search form about `app\models\phoenix\SendExclusionsRules`.
 */
class SendExclusionsRulesSearch extends SendExclusionsRules
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'isActive'], 'integer'],
            [['name', 'regDateFrom', 'regDateTo', 'countries', 'sitesIds', 'sources', 'updatedAt', 'createdAt', 'schemaIds', 'platforms'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params): ActiveDataProvider
    {
        $query = SendExclusionsRules::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'regDateFrom' => $this->regDateFrom,
            'regDateTo' => $this->regDateTo,
            'isActive' => $this->isActive,
            'updatedAt' => $this->updatedAt,
            'createdAt' => $this->createdAt,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'countries', $this->countries])
            ->andFilterWhere(['like', 'sitesIds', $this->sitesIds])
            ->andFilterWhere(['like', 'sources', $this->sources])
            ->andFilterWhere(['like', 'schemaIds', $this->schemaIds])
            ->andFilterWhere(['like', 'platforms', $this->platforms]);

        return $dataProvider;
    }
}
