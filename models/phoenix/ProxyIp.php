<?php

namespace app\models\phoenix;

/**
 * This is the model class for table "proxyIp".
 *
 * @property integer $id
 * @property integer $ip_start
 * @property integer $ip_end
 * @property string  $anonimity
 * @property string  $type
 */
class ProxyIp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proxyIp';
    }
}