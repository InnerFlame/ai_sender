<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "phoenixSenderProfileRegister".
 *
 * @property integer $id
 * @property integer $profileId
 * @property string $timeToRegister
 * @property string $registerStartedAt
 * @property string $registeredAt
 * @property integer $ruleId
 * @property integer $errorAt
 */
class SenderProfileRegister extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'senderProfileRegister';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['timeToRegister'], 'required'],
            [['profileId', 'ruleId'], 'integer'],
            [['timeToRegister', 'registeredAt', 'registerStartedAt', 'errorAt'], 'safe']
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['registerError'] = ['errorAt'];
        $scenarios['reRegister']    = ['registerStartedAt', 'timeToRegister'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'profileId'         => 'Profile ID',
            'timeToRegister'    => 'Time To Register',
            'registeredAt'      => 'Registered',
            'registerStartedAt' => 'Register Started At',
            'ruleId'            => 'Rule ID',
        ];
    }

    public function getPhoenixSenderProfile() {
        return $this->hasOne(SenderProfile::className(), ['id' => 'profileId']);
    }
}
