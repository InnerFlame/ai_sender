<?php

namespace app\models\phoenix;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * GroupSearch represents the model behind the search form about `app\models\Group`.
 */
class GroupSearch extends Group
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'schemaId', 'sendMode', 'messageCount', 'period'], 'integer'],
            [['title'], 'string'],
            [['startTimeOffset', 'endTimeOffset'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Group::find();

        $query->joinWith(['schema']);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $dataProvider->sort->attributes['phoenixSchema.title'] = [
            'asc'  => ['schema.title' => SORT_ASC],
            'desc' => ['schema.title' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'group.id'           => $this->id,
            'group.schemaId'     => $this->schemaId,
            'group.sendMode'     => $this->sendMode,
            'group.messageCount' => $this->messageCount,
            'group.period'       => $this->period
        ]);

        $query->andFilterWhere(['<=', 'group.startTimeOffset', $this->startTimeOffset])
            ->andFilterWhere(['>=', 'group.endTimeOffset', $this->endTimeOffset])
            ->andFilterWhere(['like', 'group.title', $this->title]);

        return $dataProvider;
    }
}
