<?php

namespace app\models\phoenix;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\phoenix\NichesCommTroublesLog;

/**
 * NichesCommTroublesLogSearch represents the model behind the search form about `app\models\phoenix\NichesCommTroublesLog`.
 */
class NichesCommTroublesLogSearch extends NichesCommTroublesLog
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'profileType'], 'integer'],
            [['niche', 'country', 'message', 'createdAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params): ActiveDataProvider
    {
        $query = NichesCommTroublesLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['createdAt' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (isset($this->createdAt) && !empty($this->createdAt)) {
            $fromTime = $this->createdAt . ' 00:00:00';
            $toTime   = $this->createdAt . ' 23:59:59';

            $query->where(['between', 'createdAt', $fromTime, $toTime]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'            => $this->id,
            'profileType'   => $this->profileType,
            'country'       => $this->country,
            'niche'         => $this->niche,
        ]);

        $query
            ->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}
