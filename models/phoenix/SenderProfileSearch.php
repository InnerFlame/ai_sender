<?php

namespace app\models\phoenix;

use app\models\rpc\Client;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ProfileSenderSearch represents the model behind the search form about `app\models\SenderProfile`.
 */
class SenderProfileSearch extends SenderProfile
{
    public $status;
    public $isBanned;
    public $readyToWork;
    public $timeToRegisterFrom;
    public $timeToRegisterTo;
    public $registeredFrom;
    public $registeredTo;
    public $removedAtFrom;
    public $removedAtTo;
    public $bannedAtFrom;
    public $bannedAtTo;
    public $niche;

    public static $statuses = [
        'online'  => 'online',
        'offline' =>'offline'
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'site'], 'integer'],
            [
                [
                    'readyToWork',
                    'isBanned',
                    'country',
                    'email',
                    'status',
                    'userType',
                    'originId',
                    'actionWay',
                    'timeToRegisterFrom',
                    'timeToRegisterTo',
                    'registeredFrom',
                    'registeredTo',
                    'removedAtFrom',
                    'removedAtTo',
                    'bannedAtFrom',
                    'bannedAtTo',
                    'language',
                    'photoStatus',
                    'niche',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->getQuery($params),
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        $dataProvider->sort->attributes['senderProfileRegister.timeToRegister'] = [
            'asc'  => ['senderProfileRegister.timeToRegister' => SORT_ASC],
            'desc' => ['senderProfileRegister.timeToRegister' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['senderProfileRegister.registeredAt'] = [
            'asc'  => ['senderProfileRegister.registeredAt' => SORT_ASC],
            'desc' => ['senderProfileRegister.registeredAt' => SORT_DESC],
        ];

        return $dataProvider;
    }

    public function getQuery($params)
    {
        $query = SenderProfile::find()
            ->joinWith(['senderProfileRegister']);

        $this->load($params);
        if (!$this->validate()) {
            return $query;
        }

        if ($this->readyToWork) {
            $query->andFilterWhere([
                'bannedAt'         => '0000-00-00 00:00:00',
                'deactivatedAt'    => '0000-00-00 00:00:00',
                'pausedAt'         => '0000-00-00 00:00:00',
                'removedAt'        => '0000-00-00 00:00:00',
                'photoStatus'      => SenderProfile::STATUS_APPROVED,
            ]);
        }

        $query->andFilterWhere([
            'senderProfile.id' => $this->id,
            'site'             => $this->site,
            'country'          => $this->country,
            'email'            => $this->email,
            'userType'         => $this->userType,
            'originId'         => $this->originId,
            'actionWay'        => $this->actionWay,
            'language'         => $this->language,
            'photoStatus'      => $this->photoStatus,
        ]);

        if ($this->timeToRegisterFrom) {
            $query->andFilterWhere([
                'between',
                'senderProfileRegister.timeToRegister',
                $this->timeToRegisterFrom,
                $this->timeToRegisterTo . ' 23:59:59',
            ]);
        }

        if ($this->registeredFrom) {
            $query->andFilterWhere([
                'between',
                'senderProfileRegister.registeredAt',
                $this->registeredFrom,
                $this->registeredTo . ' 23:59:59',
            ]);
        }

        if ($this->removedAtFrom) {
            $query->andFilterWhere([
                'between',
                'removedAt',
                $this->removedAtFrom,
                $this->removedAtTo . ' 23:59:59',
            ]);
        }

        if ($this->status == 'online') {
            $query->andFilterWhere(['in', 'senderProfile.id', $this->getOnlineProfileIds()]);
        }

        if ($this->status == 'offline') {
            $query->andFilterWhere(['not in', 'senderProfile.id', $this->getOnlineProfileIds()]);
        }

        if ($this->isBanned == 'no') {
            $query->andFilterWhere(['bannedAt' => 0]);
        } elseif ($this->isBanned == 'yes') {
            $query->andFilterWhere(['>', 'bannedAt', '0000-00-00 00:00:00']);
        }

        if ($this->isBanned != 'no' && !empty($this->bannedAtFrom)) {
            $query->andFilterWhere([
                'between',
                'bannedAt',
                 $this->bannedAtFrom,
                 $this->bannedAtTo . ' 23:59:59',
            ]);
        }

        if ($this->niche) {
            \Yii::$container->get('nicheQuery')->addToQuery($query, $this->niche, SenderProfile::tableName());
        }

        return $query;
    }

    private function getOnlineProfileIds()
    {
        $client = Client::find()->where(['status' => Client::STATUS_STARTED])->all();

        return ArrayHelper::getColumn($client, 'profileId');
    }
}
