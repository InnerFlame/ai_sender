<?php

namespace app\models\phoenix;

use sender\registration\components\ProfilesFieldConverter;

/**
 * This is the model class for table "phoenixSenderProfile".
 *
 * @property integer $id
 * @property string $firstName
 * @property string $lastName
 * @property string $screenName
 * @property string $birthday
 * @property integer $site
 * @property string $country
 * @property integer $sexuality
 * @property integer $gender
 * @property string $description
 * @property integer $photo
 * @property string $email
 * @property string $internalDeviceIdentifier
 * @property string $deviceIdHex
 * @property string $gtmClientId
 * @property integer $userAgent
 * @property string $language
 * @property integer $version
 * @property integer $bundle
 * @property string $marker
 * @property string $createdAt
 * @property string $originId
 * @property string $accessToken
 * @property string $refreshToken
 * @property string $bannedAt
 * @property string $photoStatus
 * @property string $userType
 * @property string $photoUploadedAt
 * @property string $location
 * @property string $lastTimeOnline
 * @property integer $online
 * @property string $password
 * @property string $state
 * @property string $city
 * @property string $deactivatedAt
 * @property string $pausedAt
 * @property string $ip
 * @property string $actionWay
 * @property string $timeOffset
 * @property string $canBeInPullAt
 * @property string $liveTime
 * @property string $removedAt
 */
class SenderProfile extends \yii\db\ActiveRecord
{
    const STATUS_UPLOAD                = 'upload';
    const STATUS_ERROR                 = 'error';
    const STATUS_APPROVED              = 'approved';
    const STATUS_DECLINED              = 'declined';
    const STATUS_NO_PHOTO              = 'noPhoto';
    const STATUS_UPLOADED              = 'uploaded';
    const STATUS_EMPTY_PHOTO_IN_SENDER = 'emptyPhotoInSender';

    const STATUS_NO_PHOTO_FOR_REUPLOAD          = 'noPhotoForReupload';
    const STATUS_TO_REUPLOAD                    = 'toReupload';
    const STATUS_TO_REUPLOAD_CONNECTION_REFUSED = 'toReuploadConnectionRefused';
    const STATUS_ATTEMPTS_TO_END_FOR_REUPLOAD   = 'attemptsToEndForReupload';

    const TYPE_SENDER   = 'sender';
    const TYPE_OBSERVER = 'observer';
    const TYPE_SEARCHER = 'searcher';

    const ACTION_WAY_EXTERNAL = 'external';
    const ACTION_WAY_DEFAULT  = 'default';
    const ACTION_WAY_USED     = 'external';

    /** @var string[] $userTypes */
    public static $userTypes = [
        self::TYPE_SENDER   => self::TYPE_SENDER,
        self::TYPE_OBSERVER => self::TYPE_OBSERVER,
        self::TYPE_SEARCHER => self::TYPE_SEARCHER,
    ];

    /** @var string[] $photoStatuses */
    public static $photoStatuses = [
        self::STATUS_UPLOAD   => self::STATUS_UPLOAD,
        self::STATUS_ERROR    => self::STATUS_ERROR,
        self::STATUS_APPROVED => self::STATUS_APPROVED,
        self::STATUS_DECLINED => self::STATUS_DECLINED,
        self::STATUS_NO_PHOTO => self::STATUS_NO_PHOTO,
        self::STATUS_UPLOADED => self::STATUS_UPLOADED,
    ];

    /** @var null $isNicheSender */
    public $isNicheSender = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'senderProfile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstName', 'lastName', 'birthday', 'site', 'country', 'sexuality', 'gender'], 'required'],
            [['birthday', 'createdAt', 'bannedAt'], 'safe'],
            [['originId', 'accessToken', 'refreshToken', 'userType'], 'safe'],
            [['site', 'sexuality', 'gender', 'userAgent', 'version'], 'integer'],
            [['description'], 'string'],
            [['firstName', 'lastName', 'screenName', 'email'], 'string', 'max' => 50],
            [['internalDeviceIdentifier', 'marker'], 'string', 'max' => 32],
            [['gtmClientId'], 'string', 'max' => 36],
            [['deviceIdHex'], 'string', 'max' => 48],
            [['language'], 'string', 'max' => 5],
            [
                [
                    'photoStatus',
                    'photo',
                    'photoUploadedAt',
                    'location',
                    'lastTimeOnline',
                    'online',
                    'ip',
                    'actionWay',
                    'timeOffset',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                       => 'ID',
            'firstName'                => 'First Name',
            'lastName'                 => 'Last Name',
            'screenName'               => 'Screen Name',
            'birthday'                 => 'Birthday',
            'site'                     => 'Site',
            'country'                  => 'Country',
            'sexuality'                => 'Sexuality',
            'gender'                   => 'Gender',
            'description'              => 'Description',
            'photo'                    => 'Photo',
            'email'                    => 'Email',
            'internalDeviceIdentifier' => 'Internal Device Identifier',
            'deviceIdHex'              => 'Device Id Hex',
            'gtmClientId'              => 'Gtm Client ID',
            'userAgent'                => 'User Agent',
            'language'                 => 'Language',
            'version'                  => 'Version',
            'marker'                   => 'Marker',
            'originId'                 => 'Origin Id',
            'accessToken'              => 'Access Token',
            'refreshToken'             => 'Refresh Token',
            'createdAt'                => 'Created At',
            'photoStatus'              => 'Photo Status',
            'photoUploadedAt'          => 'Photo Uploaded At',
            'location'                 => 'Location',
            'lastTimeOnline'           => 'lastTimeOnline',
        ];
    }

    public function getSenderProfileRegister()
    {
        return $this->hasOne(SenderProfileRegister::className(), ['profileId' => 'id']);
    }

    public function getSenderProfileRemoveSchedule()
    {
        return $this->hasMany(SenderProfileRemoveSchedule::className(), ['senderId' => 'id']);
    }

    public function afterDelete()
    {
        parent::afterDelete();

        SenderProfileRegister::deleteAll(['profileId' => $this->id]);
    }

    public static function getCountByBirthday(string $birthday)
    {
        return self::find()->where(['birthday' => $birthday])->count();
    }

    public static function getCount()
    {
        return self::find()->count();
    }

    public static function deactivate(int $id)
    {
        $sender = self::findOne($id);

        if ($sender) {
            $sender->deactivatedAt = date('Y-m-d H:i:s');
            $sender->save();
        }
    }

    public static function temporaryDeactivate(int $id)
    {
        $sender = self::findOne($id);

        if ($sender) {
            $sender->pausedAt = date('Y-m-d H:i:s');
            $sender->save();
        }
    }

    public static function setStatusOffline(int $id)
    {
        $senderProfile = self::findOne($id);

        if ($senderProfile) {
            $senderProfile->lastTimeOnline = date('Y-m-d H:i:s');
            $senderProfile->online         = 0;
            $senderProfile->save();
        }
    }

    public static function setStatusOnline(int $id)
    {
        $senderProfile = self::findOne($id);

        if ($senderProfile) {
            $senderProfile->online = 1;
            $senderProfile->save();
        }
    }

    public function getSenderProfileAttributes()
    {
        return $this->hasOne(
            SenderProfileAttributes::className(),
            ['id' => 'id']
        )->inverseOf('senderProfile');
    }

    public function getSenderProfileAttributesForRegistration()
    {
        $attributes = [
            'height'         => ProfilesFieldConverter::convertLocalToRemote(
                'height',
                $this->senderProfileAttributes->height
            ),
            'weight'         => ProfilesFieldConverter::convertLocalToRemote(
                'weight',
                $this->senderProfileAttributes->weight
            ),
            'build'          => ProfilesFieldConverter::convertLocalToRemote(
                'build',
                $this->senderProfileAttributes->build
            ),
            'race'           => ProfilesFieldConverter::convertLocalToRemote(
                'race',
                $this->senderProfileAttributes->race
            ),
            'glasses'        => ProfilesFieldConverter::convertLocalToRemote(
                'glasses',
                $this->senderProfileAttributes->glasses
            ),
            'religion'       => ProfilesFieldConverter::convertLocalToRemote(
                'religion',
                $this->senderProfileAttributes->religion
            ),
            'marital_status' => ProfilesFieldConverter::convertLocalToRemote(
                'familyStatus',
                $this->senderProfileAttributes->familyStatus
            ),
            'eye_color'      => ProfilesFieldConverter::convertLocalToRemote(
                'eyeColor',
                $this->senderProfileAttributes->eyeColor
            ),
            'hair_color'     => ProfilesFieldConverter::convertLocalToRemote(
                'hairColor',
                $this->senderProfileAttributes->hairColor
            ),
        ];

        return $attributes;
    }

    /**
     * @return int
     */
    public function getAge()
    {
        return (int) date_diff(date_create($this->birthday), date_create('now'))->y;
    }

    public function changePhotoStatus(string $status)
    {
        $this->photoStatus = $status;
        $this->save();
    }
}
