<?php

namespace app\models\phoenix;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SchemaSearch represents the model behind the search form about `app\models\Schema`.
 */
class SchemaSearch extends Schema
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ageFrom', 'ageTo', 'isPaid', 'gender', 'sexuality'], 'integer'],
            [['title', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Schema::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ageFrom' => $this->ageFrom,
            'ageTo' => $this->ageTo,
            'isPaid' => $this->isPaid,
            'sexuality' => $this->sexuality,
            'gender' => $this->gender,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
