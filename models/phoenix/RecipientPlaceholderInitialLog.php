<?php

namespace app\models\phoenix;

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $recipientId
 * @property integer $countSent
 * @property integer $lastRestart
 * @property string $createdAt
 */
class RecipientPlaceholderInitialLog extends ActiveRecord
{
    public static function tableName()
    {
        return 'recipientPlaceholderInitialLog';
    }

    public static function createRecord($recipientId, $restartNumber)
    {
        $recipientPlaceholderInitial              = new self;
        $recipientPlaceholderInitial->recipientId = $recipientId;
        $recipientPlaceholderInitial->countSent   = 1;
        $recipientPlaceholderInitial->lastRestart = $restartNumber;
        $recipientPlaceholderInitial->save();

        return $recipientPlaceholderInitial;
    }

    public function updateRecord($restart)
    {
        $this->lastRestart = $restart;
        $this->countSent++;
        $this->save();
    }
}