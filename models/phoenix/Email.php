<?php

namespace app\models\phoenix;

use yii\base\Model;

class Email extends Model
{
    public $emailFile;

    public function rules()
    {
        return [['emailFile', 'file', 'skipOnEmpty' => false, 'extensions' => 'csv']];
    }
}