<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "recipientSchema".
 *
 * @property integer $id
 * @property integer $recipientId
 * @property integer $schemaId
 * @property integer $groupId
 * @property string $createdAt
 * @property string $activatedAt
 * @property string $startedAt
 * @property integer $restartNumber
 * @property boolean $isProcessed
 */
class RecipientSchema extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recipientSchema';
    }
}
