<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "delaySentCommunication".
 *
 * @property integer $id
 * @property integer $countSent
 * @property integer $delay
 * @property integer $step
 * @property string $timeToSend
 * @property string $createdAt
 */
class DelaySentCommunication extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'delaySentCommunication';
    }
}