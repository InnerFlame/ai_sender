<?php

namespace app\models\phoenix;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%sendExclusionsRules}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $regDateFrom
 * @property string $regDateTo
 * @property string $countries
 * @property string $sitesIds
 * @property string $sources
 * @property integer $isActive
 * @property string $updatedAt
 * @property string $createdAt
 * @property string $schemaIds
 * @property string $platforms
 */
class SendExclusionsRules extends \yii\db\ActiveRecord
{
    /** @var array $countriesArray */
    public $countriesArray;

    /** @var array $sitesIdsArray */
    public $sitesIdsArray;

    /** @var array $sourcesArray */
    public $sourcesArray;

    /** @var array $schemaIdsArray */
    public $schemaIdsArray;

    /** @var array $platformsArray */
    public $platformsArray;

    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors(): array
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
                'value'              => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $propertiesToReplace = $this->getAttributes([
            'countries',
            'sitesIds',
            'sources',
            'schemaIds',
            'platforms',
        ]);

        foreach ($propertiesToReplace as $name => $value) {
            if (!empty($value)) {
                $this->{$name . 'Array'} = json_decode($value);
            }
        }
    }

    /**
     * @inheritdoc
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert): bool
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $propertiesToReplace = get_object_vars($this);

        foreach ($propertiesToReplace as $name => $value) {
            $activeRecordProperty = str_replace('Array', '', $name);
            if (!empty($value)) {
                $this->{$activeRecordProperty} = json_encode($value);
            } else {
                $this->{$activeRecordProperty} = NULL;
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return '{{%sendExclusionsRules}}';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [
                [
                    'regDateFrom',
                    'regDateTo',
                    'updatedAt',
                    'createdAt',
                    'countriesArray',
                    'sitesIdsArray',
                    'sourcesArray',
                    'schemaIdsArray',
                    'platformsArray',
                ],
                'safe'
            ],
            [['countries', 'sitesIds', 'sources', 'schemaIds', 'platforms'], 'string'],
            [['isActive'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id'                => 'ID',
            'name'              => 'Name',
            'regDateFrom'       => 'Reg Date From',
            'regDateTo'         => 'Reg Date To',
            'countries'         => 'Countries',
            'sitesIds'          => 'Sites Ids',
            'sources'           => 'Sources',
            'isActive'          => 'Is Active',
            'updatedAt'         => 'Updated At',
            'createdAt'         => 'Created At',
            'countriesArray'    => 'Countries',
            'sitesIdsArray'     => 'Sites Ids',
            'sourcesArray'      => 'Sources',
        ];
    }

    public function getSourcesValues(): array 
    {
        $sources = \Yii::$app->yaml->parse('sources');

        return array_combine($sources, $sources);
    }
}
