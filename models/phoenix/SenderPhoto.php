<?php

namespace app\models\phoenix;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "phoenixSenderPhoto".
 *
 * @property integer $id
 * @property string $name
 * @property integer $isUsed
 * @property string $createdAt
 * @property integer $level
 * @property integer $birthdayYear
 * @property integer $birthdayMonth
 * @property integer $birthdayDay
 * @property integer $isBad
 * @property integer $badPhotoReason
 */
class SenderPhoto extends ActiveRecord
{
    const PHOTO_BASE_PATH = '/images/senderPhoto/';
    const MIN_HEIGHT = 320;
    const MIN_WIDTH = 320;
    const BAD_REASON_ALREADY_USED = 'already_used';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'senderPhoto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['isUsed', 'level', 'birthdayYear', 'birthdayMonth', 'birthdayDay', 'isBad'], 'integer'],
            [['createdAt'], 'safe'],
            [['name'], 'string', 'max' => 45],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'name'          => 'Name',
            'isUsed'        => 'Is Used',
            'createdAt'     => 'Created At',
            'level'         => 'Level',
            'birthdayYear'  => 'Birthday Year',
            'birthdayMonth' => 'Birthday Month',
            'birthdayDay'   => 'Birthday Day',
        ];
    }

    public function validateAndMarkBadPhoto()
    {
        $fileName = \Yii::$app->basePath . '/web' . SenderPhoto::PHOTO_BASE_PATH . $this->name;
        if (!is_file($fileName) && !is_readable($fileName)) {
            $this->isBad = true;
            $this->badPhotoReason = 'file not accessible';
            $this->save();
            return false;
        }
        $size = @getimagesize($fileName);
        if (!$size) {
            $this->isBad = true;
            $this->badPhotoReason = 'cannot get image size';
            $this->save();
            return false;
        }

        if ($size[0] < self::MIN_WIDTH || $size[1] < self::MIN_HEIGHT) {
            $this->isBad = true;
            $this->badPhotoReason = 'image size less than ' . self::MIN_HEIGHT . 'x' . self::MIN_WIDTH;
            $this->save();
            return false;
        }
        return true;
    }
}
