<?php

namespace app\models\phoenix;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%userExclusionsRules}}".
 *
 * @property integer $id
 * @property string $genders
 * @property string $orientation
 * @property string $isPaid
 * @property string $searchable
 * @property string $registrationPlatforms
 * @property string $excludedSources
 * @property integer $isActive
 * @property string $updatedAt
 */
class UserExclusionsRules extends \yii\db\ActiveRecord
{
    /** @var array $gendersArray */
    public $gendersArray;

    /** @var array $orientationArray */
    public $orientationArray;

    /** @var array $isPaidArray */
    public $isPaidArray;

    /** @var array $searchableArray */
    public $searchableArray;

    /** @var array $registrationPlatformsArray */
    public $registrationPlatformsArray;

    /** @var array $excludedSourcesArray */
    public $excludedSourcesArray;

    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors(): array
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => false,
                'updatedAtAttribute' => 'updatedAt',
                'value'              => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $propertiesToReplace = $this->getAttributes(null, ['id', 'isActive', 'updatedAt']);

        foreach ($propertiesToReplace as $name => $value) {
            if (!empty($value)) {
                $this->{$name . 'Array'} = json_decode($value);
            }
        }
    }

    /**
     * @inheritdoc
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert): bool
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $propertiesToReplace = get_object_vars($this);

        foreach ($propertiesToReplace as $name => $value) {
            $activeRecordProperty = str_replace('Array', '', $name);
            if (!empty($value)) {
                $this->{$activeRecordProperty} = json_encode($value);
            } else {
                $this->{$activeRecordProperty} = NULL;
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return '{{%userExclusionsRules}}';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['registrationPlatforms', 'excludedSources'], 'string'],
            [['isActive'], 'integer'],
            [
                [
                    'updatedAt',
                    'gendersArray',
                    'orientationArray',
                    'isPaidArray',
                    'searchableArray',
                    'registrationPlatformsArray',
                    'excludedSourcesArray'
                ],
                'safe'
            ],
            [['genders', 'orientation', 'isPaid', 'searchable'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id'                         => 'ID',
            'genders'                    => 'Genders',
            'orientation'                => 'Orientation',
            'isPaid'                     => 'Is Paid',
            'searchable'                 => 'Searchable',
            'registrationPlatforms'      => 'Registration Platforms',
            'excludedSources'            => 'Excluded Sources',
            'isActive'                   => 'Is Active',
            'updatedAt'                  => 'Updated At',
            'gendersArray'               => 'Genders',
            'orientationArray'           => 'Orientation',
            'isPaidArray'                => 'Is Paid',
            'searchableArray'            => 'Searchable',
            'registrationPlatformsArray' => 'Registration Platforms',
            'excludedSourcesArray'       => 'Excluded Sources',
        ];
    }

    /**
     * =======================Getters for dropDowns========================
     */

    public function getSearchableValues(): array
    {
        return [
            'true' => 'True'
        ];
    }

    public function getIsPaidValues(): array
    {
        return [
            'true' => 'True'
        ];
    }

    public function getExcludedSources(): array
    {
        return [
            'dd' => 'dd',
            'direct' => 'direct',
            'seo' => 'seo',
            'brand' => 'brand',
            'intc' => 'intc',
            'cpac' => 'cpac',
            'InnerClean' => 'InnerClean',
            'ddc' => 'ddc',
            'ppc' => 'ppc',
            'ppcc' => 'ppcc'
        ];
    }

    public function getOrientationsValues(): array
    {
        return [
            '1' => '1'
        ];
    }

    /** ================================================================= */
}
