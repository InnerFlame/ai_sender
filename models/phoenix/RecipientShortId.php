<?php

namespace app\models\phoenix;


/**
 * This is the model class for table "recipientShortId".
 *
 * @property integer $id
 * @property string $recipientId
 * @property string $shortId
 * @property string $remoteId
 * @property string $country
 * @property integer $site
 * @property string $createdAt
 */
class RecipientShortId extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recipientShortId';
    }
}
