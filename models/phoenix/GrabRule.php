<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "phoenixGrabRule".
 *
 * @property integer $id
 * @property string $country
 * @property string $city
 * @property string $state
 * @property integer $ageFrom
 * @property integer $ageTo
 * @property integer $isActive
 * @property string $createdAt
 * @property integer $site
 * @property integer $sendRuleId
 */
class GrabRule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grabRule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country'], 'required'],
            [['ageFrom', 'ageTo', 'isActive', 'site', 'sendRuleId'], 'integer'],
            [['createdAt'], 'safe'],
            [['country'], 'string', 'max' => 3],
            [['city'], 'string', 'max' => 50],
            [['state'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'country'   => 'Country',
            'city'      => 'City',
            'state'     => 'State',
            'ageFrom'   => 'Age From',
            'ageTo'     => 'Age To',
            'isActive'  => 'Is Active',
            'createdAt' => 'Created At',
            'site'      => 'Site',
        ];
    }

    public static function generate(SendRule $sendRule)
    {
        $grabRule             = new self;
        $grabRule->country    = $sendRule->country;
        $grabRule->site       = $sendRule->site;
        $grabRule->isActive   = $sendRule->isActive;
        $grabRule->ageFrom    = $sendRule->schema->ageFrom;
        $grabRule->ageTo      = $sendRule->schema->ageTo;
        $grabRule->sendRuleId = $sendRule->id;

        $grabRule->save();
    }

    public static function updateForGenerate(SendRule $sendRule)
    {
        $grabRule = self::findOne(['sendRuleId' => $sendRule->id]);

        if ($grabRule) {
            $grabRule->country    = $sendRule->country;
            $grabRule->site       = $sendRule->site;
            $grabRule->isActive   = $sendRule->isActive;
            $grabRule->ageFrom    = $sendRule->schema->ageFrom;
            $grabRule->ageTo      = $sendRule->schema->ageTo;

            $grabRule->save();
        }
    }
}