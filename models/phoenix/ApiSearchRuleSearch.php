<?php

namespace app\models\phoenix;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\phoenix\ApiSearchRule;

/**
 * ApiSearchRuleSearch represents the model behind the search form about `app\models\phoenix\ApiSearchRule`.
 */
class ApiSearchRuleSearch extends ApiSearchRule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'isActive'], 'integer'],
            [['country'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query        = ApiSearchRule::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'       => $this->id,
            'isActive' => $this->isActive,
            'country'  => $this->country,
        ]);

        return $dataProvider;
    }
}
