<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "phoenixProcessedActivities".
 *
 * @property integer $schemaId
 * @property integer $recipientId
 * @property integer $groupId
 * @property integer $activityId
 */
class ProcessedActivities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'processedActivities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['schemaId', 'recipientId', 'groupId', 'activityId'], 'integer'],
            [['schemaId', 'recipientId', 'groupId', 'activityId'], 'unique', 'targetAttribute' => ['schemaId', 'recipientId', 'groupId', 'activityId'], 'message' => 'The combination of Schema ID, Recipient ID, Group ID and Activity ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'schemaId' => 'Schema ID',
            'recipientId' => 'Recipient ID',
            'groupId' => 'Group ID',
            'activityId' => 'Activity ID',
        ];
    }
}
