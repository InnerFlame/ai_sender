<?php

namespace app\models\phoenix;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%searchClient}}".
 *
 * @property integer $id
 * @property integer $profileId
 * @property integer $ruleId
 * @property string $location
 * @property string $createdAt
 */
class SearchClient extends \yii\db\ActiveRecord
{
    /**
     * Bind createdAt value on create event
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%searchClient}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profileId', 'ruleId'], 'integer'],
            [['createdAt'], 'safe'],
            [['location'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profileId' => 'Profile ID',
            'ruleId' => 'Rule ID',
            'location' => 'Location',
            'createdAt' => 'Created At',
        ];
    }
}
