<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "excludedSite".
 *
 * @property integer $id
 * @property string $country
 * @property integer $site
 * @property integer $isActive
 * @property string $createdAt
 */
class ExcludedSite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'excludedSite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site'], 'required'],
            [['isActive'], 'integer'],
            [['createdAt'], 'safe'],
            [['site', 'country'], 'unique', 'targetAttribute' => ['site', 'country'], 'message' => 'The combination of Site and Country has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country' => 'Country',
            'site' => 'Site',
            'isActive' => 'Is Active',
            'createdAt' => 'Created At',
        ];
    }
}
