<?php

namespace app\models\phoenix;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "senderEmail".
 *
 * @property integer $id
 * @property string $email
 * @property integer $isUsed
 * @property string $createdAt
 */
class SenderEmail extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'senderEmail';
    }

    public static function reuse(SenderProfile $senderProfile)
    {
        $email = self::findOne(['email' => $senderProfile->email]);
        if (!empty($email)) {
            $email->isUsed = 0;
            $email->save();
        }
    }
}
