<?php

namespace app\models\phoenix;

/**
 * This is the model class for table "rpcErrorLog".
 *
 * @property integer $id
 * @property string $method
 * @property string $error
 * @property integer $errorCount
 * @property string $errorDate
 * @property string $createdAt
 */
class RpcErrorLog extends \yii\db\ActiveRecord
{
    private static $errorsType = [
        'proxyError'        => 'proxy',
        'massBlockedLogin'  => 'massBlocked\/protect\/login',
        'timeout'           => 'timeout',
        'notFoundContainer' => 'not found container',
        'notLogged'         => 'not logged',
        'notStarted'        => 'not started',
        'alreadyLogged'     => 'already logged',
        'alreadyStarted'    => 'already started',
        'unexpectedEOF'     => 'unexpected EOF',
        'internalServer'    => 'An internal server error has occurred',
        'unknownAuthority'  => 'unknownAuthority',
        'unknownError'      => 'unknown error',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rpcErrorLog';
    }

    public static function getErrorsType()
    {
        return self::$errorsType;
    }
}