<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "phoenixStepMessage".
 *
 * @property integer $id
 * @property string $body
 * @property string $createdAt
 * @property string $hash
 * @property integer $hasPlaceholder
 */
class StepMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stepMessage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['createdAt', 'hasPlaceholder'], 'safe'],
            [['hash'], 'string', 'max' => 32],
            [['hash'], 'unique'],
        ];
    }
}