<?php

namespace app\models\phoenix;

use yii\db\ActiveRecord;

/**
 *
 * @property integer $id
 * @property integer $countBanned
 * @property string $country
 * @property string $bannedAt
 * @property string $createdAt
 */
class SenderBanStat extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'senderBanStat';
    }
}