<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "phoenixStepMessage".
 *
 * @property integer $id
 * @property integer $countGrab
 * @property integer $countNotSent
 * @property string $country
 * @property string $grabbedAt
 * @property string $createdAt
 */
class UnprocessedRecipient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unprocessedRecipient';
    }
}
