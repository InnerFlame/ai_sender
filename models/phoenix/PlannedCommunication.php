<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "plannedCommunication".
 *
 * @property integer $id
 * @property integer $countToSend
 * @property integer $countNotSent
 * @property integer $countSent
 * @property integer $step
 * @property string $timeToSend
 * @property string $createdAt
 */
class PlannedCommunication extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plannedCommunication';
    }
}