<?php

namespace app\models\phoenix;


/**
 * This is the model class for table "prepaidRecipients".
 *
 * @property integer $recipientId
 */
class PrepaidRecipients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prepaidRecipients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recipientId'], 'integer'],
            [['recipientId'], 'unique'],
            [['recipientId'], 'required']
        ];
    }
}
