<?php

namespace app\models\phoenix;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\phoenix\CommunicationContainer;

/**
 * CommunicationContainerSearch represents the model behind the search form about `app\models\phoenix\CommunicationContainer`.
 */
class CommunicationContainerSearch extends CommunicationContainer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'countStarted', 'isActive'], 'integer'],
            [['country'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query        = CommunicationContainer::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'           => $this->id,
            'country'      => $this->country,
            'countStarted' => $this->countStarted,
            'isActive'     => $this->isActive,
        ]);

        return $dataProvider;
    }
}
