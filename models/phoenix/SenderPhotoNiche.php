<?php

namespace app\models\phoenix;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "phoenixSenderPhoto".
 *
 * @property integer $id
 * @property integer $photoId
 * @property string $niche
 * @property string $createdAt
 */
class SenderPhotoNiche extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'senderPhotoNiche';
    }
}