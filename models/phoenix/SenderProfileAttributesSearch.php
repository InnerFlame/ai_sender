<?php

namespace app\models\phoenix;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class SenderProfileAttributesSearch extends SenderProfileAttributes
{
    public $status;
    public $age;
    public $language;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'photoLevel', 'weight', 'race', 'age'], 'integer'],
            [['language'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SenderProfile::find();

        $query->joinWith(['senderProfileAttributes']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'senderProfile.id' => $this->id,
            'senderProfile.language' => $this->language,
        ]);
        if (is_numeric($this->photoLevel) && $this->photoLevel == 0) {
            $query->andWhere([
                'or',
                ['is', 'senderProfileAttributes.photoLevel', null],
                ['=', 'senderProfileAttributes.photoLevel', 0],
            ]);
        } else {
            $query->andFilterWhere(['senderProfileAttributes.photoLevel' => $this->photoLevel]);
        }
        if (is_numeric($this->weight) && $this->weight == 0) {
            $query->andWhere([
                'or',
                ['is', 'senderProfileAttributes.weight', null],
                ['=', 'senderProfileAttributes.weight', 0],
            ]);
        } else {
            $query->andFilterWhere(['senderProfileAttributes.weight' => $this->weight]);
        }
        if (is_numeric($this->race) && $this->race == 0) {
            $query->andWhere([
                'or',
                ['is', 'senderProfileAttributes.race', null],
                ['=', 'senderProfileAttributes.race', 0],
            ]);
        } else {
            $query->andFilterWhere(['senderProfileAttributes.race' => $this->race]);
        }
        if ($this->age) {
            $query->andFilterWhere(['>', 'senderProfile.birthday', (new \DateTime(($this->age + 1) . ' years ago'))->format('Y-m-d')]);
            $query->andFilterWhere(['<=', 'senderProfile.birthday', (new \DateTime(($this->age) . ' years ago'))->format('Y-m-d')]);
        }
        return $dataProvider;
    }
}
