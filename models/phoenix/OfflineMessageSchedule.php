<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "phoenixOfflineMessageSchedule".
 *
 * @property integer $id
 * @property integer $profileId
 * @property string $startedCheck
 * @property string $createdAt
 */
class OfflineMessageSchedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'offlineMessageSchedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profileId'], 'required'],
            [['profileId'], 'integer'],
            [['startedCheck', 'createdAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'profileId'    => 'Profile ID',
            'startedCheck' => 'Started Check',
            'createdAt'    => 'Created At',
        ];
    }
}
