<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "phoenixActivityScheduler".
 *
 * @property integer $id
 * @property integer $recipientId
 * @property integer $senderId
 * @property integer $activityType
 * @property string $createdAt
 * @property string $timeToSend
 * @property string $sentAt
 */
class ActivityScheduler extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activityScheduler';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['senderId', 'activityType'], 'integer'],
            [['createdAt', 'timeToSend', 'sentAt','recipientId', 'senderId'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'recipientId' => 'Recipient ID',
            'senderId' => 'Sender ID',
            'activityType' => 'Activity Type',
            'createdAt' => 'Created At',
            'timeToSend' => 'Time To Send',
            'sentAt' => 'Sent At',
        ];
    }
}
