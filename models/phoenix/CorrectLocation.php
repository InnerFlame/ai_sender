<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "correctLocation".
 *
 * @property integer $id
 * @property integer $correctName
 * @property string $inCorrectName
 * @property string $createdAt
 */
class CorrectLocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'correctLocation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['correctName', 'inCorrectName'], 'required'],
            [['createdAt'], 'safe'],
            [['inCorrectName', 'correctName'], 'string', 'max' => 255],
        ];
    }
}