<?php

namespace app\models\phoenix;

use yii\db\ActiveQuery;

/**
 * This is the model class for table "phoenixSendRule".
 *
 * @property integer $id
 * @property integer $schemaId
 * @property integer $site
 * @property string  $project
 * @property integer $country
 * @property integer $isActive
 */
class SendRule extends \yii\db\ActiveRecord
{
    public $traffSourcesArray = [];
    
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'sendRule';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['schemaId', 'country'], 'required'],
            [['schemaId', 'site', 'country', 'project'], 'unique',
                'targetAttribute' => ['schemaId', 'site', 'country', 'project'],
                'message' => 'The combination of Schema ID, Site, Project, Country  has already been taken.'],
            [['isActive'], 'safe'],
            [['traffSourcesArray'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id'                => 'ID',
            'schemaId'          => 'Schema',
            'site'              => 'Site',
            'project'           => 'Project',
            'country'           => 'Country',
            'isActive'          => 'Active',
            'traffSourcesArray' => 'Traffic sources'
        ];
    }

    public function getSchema(): ActiveQuery
    {
        return $this->hasOne(Schema::className(), ['id' => 'schemaId']);
    }

    public function afterFind()
    {
        $traffSources = [];
        $traffSourcesModels = SendRuleToTraffSource::findAll(['sendRuleId' => $this->id]);
        foreach ($traffSourcesModels as $traffSourcesModel) {
            $traffSources[] = $traffSourcesModel->traffSource;
        }

        $this->traffSourcesArray = $traffSources;

        parent::afterFind();
    }

    public function beforeSave($insert): bool
    {
        if (empty($this->project)) {
            $this->project = null;
        }

        if (empty($this->site)) {
            $this->site = null;
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->refresh();

        SendRuleToTraffSource::deleteAll(['sendRuleId' => $this->id]);

        if (!empty($this->traffSourcesArray)) {
            foreach ($this->traffSourcesArray as $sourceItem) {
                $newRelation = new SendRuleToTraffSource([
                    'sendRuleId'  => $this->id,
                    'traffSource' => $sourceItem
                ]);
                $newRelation->save();
            }
        }


        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete()
    {
        SendRuleToTraffSource::deleteAll(['sendRuleId' => $this->id]);

        parent::afterDelete();
    }
}
