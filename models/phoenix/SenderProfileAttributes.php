<?php

namespace app\models\phoenix;

/**
 * @property integer $id
 * @property integer $photoLevel
 * @property integer $height
 * @property integer $weight
 * @property integer $build
 * @property integer $race
 * @property integer $eyeColor
 * @property integer $hairColor
 * @property integer $familyStatus
 * @property boolean $glasses
 * @property integer $religion
 */
class SenderProfileAttributes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'senderProfileAttributes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['photoLevel', 'height', 'weight', 'build', 'race', 'eyeColor', 'hairColor', 'familyStatus', 'religion'], 'default', 'value' => 0],
            [['photoLevel', 'height', 'weight', 'build', 'race', 'eyeColor', 'hairColor', 'familyStatus', 'religion', 'glasses'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'photoLevel' => 'Photo level',
            'height' => 'Height',
            'weight' => 'Weight',
            'build' => 'Build',
            'race' => 'Race',
            'eyeColor' => 'Eyecolor',
            'hairColor' => 'Hair color',
            'familyStatus' => 'Family status',
            'glasses' => 'Glasses',
            'religion' => 'Religion',
        ];
    }

    public function getSenderProfile()
    {
        return $this->hasOne(SenderProfile::className(), ['id' => 'id']);
    }

    public static function primaryKey()
    {
        return ['id'];
    }
}
