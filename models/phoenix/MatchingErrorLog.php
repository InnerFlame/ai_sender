<?php

namespace app\models\phoenix;


/**
 * This is the model class for table "phoenixMatchingErrorLog".
 *
 * @property integer $id
 * @property integer $communicationId
 * @property string $entity
 * @property string $createdAt
 * @property string $target
 */
class MatchingErrorLog extends \yii\db\ActiveRecord
{
    const MESSAGE       = 'message';
    const SENDER        = 'sender';
    const CLIENT        = 'client';
    const RULE          = 'rule';
    const PLATFORM      = 'platform';
    const SENDER_BANNED = 'senderBanned';
    const EXCLUDED_BY_TOPIC = 'excludedByTopic';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'matchingErrorLog';
    }
}
