<?php

namespace app\models\phoenix;

/**
 * This is the model class for table "geoIp".
 *
 * @property integer $id
 * @property integer $ip_start
 * @property integer $ip_end
 * @property string  $country_code3
 * @property float  $latitude
 * @property float  $longitude
 * @property integer $rnd
 * @property string  $octets
 */
class GeoIp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geoIp';
    }
}