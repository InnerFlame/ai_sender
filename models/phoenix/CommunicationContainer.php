<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "communicationContainers".
 *
 * @property integer $id
 * @property string $country
 * @property integer $countStarted
 * @property integer $isActive
 * @property string $createdAt
 */
class CommunicationContainer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'communicationContainer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'countStarted', 'isActive'], 'required'],
            [['countStarted', 'isActive'], 'integer'],
            [['createdAt'], 'safe'],
            [['country'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'country'      => 'Country',
            'countStarted' => 'Count Started',
            'isActive'     => 'Is Active',
            'createdAt'    => 'Created At',
        ];
    }

    public static function getActiveData()
    {
        return self::find()
            ->select('country, countStarted')
            ->where(['isActive' => 1])
            ->asArray()
            ->all();
    }

    public static function getActiveDataByCountry($country)
    {
        return self::find()
            ->select('country, countStarted')
            ->where([
                'isActive' => 1,
                'country'  => $country,
            ])
            ->asArray()
            ->one();
    }
}
