<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "{{%answerRateStat}}".
 *
 * @property integer $id
 * @property integer $initials
 * @property integer $answered
 * @property string $sentAt
 * @property integer $groupId
 * @property integer $schemeId
 * @property string $country
 * @property integer $restartNumber
 */
class AnswerRateStat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%answerRateStat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['initials', 'answered', 'groupId', 'schemeId', 'restartNumber'], 'integer'],
            [['sentAt'], 'safe'],
            [['country'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'initials' => 'Initials',
            'answered' => 'Answered',
            'sentAt' => 'Sent At',
            'groupId' => 'Group',
            'schemeId' => 'Scheme',
            'country' => 'Country',
            'restartNumber' => 'Restarted',
        ];
    }
}
