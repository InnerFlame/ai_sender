<?php

namespace app\models\phoenix;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ProfileMessagesSearch represents the model behind the search form about `app\models\ProfileMessage`.
 */
class ProfileMessagesSearch extends ProfileMessage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'isPaid', 'gender', 'sexuality', 'project', 'sendMode', 'isActive'], 'integer'],
            [['subject', 'body', 'language', 'country', ], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProfileMessage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'          => $this->id,
            'isPaid'      => $this->isPaid,
            'isActive'    => $this->isActive,
            'subject'     => $this->subject,
            'language'    => $this->language,
            'country'     => $this->country,
            'gender'      => $this->gender,
            'sexuality'   => $this->sexuality,
            'project'     => $this->project,
            'sendMode'    => $this->sendMode,
        ]);

        $query->andFilterWhere(['like', 'body', $this->body]);

        return $dataProvider;
    }
}
