<?php

namespace app\models\phoenix;

/**
 * This is the model class for table "{{%sendRuleToTraffSource}}".
 *
 * @property integer $id
 * @property integer $sendRuleId
 * @property string $traffSource
 *
 * @property SendRule $sendRule
 */
class SendRuleToTraffSource extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return '{{%sendRuleToTraffSource}}';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['sendRuleId'], 'integer'],
            [['traffSource'], 'string', 'max' => 30],
        ];
    }
}
