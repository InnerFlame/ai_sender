<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "phoenixSchema".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $isPaid
 * @property integer $gender
 * @property integer $sexuality
 * @property integer $ageFrom
 * @property integer $ageTo
 * @property string $createdAt
 * @property integer $timeToRestart
 * @property integer $restartCount
 * @property string $splitName
 * @property integer $splitGroup
 */
class Schema extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schema';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'ageFrom', 'ageTo'], 'required'],
            [['isPaid', 'gender', 'sexuality', 'ageFrom', 'ageTo', 'timeToRestart', 'restartCount', 'splitGroup'], 'integer'],
            [['createdAt'], 'safe'],
            [['title'], 'string', 'max' => 100],
            [['description', 'splitName'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'title'         => 'Title',
            'description'   => 'Description',
            'isPaid'        => 'Is Paid',
            'gender'        => 'Gender',
            'sexuality'     => 'Sexuality',
            'ageFrom'       => 'Age From',
            'ageTo'         => 'Age To',
            'createdAt'     => 'Created At',
            'timeToRestart' => 'Time To Restart',
            'restartCount'  => 'Restart Count',
            'splitName'     => 'Split Name',
            'splitGroup'    => 'Split Group',
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return string
     */
    public static function getTitleById(int $id): string
    {
        if (empty($id)) {
            return 'Undefined schema';
        }

        $model = self::findOne($id);

        if (!$model) {
            return 'Undefined schema';
        }

        return $model->title;
    }
}
