<?php

namespace app\models\phoenix;

/**
 * This is the model class for table "phoenixStepMessage".
 *
 * @property integer $id
 * @property string $message
 * @property string $type
 * @property integer $timestamp
 * @property string $lang
 */
class IncorrectMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'incorrectMessages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message', 'type', 'lang'], 'string'],
        ];
    }
}
