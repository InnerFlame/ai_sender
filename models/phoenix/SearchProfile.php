<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "phoenixSearchProfile".
 *
 * @property integer $id
 * @property integer $observerId
 * @property integer $countSearch
 * @property string $timeStartSearch
 * @property string $createdAt
 */
class SearchProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'searchProfile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['observerId'], 'required'],
            [['observerId', 'countSearch'], 'integer'],
            [['timeStartSearch', 'createdAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'observerId' => 'Profile ID',
            'countSearch' => 'Count Search',
            'timeStartSearch' => 'Time Start Search',
            'createdAt' => 'Created At',
        ];
    }
}