<?php

namespace app\models\phoenix;

use Yii;

/**
 * This is the model class for table "senderWithoutPhotoLog".
 *
 * @property integer $id
 * @property integer $senderId
 * @property string $createdAt
 */
class SenderWithoutPhotoLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'senderWithoutPhotoLog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['senderId'], 'required'],
            [['senderId'], 'integer'],
            [['createdAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'senderId' => 'Sender ID',
            'createdAt' => 'Created At',
        ];
    }

    public static function getLogs($request)
    {
        $logs = self::find();

        if (!empty($request['dateFrom'])) {
            $logs->andWhere('createdAt >= :dateFrom', [':dateFrom' => strtotime($request['dateFrom'] . ' 00:00:00')]);
        }

        if (!empty($request['dateTo'])) {
            $logs->andWhere('createdAt <= :dateTo', [':dateTo' => strtotime($request['dateTo'] . ' 23:59:59')]);
        }

        if (!empty($request['senderId'])) {
            $logs->andWhere(['senderId' => $request['senderId']]);
        }

        return $logs->orderBy('id desc');
    }

    public static function logData(SenderProfile $sender)
    {
        $self           = new self;
        $self->senderId = $sender->id;
        $self->save();
    }
}
