<?php

namespace app\models\phoenix;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;

/**
 * RecipientProfileSearch represents the model behind the search form about `app\models\RecipientProfile`.
 */
class RecipientProfileSearch extends RecipientProfile
{
    public $dateFrom;
    public $dateTo;
    public $isPrepaid;

    const UNDEFINED_GENERAL_NICHE = 'undefined';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'isPaid', 'sexuality', 'gender', 'site', 'isPrepaid'], 'integer'],
            [
                ['remoteId', 'activationTime', 'dateFrom', 'dateTo', 'country', 'niche', 'complained', 'searchable'],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = (new Query())->select('r.*')
            ->addSelect(new Expression('IF(pr.recipientId, 1, 0) as `isPrepaid`'))
            ->from(RecipientProfile::tableName() . ' as r')
            ->leftJoin(PrepaidRecipients::tableName() . ' as pr', 'r.id = pr.recipientId');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (isset($this->niche) && !empty($this->niche)) {
            if ($this->niche == self::UNDEFINED_GENERAL_NICHE) {
                $nichesCondition = ['niche' => null];
            } else {
                $nichesCondition = ['niche' => $this->niche];
            }
            $query->andWhere($nichesCondition);
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'r.id'      => $this->id,
            'isPaid'    => $this->isPaid,
            'sexuality' => $this->sexuality,
            'gender'    => $this->gender,
            'site'      => $this->site,
            'country'   => $this->country,
        ]);

        if ($this->complained != '' && !is_null($this->complained)) {
            $query->andWhere(['complained' => $this->complained]);
        }

        if ($this->searchable != '' && !is_null($this->searchable)) {
            $query->andWhere(['searchable' => $this->searchable]);
        }

        if (!empty($this->dateFrom)) {
            $query->andFilterWhere(['>=', 'activationTime', $this->dateFrom . ' 00:00:00']);
        }

        if (!empty($this->dateTo)) {
            $query->andFilterWhere(['<=', 'activationTime', $this->dateTo . ' 23:59:59']);
        }

        if (isset($this->isPrepaid)) {
            if ($this->isPrepaid == 0) {
                $query->andWhere('pr.recipientId IS NULL');
            } else {
                $query->andWhere('pr.recipientId IS NOT NULL');
            }
        }

        $query->andFilterWhere(['like', 'remoteId', $this->remoteId]);

        $query->orderBy('id desc');

        return $dataProvider;
    }
}
