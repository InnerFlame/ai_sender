<?php

namespace app\models\phoenix;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\phoenix\GrabRule;

/**
 * GrabRuleSearch represents the model behind the search form about `app\models\phoenix\GrabRule`.
 */
class GrabRuleSearch extends GrabRule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ageFrom', 'ageTo', 'isActive'], 'integer'],
            [['country', 'city', 'state', 'createdAt', 'site'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GrabRule::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'        => $this->id,
            'ageFrom'   => $this->ageFrom,
            'ageTo'     => $this->ageTo,
            'isActive'  => $this->isActive,
            'createdAt' => $this->createdAt,
            'site'      => $this->site,
        ]);

        $query->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state', $this->state]);

        return $dataProvider;
    }
}
