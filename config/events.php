<?php

require_once(__DIR__ . '/../components/rpc/PresenceEventsProcessorComponent.php');
require_once(__DIR__ . '/../components/phoenix/ContainerClientRpcComponent.php');
require_once(__DIR__ . '/../components/phoenix/RpcErrorComponent.php');

return [
    'eventDispatcher' => [
        'class' => 'sender\events\components\EventDispatcher',
    ],
];
