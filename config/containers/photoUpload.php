<?php

return function () {
    /** @var \app\components\rpc\RpcClientComponent $rpcClient */
    $rpcClient = \Yii::$app->rpcClient;
    /** @var \sender\interaction\components\InteractionClient $interactionClient */
    $interactionClient = \Yii::$container->get('interactionClient');
    return new \sender\registration\components\PhotoUpload($rpcClient, $interactionClient);
};
