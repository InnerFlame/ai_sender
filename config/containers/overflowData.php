<?php

return function ($containers, $params) {
    if (!empty($params['overflowDataList'])) {
        $overflowDataList = $params['overflowDataList'];
    } else {
        $overflowDataList = [
            'Communication',
            'IncomingMessage',
            'RecipientSchema',
            'Recipient',
        ];
    }

    $tableFactory = new \app\components\overflowData\TableFactory();
    $table        = new \app\components\overflowData\Table($tableFactory, $overflowDataList);

    $overflowFactory = new \app\components\overflowData\OverflowFactory();
    $overflow        = new \app\components\overflowData\Overflow($overflowFactory, $overflowDataList);

    return new \app\components\overflowData\OverflowData($table, $overflow);
};
