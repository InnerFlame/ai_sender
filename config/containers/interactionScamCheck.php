<?php

return function () {
    /** @var \app\components\amqp\InternalAmqpComponent $internalAmqp */
    $internalAmqp = \Yii::$app->internalAmqp;
    /** @var \sender\communication\components\containerManager\ContainerManager $containerManager */
    $containerManager = \Yii::$container->get('communicationContainerManager');

    return new \sender\interaction\components\InteractionScamCheck(
        $internalAmqp,
        $containerManager
    );
};
