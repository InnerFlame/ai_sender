<?php

return function () {
    $settings = \Yii::$app->yaml->parse('interaction/settings');
    return new \sender\interaction\components\InteractionRouter($settings);
};
