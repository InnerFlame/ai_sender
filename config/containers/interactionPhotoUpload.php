<?php

return function () {
    /** @var \app\components\amqp\InternalAmqpComponent $internalAmqp */
    $internalAmqp = \Yii::$app->internalAmqp;
    /** @var \app\components\phoenix\reUpload\PhotoReUploadScheduler $reuploadScheduler */
    $reuploadScheduler = \Yii::$container->get('photoReUploadScheduler');

    return new \sender\interaction\components\InteractionPhotoUpload(
        $internalAmqp,
        $reuploadScheduler
    );
};
