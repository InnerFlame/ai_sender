<?php

return function () {
    $cache = \Yii::$app->cache;

    return new \sender\split\components\SplitSystem(
        $cache
    );
};
