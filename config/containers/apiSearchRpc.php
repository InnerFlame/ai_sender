<?php

return function () {

    /** @var \app\components\common\IdEncoder $idEncoder */
    $idEncoder = \Yii::$container->get('idEncoder');

    /** @var \sender\monitoring\components\ZabbixMemcachedConnection $zabbixMemcachedConnection */
    $zabbixMemcachedConnection = \Yii::$container->get('zabbixMemcachedConnection');

    $recipientSaver   = new \app\components\phoenix\search\RecipientSaver($idEncoder);
    $recipientChecker = new \app\components\phoenix\search\RecipientChecker();

    return new \app\components\phoenix\search\ApiSearchRpc(
        $recipientSaver,
        $recipientChecker,
        $zabbixMemcachedConnection
    );
};
