<?php

return function () {
    $countries         = \Yii::$app->yaml->parse('country');
    $containersManager = \Yii::$container->get('communicationContainerManager');
    $nicheQuery        = \Yii::$container->get('nicheQuery');
    return new \sender\dashboard\components\Dashboard(
        $countries,
        $containersManager,
        $nicheQuery
    );
};
