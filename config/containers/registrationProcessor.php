<?php

return function () {
    /** @var \sender\interaction\components\InteractionClient $interactionClient */
    $interactionClient = \Yii::$container->get('interactionClient');
    /** @var \sender\registration\components\LocationCorrecter $locationCorrecter */
    $locationCorrecter     = \Yii::$container->get('locationCorrecter');

    return new \sender\registration\components\RegistrationProcessor($interactionClient, $locationCorrecter);
};
