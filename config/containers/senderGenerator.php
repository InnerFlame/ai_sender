<?php

return function () {
    /** @var \sender\registration\components\generate\EmailGenerator $emailGenerator */
    /** @var \sender\registration\components\generate\ScreenNameGenerator $screenName */

    $screenName          = \Yii::$container->get('screenName');
    $emailGenerator      = \Yii::$container->get('emailGenerator');
    $siteGenerator       = \Yii::$container->get('siteGenerator');
    $attributeCreator    = new \sender\registration\components\generate\AttributeCreator();
    $geoIp               = new \sender\registration\components\generate\GeoIp();
    /** @var \sender\registration\components\generate\Photo $photoGenerator */
    $photoGenerator      = $photoGenerator = \Yii::$container->get('photoGenerator');
    $birthday            = new \sender\registration\components\generate\BirthdayGenerator();
    $randomizer          = new \sender\registration\components\generate\Randomizer();
    $cityData            = new \sender\registration\components\generate\CityData();
    $registrationCreator = new \sender\registration\components\generate\RegistrationCreator();

    return new \sender\registration\components\generate\SenderGenerator(
        $screenName,
        $attributeCreator,
        $geoIp,
        $photoGenerator,
        $birthday,
        $randomizer,
        $cityData,
        $emailGenerator,
        $siteGenerator,
        $registrationCreator
    );
};
