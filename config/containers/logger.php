<?php

return function ($obj, $defaultContext) {

    $transport = new Gelf\Transport\UdpTransport(
        \app\config\Constant::LOGSTASH_HOST,
        \app\config\Constant::LOGSTASH_PORT
    );

    $publisher = new Gelf\Publisher();
    $publisher->addTransport($transport);
    $handler = new Monolog\Handler\GelfHandler($publisher);
    $logger = new \sender\logger\components\Log('sender', [$handler], [], $defaultContext);
    return $logger;
};
