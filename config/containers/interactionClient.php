<?php

return function () {
    return new \sender\interaction\components\InteractionClient(
        \app\config\Constant::INTERACTION_BASE_URL,
        \app\config\Constant::INTERACTION_USER_PWD,
        \Yii::$app->proxyManager,
        \Yii::$app->yaml
    );
};
