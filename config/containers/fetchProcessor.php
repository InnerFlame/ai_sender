<?php

return function () {
    $rpcMethods = \Yii::$app->rpcMethods;
    return new \sender\registration\components\fetch\FetchProcessor($rpcMethods);
};