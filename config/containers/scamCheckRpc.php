<?php

return function () {
    /** @var \app\components\rpc\RpcClientComponent $rpcClient */
    $rpcClient = \Yii::$app->rpcClient;
    /** @var \app\components\amqp\InternalAmqpComponent $internalAmqp */
    $internalAmqp = \Yii::$app->internalAmqp;
    /** @var \sender\communication\components\containerManager\ContainerManager $containerManager */
    $containerManager = \Yii::$container->get('communicationContainerManager');

    return new \sender\scamCheck\components\ScamCheckRpc(
        $internalAmqp,
        $rpcClient,
        $containerManager
    );
};
