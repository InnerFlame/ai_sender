<?php

return function () {
    /** @var \app\components\rpc\RpcClientComponent $rpcClient */
    $rpcClient = \Yii::$app->rpcClient;
    return new \sender\scamCheck\components\ScamCheck($rpcClient);
};
