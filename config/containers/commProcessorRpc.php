<?php

return function () {
    $internalAMQP = \Yii::$app->internalAmqp;
    /** @var \sender\communication\components\containerManager\ContainerManager $containersManager */
    $containersManager = \Yii::$container->get('communicationContainerManager');

    return new \sender\communication\components\commProcessor\CommProcessorRpc(
        $internalAMQP,
        $containersManager
    );
};
