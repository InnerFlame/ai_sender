<?php

use sender\communication\components\containerManager\ContainerManager;
use sender\registration\components\generate\SenderGenerator;
use sender\registration\components\RegistrationActualize;

return function () {
    /** @var SenderGenerator $senderGenerator */
    $senderGenerator = \Yii::$container->get('senderGenerator');
    /** @var ContainerManager $containersManager */
    $containersManager = \Yii::$container->get('communicationContainerManager');
    /** @var \sender\niche\components\NicheQuery $nicheQuery */
    $nicheQuery = \Yii::$container->get('nicheQuery');
    $registrationSettings = \Yii::$app->yaml->parse('registrationSettings');
    $defaultLanguages = \Yii::$app->yaml->parseByKey('language', 'defaultLanguage');
    return new RegistrationActualize(
        $senderGenerator,
        $containersManager,
        $nicheQuery,
        $registrationSettings,
        $defaultLanguages
    );
};
