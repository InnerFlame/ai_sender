<?php

return function() {
    $senderGenerator = \Yii::$container->get('senderGenerator');
    return new \sender\registration\components\RegistrationScheduler($senderGenerator);
};