<?php

return function () {
    /** @var \sender\niche\components\NicheQuery $nicheQuery */
    $nicheQuery = \Yii::$container->get('nicheQuery');
    /** @var \sender\logger\components\Log $logger */
    $logger = \Yii::$container->get('logger', \sender\logger\components\Log::LIFE_CYCLE_DEFAULT_CONTEXT);
    /** @var \sender\interaction\components\InteractionClient $interactionClient */
    $interactionClient = \Yii::$container->get('interactionClient');
    return new \sender\communication\components\containerManager\ContainerManager(
        new \sender\communication\components\containerManager\PoolUpdater($nicheQuery, $interactionClient, $logger),
        new \sender\communication\components\containerManager\ContainerStarter(),
        new \sender\communication\components\containerManager\InteractionStarter($interactionClient),
        $interactionClient,
        $logger
    );
};
