<?php

use app\models\rpc\Container;

return function () {
    /** @var \app\components\rpc\RpcClientComponent $rpcClient */
    $rpcClient = \Yii::$app->rpcClient;
    $setting = [
        'containersType'    => [
            Container::TYPE_REGISTRATION,
            Container::TYPE_RE_UPLOAD_PHOTO,
            Container::TYPE_OBSERVE,
            Container::TYPE_SEARCH,
            Container::TYPE_FETCH,
            '',
        ],
        'liveTime'          => [
            Container::TYPE_REGISTRATION    => 600,
            Container::TYPE_RE_UPLOAD_PHOTO => 180,
            'def'                           => 300,
        ],
        'status'            => [
            'def'                         => [Container::STATUS_STARTED, Container::STATUS_WAIT],
        ],
        'actionsBeforeStop' => [
            Container::TYPE_RE_UPLOAD_PHOTO =>
                new \sender\expiredContainer\components\actionsBeforeStop\ReUploadPhoto(),
            Container::TYPE_REGISTRATION    => new \sender\expiredContainer\components\actionsBeforeStop\Registration(),
        ]
    ];

    return new \sender\expiredContainer\components\ExpiredCloser($setting, $rpcClient);
};
