<?php

return function () {
    $notRightWords = \Yii::$app->yaml->parse('screenName/notRightWords');
    return new \sender\registration\components\generate\ScreenNameGenerator($notRightWords);
};