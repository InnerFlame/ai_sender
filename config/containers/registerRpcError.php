<?php

return function () {
    /** @var \app\components\rpc\RpcClientComponent $rpcClient */
    $rpcClient = \Yii::$app->rpcClient;
    /** @var \app\components\phoenix\reUpload\PhotoReUploadScheduler $reuploadScheduler */
    $reuploadScheduler = \Yii::$container->get('photoReUploadScheduler');
    $uploadedByAnotherUser
        = new \sender\registration\components\rpcErrorStrategy\uploadPhotoRpcErrorStrategy\UploadedByAnotherUser(
            $rpcClient,
            $reuploadScheduler
        );
    $connectionRefused
        = new \sender\registration\components\rpcErrorStrategy\uploadPhotoRpcErrorStrategy\ConnectionRefused(
            $rpcClient
        );
    $fetchScheduler = \Yii::$container->get('fetchScheduler');

    $registerErrorStrategy = new \sender\registration\components\rpcErrorStrategy\RegisterRpcErrorStrategy(
        $rpcClient,
        $fetchScheduler
    );
    $uploadPhotoErrorStrategy
        = new \sender\registration\components\rpcErrorStrategy\uploadPhotoRpcErrorStrategy\UploadPhotoRpcErrorStrategy(
            $uploadedByAnotherUser,
            $connectionRefused
        );

    $errorLogger = new \sender\registration\components\ErrorLogger();

    return new \sender\registration\components\rpcErrorStrategy\RegisterRpcError(
        $registerErrorStrategy,
        $uploadPhotoErrorStrategy,
        $errorLogger
    );
};
