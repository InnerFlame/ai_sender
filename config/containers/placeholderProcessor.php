<?php

return function () {


    $queue                     = 'processGettingPlaceholder';
    $evaSettings               = \Yii::$app->yaml->parseByKey('sendingPlaceholder', 'eva');
    $idEncoder                 = \Yii::$container->get('idEncoder');

    $listPlaceholderProcessors = [
        new \sender\communication\components\placeholder\PlaceholderProcessorEva($evaSettings, $queue),
    ];

    /** @var \sender\split\components\SplitSystem $splitSystem */
    $splitSystem = \Yii::$container->get('splitSystem');

    return new \sender\communication\components\placeholder\PlaceholderProcessor(
        $listPlaceholderProcessors,
        $queue,
        $splitSystem,
        $idEncoder
    );
};
