<?php

return function () {
    /** @var \sender\registration\components\generate\Photo $photoGenerator */
    $photoGenerator = \Yii::$container->get('photoGenerator');
    return new \app\components\phoenix\reUpload\PhotoReUploadScheduler($photoGenerator);
};
