<?php

return function () {
    /** @var \sender\monitoring\components\ZabbixMemcachedConnection $zabbixMemcachedConnection */
    $zabbixMemcachedConnection = \Yii::$container->get('zabbixMemcachedConnection');
    return new \sender\monitoring\components\ZabbixProxyError($zabbixMemcachedConnection);
};
