<?php

return function() {
    $scamWordsCacheKey   = \app\components\phoenix\checkMessage\AnalyzerData::SCAM_WORDS_FILE_DATA_CACHE_KEY;
    $cachedScamWordsData = Yii::$app->cache->get($scamWordsCacheKey);

    if (!$cachedScamWordsData) {
        if (!is_file(\Yii::$app->basePath . '/web/files/scam_words.csv')) {
            $scamWords = [];
        } else {
            $scamWords = array_map('str_getcsv', file(\Yii::$app->basePath . '/web/files/scam_words.csv'));

            $scamWordsCacheLifeTime = \app\components\phoenix\checkMessage\AnalyzerData::SCAM_WORDS_FILE_DATA_CACHE_LIFETIME;
            Yii::$app->cache->set($scamWordsCacheKey, $scamWords, $scamWordsCacheLifeTime);
        }
    } else {
        $scamWords = $cachedScamWordsData;
    }

    $analyzerDataConfig = \Yii::$app->yaml->parse('analyzerData');
    $unicodeEmojisCfg   = \Yii::$app->yaml->parse('unicodeEmojis');
    $analyzerData       = new \app\components\phoenix\checkMessage\AnalyzerData(
        $analyzerDataConfig['chars']['punctuation'],
        $analyzerDataConfig['chars']['special'],
        $analyzerDataConfig['chars']['other'],
        $analyzerDataConfig['stringDigits'],
        $analyzerDataConfig['emailPatterns'],
        $analyzerDataConfig['messengersPatterns'],
        $analyzerDataConfig['phoneWordsPatterns'],
        $analyzerDataConfig['forbiddenLetters'],
        $scamWords,
        $unicodeEmojisCfg['def']
    );

    $analyzerFactory = new \app\components\phoenix\checkMessage\AnalyzerFactory($analyzerData);
    return new \app\components\phoenix\checkMessage\SuspiciousWords($analyzerFactory);
};
