<?php

return function () {
    /** @var \app\components\rpc\RpcClientComponent $rpcClient */
    $rpcClient = \Yii::$app->rpcClient;
    /** @var \sender\registration\components\LocationCorrecter $locationCorrecter */
    $locationCorrecter     = \Yii::$container->get('locationCorrecter');
    $registeredStrategy    = new \sender\registration\components\rpcStrategy\RegisteredStrategy();
    $uploadedPhotoStrategy = new \sender\registration\components\rpcStrategy\UploadedPhotoStrategy(
        $rpcClient
    );

    return new \sender\registration\components\RegisterRpc(
        $locationCorrecter,
        $registeredStrategy,
        $uploadedPhotoStrategy
    );
};
