<?php

return function() {
    /** @var \sender\niche\components\NicheQuery $nicheQuery */
    $nicheQuery = \Yii::$container->get('nicheQuery');
    return new \sender\communication\components\containerManager\RemainderProfileChecker($nicheQuery);
};