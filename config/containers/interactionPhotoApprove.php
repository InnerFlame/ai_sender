<?php

return function () {
    /** @var \app\components\amqp\InternalAmqpComponent $internalAmqp */
    $internalAmqp = \Yii::$app->internalAmqp;
    /** @var \sender\communication\components\containerManager\ContainerManager $containerManager */
    $containerManager = \Yii::$container->get('communicationContainerManager');
    /** @var \sender\interaction\components\InteractionClient $interactionClient */
    $interactionClient = \Yii::$container->get('interactionClient');

    return new \sender\interaction\components\InteractionPhotoApprove(
        $internalAmqp,
        $containerManager,
        $interactionClient
    );
};
