<?php

return function () {
    /** @var \sender\niche\components\NicheQuery $nicheQuery */
    $nicheQuery           = \Yii::$container->get('nicheQuery');
    /** @var \sender\communication\components\containerManager\ContainerManager $containersManager */
    $containersManager    = \Yii::$container->get('communicationContainerManager');
    /** @var \sender\interaction\components\InteractionClient $interactionClient */
    $interactionClient = \Yii::$container->get('interactionClient');
    $errorLogger          = new \sender\communication\components\commProcessor\ErrorLogger();
    $commEvaProcessor     = new \sender\communication\components\commProcessor\CommEvaProcessor(
        $errorLogger,
        $containersManager,
        $interactionClient
    );
    $commInitialProcessor = new \sender\communication\components\commProcessor\CommInitialProcessor(
        $errorLogger,
        $containersManager,
        $nicheQuery,
        $interactionClient
    );
    $internalAMQP         = \Yii::$app->internalAmqp;
    $comScheduler         = \Yii::$app->commScheduler;
    $recipientChecker     = new \sender\communication\components\commProcessor\RecipientChecker($errorLogger);

    return new \sender\communication\components\commProcessor\CommProcessor(
        $commEvaProcessor,
        $commInitialProcessor,
        $internalAMQP,
        $comScheduler,
        $recipientChecker
    );
};
