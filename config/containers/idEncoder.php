<?php

return function () {
    /* @var $suspiciousWords \app\components\phoenix\checkMessage\SuspiciousWords */
    $suspiciousWords = \Yii::$container->get('suspiciousWords');

    return new \app\components\common\IdEncoder($suspiciousWords);
};
