<?php

return function () {
    return new \sender\monitoring\components\ZabbixMemcachedConnection(\Yii::$app->get('cache')->memcache);
};
