<?php

return function () {
    $rpcClient         = \Yii::$app->rpcClient;
    $locationCorrecter = \Yii::$container->get('locationCorrecter');

    return new \sender\registration\components\fetch\FetchRpc($rpcClient, $locationCorrecter);
};
