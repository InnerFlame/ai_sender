<?php

return function () {
    /** @var \sender\interaction\components\InteractionClient $interactionClient */
    $interactionClient = \Yii::$container->get('interactionClient');
    return new \sender\removeSenderProfile\components\SenderProfileRemoveProcessor($interactionClient);
};
