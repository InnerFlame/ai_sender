<?php
require_once(__DIR__ . '/Constant.php');

use app\config\Constant;

$transport = new Gelf\Transport\UdpTransport(
    \app\config\Constant::LOGSTASH_HOST,
    \app\config\Constant::LOGSTASH_PORT
);

$publisher = new Gelf\Publisher();
$publisher->addTransport($transport);
$handler = new Monolog\Handler\GelfHandler($publisher);
$logger = new \sender\logger\components\Log('sender', [$handler]);

return [
    'db'                             => [
        'class'    => 'yii\db\Connection',
        'dsn'      => Constant::DB_DSN,
        'username' => Constant::DB_USER,
        'password' => Constant::DB_PASS,
        'charset'  => 'utf8',
    ],
    'yaml'                           => [
        'class'        => 'app\components\common\YamlComponent',
        'pathOfConfig' => __DIR__ . '/yaml/'
    ],
    'authManager'                    => [
        'class'        => 'yii\rbac\PhpManager',
        'defaultRoles' => ['admin', 'superAdmin', 'contentManager']
    ],
    'cache'                          => [
        'class'        => 'yii\caching\MemCache',
        'useMemcached' => true,
        'servers'      => [
            [
                'host' => Constant::MEMCACHE_HOST,
                'port' => Constant::MEMCACHE_PORT,
            ],
        ],
        'keyPrefix'    => Constant::MEMCACHE_PREFIX,
    ],
    'senderPhoto'                    => [
        'class' => 'app\components\common\SenderPhotoComponent',
    ],
    'commScheduler'                  => [
        'class' => 'app\components\phoenix\communication\CommSchedulerComponent'
    ],
    'rpcClient'                      => [
        'class'                   => 'app\components\rpc\RpcClientComponent',
        'limitContainersRegister' => Constant::LIMIT_CONTAINERS_REGISTER,
        'limitContainersObserve'  => Constant::LIMIT_CONTAINERS_OBSERVE,
        'limitContainersSearch'   => Constant::LIMIT_CONTAINERS_SEARCH,
        'limitContainersComm'     => Constant::LIMIT_CONTAINERS_COMM,
        'limitContainersRemove'   => Constant::LIMIT_CONTAINERS_REMOVE,
        'enableLogs'              => Constant::ENABLE_RPC_LOGS,
    ],
    'internalAmqp'                   => [
        'class' => 'app\components\amqp\InternalAmqpComponent',
        'host'  => Constant::AMQP_HOST,
        'port'  => Constant::AMQP_PORT,
        'user'  => Constant::AMQP_USER,
        'pass'  => Constant::AMQP_PASS,
        'vhost' => Constant::AMQP_VHOST,
        'prefix' => Constant::AMQP_PREFIX,
    ],
    'controllersAmqp'                => [
        'class' => 'app\components\amqp\ControllersAmqpComponent',
        'host'  => Constant::AMQP_HOST,
        'port'  => Constant::AMQP_PORT,
        'user'  => Constant::AMQP_USER,
        'pass'  => Constant::AMQP_PASS,
        'vhost' => Constant::AMQP_VHOST,
    ],
    'presenceAmqp'                   => [
        'class' => 'app\components\amqp\PresenceAmqpComponent',
        'host'  => Constant::AMQP_HOST,
        'port'  => Constant::AMQP_PORT,
        'user'  => Constant::AMQP_USER,
        'pass'  => Constant::AMQP_PASS,
        'vhost' => Constant::AMQP_VHOST,
    ],
    'rpcMethods'                     => [
        'class' => 'app\components\rpc\RpcMethodsComponent',
    ],
    'rpcEventsProcessor'             => [
        'class' => 'app\components\rpc\RpcEventsProcessorComponent'
    ],
    'presenceEventsProcessor'        => [
        'class' => 'app\components\rpc\PresenceEventsProcessorComponent'
    ],
    'stepByStepScheduler'            => [
        'class' => 'app\components\phoenix\communication\StepByStepSchedulerComponent'
    ],
    'proxyManager'                   => [
        'class'    => 'app\components\proxy\ProxyManagerComponent',
        'luminati' => [
            'customer'    => Constant::LUMINATI_CUSTOMER,
            'zone'        => Constant::LUMINATI_ZONE,
            'zone_shared' => Constant::LUMINATI_ZONE_SHARED,
            'password'    => Constant::LUMINATI_PASS,
        ],
        'socks5'   => [
            'user' => Constant::SOCKS5_USER,
            'pass' => Constant::SOCKS5_PASS,
        ],
    ],
    'parseCsv'                       => [
        'class' => 'app\components\common\ParseCsvComponent'
    ],
    'redis'                          => [
        'class'    => 'yii\redis\Connection',
        'hostname' => Constant::REDIS_HOST,
        'port'     => Constant::REDIS_PORT,
        'database' => Constant::REDIS_DB,
    ],
    'urlManager'                     => [
        'baseUrl'         => Constant::BASE_URL,
        'enablePrettyUrl' => true,
        'showScriptName'  => false,
        'rules'           => [],
    ],
    'tool'                           => [
        'class' => 'app\components\phoenix\report\TesterToolComponent',
    ],
    'rpcError'                       => [
        'class'     => 'app\components\phoenix\RpcErrorComponent',
        'delayTime' => 180,
    ],
    'statCommunication'              => [
        'class' => 'app\components\phoenix\report\StatCommunicationComponent',

    ],
    'location'                       => [
        'class' => 'app\components\phoenix\LocationComponent',
    ],
    'apiSearch'                      => [
        'class' => 'app\components\phoenix\search\ApiSearchComponent',
    ],
    'searcher'                       => [
        'class' => 'app\components\phoenix\search\SearcherComponent',
    ],
    'searchRpc'                      => [
        'class' => 'app\components\phoenix\search\SearchRpcComponent',
    ],
    'reUploadScheduler'              => [
        'class' => 'app\components\phoenix\reUpload\PhotoReUploadScheduler',
    ],
    'reUploadProcessor'              => [
        'class' => 'app\components\phoenix\reUpload\PhotoReUploadProcessorComponent',
    ],
    'profileDeactivation'            => [
        'class' => 'app\components\phoenix\ProfileDeactivationComponent',
    ],
    'phoenixRpcEventsProcessor'      => [
        'class' => 'app\components\phoenix\RpcEventsProcessorComponent',
    ],
    'phoenixPresenceEventsProcessor' => [
        'class' => 'app\components\phoenix\PresenceEventsProcessorComponent',
    ],
    'log' => [
        'traceLevel' => 3,
        'flushInterval' => 1,
        'targets'    => [
            [
                'class' => \sender\logger\components\MonologTarget::class,
                'logger' => $logger,
                'levels' => ['info', 'error', 'warning'],
                'logVars' => [],
                'except' => ['yii\\db\\*', 'yii\\web\\*'],
                'microtime' => true,
                'exportInterval' => 1,

            ],
        ],
    ],
    'aggregatorCommunication'        => [
        'class' => 'app\components\phoenix\report\AggregatorCommunicationComponent',
    ],
    'unprocessedRecipientStats'      => [
        'class' => 'app\components\phoenix\report\UnprocessedRecipientStatsComponent',
    ],
    'delayStat'                      => [
        'class' => 'app\components\phoenix\report\DelayStatComponent',
        'smsEnable' => Constant::SMS_ENABLE,
    ],
    'containerClientRpc'             => [
        'class' => 'app\components\phoenix\ContainerClientRpcComponent',
    ],
    'recipientDataCleaner'            => [
        'class' => 'app\components\cleaner\RecipientDataCleanerComponent',
    ],
    'evaMessage'                     => [
        'class' => 'app\components\phoenix\communication\EvaMessageComponent',
    ],
    'containersSynchronization'      => [
        'class' => 'app\components\rpc\ContainersSynchronizationComponent'
    ],
    'messageCheck'                   => [
        'class' => 'app\components\phoenix\MessageCheckComponent',
    ],
    'textTransformation'             => [
        'class' => 'app\components\common\TextTransformationComponent',
    ],
    'rpcErrorLog'                    => [
        'class' => 'app\components\phoenix\report\RpcErrorLogComponent',
    ],
    'geoIp'                          => [
        'class' => 'app\components\common\GeoIpComponent'
    ],
    'checkMessage'                   => [
        'class' => 'app\components\phoenix\checkMessage\CheckMessageComponent',
    ],
    'dataFormatter'                  => [
        'class' => 'app\components\phoenix\report\DataFormatterComponent',
    ],
    'lifeCycle'                      => [
        'class' => 'app\components\phoenix\communication\LifeCycleComponent',
    ],
    'sms'                           => [
        'class' => 'app\components\common\SmsComponent',
        'wsdl'          => Constant::SMS_WSDL,
        'title'         => Constant::SMS_TITLE,
        'login'         => Constant::SMS_LOGIN,
        'password'      => Constant::SMS_PASSWORD,
        'recipients'    => Constant::SMS_RECIPIENTS,
    ],
    'senderBanStat'                  => [
        'class' => 'app\components\phoenix\report\SenderBanStatComponent',
    ],
    'message'                        => [
        'class' => 'app\components\phoenix\communication\MessageComponent',
    ],
    'userAgentManager' => [
        'class' => 'app\components\userAgent\UserAgentManagerComponent',
    ],
    'curl' => [
        'class' => \linslin\yii2\curl\Curl::class
    ],
    'registrationStat'               => [
        'class' => 'app\components\phoenix\report\RegistrationStatComponent',
    ],
    'answersRateStat'                => [
        'class' => 'app\components\phoenix\report\AnswersRateStatComponent',
    ],
    'resendingStat'                  => [
        'class' => 'app\components\phoenix\report\ResendingStatComponent',
    ],
];
