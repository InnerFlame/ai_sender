<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests/codeception');
$params     = require(__DIR__ . '/params.php');
$components = require(__DIR__ . '/components.php');
$container  = require(__DIR__ . '/container.php');
$events     = require(__DIR__ . '/events.php');

$config = [
    'id'                  => 'basic-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'app\commands',
    'components'          => $components + $events + [],
    'params'              => $params,
    'container'           => $container + [],
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
