<?php

namespace app\config;

class ConstantCommon
{
    const TARGET_PHOENIX = 'phoenix';
    const TARGET_PHOENIX_GO = 'phoenix_go';
    const TARGET_PHOENIX_WEB = 'webctrl';

}