<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\ExcludedSite */

$this->title = 'Update Excluded Site: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Excluded Sites', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="excluded-site-update">


    <?= $this->render('_form', [
        'model'   => $model,
        'site'    => $site,
        'country' => $country,
        'action' => 'update'
    ]) ?>

</div>
