<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\phoenix\ExcludedSiteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Excluded Sites';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="excluded-site-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Excluded Site', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'site',
                'filter' => Html::activeDropDownList($searchModel, 'site', $site, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('site', $model->site)['name'];
                }

            ],
            [
                'attribute' => 'country',
                'filter' => Html::activeDropDownList($searchModel, 'country', $country, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return (!empty($model->country)) ? Yii::$app->yaml->parseByKey('country', $model->country) : 'Any Countries';
                }

            ],
            [
                'attribute' => 'isActive',
                'filter' => Html::activeDropDownList($searchModel, 'isActive', ['No', 'Yes'], ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return $model->isActive == 1 ? 'Yes' : 'No';
                }

            ],
            'createdAt',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
