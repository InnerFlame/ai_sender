<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\ExcludedSite */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="excluded-site-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if ($action == 'update'): ?>

        <?= $form->field($model, 'site')->listBox($site, ['style' => 'height: 250px;']) ?>

        <?= $form->field($model, 'country')->listBox($country, ['style' => 'height: 250px;']) ?>

    <?php else: ?>

        <?= $form->field($model, 'site')->listBox($site, ['multiple' => 'multiple', 'style' => 'height: 250px;']) ?>

        <?= $form->field($model, 'country')->listBox($country, ['multiple' => 'multiple', 'style' => 'height: 250px;']) ?>

    <?php endif; ?>

    <?= $form->field($model, 'isActive')->dropDownList(['Disable', 'Enable']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
