<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\phoenix\ExcludedSite */

$this->title = 'Create Excluded Site';
$this->params['breadcrumbs'][] = ['label' => 'Excluded Sites', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="excluded-site-create">


    <?= $this->render('_form', [
        'model'   => $model,
        'site'    => $site,
        'country' => $country,
        'action' => 'create'
    ]) ?>

</div>
