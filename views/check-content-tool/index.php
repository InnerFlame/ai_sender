<?php

use yii\grid\GridView;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = 'Check content tool messages';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/js/checkMessage/checkMessage.js',
    ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset']]);
?>
<div class="presence-event-log-index">

    <p>
        <?= Html::a('Phrases', ['phrases'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Templates', ['templates'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Initials', ['initials'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'message',
            [
                'attribute' => 'message',
                'header' => 'Validation',
                'format' => 'raw',
                'class' => 'yii\grid\DataColumn',
                'value' => function ($row) {
                    $result = '';
                    $analyzersToExclude = (!empty($row['lang']) && $row['lang'] != 'en') ? ['DifferentEncoding'] : [];
                    foreach (\Yii::$app->checkMessage->checkMessageForTool($row['message'], $analyzersToExclude) as $checkResult) {
                        $result .= '<li>' . $checkResult['analyzer'] . ' <b style="white-space: nowrap">' . $checkResult['scamWord'] . '</b></li>';
                    }
                    return Html::tag('ul', $result, ['class' => 'dl-horizontal']);
                }
            ],
            'lang',
            'timestamp',
        ],
    ]);
    ?>
</div>
