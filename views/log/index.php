<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-index">


    <p>
        <?= Html::a('Clean Logs', ['clean'], ['class' => 'btn btn-danger']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => [],
        'columns' => [
            'id',
            'level',
            'category',
            [
                'attribute' => 'log_time',
                'filter' => \kartik\date\DatePicker::widget([
                    'name'          => 'dateFrom',
                    'value'         => $request['dateFrom'] ?? null,
                    'type'          => \kartik\date\DatePicker::TYPE_RANGE,
                    'name2'         => 'dateTo',
                    'value2'        => $request['dateTo'] ?? null,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'yyyy-mm-dd'
                    ]
                ]),
                'value' => function($model) {
                    return date('Y-m-d H:i:s', $model->log_time);
                }
            ],
            [
                'attribute' => 'message',
                'value' => function($model) {
                    return \yii\helpers\StringHelper::truncate($model->message, 150);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url , ['class' => 'view', 'data-pjax' => '0']);
                    },
                ],
            ]
        ],

    ]);

    $this->registerJs(
        "$(document).on('ready pjax:success', function() {
            $('.view').click(function(e){
               e.preventDefault();
               $('#pModal').modal('show')
                          .find('.modal-content')
                          .load($(this).attr('href'));
           });
        });
    ");

    yii\bootstrap\Modal::begin([
        'id'=>'pModal'
    ]);
    yii\bootstrap\Modal::end();
    ?>
</div>
