<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-index">


    <p>
        <?= Html::a('Clean Logs', ['clean-sender-without-photo'], ['class' => 'btn btn-danger']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => [],
        'columns' => [
            'id',
            [
                'attribute' => 'senderId',
                'filter' => Html::textInput('senderId', $request['senderId'] ?? null, ['class' => 'form-control']),
            ],
            [
                'attribute' => 'createdAt',
                'filter' => \kartik\date\DatePicker::widget([
                    'name'          => 'dateFrom',
                    'value'         => $request['dateFrom'] ?? null,
                    'type'          => \kartik\date\DatePicker::TYPE_RANGE,
                    'name2'         => 'dateTo',
                    'value2'        => $request['dateTo'] ?? null,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'yyyy-mm-dd'
                    ]
                ]),
            ],
        ],

    ]);
    ?>
</div>
