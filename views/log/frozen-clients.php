<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataProviderClient yii\data\ActiveDataProvider */

$this->title = 'Frozen Clients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-index">
    <h4>Count Frozen   - <?= $count ?></h4>

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Clients</th>
                <th>Date Frozen</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data as $item): ?>
                <tr>
                    <td><?php echo $item; ?></td>
                    <td><?php echo date('Y-m-d H:i:s', \Yii::$app->redis->zscore('frozenClients', $item)); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
