<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \sender\split\models\Split */
/* @var $sites array */
/* @var $genders array */
/* @var $sexualities array */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Splits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="split-view">
    
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]); ?>
    </p>

    <div class="col-md-3 text-center">
        <h2>
            <span class="label label-<?= $model->isActive ? 'success' : 'warning'; ?>">
                Active Status: <?= $model->isActive ? 'Active' : 'InActive'; ?>
            </span>
        </h2>
    </div>

    <div class="col-md-3 text-center">
        <h2>
            <span class="label label-primary">Started At: <?= $model->startedAt; ?></span>
        </h2>
    </div>

    <div class="col-md-3 text-center">
        <h2>
            <span class="label label-primary">Finished At: <?= $model->finishedAt; ?></span>
        </h2>
    </div>

    <div class="col-md-3 text-center">
        <h2>
            <span class="label label-default">Created At: <?= $model->createdAt; ?></span>
        </h2>
    </div>

    <br>
    <br>
    <br>
    <br>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            [
                'label' => 'Active Status',
                'value' => $model->isActive ? 'Active' : 'InActive'
            ],
            'percentage',

            [
                'label' => 'Countries',
                'value' => empty($model->country) ? 'Not set' : implode(', ', $model->country),
            ],
            [
                'label' => 'Genders',
                'value' => empty($model->gender) ? 'Not set' : implode(', ', $model->gender),
            ],
            [
                'label' => 'Sexualities',
                'value' => empty($model->sexuality) ? 'Not set' : implode(', ', $model->sexuality),
            ],
            [
                'label' => 'Register Platforms',
                'value' => empty($model->registrationPlatform) ? 'Not set' : implode(', ', $model->registrationPlatform),
            ],
            [
                'label' => 'Sites',
                'value' => empty($model->site) ? 'Not set' : implode(', ', $model->site),
            ],
            [
                'label' => 'Sources',
                'value' => empty($model->source) ? 'Not set' : implode(', ', $model->source),
            ],
            [
                'label' => 'Is paid',
                'value' => empty($model->isPaid) ? 'Not set' : implode(', ', $model->isPaid),
            ],
            'regDateFrom',
            'regDateTo'
        ],
    ]); ?>

</div>
