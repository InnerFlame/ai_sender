<?php

/* @var $this yii\web\View */
/* @var $splits array */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Split Export Page';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="split-index">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-5">
        <div class="form-group">
            <?= Html::dropDownList(
                'splitId',
                null,
                $splits,
                ['class' => 'form-control', 'prompt' => 'Choose split for export', 'required' => 'required']
            ); ?>
        </div>
        <div class="form-group">
            <?= Html::input(
                'number',
                'group',
                '',
                ['class' => 'form-control', 'placeholder' => 'Enter split group', 'required' => 'required']
            ); ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton(
                'Export &nbsp;&nbsp;<i class="fa fa-download"></i>',
                ['class' => 'btn btn-lg btn-success']
            ); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
