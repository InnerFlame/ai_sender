<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model \sender\split\models\Split */
/* @var $form yii\widgets\ActiveForm */
/* @var $countries array */
/* @var $sites array */
/* @var $registrationPlatforms array */
/* @var $genders array */
/* @var $sexualities array */
/* @var $sources array */

$this->registerCss("
.content {
    min-height: 1250px !important;
}
");
?>

<div class="split-form col-md-12">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]); ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]); ?>

        <?=  $form->field($model, 'startedAt')->widget(DateTimePicker::classname(), [
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd hh:ii:00'
            ]
        ]); ?>

        <?=  $form->field($model, 'finishedAt')->widget(DateTimePicker::classname(), [
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd hh:ii:00'
            ]
        ]); ?>

        <?= $form->field($model, 'isActive')->dropDownList([false => 'Inactive', true => 'Active']); ?>

        <?php if(!$model->isNewRecord): ?>
            <h2>
                <span class="label label-default">Created At: <?= $model->createdAt; ?></span>
            </h2>
        <?php endif; ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'percentage')->widget(MaskedInput::className(), [
            'mask' => '99,99,99,99,99,99,99,99,99,99',
        ]); ?>

        <?= $form->field($model, 'sexuality')->listBox($sexualities, ['multiple' => true]); ?>

        <?= $form->field($model, 'gender')->listBox($genders, ['multiple' => true]); ?>

        <?= $form->field($model, 'site')->listBox($sites, ['multiple' => true]); ?>

        <?= $form->field($model, 'country')->listBox($countries, ['multiple' => true]); ?>

        <?= $form->field($model, 'source')->listBox($sources, ['multiple' => true]); ?>

        <?= $form->field($model, 'registrationPlatform')->listBox($registrationPlatforms, ['multiple' => true]); ?>

        <?= $form->field($model, 'isPaid')->listBox([true => 'True', false => 'False'], ['multiple' => true]); ?>

        <?=  $form->field($model, 'regDateFrom')->widget(DateTimePicker::classname(), [
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd hh:ii:00'
            ]
        ]); ?>

        <?=  $form->field($model, 'regDateTo')->widget(DateTimePicker::classname(), [
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd hh:ii:00'
            ]
        ]); ?>

    </div>

    <div class="col-md-6">
        <div class="form-group">
            <?= Html::submitButton(
                    $model->isNewRecord
                        ? 'Create Setting &nbsp;&nbsp;<i class="fa fa-plus-circle"></i>'
                        : 'Update Setting &nbsp;&nbsp;<i class="fa fa-refresh"></i>',
                    ['class' => $model->isNewRecord ? 'btn btn-lg btn-success' : 'btn btn-lg btn-primary']
            ); ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
