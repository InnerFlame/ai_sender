<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel \sender\split\models\SplitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Splits';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="split-index">

    <p>
        <?= Html::a('Create Split Setting', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'id',
                'name',
                'description:ntext',
                [
                    'attribute' => 'isActive',
                    'format'    => 'raw',
                    'value'     => function($model)
                    {
                        /* @var $model \sender\split\models\Split */
                        return $model->isActive
                            ? Html::tag('p', 'Active', ['class' => 'text-success'])
                            : Html::tag('p', 'InActive', ['class' => 'text-danger']);
                    },
                    'filter' => Html::activeDropDownList(
                            $searchModel,
                            'isActive',
                            [false => 'InActive', true => 'Active'],
                            ['class' => 'form-control', 'prompt' => '']
                    )
                ],
                [
                    'attribute' => 'startedAt',
                    'filter'    => DatePicker::widget([
                        'model'         => $searchModel,
                        'attribute'     => 'startedAt',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format'    => 'yyyy-mm-dd'
                        ]
                    ]),
                ],
                [
                    'attribute' => 'finishedAt',
                    'filter'    => DatePicker::widget([
                        'model'         => $searchModel,
                        'attribute'     => 'finishedAt',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format'    => 'yyyy-mm-dd'
                        ]
                    ]),
                ],
                [
                    'attribute' => 'createdAt',
                    'filter'    => DatePicker::widget([
                        'model'         => $searchModel,
                        'attribute'     => 'createdAt',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format'    => 'yyyy-mm-dd'
                        ]
                    ]),
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
