<?php

/* @var $this yii\web\View */
/* @var $model \sender\split\models\Split */
/* @var $countries array */
/* @var $sites array */
/* @var $registrationPlatforms array */
/* @var $genders array */
/* @var $sexualities array */
/* @var $sources array */

$this->title = 'Update Split: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Splits', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="split-update">

    <?= $this->render('_form', [
        'model'                 => $model,
        'countries'             => $countries,
        'sites'                 => $sites,
        'registrationPlatforms' => $registrationPlatforms,
        'genders'               => $genders,
        'sexualities'           => $sexualities,
        'sources'               => $sources
    ]); ?>

</div>
