<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataProviderClient yii\data\ActiveDataProvider */

$this->title = 'Client';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-index">
    <h1>Client</h1>

    <h4>Client all     - <?= $startedCount + $loginCount ?></h4>
    <h4>Client started - <?= $startedCount ?></h4>
    <h4>Client login   - <?= $loginCount ?></h4>

    <?= Html::a('Clean Stopped', ['clean-stopped-client'], ['class' => 'btn btn-danger']) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => [],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
                'filter'    => Html::input('text', 'name', $request['name'] ?? null, ['class' => 'form-control']),
            ],
            [
                'attribute' => 'status',
                'filter'    => Html::dropDownList('status', $request['status'] ?? null, $statuses, ['class' => 'form-control', 'prompt' => '']),
            ],
            [
                'attribute' => 'type',
                'filter'    => Html::dropDownList('type', $request['type'] ?? null, $types, ['class' => 'form-control', 'prompt' => '']),
            ],
            [
                'attribute' => 'country',
                'filter'    => Html::dropDownList('country', $request['country'] ?? null, $country, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    if (!empty($model->country)) {
                        return Yii::$app->yaml->parseByKey('country', $model->country);
                    }

                    return null;
                }
            ],
            [
                'attribute' => 'site',
                'filter'    => Html::dropDownList('site', $request['site'] ?? null, $site, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    if (!empty($model->site)) {
                        return Yii::$app->yaml->parseByKey('site', $model->site)['name'];
                    }

                    return null;
                }
            ],
            [
                'attribute' => 'timeSetStatus',
                'value' => function ($model) {
                    return date('Y-m-d H:i:s', $model->timeSetStatus);
                }
            ],
            [
                'attribute' => 'refreshingTime',
                'value' => function ($model) {
                    return date('Y-m-d H:i:s', $model->refreshingTime);
                }
            ],
            'target',
        ],
    ]); ?>

</div>
