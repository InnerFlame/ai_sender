<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataProviderClient yii\data\ActiveDataProvider */

$this->title = 'Containers';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/js/containerClient/stopContainers.js',  ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset',]]);
?>
<div class="container-index">

    <h4>Containers started - <?php echo array_sum($containersCountForSections)?></h4>

    <div>
        <?php foreach($containersCountForSections as $key => $containerCount):?>
            <?php echo $key . ' - ' . $containerCount?>
        <?php endforeach;?>
    </div>


    <?= \diiimonn\widgets\SpinnerCanvas::widget([
        'options' => [
            'class' => 'spinner',
            'style' => 'display: none;',
        ],
        'scriptOptions' => [
            'radius' => 5,
            'height' => 5,
            'width' => 1.6,
            'dashes' => 10,
            'opacity' => 1,
            'padding' => 3,
            'rotation' => 700,
            'color' => '#666666',
        ]
    ]) ?>

    <a href="javascript:void(0);" id="stop-all" class="btn btn-danger">Stop All</a>
    <?= Html::a('Clean Stopped', ['clean-stopped-container'], ['class' => 'btn btn-danger']) ?>

    <div id="error-box" style="color: red;margin: 10px;"></div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => [],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
                'filter'    => Html::input('text', 'name', $request['name'] ?? null, ['class' => 'form-control']),

            ],
            [
                'attribute' => 'status',
                'filter'    => Html::dropDownList('status', $request['status'] ?? null, $statuses, ['class' => 'form-control', 'prompt' => '']),

            ],
            [
                'attribute' => 'type',
                'filter'    => Html::dropDownList('type', $request['type'] ?? null, $types, ['class' => 'form-control', 'prompt' => '']),

            ],
            [
                'attribute' => 'country',
                'filter'    => Html::dropDownList('country', $request['country'] ?? null, $country, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    if (!empty($model->country)) {
                        return Yii::$app->yaml->parseByKey('country', $model->country);
                    }

                    return $model->country;
                }

            ],
            [
                'attribute' => 'site',
                'filter'    => Html::dropDownList('site', $request['site'] ?? null, $site, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    if (!empty($model->site)) {
                        return Yii::$app->yaml->parseByKey('site', $model->site)['name'];
                    }

                    return null;
                }

            ],
            [
                'attribute' => 'timeSetStatus',
                'value' => function ($model) {
                    return date('Y-m-d H:i:s', $model->timeSetStatus);
                }
            ],
            [
                'attribute' => 'lastTimeActivity',
                'value' => function ($model) {
                    return date('Y-m-d H:i:s', $model->lastTimeActivity);
                }
            ],
            'target',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{stop}',
                'buttons' => [
                    'stop' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-remove-circle"></span>',
                            ['container-client/stop-one', 'id' => $model->id],
                            [
                                'title' => 'Stop/Delete Container',
                                'data-method' => 'post',
                            ]
                        );
                    },
                ],
            ],
        ],
    ]); ?>
</div>
