<?php

use yii\helpers\Html;

/** @var \yii\data\ArrayDataProvider $runningClients */
/** @var \yii\data\ArrayDataProvider $readyProfiles */
/** @var array $notUsedEmailsCount */
/** @var \yii\data\ArrayDataProvider $notUsedPhotosCount */
$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;

$formatNumber = function (int $value, int $minValue) {
    return Html::tag(
        'span',
        $value,
        (!empty($value) && empty($minValue)) || ($value < $minValue)
            ? ['class' => 'text-danger', 'style' => 'font-weight:bold']
            : []
    );
}
?>
<div class="row">
    <div class="col-lg-12">
        <h3>Running clients</h3>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $runningClients,
            'columns' => [
                'country',
                'limit',
                [
                    'header' => 'Runnig<br><small>(initial / answers)</small>',
                    'format' => 'raw',
                    'value' => function ($model) use ($formatNumber) {
                        $minValue = $model['limit'] / 2;
                        $running = $model['running'];
                        return
                            $formatNumber($running['initialAndAnswers'], $minValue)
                            . ' / ' . $running['onlyAnswers'];
                    }
                ],
                'limit',
            ],
        ])
        ?>
    </div>
    <div class="col-lg-12">
        <h3>Ready profiles</h3>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $readyProfiles,
            'columns' => [
                'country',
                [
                    'header' => 'Default Action Way',
                    'attribute' => 'actionWayDefaultCnt'
                ],
                [
                    'header' => 'External Action Way',
                    'attribute' => 'actionWayExternalCnt'
                ],
            ],
        ])
        ?>
    </div>
    <div class="col-lg-12">
        <h3>Not used e-mails: <?= $notUsedEmailsCount ?></h3>
    </div>
    <div class="col-lg-12">
        <h3>Not used photos</h3>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $notUsedPhotosCount,
            'columns' => [
                [
                    'header' => 'Birthday Year',
                    'attribute' => 'birthdayYear'
                ],
                [
                    'header' => 'Not Used Photos Count',
                    'attribute' => 'notUsedPhotosCount'
                ],
            ],
        ])
        ?>
    </div>

</div>
