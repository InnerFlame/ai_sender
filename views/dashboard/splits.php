<?php

use yii\grid\GridView;

/** @var \yii\data\ArrayDataProvider $splitsData */
/* @var array $sites */
/* @var array $schemes */
/* @var array $traffSources */
/* @var array $genders */
/* @var array $sexualities */

$this->title = 'Splits Dashboard';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
    <div class="col-lg-12">
        <h3>Active Splits</h3>
        <?= GridView::widget([
            'dataProvider' => $splitsData,
            'columns' => [
                'name',
                'percentage',
                [
                    'attribute' => 'country',
                    'value'     => function ($model) {
                        if (!$model['country']) {
                            return 'Not set';
                        }
                        return implode(', ', $model['country']);
                    }
                ],
                [
                    'attribute' => 'site',
                    'value'     => function ($model) use ($sites) {
                        if (!$model['site']) {
                            return 'Not set';
                        }
                        $siteNames = [];
                        foreach ($model['site'] as $id => $name) {
                            $siteNames[] = $sites[$name];
                        }

                        return implode(', ', $siteNames);
                    }
                ],
                [
                    'attribute' => 'source',
                    'value'     => function ($model) {
                        if (!$model['source']) {
                            return 'Not set';
                        }
                        return implode(', ', $model['source']);
                    }
                ],
                [
                    'attribute' => 'gender',
                    'value'     => function ($model) use ($genders) {
                        if (!$model['gender']) {
                            return 'Not set';
                        }
                        $genderNames = [];
                        foreach ($model['gender'] as $genderIndex) {
                            $genderNames[] = $genders[$genderIndex];
                        }

                        return implode(', ', $genderNames);
                    }
                ],
                [
                    'attribute' => 'sexuality',
                    'value'     => function ($model) use ($sexualities) {
                        if (!$model['sexuality']) {
                            return 'Not set';
                        }
                        $sexualityNames = [];
                        foreach ($model['sexuality'] as $sexualityIndex) {
                            $sexualityNames[] = $sexualities[$sexualityIndex];
                        }

                        return implode(', ', $sexualityNames);
                    }
                ],
                [
                    'attribute' => 'isPaid',
                    'value'     => function ($model) {
                        if (!$model['isPaid']) {
                            return 'Not set';
                        }
                        return implode(', ', $model['isPaid']);
                    }
                ],
                [
                    'attribute' => 'registrationPlatform',
                    'value'     => function ($model) {
                        if (!$model['registrationPlatform']) {
                            return 'Not set';
                        }
                        return implode(', ', $model['registrationPlatform']);
                    }
                ],
                'regDateFrom',
                'regDateTo',
                'startedAt',
                'finishedAt',
                [
                    'class'    => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                    'buttons'  => [
                        'view' => function ($url, $model, $index) {
                            return \yii\helpers\Html::a(
                                '<span class="glyphicon glyphicon-eye-open"></span>',
                                ['/split/view?id=' . $model['id']],
                                ['title' => 'View split', 'data-pjax' => '0', 'target' => '_blank']
                            );
                        },
                    ]
                ],
            ]
        ])
        ?>
    </div>
</div>


