<?php

use yii\helpers\Html;
use yii\grid\GridView;

/** @var \yii\data\ArrayDataProvider $recipientsNiches */
/** @var \yii\data\ArrayDataProvider $sendersNiches */


$this->title = 'Niches Dashboard';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-lg-12">
        <h3>Senders niches</h3>
        <?= GridView::widget([
            'dataProvider' => $sendersNiches,
            'columns' => [
                'niche',
                'countInSystem',
                'readyToWork',
                [
                    'attribute' => 'isLaunched',
                    'format'    => 'raw',
                    'value'     => function ($model) {
                        if ($model['isLaunched'] !== false) {
                            $launchedString = $model['isLaunched'];

                            return Html::tag('span', $launchedString, ['class' => 'text-success']);
                        }

                        return Html::tag('span', 'Not launched', ['class' => 'text-danger']);
                    }
                ]
            ],
        ]);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h3>Recipients niches</h3>
        <?= GridView::widget([
            'dataProvider' => $recipientsNiches,
            'columns' => [
                'niche',
                'countInSystem'
            ],
        ])
        ?>
    </div>
</div>


