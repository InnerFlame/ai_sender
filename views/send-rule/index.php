<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\phoenix\SendRuleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $site array */
/* @var $country array */
/* @var $projects array */
/* @var $traffSources array */
/* @var $schema array */

$this->title = 'Apply Schemas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="apply-schema-index">

    <p>
        <?= Html::a('Create Apply Schema', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'id',
            [
                'attribute' => 'schemaId',
                'filter'    => Html::activeDropDownList($searchModel, 'schemaId', $schema, ['class' => 'form-control', 'prompt' => '']),
                'value'     => function ($model) use ($schema) {
                    /**@var $model \app\models\phoenix\SendRule */
                    return $schema[$model->schemaId];
                }
            ],
            [
                'attribute' => 'site',
                'filter'    => Html::activeDropDownList($searchModel, 'site', $site, ['class' => 'form-control', 'prompt' => '']),
                'value'     => function ($model) use ($site) {
                    /**@var $model \app\models\phoenix\SendRule */
                    if (is_null($model->project)) {
                        return (!empty($model->site)) ? $site[$model->site] : 'Any Site';
                    }

                    return 'All sites from project';
                }

            ],
            [
                'attribute' => 'project',
                'filter' => Html::activeDropDownList($searchModel, 'project', $projects, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) use ($projects) {
                    /**@var $model \app\models\phoenix\SendRule */
                    if (is_null($model->project) && is_null($model->site)) {
                        return 'Any project';
                    }

                    if (is_null($model->project)) {
                        return 'Project not specified';
                    }

                    return $model->project;
                }
            ],
            [
                'attribute' => 'country',
                'filter' => Html::activeDropDownList($searchModel, 'country', $country, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) use ($country) {
                    /**@var $model \app\models\phoenix\SendRule */
                    return $country[$model->country];
                }

            ],
            [
                'attribute' => 'traffSource',
                'format' => 'raw',
                'value' => function ($model) use ($traffSources) {
                    /**@var $model \app\models\phoenix\SendRule */
                    return empty($model->traffSourcesArray) ? 'All sources' : implode(', <br>', $model->traffSourcesArray);
                }

            ],
            [
                'attribute' => 'isActive',
                'filter' => Html::activeDropDownList($searchModel, 'isActive', ['No', 'Yes'],
                    ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    /**@var $model \app\models\phoenix\SendRule */
                    return $model->isActive == 1 ? 'Yes' : 'No';
                }

            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>

</div>
