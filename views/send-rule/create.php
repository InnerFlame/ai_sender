<?php


/* @var $this yii\web\View */
/* @var $model app\models\phoenix\SendRule */
/* @var $site array */
/* @var $country array */
/* @var $projects array */
/* @var $traffSources array */
/* @var $schema array */


$this->title = 'Create Apply Schema';
$this->params['breadcrumbs'][] = ['label' => 'Apply Schemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="apply-schema-create">


    <?= $this->render('_form', [
        'model'        => $model,
        'site'         => $site,
        'country'      => $country,
        'schema'       => $schema,
        'projects'     => $projects,
        'traffSources' => $traffSources
    ]) ?>

</div>
