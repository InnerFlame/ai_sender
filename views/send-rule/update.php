<?php

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\SendRule */
/* @var $site array */
/* @var $country array */
/* @var $projects array */
/* @var $traffSources array */
/* @var $schema array */

$this->title = 'Update Apply Schema: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Apply Schemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="apply-schema-update">

    <?= $this->render('_form', [
        'model'        => $model,
        'site'         => $site,
        'country'      => $country,
        'schema'       => $schema,
        'projects'     => $projects,
        'traffSources' => $traffSources
    ]) ?>

</div>
