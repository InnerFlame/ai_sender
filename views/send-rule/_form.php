<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JqueryAsset;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\SendRule */
/* @var $form yii\widgets\ActiveForm */
/* @var $site array */
/* @var $country array */
/* @var $projects array */
/* @var $traffSources array */
/* @var $schema array */

$this->registerJsFile('/js/sendRules/form.js', ['depends' => [JqueryAsset::className()]]);
?>

<div class="apply-schema-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if (!$model->isNewRecord): ?>

        <?= $form->field($model, 'schemaId')->dropDownList($schema, ['class' => 'form-control', 'prompt' => '']) ?>

        <?= $form->field($model, 'site')->dropDownList($site, ['class' => 'form-control', 'prompt' => '']) ?>

        <?= $form->field($model, 'project')->dropDownList($projects, ['class' => 'form-control', 'prompt' => '']) ?>

        <?= $form->field($model, 'country')->dropDownList($country, ['class' => 'form-control', 'prompt' => '']) ?>

        <?= $form->field($model, 'traffSourcesArray')->listBox($traffSources, ['class' => 'form-control', 'multiple' => true]) ?>

    <?php else: ?>

        <?= $form->field($model, 'schemaId')->listBox($schema, ['multiple' => 'multiple']) ?>

        <?= $form->field($model, 'site')->listBox($site, ['multiple' => 'multiple', 'class' => 'form-control sitesBox']) ?>

        <?= $form->field($model, 'project')->listBox($projects, ['multiple' => 'multiple', 'class' => 'form-control projectsBox']) ?>

        <?= $form->field($model, 'country')->listBox($country, ['multiple' => 'multiple']) ?>

        <?= $form->field($model, 'traffSourcesArray')->listBox($traffSources, ['class' => 'form-control', 'multiple' => true]) ?>

    <?php endif; ?>

    <?= $form->field($model, 'isActive')->dropDownList(['Disable', 'Enable']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-lg btn-success' : 'btn btn-lg btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
