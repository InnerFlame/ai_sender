<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\Group */

$this->title = 'Update Group: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="group-update">


    <?= $this->render('_form', [
        'model'             => $model,
        'sendMode'          => $sendMode,
        'typesSendActivity' => $typesSendActivity,
    ]) ?>

</div>
