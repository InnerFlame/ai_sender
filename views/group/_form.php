<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\phoenix\Schema;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\Group */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="group-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'schemaId')->dropDownList(ArrayHelper::map(Schema::find()->all(), 'id', 'title'), ['prompt' => 'Select Schema']) ?>

    <?= $form->field($model, 'sendMode')->dropDownList($sendMode, ['prompt' => 'Select Message Type']) ?>

    <?= $form->field($model, 'messageCount')->textInput() ?>

    <?= $form->field($model, 'startTimeOffset')->textInput() ?>

    <?= $form->field($model, 'endTimeOffset')->textInput() ?>

    <?= $form->field($model, 'period')->textInput() ?>

    <?= $form->field($model, 'isInitial')->dropDownList(['No', 'Yes']) ?>

    <?= $form->field($model, 'typeSendActivity')->dropDownList($typesSendActivity) ?>

    <?= $form->field($model, 'percentWink')->textInput() ?>

    <?= $form->field($model, 'percentAddToFriend')->textInput() ?>

    <?= $form->field($model, 'percentBrowse')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
