<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\phoenix\Group */

$this->title = 'Create Group';
$this->params['breadcrumbs'][] = ['label' => 'Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-create">


    <?= $this->render('_form', [
        'model'             => $model,
        'sendMode'          => $sendMode,
        'typesSendActivity' => $typesSendActivity,
    ]) ?>

</div>
