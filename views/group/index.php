<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\phoenix\Schema;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\phoenix\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Groups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">


    <p>
        <?= Html::a('Create Group', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'attribute' => 'schemaId',
                'value' => 'schema.title',
                'filter' => ArrayHelper::map(Schema::find()->all(), 'id', 'title')
            ],
            [
                'attribute' => 'sendMode',
                'filter' => Html::activeDropDownList($searchModel, 'sendMode', $sendMode, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('sendMode', $model->sendMode);
                }

            ],
            'startTimeOffset',
            'endTimeOffset',
            'messageCount',
            'period',
            [
                'attribute' => 'isInitial',
                'value' => function ($model) {
                    return $model->isInitial == 1 ? 'Yes' : 'No';
                }
            ],
            'typeSendActivity',
            'percentWink',
            'percentAddToFriend',
            'percentBrowse',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>