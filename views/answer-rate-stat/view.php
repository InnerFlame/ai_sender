<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\AnswerRateStat */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Answer Rate Stats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="answer-rate-stat-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'initials',
            'answered',
            'sentAt',
            'groupId',
            'schemeId',
            'country',
            'restartNumber',
        ],
    ]) ?>

</div>
