<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\phoenix\AnswerRateStatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/**
 * @var $country array
 */
$this->title = 'Answer Rate Stats';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="answer-rate-stat-index">

<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'country',
                'filter' => Html::activeDropDownList($searchModel, 'country', $country, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    /** @var $model \app\models\phoenix\AnswerRateStat */
                    return Yii::$app->yaml->parseByKey('country', $model->country);
                }
            ],
            [
                'attribute' => 'initials',
                'format' => 'raw',
                'value' => function ($model) use ($searchModel) {
                    /** @var $model \app\models\phoenix\AnswerRateStat */
                    return $model->initials;
                }
            ],
            [
                'attribute' => 'answered',
                'format' => 'raw',
                'value' => function ($model) use ($searchModel) {
                    /** @var $model \app\models\phoenix\AnswerRateStat */
                    return $model->answered;
                }
            ],
            [
                'attribute' => 'groupId',
                'filter' => Html::activeDropDownList($searchModel, 'groupId', $searchModel->getGroupsFilterData(),
                    ['class' => 'form-control', 'prompt' => 'All']),
                'format' => 'raw',
                'value' => function ($model) use ($searchModel) {
                    /** @var $model \app\models\phoenix\AnswerRateStat */
                    return $searchModel->getGroupTitle($model->groupId);
                }
            ],
            [
                'attribute' => 'schemeId',
                'filter' => Html::activeDropDownList($searchModel, 'schemeId', $searchModel->getSchemesFilterData(),
                    ['class' => 'form-control', 'prompt' => 'All']),
                'format' => 'raw',
                'value' => function ($model) use ($searchModel) {
                    /** @var $model \app\models\phoenix\AnswerRateStat */
                    return $searchModel->getSchemeTitle($model->schemeId);
                }
            ],
            [
                'label' => 'Restarted number',
                'attribute' => 'restartNumber',
                'format' => 'raw',
                'value' => function ($model) use ($searchModel) {
                    /** @var $model \app\models\phoenix\AnswerRateStat */
                    return $model->restartNumber;
                }
            ],
            [
                'label' => 'Restarted status',
                'attribute' => 'restartedStatus',
                'filter' => Html::activeDropDownList($searchModel, 'restartedStatus',
                    [$searchModel::RESTART_STATUS_FALSE => 'Not restarted', $searchModel::RESTART_STATUS_TRUE => 'Restarted'],
                    ['class' => 'form-control', 'prompt' => 'All']),
                'format' => 'raw',
                'value' => function ($model) use ($searchModel){
                    /** @var $model \app\models\phoenix\AnswerRateStat */
                    if (!$model->restartNumber) {
                        return "<p class='text-warning'>Not restarted</p>";
                    }
                    return "<p class='text-success'>Restarted</p>";
                }
            ],
            [
                'label' => 'Date',
                'format' => 'raw',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'sentAtFrom',
                    'type' => \kartik\date\DatePicker::TYPE_RANGE,
                    'attribute2' => 'sentAtTo',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]),
                'value' => function ($model) {
                    /** @var $model \app\models\phoenix\AnswerRateStat */
                    return date('Y-m-d', strtotime($model->sentAt));
                }
            ],
            [
                'label' => 'Answers Rate',
                'filter' => false,
                'attribute' => 'answersRate',
                'format' => 'raw',
                'value' => function ($model) use ($searchModel) {
                    /** @var $model \app\models\phoenix\AnswerRateStat */
                    return number_format( ($model->answered/$model->initials) * 100, 2 ) . '%';
                }
            ]
        ]
    ]); ?>
<?php Pjax::end(); ?></div>
