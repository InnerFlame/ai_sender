<?php

use app\models\phoenix\SenderProfileAttributes;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\phoenix\SenderProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Senders Profile Attributes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-sender-index">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'email',
            [
                'attribute' => 'language',
                'filter' => Html::activeDropDownList($searchModel, 'language', $languages,
                    ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) use ($languages) {
                    return isset($model->language) ? $languages[$model->language] : null;
                }
            ],
            [
                'attribute' => 'age',
                'filter' => Html::activeInput('text', $searchModel, 'age', ['class' => 'form-control']),
                'value' => function ($model) {
                    return date_diff(new \DateTime(), date_create($model->birthday))->format('%y');
                }
            ],
            [
                'attribute' => 'senderProfileAttributes.photoLevel',
                'filter' => Html::activeDropDownList($searchModel, 'photoLevel', $attributes['photoLevel'],
                    ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) use ($attributes) {
                    return $attributes['photoLevel'][isset($model->senderProfileAttributes) ? $model->senderProfileAttributes->photoLevel : 0];
                }
            ],
            [
                'attribute' => 'senderProfileAttributes.height',
                'value' => function ($model) use ($attributes) {
                    return $attributes['height'][isset($model->senderProfileAttributes) ? $model->senderProfileAttributes->height : 0];
                }
            ],
            [
                'attribute' => 'senderProfileAttributes.weight',
                'filter' => Html::activeDropDownList($searchModel, 'weight', $attributes['weight'],
                    ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) use ($attributes) {
                    return $attributes['weight'][isset($model->senderProfileAttributes) ? $model->senderProfileAttributes->weight : 0];
                }
            ],
            [
                'attribute' => 'senderProfileAttributes.build',
                'value' => function ($model) use ($attributes) {
                    return $attributes['build'][isset($model->senderProfileAttributes) ? $model->senderProfileAttributes->build : 0];
                }
            ],
            [
                'attribute' => 'senderProfileAttributes.race',
                'filter' => Html::activeDropDownList($searchModel, 'race', $attributes['race'],
                    ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) use ($attributes) {
                    return $attributes['race'][isset($model->senderProfileAttributes) ? $model->senderProfileAttributes->race : 0];
                }
            ],
            [
                'attribute' => 'senderProfileAttributes.eyeColor',
                'value' => function ($model) use ($attributes) {
                    return $attributes['eyeColor'][isset($model->senderProfileAttributes) ? $model->senderProfileAttributes->eyeColor : 0];
                }
            ],
            [
                'attribute' => 'senderProfileAttributes.hairColor',
                'value' => function ($model) use ($attributes) {
                    return $attributes['hairColor'][isset($model->senderProfileAttributes) ? $model->senderProfileAttributes->hairColor : 0];
                }
            ],
            [
                'attribute' => 'senderProfileAttributes.familyStatus',
                'value' => function ($model) use ($attributes) {
                    return $attributes['familyStatus'][isset($model->senderProfileAttributes) ? $model->senderProfileAttributes->familyStatus : 0];
                }
            ],
            [
                'attribute' => 'senderProfileAttributes.glasses',
                'value' => function ($model) use ($attributes) {
                    return $attributes['glasses'][isset($model->senderProfileAttributes) ? $model->senderProfileAttributes->glasses : 0];
                }
            ],
            [
                'attribute' => 'senderProfileAttributes.religion',
                'value' => function ($model) use ($attributes) {
                    return $attributes['religion'][isset($model->senderProfileAttributes) ? $model->senderProfileAttributes->religion : 0];
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ]
        ],
    ]);

    $this->registerJs(
        "$(document).on('ready pjax:success', function() {
            $('.view').click(function(e){
               e.preventDefault();
               $('#pModal').modal('show')
                          .find('.modal-content')
                          .load($(this).attr('href'));
           });
        });
    ");

    yii\bootstrap\Modal::begin([
        'id' => 'pModal'
    ]);
    yii\bootstrap\Modal::end();

    ?>
</div>
