<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\SenderProfileAttributes */

$this->title = 'Update Sender Profile Attributes: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sender Profile Attributes', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sender-profile-attributes-update">


    <div class="schema-form">

        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'photoLevel')->dropDownList($attributes['photoLevel']) ?>
        <?= $form->field($model, 'height')->dropDownList($attributes['height']) ?>
        <?= $form->field($model, 'weight')->dropDownList($attributes['weight']) ?>
        <?= $form->field($model, 'build')->dropDownList($attributes['build']) ?>
        <?= $form->field($model, 'race')->dropDownList($attributes['race']) ?>
        <?= $form->field($model, 'eyeColor')->dropDownList($attributes['eyeColor']) ?>
        <?= $form->field($model, 'hairColor')->dropDownList($attributes['hairColor']) ?>
        <?= $form->field($model, 'familyStatus')->dropDownList($attributes['familyStatus']) ?>
        <?= $form->field($model, 'glasses')->dropDownList($attributes['glasses']) ?>
        <?= $form->field($model, 'religion')->dropDownList($attributes['religion']) ?>

        <div class="form-group">
            <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
