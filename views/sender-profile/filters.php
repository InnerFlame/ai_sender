<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="user-search" style="    width: 100%;
    float: left;">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div style="width: 25%;float: left;">
        <?= $form->field($model, 'readyToWork')->dropDownList(['No', 'Yes'], ['prompt' => ''])->label(null, ['style' => 'color:red'])?>
    </div>
    <div style="width: 25%;float: left;">
        <?= $form->field($model, 'id')?>
    </div>
    <div style="width: 25%;float: left;">
        <?= $form->field($model, 'originId')?>
    </div>
    <div style="width: 25%;float: left;">
        <?= $form->field($model, 'email')?>
    </div>
    <div style="width: 25%;float: left;">
        <?= $form->field($model, 'photoStatus')->dropDownList($photoStatus, ['prompt' => ''])?>
    </div>
    <div style="width: 25%;float: left;">
        <?= $form->field($model, 'site')->dropDownList($site, ['prompt' => '']) ?>
    </div>
    <div style="width: 25%;float: left;">
        <?= $form->field($model, 'country')->dropDownList($country, ['prompt' => '']) ?>
    </div>
    <div style="width: 25%;float: left;">
        <?= $form->field($model, 'language')->dropDownList($language, ['prompt' => '']) ?>
    </div>
    <div style="width: 25%;float: left;">
        <?= $form->field($model, 'userType')->dropDownList($userType, ['prompt' => '']) ?>
    </div>
    <div style="width: 25%;float: left;">
        <?= $form->field($model, 'actionWay')->dropDownList($actionWay, ['prompt' => '']) ?>
    </div>
    <div style="width: 25%;float: left;">
        <?= $form->field($model, 'status')->dropDownList($statuses, ['prompt' => '']) ?>
    </div>
    <div style="width: 25%;float: left;">
        <?= $form->field($model, 'isBanned')->dropDownList(['no' => 'No', 'yes' => 'Yes'], ['prompt' => '']) ?>
    </div>

    <div style="width: 25%;float: left;">
        <?= $form->field($model, 'niche')->dropDownList($niche, ['prompt' => '']) ?>
    </div>

    <div style="width: 25%;float: left;margin-bottom: 15px;">
        <?= Html::label('Banned At')?>
        <?= \kartik\date\DatePicker::widget([
            'model'         => $model,
            'attribute'     => 'bannedAtFrom',
            'attribute2'    => 'bannedAtTo',
            'type'          => \kartik\date\DatePicker::TYPE_RANGE,
            'pluginOptions' => [
                'format'    => 'yyyy-mm-dd',
                'autoclose' => true,
            ]
        ])?>
    </div>
    <div style="width: 25%;float: left;margin-bottom: 15px;">
        <?= Html::label('Time To Start Registration')?>
        <?= \kartik\date\DatePicker::widget([
            'model'         => $model,
            'attribute'     => 'timeToRegisterFrom',
            'attribute2'    => 'timeToRegisterTo',
            'type'          => \kartik\date\DatePicker::TYPE_RANGE,
            'pluginOptions' => [
                'format'    => 'yyyy-mm-dd',
                'autoclose' => true,
            ]
        ])?>
    </div>
    <div style="width: 25%;float: left;margin-bottom: 15px;">
        <?= Html::label('Registered At')?>
        <?= \kartik\date\DatePicker::widget([
            'model'         => $model,
            'attribute'     => 'registeredFrom',
            'attribute2'    => 'registeredTo',
            'type'          => \kartik\date\DatePicker::TYPE_RANGE,
            'pluginOptions' => [
                'format'    => 'yyyy-mm-dd',
                'autoclose' => true,
            ]
        ])?>
    </div>
    <div style="width: 25%;float: left;margin-bottom: 15px;">
        <?= Html::label('Removed At')?>
        <?= \kartik\date\DatePicker::widget([
            'model'         => $model,
            'attribute'     => 'removedAtFrom',
            'attribute2'    => 'removedAtTo',
            'type'          => \kartik\date\DatePicker::TYPE_RANGE,
            'pluginOptions' => [
                'format'    => 'yyyy-mm-dd',
                'autoclose' => true,
            ]
        ])?>
    </div>

    <div style="float: left;width: 100%;margin-bottom: 15px;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Reset', [''], ['class' => 'btn btn-info']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>