<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\SenderProfile */

$this->title = $model->firstName . ' ' . $model->lastName;
$this->params['breadcrumbs'][] = ['label' => 'Profile Senders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-sender-view">


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'firstName',
            'lastName',
            'screenName',
            'email',
            'photoStatus',
            'ip',
            'timeOffset',
            'actionWay',
            'birthday',
            [
                'attribute' => 'site',
                'value' => $site

            ],
            [
                'attribute' => 'country',
                'value' => $country

            ],
            'location',
            [
                'attribute' => 'sexuality',
                'value' => $sexuality

            ],
            [
                'attribute' => 'gender',
                'value' => $gender

            ],
            'internalDeviceIdentifier',
            'deviceIdHex',
            'gtmClientId',
            [
                'attribute' => 'userAgent',
                'value'     => function ($model) {
                    if (!empty($model->userAgent)) {
                        return \Yii::$app->userAgentManager->getUserAgentById($model->userAgent);
                    }
                    return null;
                },
            ],
            'version',
            'marker',
            'password',
            'language',
            'originId',
            'accessToken',
            'refreshToken',
            'bannedAt',
            'deactivatedAt',
            'pausedAt',
            'removedAt',
            'createdAt',
            [
                'attribute' => 'photo',
                'value'=> $imageUrl,
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
        ],
    ]) ?>

</div>
