<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\phoenix\SenderProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $senderProfileSearchParams string[] */

$this->title = 'Profile Senders';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="profile-sender-index">

    <div style="float: left;width: 100%;margin-bottom: 10px;">
        <h4>Photo status</h4>
        <div style="background: #DFF0D8;float: left;padding: 6px;">Approved</div>
        <div style="background: #F2DEDE;float: left;padding: 6px;">Not Approved</div>
        <div style="background: red;float: left;padding: 6px;">Is Banned</div>
        <div style="background: #F9F9F9;float: left;padding: 6px;">The observer has not photo status</div>
    </div>

    <a class="pull-right"
       href="<?= \yii\helpers\Url::to(['sender-profile/export', 'SenderProfileSearch' => $senderProfileSearchParams])?>"
    >Export to CSV</a>

    <?= $this->render('filters', [
        'model'       => $searchModel,
        'site'        => $site,
        'country'     => $country,
        'userType'    => $userTypes,
        'actionWay'   => $actionWay,
        'statuses'    => $statuses,
        'language'    => $language,
        'photoStatus' => $photoStatus,
        'niche'       => $niche,
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'screenName',
            'email',
            'originId',
            [
                'attribute' => 'site',
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('site', $model->site)['name'];
                }

            ],
            [
                'attribute' => 'country',
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('country', $model->country);
                }

            ],
            'userType',
            [
                'attribute' => 'actionWay',
                'value' => function ($model) {
                    return $model->actionWay;
                }
            ],
            'bannedAt',
            'senderProfileRegister.timeToRegister',
            'senderProfileRegister.registeredAt',
            'removedAt',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open" title="View More Info"></span>', $url , ['class' => 'view', 'data-pjax' => '0']);
                    },
                ],
            ],
        ],
        'rowOptions' => function ($model){
            if (strtotime($model->bannedAt) > 0) {
                return ['class' => 'scammer'];
            }
            if($model->userType != 'observer'){
                if ($model->photoStatus == 'approved') {
                    return ['class' => 'success'];
                } else {
                    return ['class' => 'danger'];
                }
            }

            return false;
        },
    ]);

    $this->registerJs(
        "$(document).on('ready pjax:success', function() {
            $('.view').click(function(e){
               e.preventDefault();
               $('#pModal').modal('show')
                          .find('.modal-content')
                          .load($(this).attr('href'));
           });
        });
    ");

    yii\bootstrap\Modal::begin([
        'id'=>'pModal'
    ]);
    yii\bootstrap\Modal::end();

    ?>
</div>
