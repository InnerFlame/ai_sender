<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\rpc\RpcLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rpc Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rpc-log-index">


    <p>
        <?= Html::a('Clean Logs', ['clean'], ['class' => 'btn btn-danger']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'clientName',
            [
                'attribute' => 'direction',
                'filter' => Html::activeDropDownList($searchModel, 'direction', ['incoming' => 'incoming', 'outgoing' => 'outgoing'], ['class' => 'form-control', 'prompt' => '']),

            ],
            'method',
            'createdAt',
            [
                'attribute' => 'params',
                'value' => function($model) {
                    return \yii\helpers\StringHelper::truncate($model->params, 150);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url , ['class' => 'view', 'data-pjax' => '0']);
                    },
                ],
            ],
        ],
        'rowOptions' => function ($model){

            if($model->direction == 'incoming'){
                return ['class' => 'success'];
            } else {
                return ['class' => 'danger'];
            }
        },
    ]);

    $this->registerJs(
        "$(document).on('ready pjax:success', function() {
            $('.view').click(function(e){
               e.preventDefault();
               $('#pModal').modal('show')
                          .find('.modal-content')
                          .load($(this).attr('href'));
           });
        });
    ");

    yii\bootstrap\Modal::begin([
        'id'=>'pModal'
    ]);
    yii\bootstrap\Modal::end();
    ?>
</div>
