<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \sender\registration\models\RulesRegistration */

$this->title = 'Create Rules Profile Sender Registration';
$this->params['breadcrumbs'][] = ['label' => 'Rules Profile Sender Registrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rules-profile-sender-registration-create">


    <?= $this->render('_form', [
        'model'     => $model,
        'country'   => $country,
        'sexuality' => $sexuality,
        'gender'    => $gender,
        'niche'     => $niche,
        'language'  => $language,
    ]) ?>

</div>
