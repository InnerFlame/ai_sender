<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use app\models\phoenix\SenderProfile;

/* @var $this yii\web\View */
/* @var $model \sender\registration\models\RulesRegistration */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rules-profile-sender-registration-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'niche')->dropDownList($niche, ['prompt' => 'Select Niche']) ?>

    <?= $form->field($model, 'country')->dropDownList($country, ['prompt' => 'Select Country']) ?>

    <?= $form->field($model, 'language')->dropDownList($language, ['prompt' => 'Select Language']) ?>

    <?= $form->field($model, 'sexuality')->dropDownList($sexuality, ['value' => 1, 'prompt' => 'Select Sexuality']) ?>

    <?= $form->field($model, 'gender')->dropDownList($gender, ['value' => 2, 'prompt' => 'Select Gender']) ?>

    <?= $form->field($model, 'ageFrom')->textInput() ?>

    <?= $form->field($model, 'ageTo')->textInput() ?>

    <?= $form->field($model, 'userType')->dropDownList([
        SenderProfile::TYPE_SENDER   => SenderProfile::TYPE_SENDER,
        SenderProfile::TYPE_OBSERVER => SenderProfile::TYPE_OBSERVER,
        SenderProfile::TYPE_SEARCHER => SenderProfile::TYPE_SEARCHER
    ]) ?>

    <?= $form->field($model, 'profilesCount')->textInput() ?>

    <?= $form->field($model, 'isActive')->dropDownList(['No', 'Yes'], ['value' => 1]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
