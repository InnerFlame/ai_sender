<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \sender\registration\models\RulesRegistrationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rules Profile Sender Registrations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rules-profile-sender-registration-index">

    <p>
        <?= Html::a('Create Rules Profile Sender Registration', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'niche',
                'filter'    => Html::activeDropDownList($searchModel, 'niche', $niche, ['class' => 'form-control', 'prompt' => '']),
                'value'     => function ($model) {
                    return $model->niche;
                }
            ],
            [
                'attribute' => 'country',
                'filter' => Html::activeDropDownList($searchModel, 'country', $country, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) use ($country) {
                    return $country[$model->country];
                }

            ],
            [
                'attribute' => 'language',
                'filter' => Html::activeDropDownList($searchModel, 'language', $language, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) use ($language) {
                    return $language[$model->language] ?? null;
                }

            ],
            'ageFrom',
            'ageTo',
            'userType',
            'profilesCount',
            'countCreated',
            'isActive',
            'hasDone',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
