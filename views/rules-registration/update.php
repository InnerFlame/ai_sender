<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \sender\registration\models\RulesRegistration */

$this->title = 'Update Rules Profile Sender Registration: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rules Profile Sender Registrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rules-profile-sender-registration-update">


    <?= $this->render('_form', [
        'model'     => $model,
        'country'   => $country,
        'sexuality' => $sexuality,
        'gender'    => $gender,
        'niche'     => $niche,
        'language'  => $language,
    ]) ?>

</div>
