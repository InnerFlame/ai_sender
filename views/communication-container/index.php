<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Communication Containers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="communication-container-index">


    <p>
        <?= Html::a('Create Communication Container', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'country',
                'filter' => Html::activeDropDownList($searchModel, 'country', $country, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('country', $model->country);
                }

            ],
            'countStarted',
            [
                'attribute' => 'isActive',
                'filter' => Html::activeDropDownList($searchModel, 'isActive', ['No', 'Yes'], ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return $model->isActive ? 'Yes' : 'No';
                }

            ],
            'createdAt',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
