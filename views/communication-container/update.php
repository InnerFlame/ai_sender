<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\CommunicationContainer */

$this->title = 'Update Communication Container: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Communication Containers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="communication-container-update">


    <?= $this->render('_form', [
        'model'   => $model,
        'country' => $country,
    ]) ?>

</div>
