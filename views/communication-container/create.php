<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\phoenix\CommunicationContainer */

$this->title = 'Create Communication Container';
$this->params['breadcrumbs'][] = ['label' => 'Communication Containers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="communication-container-create">


    <?= $this->render('_form', [
        'model'   => $model,
        'country' => $country,
    ]) ?>

</div>
