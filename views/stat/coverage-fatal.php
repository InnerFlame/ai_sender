<?php

use yii\helpers\Html;
use diiimonn\widgets\SpinnerCanvas;

/* @var $this yii\web\View */

$this->title = 'Coverage Fatal';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(
    '/js/lib/highcharts/highcharts.js',
    ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset']]
);
$this->registerJsFile(
    '/js/stat/coverageFatal.js',
    ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset']]
);
?>

<div class="container-index">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <div class="container-data">
        <div style="float: left; width: 15%;">
            <?php
            echo \kartik\date\DatePicker::widget([
                'name'          => 'dateFrom',
                'value'         => date('Y-m-d'),
                'type'          => \kartik\date\DatePicker::TYPE_RANGE,
                'name2'         => 'dateTo',
                'value2'        => date('Y-m-d'),
                'pluginOptions' => [
                    'autoclose' => true,
                    'format'    => 'yyyy-mm-dd'
                ]
            ]);
            ?>
        </div>

        <div style="float: left; width: 15%;">
            <?php echo Html::dropDownList(
                'country',
                null,
                $countries,
                ['prompt' => 'All Countries', 'class' => 'form-control', 'id' => 'country']
            )?>
        </div>

        <div style="float: left;">
            <?php echo Html::submitButton('Find', ['class' => 'btn btn-success', 'id' => 'find']) ?>
        </div>

        <div id="content-box"></div>

        <?= SpinnerCanvas::widget([
            'options' => [
                'class' => 'spinner',
                'style' => 'display: none;top: 120px;',
            ],
            'scriptOptions' => [
                'radius' => 5,
                'height' => 5,
                'width' => 1.6,
                'dashes' => 10,
                'opacity' => 1,
                'padding' => 3,
                'rotation' => 700,
                'color' => '#666666',
            ]
        ]) ?>
    </div>


</div>
