<?php

use yii\helpers\Html;
use diiimonn\widgets\SpinnerCanvas;

/* @var $this yii\web\View */

$this->title = 'Sender Ban Stat';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/js/lib/highcharts/highcharts.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset']]);
$this->registerJsFile('/js/stat/senderBanStat.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset']]);
?>

<div class="container-index">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <div class="container-data">

        <div style="float: left; width: 15%;">
            <?php echo Html::label('Date', 'date')?>
            <?php
            echo \kartik\date\DatePicker::widget([
                'name'          => 'dateFrom',
                'value'         => date('Y-m-d'),
                'type'          => \kartik\date\DatePicker::TYPE_RANGE,
                'name2'         => 'dateTo',
                'value2'        => date('Y-m-d'),
                'pluginOptions' => [
                    'autoclose' => true,
                    'format'    => 'yyyy-mm-dd'
                ]
            ]);
            ?>
        </div>

        <div style="float: left; width: 10%;margin-left: 5px;" class="group-box">
            <?php echo Html::label('Group', 'group')?>
            <?php echo Html::dropDownList('group', null, ['hour' => 'By Hours', 'minute' => 'By 15 Minutes'] , ['class' => 'form-control', 'id' => 'group'])?>
        </div>

        <div style="float: left; width: 10%;margin-left: 5px;" class="country-box">
            <?php echo Html::label('Country', 'country')?>
            <?php echo Html::dropDownList('country', null, $countries, ['class' => 'form-control', 'id' => 'country', 'prompt' => ''])?>
        </div>

        <div style="float: left;margin-left: 10px;margin-top: 25px;">
            <?php echo Html::submitButton('Find', ['class' => 'btn btn-success', 'id' => 'find']) ?>
        </div>

        <div id="content-box"></div>

        <?= SpinnerCanvas::widget([
            'options' => [
                'class' => 'spinner',
                'style' => 'display: none;top: 120px;',
            ],
            'scriptOptions' => [
                'radius' => 5,
                'height' => 5,
                'width' => 1.6,
                'dashes' => 10,
                'opacity' => 1,
                'padding' => 3,
                'rotation' => 700,
                'color' => '#666666',
            ]
        ]) ?>
    </div>
</div>
