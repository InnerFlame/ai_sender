<?php

use yii\helpers\Html;
use diiimonn\widgets\SpinnerCanvas;
use \kartik\date\DatePicker;
use \kartik\datetime\DateTimePicker;


/* @var $this yii\web\View */

$this->title = 'Stat Of Communication';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/js/lib/highcharts/highcharts.js',  ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset',]]);
$this->registerJsFile('/js/stat/statCommunication.js',  ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset',]]);
?>


<div class="container-index">
    <h1><?php echo Html::encode($this->title) ?></h1>

    <div class="container-data">
        <div style="width: 25%;float:left;">

            <div>
                <?php echo Html::label('Site', 'site')?>
                <?php echo Html::listBox('site', null, $sites, ['class' => 'form-control', 'multiple' => 'multiple', 'id' => 'site'])?>
            </div>

            <div>
                <?php echo Html::label('Country', 'country')?>
                <?php echo Html::listBox('country', null, $countries, ['class' => 'form-control', 'multiple' => 'multiple', 'id' => 'country'])?>
            </div>

            <div>
                <?php echo Html::label('Step', 'step')?>
                <?php echo Html::dropDownList('step', null, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], ['class' => 'form-control', 'id' => 'step', 'prompt' => 'All Steps'])?>
            </div>

            <div class="group-box">
                <?php echo Html::label('Group', 'group')?>
                <?php echo Html::dropDownList('group', null, ['hour' => 'By Hours', 'minute' => 'By 15 Minutes'] , ['class' => 'form-control', 'id' => 'group'])?>
            </div>

            <div>
                <?php echo Html::label('Date', 'date')?>

                <?php
                    echo DatePicker::widget([
                        'name'          => 'dateFrom',
                        'value'         => date('Y-m-d'),
                        'type'          => DatePicker::TYPE_RANGE,
                        'name2'         => 'dateTo',
                        'value2'        => date('Y-m-d'),
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format'    => 'yyyy-mm-dd'
                        ]
                    ]);
                ?>
            </div>



            <div>
                <?php echo Html::submitButton('Upload', ['class' => 'btn btn-success', 'id' => 'upload']) ?>
            </div>
        </div>

        <div id="content-box" style="width: 75%;float:left;margin-top: 25px;"></div>

        <?= SpinnerCanvas::widget([
            'options' => [
                'class' => 'spinner',
                'style' => 'display: none;top: 120px;',
            ],
            'scriptOptions' => [
                'radius' => 5,
                'height' => 5,
                'width' => 1.6,
                'dashes' => 10,
                'opacity' => 1,
                'padding' => 3,
                'rotation' => 700,
                'color' => '#666666',
            ]
        ]) ?>

    </div>


</div>
