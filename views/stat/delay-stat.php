<?php

use yii\helpers\Html;
use diiimonn\widgets\SpinnerCanvas;

/* @var $this yii\web\View */

$this->title = 'Delay Stat';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/js/lib/highcharts/highcharts.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset']]);
$this->registerJsFile('/js/stat/delayStat.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset']]);
?>

<div class="container-index">
    <h1><?php echo Html::encode($this->title) ?></h1>
    <div class="container-data">

        <div style="float: left; width: 15%;">
            <?php echo Html::label('Date', 'date')?>
            <?php
                echo \kartik\date\DatePicker::widget([
                    'name'          => 'dateFrom',
                    'value'         => date('Y-m-d'),
                    'type'          => \kartik\date\DatePicker::TYPE_RANGE,
                    'name2'         => 'dateTo',
                    'value2'        => date('Y-m-d'),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'yyyy-mm-dd'
                    ]
                ]);
            ?>
        </div>

        <div style="float: left; width: 10%;margin-left: 5px;" class="group-box">
            <?php echo Html::label('Group', 'group')?>
            <?php echo Html::dropDownList('group', null, ['hour' => 'By Hours', 'minute' => 'By 15 Minutes'] , ['class' => 'form-control', 'id' => 'group'])?>
        </div>

        <div style="float: left; width: 7%;margin-left: 5px;">
            <?php echo Html::label('Delay Time (sec.)', 'delay')?>
            <?php echo Html::dropDownList('delay', null, [30 => 30, 60 => 60, 90 => 90, 120 => 120, 150 => 150, 180 => 180], ['class' => 'form-control', 'id' => 'delay'])?>
        </div>

        <div style="float: left; width: 10%;margin-left: 5px;">
            <?php echo Html::label('Type Communication', 'type')?>
            <?php echo Html::dropDownList('type', null, ['initial' => 'Initial', 'eva' => 'Eva Logic'], ['class' => 'form-control', 'id' => 'type'])?>
        </div>

        <div id="step-box" style="float: left; width: 10%; display: none;margin-left: 5px;">
            <?php echo Html::label('Step', 'step')?>
            <?php echo Html::dropDownList('step', null, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], ['class' => 'form-control', 'id' => 'step', 'prompt' => 'All Steps'])?>
        </div>

        <div style="float: left;margin-left: 10px;margin-top: 25px;">
            <?php echo Html::submitButton('Find', ['class' => 'btn btn-success', 'id' => 'find']) ?>
        </div>

        <div id="content-box"></div>

        <?= SpinnerCanvas::widget([
            'options' => [
                'class' => 'spinner',
                'style' => 'display: none;top: 120px;',
            ],
            'scriptOptions' => [
                'radius' => 5,
                'height' => 5,
                'width' => 1.6,
                'dashes' => 10,
                'opacity' => 1,
                'padding' => 3,
                'rotation' => 700,
                'color' => '#666666',
            ]
        ]) ?>
    </div>


</div>
