<?php

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $cachedData
 * @var $alertingDisabledAt
 */

$this->title = 'Sms tools';


$cacheStatus =
    !$cachedData
    ? '<span class="text-success">Enabled</span>'
    : '<span class="text-danger">Disabled for '.$cachedData.' at '.date('Y-m-d H:i', $alertingDisabledAt).'</span>';
?>
<h2>Alerting Status: <?= $cacheStatus; ?></h2>


<div class="col-md-12">
    <?= Html::beginForm('', 'post', ['class' => 'form-inline']); ?>
        <p class="text text-info">Enter period in minutes (30m) or in hours (2h), max is 6h</p>
        <?= Html::input('text', 'period', '', ['class' => 'form-control']); ?>
        <?= Html::submitButton('Accept', ['class' => 'btn btn-success']); ?>
    <?= Html::endForm(); ?>
</div>

<?php if($cachedData): ?>
    <div class="col-md-2 text-center" style="margin-top: 15px;">
        <?= Html::beginForm('', 'post'); ?>
        <?= Html::hiddenInput('enableAlerting', '1'); ?>
        <?= Html::submitButton('Enable all SMS', ['class' => 'btn btn-success btn-lg']); ?>
        <?= Html::endForm(); ?>
    </div>
<?php endif; ?>
