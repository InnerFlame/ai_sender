<?php

use yii\helpers\Html;

$this->title = 'Clear Yaml Cache';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-index">


    <p>
        <?= Html::a('Clear Cache', ['', 'clear' => 1], ['class' => 'btn btn-danger']) ?>
    </p>

    <div class="col-lg-12">
        <?php if(Yii::$app->session->hasFlash('error')): ?>
            <div class="alert alert-danger" role="alert">
                <?= Yii::$app->session->getFlash('error') ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="col-lg-12">
        <?php if(Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success" role="alert">
                <?= Yii::$app->session->getFlash('success') ?>
            </div>
        <?php endif; ?>
    </div>

</div>
