<?php

use app\models\phoenix\SenderPhoto;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\phoenix\SenderPhotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sender Photos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sender-photo-index">


    <?php if (\Yii::$app->session->getFlash('reusedError')): ?>
        <div class="has-error">
            <div class="help-block"><?php echo \Yii::$app->session->getFlash('reusedError'); ?></div>
        </div>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'birthdayYear',
            [
                'attribute' => 'isUsed',
                'value'     => function($model) {
                    return $model->isUsed ? 'Yes' : 'No';
                }
            ],
            [
                'contentOptions' => [
                    'style' => 'max-width: 150px;'
                ],
                'format'    => 'html',
                'value'     => function($model) {
                    return Html::img(SenderPhoto::PHOTO_BASE_PATH . $model->name, ['class' => 'sender-photo']);
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{active}',
                'buttons' => [
                    'active' => function ($url, $model) {
                        if ($model->isUsed) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-ok-circle"></span>',
                                ['sender-photo/reused-photo', 'id' => $model->id],
                                [
                                    'title' => 'Active Rule',
                                    'aria-label' => 'Active Rule',
                                    'data-pjax' => 0,
                                    'data-method' => 'post',
                                    'data-confirm' => 'Are you sure you want to reused this photo?'
                                ]
                            );
                        }

                        return null;
                    },
                ],
            ],
        ],
        'rowOptions' => function ($model){
            if($model->isBad){
                return ['class' => 'danger'];
            }

            return false;
        },
    ]); ?>
</div>
