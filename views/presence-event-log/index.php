<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\models\phoenix\PresenceEventLogSearch */

$this->title = 'Presence Event Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="presence-event-log-index">


    <p>
        <?= Html::a('Clean Logs', ['clean'], ['class' => 'btn btn-danger']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'clientName',
            'action',
            'createdAt'
        ],
    ]); ?>
</div>
