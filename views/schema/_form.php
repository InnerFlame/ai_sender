<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Site;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\Schema */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="schema-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isPaid')->dropDownList([ 1 => 'Yes', 0 => 'No', ], ['prompt' => 'Select Paid']) ?>

    <?= $form->field($model, 'gender')->dropDownList($gender, ['prompt' => 'Select Gender']) ?>

    <?= $form->field($model, 'sexuality')->dropDownList($sexuality, ['prompt' => 'Select Sexuality']) ?>

    <?= $form->field($model, 'timeToRestart')->textInput() ?>

    <?= $form->field($model, 'restartCount')->textInput() ?>

    <?= $form->field($model, 'ageFrom')->textInput() ?>

    <?= $form->field($model, 'ageTo')->textInput() ?>

    <?= $form->field($model, 'splitName')->textInput() ?>

    <?= $form->field($model, 'splitGroup')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
