<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\phoenix\Schema */

$this->title = 'Create Schema';
$this->params['breadcrumbs'][] = ['label' => 'Schemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schema-create">


    <?= $this->render('_form', [
        'model'     => $model,
        'gender'    => $gender,
        'sexuality' => $sexuality,
    ]) ?>

</div>
