<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Site;


/* @var $this yii\web\View */
/* @var $searchModel app\models\phoenix\SchemaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Schemas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schema-index">


    <p>
        <?= Html::a('Create Schema', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            'description',
            [
                'attribute' => 'isPaid',
                'filter' => Html::activeDropDownList($searchModel, 'isPaid', ['No', 'Yes'], ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return $model->isPaid == 1 ? 'Yes' : 'No';
                }
            ],
            [
                'attribute' => 'gender',
                'filter' => Html::activeDropDownList($searchModel, 'gender', $gender, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('gender', $model->gender);
                }
            ],
            [
                'attribute' => 'sexuality',
                'filter' => Html::activeDropDownList($searchModel, 'sexuality', $sexuality, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('sexuality', $model->sexuality);
                }
            ],
            'ageFrom',
            'ageTo',
            'timeToRestart',
            'restartCount',
            'splitName',
            'splitGroup',
            'createdAt',

            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>
</div>
