<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\Schema */

$this->title = 'Update Schema: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Schemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="schema-update">


    <?= $this->render('_form', [
        'model'     => $model,
        'gender'    => $gender,
        'sexuality' => $sexuality,
    ]) ?>

</div>
