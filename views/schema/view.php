<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\Schema */
/* @var $searchModel app\models\phoenix\SchemaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Schemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schema-view">


    <p>
        <?= Html::a('Add Group', ['group/create'], ['class' => 'btn btn-success', 'target' => '_blank']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => array_merge([
            'id',
            'title',
            'description',
            [
                'attribute' => 'isPaid',
                'value'     => $isPaid
            ],
            [
                'attribute' => 'gender',
                'value'     => $gender
            ],
            [
                'attribute' => 'sexuality',
                'value'     => $sexuality
            ],
            'ageFrom',
            'ageTo',
            'timeToRestart',
            'restartCount',
            'splitName',
            'splitGroup',
            'createdAt'
            ]
        )
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'attribute' => 'sendMode',
                'filter'    => Html::activeDropDownList($searchModel, 'sendMode', $sendMode, ['class' => 'form-control', 'prompt' => '']),
                'value'     => function ($model) {
                    return Yii::$app->yaml->parseByKey('sendMode', $model->sendMode);
                }

            ],
            'startTimeOffset',
            'endTimeOffset',
            'messageCount',
            'period',
        ],
    ]); ?>


</div>
