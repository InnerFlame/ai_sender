<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\common\User */

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'Аккаунты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">


    <?= $this->render('_form', [
        'model' => $model,
        'action' => 'create'
    ]) ?>

</div>
