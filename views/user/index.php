<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\common\User;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">


    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'email:email',
            [
                'attribute' => 'role',
                'value' => function($model) {
                    switch ($model->role) {
                        case User::SUPER_ADMIN_KEY:
                            return User::SUPER_ADMIN;
                            break;
                        case User::ADMIN_KEY:
                            return User::ADMIN;
                            break;
                        case User::CONTENT_MANAGER_KEY:
                            return User::CONTENT_MANAGER;
                            break;
                    }
                }
            ],
            'createdAt',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>
</div>
