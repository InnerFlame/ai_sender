<?php

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\SendExclusionsRules */

/**
 * @var $countries array
 * @var $sites array
 * @var $sources
 */

$this->title = 'Create Exclusion Rule';
$this->params['breadcrumbs'][] = ['label' => 'Send Exclusions Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="send-exclusions-rules-create">

    <?= $this->render('_form', [
        'model'     => $model,
        'countries' => $countries,
        'sites'     => $sites,
        'sources'   => $sources,
        'schemaIds' => $schemaIds,
        'platforms' => $platforms,
    ]) ?>

</div>
