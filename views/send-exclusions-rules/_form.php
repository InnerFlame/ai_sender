<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\SendExclusionsRules */
/* @var $form yii\widgets\ActiveForm */

/**
 * @var $countries array
 * @var $sites array
 * @var $sources
 */
?>

<div class="send-exclusions-rules-form col-md-12">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'countriesArray')->listBox($countries, ['multiple' => true, 'size' => 5]) ?>

        <?= $form->field($model, 'sitesIdsArray')->listBox($sites, ['multiple' => true, 'size' => 10]) ?>

        <?= $form->field($model, 'sourcesArray')->listBox($sources, ['multiple' => true]) ?>

        <?= $form->field($model, 'schemaIdsArray')->listBox($schemaIds, ['multiple' => true]) ?>

        <?= $form->field($model, 'platformsArray')->listBox($platforms, ['multiple' => true]) ?>

        <?= $form->field($model, 'regDateFrom')->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format'    => 'yyyy-mm-dd'
            ]
        ]); ?>

        <?= $form->field($model, 'regDateTo')->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format'    => 'yyyy-mm-dd'
            ]
        ]); ?>
    </div>

    <div class="col-md-6 text-center">
        <?= $form->field($model, 'isActive')->dropDownList([0 => 'Not Active', 1 => 'Active']) ?>

        <?php if(!$model->isNewRecord): ?>
            <h3>
                <span class="label label-default">Updated At: <?= $model->updatedAt; ?></span>
                <span class="label label-default">Created At: <?= $model->createdAt; ?></span>
            </h3>
        <?php endif; ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success btn-lg' : 'btn btn-primary btn-lg']) ?>
        </div>
    </div>













    <?php ActiveForm::end(); ?>

</div>
