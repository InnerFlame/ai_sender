<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\phoenix\SendExclusionsRulesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/**
 * @var $countries array
 * @var $sites array
 */

$this->title = 'Send Exclusions Rules';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="send-exclusions-rules-index">

    <p>
        <?= Html::a('Create Exclusion Rule', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            [
                'attribute' => 'countries',
                'contentOptions' => ['style'=>'max-width: 250px;'],
                'format' => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'countries', $countries,
                    ['prompt' => '', 'class' => 'form-control']),
                'value' => function ($model) {
                    /** @var $model \app\models\phoenix\SendExclusionsRules */
                    if (empty($model->countriesArray)) {
                        return Html::tag('p', 'Not Set', ['class' => 'text-danger']);
                    }

                    return implode(', ', $model->countriesArray);
                },
            ],
            [
                'attribute' => 'sitesIds',
                'contentOptions' => ['style'=>'max-width: 250px;'],
                'format' => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'sitesIds', $sites,
                    ['prompt' => '', 'class' => 'form-control']),
                'label' => 'Sites',
                'value' => function ($model) use ($sites) {
                    /** @var $model \app\models\phoenix\SendExclusionsRules */
                    if (empty($model->sitesIdsArray)) {
                        return Html::tag('p', 'Not Set', ['class' => 'text-danger']);
                    }

                    $out = [];
                    foreach ($model->sitesIdsArray as $site) {
                        $out[] = $sites[$site];
                    }

                    return implode(', ', $out);
                },
            ],
            [
                'attribute' => 'sources',
                'contentOptions' => ['style'=>'max-width: 250px;'],
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var $model \app\models\phoenix\SendExclusionsRules */
                    if (empty($model->sourcesArray)) {
                        return Html::tag('p', 'Not Set', ['class' => 'text-danger']);
                    }

                    return implode(', ', $model->sourcesArray);
                },
            ],
            [
                'attribute' => 'schemaIds',
                'contentOptions' => ['style'=>'max-width: 250px;'],
                'format' => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'schemaIds', $schemaIds,
                    ['prompt' => '', 'class' => 'form-control']),
                'value' => function ($model) use ($schemaIds) {
                    /** @var $model \app\models\phoenix\SendExclusionsRules */
                    if (empty($model->schemaIdsArray)) {
                        return Html::tag('p', 'Not Set', ['class' => 'text-danger']);
                    }

                    $out = [];
                    foreach ($model->schemaIdsArray as $schema) {
                        $out[] = $schemaIds[$schema];
                    }

                    return implode(', ', $out);
                },
            ],
            [
                'attribute' => 'platforms',
                'contentOptions' => ['style'=>'max-width: 250px;'],
                'format' => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'platforms', $platforms,
                    ['prompt' => '', 'class' => 'form-control']),
                'value' => function ($model) {
                    /** @var $model \app\models\phoenix\SendExclusionsRules */
                    if (empty($model->platformsArray)) {
                        return Html::tag('p', 'Not Set', ['class' => 'text-danger']);
                    }

                    return implode(', ', $model->platformsArray);
                },
            ],
            [
                'attribute' => 'isActive',
                'format' => 'raw',
                'label' => 'Active status',
                'filter' => Html::activeDropDownList($searchModel, 'isActive', [0 => 'Not Active', 1 => 'Active'],
                    ['prompt' => '', 'class' => 'form-control']),
                'value' => function ($model) {
                    /** @var $model \app\models\phoenix\SendExclusionsRules */
                    if ($model->isActive) {
                       return Html::tag('p', 'Active', ['class' => 'text-success']);
                    }
                    return Html::tag('p', 'Not Active', ['class' => 'text-danger']);
                },
            ],
            [
                'attribute' => 'regDateFrom',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'regDateFrom',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'attribute' => 'regDateTo',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'regDateTo',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            /* TODO uncomment this if u need filters on created or updated date
            [
                'attribute' => 'createdAt',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'createdAt',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'attribute' => 'updatedAt',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'updatedAt',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            */

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
