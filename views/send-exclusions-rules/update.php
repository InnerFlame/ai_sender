<?php

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\SendExclusionsRules */

/**
 * @var $countries array
 * @var $sites array
 * @var $sources
 */
$this->title = 'Update Send Exclusions Rules: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Send Exclusions Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="send-exclusions-rules-update">

    <?= $this->render('_form', [
        'model'     => $model,
        'countries' => $countries,
        'sites'     => $sites,
        'sources'   => $sources,
        'schemaIds' => $schemaIds,
        'platforms' => $platforms,
    ]) ?>

</div>
