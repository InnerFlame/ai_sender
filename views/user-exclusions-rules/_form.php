<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\UserExclusionsRules */
/* @var $form yii\widgets\ActiveForm */

/**
 * @var array $genders
 * @var array $searchable
 * @var array $isPaid
 * @var array $registrationPlatforms
 * @var array $excludedSources
 * @var array $orientations
 */
?>

<div class="user-exclusions-rules-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'gendersArray')->listBox($genders, ['multiple' => true, 'required' => true]) ?>

    <?= $form->field($model, 'orientationArray')->listBox($orientations, ['multiple' => true, 'required' => true]) ?>

    <?= $form->field($model, 'isPaidArray')->listBox($isPaid, ['multiple' => true, 'required' => true]) ?>

    <?= $form->field($model, 'searchableArray')->listBox($searchable, ['multiple' => true, 'required' => true]) ?>

    <?= $form->field($model, 'registrationPlatformsArray')->listBox($registrationPlatforms, ['multiple' => true, 'required' => true]) ?>

    <?= $form->field($model, 'excludedSourcesArray')->listBox($excludedSources, ['multiple' => true, 'required' => true]) ?>

    <?= $form->field($model, 'isActive')->dropDownList([0 => 'Not Active', 1 => 'Active']) ?>

    <?= $form->field($model, 'updatedAt')->textInput(['disabled' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Edit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
