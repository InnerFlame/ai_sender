<?php

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\UserExclusionsRules */
/**
 * @var array $genders
 * @var array $searchable
 * @var array $isPaid
 * @var array $registrationPlatforms
 * @var array $excludedSources
 * @var array $orientations
 */
$this->title = 'Edit User Exclusions Rules';
$this->params['breadcrumbs'][] = ['label' => 'User Exclusions Rules', 'url' => ['edit']];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="user-exclusions-rules-update">

    <?= $this->render('_form', [
        'model'                 => $model,
        'genders'               => $genders,
        'isPaid'                => $isPaid,
        'searchable'            => $searchable,
        'registrationPlatforms' => $registrationPlatforms,
        'excludedSources'       => $excludedSources,
        'orientations'          => $orientations
    ]) ?>

</div>
