<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\phoenix\NichesCommTroublesLog;

/* @var $this yii\web\View */
/* @var $countries array */
/* @var $niches array */
/* @var $searchModel app\models\phoenix\NichesCommTroublesLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Niches Comm Troubles Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="niches-comm-troubles-log-index">

<?= Html::a('Clean error log', '/niches-comm-troubles/clean-niches-log', ['class' => 'btn btn-danger', 'style' => 'float:right']); ?>

<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            [
                'attribute' => 'niche',
                'format'    => 'raw',
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'niche',
                    $niches,
                    ['class' => 'form-control', 'prompt' => '']
                )
            ],
            [
                'attribute' => 'country',
                'format'    => 'raw',
                'filter'    => Html::activeDropDownList(
                    $searchModel,
                    'country',
                    $countries,
                    ['class' => 'form-control', 'prompt' => '']
                )
            ],
            [
                'attribute' => 'profileType',
                'format'    => 'raw',
                'value'     => function($model) {
                    if ($model->profileType == NichesCommTroublesLog::PROFILE_TYPE_RECIPIENT) {
                        return 'Recipient';
                    }
                    
                    return 'Sender';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'profileType',
                    [NichesCommTroublesLog::PROFILE_TYPE_SENDER => 'Sender', NichesCommTroublesLog::PROFILE_TYPE_RECIPIENT => 'Recipient'],
                    ['class' => 'form-control', 'prompt' => '']
                )
            ],
            'message',
            [
                'headerOptions' => ['style' => 'width:20%'],
                'attribute' => 'createdAt',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'createdAt',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'yyyy-mm-dd'
                    ]
                ]),
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open" title="View More Info"></span>', $url , ['class' => 'view', 'data-pjax' => '0']);
                    },
                ],
            ],
        ],
    ]);

    ?>
<?php
    Pjax::end();

    $this->registerJs("
            $(document).on('ready pjax:success', function() {
                $('.view').click(function(e){
                   e.preventDefault();
                   $('#pModal').modal('show')
                              .find('.modal-content')
                              .load($(this).attr('href'));
                });
            });
        ");

    yii\bootstrap\Modal::begin([
        'id'=>'pModal'
    ]);
    yii\bootstrap\Modal::end();
?>
</div>
