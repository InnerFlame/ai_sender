<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\NichesCommTroublesLog */


?>
<div class="niches-comm-troubles-log-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'niche',
            'country',
            'profileType',
            'message',
            'createdAt',
        ],
    ]) ?>

</div>
