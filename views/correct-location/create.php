<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\phoenix\CorrectLocation */

$this->title = 'Create Correct Location';
$this->params['breadcrumbs'][] = ['label' => 'Correct Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="correct-location-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
