<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\phoenix\ApiSearchRule */

$this->title = 'Create Api Search Rule';
$this->params['breadcrumbs'][] = ['label' => 'Api Search Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-search-rule-create">


    <?= $this->render('_form', [
        'model'   => $model,
        'country' => $country,
    ]) ?>

</div>
