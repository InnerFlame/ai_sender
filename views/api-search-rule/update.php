<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\ApiSearchRule */

$this->title = 'Update Api Search Rule: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Api Search Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="api-search-rule-update">


    <?= $this->render('_form', [
        'model'   => $model,
        'country' => $country,
    ]) ?>

</div>
