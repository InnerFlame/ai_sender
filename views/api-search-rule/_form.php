<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\ApiSearchRule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="api-search-rule-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'country')->dropDownList($country, ['prompt' => 'Select Country']) ?>

    <?= $form->field($model, 'isActive')->dropDownList(['No', 'Yes'], ['prompt' => 'Is Active']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
