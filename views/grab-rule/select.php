<?php
use yii\helpers\Html;
?>

<div class="form-group">
    <?php echo Html::label($type, $type, ['style' => 'text-transform: capitalize;'])?>
    <?php echo Html::dropDownList('GrabRule[' . $type . ']', null, $data[$type], ['class' => 'form-control', 'id' => 'grabrule-' . $type, 'prompt' => 'Select ' . $type]); ?>
</div>

