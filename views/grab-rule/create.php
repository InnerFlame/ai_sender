<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\phoenix\GrabRule */

$this->title = 'Create Search Rule';
$this->params['breadcrumbs'][] = ['label' => 'Search Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/js/grabRule/selectLocation.js',  ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset',]]);
?>
<div class="grab-rule-create">


    <?= $this->render('_form', [
        'model'   => $model,
        'country' => $country,
        'site'    => $site,
    ]) ?>

</div>
