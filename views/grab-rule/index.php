<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\phoenix\GrabRuleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Search Rules';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grab-rule-index">


    <p>
        <?= Html::a('Create Search Rule', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'country',
                'filter' => Html::activeDropDownList($searchModel, 'country', $country, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('country', $model->country);
                }

            ],
            'city',
            'state',
            [
                'attribute' => 'site',
                'filter' => Html::activeDropDownList($searchModel, 'site', $site, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    if ($model->site) {
                        return Yii::$app->yaml->parseByKey('site', $model->site)['name'];
                    }

                    return null;
                }

            ],
            'ageFrom',
            'ageTo',
            [
                'attribute' => 'isActive',
                'filter' => Html::activeDropDownList($searchModel, 'isActive', [0 => 'No', 1 => 'Yes'], ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return $model->isActive ? 'Yes' : 'No';
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{active} {delete}',
                'buttons' => [
                    'active' => function ($url, $model) {
                        if (!$model->isActive) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-ok-circle"></span>',
                                ['grab-rule/active-rule', 'id' => $model->id],
                                [
                                    'title' => 'Active Rule',
                                    'aria-label' => 'Active Rule',
                                    'data-method' => 'post',
                                ]
                            );
                        } else {
                            return Html::a(
                                '<span class="glyphicon glyphicon-remove-circle"></span>',
                                ['grab-rule/disable-rule', 'id' => $model->id],
                                [
                                    'title' => 'Disable Rule',
                                    'aria-label' => 'disable Rule',
                                    'data-method' => 'post',
                                ]
                            );
                        }
                    },
                ],
            ],
        ],
    ]); ?>
</div>
