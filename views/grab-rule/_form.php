<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\GrabRule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="grab-rule-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'country')->dropDownList($country, ['prompt' => 'Select Country']) ?>

    <div id="state"></div>
    <div id="city"></div>

    <?= \diiimonn\widgets\SpinnerCanvas::widget([
        'options' => [
            'class' => 'spinner',
            'style' => 'display: none;',
        ],
        'scriptOptions' => [
            'radius' => 5,
            'height' => 5,
            'width' => 1.6,
            'dashes' => 10,
            'opacity' => 1,
            'padding' => 3,
            'rotation' => 700,
            'color' => '#666666',
        ]
    ]) ?>

    <?= $form->field($model, 'site')->dropDownList($site, ['prompt' => 'Select Site']) ?>


    <?= $form->field($model, 'ageFrom')->textInput() ?>

    <?= $form->field($model, 'ageTo')->textInput() ?>

    <?= $form->field($model, 'isActive')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>



