<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\ProfileMessage */

$this->title = 'Update Profile Message: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Profile Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="profile-message-update">


    <?= $this->render('_form', [
        'model'       => $model,
        'gender'      => $gender,
        'projects'    => $projects,
        'sexuality'   => $sexuality,
        'country'     => $country,
        'language'    => $language,
        'sendMode'    => $sendMode,
    ]) ?>

</div>
