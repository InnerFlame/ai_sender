<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\phoenix\ProfileMessagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Profile Messages';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/js/profileMessage/multipleAction.js',  ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset',]]);
?>
<div class="profile-message-index">


    <p>
        <?= Html::a('Create Profile Message', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= Html::beginForm('multiple-edit', 'get')?>

    <p>
        <a href="javascript:void(0);" class="glyphicon glyphicon-pencil" id="multiple-edit"></a>
        <a href="javascript:void(0);" class="glyphicon glyphicon-trash" id="multiple-delete"></a>
        <a href="javascript:void(0);" class="glyphicon glyphicon-ok-circle" id="multiple-activation"></a>
        <a href="javascript:void(0);" class="glyphicon glyphicon-remove-circle" id="multiple-deactivation"></a>
    </p>

    <?= \diiimonn\widgets\SpinnerCanvas::widget([
        'options' => [
            'class' => 'spinner',
            'style' => 'display: none;',
        ],
        'scriptOptions' => [
            'radius' => 5,
            'height' => 5,
            'width' => 1.6,
            'dashes' => 10,
            'opacity' => 1,
            'padding' => 3,
            'rotation' => 700,
            'color' => '#666666',
        ]
    ]) ?>

    <p id="content-box"></p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\CheckboxColumn'],

            'body:ntext',
            [
                'attribute' => 'language',
                'filter'    => $language,
            ],
            [
                'attribute' => 'country',
                'filter' => Html::activeDropDownList($searchModel, 'country', $country, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('country', $model->country);
                }
            ],
            [
                'attribute' => 'gender',
                'filter' => Html::activeDropDownList($searchModel, 'gender', $gender, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('gender', $model->gender);
                }
            ],
            [
                'attribute' => 'sexuality',
                'filter' => Html::activeDropDownList($searchModel, 'sexuality', $sexuality, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('sexuality', $model->sexuality);
                }
            ],
            [
                'attribute' => 'project',
                'filter' => Html::activeDropDownList($searchModel, 'project', $projects, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('phoenixProject', $model->project);
                }
            ],
            [
                'attribute' => 'sendMode',
                'filter' => Html::activeDropDownList($searchModel, 'sendMode', $sendMode, ['class' => 'form-control', 'prompt' => '']),
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('sendMode', $model->sendMode);
                }
            ],
            [
                'attribute' => 'isPaid',
                'filter'    => [0 => 'No', 1 => 'Yes'],
                'value' => function ($model) {
                    return $model->isPaid ? 'Yes' : 'No';
                }
            ],
            [
                'attribute' => 'isActive',
                'filter' => [0 => 'No', 1 => 'Yes'],
                'value' => function ($model) {
                    return $model->isActive ? 'Yes' : 'No';
                }
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?= Html::endForm();?>
</div>
