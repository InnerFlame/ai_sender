<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\phoenix\ProfileMessage */

$this->title = 'Update Profile Messages';
$this->params['breadcrumbs'][] = ['label' => 'Update Profile Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-message-create">


    <?php echo Html::beginForm('multiple-edit', 'post')?>

    <?php foreach ($messages as $message): ?>
        <div class="form-group">
            <textarea class="form-control" name="body[<?php echo $message->id ?>]" rows="6"><?php echo $message->body ?></textarea>
        </div>
    <?php endforeach; ?>

    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php echo Html::endForm();?>

</div>
