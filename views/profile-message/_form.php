<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\ProfileMessage */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="profile-message-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'language')-> dropDownList($language) ?>

    <?= $form->field($model, 'country')->dropDownList($country) ?>

    <?= $form->field($model, 'gender')->dropDownList($gender) ?>

    <?= $form->field($model, 'sexuality')->dropDownList($sexuality) ?>

    <?= $form->field($model, 'sendMode')->dropDownList($sendMode) ?>

    <?= $form->field($model, 'project')->dropDownList($projects) ?>

    <?= $form->field($model, 'isPaid')->dropDownList([0 => 'No', 1 => 'Yes']) ?>

    <?= $form->field($model, 'isActive')->dropDownList([0 => 'No', 1 => 'Yes']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
