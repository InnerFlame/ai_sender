<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\phoenix\ProfileMessage */

$this->title = 'Create Profile Message';
$this->params['breadcrumbs'][] = ['label' => 'Profile Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-message-create">


    <?= $this->render('_form', [
        'model'       => $model,
        'gender'      => $gender,
        'projects'    => $projects,
        'sexuality'   => $sexuality,
        'country'     => $country,
        'language'    => $language,
        'sendMode'    => $sendMode,
    ]) ?>

</div>
