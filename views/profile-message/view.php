<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\phoenix\ProfileMessage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Profile Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-message-view">


    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this message?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'subject',
            'body:ntext',
            'language',
            [
                'attribute' => 'sendMode',
                'value'     => $sendMode
            ],
            [
                'attribute' => 'country',
                'value'     => $country
            ],
            [
                'attribute' => 'gender',
                'value'     => $gender
            ],
            [
                'attribute' => 'sexuality',
                'value'     => $sexuality
            ],
            [
                'attribute' => 'project',
                'value'     => $project
            ],
            [
                'attribute' => 'isPaid',
                'value'     => $isPaid
            ],
            [
                'attribute' => 'isActive',
                'value'     => $isActive
            ],
        ],
    ]) ?>

</div>
