<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Email';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('/js/email/check.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset']]);

?>

<div class="excluded-site-form">

    <div style="width: 50%;float: left">
        <?= Html::textInput('email', null, ['class' => 'form-control'])?>
        <div style="float: left;margin-top: 15px;">
            <?php echo Html::submitButton('Check', ['class' => 'btn btn-success', 'id' => 'check']) ?>
        </div>
        <div id="content-box"></div>
    </div>

    <div>
        <?= \diiimonn\widgets\SpinnerCanvas::widget([
            'options' => [
                'class' => 'spinner',
                'style' => 'display:none;top: 120px;',
            ],
            'scriptOptions' => [
                'radius' => 10,
                'height' => 10,
                'width' => 1.6,
                'dashes' => 10,
                'opacity' => 1,
                'padding' => 3,
                'rotation' => 700,
                'color' => '#666666',
            ]
        ]) ?>
    </div>

    <div style="width: 100%;float: left;margin-top: 25px;">
        <?php $form =ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <?= $form->field($model, 'emailFile')->fileInput() ?>
            <div class="form-group">
                <?= Html::submitButton('Upload File', ['class' => 'btn btn-success']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

