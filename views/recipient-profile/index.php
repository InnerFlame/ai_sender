<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\phoenix\RecipientProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/**
 * @var $site
 * @var $country
 * @var $gender
 * @var $niches
 * @var $sexuality
 */
$this->title = 'Recipient Profiles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recipient-profile-index">


    <?php echo Html::a('Reset Filters', ['recipient-profile/index'], ['class' => 'btn btn-success'])?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'remoteId',
            [
                'attribute' => 'site',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'site',
                    $site,
                    ['class' => 'form-control', 'prompt' => '']
                ),
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('site', $model['site'])['name'];
                }
            ],
            [
                'attribute' => 'country',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'country',
                    $country,
                    ['class' => 'form-control', 'prompt' => '']
                ),
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('country', $model['country']);
                }
            ],
            [
                'attribute' => 'isPaid',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'isPaid',
                    ['No', 'Yes'],
                    ['class' => 'form-control', 'prompt' => '']
                ),
                'value' => function ($model) {
                    return $model['isPaid'] == 1 ? 'Yes' : 'No';
                }
            ],
            [
                'attribute' => 'gender',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'gender',
                    $gender,
                    ['class' => 'form-control', 'prompt' => '']
                ),
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('gender', $model['gender']);
                }
            ],
            [
                'attribute' => 'sexuality',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'sexuality',
                    $sexuality,
                    ['class' => 'form-control', 'prompt' => '']
                ),
                'value' => function ($model) {
                    return Yii::$app->yaml->parseByKey('sexuality', $model['sexuality']);
                }
            ],
            [
                'attribute' => 'niche',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'niche',
                    $niches,
                    ['class' => 'form-control', 'prompt' => '']
                ),
                'value' => function ($model) use ($searchModel) {
                    /**@var $model \app\models\phoenix\RecipientProfile */
                    return $model['niche'] ?? 'Undefined/General';
                }
            ],
            [
                'attribute' => 'complained',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'complained',
                    [
                        true  => 'Complained',
                        false => 'Not complained'
                    ],
                    ['class' => 'form-control', 'prompt' => '']
                ),
                'value' => function ($model) {
                    /**@var $model \app\models\phoenix\RecipientProfile */
                    return $model['complained'] ?? 'Not set';
                }
            ],
            [
                'attribute' => 'searchable',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'searchable',
                    [
                        true  => 'Searchable',
                        false => 'Not searchable'
                    ],
                    ['class' => 'form-control', 'prompt' => '']
                ),
                'value' => function ($model) {
                    /**@var $model \app\models\phoenix\RecipientProfile */
                    return $model['searchable'] ?? 'Not set';
                }
            ],
            [
                'attribute' => 'isPrepaid',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'isPrepaid',
                    ['No', 'Yes'],
                    ['class' => 'form-control', 'prompt' => '']
                ),
                'value' => function ($model) {
                    return $model['isPrepaid'] == 1 ? 'Yes' : 'No';
                }
            ],
            [
                'attribute' => 'activationTime',
                'filter'    => \kartik\date\DatePicker::widget([
                    'value'         => $searchModel->dateFrom,
                    'name'          => $searchModel->formName() . '[dateFrom]',
                    'type'          => \kartik\date\DatePicker::TYPE_RANGE,
                    'name2'         => $searchModel->formName() . '[dateTo]',
                    'value2'        => $searchModel->dateTo,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'yyyy-mm-dd'
                    ]
                ])
            ],
        ],
    ]); ?>
</div>
