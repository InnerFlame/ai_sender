<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $sites array
 * @var $countries array
 */

$this->title = 'Export To SCV Recipient ShortId';
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="col-md-12">
    <div class="col-md-8 col-md-offset-2">
        <?php $form = ActiveForm::begin(); ?>

            <div class="col-md-12 form-group">
                <?= Html::label('Date', 'date'); ?>
                <?= DatePicker::widget([
                    'name'          => 'dateFrom',
                    'id'            => 'date',
                    'type'          => DatePicker::TYPE_RANGE,
                    'name2'         => 'dateTo',
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd']
                ]); ?>
            </div>

            <div class="col-md-6 form-group">
                <?= Html::label('Site', 'site'); ?>
                <?= Html::listBox('site', null, $sites, [
                    'class'    => 'form-control',
                    'multiple' => 'multiple',
                    'id'       => 'site',
                    'style'    => 'height:450px'
                ]) ?>
            </div>

            <div class="col-md-6 form-group">
                <?= Html::label('Country', 'country'); ?>
                <?= Html::listBox('country', null, $countries, [
                    'class'    => 'form-control',
                    'multiple' => 'multiple',
                    'id'       => 'country',
                    'style'    => 'height:450px'
                ]) ?>
            </div>

            <div class="col-md-12 form-group">
                <?= Html::submitButton('Export to CSV &nbsp;&nbsp;<span class="glyphicon glyphicon-export"></span>',
                    ['class' => 'btn btn-lg btn-success', 'style' => 'width:100%']); ?>
            </div>

            <div class="col-md-4 form-group pull-right">
                <?= Html::a('Clean scam words cache data &nbsp;&nbsp;<span class="glyphicon glyphicon-remove-circle"></span>',
                    ['recipient-profile/clean-scam-words-cache'],
                    ['class' => 'btn btn-lg btn-warning', 'style' => 'width:100%;']); ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
