<?php

/* @var $this yii\web\View */
/* @var $forcedData array */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Force Change User Split Group Tool';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12">
    <br>
    <p class="text-info">If you force user split group, it will be forced for 1 day.</p>
    <div class="col-md-4">
        <?php $form = ActiveForm::begin(); ?>
        <div class="form-group">
            <label>User id</label>
            <?= Html::input(
                'text',
                'userId',
                $resultData['userId'] ?? null,
                ['class' => 'form-control', 'placeholder' => 'Enter user id here']
            ); ?>
        </div>

        <div class="form-group">
            <label>Group</label>
            <?= Html::input(
                'number',
                'splitGroup',
                $resultData['splitGroup'] ?? null,
                ['class' => 'form-control', 'placeholder' => 'Enter split group here', 'min' => 0]
            ); ?>
        </div>


        <div class="form-group">
            <?= Html::submitButton('Force &nbsp;&nbsp;<i class="fa fa-lock" aria-hidden="true"></i>',
                ['class' => 'btn btn-lg btn-warning', 'style' => 'width: 100%']
            ); ?>
        </div>

        <?php ActiveForm::end(); ?>

        <?php $form = ActiveForm::begin(); ?>
            <?= Html::hiddenInput('resetAll', true); ?>

            <?= Html::submitButton('Reset all forced data&nbsp;&nbsp;<i class="fa fa-unlock-alt" aria-hidden="true"></i>',
                [
                    'class'    => 'btn btn-lg btn-success',
                    'style'    => 'width: 50%; float:right;',
                    'disabled' => empty($forcedData)
                ]
            ); ?>
        <?php ActiveForm::end(); ?>

    </div>
    <div class="col-md-6">
        <?php if(isset($forcedData) && !empty($forcedData)): ?>
            <button data-toggle="collapse" data-target="#forcedData" class="btn btn-lg btn-primary" style="width: 100%">
                Click me to see forced data &nbsp;&nbsp<i class="fa fa-arrow-down" aria-hidden="true"></i>
            </button>

            <div id="forcedData" class="collapse">
                <ul>
                    <?php foreach ($forcedData as $userId => $group): ?>
                        <li>User - <?= $userId; ?>: Group - <?= $group; ?></li>
                    <?php endforeach; ?>
                </ul>

            </div>
        <?php endif; ?>
    </div>
</div>
