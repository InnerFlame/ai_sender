<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $splits array */
/* @var $resultData array */

$this->title = 'Check Split User Tool';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-md-12">
    <div class="col-md-6">
        <h3>Please fill this form</h3>

        <?php $form = ActiveForm::begin(); ?>
        <div class="form-group">
            <label>Split</label>
            <?= Html::dropDownList(
                    'splitName',
                    $resultData['userSplitData']['name'] ?? null,
                    array_combine(ArrayHelper::getColumn($splits, 'name'), ArrayHelper::getColumn($splits, 'name')),
                    ['class' => 'form-control', 'prompt' => 'Choose split for check', 'required' => true]
            ); ?>
        </div>

        <div class="form-group">
            <label>User id</label>
            <?= Html::input(
                    'text',
                    'userId',
                    $resultData['userData']['remoteId'] ?? null,
                    ['class' => 'form-control', 'placeholder' => 'Enter user id here', 'required' => true]
            ); ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Get Results &nbsp;&nbsp;<i class="fa fa-rocket" aria-hidden="true"></i>',
                ['class' => 'btn btn-lg btn-success', 'style' => 'width: 100%']
            ); ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-6">
        <?php if(isset($resultData['splitGroup']) && !is_null($resultData['splitGroup'])): ?>
            <h3>Here your results!</h3>
            <h2 class="text-center">
                <span class="label label-success">User Split Group Is: <?= $resultData['splitGroup'] ;?></span>
            </h2>
        <?php else: ?>
                <h2>Not under split!</h2>
        <?php endif; ?>
        <br>
        <br>

        <?php if(isset($resultData['userData'])): ?>
            <button data-toggle="collapse" data-target="#userData" class="btn btn-lg btn-default" style="width: 100%">
                Click me to see user data &nbsp;&nbsp<i class="fa fa-arrow-down" aria-hidden="true"></i>
            </button>

            <div id="userData" class="collapse">
                <ul>
                    <?php foreach ($resultData['userData'] as $attr => $value): ?>
                        <li><?= $attr; ?>: <?= is_array($value) ? implode(',', $value) : $value; ?></li>
                    <?php endforeach; ?>
                </ul>

            </div>
        <?php endif; ?>
        <br>
        <br>

        <?php if(isset($resultData['userSplitData'])): ?>

            <button data-toggle="collapse" data-target="#splitData" class="btn btn-lg btn-primary" style="width: 100%">
                Click me to see split data &nbsp;&nbsp<i class="fa fa-arrow-down" aria-hidden="true"></i>
            </button>

            <div id="splitData" class="collapse">
                <ul>
                    <?php foreach ($resultData['userSplitData'] as $attr => $value): ?>
                        <li><?= $attr; ?>: <?= is_array($value) ? implode(',', $value) : $value; ?></li>
                    <?php endforeach; ?>
                </ul>

            </div>
        <?php endif; ?>
    </div>
</div>
