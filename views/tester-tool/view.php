<?php
/**
 * @var $data array
 */
?>
<div style="padding: 10px">
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>User Id</th>
                <th>Phoenix Id</th>
                <th>Who sent</th>
                <th>Message</th>
                <th>Step</th>
                <th>Time To Send</th>
                <th>Send Started At</th>
                <th>Sent At</th>
            <tr>
        </thead>
        <tbody>
            <?php foreach ($data as $item) : ?>
                <tr style="background-color:<?php echo $item['color']; ?>">
                    <td><?php echo $item['id']; ?></td>
                    <td><?php echo $item['originId']; ?></td>
                    <td><?php echo $item['who']; ?></td>
                    <td><?php echo $item['body']; ?></td>
                    <td><?php echo $item['step']; ?></td>
                    <td><?php echo $item['timeToSend']; ?></td>
                    <td><?php echo $item['sendStartedAt']; ?></td>
                    <td><?php echo $item['sentAt']; ?></td>
                </tr>
            <?php endforeach;?>
        </tbody>

    </table>
</div>
<style>
    .modal-dialog {
        width: auto;
    }
</style