<?php
use app\models\phoenix\Schema;

/**
 * @var $data array
 */
$countries = \Yii::$app->yaml->parse('country');
$sites     = \Yii::$app->yaml->parse('site');
?>
<?php foreach ($data as $item) : ?>
    <h3>Recipient Info</h3>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Recipient Id</th>
                <th>Phoenix Id</th>
                <th>Country</th>
                <th>Last Login At</th>
                <th>Grabbing Date</th>
                <th>Photo</th>
                <th>Replied</th>
                <th>Registration Platform</th>
                <th>Complained status</th>
            <tr>
        </thead>
        <tbody>
            <tr style="<?= $item['recipient']['activationTime'] != '0000-00-00 00:00:00' ? 'background-color:#4cae4c' : ''; ?>">
                <td><?= $item['recipient']['id']; ?></td>
                <td><?= $item['recipient']['remoteId']; ?></td>
                <td><?= $countries[$item['recipient']['country']]; ?></td>
                <td><?= $item['recipient']['lastOnlineDate']; ?></td>
                <td><?= $item['recipient']['createdAt']; ?></td>
                <td><?= $item['recipient']['isWithPhoto']                                ? 'Yes' : 'No'; ?></td>
                <td><?= ($item['recipient']['activationTime'] != '0000-00-00 00:00:00' ) ? 'Yes' : 'No'; ?></td>
                <td><?= $item['recipient']['registrationPlatform']; ?></td>
                <td
                    <?= $item['recipient']['complained'] == 1 ? 'style=background-color:red;color:black;' : ''?>
                >
                    <?= $item['recipient']['complained'] ?? 'Undefined'; ?>
                </td>
            </tr>
        </tbody>
    </table>

    <h3>Chat Info</h3>
    <table class="table table-striped table-bordered"><thead>
        <tr>
            <th>Sender Id</th>
            <th>Screen Name</th>
            <th>Phoenix Id</th>
            <th>Site</th>
            <th>Country</th>
            <th>Group</th>
            <th>Time To Send</th>
            <th>Send Started At</th>
            <th>Sent At</th>
            <th>Restart Number</th>
            <th>Schema</th>
            <th></th>
        <tr>
        </thead>
        <tbody>
            <?php foreach ($item['communicationData'] as $itemComm): ?>
                <tr style='background-color: <?= $itemComm['incomingMessage'] ? '#4cae4c' : '#ddd' ?>'>
                    <?php if (!empty($item['errors'][$itemComm['communicationId']]) && $itemComm['sendStartedAt'] == 0): ?>
                        <td colspan="3">
                            <span style="float: left;color: red;text-transform: capitalize;margin-right: 10px;">
                                ERRORS:&nbsp;
                            </span>
                            <?php foreach ($item['errors'][$itemComm['communicationId']] as $error): ?>
                                <span style="float: left;color: red;text-transform: capitalize;margin-right: 10px;">
                                    <?= $error->entity; ?>.&nbsp;&nbsp;
                                </span>
                            <?php endforeach; ?>
                        </td>
                    <?php else: ?>
                        <td><?php echo $itemComm['senderId']    ?? 'Not Set'; ?></td>
                        <td><?php echo $itemComm['screenName']  ?? 'Not Set'; ?></td>
                        <td><?php echo $itemComm['originId']    ?? 'Not Set'; ?></td>
                    <?php endif; ?>
                    <td>
                        <?= $itemComm['site'] ? $sites[$itemComm['site']]['name'] : 'Not Set'; ?>
                    </td>
                    <td>
                        <?= $itemComm['country'] ? $countries[$itemComm['country']]  : 'Not Set'; ?>
                    </td>
                    <td>
                        <?= $itemComm['title']; ?>
                    </td>
                    <td>
                        <?= $itemComm['timeToSend']; ?>
                    </td>
                    <td>
                        <?= $itemComm['sendStartedAt']; ?>
                    </td>
                    <td>
                        <?= $itemComm['sentAt']; ?>
                    </td>
                    <td>
                        <?= $itemComm['restartNumber']; ?>
                    </td>
                    <td>
                        <?php
                        $schemaId = (int) $itemComm['schemaId'] ?? 0;
                        echo Schema::getTitleById($schemaId);
                        ?>
                    </td>
                    <td>
                        <?php if ($itemComm['senderId'] && $itemComm['messageId']): ?>
                            <?= \yii\helpers\Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                                ['tester-tool/view-messages', 'recipientId' => $item['recipient']['id'], 'senderId' => $itemComm['senderId']],
                                ['class' => 'view-history']
                            )?>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endforeach; ?>