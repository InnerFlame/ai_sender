<?php

use yii\helpers\Html;
use diiimonn\widgets\SpinnerCanvas;


/* @var $this yii\web\View */

$this->title = 'Tester Tool';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/js/testerTool.js',  ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset',]]);
?>


<div class="container-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="container-data">
        <div style="margin-bottom: 15px;">
            <div style="width: 60%;float: left;">
                <?= Html::textarea('profileIds', null, ['class' => 'form-control', 'id' => 'profileIds', 'style' => 'height: 160px;'])?>
            </div>

            <?= Html::submitButton('Find', ['class' => 'btn btn-primary', 'style' => 'margin-left:10px', 'id' => 'find']) ?>

            <div id="response-box" style="float: left;width: 100%;"></div>
        </div>
    </div>

    <?= SpinnerCanvas::widget([
        'options' => [
            'class' => 'spinner',
            'style' => 'display:none;',
        ],
        'scriptOptions' => [
            'radius' => 5,
            'height' => 5,
            'width' => 1.6,
            'dashes' => 10,
            'opacity' => 1,
            'padding' => 3,
            'rotation' => 700,
            'color' => '#666666',
        ]
    ]) ?>

</div>

<?php
    $this->registerJs(
        "$(document).on('click', '.view-history', function(e){
            e.preventDefault();
               $('#pModal').modal('show')
                          .find('.modal-content')
                          .load($(this).attr('href'));
        });
    ");

    yii\bootstrap\Modal::begin([
        'id'=>'pModal'
    ]);
    yii\bootstrap\Modal::end();
?>

