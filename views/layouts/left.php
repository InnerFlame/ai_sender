<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->email; ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
<!--        <form action="#" method="get" class="sidebar-form">-->
<!--            <div class="input-group">-->
<!--                <input type="text" name="q" class="form-control" placeholder="Search..."/>-->
<!--              <span class="input-group-btn">-->
<!--                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>-->
<!--                </button>-->
<!--              </span>-->
<!--            </div>-->
<!--        </form>-->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Main menu', 'options' => ['class' => 'header']],
                    [
                        'label'   => 'Dashboards',
                        'icon'    => 'dashboard',
                        'url'     => '#',
                        'items'   => [
                            ['label' => 'Sender Dashboard', 'url' => ['/dashboard'], 'icon' => 'dashboard', 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Niches Dashboard', 'url' => ['/dashboard/niches'], 'icon' => 'dashboard', 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Splits Dashboard', 'url' => ['/dashboard/splits'], 'icon' => 'dashboard', 'visible' => Yii::$app->user->can('create')],
                        ],
                        'visible' => Yii::$app->user->can('create')
                    ],
                    [
                        'label' => 'Developer Tools',
                        'icon' => 'code',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Container', 'icon' => 'file-code-o', 'url' => ['/container-client/container'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Client', 'icon' => 'file-code-o', 'url' => ['/container-client/client'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Clear Yaml Cache',  'icon' => 'file-code-o', 'url' => ['/cache/clear-yaml'], 'visible' => Yii::$app->user->can('create')],
                            [
                                'label' => 'Logs',
                                'icon' => 'code',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Frozen Clients',  'icon' => 'file-code-o', 'url' => ['/log/frozen-clients'], 'visible' => Yii::$app->user->can('create')],
                                    ['label' => 'RPC Log',  'icon' => 'file-code-o', 'url' => ['/rpc-log/index'], 'visible' => Yii::$app->user->can('create')],
                                    ['label' => 'RPC Error Log',  'icon' => 'file-code-o', 'url' => ['/rpc-log/rpc-error-log'], 'visible' => Yii::$app->user->can('create')],
                                    ['label' => 'Presence log',  'icon' => 'file-code-o', 'url' => ['/presence-event-log/index'], 'visible' => Yii::$app->user->can('create')],
                                    ['label' => 'Error log',  'icon' => 'file-code-o', 'url' => ['/log/index'], 'visible' => Yii::$app->user->can('create')],
                                    ['label' => 'Niches communication Log',  'icon' => 'file-code-o', 'url' => ['/niches-comm-troubles/index'], 'visible' => Yii::$app->user->can('create')],
                                    ['label' => 'Sender Without Photo Log',  'icon' => 'file-code-o', 'url' => ['/log/sender-without-photo'], 'visible' => Yii::$app->user->can('create')],
                                    ['label' => 'Registration Error Log',  'icon' => 'file-code-o', 'url' => ['/log/registration-error-log'], 'visible' => Yii::$app->user->can('create')],
                                ],
                            ]
                        ],
                        'visible' => Yii::$app->user->can('create')

                    ],


                    ['label' => 'Create User', 'icon' => 'user-plus', 'url' => ['/user/index'], 'visible' => Yii::$app->user->can('crudUser')],
                    [
                        'label' => 'Tools',
                        'url' => '#',
                        'icon' => 'cogs',
                        'items' => [
                            ['label' => 'Tester tool', 'icon' => 'cog', 'url' => ['tester-tool/index'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Checking Message', 'icon' => 'cog', 'url' => ['check-message/index'], 'visible' => Yii::$app->user->can('contentManager')],
                            ['label' => 'Check content tool messages', 'icon' => 'cog', 'url' => ['check-content-tool/index'],
                                'visible' => Yii::$app->user->can('contentManager')],
                            ['label' => 'Send Exclusion Rules', 'icon' => 'cog', 'url' => ['send-exclusions-rules/index'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'User Exclusion Rules', 'icon' => 'cog', 'url' => ['user-exclusions-rules/edit'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Correct Location', 'icon' => 'cog', 'url' => ['correct-location/index'], 'visible' => Yii::$app->user->can('contentManager')],
                            ['label' => 'Email', 'icon' => 'cog', 'url' => ['email/index'], 'visible' => Yii::$app->user->can('create')],
                        ],
                    ],
                    [
                        'label' => 'Monitoring',
                        'icon' => 'area-chart',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Stat Of Communication', 'icon' => 'pie-chart', 'url' => ['/stat/stat-communication'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Daily Stats All Locations', 'icon' => 'pie-chart', 'url' => ['/stat/daily-stats-all-locations'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Coverage Fatal', 'icon' => 'pie-chart', 'url' => ['/stat/coverage-fatal'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Delay Stat', 'icon' => 'pie-chart', 'url' => ['/stat/delay-stat'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Sender Ban Stat', 'icon' => 'pie-chart', 'url' => ['/stat/sender-ban-stat'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Registration Stat', 'icon' => 'pie-chart', 'url' => ['/stat/registration-stat'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Answers Rate Stat', 'icon' => 'pie-chart', 'url' => ['/answer-rate-stat/index'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Resending stat', 'icon' => 'pie-chart', 'url' => ['/stat/resending-stat'], 'visible' => Yii::$app->user->can('create')],
                        ],
                        'visible' => Yii::$app->user->can('create')
                    ],
                    [
                        'label' => 'Sender',
                        'icon' => 'cubes',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Senders Message', 'icon' => 'cube', 'url' => ['/profile-message/index'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Senders Profile', 'icon' => 'cube', 'url' => ['/sender-profile/index'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Senders Profile Attributes', 'icon' => 'cube', 'url' => ['/sender-profile-attributes/index'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Rules Of Registration ', 'icon' => 'cube', 'url' => ['/rules-registration/index'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Senders Photo', 'icon' => 'cube', 'url' => ['/sender-photo/index'], 'visible' => Yii::$app->user->can('create')],
                        ],
                        'visible' => Yii::$app->user->can('create')
                    ],
                    [
                        'label' => 'Phoenix',
                        'icon' => 'cubes',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Schema', 'icon' => 'cube', 'url' => ['/schema/index'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Group', 'icon' => 'cube', 'url' => ['/group/index'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Send Rule', 'icon' => 'cube', 'url' => ['/send-rule/index'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Excluded Sites', 'icon' => 'cube', 'url' => ['/excluded-site/index'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Communication Container', 'icon' => 'cube', 'url' => ['communication-container/index'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Search Rule', 'icon' => 'cube', 'url' => ['grab-rule/index'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Api Search Rule', 'icon' => 'cube', 'url' => ['api-search-rule/index'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Recipients Profile', 'icon' => 'cube', 'url' => ['/recipient-profile/index'], 'visible' => Yii::$app->user->can('create')],
                            ['label' => 'Recipient ShortId', 'icon' => 'cube', 'url' => ['/recipient-profile/export-short-id'], 'visible' => Yii::$app->user->can('create')],
                        ],
                        'visible' => Yii::$app->user->can('create')
                    ],
                    [
                        'label' => 'Splits',
                        'icon'  => 'percent',
                        'url'   => '#',
                        'items' => [
                            ['label' => 'Splits', 'icon' => 'bars', 'url' => ['/split/index']],
                            ['label' => 'Recipients export', 'icon' => 'vcard', 'url' => ['/split/export']],
                            ['label' => 'Check user split', 'icon' => 'cog', 'url' => ['tester-tool/check-split-user']],
                            ['label' => 'Force change user split group', 'icon' => 'cog', 'url' => ['tester-tool/force-change-user-split']]
                        ],
                        'visible' => Yii::$app->user->can('create')
                    ],
                    ['label' => 'Developer tools', 'options' => ['class' => 'header'], 'visible' => (YII_ENV == 'dev')],
                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'], 'visible' => (YII_ENV == 'dev')],
                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'], 'visible' => (YII_ENV == 'dev')],
                    ['label' => 'SmsTools', 'icon' => 'warning', 'url' => ['/sms-tools'], 'visible' => Yii::$app->user->can('create')],
                ],

            ]
        ) ?>

    </section>

</aside>
