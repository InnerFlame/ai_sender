<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            [
                'label' => 'Developer Tools',
                'items' => [
                    ['label' => 'Container', 'url' => ['/container-client/container'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Client', 'url' => ['/container-client/client'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Frozen Clients', 'url' => ['/log/frozen-clients'], 'visible' => Yii::$app->user->can('create')],
                    '<li class="divider"></li>',
                    ['label' => 'RPC Log', 'url' => ['/rpc-log/index'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'RPC Error Log', 'url' => ['/rpc-log/rpc-error-log'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Presence log', 'url' => ['/presence-event-log/index'], 'visible' => Yii::$app->user->can('create')],
                    '<li class="divider"></li>',
                    ['label' => 'Error log', 'url' => ['/log/index'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Sender Without Photo Log', 'url' => ['/log/sender-without-photo'], 'visible' => Yii::$app->user->can('create')],
                    '<li class="divider"></li>',
                    ['label' => 'Clear Yaml Cache', 'url' => ['/cache/clear-yaml'], 'visible' => Yii::$app->user->can('create')],
                ],
                'visible' => Yii::$app->user->can('create')

            ],
            ['label' => 'Create User', 'url' => ['/user/index'], 'visible' => Yii::$app->user->can('crudUser')],
            [
                'label' => 'Tools',
                'items' => [
                    ['label' => 'Tester tool', 'url' => ['tester-tool/index'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Checking Message', 'url' => ['check-message/index'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Check content tool messages', 'url' => ['check-content-tool/index'], 'visible' => Yii::$app->user->can('create')],
                ],
                'visible' => Yii::$app->user->can('create')
            ],
            [
                'label' => 'Monitoring',
                'items' => [
                    ['label' => 'Stat Of Communication', 'url' => ['/stat/stat-communication'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Daily Stats All Locations', 'url' => ['/stat/daily-stats-all-locations'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Coverage Fatal', 'url' => ['/stat/coverage-fatal'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Delay Stat', 'url' => ['/stat/delay-stat'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Sender Ban Stat', 'url' => ['/stat/sender-ban-stat'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Registration Stat', 'url' => ['/stat/registration-stat'], 'visible' => Yii::$app->user->can('create')],
                ],
                'visible' => Yii::$app->user->can('create')
            ],
            [
                'label' => 'Phoenix',
                'items' => [
                    ['label' => 'Schema', 'url' => ['/schema/index'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Group', 'url' => ['/group/index'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Send Rule', 'url' => ['/send-rule/index'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Excluded Sites', 'url' => ['/excluded-site/index'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Communication Container', 'url' => ['communication-container/index'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Search Rule', 'url' => ['grab-rule/index'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Api Search Rule', 'url' => ['api-search-rule/index'], 'visible' => Yii::$app->user->can('create')],
                    '<li class="divider"></li>',
                    ['label' => 'Senders Message', 'url' => ['/profile-message/index'], 'visible' => Yii::$app->user->can('create')],
                    '<li class="divider"></li>',
                    ['label' => 'Recipients Profile', 'url' => ['/recipient-profile/index'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Senders Profile', 'url' => ['/sender-profile/index'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Senders Profile Attributes', 'url' => ['/sender-profile-attributes/index'], 'visible' => Yii::$app->user->can('create')],
                    '<li class="divider"></li>',
                    ['label' => 'Rules Profile of Sender Registration', 'url' => ['/rules-registration/index'], 'visible' => Yii::$app->user->can('create')],
                    ['label' => 'Senders Photo', 'url' => ['/sender-photo/index'], 'visible' => Yii::$app->user->can('create')],
                ],
                'visible' => Yii::$app->user->can('create')
            ],
            Yii::$app->user->isGuest ? (
            ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
