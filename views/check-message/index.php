<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */

$this->title = 'Checking Message';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/js/checkMessage/checkMessage.js', ['depends' => ['yii\web\YiiAsset', 'yii\bootstrap\BootstrapAsset']]);
?>
<div class="presence-event-log-index">


    <div>
        <div style="width: 50%;float: left">
            <?= Html::textarea('message', null, ['class' => 'form-control', 'rows' => 10]);?>

            <div style="float: left;margin-top: 15px;">
                <?php echo Html::submitButton('Check', ['class' => 'btn btn-success', 'id' => 'check']) ?>
            </div>
        </div>

        <div style="width: 48%;float: left;margin-top: 25px;margin-left: 2%;">
            <?= \diiimonn\widgets\SpinnerCanvas::widget([
                'options' => [
                    'class' => 'spinner',
                    'style' => 'display:none;top: 120px;',
                ],
                'scriptOptions' => [
                    'radius' => 10,
                    'height' => 10,
                    'width' => 1.6,
                    'dashes' => 10,
                    'opacity' => 1,
                    'padding' => 3,
                    'rotation' => 700,
                    'color' => '#666666',
                ]
            ]) ?>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Analyzers</th>
                    <th>Scam Words</th>
                    <th>Text</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    <div id="content-box" style="float: left;width: 100%;"></div>
</div>