<?php

namespace sender\expiredContainer\components;

use app\components\rpc\RpcClientComponent;
use app\models\rpc\Container;
use sender\expiredContainer\components\actionsBeforeStop\ActionBeforeStopInterface;

class ExpiredCloser
{
    private $setting;
    private $rpcClient;

    public function __construct(
        array $setting,
        RpcClientComponent $rpcClient
    )
    {
        $this->setting = $setting;
        $this->rpcClient = $rpcClient;
    }

    public function stopContainers()
    {
        foreach ($this->setting['containersType'] as $containerType) {
            $containers = $this->getContainers($containerType);
            /** @var Container $container */
            foreach ($containers as $container) {
                $actionBeforeStop = $this->getActionBeforeStop($containerType);
                if ($actionBeforeStop instanceof ActionBeforeStopInterface) {
                    $actionBeforeStop->action($container);
                }

                $this->rpcClient->stopRpcContainer($container->name, $container->target);
                $this->rpcClient->deleteContainerClient($container->name);
            }
        }
    }

    private function getContainers($containerType)
    {
        return Container::find()
            ->where([
                'type'   => $containerType,
                'status' => $this->getStatus($containerType),
            ])
            ->andWhere(['<=', 'lastTimeActivity', time() - $this->getLiveTime($containerType)])
            ->all();
    }

    private function getLiveTime($containerType)
    {
        return $this->setting['liveTime'][$containerType] ?? $this->setting['liveTime']['def'];
    }

    private function getStatus($containerType)
    {
        return $this->setting['status'][$containerType] ?? $this->setting['status']['def'];
    }

    private function getActionBeforeStop($containerType)
    {
        return $this->setting['actionsBeforeStop'][$containerType] ?? null;
    }
}