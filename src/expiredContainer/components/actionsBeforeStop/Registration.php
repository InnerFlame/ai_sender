<?php

namespace sender\expiredContainer\components\actionsBeforeStop;

use app\models\phoenix\SenderProfileRegister;
use app\models\rpc\Container;

class Registration implements ActionBeforeStopInterface
{
    public function action(Container  $container)
    {
        /** @var SenderProfileRegister $senderProfileRegister */
        $senderProfileRegister = SenderProfileRegister::findOne(['profileId' => $container->profileId]);
        if ($senderProfileRegister) {
            if (empty($senderProfileRegister->phoenixSenderProfile->originId)) {
                $senderProfileRegister->registerStartedAt = 0;
                $senderProfileRegister->save();
            }
        }
    }
}