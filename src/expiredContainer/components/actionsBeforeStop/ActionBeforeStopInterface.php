<?php

namespace sender\expiredContainer\components\actionsBeforeStop;

use app\models\rpc\Container;

interface ActionBeforeStopInterface
{
    public function action(Container  $container);
}