<?php

namespace sender\expiredContainer\components\actionsBeforeStop;

use app\models\phoenix\ReUploadPhotoScheduler;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Container;

class ReUploadPhoto implements ActionBeforeStopInterface
{
    public function action(Container  $container)
    {
        $sender = SenderProfile::findOne($container->profileId);
        $sender->changePhotoStatus(SenderProfile::STATUS_TO_REUPLOAD_CONNECTION_REFUSED);

        $reUpload = new ReUploadPhotoScheduler();
        $reUpload->senderId = $container->profileId;
        $reUpload->timeToReUpload = date('Y-m-d H:i:s', time() + 60);
        $reUpload->save();
    }
}