<?php

namespace sender\scamCheck\components;

use app\components\rpc\RpcClientComponent;
use app\config\Constant;
use app\models\phoenix\CommunicationContainer;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class ScamCheck
{
    private const BATCH_SIZE = 50;

    /** @var RpcClientComponent */
    private $rpcClient;

    public function __construct(RpcClientComponent $rpcClient)
    {
        $this->rpcClient = $rpcClient;
    }

    public function runCheck()
    {
        foreach ($this->getCountries() as $country) {
            $startedClients = $this->getStartedClients($country);
            foreach (array_chunk($startedClients, self::BATCH_SIZE) as $clientsToCheck) {
                $this->rpcSearch(
                    $startedClients[array_rand($startedClients, 1)],
                    $clientsToCheck
                );
            }
        }
    }

    private function getCountries(): array
    {
        $containersSettings = CommunicationContainer::getActiveData();
        return array_column($containersSettings, 'country');
    }

    /**
     * @param string $country
     * @return Client[]
     */
    private function getStartedClients(string $country): array
    {
        $clients = Client::findAll([
            'target' => Constant::TARGET_PHOENIX,
            'status' => Client::STATUS_STARTED,
            'type' => Client::TYPE_COMMUNICATION,
            'country' => $country,
        ]);

        return ArrayHelper::index($clients, 'profileId');
    }

    /**
     * @param Client $searcherClient
     * @param Client[] $clientsToCheck
     */
    private function rpcSearch(Client $searcherClient, array $clientsToCheck)
    {
        $searcherProfile = SenderProfile::findOne($searcherClient->profileId);

        $profilesIds = (new Query())
            ->select('originId')
            ->from(SenderProfile::tableName())
            ->where([
                'id' => array_column($clientsToCheck, 'profileId'),
            ])
            ->all();


        $params = [
            'country' => $searcherProfile->country,
            'userId' => array_column($profilesIds, 'originId'),
        ];

        $additional = [
            'clientName' => Constant::TARGET_PHOENIX . '.' . $searcherProfile->id,
            'target' => Constant::TARGET_PHOENIX,
            'scamCheck' => true,
        ];

        $this->rpcClient->rpcCall(
            'client.' . Constant::TARGET_PHOENIX . '.' . $searcherProfile->id,
            'searchNewsFeed',
            [$params],
            $additional
        );
    }
}
