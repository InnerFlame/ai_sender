<?php

namespace sender\scamCheck\components;

use app\components\amqp\InternalAmqpComponent;
use app\components\rpc\RpcClientComponent;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Container;
use sender\communication\components\containerManager\ContainerManager;

class ScamCheckRpc
{
    const QUEUE = 'scamCheckResponse';
    /** @var InternalAmqpComponent */
    private $internalAmqp;
    /** @var RpcClientComponent */
    private $rpcClient;
    /** @var ContainerManager */
    private $containerManager;

    public function __construct(
        InternalAmqpComponent $internalAmqp,
        RpcClientComponent $rpcClient,
        ContainerManager $containerManager
    ) {
        $this->internalAmqp = $internalAmqp;
        $this->rpcClient = $rpcClient;
        $this->containerManager = $containerManager;
    }

    public function processSearchResult()
    {
        $this->internalAmqp->consumeEvents(self::QUEUE, [$this, 'searchResultHandler']);
    }

    public function searchResultHandler(array $message)
    {
        $data = $message['incomingData']['result'];

        if (empty($data['data']['users'])) {
            return true;
        }

        foreach ($data['data']['users'] as $user) {
            if (!empty($user['isScammer'])) {
                $sender = SenderProfile::findOne(['originId' => $user['id']]);
                if (!$sender || $sender->bannedAt > 0) {
                    continue;
                }
                $this->bannedSenderEvent($sender);
            }
        }
        return true;
    }

    private function bannedSenderEvent(SenderProfile $sender)
    {
        $sender->bannedAt = date('Y-m-d H:i:s');
        $sender->save();

        $this->containerManager->deleteContainer($sender);
        if (\Yii::$container->get('interactionRouter')->isSeparatedInteraction($sender)) {
            $container = Container::findOne(['profileId' => $sender->id]);
            if (!empty($container)) {
                $this->rpcClient->stopRpcContainer($container['target'] . '.' . $sender->id);
            }
        }
    }
}
