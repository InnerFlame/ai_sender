<?php

namespace sender\registration\models;

use Yii;

/**
 * This is the model class for table "phoenixRulesRegistration".
 *
 * @property integer $id
 * @property string $country
 * @property integer $sexuality
 * @property integer $gender
 * @property integer $isActive
 * @property integer $profilesCount
 * @property string $niche
 * @property integer $countCreated
 * @property integer $hasDone
 * @property integer $ageFrom
 * @property integer $ageTo
 * @property string $language
 * @property string $userType
 */
class RulesRegistration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rulesRegistration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'sexuality', 'gender', 'profilesCount', 'language', 'userType'], 'required'],
            [['sexuality', 'gender', 'isActive', 'profilesCount', 'ageFrom', 'ageTo'], 'integer'],
            [['niche'], 'safe'],
            [['niche'], 'default', 'value' => NULL],
            ['ageFrom', 'compare', 'compareValue' => 18, 'operator' => '>='],
            ['ageTo', 'compare', 'compareAttribute' => 'ageFrom', 'operator' => '>', 'message' => 'Must be greater ageFrom'],
        ];
    }

    public function validateGeneratedProfilesAgeByNiche()
    {
        if (is_null($this->niche) || !in_array($this->niche, ['mature', 'cougar'])) {
            if ($this->ageFrom >= 30 || $this->ageTo >= 30) {
                Yii::$app->session->setFlash('error', 'For general profiles you can generate profiles younger than 30 years only.');
                return false;
            }
            return true;
        }

        if($this->ageFrom < 30 || $this->ageTo > 60) {
            Yii::$app->session->setFlash('error', 'Invalid age range for this niche');
            return false;
        }

        return true;
    }

}
