<?php

namespace sender\registration\models;

use yii\db\ActiveRecord;

/**
 * Class RegisterErrorLog
 * @package sender\registration\models
 * @property integer $id
 * @property integer $senderId
 * @property string $error
 * @property string $createdAt
 */
class RegisterErrorLog extends ActiveRecord
{
    public static function tableName()
    {
        return 'registerErrorLog';
    }

    public static function getLogs($request)
    {
        $logs = self::find();
        if (!empty($request['dateFrom'])) {
            $logs->andWhere('createdAt >= :dateFrom', [':dateFrom' => strtotime($request['dateFrom'] . ' 00:00:00')]);
        }

        if (!empty($request['dateTo'])) {
            $logs->andWhere('createdAt <= :dateTo', [':dateTo' => strtotime($request['dateTo'] . ' 23:59:59')]);
        }

        if (!empty($request['senderId'])) {
            $logs->andWhere(['senderId' => $request['senderId']]);
        }

        return $logs->orderBy('id desc');
    }
}