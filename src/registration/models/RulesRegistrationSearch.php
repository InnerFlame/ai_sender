<?php

namespace sender\registration\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RulesProfileSenderRegistrationSearch represents the model behind the search form about `app\models\RulesRegistration`.
 */
class RulesRegistrationSearch extends RulesRegistration
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           [['id', 'sexuality', 'gender', 'isActive', 'hasDone'], 'integer'],
           [['country', 'niche'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query       = RulesRegistration::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'            => $this->id,
            'isActive'      => $this->isActive,
            'hasDone'       => $this->hasDone,
            'niche'         => $this->niche,
            'country'       => $this->country,
        ]);

        return $dataProvider;
    }
}
