<?php

namespace sender\registration\models;

use yii\db\ActiveRecord;

/**
 * Class SenderFetchSchedule
 * @package sender\registration\models
 * @property integer $id
 * @property integer $senderId
 * @property string $timeToFetch
 * @property string $startedAt
 * @property string $createdAt
 */

class SenderFetchSchedule extends ActiveRecord
{
    public static function tableName()
    {
        return 'senderFetchSchedule';
    }
}