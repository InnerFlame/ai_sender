<?php

namespace sender\registration\components;

use sender\registration\components\rpcStrategy\RegisteredStrategy;
use sender\registration\components\rpcStrategy\UploadedPhotoStrategy;

class RegisterRpc
{
    const QUEUE = 'rpcRegisterResponse';

    /** @var  LocationCorrecter */
    private $locationCorrecter;

    /** @var  RegisteredStrategy */
    private $registeredStrategy;

    /** @var  UploadedPhotoStrategy */
    private $uploadedPhotoStrategy;

    public function __construct(
        LocationCorrecter $locationCorrecter,
        RegisteredStrategy $registeredStrategy,
        UploadedPhotoStrategy $uploadedPhotoStrategy
    ) {
        $this->locationCorrecter     = $locationCorrecter;
        $this->registeredStrategy    = $registeredStrategy;
        $this->uploadedPhotoStrategy = $uploadedPhotoStrategy;
    }

    public function processRegister()
    {
        \Yii::$app->internalAmqp->consumeEvents(self::QUEUE, [$this, 'registerHandler']);
    }

    public function registerHandler(array $message)
    {
        switch ($message['rpcCallInfo']['method']) {
            case 'register':
                $this->registeredStrategy->processResult($this->locationCorrecter, $message['incomingData']['result']);
                break;
            case 'photoUpload':
                $this->uploadedPhotoStrategy->processResult($message);
                break;
        }

        return true;
    }
}
