<?php

namespace sender\registration\components\rpcErrorStrategy\uploadPhotoRpcErrorStrategy;

use sender\registration\components\rpcErrorStrategy\ErrorRpcStrategyInterface;

class UploadPhotoRpcErrorStrategy implements ErrorRpcStrategyInterface
{
    /** @var UploadedByAnotherUser $senderPhoto */
    private $uploadedByAnotherUser;
    /** @var ConnectionRefused $senderPhoto */
    private $connectionRefused;

    const COUNT_REUSED = 3;
    const COUNT_REUSED_CACHE_KEY = 'countReusedCacheKey';

    public function __construct(
        UploadedByAnotherUser $uploadedByAnotherUser,
        ConnectionRefused $connectionRefused
    ) {
        $this->uploadedByAnotherUser = $uploadedByAnotherUser;
        $this->connectionRefused     = $connectionRefused;
    }

    public function processingError(array $rpcCallInfo, array $incomingData)
    {
        if (strpos($incomingData['error'], '"code":481')
            || strpos($incomingData['error'], 'already exist')
        ) {
            $this->uploadedByAnotherUser->processingError($rpcCallInfo);
        } elseif (strpos($incomingData['error'], 'connection refused')) {
            $this->connectionRefused->processingError($rpcCallInfo);
        }
    }
}
