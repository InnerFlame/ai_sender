<?php

namespace sender\registration\components\rpcErrorStrategy\uploadPhotoRpcErrorStrategy;

use app\components\phoenix\reUpload\PhotoReUploadScheduler;
use app\components\rpc\RpcClientComponent;
use app\models\phoenix\SenderProfile;

class UploadedByAnotherUser
{
    /** @var RpcClientComponent $rpcClient */
    private $rpcClient;
    /** @var PhotoReUploadScheduler $senderPhoto */
    private $reuploadScheduler;

    const COUNT_REUSED = 5;
    const COUNT_REUSED_CACHE_KEY = 'countReusedCacheKey';

    public function __construct(
        RpcClientComponent $rpcClient,
        PhotoReUploadScheduler $reuploadScheduler
    ) {
        $this->rpcClient         = $rpcClient;
        $this->reuploadScheduler = $reuploadScheduler;
    }

    public function processingError(array $rpcCallInfo)
    {
        $sender = SenderProfile::findOne(['id' => $rpcCallInfo['additional']['profileId']]);
        if (empty($sender)) {
            return;
        }

        $this->addToReuploadSchedule($sender);
    }

    private function addToReuploadSchedule(SenderProfile $sender)
    {
        $this->rpcClient->stopRpcContainer('phoenix.' . $sender->id);
        $this->reuploadScheduler->addToSchedule($sender, 1800);
    }
}
