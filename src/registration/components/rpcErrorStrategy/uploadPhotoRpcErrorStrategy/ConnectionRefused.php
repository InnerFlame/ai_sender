<?php

namespace sender\registration\components\rpcErrorStrategy\uploadPhotoRpcErrorStrategy;

use app\components\rpc\RpcClientComponent;
use app\models\phoenix\ReUploadPhotoScheduler;
use app\models\phoenix\SenderProfile;
use yii\db\Query;

class ConnectionRefused
{
    /** @var RpcClientComponent $rpcClient */
    private $rpcClient;

    const COUNT_REUSED = 5;
    const COUNT_REUSED_CACHE_KEY = 'countReusedCacheKey';

    public function __construct(RpcClientComponent $rpcClient)
    {
        $this->rpcClient = $rpcClient;
    }

    public function processingError(array $rpcCallInfo)
    {
        $this->rpcClient->stopRpcContainer('phoenix.' . $rpcCallInfo['additional']['profileId']);
        if (!$this->isInProcess($rpcCallInfo['additional']['profileId'])) {
            $sender = SenderProfile::findOne($rpcCallInfo['additional']['profileId']);
            $sender->changePhotoStatus(SenderProfile::STATUS_TO_REUPLOAD_CONNECTION_REFUSED);

            $reUpload = new ReUploadPhotoScheduler();
            $reUpload->senderId = $sender->id;
            $reUpload->timeToReUpload = date('Y-m-d H:i:s', time() + 900);
            $reUpload->save();
        }
    }

    private function isInProcess($senderId)
    {
        $query = new Query();
        return $query->select('id')
            ->from(ReUploadPhotoScheduler::tableName())
            ->where([
                'senderId'      => $senderId,
                'startReUpload' => '0000-00-00 00:00:00'
            ])
            ->one();
    }

}