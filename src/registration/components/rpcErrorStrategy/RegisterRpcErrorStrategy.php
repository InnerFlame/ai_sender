<?php

namespace sender\registration\components\rpcErrorStrategy;

use app\components\rpc\RpcClientComponent;
use app\models\phoenix\SenderProfileRegister;
use sender\registration\components\fetch\FetchScheduler;

class RegisterRpcErrorStrategy implements ErrorRpcStrategyInterface
{
    /** @var RpcClientComponent $rpcClient */
    private $rpcClient;
    /** @var FetchScheduler $fetchScheduler */
    private $fetchScheduler;

    public function __construct(
        RpcClientComponent $rpcClient,
        FetchScheduler $fetchScheduler
    )
    {
        $this->rpcClient      = $rpcClient;
        $this->fetchScheduler = $fetchScheduler;
    }

    public function processingError(array $rpcCallInfo, array $incomingData)
    {
        $senderProfileRegister = SenderProfileRegister::findOne(['id' => $rpcCallInfo['params'][0]['id']]);
        if ($senderProfileRegister) {
            $senderProfileRegister->scenario = 'registerError';
            $senderProfileRegister->errorAt  = date('Y-m-d H:i:s');
            $senderProfileRegister->save();

            $this->rpcClient->stopRpcContainer('phoenix.' . $senderProfileRegister->profileId);
        }

        if (strpos($incomingData['error'], '"code":401')) {
            $this->fetchScheduler->schedule($senderProfileRegister->profileId);
        }
    }
}