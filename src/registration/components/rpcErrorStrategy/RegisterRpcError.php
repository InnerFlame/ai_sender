<?php

namespace sender\registration\components\rpcErrorStrategy;

use sender\registration\components\ErrorLogger;
use sender\registration\components\rpcErrorStrategy\uploadPhotoRpcErrorStrategy\UploadPhotoRpcErrorStrategy;

class RegisterRpcError
{
    private $registerErrorStrategy;
    private $uploadPhotoErrorStrategy;
    private $errorLogger;

    public function __construct(
        RegisterRpcErrorStrategy $registerErrorStrategy,
        UploadPhotoRpcErrorStrategy $uploadPhotoErrorStrategy,
        ErrorLogger $errorLogger
    )
    {
        $this->registerErrorStrategy    = $registerErrorStrategy;
        $this->uploadPhotoErrorStrategy = $uploadPhotoErrorStrategy;
        $this->errorLogger = $errorLogger;
    }

    public function processingError(array $rpcCallInfo, array $incomingData)
    {
        $this->errorLogger->log($rpcCallInfo['additional']['profileId'], json_encode($incomingData));
        $strategy = $this->getStrategy($rpcCallInfo);
        if ($strategy instanceof ErrorRpcStrategyInterface){
            $strategy->processingError($rpcCallInfo, $incomingData);
        }
    }

    private function getStrategy($rpcCallInfo)
    {
        switch ($rpcCallInfo['method']) {
            case 'register':
                return $this->registerErrorStrategy;
                break;
            case 'photoUpload':
                return $this->uploadPhotoErrorStrategy;
                break;
            default:
                return null;
        }
    }
}