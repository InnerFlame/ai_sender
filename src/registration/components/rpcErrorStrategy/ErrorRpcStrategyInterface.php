<?php

namespace sender\registration\components\rpcErrorStrategy;

interface ErrorRpcStrategyInterface
{
    public function processingError(array $rpcCallInfo, array $incomingData);
}