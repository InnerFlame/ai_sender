<?php

namespace sender\registration\components\generate;

use app\models\phoenix\SenderProfile;

interface PhotoInterface
{
    public function get(SenderProfile $senderProfile);
}
