<?php

namespace sender\registration\components\generate;


use yii\helpers\ArrayHelper;

class CityData
{
    public function get($country)
    {
        $cities = \Yii::$app->yaml->parseByKey('city', $country);
        $data   = [];

        if (!is_array($cities[0])) {
            $data['state'] = NULL;
            $data['city']  = $cities[array_rand($cities)];
        } else {
            if (empty($cities[0]['count'])) {
                $cityData      = $cities[array_rand($cities)];
                $data['state'] = $cityData['state'] ?? NULL;
                $data['city']  = $cityData['city'];
            } else {
                $maxCount = max(ArrayHelper::getColumn($cities, 'count'));
                $rand     = rand(0, $maxCount);

                ArrayHelper::multisort($cities, 'count');

                foreach ($cities as $item) {
                    if ($item['count'] >= $rand) {
                        $data['state'] = $item['state'] ?? NULL;
                        $data['city']  = $item['city'];
                        break;
                    }
                }
            }
        }

        return $data;
    }
}