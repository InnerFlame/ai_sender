<?php

namespace sender\registration\components\generate;

use app\config\Constant;
use app\models\phoenix\SenderEmail;
use linslin\yii2\curl\Curl;
use yii\base\Component;
use yii\base\ErrorException;
use yii\db\Expression;
use yii\db\Query;

class EmailGenerator extends Component
{
    public function getEmail()
    {
        /** @var SenderEmail $senderEmail */
        $senderEmail = SenderEmail::find()
            ->where(['isUsed' => 0])
            ->orderBy(new Expression('rand()'))
            ->one();
        if (!empty($senderEmail)) {
            $senderEmail->isUsed = 1;
            $senderEmail->save();

            return $senderEmail->email;
        }

        throw new ErrorException('Not email in our pull');
    }

    public function getEmailByScreenName($name)
    {
        $cookie = $this->getCookieForTempMail();
        $domain = $this->getDomain();

        if (!$domain) {
            return false;
        }

        $mail     = mb_strtolower($name . $domain);
        $postData = [
            'mail'   => mb_strtolower($name),
            'domain' => $domain,
            'csrf'   => $this->getCsrfByCookie($cookie)
        ];


        $query = http_build_query($postData);

        $curl = new Curl();

        $data = $curl->setOption(CURLOPT_POSTFIELDS, $query)
            ->setOption(CURLOPT_CONNECTTIMEOUT, 30)
            ->setOption(CURLOPT_TIMEOUT, 30)
            ->setOption(CURLOPT_COOKIE, $cookie)
            ->setOption(CURLOPT_SSL_VERIFYHOST, 0)
            ->setOption(CURLOPT_SSL_VERIFYPEER, 0)
            ->setOption(CURLOPT_PROXY, 'http://zproxy.luminati.io:22225')
            ->setOption(CURLOPT_PROXYUSERPWD, 'lum-customer-' . Constant::LUMINATI_CUSTOMER . '-zone-' . Constant::LUMINATI_ZONE . ':' . Constant::LUMINATI_PASS)
            ->post('https://temp-mail.org/en/option/change');

        if (stripos($data, $mail) !== false) {
            return $mail;
        }

        return false;
    }

    /**
     * get all domains from api temp-mail
     * @return bool
     */
    private function getDomain()
    {
        $curl = new Curl();

        $data = $curl->setOption(CURLOPT_CONNECTTIMEOUT, 30)
            ->setOption(CURLOPT_TIMEOUT, 30)
            ->setOption(CURLOPT_SSL_VERIFYHOST, 0)
            ->setOption(CURLOPT_SSL_VERIFYPEER, 0)
            ->setOption(CURLOPT_PROXY, 'http://zproxy.luminati.io:22225')
            ->setOption(CURLOPT_PROXYUSERPWD, 'lum-customer-' . Constant::LUMINATI_CUSTOMER . '-zone-' . Constant::LUMINATI_ZONE . ':' . Constant::LUMINATI_PASS)
            ->get('https://api.temp-mail.org/request/domains/format/json/');

        if (empty($data)) {
            return false;
        }

        $data = json_decode($data);

        if (empty($data)) {
            return false;
        }

        $domain = $data[array_rand($data)];

        return $domain;
    }

    private function getCsrfByCookie($cookie)
    {
        $cookie = substr($cookie, strpos($cookie, 'csrf=') + 5);
        return substr($cookie, 0,  strpos($cookie, '; '));
    }

    private function getCookieForTempMail()
    {
        $curl = new Curl();

        $data = $curl->setOption(CURLOPT_CONNECTTIMEOUT, 30)
            ->setOption(CURLOPT_TIMEOUT, 30)
            ->setOption(CURLOPT_SSL_VERIFYHOST, 0)
            ->setOption(CURLOPT_SSL_VERIFYPEER, 0)
            ->setOption(CURLOPT_HEADER, 1)
            ->setOption(CURLOPT_PROXY, 'http://zproxy.luminati.io:22225')
            ->setOption(CURLOPT_PROXYUSERPWD, 'lum-customer-' . Constant::LUMINATI_CUSTOMER . '-zone-' . Constant::LUMINATI_ZONE . ':' . Constant::LUMINATI_PASS)
            ->get('https://temp-mail.org');

        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $data, $matches);

        return implode('; ', $matches[1]);
    }

    public function pushEmailToDb()
    {
        if (!is_file(\Yii::$app->basePath . '/web/files/emails.csv')) {
            return false;
        }

        $data   = [];
        $emails = array_map('str_getcsv', file(\Yii::$app->basePath . '/web/files/emails.csv'));
        foreach ($emails as $email) {
            $email = explode(':', $email[0])[0];
            list(, $domain) = explode('@', $email);
            $senderEmail = SenderEmail::findOne(['email' => $email]);
            if (empty($senderEmail)) {
                $data[] = [
                    'email'  => $email,
                    'domain' => $domain,
                ];
            }

            if (count($data) >= 100) {
                \Yii::$app->db->createCommand()->batchInsert(SenderEmail::tableName(), ['email', 'domain'], $data)->execute();
                $data = [];
            }
        }

        return true;
    }

    public function filterEmailsForUnreadableEmail()
    {
        $ids        = [];
        $dataToSave = [];
        $query = (new Query())->select('id, email')
            ->from(SenderEmail::tableName());
        foreach ($query->batch(100) as $emails) {
            foreach ($emails as $item) {
                if (!$this->isValidEmail($item['email'])) {
                    $ids[]      = $item['id'];
                    $dataToSave = [$item['email']];
                }

                if (count($ids) >= 200) {
                    SenderEmail::deleteAll(['id' => $ids]);
                    $ids = [];
                }
            }
        }

        if (!empty($ids)) {
            SenderEmail::deleteAll(['id' => $ids]);
        }

        $this->saveToCsv($dataToSave);
    }

    private function saveToCsv($dataToSave)
    {
        $file = fopen(\Yii::$app->basePath . '/web/files/blackEmails.csv', 'w');
        foreach ($dataToSave as $item) {
            fputcsv($file, $item);
        }

        fclose($file);

        return true;
    }

    public function isValidEmail($email)
    {
        $vowels = 'aeiouy';
        if (preg_match('/^.*?([' . $vowels . ']{5,}|[^' . $vowels . ']{6,}).*?@/i', $email, $matches)) {
            return false;
        }

        return true;
    }
}