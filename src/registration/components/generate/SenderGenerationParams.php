<?php

namespace sender\registration\components\generate;

use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileRemoveSchedule;
use sender\registration\models\RulesRegistration;

class SenderGenerationParams
{
    public $ruleId;
    public $niche;
    public $country;
    public $language;
    public $sexuality;
    public $gender;
    public $count;
    public $ageFrom;
    public $ageTo;
    public $userType;
    public $height;
    public $weight;
    public $build;
    public $race;
    public $eyeColor;
    public $hairColor;
    public $familyStatus;
    public $glasses;
    public $religion;
    public $timeToRegister;

    public static function getSenderDataForRegisterRuleGeneration(RulesRegistration $rulesRegistration)
    {
        $senderGeneration                 = new self();
        $senderGeneration->ruleId         = $rulesRegistration->id;
        $senderGeneration->country        = $rulesRegistration->country;
        $senderGeneration->language       = $rulesRegistration->language;
        $senderGeneration->sexuality      = $rulesRegistration->sexuality;
        $senderGeneration->gender         = $rulesRegistration->gender;
        $senderGeneration->userType       = $rulesRegistration->userType;
        $senderGeneration->ageFrom        = $rulesRegistration->ageFrom;
        $senderGeneration->ageTo          = $rulesRegistration->ageTo;
        $senderGeneration->niche          = $rulesRegistration->niche;
        $senderGeneration->timeToRegister = date('Y-m-d H:i:s');

        if ($race = self::getRaceByNiche($rulesRegistration->niche)) {
            $senderGeneration->race = $race;
        }

        return $senderGeneration;
    }

    protected static function getRaceByNiche(string $niche = null)
    {
        switch ($niche) {
            case 'black':
                return 3;
                break;
            case 'asian':
                return 2;
                break;
            case 'latino':
                return 6;
                break;
        }

        return null;
    }

    public static function getSenderDataForGenerationAfterRemove(
        SenderProfile $senderProfile,
        SenderProfileRemoveSchedule $senderProfileRemoveSchedule
    ) {
        $age = date('Y') - date('Y', strtotime($senderProfile->birthday));
        if ($age > 21) {
            $ageFrom = $age - 3;
        } else {
            $ageFrom = $age;
        }
        $ageTo = $age + 3;

        $senderGeneration                 = new self();
        $senderGeneration->ruleId         = 0;
        $senderGeneration->country        = $senderProfile->country;
        $senderGeneration->language       = $senderProfile->language;
        $senderGeneration->sexuality      = $senderProfile->sexuality;
        $senderGeneration->gender         = $senderProfile->gender;
        $senderGeneration->userType       = $senderProfile->userType;
        $senderGeneration->ageFrom        = $ageFrom;
        $senderGeneration->ageTo          = $ageTo;
        $senderGeneration->niche          = $senderProfileRemoveSchedule->niche;
        $senderGeneration->height         = $senderProfile->senderProfileAttributes->height;
        $senderGeneration->weight         = $senderProfile->senderProfileAttributes->weight;
        $senderGeneration->build          = $senderProfile->senderProfileAttributes->build;
        $senderGeneration->race           = $senderProfile->senderProfileAttributes->race;
        $senderGeneration->eyeColor       = $senderProfile->senderProfileAttributes->eyeColor;
        $senderGeneration->hairColor      = $senderProfile->senderProfileAttributes->hairColor;
        $senderGeneration->familyStatus   = $senderProfile->senderProfileAttributes->familyStatus;
        $senderGeneration->glasses        = $senderProfile->senderProfileAttributes->glasses;
        $senderGeneration->religion       = $senderProfile->senderProfileAttributes->religion;
        $senderGeneration->timeToRegister = date('Y-m-d H:i:s', time() + rand(180, 600));

        return $senderGeneration;
    }
}
