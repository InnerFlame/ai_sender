<?php

namespace sender\registration\components\generate;

use app\models\phoenix\SenderProfile;

class BirthdayGenerator
{
    public function get(SenderGenerationParams $data, int $totalCountProfiles = 0)
    {
        $birthday = date('Y-m-d', $this->getRandomDate($data->ageFrom, $data->ageTo));
        while ($this->validBirthdayPercentSenderProfile($birthday, $totalCountProfiles)) {
            $birthday = date('Y-m-d', $this->getRandomDate($data->ageFrom, $data->ageTo));
        }

        return $birthday;
    }

    private function getRandomDate($yearFrom = 18, $yearTo = 25)
    {
        return mt_rand(strtotime("- $yearTo year"), strtotime("- $yearFrom year"));
    }

    private function validBirthdayPercentSenderProfile($birthday, $count)
    {
        if($count == 0) {
            $count = SenderProfile::getCount();
        }

        $birthdayCount = SenderProfile::getCountByBirthday($birthday);
        $result        = false;
        if($birthdayCount > 0 && $birthdayCount > ($count * 0.05)) {
            $result = true;
        }

        return $result;
    }
}