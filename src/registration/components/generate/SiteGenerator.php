<?php

namespace sender\registration\components\generate;

class SiteGenerator
{
    public function get(SenderGenerationParams $data): int
    {
        $allowedSites = \Yii::$app->yaml->parseByKey('senderGenerator/availableSites', $data->country);

        if (empty($allowedSites)) {
            $allowedSites = \Yii::$app->yaml->parseByKey('senderGenerator/availableSites', 'DEF');
        }

        if (empty($allowedSites)) {
            throw new \Exception('No available sites to registranion on: ' . $data->country);
        }

        $site = $allowedSites[array_rand($allowedSites)];

        return $site;
    }
}