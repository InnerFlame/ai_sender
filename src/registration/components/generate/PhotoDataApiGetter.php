<?php

namespace sender\registration\components\generate;

use app\config\Constant;
use app\models\phoenix\SenderPhoto;
use app\models\phoenix\SenderPhotoNiche;
use app\models\phoenix\SenderProfile;
use linslin\yii2\curl\Curl;
use sender\niche\components\NicheQuery;

class PhotoDataApiGetter implements PhotoInterface
{
    /** @var NicheQuery $nicheQuery */
    private $nicheQuery;

    public function __construct(NicheQuery $nicheQuery)
    {
        $this->nicheQuery = $nicheQuery;
    }

    public function get(SenderProfile $senderProfile)
    {
        try {
            $query = $this->makeQuery($senderProfile);
            $userPwd = $this->getUserPwd();
            $data = $this->getDataFromPhotoApi($query, $userPwd);
            $image = $this->getImageByUrl($data->data->photoUrl, $userPwd);

            $photoName = \Yii::$app->senderPhoto->getSubDir($data->data->photoName) . $data->data->photoName;
            $senderPhoto = $this->savePhotoToDB($photoName, $senderProfile);
            $pathToSave = \Yii::$app->senderPhoto->getBasePath() . $photoName;
            file_put_contents($pathToSave, $image);
        } catch (\Throwable $e) {
            throw new \Exception($e->getTraceAsString());
        }

        return $senderPhoto->id;
    }

    private function savePhotoToDB(string $photoName, SenderProfile $senderProfile)
    {
        $senderPhoto = new SenderPhoto();
        $senderPhoto->name = $photoName;
        $senderPhoto->birthdayYear = date('Y', strtotime($senderProfile->birthday));
        $senderPhoto->birthdayMonth = date('m', strtotime($senderProfile->birthday));
        $senderPhoto->birthdayDay = date('d', strtotime($senderProfile->birthday));
        if (!$senderPhoto->save()) {
            throw new \Exception('Error save senderPhoto');
        }

        $senderPhoto->refresh();

        $senderPhotoNiche = new SenderPhotoNiche();
        $senderPhotoNiche->photoId = $senderPhoto->id;
        $senderPhotoNiche->niche = $this->nicheQuery->getNiche($senderProfile);
        if (!$senderPhotoNiche->save()) {
            throw new \Exception('Error save SenderPhotoNiche');
        }

        return $senderPhoto;
    }

    private function getDataFromPhotoApi(array $query, string $userPwd)
    {
        $curl = new Curl();
        $dataJson = $curl->setOption(CURLOPT_POSTFIELDS, $query)
            ->setOption(CURLOPT_HEADER, 1)
            ->setOption(CURLOPT_USERPWD, $userPwd)
            ->setOption(CURLOPT_CONNECTTIMEOUT, 30)
            ->setOption(CURLOPT_TIMEOUT, 30)
            ->setOption(CURLOPT_SSL_VERIFYHOST, 0)
            ->setOption(CURLOPT_SSL_VERIFYPEER, 0)
            ->post(Constant::TUGGED_GETTING_PHOTO_API);

        $data = json_decode($dataJson);
        if (empty($data->code) || $data->code == 200) {
            throw new \Exception('Empty response in photo data api');
        }

        return $data;
    }

    private function getImageByUrl(string $photoUrl, string $userPwd)
    {
        $curl = new Curl();

        return $curl->setOption(CURLOPT_HEADER, 1)
            ->setOption(CURLOPT_USERPWD, $userPwd)
            ->setOption(CURLOPT_SSL_VERIFYHOST, 0)
            ->setOption(CURLOPT_SSL_VERIFYHOST, 0)
            ->setOption(CURLOPT_FOLLOWLOCATION, 1)
            ->setOption(CURLOPT_ENCODING, '')
            ->get($photoUrl);
    }



    private function getUserPwd()
    {
        return Constant::TUGGED_GETTING_PHOTO_API_USER
            . ':' . Constant::TUGGED_GETTING_PHOTO_API_PWD;
    }

    private function makeQuery(SenderProfile $senderProfile)
    {
        $niche = $this->nicheQuery->getNiche($senderProfile);
        $age = date('Y') - date('Y', strtotime($senderProfile->birthday));
        $ageFrom = $age - 5;
        if ($ageFrom < 18) {
            $ageFrom = 18;
        }
        $ageTo = $age + 5;

        return [
            'ageFrom' => $ageFrom,
            'ageTo' => $ageTo,
            'niche' => $niche
        ];
    }
}
