<?php

namespace sender\registration\components\generate;

use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileRegister;

class RegistrationCreator
{
    public function create(SenderProfile $profile, SenderGenerationParams $data)
    {
        $profileRegister                 = new SenderProfileRegister();
        $profileRegister->profileId      = $profile->id;
        $profileRegister->timeToRegister = date('Y-m-d H:i:s', time() + rand(180, 600));
        $profileRegister->ruleId         = $data->ruleId;
        $profileRegister->save();
    }
}