<?php

namespace sender\registration\components\generate;

use app\models\phoenix\SenderProfileAttributes;
use app\models\phoenix\SenderProfile;

class AttributeCreator
{
    public function create(SenderGenerationParams $data, SenderProfile $profile)
    {
        $model               = new SenderProfileAttributes();
        $model->id           = $profile->id;
        $model->height       = $data->height ?? 0;
        $model->weight       = $data->weight ?? 0;
        $model->build        = $data->build ?? 0;
        $model->race         = $data->race ?? 0;
        $model->eyeColor     = $data->eyeColor ?? 0;
        $model->hairColor    = $data->hairColor ?? 0;
        $model->familyStatus = $data->familyStatus ?? 0;
        $model->glasses      = $data->glasses ?? 0;
        $model->religion     = $data->religion ?? 0;
        $model->save();
    }
}