<?php

namespace sender\registration\components\generate;


class Randomizer
{
    private $maxLenString = 5;

    public function getRandomString()
    {
        $all = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        $string = '';

        $cnt = strlen($all) - 1;

        srand((int)(microtime()*1000000));
        for($i=0; $i < $this->maxLenString; $i++) {
            $string .= $all[rand(0, $cnt)];
        }

        return $string;
    }

    public function getRandomDate($yearFrom = 18, $yearTo = 25)
    {
        return mt_rand(strtotime("- $yearTo year"), strtotime("- $yearFrom year"));
    }

    public function getRandParamKey($param)
    {
        $data      = \Yii::$app->yaml->parse($param);
        $randomKey = array_rand($data);

        return $randomKey;
    }

    public function getRandParamByKey($param, $key)
    {
        $data      = \Yii::$app->yaml->parseByKey($param, $key);
        $randomKey = array_rand($data);

        return $data[$randomKey];
    }

    public function getHex($start = 0, $end = 8)
    {
        return substr(md5(microtime() . rand(1, 100)), $start, $end);
    }
}