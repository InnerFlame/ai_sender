<?php

namespace sender\registration\components\generate;

use app\models\phoenix\ProxyIp;
use app\models\phoenix\SenderProfile;
use yii\base\ErrorException;
use yii\db\Query;
use app\models\phoenix\GeoIp as GeoIpModel;

class GeoIp
{
    public function get($country)
    {
        $geoIpData = $this->getGeoIpData($country);

        if (empty($geoIpData)) {
            throw new ErrorException('Not found geo ip, try again');
        }

        $geoIp = long2ip(rand($geoIpData['ipStart'], $geoIpData['ipEnd']));

        return $geoIp;
    }

    private function getGeoIpData($country)
    {
        $query = (new Query())
            ->select('min(rnd) as min, max(rnd) as max')
            ->from(GeoIpModel::tableName())
            ->where(['country_code3' => mb_strtolower($country)]);

        $rndMinMax = $query->one();
        $rand      = rand($rndMinMax['min'], $rndMinMax['max']);

        $query     = (new Query())
            ->select('g.id, g.ip_start as ipStart, g.ip_end as ipEnd')
            ->from(GeoIpModel::tableName() . ' as g')
            ->leftJoin(
                ProxyIp::tableName() . ' as pi',
                '(pi.ipStart < g.ip_start and g.ip_start < pi.ipEnd) or (pi.ipStart < g.ip_end and g.ip_end < pi.ipEnd)'
            )
            ->where([
                'g.country_code3' => mb_strtolower($country),
                'g.rnd'           => $rand,
            ])
            ->andWhere('pi.id is NULL')
            ->limit(1);

        $result = $query->one();

        return $result;
    }

    private function hasIp($ip)
    {
        $query = (new Query())->select('id')
            ->from(SenderProfile::tableName())
            ->where(['ip' => $ip]);

        $id = $query->one();

        return $id;
    }
}