<?php

namespace sender\registration\components\generate;

use app\models\phoenix\SenderPhoto;
use app\models\phoenix\SenderPhotoNiche;
use app\models\phoenix\SenderProfile;
use sender\niche\components\NicheQuery;
use yii\base\ErrorException;
use yii\db\Expression;

class Photo implements PhotoInterface
{
    /** @var NicheQuery $nicheQuery */
    private $nicheQuery;

    public function __construct(NicheQuery $nicheQuery)
    {
        $this->nicheQuery = $nicheQuery;
    }

    public function get(SenderProfile $senderProfile)
    {
        if ($senderProfile->userType == SenderProfile::TYPE_OBSERVER) {
            return null;
        }

        $niche        = $this->nicheQuery->getNiche($senderProfile);
        $birthdayYear = date('Y', strtotime($senderProfile->birthday)) - 5;
        for ($i = 0; $i < 10; $i++) {
            /** @var SenderPhoto $photo */
            $photo = SenderPhoto::find()
                ->from(SenderPhoto::tableName() . ' as p')
                ->innerJoin(SenderPhotoNiche::tableName() . ' as ph', 'p.id=ph.photoId')
                ->where([
                    'p.isBad'        => 0,
                    'p.isUsed'       => 0,
                    'p.birthdayYear' => $birthdayYear,
                    'ph.niche'       => $niche
                ])
                ->orderBy(new Expression('rand()'))
                ->one();

            if (!empty($photo)) {
                $photo->isUsed = 1;
                $photo->save();

                return $photo->id;
            }

            $birthdayYear++;
        }

        throw new ErrorException(
            'Error to attach photo to profile (All photos used for '
            . $niche . ' - ' . $senderProfile->birthday . ')'
        );
    }
}
