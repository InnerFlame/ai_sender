<?php

namespace sender\registration\components\generate;

use yii\base\ErrorException;
use app\models\phoenix\SenderProfile;
use Ramsey\Uuid\Uuid;

class SenderGenerator
{
    const PHOENIX_APP_MARKER = 'hand3856be45';

    /** @var string[] $countries */
    public $countries = ['AF', 'AL', 'DZ', 'AS'];

    /** @var ScreenNameGenerator */
    private $screenName;

    /** @var AttributeCreator */
    private $attributeCreator;

    /** @var  GeoIp */
    private $geoIp;

    /** @var  PhotoInterface */
    private $photo;

    /** @var  BirthdayGenerator */
    private $birthday;

    /** @var  Randomizer */
    private $randomizer;

    /** @var  CityData */
    private $cityData;

    /** @var  EmailGenerator */
    private $emailGenerator;

    /** @var  SiteGenerator */
    private $siteGenerator;

    /** @var  RegistrationCreator */
    private $registrationCreator;

    public function __construct(
        ScreenNameGenerator $screenName,
        AttributeCreator $attributeCreator,
        GeoIp $geoIp,
        PhotoInterface $photo,
        BirthdayGenerator $birthday,
        Randomizer $randomizer,
        CityData $cityData,
        EmailGenerator $emailGenerator,
        SiteGenerator $siteGenerator,
        RegistrationCreator $registrationCreator
    ) {
        $this->screenName          = $screenName;
        $this->attributeCreator    = $attributeCreator;
        $this->geoIp               = $geoIp;
        $this->photo               = $photo;
        $this->birthday            = $birthday;
        $this->randomizer          = $randomizer;
        $this->cityData            = $cityData;
        $this->emailGenerator      = $emailGenerator;
        $this->siteGenerator      = $siteGenerator;
        $this->registrationCreator = $registrationCreator;
    }

    /**
     * @param SenderGenerationParams $data
     * @return int
     * @throws \yii\db\Exception
     */
    public function createSender(SenderGenerationParams $data)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $profile = $this->createSenderProfiles($data);
            $this->registrationCreator->create($profile, $data);
            $transaction->commit();
            $id = $profile->id;
        } catch (ErrorException $e) {
            \Yii::warning($e->getMessage());
            $transaction->rollBack();
            $id = 0;
        }

        return $id;
    }

    private function createSenderProfiles(SenderGenerationParams $data, int $totalCountProfiles = 0)
    {
        $profile            = $this->generateMinimalProfile($data->country);
        $profile->site      = $this->siteGenerator->get($data);
        $profile->country   = $data->country;
        $profile->language  = $data->language;
        $profile->sexuality = $data->sexuality;
        $profile->gender    = $data->gender;
        $profile->userType  = $data->userType;
        $profile->birthday  = $this->birthday->get($data, $totalCountProfiles);
        $profile->photo     = $this->photo->get($profile);
        $profile->email     = $this->emailGenerator->getEmail();

        $profile->save();
        $this->attributeCreator->create($data, $profile);

        return $profile;
    }

    public function generateMinimalProfile(string $country): SenderProfile
    {
        $cityData                          = $this->cityData->get($country);
        $profile                           = new SenderProfile();
        $profile->marker                   = self::PHOENIX_APP_MARKER;
        $profile->actionWay                = SenderProfile::ACTION_WAY_DEFAULT;
        $profile->firstName                = $this->screenName->getFirstName();
        $profile->lastName                 = $this->screenName->getSecondName();
        $profile->gtmClientId              = (string)Uuid::uuid4();
        $profile->version                  = $this->randomizer->getRandParamKey('versions');
        $profile->internalDeviceIdentifier = $this->randomizer->getHex() . $this->randomizer->getHex();
        $profile->deviceIdHex              = "00000000" . $this->randomizer->getHex()
            . "ffffffff" . $this->randomizer->getHex();
        $profile->password                 = $this->randomizer->getHex(0, 12);
        $profile->screenName               = $this->screenName->getScreenName();
        $profile->state                    = $cityData['state'];
        $profile->city                     = $cityData['city'];
        $profile->ip                       = $this->geoIp->get($country);
        return $profile;
    }
}
