<?php

namespace sender\registration\components;

use app\models\phoenix\CommunicationContainer;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileRegister;
use sender\communication\components\containerManager\ContainerManager;
use sender\niche\components\NicheQuery;
use sender\registration\components\generate\SenderGenerationParams;
use sender\registration\components\generate\SenderGenerator;
use sender\registration\models\RulesRegistration;
use yii\db\Query;

/**
 * Component to actualize profile`s registrations
 */
class RegistrationActualize
{
    const STOP_COMM_COUNT     = 80;
    const INTERVAL_LIMIT_TIME = 10800; #3h
    const MAX_REGS_IN_DAY     = 6000;

    /** @var SenderGenerator */
    private $senderGenerator;
    /** @var ContainerManager */
    private $containerManager;
    /** @var array */
    private $registrationSettings;
    /** @var array */
    private $defaultLanguages;
    /** @var NicheQuery */
    private $nicheQuery;


    public function __construct(
        SenderGenerator $senderGenerator,
        ContainerManager $containerManager,
        NicheQuery $nicheQuery,
        array $registrationSettings,
        array $defaultLanguages
    ) {
        $this->senderGenerator = $senderGenerator;
        $this->containerManager = $containerManager;
        $this->nicheQuery = $nicheQuery;
        $this->registrationSettings = $registrationSettings;
        $this->defaultLanguages = $defaultLanguages;
    }

    public function actualize()
    {
        $calculatedRegisterCounts = $this->getCalculateRegisterCounts();

        $limitRegisterCountByInterval = $this->getLimitRegisterCountByInterval();
        $scheduledRegisterCountByInterval = $this->getScheduledRegisterCountByInterval();

        $canToRunRegisterCountsByInterval = $limitRegisterCountByInterval - $scheduledRegisterCountByInterval;
        if ($canToRunRegisterCountsByInterval <= 0) {
            return;
        }

        foreach ($calculatedRegisterCounts as $country => $requiredRegisterCounts) {
            $requiredCount = [];

            if ($canToRunRegisterCountsByInterval <= 0) {
                break;
            }

            if (!isset($this->registrationSettings[$country]['niche'])) {
                $this->registrationSettings[$country]['niche'] = [null => 100];
            }
            if (!isset($this->registrationSettings[$country]['language'])) {
                $this->registrationSettings[$country]['language'] = [$this->defaultLanguages[$country] => 100];
            }
            foreach ($this->registrationSettings[$country]['niche'] as $niche => $nichePercent) {
                foreach ($this->registrationSettings[$country]['language'] as $language => $languagePercent) {
                    $requiredCount[] = [
                        'params' => ['niche' => $niche, 'language' => $language],
                        'neededInPool' => $requiredRegisterCounts * $nichePercent * $languagePercent / 10000];
                }
            }

            foreach ($requiredCount as $registrationRule) {
                $language = $registrationRule['params']['language'];
                $niche = $registrationRule['params']['niche'];

                $currentCount = $this->getExistingCountSenders($country, $language, $niche);
                if ($registrationRule['neededInPool'] > $currentCount) {
                    $neededToReg = $registrationRule['neededInPool'] - $currentCount;
                    if ($neededToReg >= $canToRunRegisterCountsByInterval) {
                        $neededToReg = $canToRunRegisterCountsByInterval;
                    }
                    $registrationRule = $this->getRegistrationRule($country, $language, $niche);
                    $this->generateNewSenders($neededToReg, $registrationRule);
                    $canToRunRegisterCountsByInterval -= $neededToReg;
                }
            }
        }
    }

    public function getCalculateRegisterCounts(): array
    {
        $containersSettings = CommunicationContainer::getActiveData();

        $calculateClientsCount = [];
        foreach ($containersSettings as $row) {
            $calculateClientsCount[$row['country']] = $row['countStarted'] * self::STOP_COMM_COUNT;
        }

        return $calculateClientsCount;
    }

    private function getRegistrationRule(string $country, string $language, string $niche = null): array
    {
        if ($niche == 'general') {
            $niche = null;
        }
        return RulesRegistration::find()
            ->where([
                'isActive' => 1,
                'userType' => SenderProfile::TYPE_SENDER,
                'country' => $country,
                'language' => $language,
                'niche' => $niche
            ])
            ->groupBy('country')
            ->indexBy('country')
            ->all();
    }

    private function getScheduledRegisterCount(string $country, string $language): Query
    {
        return $this->getMainQueryForScheduledRegisterCount()
            ->select(['count(sp.id) AS count', 'sp.country AS country'])
            ->andWhere(['country' => $country])
            ->andWhere(['language' => $language]);
    }

    private function getScheduledRegisterCountByInterval(): int
    {
        return $this->getMainQueryForScheduledRegisterCount()
            ->select(['sp.id'])
            ->andWhere(['<', 'timeToRegister', date("Y-m-d H:i:s", strtotime('+3 hours'))])
            ->andWhere(['>', 'timeToRegister', date("Y-m-d H:i:s")])
            ->count();
    }

    private function getLimitRegisterCountByInterval(): int
    {
        return self::MAX_REGS_IN_DAY / 24 * 3; #by 3 hours
    }

    private function getMainQueryForScheduledRegisterCount()
    {
        return (new Query())
            ->from(SenderProfileRegister::tableName() . ' as spr')
            ->innerJoin(SenderProfile::tableName() . ' as sp', 'spr.profileId = sp.id')
            ->andWhere("registerStartedAt = '0000-00-00 00:00:00'");
    }

    private function generateNewSenders(
        int $needToRun,
        RulesRegistration $registrationRule
    ): void {
        while ($needToRun > 0) {
            $senderGeneration = SenderGenerationParams::getSenderDataForRegisterRuleGeneration(
                $registrationRule
            );

            $senderGeneration->timeToRegister = date(
                'Y-m-d H:i:s',
                time() + rand(0, self::INTERVAL_LIMIT_TIME)
            );
            $this->senderGenerator->createSender($senderGeneration);
            --$needToRun;
        }
    }

    private function getExistingCountSenders(string $country, string $language, string $niche = null): int
    {
        return $this->getReadySenders($country, $language, $niche) +
            $this->getScheduledRegistrationCountWithNiche($country, $language, $niche);
    }

    private function getReadySenders(string $country, string $language, string $niche = null): int
    {
        $getReadySendersQuery = $this->containerManager->getReadySendersByCountryQuery($country, $language);
        $this->nicheQuery->addToQuery($getReadySendersQuery, $niche, SenderProfile::tableName());
        return $getReadySendersQuery->count();
    }

    private function getScheduledRegistrationCountWithNiche(
        string $country,
        string $language,
        string $niche = null
    ): int {
        $scheduledCountQuery = $this->getScheduledRegisterCount($country, $language);
        $this->nicheQuery->addToQuery($scheduledCountQuery, $niche, SenderProfile::tableName());
        return $scheduledCountQuery->count();
    }
}
