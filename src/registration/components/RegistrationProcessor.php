<?php

namespace sender\registration\components;

use app\config\Constant;
use app\exception\NoUserAgentsException;
use app\models\phoenix\ReUploadPhotoScheduler;
use app\models\phoenix\SearchProfile;
use sender\interaction\components\InteractionClient;
use sender\registration\models\RulesRegistration;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileRegister;
use app\models\rpc\Container;
use yii\db\Query;

/**
 * Component to process profile`s registrations
 */
class RegistrationProcessor
{
    /** @var string $target */
    protected $target = 'phoenix';
    /** @var  LocationCorrecter */
    private $locationCorrecter;
    /** @var InteractionClient $interactionClient */
    private $interactionClient;

    public function __construct(InteractionClient $interactionClient, LocationCorrecter $locationCorrecter)
    {
        $this->interactionClient = $interactionClient;
        $this->locationCorrecter = $locationCorrecter;
    }

    public function createRpcContainersForScenarios(int $limit = 10)
    {
        $profilesToRegister = $this->getProfilesReadyToRegister($limit);

        //No ready profiles - nothing to do here
        if (empty($profilesToRegister)) {
            return false;
        }

        foreach ($profilesToRegister as $profileToRegister) {
            if (\Yii::$container->get('interactionRouter')
                ->isSeparatedInteraction($profileToRegister->phoenixSenderProfile)
            ) {
                \Yii::$app->rpcMethods->createRpcContainer(
                    $profileToRegister->phoenixSenderProfile,
                    Container::TYPE_REGISTRATION,
                    Constant::TARGET_PHOENIX
                );
                sleep(20);
            }
        }

        return true;
    }

    /**
     * Select ready to registration profiles and call API to register them
     * @return bool
     */
    public function processRegistrationScenarios(int $limit = 10)
    {
        /** @var SenderProfileRegister[] $profilesToRegister */
        $profilesToRegister = $this->getProfilesReadyToRegister($limit);

        //No ready profiles - nothing to do here
        if (empty($profilesToRegister)) {
            return false;
        }
        foreach ($profilesToRegister as $profileToRegister) {
            /** @var SenderProfile $senderProfile */
            $senderProfile = $profileToRegister->phoenixSenderProfile;
            if (\Yii::$container->get('interactionRouter')->isSeparatedInteraction($senderProfile)) {
                $rpcContainer = \Yii::$app->rpcClient->getRpcContainer(
                    $profileToRegister->phoenixSenderProfile,
                    $this->target
                );
                if (empty($rpcContainer) || $rpcContainer->status != Container::STATUS_STARTED) {
                    //can`t start registration while client still not started
                    continue;
                }
                if ($this->callRegistrationApi($rpcContainer, $profileToRegister)) {
                    $profileToRegister->registerStartedAt = date('Y-m-d H:i:s');
                    $profileToRegister->save();
                }
                sleep(1);
            } else {
                $data = $this->getDataForRegistration($senderProfile);
                $result = $this->interactionClient->register($senderProfile, $data);

                if (!empty($result) && $result['clientStatus'] == 'connected') {
                    $profileToRegister->registeredAt = date('Y-m-d H:i:s');
                    $profileToRegister->save();

                    $senderProfile->originId     = $result['phoenixId'];
                    $senderProfile->accessToken  = $result['access_token'];
                    $senderProfile->refreshToken = $result['refresh_token'];
                    $senderProfile->location     = $this->locationCorrecter->correct($result['location']);
                    $senderProfile->pausedAt     = date('Y-m-d H:i:s');
                    $senderProfile->save();

                    if ($senderProfile->userType == SenderProfile::TYPE_SEARCHER) {
                        $searchProfile             = new SearchProfile();
                        $searchProfile->observerId = $senderProfile->id;
                        $searchProfile->save();
                    }

                    $reUpload                 = new ReUploadPhotoScheduler();
                    $reUpload->senderId       = $senderProfile->id;
                    $reUpload->timeToReUpload = date('Y-m-d H:i:s', time() + rand(180, 300));
                    $reUpload->save();
                } else {
                    $profileToRegister->errorAt  = date('Y-m-d H:i:s');
                    $profileToRegister->save();
                }
            }
        }

        return true;
    }

    /**
     * Select ready to registration profiles
     * @return array
     */
    private function getProfilesReadyToRegister(int $limit = 10)
    {
        $activeRuleIds = (new Query())
            ->select(['id'])
            ->from(RulesRegistration::tableName())
            ->where(['isActive' => true])
            ->column();

        $activeRuleIds[] = 0;

        $profilesToRegister = SenderProfileRegister::find()
            ->joinWith('phoenixSenderProfile')
            ->where('timeToRegister < :timeToRegister', ['timeToRegister' => date('Y-m-d H:i:s')])
            ->andWhere("registerStartedAt = '0000-00-00 00:00:00'")
            ->andWhere(['in', 'ruleId', $activeRuleIds])
            ->limit($limit)
            ->all();

        return $profilesToRegister;
    }

    /**
     * Call API to register profiles by data
     */
    private function callRegistrationApi(Container $rpcContainer, SenderProfileRegister $registrationData)
    {
        /** @var SenderProfile $senderProfile */
        $senderProfile = $registrationData->phoenixSenderProfile;
        $data = $this->getDataForRegistration($senderProfile);

        $additional = [
            'target'     => $this->target,
            'clientName' => $rpcContainer->name,
            'profileId'  => $senderProfile->id,
        ];

        \Yii::$app->rpcClient->rpcCall('client.' . $rpcContainer->name, 'register', [$data], $additional);

        Container::updateLastTimeActivity($rpcContainer->name, Constant::TARGET_PHOENIX);

        return true;
    }

    private function getDataForRegistration(SenderProfile $senderProfile): array
    {
        $language      = $senderProfile->language;
        $country       = $senderProfile->country;
        $proxy         = \Yii::$app->proxyManager->getProxy($senderProfile, $this->target);

        if (is_null($senderProfile->userAgent)) {
            try {
                $userAgentId = \Yii::$app->userAgentManager->getUserAgentId();
                $senderProfile->userAgent = $userAgentId;
            } catch (NoUserAgentsException $e) {
                \Yii::warning('Did not find user-agent for - ' . $senderProfile->id);
                return [];
            }
        }

        $userAgent = \Yii::$app->userAgentManager->getUserAgentById($senderProfile->userAgent);
        $userAgent = str_replace(['$(language)', '$(country)'], [$language, $country], $userAgent);
        if (empty($senderProfile->ip)) {
            for ($i = 0; $i <= 5; $i++) {
                $ip = \Yii::$app->proxyManager->getIpByProxy($proxy);

                if (!$this->isIp($ip) || $this->hasIp($ip)) {
                    $proxy = \Yii::$app->proxyManager->getProxy($senderProfile, $this->target);
                    $ip = null;
                } else {
                    break;
                }
            }

            if (empty($ip)) {
                \Yii::warning('Did not find the unique ip for - ' . $senderProfile->id);
                return [];
            }

            $senderProfile->ip = $ip;
        }

        for ($i = 0; $i <= 5; $i++) {
            $proxy      = \Yii::$app->proxyManager->getProxy($senderProfile, $this->target);
            $timeOffset = \Yii::$app->geoIp->getTimezoneByIp($senderProfile->ip, $proxy);
            if ($timeOffset !== false) {
                $senderProfile->timeOffset = $timeOffset;
                break;
            }
        }

        $senderProfile->save();

        if (is_null($senderProfile->timeOffset)) {
            \Yii::warning('Did not find the timeOffset for - ' . $senderProfile->id);
            return [];
        }

        $data = [
            'id'                       => $senderProfile->id,
            'profileId'                => $senderProfile->id,
            'host'                     => \Yii::$app->yaml->parseByKey('site', $senderProfile->site)['name'],
            'email'                    => $senderProfile->email,
            'password'                 => $senderProfile->password,
            'birthDate'                => $senderProfile->birthday,
            'deviceIdHex'              => $senderProfile->deviceIdHex,
            'internalDeviceIdentifier' => $senderProfile->internalDeviceIdentifier,
            'gtmClientId'              => $senderProfile->gtmClientId,
            'userAgent'                => $userAgent,
            'proxy'                    => $proxy,
            'ip'                       => $senderProfile->ip,
            'locale'                   => [
                'language' => $language,
                'country'  => $country,
            ],
            'app'                      => [
                'bundle'  => \Yii::$app->yaml->parseByKey('bundles', $senderProfile->site),
                'version' => \Yii::$app->yaml->parseByKey('versions', $senderProfile->version),
                'marker'  => $senderProfile->marker,
            ],
            'referrer'                 => 'utm_source%3Dgoogle-play%26utm_medium%3Dorganic',
            'screenname'               => $senderProfile->screenName,
            'attributes'               => $senderProfile->getSenderProfileAttributesForRegistration(),
        ];

        return $data;
    }

    private function hasIp(string $ip)
    {
        $query = (new Query())->select('id')
            ->from(SenderProfile::tableName())
            ->where(['ip' => $ip]);

        $id = $query->one();

        return $id;
    }

    private function isIp(string $ip)
    {
        if (!filter_var($ip, FILTER_VALIDATE_IP) === false) {
            return true;
        }

        return false;
    }

    public function activateProfileAfterRegister()
    {
        $query = (new Query())->select('sp.id')
            ->from(SenderProfileRegister::tableName() . ' as spr')
            ->innerJoin(SenderProfile::tableName() . ' as sp', 'spr.profileId=sp.id')
            ->where(
                'spr.registeredAt >= :timeStart',
                [
                    'timeStart' => date('Y-m-d H:i:s', time() - 266400)
                ]
            )
            ->andWhere(
                'spr.registeredAt <= :timeEnd',
                [
                    'timeEnd' => date('Y-m-d H:i:s', time() - 259200)
                ]
            )
            ->andWhere('sp.pausedAt > "0000-00-00 00:00:00"');

        $senderIds = $query->column();

        \Yii::$app->db->createCommand()->update(
            SenderProfile::tableName(),
            ['pausedAt' => '0000-00-00 00:00:00'],
            ['id' => $senderIds]
        )->execute();
    }
}
