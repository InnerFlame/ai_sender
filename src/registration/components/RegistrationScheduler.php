<?php

namespace sender\registration\components;

use sender\registration\components\generate\SenderGenerationParams;
use sender\registration\components\generate\SenderGenerator;
use sender\registration\models\RulesRegistration;
use app\models\phoenix\SenderProfileRegister;
use yii\db\Query;

class RegistrationScheduler
{
    const LIMIT_COUNT_HOUR_CREATED = 100;

    /** @var SenderGenerator  */
    private $senderGenerator;

    public function __construct(SenderGenerator $senderGenerator)
    {
        $this->senderGenerator = $senderGenerator;
    }

    public function create()
    {
        $registrationRules = $this->getRegistrationRules();
        /** @var RulesRegistration $registrationRule */
        foreach ($registrationRules as $registrationRule) {
            if ($this->hasRuleDone($registrationRule)) {
                $this->ruleHasDone($registrationRule);
                continue;
            }

            $countHourCreated = $this->getCountHourCreated($registrationRule);
            if ($countHourCreated >= self::LIMIT_COUNT_HOUR_CREATED) {
                continue;
            }

            $time             = time();
            $senderGeneration = SenderGenerationParams::getSenderDataForRegisterRuleGeneration($registrationRule);
            for ($i = $countHourCreated; $i < self::LIMIT_COUNT_HOUR_CREATED; $i++) {
                $senderGeneration->timeToRegister = date('Y-m-d H:i:s', $time);
                if ($this->senderGenerator->createSender($senderGeneration)) {
                    $time += rand(60, 180);
                    $registrationRule->countCreated++;
                    $registrationRule->save();
                }
            }
        }
    }

    private function getCountHourCreated(RulesRegistration $registrationRule)
    {
        return (new Query())->select('count(id)')
            ->from(SenderProfileRegister::tableName())
            ->where(['ruleId' => $registrationRule->id])
            ->andWhere(['>=', 'createdAt', date('Y-m-d H:00:00')])
            ->andWhere(['<=', 'createdAt', date('Y-m-d H:59:59')])
            ->scalar();
    }

    private function getRegistrationRules()
    {
        return RulesRegistration::find()
            ->where([
                'isActive' => 1,
                'hasDone'  => 0,
            ])->all();
    }

    private function hasRuleDone(RulesRegistration $registrationRule): bool
    {
        $countCreated = (new Query())->select('count(id)')
            ->from(SenderProfileRegister::tableName())
            ->where(['ruleId' => $registrationRule->id])
            ->scalar();

        if ($countCreated >= $registrationRule->profilesCount) {
            return true;
        }

        return false;
    }

    public function ruleHasDone(RulesRegistration $registrationRule)
    {
        $registrationRule->hasDone = 1;
        $registrationRule->save();
    }
}
