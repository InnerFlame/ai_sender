<?php

namespace sender\registration\components\fetch;

use sender\registration\models\SenderFetchSchedule;

class FetchScheduler
{
    public function schedule($senderId, $time = 600)
    {
        if ($this->isFetched($senderId)) {
            return;
        }

        $fetchSchedule              = new SenderFetchSchedule();
        $fetchSchedule->senderId    = $senderId;
        $fetchSchedule->timeToFetch = $this->getTimeToCheck($time);
        $fetchSchedule->save();

    }

    private function getTimeToCheck($time)
    {
        return date('Y-m-d H:i:s', time() + $time);
    }

    /**
     * @param $senderId
     * @return static
     */
    private function isFetched($senderId)
    {
        return SenderFetchSchedule::findOne(['senderId' => $senderId]);
    }
}
