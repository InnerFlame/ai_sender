<?php

namespace sender\registration\components\fetch;

use app\components\rpc\RpcMethodsComponent;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use app\models\rpc\Container;
use sender\registration\models\SenderFetchSchedule;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class FetchProcessor
{
    /** @var  RpcMethodsComponent $rpcMethods*/
    private $rpcMethods;

    const LIMIT = 50;

    public function __construct(RpcMethodsComponent $rpcMethods)
    {
        $this->rpcMethods = $rpcMethods;
    }

    public function processFetch()
    {
        $readyToFetch = $this->getReadyProfileToFetch();
        foreach ($readyToFetch as $item) {
            $senderProfile = SenderProfile::findOne($item['senderId']);
            $this->rpcMethods->createRpcContainer($senderProfile, Container::TYPE_FETCH);
        }

        if (!empty($readyToFetch)) {
            \Yii::$app->db->createCommand()->update(
                SenderFetchSchedule::tableName(),
                ['startedAt' => date('Y-m-d H:i:s')],
                ['id' => ArrayHelper::getColumn($readyToFetch, 'id')]
            )->execute();
        }
    }

    public function loginClient(SenderProfile $profile)
    {
        $this->rpcMethods->loginRpcClient($profile, Client::TYPE_FETCH);
        return true;
    }

    public function fetch(array $additionalData)
    {
        $data = [
            'id'          => '',
            'geo'         => '',
            'birthday'    => '',
        ];

        \Yii::$app->rpcClient->rpcCall('client.' . $additionalData['clientName'], 'fetch', [$data], $additionalData);
    }

    private function getReadyProfileToFetch()
    {
        return (new Query())
            ->select('id, senderId')
            ->from(SenderFetchSchedule::tableName())
            ->where(['<=', 'timeToFetch', date('Y-m-d H:i:s')])
            ->andWhere(['startedAt' => '0000-00-00 00:00:00'])
            ->limit(self::LIMIT)
            ->all();
    }
}
