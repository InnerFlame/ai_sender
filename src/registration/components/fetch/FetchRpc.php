<?php

namespace sender\registration\components\fetch;

use app\components\rpc\RpcClientComponent;
use app\models\phoenix\ReUploadPhotoScheduler;
use app\models\phoenix\SenderProfile;
use sender\registration\components\LocationCorrecter;
use sender\registration\models\SenderFetchSchedule;

class FetchRpc
{
    /** @var  RpcClientComponent $rpcMethods*/
    private $rpcClient;
    /** @var  LocationCorrecter $locationCorrecter */
    private $locationCorrecter;

    const LIMIT = 50;

    public function __construct(
        RpcClientComponent $rpcClient,
        LocationCorrecter $locationCorrecter

    )
    {
        $this->rpcClient         = $rpcClient;
        $this->locationCorrecter = $locationCorrecter;
    }

    public function processRpc($message)
    {
        if (empty($message['rpcCallInfo']['additional']['clientType'])
            || $message['rpcCallInfo']['additional']['clientType'] != 'fetch'
        ) {
            return;
        }

        $fetchSchedule = SenderFetchSchedule::findOne(['senderId' => $message['rpcCallInfo']['additional']['profileId']]);
        if ($fetchSchedule) {
            $sender = SenderProfile::findOne($message['rpcCallInfo']['additional']['profileId']);
            if ($sender) {
                if (strtotime($sender->birthday) == strtotime($message['incomingData']['result']['data']['birthday'])) {
                    $sender->originId = $message['incomingData']['result']['data']['id'];
                    $sender->location = $this->locationCorrecter->correct($message['incomingData']['result']['data']['geo']['city']);
                    $sender->save();

                    $reUpload                 = new ReUploadPhotoScheduler();
                    $reUpload->senderId       = $sender->id;
                    $reUpload->timeToReUpload = date('Y-m-d H:i:s', time() + 900);
                    $reUpload->save();
                }
            }
        }

        $this->rpcClient->stopRpcContainer($message['rpcCallInfo']['additional']['clientName']);
    }

}