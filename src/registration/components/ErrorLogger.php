<?php

namespace sender\registration\components;

use sender\registration\models\RegisterErrorLog;

class ErrorLogger
{
    public function log($senderId, $error)
    {
        $registerErrorLog           = new RegisterErrorLog();
        $registerErrorLog->senderId = $senderId;
        $registerErrorLog->error    = $error;
        $registerErrorLog->save();
    }
}