<?php

namespace sender\registration\components;

use app\components\rpc\RpcClientComponent;
use app\config\Constant;
use app\models\phoenix\SenderPhoto;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Container;
use sender\interaction\components\InteractionClient;
use yii\helpers\Url;

class PhotoUpload
{
    /** @var RpcClientComponent $rpcClient */
    private $rpcClient;
    /** @var InteractionClient $interactionClient */
    private $interactionClient;

    public function __construct(RpcClientComponent $rpcClient, InteractionClient $interactionClient)
    {
        $this->rpcClient = $rpcClient;
        $this->interactionClient = $interactionClient;
    }

    public function uploadPhoto(SenderProfile $senderProfile)
    {
        $photoData = $this->getPhotoData($senderProfile);
        if (empty($photoData)) {
            $this->rpcClient->stopRpcContainer('phoenix.' . $senderProfile->id);
            return;
        }

        $senderProfile->photoStatus     = SenderProfile::STATUS_UPLOAD;
        $senderProfile->photoUploadedAt = date('Y-m-d H:i:s');
        $senderProfile->save();

        $additional = [
            'profileId'  => $senderProfile->id,
            'target'     => Constant::TARGET_PHOENIX,
            'clientName' => 'phoenix.' . $senderProfile->id,
        ];

        Container::updateLastTimeActivity(
            Constant::TARGET_PHOENIX . $senderProfile->id,
            Constant::TARGET_PHOENIX
        );

        $this->rpcClient->rpcCall(
            'client.phoenix.' . $senderProfile->id,
            'photoUpload',
            [$photoData],
            $additional
        );
    }

    public function uploadInteractionPhoto(SenderProfile $senderProfile)
    {
        $photoData = $this->getPhotoData($senderProfile);
        if (empty($photoData)) {
            return;
        }

        $senderProfile->photoStatus     = SenderProfile::STATUS_UPLOAD;
        $senderProfile->photoUploadedAt = date('Y-m-d H:i:s');
        $senderProfile->save();

        $this->interactionClient->uploadPhoto($senderProfile, $photoData);
    }

    private function getPhotoData(SenderProfile $senderProfile): array
    {
        if (empty($senderProfile->photo)) {
            $senderProfile->changePhotoStatus(SenderProfile::STATUS_EMPTY_PHOTO_IN_SENDER);
            return [];
        }

        /** @var SenderPhoto $senderPhoto */
        $senderPhoto = SenderPhoto::findOne(['id' => $senderProfile->photo]);
        if (empty($senderPhoto)) {
            $senderProfile->changePhotoStatus(SenderProfile::STATUS_EMPTY_PHOTO_IN_SENDER);
            return [];
        }

        $photo = \Yii::$app->basePath . '/web' . SenderPhoto::PHOTO_BASE_PATH . $senderPhoto->name;
        if (!is_file($photo)) {
            $senderProfile->changePhotoStatus(SenderProfile::STATUS_EMPTY_PHOTO_IN_SENDER);
            return [];
        }

        try {
            $size = getimagesize($photo);
        } catch (\Throwable $e) {
            $senderProfile->changePhotoStatus(SenderProfile::STATUS_EMPTY_PHOTO_IN_SENDER);
            return [];
        }

        $data = [
            'url'      => Url::base() . SenderPhoto::PHOTO_BASE_PATH . $senderPhoto->name,
            'fileName' => str_replace('/', '', $senderPhoto->name) . '.jpg',
            'crop'     => [
                'x' => 0,
                'y' => 0,
                'w' => $size[0] ?? 100,
                'h' => $size[1] ?? 100,
            ]
        ];

        return $data;
    }
}
