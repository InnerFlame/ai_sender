<?php

namespace sender\registration\components;

class ProfilesFieldConverter
{
    //-- converters for remote data
    private static $hairColorLocalConverter = array(
        0  => 0,
        1  => 1,
        4  => 4,
        5  => 5,
        6  => 6,
        7  => 7,
        8  => 7,
        9  => 2,
        10 => 0
    );

    //-- converters for local data
    private static $hairColorRemoteConverter = array(
        0 => 0,
        1 => 1,
        4 => 4,
        5 => 5,
        6 => 6,
        7 => 7,
        2 => 9,
        3 => 9,
    );

    //-- converters for remote data
    private static $bodyTypeLocalConverter = array(
        7 => 0,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
        0 => 0,
    );

    //-- converters for local data
    private static $bodyTypeRemoteConverter = array(
        0 => 7,
        1 => 2,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
    );

    /**
     * ProfilesFieldConverter::convertRemoteToLocal()
     *
     * Convert remote value to local
     *
     * @param mixed $enumType
     * @param mixed $enumValue
     * @return mixed
     */
    public static function convertRemoteToLocal($enumType = "Uknown", $enumValue)
    {
        if(isset(self::${$enumType.'LocalConverter'})) {
            $result = isset(self::${$enumType.'LocalConverter'}[$enumValue])?
                               self::${$enumType.'LocalConverter'}[$enumValue]:0;
        } else {
            $result = $enumValue;
        }

        return $result;
    }

    /**
     * ProfilesFieldConverter::convertLocalToRemote()
     *
     * Convert local value to remote
     *
     * @param mixed $enumType
     * @param mixed $enumValue
     * @return mixed
     */
    public static function convertLocalToRemote($enumType = "Uknown", $enumValue)
    {
        if(isset(self::${$enumType.'RemoteConverter'})) {
            $result = isset(self::${$enumType.'RemoteConverter'}[$enumValue])?
                               self::${$enumType.'RemoteConverter'}[$enumValue]:0;
        } else {
            $result = $enumValue;
        }

        return $result;
    }
}