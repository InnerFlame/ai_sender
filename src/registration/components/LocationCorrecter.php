<?php

namespace sender\registration\components;

use app\models\phoenix\CorrectLocation;
use app\models\phoenix\SenderProfile;
use yii\db\Connection;

class LocationCorrecter
{
    /** @var Connection $db */
    private $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function correct($location)
    {
        $locations     = explode(',', $location);
        $rightLocation = null;
        foreach ($locations as $item) {
            if ((int)$item === 0) {
                $rightLocation = trim($item);
                break;
            }
        }

        if ($rightLocation === null || $rightLocation == '') {
            return null;
        }

        $correctLocation = CorrectLocation::findOne(['inCorrectName' => $rightLocation]);
        if (!empty($correctLocation)) {
            return $correctLocation->correctName;
        }

        return $rightLocation;
    }

    public function updateExistentSenders(CorrectLocation $correctLocation)
    {
        $this->db->createCommand()->update(
            SenderProfile::tableName(),
            ['location' => $correctLocation->correctName],
            ['location' => $correctLocation->inCorrectName]
        )->execute();
    }
}
