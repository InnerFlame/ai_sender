<?php

namespace sender\registration\components\rpcStrategy;

use app\components\rpc\RpcClientComponent;
use app\models\phoenix\SenderProfile;

class UploadedPhotoStrategy
{
    /** @var RpcClientComponent $rpcClient*/
    private $rpcClient;

    public function __construct(RpcClientComponent $rpcClient)
    {
        $this->rpcClient = $rpcClient;
    }

    public function processResult(array $incomingData)
    {
        $sender = SenderProfile::findOne($incomingData['rpcCallInfo']['additional']['profileId']);
        $sender->changePhotoStatus(SenderProfile::STATUS_UPLOADED);
        $this->rpcClient->stopRpcContainer('phoenix.' . $sender->id);
    }
}
