<?php

namespace sender\registration\components\rpcStrategy;

use app\models\phoenix\ReUploadPhotoScheduler;
use sender\registration\components\LocationCorrecter;
use app\models\phoenix\SearchProfile;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileRegister;

class RegisteredStrategy
{
    public function processResult(LocationCorrecter $locationCorrecter, array $incomingData)
    {
        $senderId = $incomingData['profileId'];
        $senderProfileRegister = SenderProfileRegister::findOne(['profileId' => $senderId]);
        /** @var SenderProfile $senderProfile */
        $senderProfile         = $senderProfileRegister->phoenixSenderProfile;

        if ($senderProfileRegister && $senderProfile) {
            $senderProfileRegister->registeredAt = date('Y-m-d H:i:s');
            $senderProfileRegister->save();

            $senderProfile->originId     = $incomingData['phoenixId'];
            $senderProfile->accessToken  = $incomingData['access_token'];
            $senderProfile->refreshToken = $incomingData['refresh_token'];
            $senderProfile->location     = $locationCorrecter->correct($incomingData['location']);
            $senderProfile->pausedAt     = date('Y-m-d H:i:s');
            $senderProfile->save();

            if ($senderProfile->userType == SenderProfile::TYPE_SEARCHER) {
                $searchProfile             = new SearchProfile();
                $searchProfile->observerId = $senderProfile->id;
                $searchProfile->save();
            }

            $reUpload                 = new ReUploadPhotoScheduler();
            $reUpload->senderId       = $senderProfile->id;
            $reUpload->timeToReUpload = date('Y-m-d H:i:s', time() + rand(180, 300));
            $reUpload->save();

            \Yii::$app->rpcClient->stopRpcContainer('phoenix.' . $senderProfile->id);
        }
    }
}
