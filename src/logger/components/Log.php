<?php

namespace sender\logger\components;
use app\models\phoenix\SenderProfile;
use Monolog\Logger;

class Log extends Logger
{
    /** @var array*/
    private $defaultContext;
    const LIFE_CYCLE_DEFAULT_CONTEXT = ['logCategory' => 'lifeCycle'];
    const EVA_DEFAULT_CONTEXT = ['logCategory' => 'evaMessage'];

    public function __construct(string $name, array $handlers = [], array $processors = [], array $defaultContext = [])
    {
        parent::__construct($name, $handlers, $processors);
        $this->defaultContext = $defaultContext;
    }

    /**
     * @param $message string
     * @param SenderProfile $profile
     * @param array $context
     */
    public function logSenderProfileInfo(string $message, SenderProfile $profile, array $context = [])
    {
        $profileInfo = [
            'senderId' => (int) $profile->id,
            'country' => $profile->country,
        ];
        $this->info($message, array_merge($profileInfo, $context));
    }
    /**
     * @param $message string
     * @param SenderProfile $profile
     * @param array $context
     */
    public function logSenderProfileErr(string $message, SenderProfile $profile, array $context = [])
    {
        $profileInfo = [
            'senderId' => (int) $profile->id,
            'country' => $profile->country,
        ];
        $this->error($message, array_merge($profileInfo, $context));
    }

    /**
     * {@inheritdoc}
     */
    public function addRecord($level, $message, array $context = [])
    {
        return parent::addRecord($level, $message, array_merge($this->defaultContext, $context));
    }
}
