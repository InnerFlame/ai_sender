<?php

namespace sender\logger\components;


use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use yii\helpers\VarDumper;
use yii\log\Logger;
use yii\log\Target;

class MonologTarget extends Target
{
    /** @var LoggerInterface */
    public $logger;
    /** @var array<int,string>[] */
    public $messages = [];
    /** @var array */
    private $levels = [
        Logger::LEVEL_ERROR => LogLevel::ERROR,
        Logger::LEVEL_WARNING => LogLevel::WARNING,
        Logger::LEVEL_INFO => LogLevel::INFO,
        Logger::LEVEL_TRACE => LogLevel::DEBUG,
        Logger::LEVEL_PROFILE => LogLevel::DEBUG,
        Logger::LEVEL_PROFILE_BEGIN => LogLevel::DEBUG,
        Logger::LEVEL_PROFILE_END => LogLevel::DEBUG,
        // Psr Levels
        LogLevel::EMERGENCY => LogLevel::EMERGENCY,
        LogLevel::ALERT => LogLevel::ALERT,
        LogLevel::CRITICAL => LogLevel::CRITICAL,
        LogLevel::ERROR => LogLevel::ERROR,
        LogLevel::WARNING => LogLevel::WARNING,
        LogLevel::NOTICE => LogLevel::NOTICE,
        LogLevel::INFO => LogLevel::INFO,
        LogLevel::DEBUG => LogLevel::DEBUG,
    ];
    public function export()
    {
        foreach ($this->messages as $message) {
            $level = $message[1];
            if (!isset($this->levels[$level])) {
                continue;
            }
            $context = [];
            if (isset($message[4])) {
                $context['trace'] = $message[4];
            }
            if (isset($message[5])) {
                $context['memory'] = $message[5];
            }
            if (isset($message[2])) {
                $context['category'] = $message[2];
            }
            $text = $message[0];
            if (!is_string($text)) {
                if ($text instanceof \Throwable || $text instanceof \Exception) {
                    $text = (string)$text;
                } else {
                    $text = VarDumper::export($text);
                }
            }

            $this->logger->log($this->levels[$level], $text, $context);
        }
    }
}
