<?php

namespace sender\niche\components;

use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileAttributes;
use yii\db\Query;

class NicheQuery
{
    private $niches = [
        'black'  => 'black',
        'asian'  => 'asian',
        'latino' => 'latino',
        'mature' => 'mature',
    ];

    public function getNiches()
    {
        return $this->niches;
    }

    public function addToQuery(Query $query, $niche, $tableAlias)
    {
        $methodCondition = $niche . 'ConditionParams';
        if (method_exists($this, $methodCondition)) {
            $query->innerJoin(SenderProfileAttributes::tableName() . ' as sa', $tableAlias . '.id=sa.id');
            foreach ($this->$methodCondition() as $condition) {
                $query->andWhere($condition);
            }
        }
    }

    public function addToQueryLeftLoinAndJoinCondition(Query $query, $niche, $tableAlias)
    {
        $methodCondition = $niche . 'ConditionParams';
        if (method_exists($this, $methodCondition)) {
            $nicheJoinConditionsArray = ['and', ['sa.id' => $tableAlias . '.id']];
            foreach ($this->$methodCondition() as $condition) {
                $nicheJoinConditionsArray[] = $condition;
            }

            $query->addSelect('sa.id AS isNicheSender');
            $query->leftJoin(SenderProfileAttributes::tableName() . ' as sa', $nicheJoinConditionsArray);
        }
    }

    public function getNiche(SenderProfile $sender)
    {
        foreach ($this->getNiches() as $niche) {
            $query = (new Query())->select('s.id')
                ->from(SenderProfile::tableName() . ' as s')
                ->where(['s.id' => $sender->id]);

            $this->addToQuery($query, $niche, 's');
            if ($query->one()) {
                return $niche;
            }
        }

        return 'general';
    }

    /*
     * CONDITION
     */
    public function matureConditionParams()
    {
        return [
            ['>=', 'birthday', date('Y') - 60 . '-01-01'],
            ['<=', 'birthday', date('Y') - 30 . '-01-01']
        ];
    }

    public  function blackConditionParams()
    {
        return [
            ['race' => 3]
        ];
    }

    public  function asianConditionParams()
    {
        return [
            ['race' => 2]
        ];
    }

    public  function latinoConditionParams()
    {
        return [
            ['race' => 6]
        ];
    }
}
