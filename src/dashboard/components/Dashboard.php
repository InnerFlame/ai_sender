<?php

namespace sender\dashboard\components;

use app\models\phoenix\CommunicationContainer;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\SenderEmail;
use app\models\phoenix\SenderPhoto;
use app\models\phoenix\SenderPhotoNiche;
use app\models\phoenix\SenderProfile;
use app\models\rpc\LifeCycleContainersInfo;
use sender\communication\components\containerManager\ContainerManager;
use sender\niche\components\NicheQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class Dashboard
{
    const YEARS_STEP = 5;

    /** @var array */
    private $countries;
    /** @var  ContainerManager $containerManager */
    private $containerManager;
    /** @var  NicheQuery $nicheQuery */
    private $nicheQuery;

    public function __construct(
        array $countries,
        ContainerManager $containerManager,
        NicheQuery $nicheQuery
    ) {
        $this->countries        = $countries;
        $this->containerManager = $containerManager;
        $this->nicheQuery       = $nicheQuery;
    }

    public function getNotUsedEmailsCount(): int
    {
        return SenderEmail::find()->where(['isUsed' => 0])->count();
    }

    public function getNotUsedPhotosCount(): array
    {
        $notUsedPhotosCount = (new Query())
            ->select('count(p.id) as count, birthdayYear, niche')
            ->from(SenderPhoto::tableName() . ' as p')
            ->innerJoin(SenderPhotoNiche::tableName() . ' as pn', 'p.id=pn.photoId')
            ->where([
                'isUsed' => 0,
                'isBad'  => 0,
            ])
            ->groupBy('birthdayYear, niche')
            ->orderBy('birthdayYear DESC')
            ->all();

        return $this->groupPhotoStats($notUsedPhotosCount);
    }

    private function groupPhotoStats(array $notUsedPhotosCount): array
    {
        $result             = [];
        $notUsedPhotosCount = ArrayHelper::index($notUsedPhotosCount, null, 'birthdayYear');
        foreach ($notUsedPhotosCount as $year => $notUsedPhotoCount) {
            $result[$year]['birthdayYear']       = $year;
            $result[$year]['notUsedPhotosCount'] = '';
            foreach ($notUsedPhotoCount as $item) {
                $result[$year]['notUsedPhotosCount'] .= sprintf('%s - %s ', $item['niche'], $item['count']);
            }
        }

        return $result;
    }

    public function getReadyClientsCounts(): array
    {
        $result   = [];
        $senders  = $this->containerManager->countReadySendersByCountry();
        foreach ($this->countries as $countryCode => $country) {
            $result[$countryCode] = [
                'country' => $country,
                'actionWayExternalCnt' => 0,
                'actionWayDefaultCnt' => 0,
            ];
        }
        foreach ($senders as $row) {
            $result[$row['country']]['actionWayDefaultCnt'] = $row['cnt'];
        }

        return $result;
    }

    public function getRunningClientsCounts(): array
    {
        $result                 = [];
        $containersSettings     = CommunicationContainer::getActiveData();
        $runningClients = LifeCycleContainersInfo::find()->all();
        foreach ($containersSettings as $row) {
            $result[$row['country']] = [
                'country' => $this->countries[$row['country']],
                'running' => ['initialAndAnswers' => 0, 'onlyAnswers' => 0],
                'limit'   => 0,
            ];
            $result[$row['country']]['limit'] = $row['countStarted'];
        }
        foreach ($runningClients as $row) {
            if (!isset($result[$row->country])) {
                continue;
            }
            $runningClientsData = json_decode($row->data, true);
            $result[$row->country]['running']['initialAndAnswers'] = count($runningClientsData['initialAndAnswers']);
            $result[$row->country]['running']['onlyAnswers']       = count($runningClientsData['onlyAnswers']);
        }

        return $result;
    }

    public function getRecipientsNichesData(): array
    {
        $result = [];
        $niches = \Yii::$app->yaml->parse('recipientNiches');

        foreach (array_keys($niches) as $niche) {
            $countInSystem = RecipientProfile::find()->where(['niche' => $niche])->count();

            $result[$niche] = [
                'niche'         => $niche,
                'countInSystem' => $countInSystem
            ];
        }

        $countGeneralInSystem = RecipientProfile::find()->where(['niche' => null])->count();
        $result['general'] = [
            'niche'         => 'general',
            'countInSystem' => $countGeneralInSystem
        ];

        return $result;
    }

    public function getSendersNichesData(): array
    {
        $result             = [];
        $niches             = \Yii::$container->get('nicheQuery')->getNiches();
        $lifecycleConfig    = \Yii::$app->yaml->parse('flexibleLifeCycle');

        foreach ($niches as $key => $niche) {
            $isLaunchedData = [];

            $queryCountInSystem = $this->getBaseQueryForSendersNichesData();
            $queryReadyToWork   = $this->getQueryForSendersNichesReadyToWork();

            $this->nicheQuery->addToQuery($queryCountInSystem, $niche, SenderProfile::tableName());
            $this->nicheQuery->addToQuery($queryReadyToWork, $niche, SenderProfile::tableName());

            $countInSystem = $queryCountInSystem->count();
            $readyToWork   = $queryReadyToWork->count();

            foreach ($lifecycleConfig['percentNiche'] as $country => $values) {
                if (isset($values[$niche])) {
                    $isLaunchedData[] = $country . '-' . $values[$niche] . '%';
                }
            }

            $result[$niche] = [
                'niche'         => $niche,
                'countInSystem' => $countInSystem,
                'readyToWork'   => $readyToWork,
                'isLaunched'    => empty($isLaunchedData) ? false : implode('/', $isLaunchedData)
            ];
        }

        return $result;
    }

    private function getQueryForSendersNichesReadyToWork()
    {
        return $this->getBaseQueryForSendersNichesData()
            ->andWhere([
                'photoStatus'   => SenderProfile::STATUS_APPROVED,
                'bannedAt'      => '0000-00-00 00:00:00',
                'deactivatedAt' => '0000-00-00 00:00:00',
                'pausedAt'      => '0000-00-00 00:00:00',
                'removedAt'     => '0000-00-00 00:00:00',
            ]);
    }

    private function getBaseQueryForSendersNichesData()
    {
        return (new Query())->from(SenderProfile::tableName());
    }
}
