<?php

namespace sender\events\components;

class EventDispatcher extends \yii\base\Component
{
    public function triggerEvent($name, $data)
    {
        $data = new DataEvent(['eventData' => $data]);
        $this->trigger($name, $data);
    }
}