<?php

namespace sender\split\components;

use app\models\phoenix\RecipientProfile;
use sender\split\models\Split;
use \yii\caching\Cache;

class SplitSystem
{
    const SPLIT_DATA_CACHE_KEY      = 'split_data';
    const SPLIT_DATA_CACHE_DURATION = 60;

    const PERCENTAGE_DELIMITER = ',';

    const FORCE_USER_SPLIT_GROUP_CACHE_KEY           = 'forcedSplitUserId_';
    const FORCED_USERS_SPLIT_GROUP_CACHE_KEY         = 'forcedData';
    const FORCE_USER_SPLIT_GROUP_CACHE_DATA_DURATION = 86400; //one day
    const FORCED_USERS_DATA_DURATION                 = 172800; //two days

    private $cache;

    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    public function getTotalSplitsResult($userData): array
    {
        $userSplits = [];
        $activeSplits = $this->getActiveSplits();
        foreach ($activeSplits as $split) {
            $group = $this->getSplitResult($userData, $split['name']);
            if ($group !== null) {
                $userSplits[$split['name']] = $group;
            }
        }

        return $userSplits;
    }

    public function getSplitResult(array $userData, string $splitName)
    {
        $splitData = $this->getActiveSplitByName($splitName);
        if (empty($splitData)) {
            return null;
        }

        if ($this->isUserUnderSplit($userData, $splitData) == false) {
            return null;
        }

        $splitGroup = $this->getUserSplitGroup($userData, $splitData);

        return $splitGroup;
    }

    public function isUserUnderSplit(array $userData, array $splitData): bool
    {
        foreach ($splitData as $attributeName => $splitValue) {
            if (in_array($attributeName, Split::$serializedAttributes)) {
                if (!empty($splitValue) && !in_array($userData[$attributeName], $splitValue)) {
                    return false;
                }
            } else {
                switch ($attributeName) {
                    case 'regDateFrom':
                        if (!empty($splitValue) && strtotime($userData['createdAt']) < strtotime($splitValue)) {
                            return false;
                        }
                        break;
                    case 'regDateTo':
                        if (!empty($splitValue) && strtotime($userData['createdAt']) >= strtotime($splitValue)) {
                            return false;
                        }
                        break;
                    default:
                        continue;
                }
            }
        }

        return true;
    }

    private function getUserSplitGroup(array $userData, array $splitData)
    {
        $forcedUserSplitGroupCacheKey = self::FORCE_USER_SPLIT_GROUP_CACHE_KEY . $userData['remoteId'];
        $cachedForcedGroup = $this->cache->get($forcedUserSplitGroupCacheKey);

        if ($cachedForcedGroup !== false) {
            return $cachedForcedGroup;
        }

        $userGroup = null;
        $number = hexdec(substr(md5($userData['remoteId']), 0, 4)) % 100;
        $total = 0;
        $percentage = explode(self::PERCENTAGE_DELIMITER, $splitData['percentage']);
        foreach ($percentage as $num => $percents) {
            $total += $percents;
            if ($number < $total) {
                $userGroup = $num;
                break;
            }
        }

        return $userGroup;
    }

    public function getActiveSplits(): array
    {
        $cacheKey = self::SPLIT_DATA_CACHE_KEY;
        $splitsData = $this->cache->get($cacheKey);
        if ($splitsData !== false) {
            return $splitsData;
        }

        $query = $this->getActiveSplitQuery();
        $activeSplits = $query->all();

        $splitsData = [];
        foreach ($activeSplits as $split) {
            $splitsData[$split->name] = $split->attributes;
        }

        $this->cache->set($cacheKey, $splitsData, self::SPLIT_DATA_CACHE_DURATION);

        return $splitsData;
    }

    private function getActiveSplitByName(string $splitName): array
    {
        $activeSplits = $this->getActiveSplits();
        if (empty($activeSplits[$splitName])) {
            return [];
        }

        return $activeSplits[$splitName];
    }

    private function getActiveSplitQuery()
    {
        $now = date('Y-m-d H:i:s');
        $query = Split::find()
            ->where(['isActive' => true])
            ->andWhere(['<', 'startedAt', $now])
            ->andWhere(['>=', 'finishedAt', $now]);

        return $query;
    }

    public function getRecipientsIdsForExportBySplit(int $splitId, int $group)
    {
        $ids        = [];
        $splitModel = Split::findOne($splitId);
        $recipients = $this->getRecipientsBySplit($splitModel);

        if (!$recipients || is_null($group)) {
            return $ids;
        }

        /* @var $recipients RecipientProfile[] */
        foreach ($recipients as $recipientProfile) {
            $profileData = ['remoteId' => $recipientProfile->remoteId];
            $splitData   = ['percentage' => $splitModel->percentage];

            if ($this->getUserSplitGroup($profileData, $splitData) != $group) {
                continue;
            }

            $ids[$recipientProfile->id] = $recipientProfile->remoteId;
        }

        return $ids;
    }

    /**
     * Get recipients by split
     *
     * @param Split $splitModel
     * @return RecipientProfile|array
     */
    private static function getRecipientsBySplit(Split $splitModel)
    {
        $query = RecipientProfile::find();
        $splitConditionAttributes = [
            'gender',
            'sexuality',
            'isPaid',
            'registrationPlatform',
            'source',
            'country'
        ];

        foreach ($splitModel as $attributeName => $splitValue) {
            if (empty($splitValue)) {
                continue;
            }

            switch ($attributeName) {
                case 'regDateFrom':
                    $query->andWhere(['>=', 'createdAt', $splitValue]);
                    break;
                case 'regDateTo':
                    $query->andWhere(['<=', 'createdAt', $splitValue]);
                    break;
                case 'site':
                    $query->andWhere(['site' => $splitValue]);
                    break;
                default:
                    if (in_array($attributeName, $splitConditionAttributes)) {
                        if (!empty($splitValue)) {
                            $query->andWhere([$attributeName => $splitValue]);
                        }
                    }
                    continue;
            }
        }

        return $query->all();
    }

    public function forceUserSplitGroup(string $userId, int $splitGroup): array
    {
        $result             = ['status' => false, 'message' => ''];
        $forcedUserCacheKey = self::FORCE_USER_SPLIT_GROUP_CACHE_KEY . $userId;
        $forcedData         = $this->cache->get(self::FORCED_USERS_SPLIT_GROUP_CACHE_KEY);

        $this->cache->set($forcedUserCacheKey, $splitGroup, self::FORCE_USER_SPLIT_GROUP_CACHE_DATA_DURATION);

        $result['status'] = true;

        if ($forcedData) {
            $forcedData[$userId] = $splitGroup;
            $this->cache->set(self::FORCED_USERS_SPLIT_GROUP_CACHE_KEY, $forcedData, self::FORCED_USERS_DATA_DURATION);

            return $result;
        }

        $this->cache->set(self::FORCED_USERS_SPLIT_GROUP_CACHE_KEY, [$userId => $splitGroup], self::FORCED_USERS_DATA_DURATION);

        return $result;
    }

    public function getAllForcedData(): array
    {
        $forcedData = $this->cache->get(self::FORCED_USERS_SPLIT_GROUP_CACHE_KEY);

        if(!$forcedData) {
            return [];
        }

        return $forcedData;
    }

    public function getAllSplitsForDropDown(): array
    {
        $splits = Split::find()->asArray()->all();

        return $splits;
    }

    public function getSplitModelByName(string $splitName): Split
    {
        $splitModel = Split::findOne(['name' => $splitName]);

        return $splitModel;
    }

    public function resetAllForcedData(): array
    {
        $result     = ['status' => false];
        $forcedData = $this->cache->get(self::FORCED_USERS_SPLIT_GROUP_CACHE_KEY);

        foreach ($forcedData as $userId => $splitGroup) {
            $forcedUserCacheKey = self::FORCE_USER_SPLIT_GROUP_CACHE_KEY . $userId;
            $result['status'] = $this->cache->delete($forcedUserCacheKey);
        }

        if ($result['status']) {
            $this->cache->delete(self::FORCED_USERS_SPLIT_GROUP_CACHE_KEY);
        }

        return $result;
    }
}