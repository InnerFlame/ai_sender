<?php

namespace sender\split\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SplitSearch represents the model behind the search form about `app\models\Split`.
 */
class SplitSearch extends Split
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'isActive'], 'integer'],
            [
                [
                    'name',
                    'createdAt',
                    'startedAt',
                    'finishedAt',
                    'percentage',
                    'regDateFrom',
                    'regDateTo',
                    'country',
                    'gender',
                    'sexuality',
                    'isPaid',
                    'registrationPlatform',
                    'site',
                    'source',
                    'description'
                ],
                'safe'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params): ActiveDataProvider
    {
        $query = Split::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->startedAt)) {
            $query->andFilterWhere([
                'between',
                'startedAt',
                $this->startedAt . ' 00:00:00',
                $this->startedAt . ' 23:59:59'
            ]);
        }

        if (!empty($this->finishedAt)) {
            $query->andFilterWhere([
                'between',
                'finishedAt',
                $this->finishedAt . ' 00:00:00',
                $this->finishedAt . ' 23:59:59'
            ]);
        }

        if (!empty($this->createdAt)) {
            $query->andFilterWhere([
                'between',
                'createdAt',
                $this->createdAt . ' 00:00:00',
                $this->createdAt . ' 23:59:59'
            ]);
        }
        $query->andFilterWhere([
            'id'          => $this->id,
            'isActive'    => $this->isActive,
            'regDateFrom' => $this->regDateFrom,
            'regDateTo'   => $this->regDateTo,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'percentage', $this->percentage])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'sexuality', $this->sexuality])
            ->andFilterWhere(['like', 'isPaid', $this->isPaid])
            ->andFilterWhere(['like', 'registrationPlatform', $this->registrationPlatform])
            ->andFilterWhere(['like', 'site', $this->site])
            ->andFilterWhere(['like', 'source', $this->source])
            ->andFilterWhere(['like', 'description', $this->description])
        ;

        return $dataProvider;
    }
}
