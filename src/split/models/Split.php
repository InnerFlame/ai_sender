<?php

namespace sender\split\models;

/**
 * This is the model class for table "split".
 *
 * @property integer $id
 * @property string $name
 * @property string $createdAt
 * @property bool $isActive
 * @property string $startedAt
 * @property string $regDateFrom
 * @property string $finishedAt
 * @property string $percentage
 * @property string $regDateTo
 * @property string $country
 * @property string $gender
 * @property string $sexuality
 * @property string $isPaid
 * @property string $registrationPlatform
 * @property string $site
 * @property string $source
 * @property string $description
 */
class Split extends \yii\db\ActiveRecord
{
    public static $serializedAttributes = [
        'country',
        'gender',
        'sexuality',
        'isPaid',
        'registrationPlatform',
        'site',
        'source',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'split';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                [
                    'name',
                    'startedAt',
                    'finishedAt',
                    'percentage'
                ],
                'required'
            ],
            [
                [
                    'name',
                    'createdAt',
                    'isActive',
                    'startedAt',
                    'finishedAt',
                    'percentage',
                    'regDateFrom',
                    'regDateTo',
                    'description'
                ],
                'string'
            ],
            [
                [
                    'createdAt',
                    'country',
                    'gender',
                    'source',
                    'sexuality',
                    'isPaid',
                    'site',
                    'registrationPlatform'
                ],
                'safe'
            ],
            ['percentage', 'percentageValidation']
        ];
    }

    /**
     * @inheritdoc
     * @return void
     */
    public function afterFind()
    {
        parent::afterFind();

        foreach (self::$serializedAttributes as $attributeName) {
            if (!empty($this->$attributeName)) {
                $this->$attributeName = json_decode($this->$attributeName, true);
            }
        }
    }

    /**
     * @inheritdoc
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert): bool
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        foreach (self::$serializedAttributes as $attributeName) {
            if (!empty($this->$attributeName)) {
                $this->$attributeName = json_encode($this->$attributeName);
            } else {
                $this->$attributeName = null;
            }
        }

        if (!empty($this->percentage)) {
            $this->percentage = $this->filterPercentageString($this->percentage);
        }

        return true;
    }

    /**
     * Custom validation for percentage field
     *
     * @param $attribute
     * @param $params
     * @return  bool
     */
    public function percentageValidation($attribute, $params): bool
    {
        $filteredPercentageString = $this->filterPercentageString($this->$attribute);
        $percentageValuesArray    = array_filter(explode(',', $filteredPercentageString));
        $totalPercentage          = array_sum($percentageValuesArray);

        if(!$this->isValidPercentageValuesLength($percentageValuesArray)) {
            $this->addError($attribute, 'One of percentage values invalid format, values must be 2 chars digit only!');
            return false;
        }

        if ($totalPercentage != 100) {
            $this->addError($attribute, 'Invalid percentage, sum of all values must equal 100, you got ' . $totalPercentage. '!');
            return false;
        }

        return true;
    }

    /**
     * Validate each percentage values for 2 digits length
     *
     * @param array $percentageValues
     * @return bool
     */
    private function isValidPercentageValuesLength(array $percentageValues): bool
    {
        foreach ($percentageValues as $value) {
            if (strlen($value) < 2) {
                return false;
            }
        }

        return true;
    }

    /**
     * Filter percentage raw string by remove all maskedInput template parts
     *
     * @param string $rawString
     * @return string
     */
    private function filterPercentageString(string $rawString): string
    {
        $filteredString = str_replace(
            [',__', ',_', '__', '_', '__,', '_,'],
            '',
            $rawString
        );

        return $filteredString;
    }
}


