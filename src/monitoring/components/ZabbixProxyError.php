<?php

namespace sender\monitoring\components;

class ZabbixProxyError
{
    const KEY = 'proxyError';

    /** @var ZabbixMemcachedConnection */
    private $zabbixMemcachedConnection;

    public function __construct(ZabbixMemcachedConnection $zabbixMemcachedConnection)
    {
        $this->zabbixMemcachedConnection = $zabbixMemcachedConnection;
    }

    public function incrementCountError()
    {
        $this->zabbixMemcachedConnection->increment(self::KEY, 1);
    }

    public function getCount()
    {
        return $this->zabbixMemcachedConnection->get(self::KEY);
    }
}
