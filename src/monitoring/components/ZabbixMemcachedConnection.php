<?php

namespace sender\monitoring\components;

class ZabbixMemcachedConnection
{
    const KEY_PREFIX = 'zbx_';

    /** @var \Memcached */
    private $cache;

    public function __construct(\Memcached $cache)
    {
        $this->cache = $this->cloneMemcachedWithPrefix($cache, self::KEY_PREFIX);
    }

    public function increment(string $key, int $step)
    {
        if ($this->cache->increment($key, $step) === false) {
            if ($this->cache->add($key, $step) === false) {
                $this->cache->increment($key, $step);
            }
        }
    }

    public function set(string $key, string $value)
    {
        $this->cache->set($key, $value);
    }

    public function get(string $key)
    {
        return $this->cache->get($key);
    }

    public function getDataAndDecrementCounters(array $keys): array
    {
        $data = $this->cache->getMulti($keys);
        if (!is_array($data)) {
            return [];
        }
        $this->decrementCounters($data);

        return $this->addMissingKeysWithZeroValue($data, $keys);
    }

    public function getData(array $keys): array
    {
        $data = $this->cache->getMulti($keys);
        if (!is_array($data)) {
            return [];
        }

        return $this->addMissingKeysWithZeroValue($data, $keys);
    }

    private function decrementCounters(array $data)
    {
        foreach ($data as $key => $value) {
            $this->cache->decrement($key, $value);
        }
    }

    private function addMissingKeysWithZeroValue(array $data, array $keys): array
    {
        return array_merge(
            array_fill_keys($keys, 0),
            $data
        );
    }

    private function cloneMemcachedWithPrefix(\Memcached $cache, string $prefix): \Memcached
    {
        $result = new \Memcached();
        $result->addServers($cache->getServerList());
        $result->setOption(\Memcached::OPT_PREFIX_KEY, $prefix);
        return $result;
    }
}
