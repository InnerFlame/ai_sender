<?php

namespace sender\communication\models;

use Yii;

/**
 * This is the model class for table "phoenixCommunicationSchedule".
 *
 * @property integer $id
 * @property integer $senderId
 * @property integer $recipientId
 * @property integer $messageId
 * @property integer $groupId
 * @property string $createdAt
 * @property string $sentAt
 * @property string $timeToSend
 * @property integer $step
 * @property string $sendStartedAt
 * @property integer $isSenderBanned
 * @property integer $attemptsCount
 * @property integer $schemaId
 * @property integer $restartNumber
 */
class CommunicationSchedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'communicationSchedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['senderId', 'recipientId', 'messageId'], 'required'],
            [['senderId', 'recipientId', 'messageId', 'groupId', 'schemaId'], 'integer'],
            [['createdAt', 'sentAt', 'timeToSend', 'step', 'sendStartedAt', 'isSenderBanned', 'attemptsCount', 'numberRestart'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'senderId'          => 'Sender ID',
            'recipientId'       => 'Recipient ID',
            'messageId'         => 'Message ID',
            'groupId'           => 'Group ID',
            'createdAt'         => 'Created At',
            'sentAt'            => 'Sent At',
            'timeToSend'        => 'Time To Send',
            'step'              => 'Step',
            'sendStartedAt'     => 'Send Started At',
            'isSenderBanned'    => 'Is Sender Banned',
        ];
    }
}