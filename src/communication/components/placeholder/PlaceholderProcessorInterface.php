<?php

namespace sender\communication\components\placeholder;

interface PlaceholderProcessorInterface
{
    public function processPlaceholder();
}