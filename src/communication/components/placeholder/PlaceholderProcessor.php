<?php

namespace sender\communication\components\placeholder;

use app\components\common\IdEncoder;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\SenderProfile;
use sender\split\components\SplitSystem;

class PlaceholderProcessor
{
    /** @var  array listPlaceholderProcessors */
    private $listPlaceholderProcessors;
    /**
     * value processGettingPlaceholder
     * @var  string $queue
     */
    private $queue;

    /* @var SplitSystem */
    protected $splitSystem;
    /* @var IdEncoder */
    protected $idEncoderComponent;

    public function __construct(
        array $listPlaceholderProcessors,
        string $queue,
        SplitSystem $splitSystem,
        IdEncoder $idEncoder
    )
    {
        $this->listPlaceholderProcessors = $listPlaceholderProcessors;
        $this->queue                     = $queue;
        $this->splitSystem               = $splitSystem;
        $this->idEncoderComponent        = $idEncoder;
    }

    public function processPlaceholder()
    {
        foreach ($this->listPlaceholderProcessors as $placeholderProcessor) {
            if ($placeholderProcessor instanceof PlaceholderProcessorInterface) {
                $placeholderProcessor->processPlaceholder();
            }
        }
    }

    public function processGetPlaceholder()
    {
        \Yii::$app->internalAmqp->consumeEvents($this->queue, [$this, 'placeholderHandler']);
    }

    public function placeholderHandler($message)
    {
        $communication = CommunicationSchedule::findOne($message['communicationId']);
        if (empty($communication)) {
            return false;
        }

        $recipient = RecipientProfile::findOne($communication->recipientId);
        $sender    = SenderProfile::findOne($communication->senderId);
        if (empty($recipient) || empty($sender)) {
            return false;
        }

        $curlData = [
            'from'             => $recipient->remoteId,
            'to'               => $sender->originId,
            'site'             => \Yii::$app->yaml->parseByKey('site', $recipient->site)['hash'],
            'country'          => $recipient->country,
            'paid'             => $recipient->isPaid,
            'traff_src'        => 'Aff Internal',
            'action_way'       => 'external',
            'platform'         => $recipient->lastUsedPlatform,
            'sender_country'   => $sender->country,
            'sender_city'      => $sender->city,
            'sender_birthday'  => $sender->birthday,
            'time_offset'      => $sender->timeOffset,
            'multi_profile_id' => $recipient->multiProfileId ?? '',
            'splits'           => $this->splitSystem->getTotalSplitsResult($recipient->attributes),
            'profile_short_id' => $this->idEncoderComponent->getRecipientShortId($recipient),
        ];

        $evaData = $this->getEvaPlaceholder($recipient, $sender, $curlData, $message);
        if (empty($evaData)) {
            return false;
        }

        foreach ($evaData->messages as $item) {
            $messageId = \Yii::$app->evaMessage->getStepMessage($item);
            if ($messageId) {
                $scheduleItem                   = new CommunicationSchedule();
                $scheduleItem->senderId         = $communication->senderId;
                $scheduleItem->recipientId      = $communication->recipientId;
                $scheduleItem->step             = $communication->step + 1;
                $scheduleItem->groupId          = $communication->groupId;
                $scheduleItem->schemaId         = $communication->schemaId;
                $scheduleItem->messageId        = $messageId;
                $scheduleItem->timeToSend       = date('Y-m-d H:i:s');
                $scheduleItem->isSenderBanned   = ($sender->bannedAt == '0000-00-00 00:00:00') ? 0 : 1;
                $scheduleItem->save();
            }
        }

        return true;
    }

    private function getEvaPlaceholder($recipient, $sender, $curlData, $message)
    {
        switch ($message['type']) {
            case 'zero':
                return \Yii::$app->evaMessage->getZeroPlaceholder($recipient, $sender, $curlData);
                break;
            case 'firstRestart':
                return \Yii::$app->evaMessage->getFirstRestartPlaceholder($recipient, $sender, $curlData);
                break;
            default:
                return \Yii::$app->evaMessage->getEvaPlaceholder($recipient, $sender, $curlData);

        }
    }
}
