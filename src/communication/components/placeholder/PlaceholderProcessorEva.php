<?php

namespace sender\communication\components\placeholder;

use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\IncomingMessage;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\StepMessage;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class PlaceholderProcessorEva implements PlaceholderProcessorInterface
{
    private $keyCache = 'keyProcessPlaceholderEva';

    /** @var  array $settings */
    private $settings;
    /**
     * value processGettingPlaceholder
     * @var  string $queue
     */
    private $queue;

    public function __construct(array $settings, string $queue)
    {
        $this->settings = $settings;
        $this->queue    = $queue;
    }

    public function processPlaceholder()
    {
        $communications = $this->getCommunication();
        foreach ($communications as $communication) {
            $expire = $this->settings['endTimeSend'] - $this->settings['startTimeSend'];
            $ttl    = \Yii::$app->redis->executeCommand('PTTL', [$this->keyCache]);
            if ($ttl > 0) {
                $expire = round($ttl / 1000);
            }

            \Yii::$app->redis->executeCommand('RPUSH', [$this->keyCache, $communication['id']]);
            \Yii::$app->redis->executeCommand('EXPIRE', [$this->keyCache, $expire]);

            if ($this->hasCommPlaceholder($communication['recipientId'], $communication['senderId'])) {
                continue;
            }

            \Yii::$app->internalAmqp->publishEvent($this->queue, ['communicationId' => $communication['id'], 'type' => 'eva']);
        }
    }

    private function getCommunication()
    {
        $ids   = [];
        $query = (new Query())->select('max(c.id) as id, c.recipientId, c.senderId')
            ->from(CommunicationSchedule::tableName() . ' as c')
            ->innerJoin(RecipientProfile::tableName() . ' as r', 'c.recipientId=r.id')
            ->where(['>=', 'c.sentAt', date('Y-m-d H:i:s', time() - $this->settings['endTimeSend'])])
            ->andWhere(['<=', 'c.sentAt', date('Y-m-d H:i:s', time() - $this->settings['startTimeSend'])])
            ->andWhere(['NOT IN', 'c.id', \Yii::$app->redis->executeCommand('LRANGE', [$this->keyCache, '0', '-1'])])
            ->andWhere(['>', 'c.step', 0])
            ->groupBy('c.senderId, c.recipientId');

        if (!empty($this->settings['country'])) {
            $query->andWhere(['r.country' => $this->settings['country']]);
        }

        foreach ($query->batch(1000) as $communicationIds) {
            $query = (new Query())->select('c.id, c.recipientId, c.senderId')
                ->from(CommunicationSchedule::tableName() . ' as c')
                ->innerJoin(StepMessage::tableName() . ' as s', 's.id=c.messageId')
                ->leftJoin(IncomingMessage::tableName() . ' as i', 'i.recipientId=c.recipientId AND i.senderId=c.senderId AND i.step>c.step')
                ->where([
                    'c.id'             => ArrayHelper::getColumn($communicationIds, 'id'),
                    's.hasPlaceholder' => 0,
                ])
                ->andWhere('i.id IS NULL');

            $ids = array_merge($ids, $query->all());
        }

        return $ids;
    }

    /**
     * Check, chat has placeholder or not
     * @param $recipientId
     * @param $senderId
     * @return array
     */
    private function hasCommPlaceholder($recipientId, $senderId)
    {
        $query = (new Query())->select('c.id')
            ->from(CommunicationSchedule::tableName() . ' as c')
            ->innerJoin(StepMessage::tableName() . ' as s', 'c.messageId=s.id')
            ->where([
                'c.recipientId'    => $recipientId,
                'c.senderId'       => $senderId,
                's.hasPlaceholder' => 1,
            ]);

        $result = $query->one();

        return $result;
    }
}