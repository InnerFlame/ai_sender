<?php

namespace sender\communication\components\placeholder;

use sender\communication\components\containerManager\ContainerManager;
use app\models\phoenix\StepMessage;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\IncomingMessage;
use app\models\phoenix\RecipientProfile;
use yii\db\Expression;
use yii\db\Query;

class PlaceholderProcessorFirstRestart implements PlaceholderProcessorInterface
{
    /** @var  array $settings */
    private $settings;
    /**
     * value processGettingPlaceholder
     * @var  string $queue
     */
    private $queue;
    /** @var ContainerManager $commContainersManager */
    private $commContainersManager;

    public function __construct(
        array $settings,
        string $queue,
        ContainerManager $commContainersManager
    ) {
        $this->settings              = $settings;
        $this->queue                 = $queue;
        $this->commContainersManager = $commContainersManager;
    }

    public function processPlaceholder()
    {
        $recipientIds = $this->getRecipients();
        foreach ($recipientIds as $recipientId) {
            if (\Yii::$app->cache->get('firstRestartRecipientId' . $recipientId)) {
                continue;
            }

            $expire = $this->settings['endTimeSend'] + $this->settings['startTimeSend'];
            \Yii::$app->cache->set('firstRestartRecipientId' . $recipientId, 1, $expire);
            if ($this->hasCommPlaceholder($recipientId)) {
                continue;
            }

            $communicationId = $this->getCommunication($recipientId);
            if (!empty($communicationId)) {
                $data = [
                    'communicationId' => $communicationId,
                    'type'            => 'firstRestart'
                ];
                \Yii::$app->internalAmqp->publishEvent($this->queue, $data);
            }
        }
    }

    /**
     * Check, chat has placeholder or not
     * @param $recipientId
     * @return array
     */
    private function hasCommPlaceholder(int $recipientId)
    {
        return (new Query())->select('c.id')
            ->from(CommunicationSchedule::tableName() . ' as c')
            ->innerJoin(StepMessage::tableName() . ' as s', 'c.messageId=s.id')
            ->where([
                'c.recipientId'    => $recipientId,
                'c.restartNumber'  => 1,
                's.hasPlaceholder' => 1,
            ])
            ->one();
    }

    private function getRecipients()
    {
        $query =  (new Query())
            ->select('c.recipientId')
            ->from(CommunicationSchedule::tableName() . ' as c')
            ->innerJoin(RecipientProfile::tableName() . ' as r', 'c.recipientId=r.id')
            ->leftJoin(
                IncomingMessage::tableName() . ' as i',
                'i.recipientId=c.recipientId AND i.createdAt>=:endTimeSend',
                [':endTimeSend' => date('Y-m-d H:i:s', time() - $this->settings['endTimeSend'])]
            )
            ->where('c.restartNumber=1')
            ->andWhere(['>=', 'c.sentAt', date('Y-m-d H:i:s', time() - $this->settings['endTimeSend'])])
            ->andWhere(['<=', 'c.sentAt', date('Y-m-d H:i:s', time() - $this->settings['startTimeSend'])])
            ->andWhere('i.id is null')
            ->groupBy('c.recipientId');

        if (!empty($this->settings['country'])) {
            $query->andWhere(['r.country' => $this->settings['country']]);
        }

        return $query->column();
    }

    private function getCommunication(int $recipientId)
    {
        $recipient = RecipientProfile::findOne($recipientId);
        $senderIds = $this->commContainersManager->getAllActiveSendersIds($recipient->country);

        return (new Query())
            ->select('id')
            ->from(CommunicationSchedule::tableName())
            ->where([
                'recipientId'   => $recipientId,
                'senderId'      => $senderIds,
                'restartNumber' => 1
            ])
            ->orderBy(new Expression('rand()'))
            ->one();
    }
}
