<?php

namespace sender\communication\components\placeholder;

use app\models\phoenix\StepMessage;
use sender\communication\components\containerManager\ContainerManager;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\IncomingMessage;
use app\models\phoenix\RecipientPlaceholderInitialLog;
use app\models\phoenix\RecipientProfile;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class PlaceholderProcessorInitial implements PlaceholderProcessorInterface
{
    /** @var  array $settings */
    private $settings;
    /**
     * value processGettingPlaceholder
     * @var  string $queue
     */
    private $queue;
    /** @var ContainerManager $containersManager */
    private $containersManager;

    public function __construct(
        array $settings,
        string $queue,
        ContainerManager $containersManager
    ) {
        $this->settings = $settings;
        $this->queue = $queue;
        $this->containersManager = $containersManager;
    }

    public function processPlaceholder()
    {
        $query = $this->getQuery();
        foreach ($query->batch() as $data) {
            foreach ($data as $item) {
                $recipientPlaceholderInitial = RecipientPlaceholderInitialLog::findOne(
                    ['recipientId' => $item['recipientId']]
                );
                if (empty($recipientPlaceholderInitial)) {
                    RecipientPlaceholderInitialLog::createRecord($item['recipientId'], $item['restartNumber']);
                } else {
                    if (!$this->canSend($recipientPlaceholderInitial, $item)) {
                        continue;
                    }

                    $recipientPlaceholderInitial->updateRecord($item['restartNumber']);
                }

                $communicationId = $this->getCommunication($item['recipientId']);
                if (!empty($communicationId)) {
                    $data = [
                        'communicationId' => $communicationId,
                        'type'            => 'zero'
                    ];
                    \Yii::$app->internalAmqp->publishEvent($this->queue, $data);
                }
            }
        }
    }

    private function getQuery(): Query
    {
        $query = (new Query())
            ->select('c.recipientId, c.restartNumber')
            ->from(CommunicationSchedule::tableName() . ' as c')
            ->innerJoin(RecipientProfile::tableName() . ' as r', 'c.recipientId=r.id')
            ->leftJoin(
                IncomingMessage::tableName() . ' as i',
                'i.recipientId=c.recipientId'
            )
            ->where(['c.step' => 0])
            ->andWhere('c.restartNumber %2 = 0')
            ->andWhere('c.restartNumber > 0')
            ->andWhere(['>=', 'c.sentAt', date('Y-m-d H:i:s', time() - $this->settings['endTimeSend'])])
            ->andWhere(['<=', 'c.sentAt', date('Y-m-d H:i:s', time() - $this->settings['startTimeSend'])])
            ->groupBy('c.recipientId')
            ->andWhere('i.id is null');

        if (!empty($this->settings['country'])) {
            $query->andWhere(['r.country' => $this->settings['country']]);
        }

        return $query;
    }

    private function getCommunication(int $recipientId)
    {
        $recipient = RecipientProfile::findOne($recipientId);
        $senderIds = $this->containersManager->getAllActiveSendersIds($recipient->country);

        $communications = (new Query())
            ->select('id, senderId, messageId')
            ->from(CommunicationSchedule::tableName())
            ->where([
                'recipientId' => $recipientId,
                'senderId'    => $senderIds,
            ])
            ->orderBy(new Expression('rand()'))
            ->all();

        $communications = ArrayHelper::index($communications, null, 'senderId');
        foreach ($communications as $senderId => $communication) {
            $messageIds = ArrayHelper::getColumn($communication, 'messageId');
            if (!$this->hasPlaceholder($messageIds)) {
                return $communication[0]['id'];
            }
        }


        return null;
    }

    private function hasPlaceholder(array $messageIds)
    {
        $stepMessage = StepMessage::findOne([
            'id'             => $messageIds,
            'hasPlaceholder' => 1,
        ]);

        if ($stepMessage) {
            return true;
        }

        return false;
    }

    private function canSend(RecipientPlaceholderInitialLog $recipientPlaceholderInitial, array $item): bool
    {
        return $recipientPlaceholderInitial->lastRestart < $item['restartNumber']
        && $recipientPlaceholderInitial->countSent < $this->settings['countSent'];
    }
}
