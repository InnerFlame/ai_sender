<?php

namespace sender\communication\components\containerManager;

use app\config\Constant;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use app\models\rpc\Container;
use yii\helpers\ArrayHelper;

class ContainerStarter extends Common implements StarterInterface
{
    public function start(array $setting)
    {
        $lastIterationProfiles = $this->getCurrentIterationProfiles($setting['country']);
        $startedContainers     =  Container::findAll([
            'target'    => Constant::TARGET_PHOENIX,
            'type'      => Container::TYPE_COMMUNICATION,
            'status'    => Container::STATUS_STARTED,
            'profileId' => $lastIterationProfiles,
        ]);

        $startedContainerIds = ArrayHelper::getColumn($startedContainers, 'profileId');
        $missedProfilesIds   = array_diff($lastIterationProfiles, $startedContainerIds);
        $profiles = SenderProfile::findAll($missedProfilesIds);
        foreach ($profiles as $profile) {
            usleep(500000);
            \Yii::$app->rpcMethods->createRpcContainer(
                $profile,
                Container::TYPE_COMMUNICATION,
                Constant::TARGET_PHOENIX
            );
        }

        $startedClients = Client::findAll([
            'target'    => Constant::TARGET_PHOENIX,
            'type'      => Client::TYPE_COMMUNICATION,
            'status'    => Client::STATUS_STARTED,
            'profileId' => $lastIterationProfiles,
        ]);

        $startedClientIds = ArrayHelper::getColumn($startedClients, 'profileId');
        $missedProfilesIds = array_diff($startedContainerIds, $startedClientIds);

        $profiles = SenderProfile::findAll($missedProfilesIds);
        foreach ($profiles as $profile) {
            \Yii::$app->rpcMethods->loginRpcClient($profile, Client::TYPE_COMMUNICATION, Constant::TARGET_PHOENIX);
        }
    }
}
