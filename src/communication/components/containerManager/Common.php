<?php

namespace sender\communication\components\containerManager;

use app\config\Constant;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use app\models\rpc\LifeCycleContainersInfo;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class Common
{
    /** @var int $cacheTtl */
    private $cacheTtl = 3600;

    const LIFE_CYCLE_CONTAINERS_INFO_KEY = 'life_cycle_containers_info';
    const INITIAL_AND_ANSWERS_KEY        = 'initial_and_answers';
    const ONLY_ANSWERS_KEY               = 'only_answers';

    protected function getContainersInfo(string $country): array
    {
        $containersInfoCacheKey = self::LIFE_CYCLE_CONTAINERS_INFO_KEY . $country;
        $containersInfo = \Yii::$app->cache->get($containersInfoCacheKey);

        if (empty($containersInfo)) {
            $lifeCycleContainersInfo = LifeCycleContainersInfo::findOne(['country' => $country]);
            if (!empty($lifeCycleContainersInfo)) {
                $containersInfo = json_decode($lifeCycleContainersInfo->data, true);
            }
        }

        if (empty($containersInfo)) {
            $containersInfo['country']           = $country;
            $containersInfo['initialAndAnswers'] = [];
            $containersInfo['onlyAnswers']       = [];
        }

        return $containersInfo;
    }

    protected function getCurrentIterationProfiles(string $country): array
    {
        $containersInfo = $this->getContainersInfo($country);

        return ArrayHelper::getColumn(
            array_merge(
                $containersInfo['initialAndAnswers'],
                $containersInfo['onlyAnswers']
            ),
            'id'
        );
    }

    protected function saveContainersInfo(array $containersInfo)
    {
        $lifeCycleContainersInfo = LifeCycleContainersInfo::findOne(['country' => $containersInfo['country']]);
        if (empty($lifeCycleContainersInfo)) {
            $lifeCycleContainersInfo = new LifeCycleContainersInfo();
            $lifeCycleContainersInfo->country = $containersInfo['country'];
        }

        $lifeCycleContainersInfo->data = json_encode($containersInfo);
        $lifeCycleContainersInfo->save();

        $cacheKey = self::LIFE_CYCLE_CONTAINERS_INFO_KEY . $containersInfo['country'];
        \Yii::$app->cache->set($cacheKey, $containersInfo, $this->cacheTtl);
    }

    protected function readySenderQueryConditions(Query $baseQuery): Query
    {
        return $baseQuery
            ->where([
                'bannedAt'      => '0000-00-00 00:00:00',
                'deactivatedAt' => '0000-00-00 00:00:00',
                'pausedAt'      => '0000-00-00 00:00:00',
                'removedAt'     => '0000-00-00 00:00:00',
                'photoStatus'   => SenderProfile::STATUS_APPROVED,
                'userType'      => SenderProfile::TYPE_SENDER,
                'actionWay'     => SenderProfile::ACTION_WAY_DEFAULT
            ])
            ->andWhere(['<=', 'canBeInPullAt', date('Y-m-d H:i:s')])
            ->andWhere([
                'or',
                ['liveTime' => 0],
                ['>', 'liveTime', date('Y-m-d H:i:s', time() + 3 * 3600)],
            ]);
    }

    protected function getStartedClients(string $country)
    {
        $clients        = Client::findAll([
            'target'    => [Constant::TARGET_PHOENIX, Constant::TARGET_PHOENIX_GO],
            'status'    => Client::STATUS_STARTED,
            'type'      => Client::TYPE_COMMUNICATION,
            'profileId' => $this->getCurrentIterationProfiles($country)
        ]);

        return ArrayHelper::index($clients, 'profileId');
    }
}
