<?php

namespace sender\communication\components\containerManager;


interface StarterInterface
{
    public function start(array $setting);
}
