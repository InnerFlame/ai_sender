<?php

namespace sender\communication\components\containerManager;

use app\models\phoenix\SenderProfile;
use sender\interaction\components\InteractionClient;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class InteractionStarter extends Common implements StarterInterface
{
    /** @var InteractionClient $interactionClient */
    private $interactionClient;

    public function __construct(InteractionClient $interactionClient)
    {
        $this->interactionClient = $interactionClient;
    }

    public function start(array $setting)
    {
        $lastIterationProfiles = $this->getCurrentIterationProfiles($setting['country']);

        $clients = $this->interactionClient->getStatus();
        $interactionClientIds = ArrayHelper::getColumn($clients, 'id');

        $existentClientIds = (new Query())
            ->select('id')
            ->from(SenderProfile::tableName())
            ->where([
                'id' => $interactionClientIds,
                'country' => $setting['country'],
            ])
            ->column();


        $missedProfilesIds = array_diff($lastIterationProfiles, $existentClientIds);
        $profiles = SenderProfile::findAll($missedProfilesIds);

        foreach ($profiles as $profile) {
            $this->interactionClient->startProfile($profile);
        }

        $excessClients = array_diff($existentClientIds, $lastIterationProfiles);
        $profiles = SenderProfile::findAll($excessClients);
        foreach ($profiles as $profile) {
            $this->interactionClient->stopProfile($profile);
        }
    }
}
