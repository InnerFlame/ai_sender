<?php

namespace sender\communication\components\containerManager;

use yii\base\ErrorException;

class LifeCycleConfigParser
{
    public static function parse($key, $country = 'DEF')
    {
        $data = \Yii::$app->yaml->parseByKey('flexibleLifeCycle', $key);
        if (empty($data)) {
            throw new ErrorException('No param in flexibleLifeCycle - ' . $key);
        }

        if (!empty($data[$country])) {
            return $data[$country];
        }

        return $data['DEF'];
    }
}