<?php

namespace sender\communication\components\containerManager;

use app\models\phoenix\CommunicationContainer;
use app\models\phoenix\SenderProfile;
use sender\niche\components\NicheQuery;
use yii\db\Query;
use linslin\yii2\curl\Curl;
use app\config\Constant;

class RemainderProfileChecker extends Common
{
    /** @var NicheQuery $nicheQuery */
    private $nicheQuery;

    public function __construct(NicheQuery $nicheQuery)
    {
        $this->nicheQuery = $nicheQuery;
    }

    public function check()
    {
        $alerts = '';
        $containersSettings = CommunicationContainer::getActiveData();
        foreach ($containersSettings as $settings) {
            $excludedIds      = $this->getCurrentIterationProfiles($settings['country']);
            $percentLanguages = LifeCycleConfigParser::parse('percentLanguage', $settings['country']);
            $percentNiches    = LifeCycleConfigParser::parse('percentNiche', $settings['country']);
            foreach ($percentNiches as $keyNiche => $percentNiche) {
                foreach ($percentLanguages as $keyLanguage => $percentLanguage) {
                    $remainder = $this->getRemainder($settings['country'], $excludedIds, $keyNiche, $keyLanguage);
                    $minNeedCount = ($settings['countStarted'] * $percentNiche) / 100;
                    $minNeedCount = round(($minNeedCount * $percentLanguage) / 100);
                    if ($remainder > $minNeedCount) {
                        continue;
                    }
                    $alerts .= 'Country:' . $settings['country'] . ';';
                    $alerts .= ' Niche:' . $keyNiche . ';';
                    $alerts .= 'Language:' . $keyLanguage . ';';
                    $alerts .= 'Min need:' . $minNeedCount . ';';
                    $alerts .= 'Remainder:' . $remainder . '.';
                }
            }
        }

        if (!empty($alerts)) {
            $text = 'Few Profiles! ' . $alerts;
            $url  = 'https://api.telegram.org/bot';
            $url .= Constant::SENDER_ALERTS_TELEGRAM_TOKEN;
            $url .= '/sendMessage?chat_id=' . Constant::SENDER_ALERTS_TELEGRAM_CHAT;
            $url .= '&text=' . $text;

            $curl = new Curl();
            $curl->get($url);
        }
    }

    private function getRemainder(string $country, array $excludedIds, string $niche, string $language)
    {
        $query = (new Query())
            ->select('count(s.id)')
            ->from(SenderProfile::tableName() . ' as s')
            ->where([
                'country'       => $country,
                'bannedAt'      => '0000-00-00 00:00:00',
                'deactivatedAt' => '0000-00-00 00:00:00',
                'pausedAt'      => '0000-00-00 00:00:00',
                'removedAt'     => '0000-00-00 00:00:00',
                'photoStatus'   => SenderProfile::STATUS_APPROVED,
                'userType'      => SenderProfile::TYPE_SENDER,
                'actionWay'     => SenderProfile::ACTION_WAY_DEFAULT,
                'language'      => $language,
            ])
            ->andWhere(['not in', 's.id', $excludedIds])
            ->andWhere([
                'or',
                ['liveTime' => 0],
                ['>', 'liveTime', date('Y-m-d H:i:s', time() + 3600 * 24 * 3)],
            ]);

        $this->nicheQuery->addToQuery($query, $niche, 's');

        return $query->scalar();
    }
}
