<?php

namespace sender\communication\components\containerManager;


use app\config\Constant;
use app\models\phoenix\CommunicationContainer;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use sender\interaction\components\InteractionClient;
use sender\logger\components\Log;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class ContainerManager extends Common
{
    /** @var PoolUpdater */
    private $poolUpdater;
    /** @var ContainerStarter */
    private $containerStarter;
    /** @var InteractionStarter $interactionStarter */
    private $interactionStarter;
    /** @var InteractionClient $interactionClient */
    private $interactionClient;
    /** @var Log */
    private $logger;

    public function __construct(
        PoolUpdater $poolUpdater,
        ContainerStarter $containerStarter,
        InteractionStarter $interactionStarter,
        InteractionClient $interactionClient,
        Log $logger
    ) {
        $this->poolUpdater      = $poolUpdater;
        $this->containerStarter = $containerStarter;
        $this->interactionStarter = $interactionStarter;
        $this->interactionClient = $interactionClient;
        $this->logger = $logger;
    }

    public function updatePool()
    {
        $containersSettings = CommunicationContainer::getActiveData();
        foreach ($containersSettings as $settings) {
            $this->poolUpdater->updatePool($settings);
        }
    }

    public function startContainers()
    {
        $containersSettings = CommunicationContainer::getActiveData();
        foreach ($containersSettings as $settings) {
            if (\Yii::$container->get('interactionRouter')
                ->isSeparatedInteractionByCountry($settings['country'])
            ) {
                $starter = $this->containerStarter;
            } else {
                $starter = $this->interactionStarter;
            }

            $starter->start($settings);
        }
    }

    public function isClientStarted(SenderProfile $sender): bool
    {
        if (\Yii::$container->get('interactionRouter')->isSeparatedInteraction($sender)) {
            $startedClients = $this->getAllStartedClients();
            if (!empty($startedClients) && !empty($startedClients[$sender->id])) {
                return true;
            }
        } else {
            $client = $this->interactionClient->getClientStatus($sender);
            if (!empty($client) && $client['clientStatus'] == 'connected') {
                return true;
            }
        }

        return false;
    }

    public function getAllStartedClients()
    {
        $clients            = [];
        $containersSettings = CommunicationContainer::getActiveData();
        //may be some troubles with extremely high clients amount
        foreach ($containersSettings as $settings) {
            $clients += $this->getStartedClients($settings['country']);
        }

        return $clients;
    }

    public function getCountPlannedToStartClients(): int
    {
        $result = 0;
        $containersSettings = CommunicationContainer::getActiveData();
        foreach ($containersSettings as $settings) {
            $containersInfo = $this->getContainersInfo($settings['country']);
            $result += count($containersInfo['initialAndAnswers']) + count($containersInfo['onlyAnswers']);
        }

        return $result;
    }

    public function getAllActiveSendersIds(string $country): array
    {
        return $this->filterStartedSenderIds($country, $this->getCurrentIterationProfiles($country));
    }

    public function getActiveInitialSenderIds(string $country): array
    {
        $containersInfo = $this->getContainersInfo($country);

        return $this->filterStartedSenderIds($country, array_keys($containersInfo['initialAndAnswers']));
    }

    protected function filterStartedSenderIds(string $country, array $senderIds)
    {
        if (\Yii::$container->get('interactionRouter')->isSeparatedInteractionByCountry($country)) {
            $clients = Client::findAll([
                'target'    => Constant::TARGET_PHOENIX,
                'status'    => Client::STATUS_STARTED,
                'type'      => Client::TYPE_COMMUNICATION,
                'profileId' => $senderIds,
            ]);

            return ArrayHelper::getColumn($clients, 'profileId');
        } else {
            $startedClientIds = [];
            $clients = $this->interactionClient->getStatus();
            if (!empty($clients)) {
                foreach ($clients as $client) {
                    if ($client['clientStatus'] == 'connected') {
                        $startedClientIds[] = $client['id'];
                    }
                }
            }

            return array_intersect($senderIds, $startedClientIds);
        }
    }

    public function deleteContainer(SenderProfile $profile)
    {
        $containersInfo = $this->getContainersInfo($profile->country);
        foreach ($containersInfo as $key => $containerInfo) {
            if (is_array($containerInfo)) {
                $containerInfo = ArrayHelper::getColumn($containerInfo, 'id');
                $keyProfile    = array_search($profile->id, $containerInfo);
                if (is_int($keyProfile)) {
                    unset($containersInfo[$key][$keyProfile]);
                    break;
                }
            }
        }

        $this->saveContainersInfo($containersInfo);
        $this->logger->logSenderProfileInfo('Profile was banned', $profile);
    }

    public function deleteContainerWithoutPhoto(SenderProfile $profile)
    {
        $this->deleteContainer($profile);
        $this->logger->logSenderProfileInfo('The profile has not photo', $profile);
    }

    public function countReadySendersByCountry(): array
    {
        return $this->readySenderQueryConditions((new Query())->from(SenderProfile::tableName()))
            ->select(new Expression('count(*) as cnt'))
            ->addSelect('country')
            ->groupBy('country')
            ->all();
    }

    public function getReadySendersByCountryQuery(string $country, string $language): Query
    {
        return $this->readySenderQueryConditions((new Query())->from(SenderProfile::tableName()))
            ->select(new Expression('count(*) as count'))
            ->andWhere(['country' => $country])
            ->andWhere(['language' => $language]);
    }
}
