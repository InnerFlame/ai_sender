<?php

namespace sender\communication\components\containerManager;

use app\models\phoenix\NichesCommTroublesLog;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use sender\interaction\components\InteractionClient;
use sender\logger\components\Log;
use sender\niche\components\NicheQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class PoolUpdater extends Common
{
    /** @var  string $country */
    private $country;
    /** @var NicheQuery $nicheQuery */
    private $nicheQuery;
    /** @var InteractionClient $interactionClient */
    private $interactionClient;
    /** @var Log */
    private $logger;
    public function __construct(NicheQuery $nicheQuery, InteractionClient $interactionClient, Log $logger)
    {
        $this->nicheQuery = $nicheQuery;
        $this->interactionClient = $interactionClient;
        $this->logger = $logger;
    }

    public function updatePool(array $settings)
    {
        $this->country  = $settings['country'];
        $containersInfo = $this->getContainersInfo($settings['country']);
        $excludedIds    = $this->getCurrentIterationProfiles($settings['country']);
        $initialKey     = self::INITIAL_AND_ANSWERS_KEY . '.' . $settings['country'];
        $answerKey      = self::ONLY_ANSWERS_KEY . '.' . $settings['country'];

        $this->updateRedisData($initialKey, $containersInfo['initialAndAnswers']);
        $this->updateRedisData($answerKey, $containersInfo['onlyAnswers']);

        $this->removeExpiredContainers($containersInfo, $answerKey);
        $this->toNextState($containersInfo, $answerKey, $initialKey);

        $shortage = $settings['countStarted'] - count($containersInfo['initialAndAnswers']);
        for ($i = 0; $i < $shortage; $i++) {
            $this->addSenderToInitial($settings, $excludedIds, $containersInfo);
        }

        $this->saveContainersInfo($containersInfo);
    }

    private function updateRedisData(string $key, array $containersInfo)
    {
        $countRecords = \Yii::$app->redis->zcard($key);
        if (!$countRecords) {
            foreach ($containersInfo as $item) {
                \Yii::$app->redis->zadd($key, $item['liveTime'], $item['id']);
            }
        }
    }

    private function toNextState(array &$containersInfo, string $answerKey, string $initialKey)
    {
        $readyToNextState = \Yii::$app->redis->zrangebyscore($initialKey, 0, time());

        \Yii::$app->redis->zremrangebyscore($initialKey, 0, time());

        foreach ($readyToNextState as $senderId) {
            unset($containersInfo['initialAndAnswers'][$senderId]);

            $start    = LifeCycleConfigParser::parse('startTimeAnswer', $this->country);
            $end      = LifeCycleConfigParser::parse('endTimeAnswer', $this->country);
            $liveTime = time() + rand($start, $end);
            \Yii::$app->redis->zadd($answerKey, $liveTime, $senderId);
            $containersInfo['onlyAnswers'][$senderId] = [
                'id'       => $senderId,
                'liveTime' => $liveTime,
            ];

            $this->logger->info('Change state', [
                'senderId' => $senderId
            ]);
        }

        $this->saveContainersInfo($containersInfo);
    }

    private function removeExpiredContainers(array &$containersInfo, string $key)
    {
        $readyToRemove = \Yii::$app->redis->zrangebyscore($key, 0, time());
        \Yii::$app->redis->zremrangebyscore($key, 0, time());

        foreach ($readyToRemove as $senderId) {
            unset($containersInfo['onlyAnswers'][$senderId]);

            $start                 = LifeCycleConfigParser::parse('startTimeCanBeInPull', $this->country);
            $end                   = LifeCycleConfigParser::parse('endTimeCanBeInPull', $this->country);
            $sender                = SenderProfile::findOne($senderId);
            $sender->canBeInPullAt = date('Y-m-d H:i:s', time() + rand($start, $end));
            $sender->save();

            $this->logger->info('Live time expired', [
                'senderId' => $senderId
            ]);

            if (\Yii::$container->get('interactionRouter')->isSeparatedInteraction($sender)) {
                $client = Client::findOne(['profileId' => $senderId]);
                if (!empty($client)) {
                    \Yii::$app->rpcClient->stopRpcContainer($client['target'] . '.' . $senderId, $client['target']);
                }
            } else {
                $this->interactionClient->stopProfile($sender);
            }
        }

        $this->saveContainersInfo($containersInfo);
    }

    private function addSenderToInitial(array $settings, array &$excludedIds, array &$containersInfo)
    {
        $niche      = $this->getMissedNiche($settings, $containersInfo['initialAndAnswers']);
        $language   = $this->getMissedLanguage($settings, $containersInfo['initialAndAnswers'], $niche);
        $sender     = $this->getSender(
            $settings['country'],
            $language,
            $excludedIds,
            $niche
        );

        if (!$sender) {
            \Yii::warning('Ended the users for country:' . $settings['country'] . ' and niche:' . $niche);
            return false;
        }

        if (strtotime($sender->liveTime) <= 0) {
            $start            = LifeCycleConfigParser::parse('startTimeLife', $this->country);
            $end              = LifeCycleConfigParser::parse('endTimeLife', $this->country);
            $sender->liveTime = date('Y-m-d H:i:s', time() + rand($start, $end));
            $sender->save();

            if (LifeCycleConfigParser::parse('canCountryRemove', $this->country)) {
                \Yii::$container->get('senderProfileRemoveScheduler')->addToSchedule($sender, $niche);
            }
        }

        $liveTime = time() + LifeCycleConfigParser::parse('timeInitial', $this->country);
        $containersInfo['initialAndAnswers'][$sender->id] = [
            'id'       => $sender->id,
            'liveTime' => $liveTime,
            'niche'    => $niche,
            'language' => $language
        ];

        \Yii::$app->redis->zadd(self::INITIAL_AND_ANSWERS_KEY . '.' . $settings['country'], $liveTime, $sender->id);

        $this->logger->logSenderProfileInfo('New life cycle', $sender);

        $excludedIds[] = $sender->id;

        return true;
    }

    private function getMissedNiche(array $settings, array $initialAndAnswerInfo)
    {
        $percentNiches = LifeCycleConfigParser::parse('percentNiche', $this->country);
        $groupInfo     = ArrayHelper::index($initialAndAnswerInfo, null, 'niche');
        foreach ($percentNiches as $key => $percentNiche) {
            if ($this->isMissedNiche($key, $percentNiche, $groupInfo, $settings['countStarted'])) {
                return $key;
            }
        }

        return null;
    }

    private function isMissedNiche(string $niche, int $percentNiche, array $groupInfo, int $countStarted)
    {
        if (empty($groupInfo) || empty($groupInfo[$niche])) {
            return true;
        }
        $percentNicheInPull = (count($groupInfo[$niche]) * 100) / $countStarted;
        if ($percentNiche > $percentNicheInPull) {
            return true;
        }

        return false;
    }

    private function getMissedLanguage(array $settings, array $initialAndAnswerInfo, string $niche)
    {
        foreach ($initialAndAnswerInfo as $key => $value) {
            if ($value['niche'] != $niche) {
                unset($initialAndAnswerInfo[$key]);
            }
        }

        $groupInfo        = ArrayHelper::index($initialAndAnswerInfo, null, 'language');
        $percentLanguages = LifeCycleConfigParser::parse('percentLanguage', $this->country);
        foreach ($percentLanguages as $language => $percentLanguage) {
            if ($this->isMissedLanguage($language, $percentLanguage, $groupInfo, $settings['countStarted'])) {
                return $language;
            }
        }

        return null;
    }

    private function isMissedLanguage(string $language, int $percentLanguages, array $groupInfo, int $countStarted)
    {
        if (empty($groupInfo) || empty($groupInfo[$language])) {
            return true;
        }
        $percentLanguageInPull = (count($groupInfo[$language]) * 100) / $countStarted;
        if ($percentLanguages > $percentLanguageInPull) {
            return true;
        }

        return false;
    }

    /**
     * @param string $country
     * @param string $language
     * @param array $excludedIds
     * @param string $niche
     * @return SenderProfile|bool
     */
    private function getSender(string $country, string $language, array $excludedIds, string $niche)
    {
        $query = $this->readySenderQueryConditions(SenderProfile::find())
            ->from(SenderProfile::tableName() . ' as s')
            ->andWhere(['country' => $country])
            ->andWhere(['language' => $language])
            ->andWhere(['not in', 's.id', $excludedIds])
            ->orderBy(new Expression('canBeInPullAt DESC, rand()'));

        $this->nicheQuery->addToQuery($query, $niche, 's');
        if (!$query->count()) {
            NichesCommTroublesLog::log(
                (string) $niche,
                (string) $country,
                NichesCommTroublesLog::PROFILE_TYPE_SENDER,
                'No available sender for this niche'
            );
        }

        return $query->one();
    }
}
