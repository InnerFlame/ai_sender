<?php

namespace sender\communication\components\commProcessor;

use app\config\Constant;
use app\models\phoenix\Group;
use app\models\phoenix\MatchingErrorLog;
use app\models\phoenix\ProfileMessage;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\Schema;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SendRule;
use app\models\phoenix\SendRuleToTraffSource;
use app\models\phoenix\StepMessage;
use app\models\rpc\Client;
use sender\communication\components\containerManager\ContainerManager;
use sender\communication\models\CommunicationSchedule;
use sender\interaction\components\InteractionClient;
use sender\niche\components\NicheQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class CommInitialProcessor extends CommProcessorAbstract
{
    const SENDER_ADULT_AGE_VALUE = 18;

    const RECIPIENT_AGE_GROUPS = [
        ['from' => 18, 'to' => 30, 'stepDown' => 2, 'stepUp' => 17],
        ['from' => 30, 'to' => 45, 'stepDown' => 8, 'stepUp' => 10],
        ['from' => 45, 'stepDown' => 12, 'stepUp' => 2],
    ];

    const SENDER_ATTRIBUTE_PRIORITY_SCORES = [
        'niche' => 3,
        'age'   => 2,
        'city'  => 1
    ];

    /** @var ErrorLogger $errorLogger */
    private $errorLogger;
    /** @var ContainerManager $containersManager */
    private $containersManager;
    /** @var NicheQuery $nicheQuery */
    private $nicheQuery;
    /** @var InteractionClient $interactionClient */
    private $interactionClient;

    public function __construct(
        ErrorLogger $errorLogger,
        ContainerManager $containersManager,
        NicheQuery $nicheQuery,
        InteractionClient $interactionClient
    ) {
        $this->errorLogger       = $errorLogger;
        $this->containersManager = $containersManager;
        $this->nicheQuery        = $nicheQuery;
        $this->interactionClient = $interactionClient;
    }

    /**
     * Process and send initial communication
     * @param CommunicationSchedule $communication
     * @return array | bool
     */
    public function processCommunication(CommunicationSchedule $communication)
    {
        $recipient = $this->getRecipientById($communication->recipientId);

        if ($recipient->isExcludeByTopic) {
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communication->id,
                'entity'          => MatchingErrorLog::EXCLUDED_BY_TOPIC,
                'country'         => $recipient->country,
            ]);
            return false;
        }

        if ($this->isMobilePlatform($recipient->lastUsedPlatform)) {
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communication->id,
                'entity'          => MatchingErrorLog::PLATFORM,
                'country'         => $recipient->country,
            ]);
            return false;
        }

        //if scheduled rule does not existent any more
        $rule = $this->getSendRule($recipient, $communication->groupId);
        if (empty($rule)) {
            $this->storeCommunicationForResending($communication, true);
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communication->id,
                'entity'          => MatchingErrorLog::RULE,
                'country'         => $recipient->country,
            ]);
            return false;
        }

        $activeSenderIds = $this->containersManager->getActiveInitialSenderIds($rule['country']);
        if (empty($activeSenderIds)) {
            $this->storeCommunicationForResending($communication);
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communication->id,
                'entity'          => 'No active sender clients',
                'country'         => $recipient->country,
            ]);
            return false;
        }

        $activeSendersIds = $this->filterSendersByDialoguesCount($activeSenderIds);
        $activeSendersIds = $this->filterSendersByExceededLimit($activeSendersIds);
        /** @var SenderProfile $sender */
        $sender = $this->getActiveSender($communication->recipientId, $activeSendersIds);
        if (empty($sender)) {
            $this->storeCommunicationForResending($communication, true);
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communication->id,
                'entity'          => MatchingErrorLog::SENDER,
                'country'         => $recipient->country,
            ]);
            return false;
        }

        $messageId = $this->getMessageId($recipient, $sender->id, $rule);
        if (empty($messageId)) {
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communication->id,
                'entity'          => MatchingErrorLog::MESSAGE,
                'country'         => $recipient->country,
            ]);
            return false;
        }

        $communication->attemptsCount += 1;
        $communication->senderId = $sender->id;
        $communication->messageId = $messageId;
        $communication->save();

        $this->incrementCountSendMessage($communication->senderId);
        if (\Yii::$container->get('interactionRouter')->isSeparatedInteraction($sender)) {
            $rpcClient = Client::findOne([
                'target'    => Constant::TARGET_PHOENIX,
                'status'    => Client::STATUS_STARTED,
                'type'      => Client::TYPE_COMMUNICATION,
                'profileId' => $sender->id,
            ]);
            if (!$rpcClient) {
                //@todo add log
                return false;
            }
            $this->callCommunicationApi($recipient, $rpcClient, $communication);
        } else {
            $result = $this->interactionClient->sendMessage(
                $sender,
                $recipient,
                \Yii::$app->message->getMessageBody($communication)
            );
            if (!empty($result)
                && $result['clientStatus'] == 'connected'
                && !empty($result['wsResponse'])
                && empty($result['wsResponse']['error'])
            ) {
                $communication->sentAt = date('Y-m-d H:i:s');
                $communication->save();
            }
        }

        return true;
    }

    /**
     * @param array $senderIds
     * @return array
     */
    private function filterSendersByDialoguesCount(array $senderIds)
    {
        $query = (new Query())
            ->select('senderId, COUNT(DISTINCT recipientId) dialoguesCount')
            ->from(CommunicationSchedule::tableName())
            ->where(['senderId' => $senderIds])
            ->andWhere(
                'sentAt >= :dialoguesPeriod',
                ['dialoguesPeriod' => date('Y-m-d H:i:s', time() - $this->dialoguesPeriod)]
            )
            ->groupBy('senderId');
        $recentlyCommunicated = $query->all();

        $recentlyCommunicatedIds = ArrayHelper::getColumn($recentlyCommunicated, 'senderId');
        $notCommunicatedIds = array_diff($senderIds, $recentlyCommunicatedIds);
        $recentlyCommunicated = array_filter(
            $recentlyCommunicated,
            function ($item) {
                return $item['dialoguesCount'] < $this->dialoguesLimit;
            }
        );

        $recentlyCommunicatedIds = ArrayHelper::getColumn($recentlyCommunicated, 'senderId');

        return array_merge($notCommunicatedIds, $recentlyCommunicatedIds);
    }

    /**
     * @param array $senderIds
     * @return array
     */
    private function filterSendersByExceededLimit(array $senderIds)
    {
        $filteredSenders = [];
        foreach ($senderIds as $senderId) {
            if (!$this->isExceededLimitForSend($senderId)) {
                $filteredSenders[] = $senderId;
            }
        }

        return $filteredSenders;
    }

    /**
     * @param int $recipientId
     * @param array $activeSenderIds
     * @return array|\yii\db\ActiveRecord|null
     */
    private function getActiveSender(int $recipientId, array $activeSenderIds)
    {
        $recipient = $this->getRecipientById($recipientId);
        $recipientIds = $this->getRecipientIdsByMultiId($recipient);
        $senderIdsFilteredBySendMultiId = $this->filterSenderBySendMultiId($activeSenderIds, $recipientIds);
        $sender = $this->getSender(
            $senderIdsFilteredBySendMultiId,
            $activeSenderIds,
            $recipient,
            $recipient->niche
        );

        if (empty($sender)) {
            return [];
        }

        return $sender;
    }

    /**
     * getting recipients id by multiProfileId
     * @param RecipientProfile $recipient
     * @return array
     */
    private function getRecipientIdsByMultiId(RecipientProfile $recipient)
    {
        if (empty($recipient->multiProfileId)) {
            return [$recipient->id];
        }

        return RecipientProfile::find()
            ->select('id')
            ->where(['multiProfileId' => $recipient->multiProfileId])
            ->asArray()
            ->column();
    }

    /**
     * removes from the list activeSenderIds
     * whe sent the message to recipient with one multiProfileId
     * @param int[] $activeSenderIds
     * @param int[] $recipientIds
     * @return mixed
     */
    private function filterSenderBySendMultiId(array $activeSenderIds, array $recipientIds)
    {
        $exclusionSenderIds = (new Query())->select('distinct(senderId)')
            ->from(CommunicationSchedule::tableName())
            ->where([
                'recipientId' => $recipientIds,
                'senderId'    => $activeSenderIds,
            ])
            ->column();

        foreach ($exclusionSenderIds as $id) {
            if (($key = array_search($id, $activeSenderIds)) !== false) {
                unset($activeSenderIds[$key]);
            }
        }

        return $activeSenderIds;
    }

    private function getSender(
        array $senderIdsFilteredBySendMultiId,
        array $activeSenderIds,
        RecipientProfile $recipient,
        ?string $niche = null
    ) {
        if (!empty($senderIdsFilteredBySendMultiId)) {
            $sender = $this->getFilteredActiveSender($recipient, $senderIdsFilteredBySendMultiId, $niche);
            if (!empty($sender)) {
                return $sender;
            }
        }

        $activeSenderIds = $this->filterSenderBySendPlaceholder($recipient->id, $activeSenderIds);
        $activeSenderIds = $this->filterSenderBySentInDay($recipient->id, $activeSenderIds);
        if (empty($activeSenderIds)) {
            return [];
        }

        $sender = $this->getFilteredActiveSender($recipient, $activeSenderIds, $niche);

        return $sender;
    }

    /**
     * @param RecipientProfile $recipient
     * @param array $activeSenderIds
     * @param string|null $niche
     * @return SenderProfile|array
     */
    private function getFilteredActiveSender(RecipientProfile $recipient, array $activeSenderIds, ?string $niche)
    {
        $query = SenderProfile::find()
            ->from(SenderProfile::tableName() . ' as s')
            ->select('s.*')
            ->where([
                'bannedAt'      => '0000-00-00 00:00:00',
                'deactivatedAt' => '0000-00-00 00:00:00',
                'pausedAt'      => '0000-00-00 00:00:00',
                'photoStatus'   => SenderProfile::STATUS_APPROVED,
                'userType'      => SenderProfile::TYPE_SENDER,
                's.id'          => $activeSenderIds,
            ])
            ->andWhere('originId IS NOT NULL')
            ->orderBy(new Expression('rand()'));

        if (!empty($recipient->getMultiLanguagesList())) {
            $query->andWhere(['language' => $recipient->getLanguage()]);
        }

        if (!empty($niche)) {
            $this->nicheQuery->addToQueryLeftLoinAndJoinCondition($query, $niche, 's');
        }

        $senders = $query->asArray()->all();
        if (empty($senders)) {
            return [];
        }

        $mostSuitableSender = $this->getMostSuitableSender($senders, $recipient);

        return $mostSuitableSender;
    }

    /**
     * @param SenderProfile[] $senderProfiles
     * @param RecipientProfile $recipient
     * @return array|SenderProfile
     */
    private function getMostSuitableSender(array $senderProfiles, RecipientProfile $recipient)
    {
        $agePriorityConfigs  = \Yii::$app->yaml->parse('agePriority');
        $mostSuitableSenders = [];

        foreach ($senderProfiles as $sender) {
            $senderFitScore = 0;

            if (!empty($recipient->city) && ($sender['city'] == $recipient->city)) {
                $senderFitScore += self::SENDER_ATTRIBUTE_PRIORITY_SCORES['city'];
            }

            if (!empty($sender['isNicheSender'])) {
                $senderFitScore += self::SENDER_ATTRIBUTE_PRIORITY_SCORES['niche'];
            }

            if (is_array($agePriorityConfigs)
                && ($recipient->country == $agePriorityConfigs['country'])
                && ($recipient->site == $agePriorityConfigs['site'])
            ) {
                if (!empty($recipient->birthday) && $recipient->birthday != '0000-00-00 00:00:00') {
                    $diapasonsData = $this->getPreferableAgeDiapasons($recipient->birthday);

                    if (!empty($diapasonsData)) {
                        list ($birthdayFrom, $birthdayTo) = $diapasonsData;
                        $senderBirthday = strtotime($sender['birthday']);

                        if (($senderBirthday > $birthdayFrom) && ($senderBirthday <= $birthdayTo)) {
                            $senderFitScore += self::SENDER_ATTRIBUTE_PRIORITY_SCORES['age'];
                        }
                    }
                }
            }


            $mostSuitableSenders[$sender['id']] = $senderFitScore;
        }

        if (empty($mostSuitableSenders)) {
            $suitableSender = array_shift($senderProfiles);
        } else {
            arsort($mostSuitableSenders);
            reset($mostSuitableSenders);
            $suitableSender = ['id' => key($mostSuitableSenders)];
        }

        if (empty($suitableSender) || empty($suitableSender['id'])) {
            return [];
        }

        $suitableSender = SenderProfile::findOne((int) $suitableSender['id']);

        return $suitableSender;
    }

    /**
     * Calculate preferable age diapasons from recipient age group for sender
     *
     * @param string $recipientBirthday
     * @return array
     */
    private function getPreferableAgeDiapasons(string $recipientBirthday): array
    {
        $recipientAgeGroup = $this->getRecipientAgeGroup($recipientBirthday);

        if (is_null($recipientAgeGroup)) {
            return [];
        }

        $adultAge                 = self::SENDER_ADULT_AGE_VALUE;
        $preferableYoungerAgeStep = self::RECIPIENT_AGE_GROUPS[$recipientAgeGroup]['stepDown'];
        $preferableOlderAgeStep   = self::RECIPIENT_AGE_GROUPS[$recipientAgeGroup]['stepUp'];

        $minAgeBirthday = strtotime(" - {$adultAge} year");

        $preferableBirthdayTo = strtotime($recipientBirthday . " + {$preferableYoungerAgeStep} year");
        if ($preferableBirthdayTo > $minAgeBirthday) {
            $preferableBirthdayTo = $minAgeBirthday;
        }

        $preferableBirthdayFrom = strtotime($recipientBirthday . " - {$preferableOlderAgeStep} year");

        return [
            $preferableBirthdayFrom,
            $preferableBirthdayTo,
        ];
    }

    /**
     * @param string $recipientBirthday
     * @return int|null
     */
    private function getRecipientAgeGroup(string $recipientBirthday)
    {
        $recipientAge = (int) date_diff(date_create($recipientBirthday), date_create('now'))->y;

        foreach (self::RECIPIENT_AGE_GROUPS as $groupKey => $groupData) {
            if (!isset($groupData['to'])) {
                if ($recipientAge >= $groupData['from']) {
                    return $groupKey;
                }
                continue;
            }

            if (($recipientAge >= $groupData['from']) && ($recipientAge < $groupData['to'])) {
                return $groupKey;
            }
        }

        return null;
    }

    /**
     * removes from the list activeSenderIds
     * who sent the placeholder to the recipient
     * @param int $recipientId
     * @param int[] $activeSenderIds
     * @return mixed
     */
    private function filterSenderBySendPlaceholder(int $recipientId, array $activeSenderIds)
    {
        $exclusionSenderIds = (new Query())->select('distinct(c.senderId)')
            ->from(CommunicationSchedule::tableName() . ' as c')
            ->innerJoin(StepMessage::tableName() . ' as sm', 'sm.id=c.messageId')
            ->where([
                'c.recipientId'     => $recipientId,
                'c.senderId'        => $activeSenderIds,
                'sm.hasPlaceholder' => 1
            ])
            ->andWhere(['>', 'c.step', 0])
            ->column();

        foreach ($exclusionSenderIds as $id) {
            if (($key = array_search($id, $activeSenderIds)) !== false) {
                unset($activeSenderIds[$key]);
            }
        }

        return $activeSenderIds;
    }

    /**
     * removes from the list activeSenderIds
     * who sent the chat on this days to the recipient
     * @param int $recipientId
     * @param int[] $activeSenderIds
     * @return mixed
     */
    private function filterSenderBySentInDay(int $recipientId, array $activeSenderIds)
    {
        $exclusionSenderIds = (new Query())->select('senderId, max(SendStartedAt) as maxSend')
            ->from(CommunicationSchedule::tableName())
            ->where([
                'recipientId' => $recipientId,
                'senderId'    => $activeSenderIds,
            ])
            ->groupBy('senderId, recipientId')
            ->having(['>', 'maxSend', date('Y-m-d H:i:s', time() - 86400)])
            ->column();

        foreach ($exclusionSenderIds as $id) {
            if (($key = array_search($id, $activeSenderIds)) !== false) {
                unset($activeSenderIds[$key]);
            }
        }

        return $activeSenderIds;
    }

    /**
     * @param RecipientProfile $recipient
     * @param int $senderId
     * @param array $rule
     * @return false|string|null
     */
    private function getMessageId(RecipientProfile $recipient, int $senderId, array $rule)
    {
        $recipientUsedMessageIds = $this->getRecipientUsedMessageIds($recipient->id);
        $senderUsedMessageIds    = $this->getSenderUsedMessageIds($senderId);

        $usedMessageIds = array_merge($recipientUsedMessageIds, $senderUsedMessageIds);

        $language = $recipient->getLanguage();
        $messageId = $this->getMessage($rule, $language, $usedMessageIds);
        if (empty($messageId)) {
            $messageId = $this->getMessage($rule, $language, $recipientUsedMessageIds);
        }

        return $messageId;
    }

    private function getRecipientUsedMessageIds(int $recipientId)
    {
        return $this->getUsedMessageIds(['recipientId' => $recipientId, 'step' => 0]);
    }

    private function getSenderUsedMessageIds(int $senderId)
    {
        return $this->getUsedMessageIds(['senderId' => $senderId, 'step' => 0]);
    }

    private function getUsedMessageIds(array $condition)
    {
        return (new Query())
            ->select('messageId')
            ->from(CommunicationSchedule::tableName())
            ->where($condition)
            ->column();
    }

    private function getMessage(array $rule, string $language, array $usedMessageIds)
    {
        return (new Query())
            ->select('id')
            ->from(ProfileMessage::tableName())
            ->where(['country'        => $rule['country']])
            ->andWhere(['language'    => $language])
            ->andWhere(['sexuality'   => $rule['sexuality']])
            ->andWhere(['gender'      => $rule['gender']])
            ->andWhere(['sendMode'    => $rule['sendMode']])
            ->andWhere(['isActive'    => 1])
            ->andWhere(['not in','id', $usedMessageIds])
            ->orderBy(new Expression('rand()'))
            ->limit(1)
            ->scalar();
    }

    /**
     * @param RecipientProfile $recipient
     * @param int $groupId
     * @return array|bool
     */
    private function getSendRule(RecipientProfile $recipient, int $groupId)
    {
        $recipientProject = RecipientProfile::getProjectFromSiteHash($recipient->site);
        $traffSource = $recipient->source ?? '';

        $query = (new Query())
            ->select('r.site,r.project,r.country,g.sendMode,sch.sexuality,sch.gender,ts.traffSource')
            ->from(SendRule::tableName() . ' as r')
            ->innerJoin(Schema::tableName() . ' as sch', 'r.schemaId=sch.id')
            ->innerJoin(Group::tableName() . ' as g', 'sch.id=g.schemaId')
            ->leftJoin(SendRuleToTraffSource::tableName() . 'as ts', 'r.id=ts.sendRuleId')
            ->where(['g.id'         => $groupId])
            ->andWhere(['r.country' => $recipient->country])
            ->andWhere([
                'or',
                ['and', 'r.site IS NULL', 'r.project IS NULL'],
                ['r.site' => $recipient->site],
                ['r.project' => $recipientProject],
            ])
            ->andWhere(['or',
                ['ts.traffSource' => null],
                ['ts.traffSource' => $traffSource],
            ])
        ;

        $sendRule = $query->one();

        return $sendRule;
    }
}
