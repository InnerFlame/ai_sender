<?php

namespace sender\communication\components\commProcessor;

use app\models\phoenix\MatchingErrorLog;

class ErrorLogger
{
    /**
     * @param array $data
     */
    public function saveMatchingError(array $data)
    {
        \Yii::$app->db->createCommand()->insert(
            MatchingErrorLog::tableName(),
            [
                'communicationId' => $data['communicationId'],
                'entity'          => $data['entity'],
                'country'         => $data['country'] ?? null,
            ]
        )->execute();
    }
}