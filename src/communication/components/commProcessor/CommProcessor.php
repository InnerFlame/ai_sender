<?php

namespace sender\communication\components\commProcessor;

use app\components\phoenix\communication\CommSchedulerComponent;
use app\config\Constant;
/** ------ Yii ------ */
use yii\db\Query;
use yii\helpers\ArrayHelper;
/** ------ Components ------ */
use app\components\amqp\InternalAmqpComponent;
use app\components\rpc\RpcClientComponent;
use app\components\rpc\RpcMethodsComponent;
/** ------ Models ------ */
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;

/**
 * Component to process communications
 */
class CommProcessor
{
    protected $limitAttempt     = 3;
    protected $target           = Constant::TARGET_PHOENIX;

    private $commEvaProcessor;
    private $commInitialProcessor;
    private $internalAMQP;
    private $comScheduler;
    private $recipientChecker;

    public function __construct(
        CommEvaProcessor $commEvaProcessor,
        CommInitialProcessor $commInitialProcessor,
        InternalAmqpComponent $internalAMQP,
        CommSchedulerComponent $comScheduler,
        RecipientChecker $recipientChecker
    )
    {
        $this->commEvaProcessor     = $commEvaProcessor;
        $this->commInitialProcessor = $commInitialProcessor;
        $this->internalAMQP         = $internalAMQP;
        $this->comScheduler         = $comScheduler;
        $this->recipientChecker     = $recipientChecker;

    }

    /**
     * Select ready initial communications and add them to queue for sending
     *
     * @return bool
     */
    public function prepareCommunications()
    {
        $itemsToSend    = [];
        $dateStart      = date('Y-m-d H:i:s', time() - 900);
        $dateEnd        = date('Y-m-d H:i:s');
        $communications = $this->getReadyCommunication(300, $dateStart, $dateEnd);
        if (empty($communications)) {
            return false;
        }

        $communications = ArrayHelper::index($communications, 'id');
        $this->setSendStartedAt($communications);
        foreach ($communications as $id => $communication) {
            $recipientId = $communication['recipientId'];
            $schemaId    = $communication['schemaId'];
            if (!$this->recipientChecker->isRecipientSuitable($recipientId, $schemaId, $id)) {
                continue;
            }

            $this->comScheduler->setNextGroupForRecipientSchema($recipientId, $schemaId);

            $itemsToSend[] = ['communicationId' => $id];
        }

        if (!empty($itemsToSend)) {
            $this->internalAMQP->batchPublish('communicationsToSend', $itemsToSend);
        }

        return true;
    }

    /**
     * Consume communication events to rabbit
     */
    public function processCommunications()
    {
        $this->internalAMQP->consumeEvents('communicationsToSend', [$this, 'communicationHandler']);
    }

    /**
     * Process communications from queue
     * @param $message
     * @return bool
     */
    public function communicationHandler($message)
    {
        $communication = CommunicationSchedule::findOne($message['communicationId']);
        if ($communication->step == 0) {
            $this->commInitialProcessor->processCommunication($communication);
        } else {
            $this->commEvaProcessor->processCommunication($communication);
        }

        return true;
    }

    /**
     * @param SenderProfile $profile
     * @return bool
     */
    public function loginClient(SenderProfile $profile)
    {
        /**@var $rpcMethodsComponent RpcMethodsComponent */
        $rpcMethodsComponent = \Yii::$app->rpcMethods;
        $rpcMethodsComponent->loginRpcClient($profile, Client::TYPE_COMMUNICATION);
        return true;
    }

    /**
     * @param $clientName
     */
    public function startClient($clientName)
    {
        /**@var $rpcClientComponent RpcClientComponent */
        $rpcClientComponent = \Yii::$app->rpcClient;
        $additional         = [
            'clientName' => $clientName,
            'target'     => $this->target,
        ];

        $rpcClientComponent->rpcCall('client.' . $clientName, 'start', [], $additional);
    }

    /**
     * Get ids of ready to send communications
     * @param integer $limit
     * @param string $dateStart
     * @param string $dateEnd
     * @return array
     */
    private function getReadyCommunication(int $limit = 200, $dateStart = null, $dateEnd = null)
    {
        if (empty($dateStart)) {
            $dateStart = date('Y-m-d H:i:s', time() - 1800);
        }

        if (empty($dateEnd)) {
            $dateEnd = date('Y-m-d H:i:s');
        }

        $query = (new Query())
            ->select('id, recipientId, schemaId')
            ->from(CommunicationSchedule::tableName())
            ->where('timeToSend < :currentTime', ['currentTime' => $dateEnd])
            ->andWhere('timeToSend >= :lastHour', ['lastHour' => $dateStart])
            ->andWhere("sendStartedAt = '0000-00-00 00:00:00'")
            ->andWhere("attemptsCount <= :attemptsCount", [':attemptsCount' => $this->limitAttempt])
            ->andWhere("isSenderBanned = 0")
            ->limit($limit);

        $communicationIds = $query->all();

        return $communicationIds;
    }

    private function setSendStartedAt($communications)
    {
        \Yii::$app->db->createCommand()->update(
            CommunicationSchedule::tableName(),
            ['sendStartedAt' => date('Y-m-d H:i:s')],
            ['id' => array_keys($communications)]
        )->execute();
    }
}
