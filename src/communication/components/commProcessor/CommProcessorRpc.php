<?php

namespace sender\communication\components\commProcessor;

use app\components\amqp\InternalAmqpComponent;
use sender\communication\components\containerManager\ContainerManager;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderWithoutPhotoLog;
use app\models\rpc\Container;

class CommProcessorRpc
{
    const QUEUE_SENT_MESSAGE = 'rpcSentMessageResponse';

    /** @var InternalAmqpComponent $internalAMQP */
    private $internalAMQP;
    /** @var ContainerManager $containersManager */
    private $containersManager;

    public function __construct(
        InternalAmqpComponent $internalAMQP,
        ContainerManager $containersManager
    ) {
        $this->internalAMQP      = $internalAMQP;
        $this->containersManager = $containersManager;
    }

    /**
     * Consume messages events to rabbit
     */
    public function processSentMessage()
    {
        $this->internalAMQP->consumeEvents(self::QUEUE_SENT_MESSAGE, [$this, 'sentMessageHandler']);
    }

    public function sentMessageHandler(array $message): bool
    {
        $messageId     = $message['rpcCallInfo']['additional']['messageId'];
        $communication = CommunicationSchedule::findOne(['id' => $messageId]);
        if ($communication) {
            if ($this->isNoPhoto($message)) {
                $sender = SenderProfile::findOne($communication->senderId);
                if ($sender) {
                    $sender->photoStatus = SenderProfile::STATUS_NO_PHOTO;
                    $sender->save();

                    SenderWithoutPhotoLog::logData($sender);

                    $this->containersManager->deleteContainerWithoutPhoto($sender);

                    if (\Yii::$container->get('interactionRouter')->isSeparatedInteraction($sender)) {
                        $container = Container::findOne(['profileId' => $sender->id]);
                        if (!empty($container)) {
                            $containerName = $container['target'] . '.' . $sender->id;
                            \Yii::$app->rpcClient->stopRpcContainer($containerName);
                        }
                    }
                }
            } else {
                $communication->sentAt = date('Y-m-d H:i:s');
                $communication->save();
            }
        }

        return true;
    }

    private function isNoPhoto(array $message): bool
    {
        return !empty($message['incomingData']['result']['result']['error'])
        && $message['incomingData']['result']['result']['error'] == 'photo communication restrict';
    }
}
