<?php

namespace sender\communication\components\commProcessor;

use app\config\Constant;
use app\models\phoenix\MatchingErrorLog;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use sender\communication\components\containerManager\ContainerManager;
use sender\communication\models\CommunicationSchedule;
use sender\interaction\components\InteractionClient;
use yii\db\Query;

class CommEvaProcessor extends CommProcessorAbstract
{
    /** @var ErrorLogger $errorLogger */
    private $errorLogger;
    /** @var ContainerManager $containersManager */
    private $containersManager;
    /** @var InteractionClient $interactionClient */
    private $interactionClient;

    public function __construct(
        ErrorLogger $errorLogger,
        ContainerManager $containersManager,
        InteractionClient $interactionClient
    ) {
        $this->errorLogger       = $errorLogger;
        $this->containersManager = $containersManager;
        $this->interactionClient = $interactionClient;
    }

    /**
     * Process and send answer communication
     * @param CommunicationSchedule $communication
     * @return array | bool
     */
    public function processCommunication(CommunicationSchedule $communication)
    {
        $recipient = $this->getRecipientById($communication->recipientId);
        if ($this->isMobilePlatform($recipient->lastUsedPlatform)) {
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communication->id,
                'entity'          => MatchingErrorLog::PLATFORM,
                'country'         => $recipient->country,
            ]);
            return false;
        }

        $sender = SenderProfile::findOne($communication->senderId);

        if ($this->containersManager->isClientStarted($sender) == false) {
            $this->storeCommunicationForResending($communication, true);
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communication->id,
                'entity'          => MatchingErrorLog::CLIENT,
                'country'         => $recipient->country,
            ]);
            return false;
        }

        if (strtotime($sender->bannedAt) > 0) {
            $this->storeCommunicationForResending($communication);
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communication->id,
                'entity'          => MatchingErrorLog::SENDER_BANNED,
                'country'         => $recipient->country,
            ]);
            return false;
        }

        if ($this->isNewDialogueAvailable($communication->senderId, $communication->recipientId) == false) {
            $this->storeCommunicationForResending($communication);
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communication->id,
                'entity'          => 'No new dialogues available',
                'country'         => $recipient->country,
            ]);
            return false;
        }

        if ($this->isExceededLimitForSend($communication->senderId)) {
            $this->storeCommunicationForResending($communication);
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communication->id,
                'entity'          => 'Limit for sender exceeded',
                'country'         => $recipient->country,
            ]);
            return false;
        }

        $this->incrementCountSendMessage($communication->senderId);

        $communication->attemptsCount += 1;
        $communication->save();

        if (\Yii::$container->get('interactionRouter')->isSeparatedInteraction($sender)) {
            $rpcClient = Client::findOne([
                'target'    => Constant::TARGET_PHOENIX,
                'status'    => Client::STATUS_STARTED,
                'type'      => Client::TYPE_COMMUNICATION,
                'profileId' => $sender->id,
            ]);
            if (!$rpcClient) {
                //@todo add log
                return false;
            }
            $this->callCommunicationApi($recipient, $rpcClient, $communication);
        } else {
            $result = $this->interactionClient->sendMessage(
                $sender,
                $recipient,
                \Yii::$app->message->getMessageBody($communication)
            );
            if (!empty($result)
                && $result['clientStatus'] == 'connected'
                && !empty($result['wsResponse'])
                && empty($result['wsResponse']['error'])
            ) {
                $communication->sentAt = date('Y-m-d H:i:s');
                $communication->save();
            }
        }

        return true;
    }

    /**
     * @param int $senderId
     * @param int|null $recipientId
     * @return bool
     */
    private function isNewDialogueAvailable(int $senderId, int $recipientId = null)
    {
        $query = (new Query())
            ->select('recipientId')
            ->from(CommunicationSchedule::tableName())
            ->where(['senderId' => $senderId])
            ->andWhere(
                'sentAt >= :dialoguesPeriod',
                ['dialoguesPeriod' => date('Y-m-d H:i:s', time() - $this->dialoguesPeriod)]
            )
            ->groupBy('recipientId');

        $lastDialogues = $query->all();

        //if limit is not reached
        if (count($lastDialogues) < $this->dialoguesLimit) {
            return true;
        }

        //if limit reached, but our recipient in recent dialogues
        foreach ($lastDialogues as $dialogue) {
            if ($dialogue['recipientId'] == $recipientId) {
                return true;
            }
        }

        return false;
    }
}
