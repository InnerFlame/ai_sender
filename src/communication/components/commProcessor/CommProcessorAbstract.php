<?php

namespace sender\communication\components\commProcessor;

use app\components\rpc\RpcClientComponent;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\RecipientProfile;
use app\models\rpc\Client;
use app\models\rpc\Container;
use yii\redis\Connection;

abstract class CommProcessorAbstract
{
    protected $limitAttempt     = 3;
    protected $dialoguesLimit   = 3;
    protected $dialoguesPeriod  = 90;
    protected $sendMessageLimit = 10;

    /**
     * Call API to send communications
     * @param RecipientProfile $recipient
     * @param Client $rpcClient
     * @param CommunicationSchedule $communication
     * @return bool
     */
    protected function callCommunicationApi(
        RecipientProfile $recipient,
        Client $rpcClient,
        CommunicationSchedule $communication
    ) {
        /**@var $rpcClientComponent RpcClientComponent */
        $rpcClientComponent = \Yii::$app->rpcClient;

        $data = [
            'toId'              => $recipient->remoteId,
            'messageType'       => 'chat',
            'copyPasteDetected' => false,
            'subject'           => 'hi',
            'message'           => \Yii::$app->message->getMessageBody($communication),
        ];

        $additional = [
            'messageId'  => $communication->id,
            'clientName' => $rpcClient['name'],
            'clientType' => $rpcClient['type'],
        ];

        $rpcClientName = 'client.' . $rpcClient['name'];
        $rpcClientComponent->rpcCall($rpcClientName, 'sendMessage', [$data], $additional);

        Container::updateLastTimeActivity($rpcClient['name'], $rpcClient['target']);

        return true;
    }

    /**
     * @param int $senderId
     */
    protected function incrementCountSendMessage(int $senderId)
    {
        /**@var $redis Connection */
        $redis     = \Yii::$app->redis;
        $key       = 'countSendMessage' . $senderId;
        $expire    = 60000;
        $ttl       = \Yii::$app->redis->executeCommand('PTTL', [$key]);
        if ($ttl > 0) {
            $expire = $ttl;
        }

        $redis->incr($key);
        $redis->executeCommand('PEXPIRE', [$key, $expire]);
    }

    /**
     * @param $recipientId
     * @return RecipientProfile|null
     */
    protected function getRecipientById($recipientId)
    {
        return RecipientProfile::findOne($recipientId);
    }

    /**
     * @param CommunicationSchedule $communication
     * @param bool $incrementAttempts
     */
    protected function storeCommunicationForResending(
        CommunicationSchedule $communication,
        bool $incrementAttempts = false
    ) {
        if ($incrementAttempts) {
            $communication->attemptsCount += 1;
            $communication->save();
        }

        $communications = CommunicationSchedule::find()
            ->where([
                'senderId'    => $communication->senderId,
                'recipientId' => $communication->recipientId,
                'groupId'     => $communication->groupId,
                'schemaId'    => $communication->schemaId,
            ])
            ->andWhere(['>=', 'timeToSend', $communication->timeToSend])
            ->all();

        foreach ($communications as $item) {
            /** @var CommunicationSchedule $item */
            $item->timeToSend    = date('Y-m-d H:i:s', strtotime($item->timeToSend) + 30);
            $item->sendStartedAt = '0000-00-00 00:00:00';
            $item->save();
        }
    }

    /**
     * @param $lastUsedPlatform
     * @return bool
     */
    protected function isMobilePlatform($lastUsedPlatform): bool
    {
        return in_array(
            $lastUsedPlatform,
            [RecipientProfile::ANDROID_APP, RecipientProfile::IOS_APP]
        );
    }

    /**
     * @param $senderId
     * @return bool
     */
    protected function isExceededLimitForSend($senderId)
    {
        /**@var $redis Connection */
        $redis = \Yii::$app->redis;

        $key = 'countSendMessage' . $senderId;
        if ($redis->get($key) >= $this->sendMessageLimit) {
            return true;
        }

        return false;
    }
}