<?php

namespace sender\communication\components\commProcessor;

use app\components\common\YamlComponent;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\RecipientSchema;
use app\models\phoenix\Schema;
use app\models\phoenix\SendExclusionsRules;

class RecipientChecker
{
    private $errorLogger;

    public function __construct(ErrorLogger $errorLogger)
    {
        $this->errorLogger = $errorLogger;
    }

    /**
     * @param $recipientId
     * @param $schemaId
     * @param $communicationId
     * @return bool
     */
    public function isRecipientSuitable($recipientId, $schemaId, $communicationId)
    {
        $recipient         = RecipientProfile::findOne($recipientId);
        $schema            = Schema::findOne($schemaId);
        $suitableCondition = (!empty($recipient) && empty($schema) && $this->hasSchema($recipientId));

        if ($suitableCondition) {
            return $this->hasExclusionDetails($recipient, $communicationId, $schemaId);
        }

        if (empty($recipient)) {
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communicationId,
                'entity'          => 'Not suitable: not existent recipient',
            ]);
            return false;
        }

        if ($recipient->complained == 1) {
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communicationId,
                'entity'          => 'Not suitable: complained 1',
            ]);
            return false;
        }

        if (empty($schema)) {
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communicationId,
                'entity'          => 'Not suitable: empty schema',
                'country'         => $recipient->country,
            ]);
            return false;
        }

        if ($recipient->isPaid != $schema->isPaid) {
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communicationId,
                'entity'          => '$recipient->isPaid != $schema->isPaid',
                'country'         => $recipient->country,
            ]);
            return false;
        }

        if (!$recipient->searchable) {
            $this->errorLogger->saveMatchingError([
                'communicationId' => $communicationId,
                'entity'          => 'Not suitable: not searchable',
                'country'         => $recipient->country,
            ]);
            return false;
        }

        return $this->hasExclusionDetails($recipient, $communicationId, $schemaId);
    }

    /**
     * @param $recipient RecipientProfile
     * @param $communicationId
     * @return bool
     */
    private function hasExclusionDetails($recipient, $communicationId, $schemaId)
    {
        /**@var $yamlParserComponent YamlComponent */
        $yamlParserComponent  = \Yii::$app->yaml;

        /**@var $sendExclusionDetails SendExclusionsRules [] */
        $sendExclusionDetails = SendExclusionsRules::find()->where(['isActive' => 1])->all();
        if (!$sendExclusionDetails) {
            return true;
        }

        foreach($sendExclusionDetails as $condition)
        {
            if (!empty($condition->regDateFrom) && ($recipient->createdAt < $condition->regDateFrom)) {
                continue;
            }

            if (!empty($condition->regDateTo) && ($recipient->createdAt > $condition->regDateTo)) {
                continue;
            }

            if (!empty($condition->countriesArray) && !in_array($recipient->country, $condition->countriesArray)) {
                continue;
            }

            $siteHash = $yamlParserComponent->parseByKey('site', $recipient->site)['hash'];

            if (!empty($condition->sitesIdsArray) && !in_array($siteHash, $condition->sitesIdsArray)) {
                continue;
            }

            if (!empty($condition->sourcesArray) && !in_array($recipient->source, $condition->sourcesArray)) {
                continue;
            }

            if (!empty($condition->schemaIdsArray) && !in_array($schemaId, $condition->schemaIdsArray)) {
                continue;
            }

            if (!empty($condition->platformsArray) && !in_array($recipient->registrationPlatform, $condition->platformsArray)) {
                continue;
            }

            $this->errorLogger->saveMatchingError([
                'communicationId' => $communicationId,
                'entity'          => 'Recipient has exclusion details',
                'country'         => $recipient->country,
            ]);
            return false;
        }

        return true;
    }

    /**
     * @param $recipientId
     * @return bool
     */
    private function hasSchema($recipientId)
    {
        if (RecipientSchema::findOne(['recipientId' => $recipientId])) {
            return true;
        }
        return false;
    }
}