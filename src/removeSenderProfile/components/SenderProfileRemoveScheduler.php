<?php

namespace sender\removeSenderProfile\components;

use sender\communication\components\containerManager\LifeCycleConfigParser;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileRemoveSchedule;

class SenderProfileRemoveScheduler
{
    public function scheduleRemove(int $limit = 200)
    {
        /** @var SenderProfile[] $sendersToRemove */
        $sendersToRemove = SenderProfile::find()
            ->from(SenderProfile::tableName() . ' as s')
            ->innerJoin(SenderProfileRemoveSchedule::tableName() . ' as sr', 's.id=sr.senderId')
            ->where('s.liveTime <= :now', [':now' => date('Y-m-d H:i:s')])
            ->andWhere('sr.id IS NULL')
            ->limit($limit)
            ->all();

        foreach ($sendersToRemove as $senderProfile) {
            $this->addToSchedule($senderProfile);
        }
    }

    private function addToSchedule(SenderProfile $senderProfile, int $timeToRemove = 0)
    {
        if (strtotime($senderProfile->removedAt) > 0) {
            return false;
        }

        if ($timeToRemove > 0) {
            $time = time() + $timeToRemove;
        } else {
            $start = LifeCycleConfigParser::parse('startTimeDelete', $senderProfile->country);
            $end   = LifeCycleConfigParser::parse('endTimeDelete', $senderProfile->country);
            $time  = strtotime($senderProfile->liveTime) + rand($start, $end);
        }

        $senderProfileDeleteSchedule               = new SenderProfileRemoveSchedule();
        $senderProfileDeleteSchedule->senderId     = $senderProfile->id;
        $senderProfileDeleteSchedule->timeToRemove = date('Y-m-d H:i:s', $time);
        $senderProfileDeleteSchedule->save();

        return true;
    }
}
