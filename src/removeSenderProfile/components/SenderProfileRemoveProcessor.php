<?php

namespace sender\removeSenderProfile\components;

use app\models\phoenix\SenderEmail;
use app\models\phoenix\SenderPhoto;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileRemoveSchedule;
use sender\interaction\components\InteractionClient;

class SenderProfileRemoveProcessor
{
    /** @var InteractionClient $interactionClient */
    private $interactionClient;


    public function __construct(InteractionClient $interactionClient)
    {
        $this->interactionClient = $interactionClient;
    }

    public function removeReadyProfiles()
    {
        $readyToDelete = $this->getReadyToRemove();
        foreach ($readyToDelete as $scheduleItem) {
            $scheduleItem->startedRemoveAt = date('Y-m-d H:i:s');
            $scheduleItem->save();
            /** @var SenderProfile $senderProfile */
            $senderProfile = $scheduleItem->senderProfile;

            $result = $this->interactionClient->removeProfile($senderProfile);
            if (!empty($result) && $result['clientStatus'] == 'removed') {
                $scheduleItem->removedAt = date('Y-m-d H:i:s');
                $scheduleItem->save();
                $senderProfile->removedAt = date('Y-m-d H:i:s');
                $senderProfile->save();

                SenderEmail::reuse($senderProfile);

                $senderPhoto = SenderPhoto::findOne($senderProfile->photo);
                $senderPhoto->isUsed = 0;
                $senderPhoto->save();
            } else {
                $scheduleItem->startedRemoveAt = '0000-00-00';
                $scheduleItem->timeToRemove = date(
                    'Y-m-d H:i:s',
                    strtotime($scheduleItem->timeToRemove) + 3600
                );
                $scheduleItem->save();
            }
        }
    }

    /**
     * @return SenderProfileRemoveSchedule[]
     */
    private function getReadyToRemove(int $limit = 50)
    {
        return SenderProfileRemoveSchedule::find()
            ->where('timeToRemove <= :now', [':now' => date('Y-m-d H:i:s')])
            ->andWhere('startedRemoveAt = "0000-00-00 00:00:00"')
            ->limit($limit)
            ->all();
    }
}
