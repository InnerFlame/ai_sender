<?php

namespace sender\interaction\components;

use app\components\common\YamlComponent;
use app\components\proxy\ProxyManagerComponent;
use app\config\Constant;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\SenderProfile;
use linslin\yii2\curl\Curl;
use yii\helpers\ArrayHelper;

class InteractionClient
{
    /** @var string $baseUrl */
    protected $baseUrl;
    /** @var string $userPwd */
    protected $userPwd;
    /** @var ProxyManagerComponent $proxyManager */
    protected $proxyManager;
    /** @var YamlComponent $yaml */
    protected $yaml;

    public function __construct(
        string $baseUrl,
        string $userPwd,
        ProxyManagerComponent $proxyManager,
        YamlComponent $yaml
    ) {
        $this->baseUrl = $baseUrl;
        $this->userPwd = $userPwd;
        $this->proxyManager = $proxyManager;
        $this->yaml = $yaml;
    }

    public function startProfile(SenderProfile $sender)
    {
        $userAgent = $this->yaml->parseByKey('userAgentMob', $sender->userAgent);

        $data = [
            'site' => $this->yaml->parseByKey('site', $sender->site)['name'],
            'email' => $sender->email,
            'password' => $sender->password,
            'birthDate' => $sender->birthday,
            'deviceIdHex' => $sender->deviceIdHex,
            'internalDeviceIdentifier' => $sender->internalDeviceIdentifier,
            'gtmClientId' => $sender->gtmClientId,
            'location' => $sender->location,
            'userAgent' => $userAgent,
            'language' => $sender->language,
            'country' => $sender->country,
            'bundle' => $this->yaml->parseByKey('bundles', $sender->site),
            'version' => $this->yaml->parseByKey('versions', $sender->version),
            'marker' => $sender->marker,
            'proxy' => $this->proxyManager->getProxy($sender, Constant::TARGET_PHOENIX, null, false),
            'ip' => $sender->ip,
        ];

        return $this->callRemoteMethod('post', "/api/client/$sender->id/start", $data);
    }

    public function stopProfile(SenderProfile $sender)
    {
        return $this->callRemoteMethod('get', "/api/client/$sender->id/stop");
    }

    public function getStatus()
    {
        return $this->callRemoteMethod('get', "/api/status");
    }

    public function getClientStatus(SenderProfile $sender): array
    {
        $clients = $this->getStatus();
        $clients = ArrayHelper::index($clients, 'id');
        if (!empty($clients[$sender->id])) {
            return $clients[$sender->id];
        }

        return [];
    }

    public function sendMessage(SenderProfile $sender, RecipientProfile $recipient, string $message)
    {
        $data = [
            'toId' => $recipient->remoteId,
            'message' => $message,
        ];

        return $this->callRemoteMethod('post', "/api/client/$sender->id/message", $data);
    }

    public function register(SenderProfile $sender, array $data)
    {
        return $this->callRemoteMethod('post', "/api/client/$sender->id/register", $data);
    }

    public function uploadPhoto(SenderProfile $sender, array $data)
    {
        return $this->callRemoteMethod('post', "/api/client/$sender->id/uploadPhoto", $data);
    }

    public function removeProfile(SenderProfile $sender)
    {
        return $this->callRemoteMethod('post', "/api/client/$sender->id/remove");
    }

    public function searchNewsFeed(SenderProfile $controlSender, string $country, array $originIds = []): array
    {
        $data = [
            'country' => $country,
            'userId' => $originIds,
        ];

        $response = $this->callRemoteMethod('post', "/api/client/$controlSender->id/searchNewsFeed", $data);

        $usersData = [];
        if (!empty($response['data']['users'])) {
            foreach ($response['data']['users'] as $user) {
                $usersData[$user['id']] = [
                    'id' => $user['id'],
                    'isScammer' => $user['isScammer'],
                    'photo_count' => $user['photo_count'],
                ];
            }
        }

        return $usersData;
    }

    protected function callRemoteMethod(string $method, string $path, array $data = []): array
    {
        try {
            $curl = new Curl();
            $curl->setOption(CURLOPT_HEADER, 1)
                ->setOption(CURLOPT_CONNECTTIMEOUT, 10)
                ->setOption(CURLOPT_TIMEOUT, 10)
                ->setOption(CURLOPT_SSL_VERIFYHOST, 0)
                ->setOption(CURLOPT_SSL_VERIFYPEER, 0);

            $headers = [
                'Content-Type: application/json',
            ];
            $curl->setOption(CURLOPT_HTTPHEADER, $headers);

            $url = $this->baseUrl . $path;

            switch ($method) {
                case 'get':
                    $url .= '?' . http_build_query($data);
                    $dataJson = $curl->get($url);
                    break;
                case 'post':
                    $curl->setOption(CURLOPT_POSTFIELDS, json_encode($data));
                    $dataJson = $curl->post($url);
                    break;
                default:
                    throw new \Exception("Unknown method: $method, path: $path");
            }

            $responseCode = $curl->getInfo(CURLINFO_HTTP_CODE);
            if ($responseCode != 200) {
                throw new \Exception("Client response code: $responseCode on request $url");
            }

            if (empty($dataJson)) {
                throw new \Exception("Empty response data on request $url");
            }
            $responseData = json_decode($dataJson, true);
        } catch (\Throwable $e) {
            \Yii::warning($e->getMessage());
            return [];
        }

        return $responseData;
    }
}
