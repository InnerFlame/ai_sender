<?php

namespace sender\interaction\components;

use app\components\amqp\InternalAmqpComponent;
use app\models\phoenix\SenderProfile;
use sender\communication\components\containerManager\ContainerManager;

class InteractionScamCheck
{
    const QUEUE = 'interactionScamCheck';
    /** @var InternalAmqpComponent */
    private $internalAmqp;
    /** @var ContainerManager */
    private $containerManager;

    public function __construct(
        InternalAmqpComponent $internalAmqp,
        ContainerManager $containerManager
    ) {
        $this->internalAmqp = $internalAmqp;
        $this->containerManager = $containerManager;
    }

    public function processScamEvents()
    {
        $this->internalAmqp->consumeEvents(self::QUEUE, [$this, 'scamEventsHandler']);
    }

    public function scamEventsHandler(array $message)
    {
        $senderId = $message['profileId'];

        $senderProfile = SenderProfile::findOne($senderId);
        if (empty($senderProfile)) {
            return;
        }
        if (strtotime($senderProfile->bannedAt) > 0) {
            return;
        }
        $senderProfile->bannedAt = date('Y-m-d H:i:s');
        $senderProfile->save();

        $this->containerManager->deleteContainer($senderProfile);
    }
}
