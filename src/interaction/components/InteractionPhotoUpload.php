<?php

namespace sender\interaction\components;

use app\components\amqp\InternalAmqpComponent;
use app\components\phoenix\reUpload\PhotoReUploadScheduler;
use app\models\phoenix\ReUploadPhotoScheduler;
use app\models\phoenix\SenderProfile;
use yii\db\Query;

class InteractionPhotoUpload
{
    const QUEUE_UPLOADED = 'interactionPhotoUploaded';
    const QUEUE_ERROR = 'interactionPhotoError';
    /** @var InternalAmqpComponent */
    private $internalAmqp;
    /** @var PhotoReUploadScheduler $senderPhoto */
    private $reuploadScheduler;


    public function __construct(
        InternalAmqpComponent $internalAmqp,
        PhotoReUploadScheduler $reuploadScheduler
    ) {
        $this->internalAmqp = $internalAmqp;
        $this->reuploadScheduler = $reuploadScheduler;
    }

    public function processPhotoUploadEvents()
    {
        $this->internalAmqp->consumeEvents(self::QUEUE_UPLOADED, [$this, 'photoUploadEventHandler']);
    }

    public function photoUploadEventHandler(array $message)
    {
        $senderId = $message['profileId'];

        $senderProfile = SenderProfile::findOne($senderId);
        if (empty($senderProfile)) {
            return;
        }

        $senderProfile->changePhotoStatus(SenderProfile::STATUS_UPLOADED);
    }

    public function processPhotoUploadErrorEvents()
    {
        $this->internalAmqp->consumeEvents(self::QUEUE_UPLOADED, [$this, 'photoUploadErrorEventHandler']);
    }

    public function photoUploadErrorEventHandler(array $message)
    {
        $senderId = $message['profileId'];

        $senderProfile = SenderProfile::findOne($senderId);
        if (empty($senderProfile)) {
            return;
        }

        switch ($message['error']) {
            case 'connectionRefused':
                if ($this->isSenderWaitingForUpload($senderProfile) == false) {
                    $senderProfile->changePhotoStatus(SenderProfile::STATUS_TO_REUPLOAD_CONNECTION_REFUSED);

                    $reUpload = new ReUploadPhotoScheduler();
                    $reUpload->senderId = $senderProfile->id;
                    $reUpload->timeToReUpload = date('Y-m-d H:i:s', time() + 900);
                    $reUpload->save();
                }
                break;
            case 'alreadyUploaded':
                $this->reuploadScheduler->addToSchedule($senderProfile, 1800);
                break;
        }
    }

    private function isSenderWaitingForUpload(SenderProfile $senderProfile)
    {
        $query = new Query();
        return $query->select('id')
            ->from(ReUploadPhotoScheduler::tableName())
            ->where([
                'senderId'      => $senderProfile->id,
                'startReUpload' => '0000-00-00 00:00:00'
            ])
            ->one();
    }
}
