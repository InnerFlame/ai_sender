<?php

namespace sender\interaction\components;

use app\models\phoenix\SenderProfile;

class InteractionRouter
{
    /** @var array $settings */
    private $settings;

    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    public function getInteractionType(string $country): string
    {
        if (!empty($this->settings[$country])) {
            return $this->settings[$country];
        }

        return $this->settings['default'];
    }

    public function isSeparatedInteraction(SenderProfile $sender): bool
    {
        if ($this->getInteractionType($sender->country) == 'separated') {
            return true;
        }

        return false;
    }

    public function isSeparatedInteractionByCountry(string $country): bool
    {
        if ($this->getInteractionType($country) == 'separated') {
            return true;
        }

        return false;
    }
}
