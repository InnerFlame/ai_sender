<?php

namespace sender\interaction\components;

use app\components\amqp\InternalAmqpComponent;
use app\models\phoenix\SenderProfile;
use sender\communication\components\containerManager\ContainerManager;

class InteractionPhotoApprove
{
    const QUEUE = 'interactionNoPhoto';

    /** @var InternalAmqpComponent */
    private $internalAmqp;
    /** @var ContainerManager */
    private $containerManager;
    /** @var InteractionClient $interactionClient */
    private $interactionClient;

    public function __construct(
        InternalAmqpComponent $internalAmqp,
        ContainerManager $containerManager,
        InteractionClient $interactionClient
    ) {
        $this->internalAmqp = $internalAmqp;
        $this->containerManager = $containerManager;
        $this->interactionClient = $interactionClient;
    }

    public function processNoPhotoEvents()
    {
        $this->internalAmqp->consumeEvents(self::QUEUE, [$this, 'noPhotoEventHandler']);
    }

    public function noPhotoEventHandler(array $message)
    {
        $senderId = $message['profileId'];

        $senderProfile = SenderProfile::findOne($senderId);
        if (empty($senderProfile)) {
            return;
        }

        $senderProfile->changePhotoStatus(SenderProfile::STATUS_NO_PHOTO);

        $this->containerManager->deleteContainer($senderProfile);
    }

    public function checkPhotoStatuses()
    {
        $sendersByCountry = $this->getSendersToCheck();

        /**
         * @var string $country
         * @var SenderProfile[] $senders
         */
        foreach ($sendersByCountry as $country => $senders) {
            $statuses = $this->getPhotoStatuses($country, array_keys($senders));
            foreach ($senders as $sender) {
                if (!empty($statuses[$sender->originId])) {
                    if (!empty($statuses[$sender->originId]['photo_count'])) {
                        $sender->changePhotoStatus(SenderProfile::STATUS_APPROVED);
                    } else {
                        $sender->changePhotoStatus(SenderProfile::STATUS_NO_PHOTO);
                    }
                }
            }
        }
    }

    private function getSendersToCheck(): array
    {
        /** @var SenderProfile[] $sendersToCheck */
        $sendersToCheck = SenderProfile::find()
            ->where([
                'photoStatus' => SenderProfile::STATUS_UPLOADED,
                'bannedAt'      => '0000-00-00 00:00:00',
                'deactivatedAt' => '0000-00-00 00:00:00',
                'pausedAt'      => '0000-00-00 00:00:00',
                'removedAt'     => '0000-00-00 00:00:00',
            ])
            ->limit(1000)
            ->all();

        $sendersByCountry = [];
        foreach ($sendersToCheck as $sender) {
            $sendersByCountry[$sender->country][$sender->originId] = $sender;
        }

        return $sendersByCountry;
    }

    /**
     * @param SenderProfile[] $senders
     */
    private function getPhotoStatuses(string $country, array $senders): array
    {
        $clients = $this->interactionClient->getStatus();
        if (!empty($clients)) {
            foreach ($clients as $client) {
                if ($client['clientStatus'] == 'connected') {
                    $controlSender = SenderProfile::findOne($client['id']);
                    break;
                }
            }
        }

        if (empty($controlSender)) {
            return [];
        }

        return $this->interactionClient->searchNewsFeed($controlSender, $country, $senders);
    }
}
