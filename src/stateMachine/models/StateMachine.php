<?php

namespace sender\stateMachine\models;

use yii\db\ActiveRecord;

/**
 * Class StateMachine
 * @package sender\stateMachine\models
 * @property integer $id
 * @property string $model
 * @property integer $modelPk
 * @property string $smName
 * @property integer $fromState
 * @property string $toState
 * @property string $status
 * @property integer $createdAt
 */
class StateMachine extends ActiveRecord
{
    public static function tableName()
    {
        return 'stateMachine';
    }

    public function getStateMachineTransitions()
    {
        return $this->hasMany(StateMachineTransition::className(), ['stateMachineId' => 'id']);
    }
}