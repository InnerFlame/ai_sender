<?php

namespace sender\stateMachine\models;

use yii\db\ActiveRecord;

/**
 * Class StateMachineTransition
 * @package sender\stateMachine\models
 * @property integer $id
 * @property integer $stateMachineId
 * @property string $transition
 * @property string $status
 * @property integer $countRemainingAttempts
 * @property integer $timeStart
 * @property integer $timeRepeat
 * @property integer $createdAt
 */
class StateMachineTransition extends ActiveRecord
{
    public static function tableName()
    {
        return 'stateMachineTransition';
    }

    public function getStateMachine()
    {
        return $this->hasOne(StateMachine::className(), ['id' => 'stateMachineId']);
    }


}