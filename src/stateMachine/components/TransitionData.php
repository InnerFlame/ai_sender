<?php

namespace sender\stateMachine\components;

class TransitionData
{
    private $model;

    private $modelPk;

    private $fromState;

    private $toState;

    private $smName;


    private $transition;

    public function __construct($model, $modelPk, $smName, $fromState, $toState, $transition)
    {
        $this->model = $model;
        $this->modelPk = $modelPk;
        $this->fromState = $fromState;
        $this->toState = $toState;
        $this->smName = $smName;
        $this->transition = $transition;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return mixed
     */
    public function getModelPk()
    {
        return $this->modelPk;
    }

    /**
     * @return mixed
     */
    public function getFromState()
    {
        return $this->fromState;
    }

    /**
     * @return mixed
     */
    public function getToState()
    {
        return $this->toState;
    }

    /**
     * @return mixed
     */
    public function getSmName()
    {
        return $this->smName;
    }

    public function getTransition()
    {
        return $this->transition;
    }

}