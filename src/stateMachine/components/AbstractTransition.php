<?php

namespace sender\stateMachine\components;

use sender\stateMachine\models\StateMachine;
use sender\stateMachine\models\StateMachineTransition;

abstract class AbstractTransition
{
    const STATUS_PROCESSING = 'processing';

    abstract public function transition(StateMachine $stateMachine);

    abstract public function repeatTransition(StateMachineTransition $stateMachineTransition);

    abstract public function getName(): string;

    abstract public function getCountRemainingAttempts(): int;

    abstract public function getRestartTime(): int;

    protected function createStateMachineTransition(StateMachine $stateMachine)
    {
        $stateMachineTransition = new StateMachineTransition();
        $stateMachineTransition->stateMachineId = $stateMachine->id;
        $stateMachineTransition->transition = static::getName();
        $stateMachineTransition->status = self::STATUS_PROCESSING;
        $stateMachineTransition->countRemainingAttempts = static::getCountRemainingAttempts();
        $stateMachineTransition->timeStart = date('Y-m-d H:i:s');
        $stateMachineTransition->timeRepeat = date('Y-m-d H:i:s', time() + static::getRestartTime());
        $stateMachineTransition->save();
    }

    protected function updateRepeatedTransition(StateMachineTransition $stateMachineTransition)
    {
        $stateMachineTransition->countRemainingAttempts--;
        $stateMachineTransition->timeStart = date('Y-m-d H:i:s');
        $stateMachineTransition->timeRepeat = date('Y-m-d H:i:s', time() + static::getRestartTime());
        $stateMachineTransition->save();
    }

}