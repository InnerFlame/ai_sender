<?php

namespace sender\stateMachine\components;

use sender\stateMachine\models\StateMachine;
use sender\stateMachine\models\StateMachineTransition;

abstract class AbstractTransitionManager
{
    /**
     * @var array $config
     */
    private $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public abstract function getStateMachineName(): string;

    public abstract function getModelName(): string;

    public function transition(TransitionData $transitionData)
    {
        $stateMachine = $this->getStateMachine($transitionData);
        if (empty($stateMachine)) {
            $stateMachine = $this->createStateMachineRecord($transitionData);
        }

        $this->getTransitionProcessor($transitionData->getToState(), $transitionData->getTransition())->transition($stateMachine);
    }

    public function toNextTransition(StateMachineTransition $stateMachineTransition)
    {
        /** @var StateMachine $stateMachine */
        $stateMachine = $stateMachineTransition->stateMachine;
        $toState = $stateMachine->toState;
        $fromState = $stateMachine->fromState;
        $nextTransition = $this->getNextTransition($stateMachineTransition);
        if (empty($nextTransition)) {
            $this->setStateMachineFinishedStatus($stateMachine);
            $fromState = $toState;
            $toState = $this->getNextState($stateMachine);
            if (empty($toState)) {
                return;
            }
            $nextTransition = $this->getFirstTransitionForState($toState);
        }

        $transitionData = new TransitionData(
            $stateMachine->model,
            $stateMachine->modelPk,
            $stateMachine->smName,
            $fromState,
            $toState,
            $nextTransition
        );

        $this->transition($transitionData);
    }

    public function repeatTransitions()
    {
        $stateMachineTransitions = $this->getStateMachineTransitionsToRestart();
        foreach ($stateMachineTransitions as $stateMachineTransition) {
            $this->getTransitionProcessor(
                $stateMachineTransition->stateMachine->toState,
                $stateMachineTransition->transition
            )->repeatTransition($stateMachineTransition);
        }
    }

    private function getStateMachineTransitionsToRestart()
    {
        return StateMachineTransition::find()
            ->from(StateMachineTransition::tableName() . ' as smt')
            ->innerJoin(StateMachine::tableName() . ' as sm', 'sm.id=smt.stateMachineId')
            ->where([
                'sm.model' => static::getModelName(),
                'sm.smName' => static::getStateMachineName(),
            ])
            ->andWhere('smt.status<>"finished"')
            ->andWhere('smt.countRemainingAttempts > 0')
            ->andWhere('smt.timeRepeat <= :now', [':now' => date('Y-m-d H:i:s')])
            ->all();
    }

    private function getFirstTransitionForState(string $state)
    {
        $transitionsListKeys = array_keys($this->config[$state]['transitionsList']);
        return $transitionsListKeys[0];
    }

    private function getNextState(StateMachine $stateMachine)
    {
        $stateKeys = array_keys($this->config);
        if (empty($stateKeys[array_search($stateMachine->toState, $stateKeys)+1])) {
            return null;
        }

        return $stateKeys[array_search($stateMachine->toState, $stateKeys)+1];
    }

    private function getNextTransition(StateMachineTransition $stateMachineTransition)
    {
        $state = $stateMachineTransition->stateMachine->toState;
        $transitionList = $this->config[$state]['transitionsList'];
        $transitionListKeys = array_keys($transitionList);
        if (empty($transitionListKeys[array_search($stateMachineTransition->transition, $transitionListKeys)+1])) {
            return null;
        }

        return $transitionListKeys[array_search($stateMachineTransition->transition, $transitionListKeys)+1];
    }

    private function getTransitionProcessor(string $state, string $transition): AbstractTransition
    {
        return $this->config[$state]['transitionsList'][$transition];
    }

    private function createStateMachineRecord(TransitionData $transitionData)
    {
        $stateMachine = new StateMachine();
        $stateMachine->model = $transitionData->getModel();
        $stateMachine->modelPk = $transitionData->getModelPk();
        $stateMachine->smName = $transitionData->getSmName();
        $stateMachine->fromState = $transitionData->getFromState();
        $stateMachine->toState = $transitionData->getToState();
        $stateMachine->status = 'processing';
        $stateMachine->save();
        $stateMachine->refresh();

        return $stateMachine;
    }

    private function setStateMachineFinishedStatus(StateMachine $stateMachine)
    {
        $stateMachine->status = 'finished';
        $stateMachine->save();
    }

    private function getStateMachine(TransitionData $transitionData)
    {
        return StateMachine::findOne([
            'model' => $transitionData->getModel(),
            'modelPk' => $transitionData->getModelPk(),
            'smName' => $transitionData->getSmName(),
            'fromState' => $transitionData->getFromState(),
            'toState' => $transitionData->getToState(),
        ]);
    }


}