<?php

namespace tests\codeception\unit\models;

use app\models\phoenix\StepMessage;
use Yii;
use yii\codeception\TestCase;
use Codeception\Specify;

class StepMessageTest extends TestCase
{
    use Specify;

    public function testSaveStepMessage()
    {
        $stepMessage                 = new StepMessage();
        $stepMessage->body           = 'some text';
        $stepMessage->hash           = md5('some text');
        $stepMessage->hasPlaceholder = rand(0, 1);

        $this->assertTrue($stepMessage->save(), 'Step Message Saved');

        $stepMessage->delete();
    }
}