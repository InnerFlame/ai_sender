<?php

namespace tests\codeception\unit\models;

use app\models\phoenix\SenderProfile;
use Yii;
use yii\codeception\TestCase;
use Codeception\Specify;

class SenderProfileTest extends TestCase
{
    use Specify;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        \Yii::$app->db->createCommand()->truncateTable(SenderProfile::tableName())->execute();
    }

    public function testSaveDataBeforeRegister()
    {

        $senderProfile = new SenderProfile();
        $senderProfile->internalDeviceIdentifier = 'internalDeviceIdentifier';
        $senderProfile->marker                   = 'marker';
        $senderProfile->gtmClientId              = 'deviceIdHex';
        $senderProfile->deviceIdHex              = 'deviceIdHex';
        $senderProfile->screenName               = 'screenName';
        $senderProfile->userAgent                = 1;
        $senderProfile->firstName                = 'firstName';
        $senderProfile->lastName                 = 'lastName';
        $senderProfile->password                 = 'password';
        $senderProfile->birthday                 = '1992-01-01';
        $senderProfile->version                  = 1;
        $senderProfile->bundle                   = 1;
        $senderProfile->photo                    = 1;
        $senderProfile->site                     = 1;
        $senderProfile->country                  = 'USA';
        $senderProfile->language                 = 'en';
        $senderProfile->sexuality                = 1;
        $senderProfile->gender                   = 1;
        $senderProfile->state                    = 'CA';
        $senderProfile->city                     = 'city';
        $senderProfile->userType                 = 'test';

        $this->assertTrue($senderProfile->save(), 'Model Save before register');
    }

    public function testSaveDataAfterRegister()
    {
        $senderProfile = SenderProfile::find()->one();
        $senderProfile->originId     = 'originId';
        $senderProfile->accessToken  = 'accessToken';
        $senderProfile->refreshToken = 'refreshToken';
        $senderProfile->location     = 'location';
        $senderProfile->pausedAt     = date('Y-m-d H:i:s');

        $this->assertTrue($senderProfile->save(), 'Model Save after register');
    }

    public function testUploadedPhoto()
    {
        $senderProfile                  = SenderProfile::find()->one();
        $senderProfile->photoStatus     = 'status';
        $senderProfile->photoUploadedAt = date('Y-m-d H:i:s');

        $this->assertTrue($senderProfile->save(), 'Model Save for uploaded photo');
    }

    public function testUploadNewPhoto()
    {
        $senderProfile        = SenderProfile::find()->one();
        $senderProfile->photo = 2;

        $this->assertTrue($senderProfile->save(), 'Model Save for upload new photo');
    }

    public function testSaveIp()
    {
        $senderProfile     = SenderProfile::find()->one();
        $senderProfile->ip = 'some ip';

        $this->assertTrue($senderProfile->save(), 'Model Save for add ip');
    }
}