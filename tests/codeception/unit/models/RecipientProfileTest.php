<?php

namespace tests\codeception\unit\models;

use app\models\phoenix\RecipientProfile;
use yii\codeception\TestCase;
use Codeception\Specify;

class RecipientProfileTest extends TestCase
{
    use Specify;

    const DEFAULT_LANG = 'en';

    public function testGetLangFromLocale_fromNullLocale_returnsDefaultLang()
    {
        $recipientProfile = new RecipientProfile();
        $recipientProfile->locale = null;

        $this->assertEquals(
            self::DEFAULT_LANG,
            $recipientProfile->getLangFromLocale(),
            'Null recipient locale must return default language'
        );
    }

    public function testGetLangFromLocale_fromLocaleWithoutCountry_returnsLang()
    {
        $expected = 'ab';
        $recipientProfile = new RecipientProfile();
        $recipientProfile->locale = 'ab';

        $this->assertEquals($expected, $recipientProfile->getLangFromLocale());
    }

    public function testGetLangFromLocale_fromLocaleWithCountry_returnsLang()
    {
        $expected = 'ab';
        $recipientProfile = new RecipientProfile();
        $recipientProfile->locale = 'ab_qw';

        $this->assertEquals($expected, $recipientProfile->getLangFromLocale());
    }

    /**
     * @dataProvider wrongLocaleProvider
     * @param string $wrongLocale
     */
    public function testGetLangFromLocale_fromWrongLocale_returnsDefaultLang(string $wrongLocale)
    {
        $recipientProfile = new RecipientProfile();
        $recipientProfile->locale = $wrongLocale;

        $this->assertEquals(self::DEFAULT_LANG, $recipientProfile->getLangFromLocale());
    }

    public function wrongLocaleProvider() {
        return [
            ['a'],
            ['abc'],
            ['a_asd'],
            ['abc_asd'],
            ['ab-aa'],
        ];
    }
}
