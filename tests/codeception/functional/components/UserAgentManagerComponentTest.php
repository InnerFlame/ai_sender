<?php

namespace tests\codeception\functional\components;

use app\components\common\YamlComponent;
use app\components\userAgent\UserAgentManagerComponent;
use app\exception\NoUserAgentsException;
use yii\caching\MemCache;
use yii\codeception\TestCase;

class UserAgentManagerComponentTest extends TestCase
{
    /**
     * @expectedException \app\exception\NoUserAgentsException
     */
    public function testGetId_whenEmptyPool_throwsException()
    {
        $cache = $this->getMockBuilder(MemCache::class)
            ->disableOriginalConstructor()
            ->getMock();
        $cache->method('exists')->willReturn(true);
        $cache->method('get')->willReturn([]);
        \Yii::$app->set('cache', $cache);

        \Yii::$app->userAgentManager->getUserAgentId();
    }

    public function testGetId_withThreeAgentsInConfig_willMakeSixItemsPool()
    {
        \Yii::$app->cache->delete(UserAgentManagerComponent::CACHE_PREFIX . date('ymd'));
        $yaml = $this->getMockBuilder(YamlComponent::class)
            ->disableOriginalConstructor()
            ->getMock();
        $yaml->method('parse')->willReturn(['a', 'b', 'c']);
        \Yii::$app->set('yaml', $yaml);

        for ($i = 0; $i < 6; $i++) {
            try {
                $id = \Yii::$app->userAgentManager->getUserAgentId();
                $this->assertInternalType('int', $id);
            } catch (NoUserAgentsException $e) {
                $this->fail('NoUserAgentsException must not be throwen');
            }
        }

        try {
            \Yii::$app->userAgentManager->getUserAgentId();
            $this->fail('NoUserAgentsException expected');
        } catch (NoUserAgentsException $e) {
        }
    }

    public function testGetUserById_withExistentId_returnsCorrectValue()
    {
        $expected = uniqid();
        $randomIndex = rand(0, PHP_INT_MAX);

        $yaml = $this->getMockBuilder(YamlComponent::class)
            ->disableOriginalConstructor()
            ->getMock();
        $yaml->method('parseByKey')->with(
            UserAgentManagerComponent::MOB_USER_AGENT_CONFIG,
            $randomIndex
        )->willReturn($expected);
        \Yii::$app->set('yaml', $yaml);

        $this->assertEquals($expected, \Yii::$app->userAgentManager->getUserAgentById($randomIndex));
    }
}
