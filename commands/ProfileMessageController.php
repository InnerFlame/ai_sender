<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\phoenix\ProfileMessage;
use app\components;

class ProfileMessageController extends Controller
{
    public function actionGenerate()
    {
        $count = 1000;
        $countries = Yii::$app->yaml->parse('country');
        $landuages = Yii::$app->yaml->parseByKey('language', 'languages');
        $gender = Yii::$app->yaml->parse('gender');
        $sexuality = Yii::$app->yaml->parse('sexuality');
        $projects = Yii::$app->yaml->parse('phoenixProject');

        for($i = 0; $i < $count; ++$i) {
            $message = new ProfileMessage();
            $message->setSubject($this->getRandomText(rand(0, 7)));
            $message->setBody($this->getRandomText(15));
            $message->setCountry(array_rand($countries));
            $message->setIsPaid(rand(0,1));
            $message->setLanguage(array_rand($landuages));
            $message->setGender(array_rand($gender));
            $message->setSexuality(array_rand($sexuality));
            $message->setSendMode(rand(0,1));
            $message->setProject(array_rand($projects));
            $message->save();
        }

    }

    protected function getRandomText($wordCount)
    {
        $arrayTextCount = substr_count($this->textLoremImpsum, ' ');
        $resultText = '';
        for($i = 0; $i < $wordCount; ++$i) {
            $resultText .=  $this->getWordByIndex(rand(0, $arrayTextCount)) . ' ';
        }
        return $resultText;
    }

    protected function getWordByIndex($index)
    {
        $res = '';
        $array = explode(' ', $this->textLoremImpsum);
        if(isset($array[$index])) {
            $res = $array[$index];
        }
        return $res;
    }

    protected $textLoremImpsum = <<<ETO
lorem ipsum dolor sit amet  consectetuer adipiscing elit aenean commodo ligula eget massa cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus donec quam felis ultricies nec pellentesque eu pretium quis sem nulla consequat enim pede justo fringilla vel aliquet vulputate arcu in rhoncus ut imperdiet a venenatis vitae nullam dictum mollis integer tincidunt cras dapibus vivamus elementum semper nisi eleifend tellus leo porttitor ac aliquam lorem ante in viverra feugiat phasellus nulla metus varius laoreet quisque rutrum etiam augue curabitur ullamcorper nam dui maecenas tempus condimentum libero neque sed nunc blandit luctus pulvinar hendrerit id odio sapien faucibus orci eros duis sed mauris nibh sodales sagittis magna bibendum velit cursus gravida mi fusce vestibulum purus scelerisque nonummy accumsan turpis primis ultrices posuere cubilia curae; lacinia tortor suscipit iaculis aliquam praesent nunc volutpat nisl vestibulum lectus risus facilisis non auctor euismod malesuada congue erat at placerat mattis pellentesque sollicitudin urna lacus suspendisse ut diam habitant morbi tristique senectus netus fames egestas tempor proin porta est hac habitasse platea dictumst ornare lobortis morbi vehicula interdum fermentum convallis molestie mauris pharetra facilisi dignissim potenti class aptent taciti sociosqu ad litora torquent per conubia nostra inceptos hymenaeos
ETO;

}