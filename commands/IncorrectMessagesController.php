<?php

namespace app\commands;

use app\models\phoenix\IncorrectMessage;
use yii\console\Controller;

class IncorrectMessagesController extends Controller
{
    public function actionVerifyAll()
    {
        $this->actionVerifyInitials();
        $this->actionVerifyPhrases();
        $this->actionVerifyTemplates();
    }

    public function actionVerifyPhrases()
    {
        IncorrectMessage::deleteAll(['type' => 'phrase']);
        $messages = \Yii::$app->checkMessage->getWrongPhrasesFromContentTool();
        foreach ($messages as $message) {
            $incorrectMessage = new IncorrectMessage();
            $incorrectMessage->type = 'phrase';
            $incorrectMessage->message = $message['text'];
            $incorrectMessage->lang = $message['lang'];

            $incorrectMessage->save();
        }
    }

    public function actionVerifyTemplates()
    {
        IncorrectMessage::deleteAll(['type' => 'template']);
        $messages = \Yii::$app->checkMessage->getWrongTemplatesFromContentTool();
        foreach ($messages as $message) {
            $incorrectMessage = new IncorrectMessage();
            $incorrectMessage->type = 'template';
            $incorrectMessage->message = $message['text'];
            $incorrectMessage->lang = $message['lang'];

            $incorrectMessage->save();
        }
    }

    public function actionVerifyInitials()
    {
        IncorrectMessage::deleteAll(['type' => 'initial']);
        $messages = \Yii::$app->checkMessage->getWrongInitialsFromDB();
        foreach ($messages as $message) {
            $incorrectMessage = new IncorrectMessage();
            $incorrectMessage->type = 'initial';
            $incorrectMessage->message = $message['text'];
            $incorrectMessage->lang = $message['lang'];

            $incorrectMessage->save();
        }
    }
}
