<?php

namespace app\commands\cron;

use yii\console\Controller;

class LifeCycleController extends Controller
{
    /**
     * php yii cron/life-cycle/check-remainder-profile
     */
    public function actionCheckRemainderProfile()
    {
        \Yii::$container->get('remainderProfileChecker')->check();
    }
}