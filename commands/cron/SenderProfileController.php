<?php

namespace app\commands\cron;

use sender\registration\components\RegistrationActualize;
use yii\console\Controller;

class SenderProfileController extends Controller
{
    /**
     * php yii cron/sender-profile/registration-actualize
     */
    public function actionRegistrationActualize()
    {
        /** @var RegistrationActualize $registrationActualize */
        $registrationActualize = \Yii::$container->get('registrationActualize');
        $registrationActualize->actualize();
    }
}
