<?php

namespace app\commands;

use sender\scamCheck\components\ScamCheck;
use yii\base\Controller;

class ScamController extends Controller
{
    public function actionCheck()
    {
        /** @var ScamCheck $scamCheck */
        $scamCheck = \Yii::$container->get('scamCheck');
        $scamCheck->runCheck();
    }
}
