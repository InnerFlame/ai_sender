<?php
namespace app\commands;

use yii\console\Controller;

class MaintenanceController extends Controller
{
    public function actionClearYamlCache()
    {
        \Yii::$app->yaml->clearCache();
    }
}
