<?php

namespace app\commands;

use app\components\common\SenderPhotoComponent;
use app\models\phoenix\SenderPhoto;
use yii\console\Controller;

class PhotoBaseController extends Controller
{
    private $data = [];

    public function actionImportPhotosFromDir(string $dir, string $niche)
    {
        /** @var SenderPhotoComponent $senderPhotoComponent */
        $senderPhotoComponent = \Yii::$app->get('senderPhoto');
        $senderPhotoComponent->importFromDir(new \SplFileInfo($dir), $niche);
    }

    public function actionSave()
    {
        $lines = file('web/files/photos_dump.txt');

        foreach ($lines as $line) {
            $array = explode('|', $line);

            $this->data[] = [trim($array[2]), trim($array[3]), trim($array[4]), trim($array[5]), trim($array[6])];

            $this->insertPartEntities();
        }

        $this->saveData();
    }

    public function actionNotFoundPhotosDelete()
    {
        $photos = SenderPhoto::find()->all();

        foreach ($photos as $photo) {
            if (!is_file(\Yii::$app->basePath . '/web' . SenderPhoto::PHOTO_BASE_PATH . $photo->name)) {
                $photo->delete();
            }
        }

        echo "Done!";
    }

    public function actionPhotosChange($startPackage = 0, $limitPackage = 0, $limit = 100)
    {
        echo "Start processing\n";
        if (!$limitPackage) {
            $photoCount   = SenderPhoto::find()->count();
            $limitPackage = $photoCount/$limit;
        }

        $offsetPackage = $limit * $startPackage;

        for ($i = $startPackage; $i < $limitPackage; $i++) {
            $time = time();
            $photos = SenderPhoto::find()
                ->limit($limit)
                ->offset($offsetPackage)
                ->all();

            echo 'Processing package - ' . $i;

            foreach ($photos as $key => $photo) {
                echo '.';
                \Yii::$app->senderPhoto->reusedPhoto($photo);
            }

            echo '  Finish processing package - ' . $i . ' ' . (time() - $time). "\n";

            $offsetPackage += $limit;
        }

        echo "Done! : "  . "\n";
    }

    /**
     * php yii photo-base/import-old-photo-to-dir
     */
    public function actionImportOldPhotoToDir()
    {
        $limit = 500;
        $offset = 0;
        $senderPhotos = $this->getSenderPhotoForImport($limit, $offset);
        while ($senderPhotos) {
            echo 'Start processing ' . $offset . '-' . $limit . PHP_EOL;
            foreach ($senderPhotos as $senderPhoto) {
                if ($senderPhoto) {
                    \Yii::$app->senderPhoto->importPhotoToDir($senderPhoto);
                }
            }
            echo 'End processing ' . $offset . '-' . $limit . PHP_EOL;
            sleep(5);
            echo 'Sleep' . PHP_EOL;

            $offset += $limit;
            $senderPhotos = $this->getSenderPhotoForImport($limit, $offset);
        }

        echo 'End script';
    }

    private function getSenderPhotoForImport($limit, $offset)
    {
        return SenderPhoto::find()
            ->limit($limit)
            ->offset($offset)
            ->all();
    }

    protected function insertPartEntities()
    {
        if (count($this->data) >= 50) {
            $this->saveData();
        }
    }

    protected function saveData()
    {
        if ($this->data) {
            \Yii::$app->db->createCommand()->batchInsert(
                SenderPhoto::tableName(),
                ['birthdayYear', 'birthdayMonth', 'birthdayDay', 'name', 'level'],
                $this->data
            )->execute();

            $this->data = [];
        }
    }
}
