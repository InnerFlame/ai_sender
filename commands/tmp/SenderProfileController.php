<?php

namespace app\commands\tmp;

use app\models\phoenix\SenderProfile;
use yii\console\Controller;

class SenderProfileController extends Controller
{
    /**
     * php yii /tmp/sender-profile/export
     */
    public function actionExport()
    {
        try {
            echo 'Export start!' . PHP_EOL;
            for ($i = 0; $i < 2; $i++) {
                $this->export($this->importIds($i), "ua-profiles-stats-sheet-{$i}.xlsx");
            }

            echo 'Export done!' . PHP_EOL;
        } catch (\Exception $e) {
            echo 'Export failed!' . PHP_EOL;
        }
    }

    /**
     * @param $sheet
     * @return array
     */
    private function importIds($sheet)
    {
        ini_set('memory_limit', '-1');

        $ids         = [];
        $filePath    = __DIR__ . '/data/ua-profiles-stats.xlsx';
        $objReader   = new \PHPExcel_Reader_Excel2007();
        $objPHPExcel = $objReader->load($filePath);
        $objSheet    = $objPHPExcel->getSheet($sheet);

        foreach ($objSheet->getRowIterator() as $row) {
            foreach ($row->getCellIterator('E','E') as $cell) {
                $ids[] = $cell->getValue();
            }
        }

        return $ids;
    }

    /**
     * @param $ids
     * @param $file
     */
    private function export($ids, $file)
    {
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getActiveSheet()
            ->setCellValue('A1', 'id')
            ->setCellValue('B1', 'id')
            ->setCellValue('C1', 'age')
            ->setCellValue('D1', 'description')
            ->setCellValue('E1', 'state')
            ->setCellValue('F1', 'city')
            ->setCellValue('G1', 'height')
            ->setCellValue('H1', 'weight')
            ->setCellValue('I1', 'build')
            ->setCellValue('J1', 'race')
            ->setCellValue('K1', 'glasses')
            ->setCellValue('L1', 'religion')
            ->setCellValue('M1', 'marital_status')
            ->setCellValue('N1', 'eye_color')
            ->setCellValue('O1', 'hair_color');

        $i = 2;
        foreach (SenderProfile::findAll(['originId' => $ids]) as $num => $senderProfile) {
            $senderProfileDetail = $senderProfile->getSenderProfileAttributesForRegistration();
echo '.';
            $objPHPExcel
                ->getActiveSheet()
                ->setCellValue('A' . $i, $senderProfile->id)
                ->setCellValue('B' . $i, $senderProfile->originId)
                ->setCellValue('C' . $i, $senderProfile->getAge())
                ->setCellValue('D' . $i, $senderProfile->description)
                ->setCellValue('E' . $i, $senderProfile->state)
                ->setCellValue('F' . $i, $senderProfile->city)
                ->setCellValue('G' . $i, $senderProfileDetail['height'])
                ->setCellValue('I' . $i, $senderProfileDetail['weight'])
                ->setCellValue('J' . $i, $senderProfileDetail['race'])
                ->setCellValue('K' . $i, $senderProfileDetail['glasses'])
                ->setCellValue('L' . $i, $senderProfileDetail['religion'])
                ->setCellValue('M' . $i, $senderProfileDetail['marital_status'])
                ->setCellValue('N' . $i, $senderProfileDetail['eye_color'])
                ->setCellValue('O' . $i, $senderProfileDetail['hair_color']);

            ++$i;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('web/' . $file);
    }
}
