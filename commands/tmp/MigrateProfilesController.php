<?php

namespace app\commands\tmp;

use app\components\generate\DataGenerateComponent;
use app\models\phoenix\SenderPhoto;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileAttributes;
use yii\console\Controller;
use yii\helpers\Console;

class MigrateProfilesController extends Controller
{
    public function actionIndex()
    {
        if (!\Yii::$app->db->createCommand("SELECT TRUE FROM INFORMATION_SCHEMA.COLUMNS
                WHERE table_name = 'martchenkoMigrateProfiles'
                AND table_schema = (SELECT DATABASE())
                AND column_name = 'senderProfileId'
            ")->queryScalar()
        ) {
            \Yii::$app->db->createCommand("ALTER TABLE `martchenkoMigrateProfiles` ADD `senderProfileId` INT AFTER id")->query();
        }

        $data = \Yii::$app->db->createCommand("SELECT * FROM martchenkoMigrateProfiles WHERE senderProfileId IS NULL")->queryAll();
        Console::startProgress(0, count($data));
        $i = 0;
        foreach ($data as $row) {
            try {
                $this->createProfile($row);
            } catch (\Exception $e) {
            }
            Console::updateProgress(++$i, count($data));
        }
        Console::endProgress();
    }

    private function createProfile($row)
    {
        /** @var DataGenerateComponent $dataGenerateComponent */
        $dataGenerateComponent = \Yii::$app->dataGenerate;
        $profile = $dataGenerateComponent->generateMinimalProfile($row['country']);
        $profile->gender = 2;
        $profile->deactivatedAt = '2000-01-01 00:00:00';
        $profile->pausedAt = '2000-01-01 00:00:00';

        $profile->site = 0;

        $profile->country = $row['country'];
        $profile->birthday = (new \DateTime())->setDate($row['birthday_year'], $row['birthday_month'],
            $row['birthday_day'])->format('Y-m-d');
        $profile->sexuality = (int)$row['sexuality_'];

        $profile->city = $row['city'];
        $profile->screenName = $row['screenname'];
        $profile->description = $row['description_'];

        if ($profile->save()) {
            \Yii::$app->db->createCommand()->update(
                'martchenkoMigrateProfiles',
                ['senderProfileId' => $profile->id],
                ['id' => $row['id']]
            )->query();

            $profileAttributes = new SenderProfileAttributes();
            $profileAttributes->id = $profile->id;
            $profileAttributes->height = $row['height_'];
            $profileAttributes->build = $row['build_'];
            $profileAttributes->eyeColor = $row['eyecol_'];
            $profileAttributes->glasses = $row['glasses_'];
            $profileAttributes->race = $row['race_'];
            $profileAttributes->hairColor = $row['haircol_'];
            $profileAttributes->weight = $row['weight_'];
            $profileAttributes->familyStatus = $row['familystatus_'];
            $profileAttributes->religion = $row['religion_'];

            $profileAttributes->save();
        }
    }

    public function actionSetPhoto()
    {
        $data = \Yii::$app->db->createCommand("
            SELECT
              pr.senderProfileId,
              pr.birthday_year,
              pr.birthday_month,
              pr.birthday_day,
              ph.photoId
            FROM martchenkoMigrateProfiles pr
              JOIN martchenkoMigratePhotos ph ON ph.profileId = pr.id
            WHERE pr.senderProfileId IS NOT NULL AND ph.photoId IS NOT NULL
        ")->queryAll();
        Console::startProgress(0, count($data));
        $i = 0;
        foreach ($data as $row) {
            try {
                $this->setPhoto($row);
            } catch (\Exception $e) {
            }
            Console::updateProgress(++$i, count($data));
        }
        Console::endProgress();
    }

    private function setPhoto($row)
    {
        $sender = SenderProfile::findOne(['id' => $row['senderProfileId']]);
        if (!is_null($sender->photo)) {
            return;
        }
        $photoName = md5(uniqid());
        $src = \Yii::$app->basePath . '/web/images/migratedPhotos/' . $row['photoId'];
        $dst = \Yii::$app->basePath . '/web/images/senderPhoto/' . $photoName;
        if (!copy($src, $dst)) {
            return;
        }

        $photo = new SenderPhoto();
        $photo->isUsed = 1;
        $photo->birthdayYear = $row['birthday_year'];
        $photo->birthdayMonth = $row['birthday_month'];
        $photo->birthdayDay = $row['birthday_day'];
        $photo->name = $photoName;
        $photo->save();
        $sender->photo = $photo->id;
        $sender->save();
    }
}
