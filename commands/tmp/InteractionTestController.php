<?php

namespace app\commands\tmp;

use app\models\phoenix\RecipientProfile;
use app\models\phoenix\SenderProfile;
use sender\interaction\components\InteractionClient;
use sender\monitoring\components\ZabbixMemcachedConnection;
use yii\console\Controller;
use yii\db\Query;

class InteractionTestController extends Controller
{
    /** @var string[] */
    private $messages = [
        'hi',
        'how R U',
        'lets chat',
        'Lets chat a little more',
        'how do you do',
        'hello',
        'Hows it going',
        'Hiya',
        'Yo',
        'Howdy',
    ];

    const INTERVAL_SECONDS = 60;

    const QUEUE_NAME = 'testInteractionMessages';

    /**
     * php yii tmp/interaction-test/prepare
     */
    public function actionPrepare()
    {
        $profiles = $this->getConnectedProfiles();
        printf("Profiles count %d\n", count($profiles));
        $delay = self::INTERVAL_SECONDS / count($profiles);
        foreach ($profiles as $profile) {
            $time_start = microtime(true);
            $data = [
                'from' => (int)$profile['id'],
                'fromOriginId' => $profile['originId'],
                'to' => $profiles[array_rand($profiles)]['originId'],
                'message' => $this->messages[array_rand($this->messages)],
            ];
            \Yii::$app->internalAmqp->publishEvent(self::QUEUE_NAME, $data);
            $time_end = microtime(true);
            $time = $time_end - $time_start;
            usleep(max(1 / 1000000, $delay - $time) * 1000000);
        }
    }

    /**
     * php yii tmp/interaction-test/stats
     */
    public function actionStats()
    {
        /** @var ZabbixMemcachedConnection $cache */
        $cache = \Yii::$container->get('zabbixMemcachedConnection');
        printf("success %d\n", $cache->get('interactionSuccess'));
        printf("error %d\n", $cache->get('interactionError'));
    }

    /**
     * php yii tmp/interaction-test/stats-reset
     */
    public function actionStatsReset()
    {
        /** @var ZabbixMemcachedConnection $cache */
        $cache = \Yii::$container->get('zabbixMemcachedConnection');
        $cache->set('interactionSuccess', 0);
        $cache->set('interactionError', 0);
    }

    /**
     * php yii tmp/interaction-test/send
     */
    public function actionSend()
    {
        /** @var InteractionClient $client */
        $client = \Yii::$container->get('interactionClient');
        /** @var ZabbixMemcachedConnection $cache */
        $cache = \Yii::$container->get('zabbixMemcachedConnection');

        \Yii::$app->internalAmqp->consumeEvents(self::QUEUE_NAME, function (array $data) use ($client, $cache) {
            $result = $client->sendMessage(
                new SenderProfile(['id' => $data['from']]),
                new RecipientProfile(['remoteId' => $data['to']]),
                $data['message']
            );
            if (!empty($result)
                && $result['clientStatus'] == 'connected'
                && !empty($result['wsResponse'])
                && empty($result['wsResponse']['error'])
            ) {
                printf("From: %s, to %s, message %s\n", $data['fromOriginId'], $data['to'], $data['message']);
                $cache->increment('interactionSuccess', 1);
            } else {
                $cache->increment('interactionError', 1);
            }
        });
    }

    private function getConnectedProfiles(): array
    {
        /** @var InteractionClient $client */
        $client = \Yii::$container->get('interactionClient');
        $connectedClient = array_filter($client->getStatus(), function ($v) {
            return $v['clientStatus'] == "connected";
        });
        $profiles = (new Query())
            ->select(['id', 'originId'])
            ->from(SenderProfile::tableName())
            ->where(['id' => array_column($connectedClient, 'id')])
            ->andWhere(['country' => 'BEL'])
            ->andWhere('originId is not null')
            ->all();
        shuffle($profiles);
        return array_values($profiles);
    }
}
