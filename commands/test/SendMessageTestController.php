<?php

namespace app\commands\test;

use app\config\Constant;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Container;
use yii\console\Controller;
use Yii;

class SendMessageTestController extends Controller
{
    private $profileId = 749;

    public function actionStart()
    {
        $profile = SenderProfile::findOne($this->profileId);

        \Yii::$app->rpcMethods->createRpcContainer($profile, Container::TYPE_COMMUNICATION, Constant::TARGET_PHOENIX);

        echo "Done" . PHP_EOL;
    }

    public function actionSend()
    {
        $rpcClient = 'phoenix.' . $this->profileId;

        $data = [
            'toId'              => '96611979a28011e6ba06441ea14ed80c',
            'messageType'       => 'chat',
            'copyPasteDetected' => false,
            'subject'           => 'hi',
            'message'           => 'hello dude',
        ];

        $additional = [
            'messageId'  => 1,
            'clientName' => $rpcClient,
        ];

        \Yii::$app->rpcClient->rpcCall('client.' . $rpcClient, 'sendMessage', [$data], $additional);

        echo "Done" . PHP_EOL;
    }
}