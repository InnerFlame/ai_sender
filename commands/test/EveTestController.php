<?php

namespace app\commands\test;

use app\models\phoenix\RecipientProfile;
use app\models\phoenix\SenderProfile;
use yii\console\Controller;
use Yii;

class EveTestController extends Controller
{
    public function actionSend()
    {
        $text = 'Hello babe!';
        $recipientId = 4803117;
        $senderId = 9845;

        $recipient  = RecipientProfile::findOne($recipientId);
        $sender = SenderProfile::findOne($senderId);

        $postData = [
            'from'       => $recipient->remoteId,
            'to'         => $sender->originId,
            'site'       => \Yii::$app->yaml->parseByKey('site', $recipient->site)['hash'],
            'country'    => $recipient->country,
            'paid'       => $recipient->isPaid,
            'locale'     => $recipient->locale,
            'traff_src_from' => $recipient->source,
            'traff_src_to'  => 'Aff Internal',
            'action_way_to' => 'external',
            'platform'   => $recipient->lastUsedPlatform,
            'text'       => $text,
        ];

        var_dump($postData);

        $evaAnswer = \Yii::$app->evaMessage->getEvaStep($recipient, $sender, $postData, $text);

        var_dump($evaAnswer);

        echo "Done" . PHP_EOL;
    }
}