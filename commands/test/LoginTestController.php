<?php

namespace app\commands\test;

use app\config\Constant;
use yii\console\Controller;
use Yii;

class LoginTestController extends Controller
{
    public function actionLogin()
    {
        $params = [
            'LoginForm' => [
                'email'    => 'coy3sue@inbox.com',
                'password' => '658627d963e1',
            ],
            'udid' => '000000002a40ce12ffffffffd64e8bfa',
        ];

        $data = $this->post('/site/login', $params);

        print_r($data);

        $access_token = $data['data']['access_token'];

        $data = $this->get('/data/fetch/args/' . json_encode(['id' => '', 'user_status' => '']), null, null, $access_token);

        print_r($data);

        echo 'Done' . PHP_EOL;
    }

    public function actionRefresh()
    {
        $data = $this->get('/site/refreshToken',[ 'key' => '9c2bd42ec1db45d31b19dbccf950d0f7']);

        print_r($data);

        $access_token = $data['data']['access_token'];

        $data = $this->get('/data/fetch/args/' . json_encode(['id' => '', 'user_status' => '']), null, null, $access_token);

        print_r($data);

        echo 'Done' . PHP_EOL;
    }

    public function actionData()
    {
        $token = 'ifidhhoaoo5m2gmuq0hc9rti73';

        $data = $this->get('/data/fetch/args/' . json_encode(['id' => '', 'user_status' => '']), null, null, $token);

        print_r($data);

        echo 'Done' . PHP_EOL;
    }

    public function actionSearch()
    {
        $token = 'jl3d4j9586vfubvpiiqhtlef10';

        $params = [
            'updatedAt_from' => time() - 60,
            'updatedAt_to'   => time(),
            'country'        => 'AUS'
        ];
        print_r($params);

        $data = $this->post('/search/newsFeed', $params, null, $token);

        print_r($data);

        echo 'Done' . PHP_EOL;
    }

    public function post($url, $params, $cookie = null, $token = null)
    {
        $headr[] = 'App-Marker: hand3856be45';
        $headr[] = 'Content-type: application/x-www-form-urlencoded';
        $headr[] = 'X-Requested-With: XMLHttpRequest';
        if($token){
            $headr[] = "Authorization: Bearer $token";
        }

        $domainUrl = 'https://www.benaughty.com';

        $url = "{$domainUrl}{$url}";
        $data = http_build_query($params);

        var_dump($url, $data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headr);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_PROXY, $this->getLuminatiProxy());

        if(!empty($cookie)){
            curl_setopt ($ch, CURLOPT_COOKIE, $cookie);
        }

        $server_output = curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        var_dump($retcode, $server_output);
        if ($retcode == 200) {
            return json_decode($server_output, true);
        }

        return false;
    }

    public function get($url, $params, $cookie = null, $token = null)
    {
        $headr = array();
        $headr[] = 'App-Marker: hand3856be45';
        $headr[] = 'Content-type: application/x-www-form-urlencoded';
        $headr[] = 'X-Requested-With: XMLHttpRequest';
        if($token){
            $headr[] = "Authorization: Bearer $token";
        }

        $get_vars = "";
        if(!empty($params)){
            $get_vars =  "?" . http_build_query($params);
        }

        $domainUrl = 'https://www.benaughty.com';

        $url  = "{$domainUrl}{$url}{$get_vars}";
        var_dump($url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headr);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_PROXY, $this->getLuminatiProxy());

        if(!empty($cookie)){
            curl_setopt ($ch, CURLOPT_COOKIE, $cookie);
        }

        $server_output = curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        var_dump($retcode, $server_output);
        curl_close($ch);

        if ($retcode == 200) {
            return json_decode($server_output, true);
        }

        return false;
    }

    public function getLuminatiProxy()
    {
        $ips = dns_get_record ('zproxy.luminati.io');

        $ip = $ips[array_rand($ips)]['ip'];

        $luminati = 'http://lum-customer-' . Constant::LUMINATI_CUSTOMER;
        $luminati .= '-zone-city';
        $luminati .= '-country-us';
        $luminati .= '-session-' . md5('12457865421');
        $luminati .= ':' . Constant::LUMINATI_PASS . '@' . $ip . ':22225';
        return $luminati;
    }
}
