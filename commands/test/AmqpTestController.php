<?php

namespace app\commands\test;

use yii\console\Controller;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


class AmqpTestController extends Controller
{
    public function actionRpcTest()
    {
        echo "Start test\n";

        $host = 'sklyar.nv.priv';
        $port = 5672;
        $user = 'test';
        $pass = 'test';
        $vhost = '/';

        $connection = new AMQPStreamConnection($host, $port, $user, $pass, $vhost);

        $exchange = 'router';
        $queueToPublish = 'client.123';

        //Declare queue to publish
        $channel = $connection->channel();
        $channel->queue_declare($queueToPublish, false, false, false, false);
        $queueToConsumeDetails = $channel->queue_declare('', false, false, true, false);
        $queueToConsume = $queueToConsumeDetails[0];
        
        $channel->exchange_declare($exchange, 'direct', false, false, false);
        $channel->queue_bind($queueToPublish, $exchange);

        //Publish
        $data = [
            'method' => 'time',
            'params' => [],
        ];
        $json = json_encode($data);
        echo $json . "\n";
        $message = new AMQPMessage(
            $json,
            [
                'content_type'      => 'text/plain',
                'delivery_mode'     => AMQPMessage::DELIVERY_MODE_PERSISTENT,
                'reply_to'          => $queueToConsume,
                'correlation_id'    => md5(microtime()),
            ]
        );
        $channel->basic_publish($message, $exchange);

        echo "published\n\n";
        ####################################
        //Consume
        echo "Start listen to $queueToConsume\n";
        $channel->queue_bind($queueToConsume, $exchange);
        $channel->basic_consume($queueToConsume, 'consumer', false, true, false, false, [$this, 'processMessage']);
        $startTime = time();
        while (count($channel->callbacks) && (time() - $startTime) < 30) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
        echo "End test\n";
    }

    public function processMessage($message)
    {
        echo "\n--------\n";
        echo $message->body;
        echo "\n--------\n";

        // Send a message with the string "quit" to cancel the consumer.
        if ($message->body === 'quit') {
            $message->delivery_info['channel']->basic_cancel($message->delivery_info['consumer_tag']);
        }
    }

    public function actionRpcPresenceEventListener()
    {
        echo "Start test\n";

        $host = 'sklyar.nv.priv';
        $port = 5672;
        $user = 'test';
        $pass = 'test';
        $vhost = '/';

        $connection = new AMQPStreamConnection($host, $port, $user, $pass, $vhost);

        $exchange = 'presence.client';
        $queueToConsume = 'presence.client';

        //Declare queue to publish
        $channel = $connection->channel();
        $channel->queue_declare($queueToConsume, false, false, true, false);
        $channel->exchange_declare($exchange, 'x-presence', false, true, false);
        $channel->queue_bind($queueToConsume, $exchange);

        echo "Start listen to $queueToConsume\n";
        $channel->basic_consume($queueToConsume, 'consumer', false, true, false, false, [$this, 'processPresenceEvent']);
        $startTime = time();
        while (count($channel->callbacks) && (time() - $startTime) < 30) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
        echo "End test\n";
    }

    public function processPresenceEvent($message)
    {
        echo "\n--------\n";
        var_dump($message->get_properties()['application_headers']->getNativeData());
        echo "\n--------\n";
    }

    public function actionRpcEventListener()
    {
        echo "Start test\n";

        $host = 'sklyar.nv.priv';
        $port = 5672;
        $user = 'test';
        $pass = 'test';
        $vhost = '/';

        $connection = new AMQPStreamConnection($host, $port, $user, $pass, $vhost);

        $queueToConsume = 'logic';
        $exchange = 'router';
        $channel = $connection->channel();
        $channel->queue_declare($queueToConsume, false, false, true, false);
        $channel->exchange_declare($exchange, 'direct', false, false, false);
        $channel->queue_bind($queueToConsume, $exchange);

        echo "Start listen to $queueToConsume\n";
        $channel->basic_consume($queueToConsume, 'consumer', false, true, false, false, [$this, 'processEvent']);
        $startTime = time();
        while (count($channel->callbacks) && (time() - $startTime) < 30) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
        echo "End test\n";
    }

    public function processEvent($message)
    {
        echo "\n--------\n";
        var_dump($message);
        echo "\n--------\n";
    }
}
