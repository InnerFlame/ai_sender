<?php

namespace app\commands\test;

use yii\console\Controller;


class AmqpExchangeTestController extends Controller
{

    public function actionPublish()
    {
        $data = [
            'name' => 'sender',
            'rnd' => mt_rand(),
        ];

        var_dump($data);

        \Yii::$app->internalAmqp->publishEvent('testQueue', $data);
    }

    public function actionConsume()
    {
        \Yii::$app->internalAmqp->consumeEvents('testQueue', [$this, 'messageHandler']);

        echo "stop consuming due to false returned" . PHP_EOL;
    }

    public function messageHandler($message)
    {
        if (empty($message)) {
            echo 'empty incoming data' . PHP_EOL;
            return false;
        }

        var_dump($message);

        return false;
    }
}