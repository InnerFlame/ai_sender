<?php

namespace app\commands\test;

use yii\console\Controller;
use Yii;

class ContainersRedisMigrationController extends Controller
{
    public function actionIndex($update = false)
    {
        echo 'Start' , PHP_EOL;

        $settings = [
            ['country' => 'AUS', 'siteId' => 1],
            ['country' => 'AUS', 'siteId' => 2],
            ['country' => 'AUS', 'siteId' => 4],
            ['country' => 'AUS', 'siteId' => 5],

            ['country' => 'GBR', 'siteId' => 1],
            ['country' => 'GBR', 'siteId' => 2],
            ['country' => 'GBR', 'siteId' => 4],
            ['country' => 'GBR', 'siteId' => 5],
        ];

        $consolidatedInfo = [];

        foreach ($settings as $item) {
            $cacheKey = 'containers_info' . $item['country'] . '_' . $item['siteId'];
            $containersInfo = \Yii::$app->cache->get($cacheKey);

            if (empty($consolidatedInfo[$item['country']])) {
                $consolidatedInfo[$item['country']]['country']          = $item['country'];
                $consolidatedInfo[$item['country']]['justStartedIds']   = array_merge($containersInfo['justStartedIds'], $containersInfo['activeIds']);
                $consolidatedInfo[$item['country']]['activeIds']        = [];
                $consolidatedInfo[$item['country']]['pausedIds']        = array_merge($containersInfo['pausedIds'], $containersInfo['readyToStopIds']);
                $consolidatedInfo[$item['country']]['readyToStopIds']   = [];
                $consolidatedInfo[$item['country']]['expirationTime']   = time() + 302400;
            } else {
                $consolidatedInfo[$item['country']]['justStartedIds']   = array_merge(
                    $consolidatedInfo[$item['country']]['justStartedIds'],
                    $containersInfo['justStartedIds'],
                    $containersInfo['activeIds']
                );

                $consolidatedInfo[$item['country']]['pausedIds']   = array_merge(
                    $consolidatedInfo[$item['country']]['pausedIds'],
                    $containersInfo['pausedIds'],
                    $containersInfo['readyToStopIds']
                );
            }
        }

        foreach ($consolidatedInfo as $itemToSave) {
            $cacheKey = 'containers_info' . $itemToSave['country'];

            var_dump($cacheKey, $itemToSave, '-----------------------');

            if ($update) {
                echo 'Update' . PHP_EOL;
                \Yii::$app->cache->set($cacheKey, $itemToSave, 320400);
            }
        }

        echo 'Done' . PHP_EOL;
    }
}
