<?php

namespace app\commands\test;

use yii\console\Controller;
use Yii;

class MobApiSearchTestController extends Controller
{
    public function actionIndex()
    {
        $stepInterval = 60;
        $syncTime = time();
        $startTime = $syncTime - 60*$stepInterval;
        $endTime = $startTime + $stepInterval;

        $country = 'CAN';
        $startedClients = \Yii::$container->get('communicationContainerManager')->getAllStartedClients();

        $client = $startedClients[array_rand($startedClients)];

        $params = [
            'updatedAt_from'    => $startTime,
            'updatedAt_to'      => $endTime,
            'country'           => $country,
        ];

        $additional = [
            'clientName' => $client->name,
        ];

        \Yii::$app->rpcClient->rpcCall('client.' . $client->name, 'searchNewsFeed', [$params], $additional);
    }
}
