<?php

namespace app\commands\test;

use yii\console\Controller;
use Yii;

class SmsTestController extends Controller
{
    public function actionSend()
    {
        $message = 'test message';
        var_dump(\Yii::$app->sms->sendMessage($message));
        echo 'Done' . PHP_EOL;
    }
}
