<?php

namespace app\commands\test;

use app\models\phoenix\RecipientProfile;
use app\models\phoenix\SenderProfile;
use yii\console\Controller;
use Yii;

class EvePlaceholderTestController extends Controller
{
    public function actionSend()
    {
        $recipientId = 4803117;
        $senderId = 9845;

        $recipient = RecipientProfile::findOne($recipientId);
        $sender    = SenderProfile::findOne($senderId);

        if (empty($recipient) || empty($sender)) {
            return false;
        }

        $curlData = [
            'to'         => $recipient->remoteId,
            'from'       => $sender->originId,
            'site'       => \Yii::$app->yaml->parseByKey('site', $recipient->site)['hash'],
            'country'    => $recipient->country,
            'paid'       => $recipient->isPaid,
            'traff_src'  => 'Aff Internal',
            'action_way' => 'external',
            'platform'   => $recipient->lastUsedPlatform,
        ];

        $evaData = \Yii::$app->evaMessage->getEvaPlaceholder($recipient, $sender, $curlData);

        var_dump($evaData);

        echo "Done" . PHP_EOL;
    }
}