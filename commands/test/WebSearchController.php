<?php

namespace app\commands\test;

use yii\console\Controller;
use Yii;
use app\config\Constant;
use app\models\rpc\Container;
use app\models\phoenix\SenderProfile;

class WebSearchController extends Controller
{
    public function actionStartContainer()
    {
        $profile = SenderProfile::findOne(206);
        \Yii::$app->rpcMethods->createRpcContainer($profile, Container::TYPE_SEARCH, 'webctrl');
    }
}