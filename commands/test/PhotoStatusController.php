<?php

namespace app\commands\test;

use yii\console\Controller;
use Yii;
use app\models\phoenix\SenderProfile;
use yii\db\Query;

class PhotoStatusController extends Controller
{
    protected $timeAfterRegister = 10800;
    protected $limitInsert       = 50;
    protected $ids               = [];
    protected $data              = [];

    public function actionIndex()
    {
        echo "Start scheduling\n";

        $this->createSchedule();

        echo "Done\n";
    }

    protected function createSchedule()
    {
        $offset = 0;
        $limit = 100;

        $profileIds = $this->getProfilesToMonitoring($offset, $limit);

        while (!empty($profileIds)) {
            echo '.';
            $observer = $this->getObserver(count($profileIds));

            $time     = time();
            $start    = 0;
            $period   = 7200;

            if ($observer) {
                foreach ($profileIds as $profileId) {
                    $this->data[] = [
                        $profileId,
                        $observer[array_rand($observer)],
                        date('Y-m-d H:i:s', $time + mt_rand($start, $start + $period)),
                    ];

                    $this->savePartEntities();

                    $start += $period;
                }

                $this->saveToSchedule();
            }

            $offset += $limit;
            $profileIds = $this->getProfilesToMonitoring($offset, $limit);
        }
    }

    protected function getProfilesToMonitoring($offset, $limit)
    {
        $query = new Query();

        return $query->select('id')
            ->from(SenderProfile::tableName())
            ->where(['photoStatus'         => [SenderProfile::STATUS_APPROVED, SenderProfile::STATUS_DECLINED]])
            ->andWhere(['bannedAt'         => '0000-00-00 00:00:00'])
            ->andWhere(['deactivatedAt'    => '0000-00-00 00:00:00'])
            ->andWhere(['pausedAt'         => '0000-00-00 00:00:00'])
            ->andWhere('originId IS NOT NULL')
            ->andWhere(['<', 'photoUploadedAt', date('Y-m-d H:i:s', time() - $this->timeAfterRegister)])
            ->offset($offset)
            ->limit($limit)
            ->column();
    }

    protected function getObserver($limit)
    {
        $query = new Query();

        return $query->select('id')
            ->from(SenderProfile::tableName())
            ->where(['userType' => SenderProfile::TYPE_OBSERVER])
            ->andWhere('originId IS NOT NULL')
            ->limit($limit)
            ->column();
    }

    protected function savePartEntities()
    {
        if (count($this->data) >= $this->limitInsert) {
            $this->saveToSchedule();
        }
    }

    protected function saveToSchedule()
    {
        if ($this->data) {
            \Yii::$app->db->createCommand()->batchInsert('photoMonitoringScheduler',
                ['senderId', 'observerId', 'timeToMonitoring'],
                $this->data
            )->execute();

            $this->data = [];
        }
    }
}