<?php

namespace app\commands\test;

use app\models\phoenix\RecipientProfile;
use yii\console\Controller;
use Yii;

class InitialSchemasTestController extends Controller
{
    public function actionIndex()
    {
        $recipientId = 4556932;
        $recipient  = RecipientProfile::findOne($recipientId);
        var_dump($recipient->attributes);

        \Yii::$app->commScheduler->addInitialRecipientSchemas($recipient);

        echo 'Done' . PHP_EOL;
    }
}
