<?php

namespace app\commands\test;

use app\models\phoenix\RecipientProfile;
use yii\console\Controller;

class IdEncoderController extends Controller
{
    private $profileId = 749;

    public function actionIndex()
    {
        $recipients = RecipientProfile::find()->limit(5000)->orderBy('id desc')->all();

        $idEncoder = \Yii::$container->get('idEncoder');

        $stats = [
            'success' => 0,
            'error' => 0,
        ];
        foreach ($recipients as $recipient) {
            echo '.';
            $hash = $idEncoder->encode($recipient);
            if (!empty($hash)) {
                $stats['success']++;
            } else {
                $stats['error']++;
            }
        }

        var_dump($stats);
        echo "Done" . PHP_EOL;
    }
}