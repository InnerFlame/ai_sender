<?php

namespace app\commands\test;

use app\config\Constant;
use yii\console\Controller;
use Yii;
use app\models\phoenix\SenderProfile;

class ProxyTestController extends Controller
{
    public function actionIndex()
    {
        echo "Start proxy test\n";

        $profile = SenderProfile::findOne(206);
        $proxy = \Yii::$app->proxyManager->getProxy($profile, 'phoenix');

        var_dump($proxy);

        echo "Done\n";
    }

    public function actionDedicatedTest()
    {
        $this->get('http://icanhazip.com');
    }

    public function get($url, $params = [])
    {
        $get_vars = "";
        if(!empty($params)){
            $get_vars =  "?" . http_build_query($params);
        }

        $url  = "{$url}{$get_vars}";
        var_dump($url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_PROXY, $this->getLuminatiProxy());


        $server_output = curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        var_dump($retcode, $server_output);

        if ($retcode == 200) {
            return json_decode($server_output, true);
        }

        return false;
    }

    public function getLuminatiProxy()
    {
        $ips = dns_get_record ('zproxy.luminati.io');

        $dnsIp = $ips[array_rand($ips)]['ip'];

        $allocatedIps = [
            '191.96.191.178',
            '191.96.191.180',
            '191.96.191.186',
            '191.96.191.195',
            '191.96.191.212',
        ];

        $proxyIp = $allocatedIps[0];

        $luminati = 'http://lum-customer-' . Constant::LUMINATI_CUSTOMER;
        $luminati .= '-zone-ded_us_2d';
        $luminati .= '-id-' . $proxyIp;
        $luminati .= ':' . Constant::LUMINATI_PASS . '@' . $dnsIp . ':22225';

        vaR_dump($luminati);

        return $luminati;
    }
}