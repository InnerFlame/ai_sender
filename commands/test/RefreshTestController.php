<?php

namespace app\commands\test;

use app\config\Constant;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use app\models\rpc\Container;
use yii\console\Controller;
use Yii;

class RefreshTestController extends Controller
{
    private $profileId = 1853;

    public function actionStart()
    {
        $profile = SenderProfile::findOne($this->profileId);

        \Yii::$app->rpcMethods->createRpcContainer($profile, Container::TYPE_COMMUNICATION, Constant::TARGET_PHOENIX);

        echo "Done" . PHP_EOL;
    }

    public function actionRefresh()
    {
        $profile = SenderProfile::findOne($this->profileId);
        \Yii::$app->rpcMethods->refreshToken($profile, Constant::TARGET_PHOENIX);
    }

    public function actionFetch()
    {
        $profile = SenderProfile::findOne($this->profileId);
        \Yii::$app->rpcMethods->fetchData($profile, Constant::TARGET_PHOENIX);
    }
}
