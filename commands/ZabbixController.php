<?php

namespace app\commands;

use app\models\phoenix\MatchingErrorLog;
use app\models\phoenix\SenderProfile;
use sender\communication\models\CommunicationSchedule;
use sender\monitoring\components\ZabbixMemcachedConnection;
use yii\base\Controller;
use yii\db\Expression;
use yii\db\Query;

class ZabbixController extends Controller
{
    const DATE_FORMAT = 'Y-m-d H:i:s';
    const ZABBIX_KEY_INITIALS_PREFIX = 'initials';
    const ZABBIX_KEY_STEPS_PREFIX = 'steps';
    const ZABBIX_KEY_SENDER_BAN = 'senders.ban';
    const ZABBIX_KEY_RESENDING = 'resending';
    const ZABBIX_KEY_COUNT_GRAB = 'grabbed';
    const ZABBIX_KEY_COUNT_NOT_SET = 'not_send';

    public function actionPing()
    {
        echo time();
    }

    public function actionGetData()
    {
        $this->printInitialStats();
        $this->printMessagesByStepsStats();
        $this->printSenderBanStats();
        $this->printSenderResendingStats();
    }

    /**
     * Get count of grab and count of unsent communications in the first half hour from the moment of grabbing
     */
    public function actionCoverageFatal()
    {
        /** @var ZabbixMemcachedConnection $zabbixMemcachedConnection */
        $zabbixMemcachedConnection = \Yii::$container->get('zabbixMemcachedConnection');

        $response = $zabbixMemcachedConnection->getDataAndDecrementCounters([ZabbixController::ZABBIX_KEY_COUNT_GRAB]);
        $response[ZabbixController::ZABBIX_KEY_COUNT_NOT_SET] = (int) $this->getIntervalCountNotSet();

        echo json_encode($response);
    }

    public function actionGetCountProxyErrors()
    {
        echo \Yii::$container->get('zabbixProxyError')->getCount();
    }

    public function actionCountStartedContainers()
    {
        echo count(\Yii::$container->get('communicationContainerManager')->getAllStartedClients());
    }

    public function actionCountPlannedContainers()
    {
        echo \Yii::$container->get('communicationContainerManager')->getCountPlannedToStartClients();
    }

    private function getIntervalCountNotSet(): int
    {
        return \Yii::$app->db->createCommand("
            SELECT
                count(rp.`id`) as `count`
            FROM `recipientProfile` rp
            LEFT JOIN `communicationSchedule` cs ON cs.`recipientId` = rp.`id`
            WHERE cs.`recipientId` IS NULL AND rp.`createdAt` >= DATE_SUB(NOW() , INTERVAL 30 MINUTE);
        ")->queryScalar();
    }

    private function printSenderResendingStats()
    {
        $lastId = (int)(new Query())
            ->select('lastTimestamp')
            ->from('zabbixTimestamp')
            ->where(['key' => self::ZABBIX_KEY_RESENDING])
            ->scalar();

        $data = (new Query())
            ->from(MatchingErrorLog::tableName())
            ->andWhere(['>', "id", $lastId])
            ->andWhere(['>', "createdAt", (new \DateTime('1 day ago'))->format(self::DATE_FORMAT)])
            ->indexBy('id')
            ->orderBy('id')
            ->all();

        foreach ($data as $row) {
            $lastId = $row['id'];
            printf(
                "- %s %d \"%s\"\n",
                self::ZABBIX_KEY_RESENDING . '.log',
                strtotime($row['createdAt']),
                addcslashes($row['entity'], '"\\')
            );
        }

        $this->updateLastZabbixTimestamp(self::ZABBIX_KEY_RESENDING, $lastId);
    }

    private function printSenderBanStats()
    {
        $startTimestamp = $this->getStartTimestamp(self::ZABBIX_KEY_SENDER_BAN);
        $endTimestamp = new \DateTime();
        $endTimestamp->setTime((int)$endTimestamp->format("H"), (int)$endTimestamp->format("i"), 0, 0);

        $data = (new Query())
            ->addSelect(new Expression('DATE_FORMAT(bannedAt, "%Y-%m-%d %H:%i:00") AS ts'))
            ->addSelect(new Expression('COUNT(*) as cnt'))
            ->from(SenderProfile::tableName())
            ->andWhere(['>', "bannedAt", $startTimestamp->format(self::DATE_FORMAT)])
            ->andWhere(['<', "bannedAt", $endTimestamp->format(self::DATE_FORMAT)])
            ->groupBy('ts')
            ->indexBy('ts')
            ->all();

        $daterange = new \DatePeriod(
            $startTimestamp,
            new \DateInterval('PT1M'),
            $endTimestamp,
            \DatePeriod::EXCLUDE_START_DATE
        );

        $maxTimestamp = $startTimestamp->getTimestamp();
        /** @var \DateTime $date */
        foreach ($daterange as $date) {
            $maxTimestamp = max($maxTimestamp, $date->getTimestamp());
            printf(
                "- %s %d %d\n",
                self::ZABBIX_KEY_SENDER_BAN,
                $date->getTimestamp(),
                (int)@$data[$date->format(self::DATE_FORMAT)]['cnt']
            );
        }

        $this->updateLastZabbixTimestamp(self::ZABBIX_KEY_SENDER_BAN, $maxTimestamp);
    }

    private function printInitialStats()
    {
        $startTimestamp = $this->getStartTimestamp(self::ZABBIX_KEY_INITIALS_PREFIX);

        $endTimestamp = new \DateTime('5 minutes ago');
        $endTimestamp->setTime((int)$endTimestamp->format("H"), (int)$endTimestamp->format("i"), 0, 0);

        $data = (new Query())
            ->addSelect(new Expression('AVG(TIMESTAMPDIFF(SECOND, timeToSend, sentAt)) as diff'))
            ->addSelect(new Expression('DATE_FORMAT(timeToSend, "%Y-%m-%d %H:%i:00") as ts'))
            ->addSelect(new Expression('COUNT(*) as total'))
            ->addSelect(new Expression('SUM(IF(TIMESTAMPDIFF(SECOND, timeToSend, sentAt)>=30,1,0)) as `delayed`'))
            ->from(CommunicationSchedule::tableName())
            ->andWhere(['step' => 0])
            ->andWhere(['>', "timeToSend", $startTimestamp->format(self::DATE_FORMAT)])
            ->andWhere(['<', "timeToSend", $endTimestamp->format(self::DATE_FORMAT)])
            ->groupBy('ts')
            ->indexBy('ts')
            ->all();

        $daterange = new \DatePeriod(
            $startTimestamp,
            new \DateInterval('PT1M'),
            $endTimestamp,
            \DatePeriod::EXCLUDE_START_DATE
        );

        $maxTimestamp = $startTimestamp->getTimestamp();
        /** @var \DateTime $date */
        foreach ($daterange as $date) {
            $maxTimestamp = max($maxTimestamp, $date->getTimestamp());
            printf(
                "- %s %d %f\n",
                self::ZABBIX_KEY_INITIALS_PREFIX . '.delay.time',
                $date->getTimestamp(),
                (float)@$data[$date->format(self::DATE_FORMAT)]['diff']
            );
            printf(
                "- %s %d %d\n",
                self::ZABBIX_KEY_INITIALS_PREFIX . '.delay.count',
                $date->getTimestamp(),
                (int)@$data[$date->format(self::DATE_FORMAT)]['delayed']
            );
            printf(
                "- %s %d %d\n",
                self::ZABBIX_KEY_INITIALS_PREFIX . '.total.count',
                $date->getTimestamp(),
                (int)@$data[$date->format(self::DATE_FORMAT)]['total']
            );
        }
        $this->updateLastZabbixTimestamp(self::ZABBIX_KEY_INITIALS_PREFIX, $maxTimestamp);
    }

    private function printMessagesByStepsStats()
    {
        $startTimestamp = $this->getStartTimestamp(self::ZABBIX_KEY_STEPS_PREFIX);

        $endTimestamp = new \DateTime('5 minutes ago');
        $endTimestamp->setTime((int)$endTimestamp->format("H"), (int)$endTimestamp->format("i"), 0, 0);

        $data = (new Query())
            ->addSelect(new Expression('COUNT(*) as total'))
            ->addSelect(new Expression('UNIX_TIMESTAMP(DATE_FORMAT(timeToSend, "%Y-%m-%d %H:%i:00")) as ts'))
            ->addSelect(new Expression('step'))
            ->from(CommunicationSchedule::tableName())
            ->andWhere(['>', "timeToSend", $startTimestamp->format(self::DATE_FORMAT)])
            ->andWhere(['<', "timeToSend", $endTimestamp->format(self::DATE_FORMAT)])
            ->andWhere(['!=', "sentAt", '0000-00-00 00:00:00'])
            ->groupBy('ts, step')
            ->all();

        $groupedData = [];
        foreach ($data as $item) {
            $groupedData[$item['ts']][$item['step']] = $item['total'];
        }
        $filledData = [];
        foreach ($groupedData as $ts => $stepsData) {
            $filledData[$ts] = $stepsData;
            $missedSteps = array_diff(range(0, 15), array_keys($stepsData));
            foreach ($missedSteps as $step) {
                $filledData[$ts][$step] = 0;
            }
        }

        $maxTimestamp = $startTimestamp->getTimestamp();
        foreach ($filledData as $ts => $stepsData) {
            foreach ($stepsData as $step => $tolal) {
                $maxTimestamp = max($maxTimestamp, $ts);
                printf(
                    "- %s %d %f\n",
                    self::ZABBIX_KEY_STEPS_PREFIX . '.' . $step . '.count',
                    $ts,
                    $tolal
                );
            }
        }

        $this->updateLastZabbixTimestamp(self::ZABBIX_KEY_STEPS_PREFIX, $maxTimestamp);
    }

    private function getStartTimestamp(string $key): \DateTime
    {
        $timestampFromDBObject = new \DateTime();
        $timestampFromDBObject->setTimestamp(
            (int)(new Query())
                ->select('lastTimestamp')
                ->from('zabbixTimestamp')
                ->where(['key' => $key])
                ->scalar()
        );
        return max(new \DateTime('1 day ago 00:00:00'), $timestampFromDBObject);
    }

    private function updateLastZabbixTimestamp(string $key, int $maxTimestamp)
    {
        \Yii::$app->db->createCommand(
            "INSERT INTO zabbixTimestamp (`key`,lastTimestamp)
              VALUES (:key,:timestamp)
              ON DUPLICATE KEY UPDATE lastTimestamp=:timestamp",
            ['key' => $key, 'timestamp' => $maxTimestamp]
        )
            ->execute();
    }
}
