<?php

namespace app\commands;

use yii\console\Controller;

class CheckMessageController extends Controller
{
    /**
     * php yii check-message/check-initial
     */
    public function actionCheckInitial()
    {
        echo 'Generation blackInitialWords file...' . PHP_EOL;
        \Yii::$app->checkMessage->checkInitialMessages();
        echo 'Done!' . PHP_EOL;
    }

    /**
     * php yii check-message/check-phrases
     */
    public function actionCheckPhrases()
    {
        echo 'Generation blackPhraseWords file...' . PHP_EOL;
        \Yii::$app->checkMessage->checkPhrases();
        echo 'Done!' . PHP_EOL;
    }

    /**
     * php yii check-message/check-templates
     */
    public function actionCheckTemplates()
    {
        echo 'Generation blackPhraseWords file...' . PHP_EOL;
        \Yii::$app->checkMessage->checkTemplates();
        echo 'Done!' . PHP_EOL;
    }
}