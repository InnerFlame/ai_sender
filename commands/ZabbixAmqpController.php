<?php

namespace app\commands;

use app\config\Constant;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use yii\console\Controller;
use yii\helpers\Console;

class ZabbixAmqpController extends Controller
{
    const QUEUES = [
        'super',
        'logic',
        'controller.ctrl1',
        'controller.ctrl2',
        'controller.ctrl4',
        'sender.communicationsToSend',
        'sender.incomingMessages',
        'sender.interactionScamCheck',
        'sender.processGettingPlaceholder',
        'sender.rpcApiSearchResponse',
        'sender.rpcClientResponse',
        'sender.rpcErrorResponse',
        'sender.rpcRegisterResponse',
        'sender.rpcSentMessageResponse',
        'sender.saveIncomingMessage',
        'sender.scamCheckResponse',
    ];

    public function actionQueues()
    {
        $result = [];
        foreach (self::QUEUES as $queue) {
            $result[] = [
                '{#QUEUE_NAME}' => $queue,
            ];
        }
        Console::output(json_encode(['data' => $result]));
    }

    public function actionStatus($queue)
    {
        $connection = new AMQPStreamConnection(
            Constant::AMQP_HOST,
            Constant::AMQP_PORT,
            Constant::AMQP_USER,
            Constant::AMQP_PASS,
            Constant::AMQP_VHOST
        );
        $channel = $connection->channel();
        try {
            list($_, $messagesCount, $consumersCount) = $channel->queue_declare($queue, true);
        } catch (\Throwable $e) {
            $messagesCount = 0;
            $consumersCount = 0;
        }
        Console::output(json_encode([
            'messages' => $messagesCount,
            'consumers' => $consumersCount,
        ]));
    }
}
