<?php

namespace app\commands;

use yii\base\InvalidParamException;
use yii\console\Controller;
use app\models\phoenix\SenderProfileRemoveSchedule;
use app\models\phoenix\SenderProfile;

/**
 * Class RemoveSendersFromFileController
 * @package app\commands
 */
class RemoveSendersFromFileController extends Controller
{
    /**
     * php yii remove-senders-from-file/remove
     * @param null $fileName
     */
    public function actionRemove($fileName = null)
    {
        if (is_string($fileName)) {
            $filePath = \Yii::$app->basePath . '/web/files/' . $fileName;
        } else {
            $filePath = \Yii::$app->basePath . '/web/files/allUsersWithoutUserAgent.csv';
        }

        if (!is_file($filePath)) {
            throw new InvalidParamException('File with originIds not found in ' . $filePath);
        }

        $stringData = file_get_contents($filePath);
        $originIds  = explode(PHP_EOL, $stringData);

        $rows = $this->prepareRowsForInsert($originIds);

        if (!empty($rows)) {
            \Yii::$app->db
                ->createCommand()
                ->batchInsert(SenderProfileRemoveSchedule::tableName(), ['senderId', 'timeToRemove'], $rows)
                ->execute();
        }
    }

    /**
     * @param array $originIds
     * @return array
     */
    private function prepareRowsForInsert(array $originIds): array
    {
        /** @var $profiles SenderProfile [] */
        $profiles = SenderProfile::find()
            ->where(['in', 'originId', $originIds])->all();

        $ids      = [];
        $minutes  = 2;
        $oneHour  = strtotime('+1 hour');

        foreach ($profiles as $profile) {
            $time  = strtotime("+{$minutes} minute", $oneHour);

            $ids[] = [
                'senderId'     => $profile->id,
                'timeToRemove' => date('Y-m-d H:i:s', $time)
            ];

            $minutes += 2;
        }

        return $ids;
    }

}