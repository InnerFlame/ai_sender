<?php

namespace app\commands\workers;

use yii\console\Controller;
use app\components\IterativeRunner;

class RpcErrorController extends Controller
{
    /**
     * php yii workers/rpc-error/process-error
     */
    public function actionProcessError()
    {
        $runner = new IterativeRunner(
            [$this, 'processError'],
            [],
            5,
            0
        );

        $runner->run();
    }

    public function processError()
    {
        \Yii::$app->rpcError->processError();
    }
}
