<?php

namespace app\commands\workers;

use yii\console\Controller;
use app\components\IterativeRunner;

class PhoenixCommRunnerController extends Controller
{
    /**
     * yii workers/phoenix-comm-runner/prepare-communication-schedule
     */
    public function actionPrepareCommunicationSchedule()
    {
        $runner = new IterativeRunner(
            [$this, 'prepareCommunicationSchedule'],
            [],
            5,
            0
        );

        $runner->run();
    }

    public function prepareCommunicationSchedule()
    {
        \Yii::$app->commScheduler->createSchedule();
    }

    /**
     * yii workers/phoenix-comm-runner/prepare-communications
     */
    public function actionPrepareCommunications()
    {
        $runner = new IterativeRunner(
            [$this, 'prepareCommunications'],
            [],
            1,
            0
        );

        $runner->run();
    }

    public function prepareCommunications()
    {
        \Yii::$container->get('commProcessor')->prepareCommunications();
    }

    /**
     * yii workers/phoenix-comm-runner/process-communications
     */
    public function actionProcessCommunications()
    {
        $runner = new IterativeRunner(
            [$this, 'processCommunications'],
            [],
            1,
            0
        );

        $runner->run();
    }

    public function processCommunications()
    {
        \Yii::$container->get('commProcessor')->processCommunications();
    }

    /**
     * yii workers/phoenix-comm-runner/process-eva-messages
     */
    public function actionProcessEvaMessages()
    {
        $runner = new IterativeRunner(
            [$this, 'processEvaMessages'],
            [],
            5,
            0
        );

        $runner->run();
    }

    public function processEvaMessages()
    {
        \Yii::$app->stepByStepScheduler->processEvaMessages();
    }

    /**
     * php yii workers/phoenix-comm-runner/process-sent-message
     */
    public function actionProcessSentMessage()
    {
        $runner = new IterativeRunner(
            [$this, 'processSentMessage'],
            [],
            5,
            0
        );

        $runner->run();
    }

    public function processSentMessage()
    {
        \Yii::$container->get('commProcessorRpc')->processSentMessage();
    }

    /**
     * php yii workers/phoenix-comm-runner/process-incoming-messages
     */
    public function actionProcessIncomingMessages()
    {
        $runner = new IterativeRunner(
            [$this, 'processIncomingMessages'],
            [],
            5,
            0
        );

        $runner->run();
    }

    public function processIncomingMessages()
    {
        \Yii::$app->stepByStepScheduler->processIncomingMessages();
    }

    /**
     * php yii workers/phoenix-comm-runner/process-placeholder
     */
    public function actionProcessPlaceholder()
    {
        $runner = new IterativeRunner(
            [$this, 'processPlaceholder'],
            [],
            5,
            0
        );

        $runner->run();
    }

    public function processPlaceholder()
    {
        \Yii::$container->get('placeholderProcessor')->processPlaceholder();
    }

    /**
     * php yii workers/phoenix-comm-runner/process-get-placeholder
     */
    public function actionProcessGetPlaceholder()
    {
        $runner = new IterativeRunner(
            [$this, 'processGetPlaceholder'],
            [],
            1,
            0
        );

        $runner->run();
    }

    public function processGetPlaceholder()
    {
        \Yii::$container->get('placeholderProcessor')->processGetPlaceholder();
    }
}