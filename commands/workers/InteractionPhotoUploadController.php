<?php

namespace app\commands\workers;

use yii\console\Controller;
use app\components\IterativeRunner;

class InteractionPhotoUploadController extends Controller
{
    /**
     * php yii workers/interaction-photo-upload/process-photo-upload-events
     */
    public function actionProcessPhotoUploadEvents()
    {
        $runner = new IterativeRunner(
            [$this, 'processPhotoUploadEvents'],
            [],
            10,
            0
        );

        $runner->run();
    }

    public function processPhotoUploadEvents()
    {
        \Yii::$container->get('interactionPhotoUpload')->processPhotoUploadEvents();
    }

    /**
     * php yii workers/interaction-photo-upload/process-photo-upload-error-events
     */
    public function actionProcessPhotoUploadErrorEvents()
    {
        $runner = new IterativeRunner(
            [$this, 'processPhotoUploadErrorEvents'],
            [],
            10,
            0
        );

        $runner->run();
    }

    public function processPhotoUploadErrorEvents()
    {
        \Yii::$container->get('interactionPhotoUpload')->processPhotoUploadErrorEvents();
    }

    /**
     * php yii workers/interaction-photo-upload/check-photo-statuses
     */
    public function actionCheckPhotoStatuses()
    {
        $runner = new IterativeRunner(
            [$this, 'checkPhotoStatuses'],
            [],
            60,
            0
        );

        $runner->run();
    }

    public function checkPhotoStatuses()
    {
        \Yii::$container->get('interactionPhotoApprove')->checkPhotoStatuses();
    }

    /**
     * php yii workers/interaction-photo-upload/process-no-photo-events
     */
    public function actionProcessNoPhotoEvents()
    {
        $runner = new IterativeRunner(
            [$this, 'processNoPhotoEvents'],
            [],
            10,
            0
        );

        $runner->run();
    }

    public function processNoPhotoEvents()
    {
        \Yii::$container->get('interactionPhotoApprove')->processNoPhotoEvents();
    }
}
