<?php

namespace app\commands\workers;

use yii\console\Controller;
use app\components\IterativeRunner;

class PhoenixSearchController extends Controller
{
    /**
     * yii workers/phoenix-search/reset-search-count
     */
    public function actionResetSearchCount()
    {
        \Yii::$app->searcher->resetSearchesCount();
    }

    /**
     * yii workers/phoenix-search/run-search
     */
    public function actionRunSearch()
    {
        $runner = new IterativeRunner(
            [$this, 'runSearch'],
            [],
            30
        );

        $runner->run();
    }

    public function runSearch()
    {
        \Yii::$app->searcher->runSearch();
    }

    /**
     * php yii workers/phoenix-search/process-search-result
     */
    public function actionProcessSearchResult()
    {
        $runner = new IterativeRunner(
            [$this, 'processSearchResult'],
            [],
            5,
            0
        );

        $runner->run();
    }

    public function processSearchResult()
    {
        \Yii::$app->searchRpc->processSearchResult();
    }
}
