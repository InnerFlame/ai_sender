<?php

namespace app\commands\workers;

use app\components\IterativeRunner;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use yii\console\Controller;

class FetchProfileController extends Controller
{
    /**
     * php yii workers/fetch-profile/fetch-process
     */
    public function actionFetchProcess()
    {
        $runner = new IterativeRunner(
            [$this, 'fetchProcess'],
            [],
            10,
            0
        );

        $runner->run();
    }

    public function fetchProcess()
    {
        \Yii::$container->get('fetchProcessor')->processFetch();
    }
}