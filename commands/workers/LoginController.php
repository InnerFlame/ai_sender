<?php

namespace app\commands\workers;

use app\components\IterativeRunner;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use yii\console\Controller;

class LoginController extends Controller
{
    /**
     * php yii workers/login/re-login
     */
    public function actionReLogin()
    {
        $runner = new IterativeRunner(
            [$this, 'reLogin'],
            [],
            10,
            0
        );

        $runner->run();
    }

    public function reLogin()
    {
        $clients = Client::find()
            ->where(['status' => Client::STATUS_WAIT])
            ->all();

        $clients = array_filter($clients, function($client) {return $client->timeSetStatus < (time() - 30);});

        foreach ($clients as $client) {
            $profile = SenderProfile::findOne($client->profileId);

            if ($profile) {
                \Yii::$app->rpcMethods->loginRpcClient($profile, $client->type, $client->target);
            }
        }
    }
}