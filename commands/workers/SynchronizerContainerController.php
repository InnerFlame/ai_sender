<?php

namespace app\commands\workers;

use yii\console\Controller;
use app\components\IterativeRunner;
use yii\redis\ActiveRecord;

class SynchronizerContainerController extends Controller
{
    /**
     * php yii workers/synchronizer-container/process-containers-synchronization
     */
    public function actionProcessContainersSynchronization()
    {
        $runner = new IterativeRunner(
            [$this, 'processContainersSynchronization'],
            [],
            900
        );

        $runner->run();
    }

    public function processContainersSynchronization()
    {
        \Yii::$app->containersSynchronization->processContainersSynchronization();
    }
}