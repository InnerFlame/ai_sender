<?php

namespace app\commands\workers;

use yii\console\Controller;
use app\components\IterativeRunner;

class EventListenerController extends Controller
{
    /**
     * yii workers/event-listener/process-rpc-events
     */
    public function actionProcessRpcEvents()
    {
        $runner = new IterativeRunner(
            [$this, 'processRpcEvents'],
            []
        );

        $runner->run();
    }

    public function processRpcEvents()
    {
        \Yii::$app->rpcEventsProcessor->processEvents();
    }

    /**
     * yii workers/event-listener/process-presence-events
     */
    public function actionProcessPresenceEvents()
    {
        $runner = new IterativeRunner(
            [$this, 'processPresenceEvents'],
            []
        );

        $runner->run();
    }

    public function processPresenceEvents()
    {
        \Yii::$app->presenceEventsProcessor->processEvents();
    }
}