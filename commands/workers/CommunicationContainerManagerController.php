<?php

namespace app\commands\workers;

use yii\console\Controller;
use app\components\IterativeRunner;

class CommunicationContainerManagerController extends Controller
{
    /**
     * yii workers/communication-container-manager/update-pool
     */
    public function actionUpdatePool()
    {
        $runner = new IterativeRunner(
            [$this, 'updatePool'],
            [],
            60,
            0
        );

        $runner->run();
    }

    public function updatePool()
    {
        \Yii::$container->get('communicationContainerManager')->updatePool();
    }

    /**
     * yii workers/communication-container-manager/start-containers
     */
    public function actionStartContainers()
    {
        $runner = new IterativeRunner(
            [$this, 'startContainers'],
            [],
            10,
            0
        );

        $runner->run();
    }

    public function startContainers()
    {
        \Yii::$container->get('communicationContainerManager')->startContainers();
    }

}