<?php

namespace app\commands\workers;

use yii\console\Controller;
use app\components\IterativeRunner;

class SenderScamCheckController extends Controller
{
    /**
     * php yii workers/sender-scam-check/process-sender-scam-check
     */
    public function actionProcessSenderScamCheck()
    {
        $runner = new IterativeRunner(
            [$this, 'processSenderScamCheck'],
            [],
            10,
            0
        );

        $runner->run();
    }

    public function processSenderScamCheck()
    {
        \Yii::$container->get('scamCheckRpc')->processSearchResult();
    }
}
