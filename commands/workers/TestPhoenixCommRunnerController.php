<?php

namespace app\commands\workers;

use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use app\models\rpc\Container;
use yii\console\Controller;

class TestPhoenixCommRunnerController extends Controller
{
    /**
     * @param int $limit
     * @param int $offset
     * yii workers/test-phoenix-comm-runner/create-containers
     */
    public function actionCreateContainers($limit = 30, $offset = 0)
    {
        $senders = SenderProfile::find()->where('originId IS NOT NULL')->limit($limit)->offset($offset)->all();

        foreach ($senders as $sender) {
            $rpcContainer = \Yii::$app->rpcClient->getRpcContainer($sender, 'phoenix');
            if (empty($rpcContainer) || $rpcContainer->status != Container::STATUS_STARTED) {
                $proxy = \Yii::$app->proxyManager->getProxy($sender, 'phoenix');
                \Yii::$app->rpcClient->startRpcContainer($sender, 'phoenix', $proxy, Container::TYPE_COMMUNICATION);
            }
            sleep(3);
        }
    }

    /**
     * @param $id
     * yii workers/test-phoenix-comm-runner/create-container-by-id
     */
    public function actionCreateContainerById($id)
    {
        $sender = SenderProfile::findOne($id);

        if ($sender) {
            \Yii::$app->rpcMethods->createRpcContainer($sender, Container::TYPE_OBSERVE, 'phoenix');
        }
    }


    /**
     * @param int $limit
     * @param int $offset
     * @return bool
     *  yii workers/test-phoenix-comm-runner/create-clients
     */
    public function actionCreateClients($limit = 30, $offset = 0)
    {
        $senders = SenderProfile::find()->where('originId IS NOT NULL')->limit($limit)->offset($offset)->all();

        foreach ($senders as $sender) {
            $rpcContainer = \Yii::$app->rpcClient->getRpcContainer($sender, 'phoenix');
            //can`t create client if no container or it`s status still not defined
            if (empty ($rpcContainer) || $rpcContainer->status != Container::STATUS_STARTED) {
                continue;
            }

            $this->createClient($sender);

            sleep(3);
        }

        return true;
    }
    
    /**
     * @param $id
     * yii workers/test-phoenix-comm-runner/create-client-by-id
     */
    public function actionCreateClientById($id)
    {
        $sender = SenderProfile::findOne($id);

        $rpcContainer = \Yii::$app->rpcClient->getRpcContainer($sender, 'phoenix');
        //can`t create client if no container or it`s status still not defined
        if (empty ($rpcContainer) || $rpcContainer->status != Container::STATUS_STARTED) {
            echo 'no container';
        }

        $this->createClient($sender);

        echo 'client.' . $id . 'in process';
    }

    protected function createClient($sender)
    {
        $rpcClient = \Yii::$app->rpcClient->getRpcClient($sender, 'phoenix');
        if (empty($rpcClient) || $rpcClient->status != Container::STATUS_STARTED) {
            //create client only if it is not existed
            $yaml          = \Yii::$app->yaml;
            $language      = $sender->language;
            $country       = $sender->country;
            $userAgent     = $yaml->parseByKey('userAgentMob', $sender->userAgent);
            $userAgent     = str_replace(['$(language)', '$(country)'], [$language, $country], $userAgent);

            $clientData = [
                'id'                       => $sender->id,
                'host'                     => $yaml->parseByKey('site', $sender->site)['name'],
                'email'                    => $sender->email,
                'password'                 => $sender->password,
                'birthDate'                => $sender->birthday,
                'deviceIdHex'              => $sender->deviceIdHex,
                'internalDeviceIdentifier' => $sender->internalDeviceIdentifier,
                'gtmClientId'              => $sender->gtmClientId,
                'location'                 => $sender->location,
                'userAgent'                => $userAgent,
                'locale'                   => [
                    'language' => $language,
                    'country'  => $country,
                ],
                'app'                      => [
                    'bundle'  => $yaml->parseByKey('bundles', $sender->site),
                    'version' => $yaml->parseByKey('versions', $sender->version),
                    'marker'  => $sender->marker,
                ],
                'proxy'                    => \Yii::$app->proxyManager->getProxy($sender, 'phoenix'),
            ];

            \Yii::$app->rpcClient->startRpcClient($clientData, 'phoenix', Client::TYPE_COMMUNICATION);
        }
    }
}