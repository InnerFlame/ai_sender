<?php

namespace app\commands\workers;

use yii\console\Controller;
use app\components\IterativeRunner;

class InteractionScamCheckController extends Controller
{
    /**
     * php yii workers/interaction-scam-check/process-scam-events
     */
    public function actionProcessScamEvents()
    {
        $runner = new IterativeRunner(
            [$this, 'processScamEvents'],
            [],
            10,
            0
        );

        $runner->run();
    }

    public function processScamEvents()
    {
        \Yii::$container->get('interactionScamCheck')->processScamEvents();
    }
}
