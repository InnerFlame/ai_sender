<?php

namespace app\commands\workers;

use yii\console\Controller;
use app\components\IterativeRunner;

class SenderProfileRemoveController extends Controller
{
    /**
     * php yii workers/sender-profile-remove/remove
     */
    public function actionRemove()
    {
        $runner = new IterativeRunner(
            [$this, 'removeReadyProfiles'],
            [],
            10,
            0
        );

        $runner->run();
    }

    public function removeReadyProfiles()
    {
        \Yii::$container->get('senderProfileRemoveProcessor')->removeReadyProfiles();
    }

    /**
     * php yii workers/sender-profile-remove/schedule-remove
     */
    public function actionScheduleRemove()
    {
        $runner = new IterativeRunner(
            [$this, 'scheduleRemove'],
            [],
            10,
            0
        );

        $runner->run();
    }

    public function scheduleRemove()
    {
        \Yii::$container->get('senderProfileRemoveScheduler')->scheduleRemove();
    }
}
