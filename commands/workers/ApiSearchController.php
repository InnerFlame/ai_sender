<?php

namespace app\commands\workers;

use yii\console\Controller;
use app\components\IterativeRunner;

class ApiSearchController extends Controller
{
    /**
     * yii workers/api-search/run-search
     */
    public function actionRunSearch()
    {
        $runner = new IterativeRunner(
            [$this, 'runSearch'],
            [],
            60
        );

        $runner->run();
    }

    public function runSearch()
    {
        \Yii::$app->apiSearch->runSearch();
    }

    /**
     * php yii workers/api-search/process-search-result
     */
    public function actionProcessSearchResult()
    {
        $runner = new IterativeRunner(
            [$this, 'processSearchResult'],
            [],
            5,
            0
        );

        $runner->run();
    }

    public function processSearchResult()
    {
        \Yii::$container->get('apiSearchRpc')->processSearchResult();
    }
}
