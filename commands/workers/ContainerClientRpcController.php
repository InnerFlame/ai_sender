<?php

namespace app\commands\workers;

use yii\console\Controller;
use app\components\IterativeRunner;

class ContainerClientRpcController extends Controller
{
    /**
     * php yii workers/container-client-rpc/process-container-client
     */
    public function actionProcessContainerClient()
    {
        $runner = new IterativeRunner(
            [$this, 'processContainerClient'],
            [],
            5,
            0
        );

        $runner->run();
    }

    public function processContainerClient()
    {
        \Yii::$app->containerClientRpc->processContainerClient();
    }
}
