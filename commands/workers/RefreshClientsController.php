<?php

namespace app\commands\workers;

use yii\console\Controller;
use app\components\IterativeRunner;

class RefreshClientsController extends Controller
{
    /**
     * yii workers/refresh-clients/refresh
     */
    public function actionRefresh()
    {
        $runner = new IterativeRunner(
            [$this, 'refreshClients'],
            [],
            30,
            0
        );

        $runner->run();
    }

    public function refreshClients()
    {
        \Yii::$app->rpcClient->refreshRpcClients();
    }
}