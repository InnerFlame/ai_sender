<?php

namespace app\commands\workers;

use app\components\phoenix\report\AnswersRateStatComponent;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\RecipientProfile;
use yii\base\ErrorException;
use yii\console\Controller;

class AggregatorController extends Controller
{
    /**
     * php yii workers/aggregator/collect-sent-communication
     */
    public function actionCollectSentCommunication()
    {
        $startTime = time() - 900;
        $startTime = (int)floor($startTime / 900) * 900;
        $endTime = time();
        $endTime = (int)floor($endTime / 900) * 900;
        $dateStart = date('Y-m-d H:i:s', $startTime);
        $dateEnd   = date('Y-m-d H:i:s', $endTime - 1);
        
        try {
            \Yii::$app->statCommunication->collectSentCommunication($dateStart, $dateEnd);
        } catch (ErrorException $e) {
            echo $e->getMessage();
        }

        echo 'Have Done from date - ' . $dateStart . ' to - ' . $dateEnd;
    }

    /**
     * php yii workers/aggregator/collect-old-sent-communication
     * @param null $dateStart
     */
    public function actionCollectOldSentCommunication($dateStart = null)
    {
        if (empty($dateStart)) {
            $minData   = CommunicationSchedule::find()->where('sentAt > 0')->min('sentAt');
            $dateStart = date('Y-m-d H:00:00', strtotime($minData));
        }

        $dateEnd   = date('Y-m-d H:59:59', strtotime($dateStart));

        while ($dateStart < date('Y-m-d H:00:00')) {
            echo 'Processing from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL;

            try {
                \Yii::$app->statCommunication->collectSentCommunication($dateStart, $dateEnd);
            } catch (ErrorException $e) {
                echo $e->getMessage();
            }

            $dateStart = date('Y-m-d H:00:00', strtotime($dateStart) + 3600);
            $dateEnd   = date('Y-m-d H:59:59', strtotime($dateStart));

            echo 'Have done process from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL . PHP_EOL;
        }

        echo 'Have Done to date - ' . $dateEnd;
    }

    /**
     * php yii workers/aggregator/collect-yesterday-sent-communication
     */
    public function actionCollectYesterdaySentCommunication()
    {
        $dateStart       = date('Y-m-d 00:00:00', time() - 3600 * 24);
        $dateEnd         = date('Y-m-d 00:59:59', time() - 3600 * 24);
        $dateEndScript   = date('Y-m-d 23:59:59', time() - 3600 * 24);

        while ($dateStart < $dateEndScript) {
            echo 'Processing from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL;

            \Yii::$app->statCommunication->collectSentCommunication($dateStart, $dateEnd);

            echo 'Have done process from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL . PHP_EOL;

            $dateStart = date('Y-m-d H:00:00', strtotime($dateStart) + 3600);
            $dateEnd   = date('Y-m-d H:59:59', strtotime($dateStart));
        }

        echo 'Have Done to date - ' . $dateEnd;
    }

    /**
     * php yii workers/aggregator/collect-unprocessed-recipient
     */
    public function actionCollectUnprocessedRecipient()
    {
        $dateStart = date('Y-m-d 00:00:00', time() - 86400);
        $dateEnd   = date('Y-m-d 23:59:59', time() - 86400);

        \Yii::$app->unprocessedRecipientStats->collectUnprocessedRecipient($dateStart, $dateEnd);

        echo 'Have Done from date - ' . $dateStart . ' to - ' . $dateEnd;
    }

    /**
     * php yii workers/aggregator/collect-old-unprocessed-recipient
     * @param null $dateStart
     */
    public function actionCollectOldUnprocessedRecipient($dateStart = null)
    {
        if (empty($dateStart)) {
            $minData   = RecipientProfile::find()->min('createdAt');
            $dateStart = date('Y-m-d H:00:00', strtotime($minData));
        }

        $dateEnd = date('Y-m-d H:59:59', strtotime($dateStart));

        while ($dateStart < date('Y-m-d H:00:00')) {
            echo 'Processing from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL;

            \Yii::$app->unprocessedRecipientStats->collectUnprocessedRecipient($dateStart, $dateEnd);

            $dateStart = date('Y-m-d H:00:00', strtotime($dateStart) + 3600);
            $dateEnd   = date('Y-m-d H:59:59', strtotime($dateStart));

            echo 'Have done process from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL . PHP_EOL;
        }

        echo 'Have Done to date - ' . $dateEnd;
    }

    /**
     * php yii workers/aggregator/collect-yesterday-unprocessed-recipient
     */
    public function actionCollectYesterdayUnprocessedRecipient()
    {
        $dateStart       = date('Y-m-d 00:00:00', time() - 3600 * 24);
        $dateEnd         = date('Y-m-d 00:59:59', time() - 3600 * 24);
        $dateEndScript   = date('Y-m-d 23:59:59', time() - 3600 * 24);

        while ($dateStart < $dateEndScript) {
            echo 'Processing from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL;

            \Yii::$app->unprocessedRecipientStats->collectUnprocessedRecipient($dateStart, $dateEnd);

            echo 'Have done process from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL . PHP_EOL;

            $dateStart = date('Y-m-d H:00:00', strtotime($dateStart) + 3600);
            $dateEnd   = date('Y-m-d H:59:59', strtotime($dateStart));
        }

        echo 'Have Done to date - ' . $dateEnd;
    }

    /**
     * php yii workers/aggregator/collect-delay-stat
     */
    public function actionCollectDelayStat()
    {
        $startTime = time() - 1800;
        $startTime = (int)floor($startTime / 900) * 900;
        $endTime = time();
        $endTime = (int)floor($endTime / 900) * 900;
        $dateStart = date('Y-m-d H:i:s', $startTime);
        $dateEnd   = date('Y-m-d H:i:s', $endTime - 1);

        \Yii::$app->delayStat->collectPlannedCommunication($dateStart, $dateEnd);
        \Yii::$app->delayStat->collectDelaySentCommunication($dateStart, $dateEnd);

        echo 'Have Done from date - ' . $dateStart . ' to - ' . $dateEnd;
    }

    /**
     * php yii workers/aggregator/collect-old-delay-stat
     * @param null $dateStart
     */
    public function actionCollectOldDelayStat($dateStart = null)
    {
        if (empty($dateStart)) {
            $minData   = RecipientProfile::find()->min('createdAt');
            $dateStart = date('Y-m-d H:00:00', strtotime($minData));
        }

        $dateEnd = date('Y-m-d H:59:59', strtotime($dateStart));

        while ($dateStart < date('Y-m-d H:00:00')) {
            echo 'Processing from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL;

            \Yii::$app->delayStat->collectPlannedCommunication($dateStart, $dateEnd);
            \Yii::$app->delayStat->collectDelaySentCommunication($dateStart, $dateEnd);

            $dateStart = date('Y-m-d H:00:00', strtotime($dateStart) + 3600);
            $dateEnd   = date('Y-m-d H:59:59', strtotime($dateStart));

            echo 'Have done process from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL . PHP_EOL;
        }

        echo 'Have Done to date - ' . $dateEnd;
    }

    /**
     * php yii workers/aggregator/collect-yesterday-delay-stat
     */
    public function actionCollectYesterdayDelayStat()
    {
        $dateStart       = date('Y-m-d 00:00:00', time() - 3600 * 24);
        $dateEnd         = date('Y-m-d 00:59:59', time() - 3600 * 24);
        $dateEndScript   = date('Y-m-d 23:59:59', time() - 3600 * 24);

        while ($dateStart < $dateEndScript) {
            echo 'Processing from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL;

            \Yii::$app->delayStat->collectPlannedCommunication($dateStart, $dateEnd);
            \Yii::$app->delayStat->collectDelaySentCommunication($dateStart, $dateEnd);

            echo 'Have done process from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL . PHP_EOL;

            $dateStart = date('Y-m-d H:00:00', strtotime($dateStart) + 3600);
            $dateEnd   = date('Y-m-d H:59:59', strtotime($dateStart));
        }

        echo 'Have Done to date - ' . $dateEnd;
    }

    /**
     * php yii workers/aggregator/collect-rpc-error-log
     */
    public function actionCollectRpcErrorLog()
    {
        $dateStart = date('Y-m-d H:00:00', time() - 3600);
        $dateEnd   = date('Y-m-d H:59:59', time() - 3600);

        \Yii::$app->rpcErrorLog->collectRpcErrorLog($dateStart, $dateEnd);

        echo 'Done! - ' . date('Y-m-d H:i:s');
    }

    public function actionDeleteOldRpcErrorLog()
    {
        \Yii::$app->rpcErrorLog->deleteOlRpcErrorLog();

        echo 'Done! - ' . date('Y-m-d H:i:s');
    }

    /**
     * php yii workers/aggregator/collect-sender-ban-stat
     */
    public function actionCollectSenderBanStat()
    {
        $startTime = time() - 1800;
        $startTime = (int)floor($startTime / 900) * 900;
        $endTime = time();
        $endTime = (int)floor($endTime / 900) * 900;
        $dateStart = date('Y-m-d H:i:s', $startTime);
        $dateEnd   = date('Y-m-d H:i:s', $endTime - 1);

        \Yii::$app->senderBanStat->collectSenderBanStat($dateStart, $dateEnd);
    }

    /**
     * php yii workers/aggregator/aggregate-answer-rate-stat
     */
    public function actionAggregateAnswerRateStat()
    {
        /** @var AnswersRateStatComponent $answerRateStatComponent */
        $answerRateStatComponent = \Yii::$app->answersRateStat;

        $dateStart = date('Y-m-d 00:00:00');
        $dateEnd   = date('Y-m-d 23:59:59');
        $answerRateStatComponent->aggregateStatData($dateStart, $dateEnd);
    }
}
