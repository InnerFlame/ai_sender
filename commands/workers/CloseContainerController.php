<?php

namespace app\commands\workers;

use app\components\IterativeRunner;
use app\config\Constant;
use app\models\rpc\Container;
use yii\console\Controller;

class CloseContainerController extends Controller
{
    /**
     * yii workers/close-container/close-all-containers
     */
    public function actionCloseAllContainers()
    {
        $containers = Container::findAll([
            'status' => Container::STATUS_STARTED,
            'target' => Constant::TARGET_PHOENIX,
        ]);

        foreach ($containers as $container) {
            \Yii::$app->rpcClient->stopRpcContainer($container->name, $container->target);
        }
    }

    /**
     * yii workers/close-container/close-expired-containers
     */
    public function actionCloseExpiredContainers()
    {
        $runner = new IterativeRunner(
            [$this, 'closeExpiredContainers'],
            [],
            600,
            0
        );

        $runner->run();
    }

    public function closeExpiredContainers()
    {
        \Yii::$container->get('expiredCloser')->stopContainers();
    }
}

