<?php

namespace app\commands\workers;

use yii\console\Controller;
use app\components\IterativeRunner;

class ReUploadController extends Controller
{
    /**
     * yii workers/re-upload/create-containers-for-re-upload
     */
    public function actionCreateContainersForReUpload()
    {
        $runner = new IterativeRunner(
            [$this, 'createContainersForReUpload'],
            [],
            60,
            0
        );

        $runner->run();
    }

    public function createContainersForReUpload()
    {
        \Yii::$app->reUploadProcessor->createRpcContainers();
    }

    /**
     * yii workers/re-upload/process-re-upload
     */
    public function actionProcessReUpload()
    {
        $runner = new IterativeRunner(
            [$this, 'processReUpload'],
            [],
            10,
            0
        );

        $runner->run();
    }

    public function processReUpload()
    {
        \Yii::$app->reUploadProcessor->processReUpload();
    }

    /**
     * yii workers/re-upload/process-interaction-uploads
     */
    public function actionProcessInteractionUploads()
    {
        $runner = new IterativeRunner(
            [$this, 'processInteractionUploads'],
            [],
            10,
            0
        );

        $runner->run();
    }

    public function processInteractionUploads()
    {
        \Yii::$app->reUploadProcessor->processInteractionUploads();
    }
}
