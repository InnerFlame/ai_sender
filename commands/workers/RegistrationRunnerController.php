<?php

namespace app\commands\workers;

use yii\console\Controller;
use app\components\IterativeRunner;

class RegistrationRunnerController extends Controller
{
    /**
     * php yii workers/registration-runner/process-registration-scenarios
     */
    public function actionProcessRegistrationScenarios()
    {
        $runner = new IterativeRunner(
            [$this, 'processRegistrationScenarios'],
            [],
            10,
            0
        );

        $runner->run();
    }

    public function processRegistrationScenarios()
    {
        \Yii::$container->get('registrationProcessor')->processRegistrationScenarios();
    }

    /**
     * php yii workers/registration-runner/create-containers-for-registration
     */
    public function actionCreateContainersForRegistration()
    {
        $runner = new IterativeRunner(
            [$this, 'createContainersForRegistration'],
            [],
            10,
            0
        );

        $runner->run();
    }

    public function createContainersForRegistration()
    {
        \Yii::$container->get('registrationProcessor')->createRpcContainersForScenarios();
    }

    /**
     * php yii workers/registration-runner/process-register
     */
    public function actionProcessRegister()
    {
        $runner = new IterativeRunner(
            [$this, 'processRegister'],
            [],
            5,
            0
        );

        $runner->run();
    }

    public function processRegister()
    {
        \Yii::$container->get('registerRpc')->processRegister();
    }

    /**
     * php yii workers/registration-runner/profile-activation
     */
    public function actionProfileActivation()
    {
        \Yii::$container->get('registrationProcessor')->activateProfileAfterRegister();
    }

    /**
     * php yii workers/registration-runner/creation-registration-schedule
     */
    public function actionCreationRegistrationSchedule()
    {
        \Yii::$container->get('registrationScheduler')->create();
    }
}
