<?php

namespace app\commands;

use app\models\phoenix\RecipientProfile;
use app\models\phoenix\RecipientShortId;
use yii\console\Controller;
use yii\db\Query;

/**
 * Class ShortidTransferController
 * @package app\commands
 */
class ShortidTransferController extends Controller
{
    /**
     * ./yii shortid-transfer/run
     *
     * @param int $batchSize
     * @throws \Exception
     */
    public function actionRun($batchSize = 500)
    {
        $offset = 0;
        $query = (new Query())
            ->select('r.id AS recipientId, s.shortId AS shortId, r.remoteId AS remoteId, r.country AS country, r.site AS site, s.createdAt AS createdAt')
            ->from(RecipientProfile::tableName() . ' as r')
            ->innerJoin(RecipientShortId::tableName() . ' as s', 's.recipientId=r.id')
            ->limit($batchSize)
            ->offset($offset);
        ;

        while ($batch = $query->createCommand()->queryAll()) {
            echo '.';
            \Yii::$app->db->createCommand()
                ->batchInsert(
                    RecipientShortId::tableName() . '_tmp',
                    ['recipientId', 'shortId', 'remoteId', 'country', 'site', 'createdAt'],
                    $batch
                )
                ->execute();
            $offset += $batchSize;
            $query->offset($offset);
        }

        \Yii::$app->db->createCommand()
            ->renameTable(RecipientShortId::tableName(), RecipientShortId::tableName() . '_original')
            ->execute();
        \Yii::$app->db->createCommand()
            ->renameTable(RecipientShortId::tableName() . '_tmp', RecipientShortId::tableName())
            ->execute();
    }

    /**
     * ./yii shortid-transfer/rollback
     *
     * @throws \yii\db\Exception
     */
    public function actionRollback()
    {
        \Yii::$app->db->createCommand()
            ->renameTable(RecipientShortId::tableName(), RecipientShortId::tableName() . '_tmp')
            ->execute();
        \Yii::$app->db->createCommand()
            ->renameTable(RecipientShortId::tableName(). '_original', RecipientShortId::tableName())
            ->execute();

    }
}