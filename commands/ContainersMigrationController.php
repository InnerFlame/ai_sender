<?php

namespace app\commands;

use app\models\rpc\Client;
use app\models\rpc\Container;
use yii\console\Controller;

class ContainersMigrationController extends Controller
{
    public function actionMigrate()
    {
        $containers = Container::find()->all();
        foreach ($containers as $container) {
            if (empty($container['type'])) {
                continue;
            }
            \Yii::$app->db->createCommand()->insert(
                'container',
                [
                    'id'                => $container['id'],
                    'profileId'         => $container['profileId'],
                    'name'              => $container['name'],
                    'status'            => $container['status'],
                    'target'            => $container['target'],
                    'type'              => $container['type'],
                    'lastTimeActivity'  => $container['lastTimeActivity'],
                    'timeSetStatus'     => $container['timeSetStatus'],
                    'country'           => $container['country'],
                    'site'              => $container['site'],
                ]
            )->execute();
        }

        $clients = Client::find()->all();
        foreach ($clients as $client) {
            if (empty($client['type'])) {
                continue;
            }
            \Yii::$app->db->createCommand()->insert(
                'client',
                [
                    'id'                => $client['id'],
                    'profileId'         => $client['profileId'],
                    'name'              => $client['name'],
                    'status'            => $client['status'],
                    'target'            => $client['target'],
                    'type'              => $client['type'],
                    'refreshingTime'    => $client['refreshingTime'],
                    'timeSetStatus'     => $client['timeSetStatus'],
                    'country'           => $client['country'],
                    'site'              => $client['site'],
                ]
            )->execute();
        }
    }
}