<?php

namespace app\commands;

use yii\console\Controller;
use yii\db\Query;

class RecipientProfileController extends Controller
{
    private $tableCur = 'recipientProfile';
    private $tableNew = 'recipientProfileNew';
    private $tableOld = 'recipientProfileOld';
    private $searchable = 'searchable';

    public $limit;

    public function options($actionID)
    {
        return ['limit'];
    }

    /**
     * Create searchable field to recipientProfile table and copy table with data.
     */
    public function actionSearchableCreate()
    {
        echo 'Begin action' . PHP_EOL;
        $this->searchableInitial();
        $this->searchableRecord($this->limit);
        $this->searchableFinish();
        echo 'Done action!' . PHP_EOL;
    }

    private function searchableRecord($limit)
    {
        $offset = 0;
        $fields = $this->searchablePrepareFields();
        do {
            $rows = $this->searchablePrepareRows($offset, $limit);
            if (!empty($rows)) {
                \Yii::$app->db
                    ->createCommand()
                    ->batchInsert($this->tableNew, $fields, $rows)
                    ->execute();
                echo 'Done package!' . PHP_EOL;
            }
            $offset += $limit;
            sleep(1);
        } while ($rows);
    }

    /**
     * @return array
     */
    private function searchablePrepareFields(): array
    {
        return \Yii::$app->db
            ->createCommand("SHOW COLUMNS FROM $this->tableCur;")
            ->queryColumn();
    }

    /**
     * @param $offset
     * @param $limit
     * @return array
     */
    private function searchablePrepareRows(int $offset, int $limit): array
    {
        return (new Query())
            ->select('*')
            ->from($this->tableCur)
            ->limit($limit)
            ->offset($offset)
            ->all();
    }

    private function searchableInitial()
    {
        $commands = [
            "CREATE TABLE $this->tableNew LIKE $this->tableCur;",
            "ALTER  TABLE $this->tableNew ADD COLUMN $this->searchable TINYINT(1) DEFAULT 1;",
        ];
        \Yii::$app->db
            ->createCommand(implode("\n", $commands))
            ->execute();
    }

    private function searchableFinish()
    {
        $commands = [
            "RENAME TABLE $this->tableCur TO $this->tableOld;",
            "RENAME TABLE $this->tableNew TO $this->tableCur;",
        ];
        \Yii::$app->db
            ->createCommand(implode("\n", $commands))
            ->execute();
    }
}