<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\rbac\UserGroupRule;
use app\models\common\User;

class RbacController extends Controller
{
    public function actionInit()
    {
        $authManager = Yii::$app->authManager;
        $authManager->removeAll();

        // Create roles
        $admin          = $authManager->createRole(User::ADMIN);
        $superAdmin     = $authManager->createRole(User::SUPER_ADMIN);
        $contentManager = $authManager->createRole(User::CONTENT_MANAGER);

        // Create simple, based on action{$NAME} permissions
        $create   = $authManager->createPermission('create');
        $crudUser = $authManager->createPermission('crudUser');
        $content  = $authManager->createPermission('content');

        // Add permissions in Yii::$app->authManager
        $authManager->add($create);
        $authManager->add($crudUser);
        $authManager->add($content);

        // Add rule, based on UserExt->group === $user->group
        $userGroupRule = new UserGroupRule();
        $authManager->add($userGroupRule);

        // Add rule "UserGroupRule" in roles
        $admin->ruleName          = $userGroupRule->name;
        $superAdmin->ruleName     = $userGroupRule->name;
        $contentManager->ruleName = $userGroupRule->name;

        // Add roles in Yii::$app->authManager
        $authManager->add($admin);
        $authManager->add($superAdmin);
        $authManager->add($contentManager);

        // Add permission-per-role in Yii::$app->authManager
        $authManager->addChild($contentManager, $content);

        $authManager->addChild($admin, $create);
        $authManager->addChild($admin, $contentManager);

        $authManager->addChild($superAdmin, $crudUser);
        $authManager->addChild($superAdmin, $content);
        $authManager->addChild($superAdmin, $admin);

    }
}