<?php

namespace app\commands;

use yii\console\Controller;

class CleanerController extends Controller
{
    /**
     * php yii cleaner/clean-recipient-data
     * @param $dateStart
     * @param $dateEnd
     */
    public function actionCleanRecipientData($dateStart, $dateEnd)
    {
        $dateEndScript = $dateEnd;
        while($dateStart < $dateEndScript) {
            $dateEnd = date('Y-m-d H:59:59', strtotime($dateStart));

            echo 'Processing from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL;
            \Yii::$app->recipientDataCleaner->clean($dateStart, $dateEnd);
            echo 'Have done process from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL . PHP_EOL;

            $dateStart = date('Y-m-d H:00:00', strtotime($dateStart) + 3600);
        }
    }

    /**
     * php yii cleaner/overflow-recipient-data
     */
    public function actionOverflowRecipientData()
    {
        $dateStart = date('Y-m-d H:i:s', strtotime('now -14 day'));
        \Yii::$container->get('overflowData')->process($dateStart);
    }

    /**
     * php yii cleaner/overflow-recipient
     */
    public function actionOverflowRecipient()
    {
        $dateStart = date('Y-m-d H:i:s', strtotime('now -14 day'));
        $params    = [
            'overflowDataList' => ['Recipient'],
        ];
        \Yii::$container->get('overflowData', $params)->process($dateStart);
    }

    /**
     * php yii cleaner/create-partition
     * @param $entity
     */
    public function actionCreatePartition($entity)
    {
        $params = [];
        if (!empty($entity)){
            $params    = [
                'overflowDataList' => [$entity],
            ];
        }

        \Yii::$container->get('overflowData', $params)->createPartition(0, 4);
    }

    /**
     * php yii cleaner/drop-partition
     * @param $entity
     */
    public function actionDropPartition($entity = null)
    {
        $params = [];
        if (!empty($entity)){
            $params    = [
                'overflowDataList' => [$entity],
            ];
        }

        \Yii::$container->get('overflowData', $params)->dropPartition(34, 30);
    }
}
