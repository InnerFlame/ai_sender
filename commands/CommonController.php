<?php

namespace app\commands;

use app\config\Constant;
use app\models\phoenix\SenderPhoto;
use app\models\phoenix\SenderProfile;
use yii\console\Controller;

class CommonController extends Controller
{
    /**
     * php yii common/push-email-to-db
     */
    public function actionPushEmailToDb()
    {
        echo 'Start process  pushing emails to db' . PHP_EOL;
        \Yii::$container->get('emailGenerator')->pushEmailToDb();
        echo 'End process  pushing emails to db' . PHP_EOL;
    }

    /**
     * php yii common/check-emails
     */
    public function actionCheckEmails()
    {
        echo 'Start process checking emails' . PHP_EOL;
        \Yii::$container->get('emailGenerator')->filterEmailsForUnreadableEmail();
        echo 'End process checking emails' . PHP_EOL;
    }

    /**
     * php yii common/set-time-offset
     */
    public function actionSetTimeOffset()
    {
        $senders  = SenderProfile::find()->where('ip IS NOT NULL and timeOffset IS NULL')->all();

        foreach ($senders as $sender) {
            $proxy              = \Yii::$app->proxyManager->getProxy($sender, Constant::TARGET_PHOENIX);
            $sender->timeOffset = \Yii::$app->geoIp->getTimezoneByIp($sender->ip, $proxy);
            if ($sender->save()) {
                echo 'Save for ' . $sender->id . PHP_EOL;
            }
        }

        echo 'Done!' . PHP_EOL;
    }

    /**
     * php yii common/validate-and-mark-bad-photo
     */
    public function actionValidateAndMarkBadPhoto()
    {
        $query = SenderPhoto::find()
            ->where(['isBad' => 0]);
        foreach ($query->batch() as $senderPhotos) {
            foreach ($senderPhotos as $senderPhoto) {
                $senderPhoto->validateAndMarkBadPhoto();
            }
        }
    }
}