#!/usr/bin/env bash

function colorEcho() {
    echo -e "\e[1;33m$@\e[0m"
}

# cd proect root dir
cd "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)/../"

colorEcho Composer install
/home/sender/bin/composer install --no-interaction

colorEcho Clear YAML cache
./yii maintenance/clear-yaml-cache

colorEcho Migrate database
./yii migrate --interactive=0
