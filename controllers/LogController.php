<?php

namespace app\controllers;

use app\models\phoenix\SenderWithoutPhotoLog;
use sender\registration\models\RegisterErrorLog;
use Yii;
use app\models\Log;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * LogController implements the CRUD actions for Log model.
 */
class LogController extends BaseController
{
    /**
     * Lists all Log models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = \Yii::$app->request->get();
        $dataProvider = new ActiveDataProvider([
            'query' => Log::getLogs($request),
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'request'      => $request,
        ]);
    }

    public function actionView(int $id)
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    protected function findModel(int $id)
    {
        if (($model = Log::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionClean()
    {
        Yii::$app->db->createCommand()->truncateTable(Log::tableName())->execute();
        $this->redirect('index');
    }

    public function actionFrozenClients()
    {
        $data  = \Yii::$app->redis->zrange('frozenClients', 0, -1);
        $count = count($data);

        return $this->render('frozen-clients', [
            'data'  => $data,
            'count' => $count,
        ]);
    }

    public function actionSenderWithoutPhoto()
    {
        $request      = \Yii::$app->request->get();
        $dataProvider = new ActiveDataProvider([
            'query'      => SenderWithoutPhotoLog::getLogs($request),
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        return $this->render('senderWithoutPhoto', [
            'dataProvider' => $dataProvider,
            'request'      => $request,
        ]);
    }

    public function actionCleanSenderWithoutPhoto()
    {
        \Yii::$app->db->createCommand()->truncateTable(SenderWithoutPhotoLog::tableName())->execute();
        $this->redirect('sender-without-photo');
    }

    public function actionRegistrationErrorLog()
    {
        $request      = \Yii::$app->request->get();
        $dataProvider = new ActiveDataProvider([
            'query'      => RegisterErrorLog::getLogs($request),
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        return $this->render('registrationErrorLog', [
            'dataProvider' => $dataProvider,
            'request'      => $request,
        ]);
    }

    public function actionCleanRegistrationErrorLog()
    {
        \Yii::$app->db->createCommand()->truncateTable(RegisterErrorLog::tableName())->execute();
        $this->redirect('registration-error-log');
    }
}
