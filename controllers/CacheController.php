<?php

namespace app\controllers;

/**
 * ContainerController implements the CRUD actions for Container model.
 */
class CacheController extends BaseController
{
    public function actionClearYaml()
    {
        if (\Yii::$app->request->get('clear')){
            \Yii::$app->yaml->clearCache();

            return $this->redirect('clear-yaml');
        }

        return $this->render('clear-yaml');
    }
}