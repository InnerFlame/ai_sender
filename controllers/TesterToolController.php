<?php

namespace app\controllers;

use app\components\phoenix\report\TesterToolComponent;
use app\models\phoenix\RecipientProfile;
use sender\split\components\SplitSystem;
use sender\split\models\Split;
use yii\base\ErrorException;
use yii\base\InvalidParamException;
use yii\base\Module;
use yii\web\Response;

/**
 * Class TesterToolController
 * @package app\controllers
 */
class TesterToolController extends BaseController
{
    /**
     * @var $testerToolComponent TesterToolComponent
     */
    private $testerToolComponent;
    /**
     * @var $response array
     */
    private $response = ['status' => 'ok', 'content' => '', 'message' => ''];

    /**
     * @inheritdoc
     * TesterToolController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->testerToolComponent = \Yii::$app->tool;
    }

    /**
     * @inheritdoc
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action): bool
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        if (in_array($action->id, ['index', 'view-messages', 'check-split-user', 'force-change-user-split'])) {
            return true;
        }

        if (!\Yii::$app->request->isAjax) {
           return false;
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return true;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Get data from tester tools component
     * @return mixed
     */
    public function actionGetData()
    {
        try {
            $this->setResponseParam('ok', 'status');

            $post = \Yii::$app->request->post();

            if (!isset($post['ids'])) {
                throw new ErrorException ('The field id is empty');
            }

            $content = $this->renderPartial('tables', [
                'data' => $this->testerToolComponent->getData($post['ids']),
            ]);
            $this->setResponseParam($content, 'content');
        } catch(ErrorException $e) {
            $this->setResponseParam('error', 'status');
            $this->setResponseParam($e->getMessage(), 'message');
        }

        return $this->getResponse();
    }

    public function actionCheckSplitUser()
    {
        $resultData = [];
        $splits     = \Yii::$container->get('splitSystem')->getAllSplitsForDropDown();
        $userId     = \Yii::$app->request->post('userId');
        $splitName  = \Yii::$app->request->post('splitName');

        if ($userId && $splitName) {
            $resultData['userId']    = $userId;
            $resultData['splitName'] = $splitName;

            $recipientModel = RecipientProfile::find()->where(['remoteId' => $userId])->asArray()->one();
            if (!$recipientModel) {
                \Yii::$app->session->setFlash('warning', 'User with such id does not exists!');

                return $this->render('check-user', [
                    'resultData' => $resultData,
                    'splits'     => $splits
                ]);
            }

            $resultData = [
                'userData'      => $recipientModel,
                'userSplitData' => \Yii::$container->get('splitSystem')->getSplitModelByName($splitName),
                'splitGroup'    => \Yii::$container->get('splitSystem')->getSplitResult($recipientModel, $splitName)
            ];
        }

        return $this->render('check-user', [
            'resultData' => $resultData,
            'splits'     => $splits
        ]);
    }

    public function actionForceChangeUserSplit()
    {
        $resetDataFlag = \Yii::$app->request->post('resetAll');
        $userId        = \Yii::$app->request->post('userId');
        $splitGroup    = \Yii::$app->request->post('splitGroup');
        $forcedData    = \Yii::$container->get('splitSystem')->getAllForcedData();

        if ($userId && ($splitGroup !== '')) {
            $result = \Yii::$container->get('splitSystem')->forceUserSplitGroup($userId, (int) $splitGroup);
        }

        if ($resetDataFlag) {
            $result = \Yii::$container->get('splitSystem')->resetAllForcedData();
        }

        if (isset($result)) {
            if (!$result['status']) {
                \Yii::$app->session->setFlash('warning', $result['message'] ?? 'Something goes wrong');
            }
            return $this->redirect(['force-change-user-split']);
        }

        return $this->render('force-split', [
            'forcedData' => $forcedData
        ]);
    }

    /**
     * @param $recipientId
     * @param $senderId
     * @return string
     */
    public function actionViewMessages($recipientId, $senderId)
    {
        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'data' => $this->testerToolComponent->getHistoryMessages($recipientId, $senderId),
            ]);
        } else {
            return $this->render('view', [
                'data' => $this->testerToolComponent->getHistoryMessages($recipientId, $senderId),
            ]);
        }
    }

    public function getResponse(): array
    {
        return $this->response;
    }

    public function setResponse(array $value)
    {
        $this->response = $value;
    }

    public function setResponseParam($value, string $param)
    {
        if(!in_array($param, array_keys($this->response))) {
            throw new InvalidParamException('There is no ' . $param . ' param for this response');
        }

        $this->response[$param] = $value;
    }
}
