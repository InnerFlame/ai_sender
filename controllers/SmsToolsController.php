<?php

namespace app\controllers;

use app\components\common\SmsComponent;
use Yii;
use yii\base\Module;
use yii\web\Request;
use yii\web\Session;

/**
 * Class SmsToolsController
 * @package app\controllers
 * @property \yii\caching\Cache $memcache
 * @property Session $session
 * @property Request|array $post
 */
class SmsToolsController extends BaseController
{
    public $memcache, $session, $post;
    const DISABLE_ALERTING_TIME = 'smsAlertingDisable';

    /**
     * SmsToolsController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config = []);

        $this->memcache = Yii::$app->cache;
        $this->session  = Yii::$app->session;
        $this->post     = Yii::$app->request->post();
    }

    public function actionIndex()
    {
        if ($this->post) {
            if ($this->enableSmsAlerting()) {
                return $this->redirect(['index']);
            }
            if($dataToCache = $this->getSmsSilencePeriod()) {
                $cacheValue    = $dataToCache['value'];
                $cacheDuration = $dataToCache['duration'];

                $this->memcache->set(SmsComponent::MEMCACHE_SMS_ALERTING_KEY, $cacheValue, $cacheDuration);
                $this->memcache->set(self::DISABLE_ALERTING_TIME, time());
                $this->session->setFlash('success', 'Action were successful');
                return $this->redirect(['index']);
            }
            $this->session->setFlash('danger', 'Something goes wrong, please check all input data');
            return $this->redirect(['index']);
        }

        return $this->render('index', [
            'cachedData'         => $this->memcache->get(SmsComponent::MEMCACHE_SMS_ALERTING_KEY),
            'alertingDisabledAt' => $this->memcache->get(self::DISABLE_ALERTING_TIME)
        ]);
    }

    /**
     * @return bool
     */
    private function enableSmsAlerting()
    {
        if (isset($this->post['enableAlerting'])) {
            $this->memcache->delete(SmsComponent::MEMCACHE_SMS_ALERTING_KEY);
            $this->memcache->delete(self::DISABLE_ALERTING_TIME);

            $this->session->setFlash('success', 'Alerting were enabled');
            return true;
        }
        return false;
    }

    /**
     * @return array|bool
     */
    private function getSmsSilencePeriod()
    {
        if (isset($this->post['period'])) {
            $period = $this->post['period'];

            if (strpos($period, 'm') !== false) {
                $value = $period;
                $period = intval($period);
                if ($period > 360) {
                    return false;
                }
                return [
                    'value'     => $value,
                    'duration'  => $period * 60
                ];
            } elseif(strpos($period, 'h') !== false) {
                $value = $period;
                $period = intval($period);
                if ($period > 6) {
                    return false;
                }
                return [
                    'value'     => $value,
                    'duration'  => $period * 3600
                ];
            }

            return false;
        }

        return false;
    }
}
