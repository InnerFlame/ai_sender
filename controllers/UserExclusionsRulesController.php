<?php

namespace app\controllers;

use Yii;
use app\models\phoenix\UserExclusionsRules;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * UserExclusionsRulesController implements the CRUD actions for UserExclusionsRules model.
 */
class UserExclusionsRulesController extends Controller
{
    /**
     * Edit an existing UserExclusionsRules model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionEdit()
    {
        $model = $this->findModel(1);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['edit']);
        } else {
            return $this->render('edit', [
                'model'                 => $model,
                'genders'               => \Yii::$app->yaml->parse('gender'),
                'isPaid'                => $model->getIsPaidValues(),
                'searchable'            => $model->getSearchableValues(),
                'registrationPlatforms' => \Yii::$app->yaml->parse('recipientPlatforms'),
                'excludedSources'       => $model->getExcludedSources(),
                'orientations'          => $model->getOrientationsValues()
            ]);
        }
    }

    /**
     * Finds the UserExclusionsRules model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserExclusionsRules the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserExclusionsRules::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
