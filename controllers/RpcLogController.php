<?php

namespace app\controllers;

use app\models\phoenix\RpcErrorLog;
use app\models\rpc\RpcLog;
use Yii;
use app\models\rpc\RpcLogSearch;
use yii\base\ErrorException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * RpcLogController implements the CRUD actions for RpcLog model.
 */
class RpcLogController extends BaseController
{
    /**
     * Lists all RpcLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RpcLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionClean()
    {
        Yii::$app->db->createCommand()->truncateTable(RpcLog::tableName())->execute();
        $this->redirect('index');
    }

    public function actionView($id)
    {
        $model   = $this->findModel($id);
        $params = json_decode($model->params);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'params' => $params,
            ]);
        } else {
            return $this->render('view', [
                'params' => $params,
            ]);
        }
    }

    protected function findModel($id)
    {
        if (($model = RpcLog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRpcErrorLog()
    {
        if (\Yii::$app->request->isAjax) {
            try {
                $response['status'] = 'ok';
                \Yii::$app->response->format = Response::FORMAT_JSON;

                $response['data'] = \Yii::$app->rpcErrorLog->getRpcErrorLogStat(\Yii::$app->request->post());
            } catch (ErrorException $e) {
                $response['status'] = 'error';
                $response['message'] = $e->getMessage();
            }

            return $response;
        }

        return $this->render('rpc-error-log', [
            'errors' => RpcErrorLog::getErrorsType(),
        ]);
    }
}
