<?php

namespace app\controllers;


use app\models\phoenix\Email;
use app\models\phoenix\SenderEmail;
use yii\base\ErrorException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\validators\EmailValidator;

class EmailController extends BaseController
{
    public function actionIndex()
    {
        $model = new Email();
        if (\Yii::$app->request->isPost) {
            header('Content-Type: text/csv');
            header('Content-disposition: attachment;filename=blackEmails.csv');
            $outputBuffer     = fopen("php://output", 'w');
            $model->emailFile = UploadedFile::getInstance($model, 'emailFile');
            $whiteEmails      = [];
            $emails           = array_map('str_getcsv', file($model->emailFile->tempName));
            $emailValidator   = new EmailValidator();
            foreach ($emails as $email) {
                $email = explode(';', $email[0]);
                if (\Yii::$container->get('emailGenerator')->isValidEmail($email[0])
                    && $emailValidator->validate($email[0])
                ) {
                    $senderEmail = SenderEmail::findOne(['email' => $email[0]]);
                    list(, $domain) = explode('@', $email[0]);
                    if (empty($senderEmail)) {
                        $whiteEmails[] = [
                            'email'  => $email[0],
                            'domain' => $domain,
                        ];
                    }
                } else {
                    fputcsv($outputBuffer, $email);
                }

                if (count($whiteEmails) >= 100) {
                    \Yii::$app->db->createCommand()->batchInsert(SenderEmail::tableName(), ['email', 'domain'], $whiteEmails)->execute();
                    $whiteEmails = [];
                }
            }

            if (!empty($whiteEmails)) {
                \Yii::$app->db->createCommand()->batchInsert(SenderEmail::tableName(), ['email', 'domain'], $whiteEmails)->execute();
            }

            return;
        }

        return $this->render('index', [
            'model' => $model
        ]);
    }

    public function actionCheck()
    {
        if (\Yii::$app->request->isAjax) {
            try {
                $response['status']  = 'ok';
                $response['isValid'] = 'valid';

                \Yii::$app->response->format = Response::FORMAT_JSON;

                if (!\Yii::$container->get('emailGenerator')->isValidEmail(\Yii::$app->request->post('email'))) {
                    $response['isValid'] = 'invalid';
                }
            } catch (ErrorException $e) {
                $response['status'] = 'error';
                $response['message'] = $e->getMessage();
            }

            return $response;
        }
    }
}
