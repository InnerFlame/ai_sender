<?php

namespace app\controllers;

use app\models\phoenix\SenderProfile;
use Yii;
use app\models\phoenix\SenderProfileAttributes;
use app\models\phoenix\SenderProfileAttributesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SenderProfileAttributesController implements the CRUD actions for SenderProfileAttributes model.
 */
class SenderProfileAttributesController extends BaseController
{
    /**
     * Lists all ProfileSender models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SenderProfileAttributesSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'attributes' => \Yii::$app->yaml->parse('senderProfileAttributes'),
            'languages' => \Yii::$app->yaml->parseByKey('language', 'languages'),
        ]);
    }

    /**
     * Updates an existing Schema model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'attributes' => \Yii::$app->yaml->parse('senderProfileAttributes'),
            ]);
        }
    }

    protected function findModel($id)
    {
        if (($model = SenderProfileAttributes::findOne($id)) !== null) {
            return $model;
        } elseif (($senderModel = SenderProfile::findOne($id)) !== null) {
            $model = new SenderProfileAttributes();
            $model->id = $senderModel->id;
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
