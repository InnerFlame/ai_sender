<?php

namespace app\controllers;

use app\models\phoenix\PresenceEventLogSearch;
use Yii;

/**
 * PresenceEventLogController implements the CRUD actions for PresenceEventLog model.
 */
class PresenceEventLogController extends BaseController
{
    /**
     * Lists all PresenceEventLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PresenceEventLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionClean()
    {
        Yii::$app->db->createCommand()->truncateTable(PresenceEventLogSearch::tableName())->execute();
        $this->redirect('index');
    }
}
