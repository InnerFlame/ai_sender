<?php

namespace app\controllers;

use app\models\phoenix\SenderPhoto;
use app\models\phoenix\SenderPhotoSearch;
use yii\web\NotFoundHttpException;

/**
 * SenderPhotoController implements the CRUD actions for SenderPhoto model.
 */
class SenderPhotoController extends BaseController
{
    /**
     * Lists all SenderPhoto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SenderPhotoSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionReusedPhoto($id)
    {
        $senderPhoto = $this->findModel($id);

        if (!\Yii::$app->senderPhoto->reusedPhoto($senderPhoto)) {
            \Yii::$app->session->setFlash('reusedError', 'Sorry, an error has occurred');
        }

        return $this->redirect(\Yii::$app->request->referrer);
    }

    protected function findModel($id)
    {
        if (($model = SenderPhoto::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
