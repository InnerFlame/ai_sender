<?php

namespace app\controllers;

use Yii;
use app\models\phoenix\Schema;
use app\models\phoenix\SchemaSearch;
use app\models\phoenix\GroupSearch;
use yii\web\NotFoundHttpException;

/**
 * SchemaController implements the CRUD actions for Schema model.
 */
class SchemaController extends BaseController
{
    /**
     * Lists all Schema models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new SchemaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'gender'       => Yii::$app->yaml->parse('gender'),
            'sexuality'    => Yii::$app->yaml->parse('sexuality'),
        ]);
    }

    /**
     * Displays a single Schema model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $queryParams = array_merge(['GroupSearch' => ['schemaId' => $id]], Yii::$app->request->getQueryParams());

        $searchModel  = new GroupSearch();
        $dataProvider = $searchModel->search($queryParams);
        $model        = $this->findModel($id);

        return $this->render('view', [
            'model'        => $model,
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'isPaid'       => $model->isPaid == 1 ? 'Yes' : 'No',
            'gender'       => Yii::$app->yaml->parseByKey('gender', $model->gender),
            'sexuality'    => Yii::$app->yaml->parseByKey('sexuality', $model->sexuality),
            'sendMode'     => Yii::$app->yaml->parse('sendMode'),
        ]);
    }

    /**
     * Creates a new Schema model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Schema();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model'     => $model,
                'gender'    => Yii::$app->yaml->parse('gender'),
                'sexuality' => Yii::$app->yaml->parse('sexuality'),
            ]);
        }
    }

    /**
     * Updates an existing Schema model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model'     => $model,
                'gender'    => Yii::$app->yaml->parse('gender'),
                'sexuality' => Yii::$app->yaml->parse('sexuality'),
            ]);
        }
    }

    /**
     * Deletes an existing Schema model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Schema model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Schema the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Schema::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
