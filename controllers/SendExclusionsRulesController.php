<?php

namespace app\controllers;

use app\models\phoenix\Schema;
use Yii;
use app\models\phoenix\SendExclusionsRules;
use app\models\phoenix\SendExclusionsRulesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SendExclusionsRulesController implements the CRUD actions for SendExclusionsRules model.
 */
class SendExclusionsRulesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SendExclusionsRules models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SendExclusionsRulesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'       => $searchModel,
            'dataProvider'      => $dataProvider,
            'countries'         => \Yii::$app->yaml->parse('country'),
            'sites'             => $this->getSitesDropDownData(),
            'schemaIds'         => $this->getSchemasDropDownData(),
            'platforms'         => $this->getPlatformsDropDownData(),
        ]);
    }

    /**
     * Displays a single SendExclusionsRules model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SendExclusionsRules model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SendExclusionsRules();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'countries'         => \Yii::$app->yaml->parse('country'),
                'sites'             => $this->getSitesDropDownData(),
                'sources'           => $model->getSourcesValues(),
                'schemaIds'         => $this->getSchemasDropDownData(),
                'platforms'         => $this->getPlatformsDropDownData(),
            ]);
        }
    }

    /**
     * Updates an existing SendExclusionsRules model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'countries'         => \Yii::$app->yaml->parse('country'),
                'sites'             => $this->getSitesDropDownData(),
                'sources'           => $model->getSourcesValues(),
                'schemaIds'         => $this->getSchemasDropDownData(),
                'platforms'         => $this->getPlatformsDropDownData(),
            ]);
        }
    }

    /**
     * Deletes an existing SendExclusionsRules model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SendExclusionsRules model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SendExclusionsRules the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SendExclusionsRules::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Get data for index and crud dropDowns
     *
     * @return array
     */
    private function getSitesDropDownData(): array
    {
        $out   = [];
        $sites = \Yii::$app->yaml->parse('site');

        foreach ($sites as $site) {
            $out[$site['hash']] = $site['name'];
        }

        return $out;
    }

    private function getSchemasDropDownData(): array
    {
        $out     = [];
        $schemas = Schema::find()->all();
        foreach ($schemas as $schema) {
            $out[$schema->id] = $schema->title;
        }

        return $out;
    }

    private function getPlatformsDropDownData(): array
    {
        return [
            'webSite'    => 'webSite',
            'mobSite'    => 'mobSite',
            'androidApp' => 'androidApp',
            'iosApp'     => 'iosApp',
        ];
    }
}
