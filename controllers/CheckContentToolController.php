<?php

namespace app\controllers;

use app\models\common\User;
use app\models\phoenix\IncorrectMessage;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;

class CheckContentToolController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::SUPER_ADMIN, User::ADMIN, User::CONTENT_MANAGER],
                    ]
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->redirect(['phrases']);
    }

    public function actionPhrases()
    {
        return $this->getViewData('phrase');
    }

    public function actionTemplates()
    {
        return $this->getViewData('template');
    }

    public function actionInitials()
    {
        return $this->getViewData('initial');
    }

    private function getViewData($type)
    {
        return $this->render(
            'index',
            [
                'dataProvider' => new ActiveDataProvider([
                    'query' => IncorrectMessage::find()->where(['type' => $type]),
                    'sort' => false,
                    'pagination' => [
                        'pageSize' => 25,
                    ],
                ]),
            ]
        );
    }
}
