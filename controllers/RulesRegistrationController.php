<?php

namespace app\controllers;

use sender\registration\models\RulesRegistration;
use sender\registration\models\RulesRegistrationSearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * RulesProfileSenderRegistrationController implements the CRUD actions for RulesRegistration model.
 */
class RulesRegistrationController extends BaseController
{
    /**
     * Lists all RulesProfileSenderRegistration models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RulesRegistrationSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'country'      => \Yii::$app->yaml->parse('country'),
            'niche'        => \Yii::$container->get('nicheQuery')->getNiches(),
            'language'     => \Yii::$app->yaml->parseByKey('language', 'languages'),
        ]);
    }

    /**
     * Creates a new RulesProfileSenderRegistration model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RulesRegistration();
        if ($model->load(\Yii::$app->request->post()) && $model->validateGeneratedProfilesAgeByNiche()) {
            if($model->validateGeneratedProfilesAgeByNiche()) {
                $model->save();
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('create', [
                'model'       => $model,
                'country'     => \Yii::$app->yaml->parse('country'),
                'sexuality'   => \Yii::$app->yaml->parse('sexuality'),
                'gender'      => \Yii::$app->yaml->parse('gender'),
                'niche'       => \Yii::$container->get('nicheQuery')->getNiches(),
                'language'    => \Yii::$app->yaml->parseByKey('language', 'languages'),
            ]);
        }
    }

    /**
     * Updates an existing RulesProfileSenderRegistration model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->validateGeneratedProfilesAgeByNiche()) {
            if($model->validateGeneratedProfilesAgeByNiche()) {
                $model->save();
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('update', [
                'model'       => $model,
                'country'     => \Yii::$app->yaml->parse('country'),
                'sexuality'   => \Yii::$app->yaml->parse('sexuality'),
                'gender'      => \Yii::$app->yaml->parse('gender'),
                'niche'       => \Yii::$container->get('nicheQuery')->getNiches(),
                'language'    => \Yii::$app->yaml->parseByKey('language', 'languages'),
            ]);
        }
    }

    /**
     * Deletes an existing RulesProfileSenderRegistration model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RulesProfileSenderRegistration model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RulesRegistration the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RulesRegistration::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
