<?php

namespace app\controllers;

use app\models\phoenix\Group;
use app\models\phoenix\GroupSearch;
use yii\web\NotFoundHttpException;

/**
 * GroupController implements the CRUD actions for Group model.
 */
class GroupController extends BaseController
{
    /**
     * Lists all Group models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GroupSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'sendMode'     => \Yii::$app->yaml->parse('sendMode'),
        ]);
    }

    /**
     * Creates a new Group model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Group();

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model'             => $model,
            'sendMode'          => \Yii::$app->yaml->parse('sendMode'),
            'typesSendActivity' => $this->getTypesSendActivity(),
        ]);
    }

    /**
     * Updates an existing Group model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model'             => $model,
            'sendMode'          => \Yii::$app->yaml->parse('sendMode'),
            'typesSendActivity' => $this->getTypesSendActivity(),
        ]);
    }

    /**
     * Deletes an existing Group model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * Finds the Group model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Group the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Group::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function getTypesSendActivity()
    {
        return [
            Group::TYPE_SEND_ACTIVITY_ALL          => Group::TYPE_SEND_ACTIVITY_ALL,
            Group::TYPE_SEND_ACTIVITY_RESTART      => Group::TYPE_SEND_ACTIVITY_RESTART,
            Group::TYPE_SEND_ACTIVITY_ZERO_RESTART => Group::TYPE_SEND_ACTIVITY_ZERO_RESTART,
        ];
    }

}
