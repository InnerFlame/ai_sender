<?php

namespace app\controllers;

use app\components\phoenix\checkMessage\AnalyzerData;
use app\models\phoenix\RecipientShortId;
use Yii;
use app\models\phoenix\RecipientProfileSearch;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * RecipientProfileController implements the CRUD actions for RecipientProfile model.
 */
class RecipientProfileController extends BaseController
{
    /**
     * Lists all RecipientProfile models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RecipientProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'gender'       => Yii::$app->yaml->parse('gender'),
            'sexuality'    => Yii::$app->yaml->parse('sexuality'),
            'site'         => ArrayHelper::getColumn(\Yii::$app->yaml->parse('site'), 'name'),
            'country'      => Yii::$app->yaml->parse('country'),
            'niches'       => $this->getNichesForIndexTable()
        ]);
    }

    /**
     * @return array
     */
    private function getNichesForIndexTable()
    {
        $out = [];
        $niches = Yii::$app->yaml->parse('recipientNiches');

        foreach ($niches as $nicheName => $data) {
            $out[$nicheName] = $nicheName;
        }

        $out[RecipientProfileSearch::UNDEFINED_GENERAL_NICHE] = 'Undefined/General';

        return $out;
    }

    public function actionExportShortId()
    {
        if (!$post = \Yii::$app->request->post()) {
            return $this->render('export-short-id', [
                'sites'     => ArrayHelper::getColumn(\Yii::$app->yaml->parse('site'), 'name'),
                'countries' => Yii::$app->yaml->parse('country'),
            ]);
        }

        $sites     = $post['site']    ?? [];
        $countries = $post['country'] ?? [];
        $dateFrom  = !empty($post['dateFrom']) ? $post['dateFrom'] . ' 00:00:00' : '';
        $dateTo    = !empty($post['dateTo'])   ? $post['dateTo']   . ' 23:59:59' : '';

        $query = (new Query())
            ->select('s.remoteId, s.shortId')
            ->from(RecipientShortId::tableName() . ' as s')
        ;

        if (!empty($sites)) {
            $query->andWhere(['s.site' => $sites]);
        }

        if (!empty($countries)) {
            $query->andWhere(['s.country' => $countries]);
        }

        if (!empty($dateFrom)) {
            if (!empty($dateTo)) {
                $query->where(['between', 's.createdAt', $dateFrom, $dateTo]);
            } else {
                $query->where(['>=', 's.createdAt', $dateFrom]);
            }
        }

        if (!$query->count()) {
            Yii::$app->session->setFlash('warning', 'No data to export.');
            return $this->redirect(['export-short-id']);
        }

        header('Content-Type: text/csv');
        header('Content-disposition: attachment;filename=export.csv');

        $outputBuffer = fopen("php://output", 'w');
        foreach ($query->batch(500) as $recipients) {
            foreach ($recipients as $recipient) {
                $data = [
                    'remoteId' => $recipient['remoteId'],
                    'shortID'  => $recipient['shortId'],
                ];
                fputcsv($outputBuffer, $data);
            }
        }

        return;
    }

    public function actionCleanScamWordsCache(): Response
    {
        if (!Yii::$app->cache->get(AnalyzerData::SCAM_WORDS_FILE_DATA_CACHE_KEY)) {
            Yii::$app->session->setFlash('warning', 'Nothing to clean!');
            return $this->redirect(['export-short-id']);
        }

        if (Yii::$app->cache->delete(AnalyzerData::SCAM_WORDS_FILE_DATA_CACHE_KEY)) {
            Yii::$app->session->setFlash('success', 'Data were cleaned successfully!');
            return $this->redirect(['export-short-id']);
        }

        Yii::$app->session->setFlash('danger', 'Got error while cleaning cached data!');
        return $this->redirect(['export-short-id']);
    }
}
