<?php

namespace app\controllers;

use yii\base\ErrorException;
use yii\web\Response;
use yii\data\ArrayDataProvider;
use app\models\rpc\Container;
use app\models\rpc\Client;
use yii\helpers\ArrayHelper;

/**
 * ContainerController implements the CRUD actions for Container model.
 */
class ContainerClientController extends BaseController
{
    public function actionContainer()
    {
        $request      = \Yii::$app->request->get();
        $dataProvider = new ArrayDataProvider([
            'allModels'  => Container::getContainers($request),
            'sort'       => [
                'attributes'   => ['status', 'type', 'country', 'site', 'lastTimeActivity', 'timeSetStatus'],
                'defaultOrder' => [
                    'status' => SORT_ASC,
                ],
            ],
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        $containersCountForSections = Container::getStartedCountForSections();

        return $this->render('container', [
            'dataProvider'               => $dataProvider,
            'containersCountForSections' => $containersCountForSections,
            'country'                    => \Yii::$app->yaml->parse('country'),
            'request'                    => $request,
            'site'                       => ArrayHelper::getColumn(\Yii::$app->yaml->parse('site'), 'name'),
            'types'                      => Container::$types,
            'statuses'                   => Container::$statuses,
        ]);
    }

    public function actionClient()
    {
        $request      = \Yii::$app->request->get();
        $dataProvider = new ArrayDataProvider([
            'allModels'  => Client::getClients($request),
            'sort'       => [
                'attributes'   => ['status', 'type', 'country', 'site', 'timeSetStatus'],
                'defaultOrder' => [
                    'status' => SORT_ASC,
                ],
            ],
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        $startedCount = Client::find()->where(['status' => Client::STATUS_STARTED])->count();
        $loginCount   = Client::find()->where(['status' => Client::STATUS_LOGIN])->count();

        return $this->render('client', [
            'dataProvider' => $dataProvider,
            'startedCount' => $startedCount,
            'loginCount'   => $loginCount,
            'country'      => \Yii::$app->yaml->parse('country'),
            'request'      => $request,
            'site'         => ArrayHelper::getColumn(\Yii::$app->yaml->parse('site'), 'name'),
            'types'        => Client::$types,
            'statuses'     => Client::$statuses,
        ]);
    }

    public function actionCleanStoppedContainer()
    {
        Container::deleteAll(['status' => Container::STATUS_STOPPED]);
        $this->redirect('container');
    }

    public function actionCleanStoppedClient()
    {
        Client::deleteAll(['status' => Client::STATUS_STOPPED]);
        $this->redirect('client');
    }

    public function actionStopAll()
    {
        try {
            $response['status'] = 'ok';

            \Yii::$app->response->format = Response::FORMAT_JSON;

            $containers = Container::find()
                ->where(['status' => Container::STATUS_STARTED])
                ->all();

            foreach ($containers as $container) {
                \Yii::$app->rpcClient->stopRpcContainer($container->name, $container->target);
            }

            Container::deleteAll();
            Client::deleteAll();
        } catch (ErrorException $e) {
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    public function actionStopOne($id)
    {
        $container = Container::findOne($id);

        if ($container->status == Container::STATUS_STARTED) {
            \Yii::$app->rpcClient->stopRpcContainer($container->name, $container->target);
        }

        Client::deleteAll(['name' => $container->name]);
        $container->delete();

        $this->redirect('container');
    }
}