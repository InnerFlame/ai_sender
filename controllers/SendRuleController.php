<?php

namespace app\controllers;

use app\models\phoenix\SendRule;
use app\models\phoenix\SendRuleSearch;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * SendRuleController implements the CRUD actions for SendRule model.
 */
class SendRuleController extends BaseController
{
    /**
     * Lists all SendRule models.
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel  = new SendRuleSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'schema'       => $this->getAvailableSchemas(),
            'site'         => $this->getAvailableSites(),
            'projects'     => $this->getAvailableProjects(),
            'country'      => $this->getAvailableCountries(),
            'traffSources' => $this->getAvailableTraffSources()
        ]);
    }

    /**
     * Creates a new SendRule model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     *
     * @return string|Response
     */
    public function actionCreate()
    {
        if ($postData = \Yii::$app->request->post('SendRule'))
        {
            $validatedData  = null;
            $isActive       = (bool) $postData['isActive']   ?? false;
            $schemas        = $postData['schemaId']          ?? [];
            $sites          = $postData['site']              ?? [];
            $postProjects   = $postData['project']           ?? [];
            $countries      = $postData['country']           ?? [];
            $traffSources   = $postData['traffSourcesArray'] ?? [];

            $validatedData = $this->validateRule(
                $schemas, $countries, $sites, $postProjects, $isActive, $traffSources
            );

            if (!empty($validatedData)) {
                /** @var $validatedData SendRule[] */
                foreach ($validatedData as $model) {
                    $model->save();
                }

                return $this->redirect(['index']);
            }

            \Yii::$app->session->setFlash('danger', 'Cant create Apply Schema!');
        }

        return $this->render('create', [
            'model'        => new SendRule(),
            'schema'       => $this->getAvailableSchemas(),
            'site'         => $this->getAvailableSites(),
            'projects'     => $this->getAvailableProjects(),
            'country'      => $this->getAvailableCountries(),
            'traffSources' => $this->getAvailableTraffSources()
        ]);
    }

    /**
     * Validate project rule before save and returns array of validated models or null
     *
     * @param $schemas
     * @param $countries
     * @param $sites array | null
     * @param $projects array | null
     * @param $isActive
     * @param $traffSources
     * @return array
     */
    private function validateRule(
        $schemas,
        $countries,
        $sites,
        $projects,
        $isActive,
        $traffSources
    ): array
    {
        $modelsToSave = [];
        $items        = !empty($sites) ? $sites : (!empty($projects) ? $projects : []);

        if (empty($items)) {
            \Yii::$app->session->setFlash('warning', 'You must specify site either project!');
            return $modelsToSave;
        }

        foreach ($schemas as $itemSchema) {
            foreach ($items as $itemSite) {
                foreach ($countries as $itemCountry) {
                    $model = new SendRule([
                        'isActive' => $isActive,
                        'country'  => empty($itemCountry) ? null : $itemCountry,
                        'schemaId' => empty($itemSchema)  ? null : $itemSchema,
                        'site'     => empty($sites)       ? null : $itemSite,
                        'project'  => empty($projects)    ? null : $itemSite
                    ]);

                    $model->traffSourcesArray  = empty($traffSources) ? [] : $traffSources;

                    if (!$model->validate()) {
                        return [];
                    }

                    $modelsToSave[] = $model;
                }
            }
        }
        


        return $modelsToSave;
    }

    /**
     * Updates an existing SendRule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($postData = \Yii::$app->request->post('SendRule')) {
            $model->isActive          = $postData['isActive']    ?? false;
            $model->schemaId          = $postData['schemaId']    ?? null;
            $model->site              = $postData['site']        ?? null;
            $model->project           = $postData['project']     ?? null;
            $model->country           = $postData['country']     ?? null;
            $model->traffSourcesArray = $postData['traffSourcesArray'] ?? null;

            if ($model->site && $model->project) {
                \Yii::$app->session->setFlash('warning', 'Please choose project OR site!');

                return $this->render('update', [
                    'model'        => $model,
                    'schema'       => $this->getAvailableSchemas(),
                    'site'         => $this->getAvailableSites(),
                    'projects'     => $this->getAvailableProjects(),
                    'country'      => $this->getAvailableCountries(),
                    'traffSources' => $this->getAvailableTraffSources()
                ]);
            }

            if (!$model->save()) {
                \Yii::$app->session->setFlash('danger', 'Error while saving Apply Schema');
            }

            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model'        => $model,
            'schema'       => $this->getAvailableSchemas(),
            'site'         => $this->getAvailableSites(),
            'projects'     => $this->getAvailableProjects(),
            'country'      => $this->getAvailableCountries(),
            'traffSources' => $this->getAvailableTraffSources()
        ]);
    }

    /**
     * Deletes an existing SendRule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SendRule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SendRule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SendRule::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
