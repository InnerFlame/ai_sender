<?php

namespace app\controllers;

use yii\base\ErrorException;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\models\common\User;

class CheckMessageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::SUPER_ADMIN, User::ADMIN, User::CONTENT_MANAGER],
                    ]
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if (\Yii::$app->request->isAjax) {
            try {
                $response['status'] = 'ok';
                \Yii::$app->response->format = Response::FORMAT_JSON;

                $response['data'] = \Yii::$app->checkMessage->checkMessageForTool(\Yii::$app->request->post('message'));
            } catch (ErrorException $e) {
                $response['status'] = 'error';
                $response['message'] = $e->getMessage();
            }

            return $response;
        }

        return $this->render('index');
    }
}
