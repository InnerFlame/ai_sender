<?php

namespace app\controllers;

use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use yii\web\Response;

class StatController extends BaseController
{
    public function actionStatCommunication()
    {
        if (\Yii::$app->request->isAjax) {
            try {
                $response['status'] = 'ok';

                \Yii::$app->response->format = Response::FORMAT_JSON;

                $response['data'] = \Yii::$app->statCommunication->getStat(\Yii::$app->request->post());
            } catch (ErrorException $e) {
                $response['status'] = 'error';
                $response['message'] = $e->getMessage();
            }

            return $response;
        }

        return $this->render('stat-communication', [
            'sites'     => ArrayHelper::getColumn(\Yii::$app->yaml->parse('site'), 'name'),
            'countries' => \Yii::$app->yaml->parse('country'),
            'genders'   => \Yii::$app->yaml->parse('gender'),
            'sendMode'  => \Yii::$app->yaml->parse('sendMode'),
        ]);
    }

    public function actionDailyStatsAllLocations()
    {
        if (\Yii::$app->request->isAjax) {
            try {
                $response['status'] = 'ok';

                \Yii::$app->response->format = Response::FORMAT_JSON;

                $response['data'] = \Yii::$app->statCommunication->getStatGroupAllLocation(\Yii::$app->request->post());
            } catch (ErrorException $e) {
                $response['status'] = 'error';
                $response['message'] = $e->getMessage();
            }

            return $response;
        }

        return $this->render('dayly-stats-all-locations');
    }

    public function actionCoverageFatal()
    {
        if (\Yii::$app->request->isAjax) {
            try {
                $response['status'] = 'ok';
                \Yii::$app->response->format = Response::FORMAT_JSON;

                $response['data'] = \Yii::$app->unprocessedRecipientStats
                    ->getCoverageFatal(\Yii::$app->request->post());
            } catch (ErrorException $e) {
                $response['status'] = 'error';
                $response['message'] = $e->getMessage();
            }

            return $response;
        }

        return $this->render('coverage-fatal', [
            'countries' => \Yii::$app->yaml->parse('country'),
        ]);
    }

    public function actionDelayStat()
    {
        if (\Yii::$app->request->isAjax) {
            try {
                $response['status'] = 'ok';
                \Yii::$app->response->format = Response::FORMAT_JSON;

                $response['data'] = \Yii::$app->delayStat->getDelayStat(\Yii::$app->request->post());
            } catch (ErrorException $e) {
                $response['status'] = 'error';
                $response['message'] = $e->getMessage();
            }

            return $response;
        }

        return $this->render('delay-stat');
    }

    public function actionSenderBanStat()
    {
        if (\Yii::$app->request->isAjax) {
            try {
                $response['status'] = 'ok';
                \Yii::$app->response->format = Response::FORMAT_JSON;

                $response['data'] = \Yii::$app->senderBanStat->getSenderBanStat(\Yii::$app->request->post());
            } catch (ErrorException $e) {
                $response['status'] = 'error';
                $response['message'] = $e->getMessage();
            }

            return $response;
        }

        return $this->render('sender-ban-stat', [
            'countries' => \Yii::$app->yaml->parse('country'),
        ]);
    }

    public function actionRegistrationStat()
    {
        if (\Yii::$app->request->isAjax) {
            try {
                $response['status'] = 'ok';
                \Yii::$app->response->format = Response::FORMAT_JSON;

                $response['data'] = \Yii::$app->registrationStat->getStat(\Yii::$app->request->post());
            } catch (ErrorException $e) {
                $response['status']  = 'error';
                $response['message'] = $e->getMessage();
            }

            return $response;
        }

        return $this->render('registration-stat', [
            'countries' => \Yii::$app->yaml->parse('country'),
        ]);
    }

    public function actionResendingStat()
    {
        if (\Yii::$app->request->isAjax) {
            try {
                $response['status'] = 'ok';
                \Yii::$app->response->format = Response::FORMAT_JSON;

                $response['data'] = \Yii::$app->resendingStat->getStat(\Yii::$app->request->post());
            } catch (ErrorException $e) {
                $response['status']  = 'error';
                $response['message'] = $e->getMessage();
            }

            return $response;
        }

        return $this->render('resending-stat', [
            'countries' => \Yii::$app->yaml->parse('country'),
        ]);
    }
}
