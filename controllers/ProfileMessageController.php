<?php

namespace app\controllers;

use app\models\phoenix\ProfileMessage;
use app\models\phoenix\ProfileMessagesSearch;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ProfileMessageController implements the CRUD actions for ProfileMessage model.
 */
class ProfileMessageController extends BaseController
{
    /**
     * Lists all ProfileMessage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProfileMessagesSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'site'         => ArrayHelper::getColumn(\Yii::$app->yaml->parse('site'), 'name'),
            'country'      => \Yii::$app->yaml->parse('country'),
            'sexuality'    => \Yii::$app->yaml->parse('sexuality'),
            'gender'       => \Yii::$app->yaml->parse('gender'),
            'projects'     => \Yii::$app->yaml->parse('phoenixProject'),
            'language'     => \Yii::$app->yaml->parseByKey('language', 'languages'),
            'sendMode'     => \Yii::$app->yaml->parse('sendMode'),
        ]);
    }

    /**
     * Displays a single ProfileMessage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model'       => $model,
            'sendMode'    => \Yii::$app->yaml->parseByKey('sendMode', $model->sendMode),
            'country'     => \Yii::$app->yaml->parseByKey('country', $model->country),
            'gender'      => \Yii::$app->yaml->parseByKey('gender', $model->gender),
            'sexuality'   => \Yii::$app->yaml->parseByKey('sexuality', $model->sexuality),
            'project'     => \Yii::$app->yaml->parseByKey('phoenixProject', $model->project),
            'isPaid'      => $model->isPaid ? 'Yes' : 'No',
            'isActive'    => $model->isActive ? 'Yes' : 'No',
        ]);
    }

    /**
     * Creates a new ProfileMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProfileMessage();

        if ($post = \Yii::$app->request->post()) {
            $postData = $post['ProfileMessage'];
            $messages = explode("\n", $postData['body']);

            foreach ($messages as $message) {
                $model = new ProfileMessage();

                $model->subject   = $postData['subject'];
                $model->body      = $message;
                $model->language  = $postData['language'];
                $model->sendMode  = $postData['sendMode'];
                $model->country   = $postData['country'];
                $model->gender    = $postData['gender'];
                $model->sexuality = $postData['sexuality'];
                $model->project   = $postData['project'];
                $model->isPaid    = $postData['isPaid'];
                $model->isActive  = $postData['isActive'];

                $model->save();
            }

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model'       => $model,
                'gender'      => \Yii::$app->yaml->parse('gender'),
                'projects'    => \Yii::$app->yaml->parse('phoenixProject'),
                'sexuality'   => \Yii::$app->yaml->parse('sexuality'),
                'country'     => \Yii::$app->yaml->parse('country'),
                'language'    => \Yii::$app->yaml->parseByKey('language', 'languages'),
                'sendMode'    => \Yii::$app->yaml->parse('sendMode'),

            ]);
        }
    }

    /**
     * Updates an existing ProfileMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $gender = \Yii::$app->yaml->parse('gender');
            $sexuality = \Yii::$app->yaml->parse('sexuality');
            $projects  = \Yii::$app->yaml->parse('phoenixProject');
            $language  = \Yii::$app->yaml->parseByKey('language', 'languages');
            $country   = \Yii::$app->yaml->parse('country');
            $sendMode  = \Yii::$app->yaml->parse('sendMode');
            return $this->render('update', [
                'model'       => $model,
                'gender'      => $gender,
                'projects'    => $projects,
                'sexuality'   => $sexuality,
                'country'     => $country,
                'language'    => $language,
                'sendMode'    => $sendMode,
            ]);
        }
    }

    /**
     * Deletes an existing ProfileMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProfileMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProfileMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProfileMessage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionMultipleDelete()
    {
        if (\Yii::$app->request->isAjax) {
            try {
                $response['status'] = 'ok';

                \Yii::$app->response->format = Response::FORMAT_JSON;

                $post = \Yii::$app->request->post();

                ProfileMessage::deleteAll(['id' => $post['ids']]);
            } catch (ErrorException $e) {
                $response['status'] = 'error';
                $response['message'] = $e->getMessage();
            }

            return $response;
        }

        return false;
    }

    public function actionMultipleEdit()
    {
        if ($post = \Yii::$app->request->post()) {
            foreach ($post['body'] as $key => $message) {
                $model = $this->findModel($key);
                $model->body = $message;
                $model->save();
            }

            return $this->redirect(['index']);
        }

        $post = \Yii::$app->request->get();

        $messages = ProfileMessage::findAll(['id' => $post['selection']]);

        return $this->render('multiple-edit', [
            'messages' => $messages,
        ]);
    }

    public function actionMultipleUpdateStatus()
    {
        if (\Yii::$app->request->isAjax) {
            try {
                $response['status'] = 'ok';

                \Yii::$app->response->format = Response::FORMAT_JSON;

                $post = \Yii::$app->request->post();

                \Yii::$app->db->createCommand()
                    ->update(ProfileMessage::tableName(), ['isActive' => $post['status']], 'id IN (' . implode(',', $post['ids']) . ')')
                    ->execute();
            } catch (ErrorException $e) {
                $response['status'] = 'error';
                $response['message'] = $e->getMessage();
            }

            return $response;
        }

        return false;
    }
}
