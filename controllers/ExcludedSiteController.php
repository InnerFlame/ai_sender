<?php

namespace app\controllers;

use Yii;
use app\models\phoenix\ExcludedSite;
use app\models\phoenix\ExcludedSiteSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ExcludedSiteController implements the CRUD actions for ExcludedSite model.
 */
class ExcludedSiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ExcludedSite models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new ExcludedSiteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'site'         => ArrayHelper::getColumn(\Yii::$app->yaml->parse('site'), 'name'),
            'country'      => \Yii::$app->yaml->parse('country')
        ]);
    }

    /**
     * Creates a new ExcludedSite model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ExcludedSite();
        $post  = \Yii::$app->request->post();
        if ($post) {
            $save      = true;
            $sites     = !empty($post['ExcludedSite']['site']) ? $post['ExcludedSite']['site'] : [null];
            $countries = !empty($post['ExcludedSite']['country']) ? $post['ExcludedSite']['country'] : [];

            foreach ($sites as $itemSite) {
                foreach ($countries as $itemCountry) {
                    $data['ExcludedSite']['country']  = $itemCountry;
                    $data['ExcludedSite']['site']     = $itemSite;
                    $data['ExcludedSite']['isActive'] = $post['ExcludedSite']['isActive'];

                    $model = new ExcludedSite();
                    $model->load($data);
                    $modelSave[] = $model;

                    if (!$model->validate()) {
                        $save = false;
                        break;
                    }
                }
            }

            if ($save) {
                foreach ($modelSave as $item) {
                    $item->save();
                }

                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model'   => $model,
            'site'    => ArrayHelper::getColumn(\Yii::$app->yaml->parse('site'), 'name'),
            'country' => \Yii::$app->yaml->parse('country'),
        ]);
    }

    /**
     * Updates an existing ExcludedSite model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model'   => $model,
                'site'    => ArrayHelper::getColumn(\Yii::$app->yaml->parse('site'), 'name'),
                'country' => \Yii::$app->yaml->parse('country')
            ]);
        }
    }

    /**
     * Deletes an existing ExcludedSite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ExcludedSite model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ExcludedSite the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ExcludedSite::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
