<?php

namespace app\controllers;

use Yii;
use sender\split\models\Split;
use yii\web\NotFoundHttpException;
use sender\split\models\SplitSearch;
use yii\web\Response;

/**
 * SplitController implements the CRUD actions for Split model.
 */
class SplitController extends BaseController
{
    /**
     * Lists all Split models.
     *
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel  = new SplitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single Split model.
     *
     * @param integer $id
     * @return string
     */
    public function actionView($id): string
    {
        return $this->render('view', [
            'model'       => $this->findModel($id),
            'genders'     => \Yii::$app->yaml->parse('gender'),
            'sexualities' => \Yii::$app->yaml->parse('sexuality'),
            'sites'       => $this->getSitesDropDownData()
        ]);
    }

    /**
     * Creates a new Split model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = new Split();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model'                 => $model,
            'countries'             => \Yii::$app->yaml->parse('country'),
            'genders'               => \Yii::$app->yaml->parse('gender'),
            'registrationPlatforms' => \Yii::$app->yaml->parse('recipientPlatforms'),
            'sexualities'           => \Yii::$app->yaml->parse('sexuality'),
            'sources'               => $this->getSourcesDropDownData(),
            'sites'                 => $this->getSitesDropDownData()
        ]);
    }

    /**
     * Updates an existing Split model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model'                 => $model,
            'countries'             => \Yii::$app->yaml->parse('country'),
            'genders'               => \Yii::$app->yaml->parse('gender'),
            'registrationPlatforms' => \Yii::$app->yaml->parse('recipientPlatforms'),
            'sexualities'           => \Yii::$app->yaml->parse('sexuality'),
            'sources'               => $this->getSourcesDropDownData(),
            'sites'                 => $this->getSitesDropDownData()
        ]);

    }

    /**
     * Deletes an existing Split model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return Response
     */
    public function actionDelete($id): Response
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Export recipientIds in csv file by split setting
     *
     * @return Response | string
     */
    public function actionExport()
    {
        $post = Yii::$app->request->post();

        if (!empty($post['splitId'])) {
            $group        = isset($post['group']) ? (int) $post['group'] : null;
            $splitId      = $post['splitId'];
            $recipientIds = \Yii::$container->get('splitSystem')->getRecipientsIdsForExportBySplit($splitId, $group);

            if (!$recipientIds) {
                Yii::$app->session->setFlash('warning', 'No recipients by split setting');
                return $this->redirect('export');
            }

            header('Content-Type: text/csv');
            header('Content-disposition: attachment;filename=export.csv');
            $outputBuffer = fopen("php://output", 'w');

            foreach ($recipientIds as $systemId => $remoteId) {
                $data = ['remoteId' => $remoteId, 'systemId' => $systemId];
                fputcsv($outputBuffer, $data);
            }

            return;
        }

        return $this->render('export', [
            'splits' => $this->getSplitsForExportDropDown()
        ]);
    }

    /**
     * Finds the Split model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Split the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Split::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Get data for index and crud dropDowns
     *
     * @return array
     */
    private function getSitesDropDownData(): array
    {
        $out   = [];
        $sites = \Yii::$app->yaml->parse('site');

        foreach ($sites as $siteId => $site) {
            $out[$siteId] = $site['name'];
        }

        return $out;
    }

    /**
     * Get data for index and crud dropDowns
     *
     * @return array
     */
    private function getSourcesDropDownData(): array
    {
        $sources = \Yii::$app->yaml->parse('sources');

        return array_combine($sources, $sources);
    }

    /**
     * Get data for export select
     *
     * @return array
     */
    private function getSplitsForExportDropDown(): array
    {
        $dropDownData = [];
        $splitModels = Split::find()->all();

        /* @var $splitModels Split[] */
        foreach ($splitModels as $splitModel) {
            $dropDownData[$splitModel->id] = $splitModel->name . ' (' . $splitModel->percentage . ')';
        }

        return $dropDownData;
    }
}
