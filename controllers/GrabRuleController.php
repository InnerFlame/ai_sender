<?php

namespace app\controllers;

use app\models\phoenix\GrabRule;
use app\models\phoenix\GrabRuleSearch;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * GrabRuleController implements the CRUD actions for GrabRule model.
 */
class GrabRuleController extends BaseController
{
    /**
     * Lists all GrabRule models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GrabRuleSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'country'      => \Yii::$app->yaml->parse('country'),
            'site'         => ArrayHelper::getColumn(\Yii::$app->yaml->parse('site'), 'name'),
        ]);
    }

    /**
     * Creates a new GrabRule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GrabRule();

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model'   => $model,
                'country' => \Yii::$app->yaml->parse('country'),
                'site'    => ArrayHelper::getColumn(\Yii::$app->yaml->parse('site'), 'name'),
            ]);
        }
    }

    public function actionGetLocation()
    {
        try {
            $response['status'] = 'ok';

            \Yii::$app->response->format = Response::FORMAT_JSON;

            $post = \Yii::$app->request->post();

            $data = \Yii::$app->location->getLocation($post);

            if (!empty($data['state'])) {
                $response['type'] = 'state';
            } else {
                $response['type'] = 'city';
            }

            $response['content'] = $this->renderPartial('select', [
                'data' => $data,
                'type' => $response['type'],
            ]);
        } catch (ErrorException $e) {
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }

        return $response;
    }


    /**
     * Deletes an existing GrabRule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GrabRule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GrabRule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GrabRule::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionActiveRule($id)
    {
        $grabRule = $this->findModel($id);
        $grabRule->isActive = 1;
        $grabRule->save();

        return $this->redirect(\Yii::$app->request->referrer);
    }

    public function actionDisableRule($id)
    {
        $grabRule = $this->findModel($id);
        $grabRule->isActive = 0;
        $grabRule->save();

        return $this->redirect(\Yii::$app->request->referrer);
    }
}
