<?php

namespace app\controllers;

use sender\registration\components\LocationCorrecter;
use Yii;
use app\models\phoenix\CorrectLocation;
use app\models\phoenix\CorrectLocationSearch;
use yii\web\NotFoundHttpException;

/**
 * CorrectLocationController implements the CRUD actions for CorrectLocation model.
 */
class CorrectLocationController extends BaseController
{
    /**
     * Lists all CorrectLocation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CorrectLocationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new CorrectLocation model.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CorrectLocation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$container->get('locationCorrecter')->updateExistentSenders($model);

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CorrectLocation model.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CorrectLocation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CorrectLocation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CorrectLocation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CorrectLocation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
