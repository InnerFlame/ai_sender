<?php

namespace app\controllers;

use sender\dashboard\components\Dashboard;
use yii\base\Module;
use yii\data\ArrayDataProvider;

/**
 * Class DashboardController
 * @package app\controllers
 */
class DashboardController extends BaseController
{
    /**
     * @var $dashBoardComponent Dashboard
     */
    private $dashBoardComponent;

    /**
     * @inheritdoc
     *
     * DashboardController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);

        /** @var $dashboardComponent Dashboard */
        $dashboardComponent = \Yii::$container->get('dashboard');

        $this->setDashBoardComponent($dashboardComponent);
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        return $this->render(
            'index',
            [
                'runningClients' => new ArrayDataProvider(
                    [
                        'allModels' => $this->getDashBoardComponent()->getRunningClientsCounts(),
                        'pagination' => false,
                    ]
                ),
                'readyProfiles' => new ArrayDataProvider(
                    [
                        'allModels' => $this->getDashBoardComponent()->getReadyClientsCounts(),
                        'pagination' => false,
                    ]
                ),
                'notUsedEmailsCount' => $this->getDashBoardComponent()->getNotUsedEmailsCount(),
                'notUsedPhotosCount' => new ArrayDataProvider(
                    [
                        'allModels' => $this->getDashBoardComponent()->getNotUsedPhotosCount(),
                        'pagination' => false,
                    ]
                ),
            ]
        );
    }

    /**
     * @return string
     */
    public function actionNiches(): string
    {
        $recipientsNiches = new ArrayDataProvider([
            'allModels'     => $this->getDashBoardComponent()->getRecipientsNichesData(),
            'pagination'    => false,
        ]);

        $sendersNiches = new ArrayDataProvider([
            'allModels'     => $this->getDashBoardComponent()->getSendersNichesData(),
            'pagination'    => false,
        ]);

        return $this->render('niches', [
            'recipientsNiches'  => $recipientsNiches,
            'sendersNiches'     => $sendersNiches
        ]);
    }

    public function actionSplits()
    {
        $splitsData = new ArrayDataProvider([
            'allModels'     => \Yii::$container->get('splitSystem')->getActiveSplits(),
            'pagination'    => false,
        ]);

        return $this->render('splits', [
            'splitsData'   => $splitsData,
            'sites'        => $this->getSitesDropDownData(),
            'genders'      => \Yii::$app->yaml->parse('gender'),
            'sexualities'  => \Yii::$app->yaml->parse('sexuality')
        ]);
    }

    /**
     * Get data for index and crud dropDowns
     *
     * @return array
     */
    private function getSitesDropDownData(): array
    {
        $out   = [];
        $sites = \Yii::$app->yaml->parse('site');

        foreach ($sites as $siteId => $site) {
            $out[$siteId] = $site['name'];
        }

        return $out;
    }

    /**
     * @return Dashboard
     */
    public function getDashBoardComponent(): Dashboard
    {
        return $this->dashBoardComponent;
    }

    /**
     * @param Dashboard $dashBoardComponent
     */
    public function setDashBoardComponent(Dashboard $dashBoardComponent)
    {
        $this->dashBoardComponent = $dashBoardComponent;
    }
}
