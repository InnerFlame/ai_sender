<?php

namespace app\controllers;

use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileSearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * ProfileSenderController implements the CRUD actions for ProfileSender model.
 */
class SenderProfileController extends BaseController
{
    /**
     * Lists all ProfileSender models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new SenderProfileSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        $actionWay    = [
            SenderProfile::ACTION_WAY_EXTERNAL => SenderProfile::ACTION_WAY_EXTERNAL,
            SenderProfile::ACTION_WAY_DEFAULT  => SenderProfile::ACTION_WAY_DEFAULT,
        ];

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'site'         => ArrayHelper::getColumn(\Yii::$app->yaml->parse('site'), 'name'),
            'country'      => \Yii::$app->yaml->parse('country'),
            'sexuality'    => \Yii::$app->yaml->parse('sexuality'),
            'gender'       => \Yii::$app->yaml->parse('gender'),
            'userTypes'    => SenderProfile::$userTypes,
            'statuses'     => SenderProfileSearch::$statuses,
            'actionWay'    => $actionWay,
            'language'     => \Yii::$app->yaml->parseByKey('language', 'languages'),
            'photoStatus'  => SenderProfile::$photoStatuses,
            'niche'        => \Yii::$container->get('nicheQuery')->getNiches(),
            'senderProfileSearchParams' => \Yii::$app->request->getQueryParam('SenderProfileSearch'),
        ]);
    }

    /**
     * Displays a single ProfileSender model.
     * @return mixed
     */
    public function actionView(int $id)
    {
        $model = $this->findModel($id);
        $data  = [
            'model'       => $model,
            'imageUrl'    => \Yii::$app->senderPhoto->getUrl($model),
            'site'        => \Yii::$app->yaml->parseByKey('site', $model->site)['name'],
            'country'     => \Yii::$app->yaml->parseByKey('country', $model->country),
            'sexuality'   => \Yii::$app->yaml->parseByKey('sexuality', $model->sexuality),
            'gender'      => \Yii::$app->yaml->parseByKey('gender', $model->gender)
        ];

        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('view', $data);
        } else {
            return $this->render('view', $data);
        }
    }

    /**
     * Deletes an existing ProfileSender model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionDelete(int $id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProfileSender model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @return SenderProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id)
    {
        if (($model = SenderProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return mixed
     */
    public function actionExport()
    {
        header('Content-Type: text/csv');
        header('Content-disposition: attachment;filename=export.csv');

        $query = (new SenderProfileSearch())
            ->getQuery(\Yii::$app->request->queryParams)
            ->select('senderProfile.id, senderProfile.originId')
            ->asArray();
        $outputBuffer = fopen("php://output", 'w');
        foreach ($query->batch(500) as $profiles) {
            foreach ($profiles as $profile) {
                $data = ['originId' => $profile['originId']];
                fputcsv($outputBuffer, $data);
            }
        }

        return;
    }
}
