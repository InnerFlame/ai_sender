<?php

namespace app\controllers;

use app\models\phoenix\Schema;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\models\common\User;

class BaseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::SUPER_ADMIN, User::ADMIN],
                    ]
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    protected function getAvailableTraffSources(): array
    {
        $traffSources = \Yii::$app->yaml->parse('sources');

        return array_combine($traffSources, $traffSources);
    }

    /**
     * @return array
     */
    protected function getAvailableCountries(): array
    {
        return \Yii::$app->yaml->parse('country');
    }

    /**
     * @return array
     */
    protected function getAvailableSites(): array
    {
        return ArrayHelper::getColumn(\Yii::$app->yaml->parse('site'), 'name');
    }

    /**
     * @return array
     */
    protected function getAvailableProjects(): array
    {
        $projects = array_keys(\Yii::$app->yaml->parse('projectBySiteId'));

        return array_combine($projects, $projects);
    }

    /**
     * @return array
     */
    protected function getAvailableSchemas(): array
    {
        $schemas       = [];
        $schemasModels = Schema::find()->all();

        /* @var $schemasModels Schema[] */
        foreach ($schemasModels as $schemaModel) {
            $schemas[$schemaModel->id] = $schemaModel->title;
        }

        return $schemas;
    }
}