<?php
return [
    'create' => [
        'type' => 2,
    ],
    'crudUser' => [
        'type' => 2,
    ],
    'content' => [
        'type' => 2,
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'create',
            'contentManager',
        ],
    ],
    'superAdmin' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'crudUser',
            'content',
            'admin',
        ],
    ],
    'contentManager' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'content',
        ],
    ],
];
