<?php
namespace app\rbac;

use Yii;
use yii\rbac\Rule;
use app\models\common\User;

class UserGroupRule extends Rule
{
    public $name = 'userGroup';

    public function execute($user, $item, $params)
    {
        if (!\Yii::$app->user->isGuest) {
            $group = \Yii::$app->user->identity->role;

            if ($item->name === User::SUPER_ADMIN) {
                return $group == User::SUPER_ADMIN_KEY;
            } elseif ($item->name === User::ADMIN) {
                return $group == User::SUPER_ADMIN_KEY || $group == User::ADMIN_KEY;
            } elseif ($item->name === User::CONTENT_MANAGER) {
                return $group == User::CONTENT_MANAGER_KEY
                    || $group == User::ADMIN_KEY
                    || User::SUPER_ADMIN_KEY;
            }
        }

        return false;
    }
}