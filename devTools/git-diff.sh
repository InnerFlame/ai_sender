#!/bin/sh
PROJECT_DIR="/srv/www/sender.dev/public_html"
DIFFS_DIR="/home/khachatryan/Documents/gitDiffs"

echo 'Your project directory is =' $PROJECT_DIR ' (change it in ShellScript)'
echo 'Your diffs storage is =' $DIFFS_DIR ' (change it in ShellScript)'


echo "Please enter base branch name: "
read input_variable
BASE_BRANCH=$input_variable

echo "Please enter your branch that you want to compare with base one: "
read input_variable
COMPARE_BRANCH=$input_variable

echo "Please enter generated file-name:  (if not it will-be generated on this template COMPARE_BRANCH.txt)"
read input_variable
FILE_NAME=$input_variable


echo "Trying to generate diff..."
cd $PROJECT_DIR

if [ "$FILE_NAME" == "" ]; then
    git diff $BASE_BRANCH $COMPARE_BRANCH > "$DIFFS_DIR/$COMPARE_BRANCH.txt"
else
    git diff $BASE_BRANCH $COMPARE_BRANCH > "$DIFFS_DIR/$FILE_NAME"
fi

echo "ALL DONE! Your diff in $DIFFS_DIR directory"

