<?php

return [
    'target_php_version' => '7.1',
    'directory_list' => [
        '.phan/stubs',
        'assets',
        'commands',
        'components',
        'controllers',
        'exception',
        'models',
        'rbac',
        'src',
        'vendor/imagine/imagine/lib/Imagine/Image',
        'vendor/linslin/yii2-curl',
        'vendor/monolog/monolog/src/Monolog',
        'vendor/php-amqplib/php-amqplib/PhpAmqpLib',
        'vendor/phpoffice/phpexcel/Classes',
        'vendor/psr/log/Psr/Log',
        'vendor/ramsey/uuid/src',
        'vendor/symfony/yaml',
        'vendor/yiisoft/yii2',
        'vendor/yiisoft/yii2-imagine',
    ],
    'exclude_analysis_directory_list' => [
        '.phan',
        'vendor',
    ],
    'suppress_issue_types' => [
        'PhanParamReqAfterOpt',
        'PhanParamSuspiciousOrder',
        'PhanUndeclaredClassConstant',

        'PhanParamTooMany',
        'PhanTypeMismatchArgument',
        'PhanTypeMismatchProperty',
        'PhanTypeMismatchReturn',
        'PhanUndeclaredFunctionInCallable',
        'PhanUndeclaredVariableDim',
        'PhanUnreferencedUseNormal',
    ],
];
