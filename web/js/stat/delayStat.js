$(document).ready(function () {
    $('[name = "dateFrom"]').change(function () {
        showGroup()
    });

    $('[name = "dateTo"]').change(function () {
        showGroup()
    });

    $('#type').change(function () {
        if ($(this).val() == 'initial') {
            $('#step-box').css('display', 'none');
        } else {
            $('#step-box').css('display', 'block');
            $('#step').val('');
        }
    });

    $('#find').click(function () {
        $('.spinner').css('display', 'block');
        $.ajax({
                url: '',
                method: 'POST',
                data: {
                    dateFrom: $('[name = "dateFrom"]').val(),
                    dateTo: $('[name = "dateTo"]').val(),
                    delay: $('#delay').val(),
                    type: $('#type').val(),
                    step: $('#step').val(),
                    group: $('#group').val()
                }
            })
            .success(function (data) {
                $('.spinner').css('display', 'none');
                $('#content-box').empty();

                if (data.status == 'ok') {
                    if (data.data.length > 0) {
                        var countInScheduler = [];
                        var countSentWithDelay = [];
                        var countNotSent = [];
                        var countSent = [];
                        var date = [];

                        for (var i = 0; i < data.data.length; i++) {
                            countInScheduler.push(parseInt(data.data[i]['countSchedule']));
                            countSentWithDelay.push(parseInt(data.data[i]['countSentWithDelay']));
                            countNotSent.push(parseInt(data.data[i]['countNotSent']));
                            countSent.push(parseInt(data.data[i]['countSent']));
                            date.push(data.data[i]['date']);
                        }

                        displayChart(countInScheduler, countSentWithDelay, countNotSent, countSent, date)
                    } else {
                        $('#content-box').append('<div style="margin-left: 20px;">No data</div>');
                    }
                } else if (data.status == 'error') {
                    $('#content-box').append('<div style="margin-left: 20px;">' + data.message + '</div>');
                }
            })
            .error(function () {
                $('.spinner').css('display', 'none');
                $('#content-box').empty().append('<div style="margin-left: 20px;">Server error!</div>');
            });
    });
});

function showGroup() {
    if ($('[name = "dateFrom"]').val() == $('[name = "dateTo"]').val()) {
        $('.group-box').css('display', 'block');
    } else {
        $('.group-box').css('display', 'none');
    }
}

function displayChart(countInScheduler, countSentWithDelay, countNotSent, countSent, date){
    $('#content-box').highcharts({
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        xAxis: {
            min: 0,
            categories: date
        },
        legend: {
            shadow: false
        },
        tooltip: {
            shared: true
        },
        plotOptions: {
            column: {
                grouping: false,
                shadow: false,
                borderWidth: 0,
                stacking: 'normal'

            },
            areaspline: {
                fillOpacity: 0.5,
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'Count In Scheduler',
            color: '#2e6da4',
            data: countInScheduler,
            stack: 'grab'
        }, {
            name: 'Count Sent With Delay',
            color: '#d43f3a',
            data: countSentWithDelay
        }, {
            name: 'Count Not Sent',
            color: '#d78f44',
            data: countNotSent
        }, {
            name: 'Count Sent',
            color: '#87CEEB',
            data: countSent
        }]
    });
}