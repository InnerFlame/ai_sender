$(document).ready(function () {
    $('#find').click(function () {
        $('.spinner').css('display', 'block');

        $.ajax({
                url: '',
                method: 'POST',
                data: {
                    date: $('[name = "date"]').val(),
                    group: $('#group').val()
                }
            })
            .success(function (data) {
                $('.spinner').css('display', 'none');
                $('#content-box').empty();

                if (data.status == 'ok') {
                    if (data.data.length > 0) {
                        for (var i = 0; i < data.data.length; i++) {
                            var count = [];
                            var date = [];
                            var title = data.data[i].site + ' - ' + data.data[i].country;
                            var id = 'box-' + i;

                            if ($('#group').val() == 'hour') {
                                $('#content-box').append('<div style="float: left;width: 50%;height: 200px" id="' + id + '"></div>');
                            } else {
                                $('#content-box').append('<div style="float: left;width: 100%;height: 200px" id="' + id + '"></div>');
                            }

                            for (var j = 0; j < data.data[i].count.length; j++) {
                                count.push(parseInt(data.data[i].count[j]));
                                date.push(data.data[i].date[j]);
                            }

                            displayChart(count, date, id, title);
                        }
                    } else {
                        $('#content-box').append('<div style="margin-left: 20px;">No data</div>');
                    }
                } else if (data.status == 'error') {
                    $('#content-box').append('<div style="margin-left: 20px;">' + data.message + '</div>');
                }
            })
            .error(function () {
                $('.spinner').css('display', 'none');
                $('#content-box').empty().append('<div style="margin-left: 20px;">Server error!</div>');
            });
    });
});

function displayChart(count, date, id, title){
    $('#' + id).highcharts({
        title: {
            text: title,
        },
        subtitle: {
            text: '',
            x: -20
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: date
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            data: count
        }]
    });
}