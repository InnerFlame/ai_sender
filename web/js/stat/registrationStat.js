$(document).ready(function () {
    $('[name = "dateFrom"]').change(function () {
        showGroup()
    });

    $('[name = "dateTo"]').change(function () {
        showGroup()
    });

    $('#find').click(function () {
        $('.spinner').css('display', 'block');

        $.ajax({
                url: '',
                method: 'POST',
                data: {
                    country:  $('#country').val(),
                    dateFrom: $('[name = "dateFrom"]').val(),
                    dateTo:   $('[name = "dateTo"]').val(),
                    group:    $('#group').val()
                }
            })
            .success(function (data) {
                $('.spinner').css('display', 'none');
                $('#content-box').empty();

                if (data.status == 'ok') {
                    if (data.data.length > 0) {
                        var countInSchedule    = [];
                        var countRegistered    = [];
                        var countNotRegistered = [];
                        var date               = [];

                        for (var i = 0; i < data.data.length; i++) {
                            countInSchedule.push(parseInt(data.data[i]['countInSchedule']));
                            countRegistered.push(parseInt(data.data[i]['countRegistered']));
                            countNotRegistered.push(parseInt(data.data[i]['countNotRegistered']));
                            date.push(data.data[i]['date']);
                        }

                        displayChart(countInSchedule, countRegistered, countNotRegistered, date);
                    } else {
                        $('#content-box').append('<div style="margin-left: 20px;">No data</div>');
                    }
                } else if (data.status == 'error') {
                    $('#content-box').append('<div style="margin-left: 20px;">' + data.message + '</div>');
                }
            })
            .error(function () {
                $('.spinner').css('display', 'none');
                $('#content-box').empty().append('<div style="margin-left: 20px;">Server error!</div>');
            });
    });
});

function showGroup() {
    if ($('[name = "dateFrom"]').val() == $('[name = "dateTo"]').val()) {
        $('.group-box').css('display', 'block');
    } else {
        $('.group-box').css('display', 'none');
    }
}

function displayChart(countInSchedule, countRegistered, countNotRegistered, date){
    $('#content-box').highcharts({
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        xAxis: {
            min: 0,
            categories: date
        },
        legend: {
            shadow: false
        },
        tooltip: {
            shared: true
        },
        plotOptions: {
            column: {
                grouping: false,
                shadow: false,
                borderWidth: 0,
                stacking: 'normal'

            },
            areaspline: {
                fillOpacity: 0.5,
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'Count In Scheduler',
            color: '#2e6da4',
            data: countInSchedule,
            stack: 'grab'
        }, {
            name: 'Count registered',
            color: '#87CEEB',
            data: countRegistered
        }, {
            name: 'Count Not Registered',
            color: '#d43f3a',
            data: countNotRegistered
        }]
    });
}