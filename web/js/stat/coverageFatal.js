$(document).ready(function () {
    $('#find').click(function () {
        $('.spinner').css('display', 'block');
            $.ajax({
                url: '',
                method: 'POST',
                data: {
                    dateFrom: $('[name = "dateFrom"]').val(),
                    dateTo: $('[name = "dateTo"]').val(),
                    country: $('#country').val(),
                }
                })
                .success(function (data) {
                    $('.spinner').css('display', 'none');
                    $('#content-box').empty();

                    if (data.status == 'ok') {
                        if (data.data.length > 0) {
                            var countGrab    = [];
                            var countNotSent = [];
                            var date         = [];

                            for (var i = 0; i < data.data.length; i++) {
                                countGrab.push(parseInt(data.data[i]['countGrab']));
                                countNotSent.push(parseInt(data.data[i]['countNotSent']));
                                date.push(data.data[i]['date']);
                            }

                            displayChart(countGrab, countNotSent ,date)
                        } else {
                            $('#content-box').append('<div style="margin-left: 20px;">No data</div>');
                        }
                    } else if (data.status == 'error') {
                        $('#content-box').append('<div style="margin-left: 20px;">' + data.message + '</div>');
                    }
                })
                .error(function () {
                    $('.spinner').css('display', 'none');
                    $('#content-box').empty().append('<div style="margin-left: 20px;">Server error!</div>');
                });
    });
});

function displayChart(countGrab, countNotSent ,date)
{
    $('#content-box').highcharts({
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        xAxis: {
            min: 0,
            categories: date
        },
        legend: {
            shadow: false
        },
        tooltip: {
            shared: true
        },
        plotOptions: {
            column: {
                grouping: false,
                shadow: false,
                borderWidth: 0,
                stacking: 'normal'

            },
            areaspline: {
                fillOpacity: 0.5,
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'Count Grab',
            color: '#2e6da4',
            data: countGrab,
            stack: 'grab'
        }, {
            name: 'Count Not Sent',
            color: '#d43f3a',
            data: countNotSent
        }]
    });
}