$(document).ready(function () {
    $('#find').click(function () {
        $('.spinner').css('display', 'block');
        $.ajax({
            url: '',
            method: 'POST',
            data: {
                dateFrom: $('[name = "dateFrom"]').val(),
                dateTo: $('[name = "dateTo"]').val(),
                country: $('#country').val()
            }
            })
            .success(function (data) {
                $('.spinner').css('display', 'none');
                $('#content-box').empty();
                if (data.status == 'ok') {
                    if (data.data.date.length > 0) {
                        var date   = [];
                        var series = [];
                        for (var i = 0; i < data.data.date.length; i++) {
                            date.push(data.data.date[i]);
                        }
                        $.each(data.data.result, function (name, data) {
                            var seriesData  = {};
                            seriesData.name = name;
                            seriesData.data = data;
                            series.push(seriesData);
                        });
                        displayChart(date, series)
                    } else {
                        $('#content-box').append('<div style="margin-left: 20px;">No data</div>');
                    }
                } else if (data.status == 'error') {
                    $('#content-box').append('<div style="margin-left: 20px;">' + data.message + '</div>');
                }
            })
            .error(function () {
                $('.spinner').css('display', 'none');
                $('#content-box').empty().append('<div style="margin-left: 20px;">Server error!</div>');
            });
    });
});


function displayChart(date, series)
{
    $('#content-box').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
        xAxis: {
            min: 0,
            categories: date
        },
        legend: {
            shadow: false
        },
        tooltip: {
            shared: true
        },
        plotOptions: {
            column: {
                grouping: false,
                shadow: false,
                borderWidth: 0,
                stacking: 'normal'

            },
            areaspline: {
                fillOpacity: 0.5,
                dataLabels: {
                    enabled: true
                }
            }
        },
        series:series
    });
}