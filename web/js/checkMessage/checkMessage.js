$(document).ready(function ($) {
    $('[name = "message"]').keydown(function (e) {
        if (e.ctrlKey && e.keyCode == 13) {
            sendAjax();
        }
    });

    $('#check').click(function () {
        sendAjax();
    })
});

function sendAjax() {
    $('.spinner').css('display', 'block');
    $('tbody').empty();
    $('#content-box').empty();

    $.ajax({
            url: '',
            method: 'POST',
            data: {
                message: $('[name = "message"]').val(),
            }
        })
        .success(function (data) {
            $('.spinner').css('display', 'none');

            if (data.status == 'ok') {
                var html = '';
                for (var i = 0; i < data.data.length; i++) {
                    html += '<tr>';
                    html += '<td>' + data.data[i].analyzer + '</td>';
                    html += '<td>' + data.data[i].scamWord + '</td>';
                    html += '<td>' + data.data[i].text + '</td>';
                    html += '</tr>';
                }

                $('tbody').append(html);
            } else if (data.status == 'error') {
                $('#content-box').append('<div style="margin-left: 20px;">' + data.message + '</div>');
            }
        })
        .error(function () {
            $('.spinner').css('display', 'none');
            $('#content-box').empty().append('<div style="margin-left: 20px;">Server error!</div>');
        });
}