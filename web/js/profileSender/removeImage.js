$(document).on('click', '.fileinput-remove', function () {
    $.ajax({
        url: '/sender-profile/delete-image',
        method: 'POST',
        data: {
            id: $('#profilesender-id').val()
        }
    });
});
