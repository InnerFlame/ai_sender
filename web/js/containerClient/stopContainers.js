$('#stop-all').click(function () {
    $(this).unbind('click');
    $('.spinner').css('display', 'block');

    $.ajax({url: '/container-client/stop-all', method: 'POST'})
        .success(function (data) {
            if (data.status == 'ok') {
                location.reload();
            } else if (data.status == 'error') {
                $('#error-box').empty();
                $('#error-box').append(data.message);
            }
        })
        .error(function () {
            $('#error-box').empty();
            $('#error-box').append('Server error!');
        });
});