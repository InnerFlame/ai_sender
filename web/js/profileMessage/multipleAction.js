$(document).ready(function () {
    $('#multiple-delete').click(function () {
        var keys = $('.grid-view').yiiGridView('getSelectedRows');

        if (keys.length > 0) {
            var data = {
                ids:    keys
            };

            sendingAjax(data, '/profile-message/multiple-delete');
        }
    });

    $('#multiple-deactivation').click(function () {
        var keys = $('.grid-view').yiiGridView('getSelectedRows');

        if (keys.length > 0) {
            var data = {
                ids:    keys,
                status: 0
            };

            sendingAjax(data, '/profile-message/multiple-update-status');
        }
    });

    $('#multiple-activation').click(function () {
        var keys = $('.grid-view').yiiGridView('getSelectedRows');

        if (keys.length > 0) {
            var data = {
                ids:    keys,
                status: 1
            };

            sendingAjax(data, '/profile-message/multiple-update-status');
        }
    });

    $('#multiple-edit').click(function () {
        var keys = $('.grid-view').yiiGridView('getSelectedRows');

        if (keys.length > 0) {
            $('form').submit();
        }
    });
});

function sendingAjax(data, url) {
    $('.spinner').css('display', 'block');
    $('#content-box').empty();

    $.ajax({
            url: url,
            method: 'POST',
            data: data
        })
        .success(function (data) {
            $('.spinner').css('display', 'none');

            if (data.status == 'ok') {
                location.reload();
            } else if (data.status == 'error') {
                $('#content-box').append('<div style="margin-left: 20px;color:red;">' + data.message + '</div>');
            }
        })
        .error(function () {
            $('.spinner').css('display', 'none');
            $('#content-box').append('<div style="margin-left: 20px;color:red;">Server error!</div>');
        });
}
