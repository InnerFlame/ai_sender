$(document).ready(function ($) {
    $('#check').click(function () {
        $('.spinner').css('display', 'block');
        $.ajax({
                url: 'check',
                method: 'POST',
                data: {
                    email: $('[name = "email"]').val(),
                }
            })
            .success(function (data) {
                $('.spinner').css('display', 'none');
                if (data.status == 'ok') {
                    if (data.isValid == 'valid') {
                        $('[name = "email"]').css('border-color', 'green');
                    } else {
                        $('[name = "email"]').css('border-color', 'red');
                    }
                } else if (data.status == 'error') {
                    $('#content-box').append('<div style="margin-left: 20px;">' + data.message + '</div>');
                }
            })
            .error(function () {
                $('.spinner').css('display', 'none');
                $('#content-box').empty().append('<div style="margin-left: 20px;">Server error!</div>');
            });
    })
});
