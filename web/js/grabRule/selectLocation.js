$('#grabrule-country').change(function () {
    var country = $(this).val();

    if (country) {
        $('#state').empty();
        $('#city').empty();
        $('.spinner').css('display', 'block');

        $.ajax({
                url: '/grab-rule/get-location',
                method: 'POST',
                data: {
                    country: country
                }
            })
            .success(function (data) {
                cleanBox();

                if (data.status == 'ok') {
                    if (data.content) {
                        $('#' + data.type).append(data.content);
                    } else {
                        $('#content-box').append('<div style="margin-left: 20px;">No data</div>');
                    }
                } else if (data.status == 'error') {
                    cleanBox();
                    $('#state').append('<div style="margin-left: 20px;">' + data.message + '</div>');
                }
            })
            .error(function () {
                cleanBox();
                $('#state').append('<div style="margin-left: 20px;">Server error!</div>');
            });
    }
});

$('body').on('change', '#grabrule-state', function () {
    var state = $(this).val();

    if (state) {
        $('#city').empty();
        $('.spinner').css('display', 'block');

        $.ajax({
                url: '/grab-rule/get-location',
                method: 'POST',
                data: {
                    country: $('#grabrule-country').val(),
                    state:   state
                }
            })
            .success(function (data) {
                cleanCity();

                if (data.status == 'ok') {
                    if (data.content) {
                        $('#' + data.type).append(data.content);
                    } else {
                        $('#content-box').append('<div style="margin-left: 20px;">No data</div>');
                    }
                } else if (data.status == 'error') {
                    cleanCity();
                    $('#state').append('<div style="margin-left: 20px;">' + data.message + '</div>');
                }
            })
            .error(function () {
                cleanCity();
                $('#state').append('<div style="margin-left: 20px;">Server error!</div>');
            });
    }
});

function cleanBox() {
    $('.spinner').css('display', 'none');
    $('#state').empty();
    $('#city').empty();
}

function cleanCity() {
    $('.spinner').css('display', 'none');
    $('#city').empty();
}