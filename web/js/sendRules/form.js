(function () {
    const sitesBox    = 'sitesBox';
    const projectsBox = 'projectsBox';

    /**
     * Main function - disable or enable select for rules sites and projects
     */
    function disableListBox() {
        $(getEventSelector()).change(function (event) {
            var elementClass = this.classList[1];
            var selectedOptions = parseInt(
                $(getClassName(elementClass, true) + ' :selected').length
            );

            if (selectedOptions === 0) {
                enableBoxes();
                return false;
            }

            if (elementClass === sitesBox) {
                $(getClassName(projectsBox, true)).prop('disabled', true);
            } else {
                $(getClassName(sitesBox, true)).prop('disabled', true);
            }

            if (!$(getClassName(projectsBox, true)).val() && !$(getClassName(sitesBox, true)).val()) {
                enableBoxes();
            }
        });
    }

    /**
     * Initial list boxes status
     * @returns {boolean}
     */
    function initialListBoxes() {
        var selectedSitesOptions = parseInt(
            $(getClassName(sitesBox, true) + ' :selected').length
        );
        var selectedProjectsOptions = parseInt(
            $(getClassName(projectsBox, true) + ' :selected').length
        );

        if (selectedProjectsOptions === 0 && selectedSitesOptions ===0) {
            return false;
        }

        if(selectedSitesOptions > 0) {
            $(getClassName(projectsBox, true)).prop('disabled', true);
        } else {
            $(getClassName(sitesBox, true)).prop('disabled', true);
        }
    }

    /**
     * Enable both selects
     */
    function enableBoxes() {
        $(getClassName(sitesBox, true)).prop('disabled', false);
        $(getClassName(projectsBox, true)).prop('disabled', false);
    }

    /**
     * Get listBox className with dor or not
     * @param className
     * @param withDot
     * @returns {*}
     */
    function getClassName(className, withDot) {
        if (withDot) {
            return '.' + className;
        }

        return className;
    }

    /**
     * Get event selector for main function
     * @returns {string}
     */
    function getEventSelector() {
        return getClassName(sitesBox, true) + ',' + getClassName(projectsBox, true);
    }

    /**
     * Run script on page ready
     */
    $(document).ready(function () {
        initialListBoxes();
        disableListBox();
    });
})();