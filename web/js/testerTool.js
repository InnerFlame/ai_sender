$(document).ready(function () {
    $('#find').click(function () {
        sendAjax();
    });

    $(document).keydown(function() {
        if (event.keyCode==13) {
            sendAjax();
        }
    });

    function sendAjax() {
        var box        = $('#response-box');
        var profileIds = $('#profileIds').val();

        if ($.trim(profileIds) != '') {
            $('.spinner').css('display', 'block');
            $.ajax({
                    url: '/tester-tool/get-data',
                    method: 'POST',
                    data: {
                        ids: profileIds
                    }
                })
                .success(function (data) {
                    $('.spinner').css('display', 'none');
                    box.empty();

                    if (data.status == 'ok') {
                        if (data.content == '') {
                            box.append('No data');
                        } else {
                            box.append(data.content);
                            box.css('padding', '10px').css('background', 'beige');
                        }
                    } else if (data.status == 'error') {
                        box.append(data.message);
                    }
                })
                .error(function () {
                    $('.spinner').css('display', 'none');
                    box.empty();
                    box.append('Server error!');
                });
        }
    }
});

