$(document).ready(function () {
    $('#find').click(function () {
        $('.spinner').css('display', 'block');
        $.ajax({
                url: '',
                method: 'POST',
                data: {
                    dateFrom: $('[name = "dateFrom"]').val(),
                    dateTo: $('[name = "dateTo"]').val(),
                    method: $('#method').val(),
                    error: $('#error').val(),
                    group: $('#group').val(),
                }
            })
            .success(function (data) {
                $('.spinner').css('display', 'none');
                $('#content-box').empty();

                if (data.status == 'ok') {
                    if (data.data.length > 0) {
                        var count = [];
                        var date  = [];

                        for (var i = 0; i < data.data.length; i++) {
                            count.push(parseInt(data.data[i]['count']));
                            date.push(data.data[i]['date']);
                        }

                        displayChart(count, date)
                    } else {
                        $('#content-box').append('<div style="margin-left: 20px;">No data</div>');
                    }
                } else if (data.status == 'error') {
                    $('#content-box').append('<div style="margin-left: 20px;">' + data.message + '</div>');
                }
            })
            .error(function () {
                $('.spinner').css('display', 'none');
                $('#content-box').empty().append('<div style="margin-left: 20px;">Server error!</div>');
            });
    });
});

function displayChart(count, date){
    $('#content-box').highcharts({
        title: {
            text: '',
        },
        subtitle: {
            text: '',
            x: -20
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: date
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            data: count,
            name: 'Error Count',
        }]
    });
}