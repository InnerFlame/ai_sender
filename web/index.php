<?php

require(__DIR__ . '/../config/Constant.php');

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', \app\config\Constant::YII_DEBUG);
defined('YII_ENV') or define('YII_ENV', \app\config\Constant::YII_ENV);

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/web.php');

(new yii\web\Application($config))->run();
