<?php

namespace app\components\userAgent;

use app\components\common\YamlComponent;
use app\exception\NoUserAgentsException;
use yii\caching\MemCache;

class UserAgentManagerComponent
{
    const CACHE_TTL = 172800;
    const CACHE_PREFIX = 'user-agent_';
    const MOB_USER_AGENT_CONFIG = 'userAgentMob';
    const MAX_USER_AGENT_USAGE_COUNT = 2;

    public function getUserAgentId()
    {
        $pool = $this->getPool();
        if (empty($pool)) {
            throw new NoUserAgentsException();
        }
        $result = array_shift($pool);
        $this->savePool($pool);

        return $result;
    }

    public function getUserAgentById($id)
    {
        return $this->getYamlComponent()->parseByKey(self::MOB_USER_AGENT_CONFIG, $id);
    }

    /**
     * @return YamlComponent
     */
    protected function getYamlComponent()
    {
        return \Yii::$app->yaml;
    }

    /**
     * @return MemCache
     */
    protected function getCacheComponent()
    {
        return \Yii::$app->cache;
    }

    private function generatePool()
    {
        $pool = [];
        $count = count($this->getYamlComponent()->parse(self::MOB_USER_AGENT_CONFIG));
        for ($i = 0; $i < self::MAX_USER_AGENT_USAGE_COUNT; $i++) {
            $pool = array_merge($pool, range(0, $count - 1));
        }
        shuffle($pool);
        $this->savePool($pool);
    }

    private function getCacheKey()
    {
        return self::CACHE_PREFIX . date('ymd');
    }

    private function savePool($pool)
    {
        return $this->getCacheComponent()->set($this->getCacheKey(), $pool, self::CACHE_TTL);
    }

    private function getPool()
    {
        if (!$this->getCacheComponent()->exists($this->getCacheKey())) {
            $this->generatePool();
        }
        return $this->getCacheComponent()->get($this->getCacheKey());
    }
}
