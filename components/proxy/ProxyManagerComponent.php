<?php

namespace app\components\proxy;

use app\models\phoenix\SenderProfile;
use linslin\yii2\curl\Curl;
use yii\base\Component;
use app\models\proxy\Socks5UsageLog;

class ProxyManagerComponent extends Component
{
    /** @var mixed[] $luminati */
    public $luminati;
    /** @var mixed[] $socks5 */
    public $socks5;

    public function getProxy(SenderProfile $profile, string $target, string $clientType = null, bool $resolveDns = true)
    {
        $proxyData = null;

        $proxyTarget = \Yii::$app->yaml->parse('proxy/proxyTarget');

        if (isset($proxyTarget[$target])) {
            $dataTarget = $proxyTarget[$target];

            if (isset($dataTarget[$clientType])) {
                $proxy = $dataTarget[$clientType];
            } else {
                $proxy = $dataTarget['default'];
            }

            switch ($proxy) {
                case 'luminati':
                    $proxyData = $this->getLuminatiProxy($profile, null, $resolveDns);
                    break;
                case 'luminati_shared':
                    $proxyData = $this->getLuminatiProxy($profile, 'luminati_shared', $resolveDns);
                    break;
                case 'socks':
                    $proxyData = $this->getSocksProxy($profile);
                    break;
            }
        }

        return $proxyData;
    }

    public function getLuminatiProxy(SenderProfile $profile, string $proxyTarget = null, bool $resolveDns = true)
    {
        if ($resolveDns == true) {
            $ips = dns_get_record('zproxy.luminati.io');
            $ips = array_filter($ips, function ($item) {
                return $item['type'] == 'A';
            });
            $domain = $ips[array_rand($ips)]['ip'];
        } else {
            $domain = 'zproxy.lum-superproxy.io';
        }

        switch ($proxyTarget) {
            case 'luminati_shared':
                $luminati = 'http://lum-customer-' . $this->luminati['customer'];
                $luminati .= '-zone-' . $this->luminati['zone_shared'];
                break;
            default:
                $luminati = 'http://lum-customer-' . $this->luminati['customer'];
                $luminati .= '-zone-' . $this->luminati['zone'];
                $luminati .= $this->getCountryForLuminati($profile->country);
                if ($this->luminati['zone'] == 'city') {
                    $luminati .= $this->getCityDataForLuminati($profile);
                }
        }

        $luminati .= '-session-' . md5($profile->id . time());
        $luminati .= ':' . $this->luminati['password'] . '@' . $domain . ':22225';

        return $luminati;
    }

    protected function getCountryForLuminati(string $country)
    {
        return '-country-' . \Yii::$app->yaml->parseByKey('countryProxyCode', $country);
    }

    protected function getCityDataForLuminati(SenderProfile $profile)
    {
        if ($profile->country == 'USA' || $profile->country == 'JPN' || $profile->country == 'CAN') {
            return '-state-' . mb_strtolower($profile->state) . '-city-' . mb_strtolower($profile->city);
        }

        return '-city-' . $profile->city;
    }

    protected function getSocksProxy(SenderProfile $profile)
    {
        $availableProxies = \Yii::$app->yaml->parse('proxy/socks5');

        $usedProxy = Socks5UsageLog::find()
            ->where(['clientName' => $profile->id])
            ->orderBy('createdAt DESC')
            ->one();

        if (!empty($usedProxy) && in_array($usedProxy->proxy, $availableProxies)) {
            //if client has used proxy and this proxy still available - use it again
            $proxy = $usedProxy->proxy;
        } else {
            $proxy = $availableProxies[array_rand($availableProxies)];
            $logEntry = new Socks5UsageLog();
            $logEntry->clientName = $profile->id;
            $logEntry->proxy = $proxy;
            $logEntry->save();
        }

        $socks5 = 'socks5://' . $this->socks5['user'] . ':' . $this->socks5['pass'] . '@' . $proxy;

        return $socks5;
    }

    public function getIpByProxy(string $proxy)
    {
        $curl = new Curl();

        $data = $curl->setOption(CURLOPT_CONNECTTIMEOUT, 30)
            ->setOption(CURLOPT_TIMEOUT, 30)
            ->setOption(CURLOPT_SSL_VERIFYHOST, 0)
            ->setOption(CURLOPT_SSL_VERIFYPEER, 0)
            ->setOption(CURLOPT_PROXY, $proxy)
            ->get('http://icanhazip.com');

        return str_replace("\n", '', $data);
    }
}
