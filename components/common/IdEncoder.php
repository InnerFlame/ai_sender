<?php

namespace app\components\common;

use app\components\phoenix\checkMessage\SuspiciousWords;
use app\components\phoenix\checkMessage\Text;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\RecipientShortId;

class IdEncoder
{
    const CONFORMITY_ARRAY_ELEMENTS_COUNT   = 10;
    const ALL_ID_ENCODING_CONFIG_ITEMS_SIGN = "*";
    const RECIPIENT_SHORT_ID_ENCODE_MAX_ATTEMPTS = 3;

    /* @var $suspiciousWordsComponent SuspiciousWords */
    private $suspiciousWordsComponent;

    private $conformity = [
        0 => 'a',
        1 => 'D',
        2 => 'E',
        3 => 'n',
        4 => 'j',
        5 => 'O',
        6 => 'l',
        7 => 'i',
        8 => 'Q',
        9 => 'p',
    ];

    /**
     * IdEncoder constructor.
     * @param SuspiciousWords $suspiciousWordsComponent
     */
    public function __construct(SuspiciousWords $suspiciousWordsComponent)
    {
        $this->suspiciousWordsComponent = $suspiciousWordsComponent;
    }

    /**
     * (New logic) Try to generate clean short id from recipientId if cant generate unique clean short id:
     *  - If short id is unique and not scam returns it
     *  - If short id scam or/and not unique returns empty string
     *
     * @param RecipientProfile $recipient
     * @return string
     */
    public function encode(RecipientProfile $recipient): string
    {
        $stringId = (string) $recipient->id;
        $strLen   = strlen($stringId);

        $validShortId = '';
        $attempts     = 1;
        do {
            $conformityArray        = $this->getConformityArray();
            $encodedShortId         = $this->getEncodedShortIdString($strLen, $conformityArray, $stringId);
            $isUniqueShortId        = $this->isUniqueShortId($encodedShortId);
            $isShortIdNotSuspicious = $this->isShortIdNotSuspicious($encodedShortId);

            if ($isShortIdNotSuspicious && $isUniqueShortId) {
                $validShortId = $encodedShortId;
            }

            $attempts++;
        } while (empty($validShortId) && $attempts <= self::RECIPIENT_SHORT_ID_ENCODE_MAX_ATTEMPTS);

        return $validShortId;
    }

    /**
     * @param RecipientProfile $recipient
     * @return string
     */
    public function getRecipientShortId(RecipientProfile $recipient): string
    {
        $shortIdModel = RecipientShortId::findOne(['recipientId' => $recipient->id]);

        if (!$shortIdModel) {
            $shortIdModel = new RecipientShortId();
            $shortIdModel->recipientId = $recipient->id;
            $shortIdModel->shortId = $this->encode($recipient);
            $shortIdModel->remoteId = $recipient->remoteId;
            $shortIdModel->site = $recipient->site;
            $shortIdModel->country = $recipient->country;
            $shortIdModel->save();
            $shortIdModel->refresh();
        }

        return $shortIdModel->shortId;
    }

    /**
     * Get shuffled A-Z+a-z array with CONFORMITY_ARRAY_ELEMENTS_COUNT elements
     *
     * @return array
     */
    private function getConformityArray(): array
    {
        $alphas = array_merge(range('A', 'Z'), range('a', 'z'));
        shuffle($alphas);

        $conformityArray = array_slice($alphas, 0, self::CONFORMITY_ARRAY_ELEMENTS_COUNT);

        return $conformityArray;
    }

    /**
     * @param string $encodedShortId
     * @return bool
     */
    private function isShortIdNotSuspicious(string $encodedShortId): bool
    {
        $text          = new Text($encodedShortId);
        $analyzeResult = $this->suspiciousWordsComponent->analyze($text);

        foreach ($analyzeResult as $key => $item) {
            if ($item->isSuspicious()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $strLen
     * @param $conformityArray
     * @param $stringId
     * @return string
     */
    private function getEncodedShortIdString($strLen, $conformityArray, $stringId): string
    {
        $encodeId = '';

        for ($i = 0; $i < $strLen; $i++) {
            if (!empty($conformityArray[$stringId[$i]])) {
                $encodeId .= $conformityArray[$stringId[$i]];
            }
        }
        return $encodeId;
    }

    /**
     * @param $encodedId
     * @return bool
     */
    private function isUniqueShortId($encodedId): bool
    {
        return (bool) !RecipientShortId::findOne(['shortId' => $encodedId]);
    }
}