<?php

namespace app\components\common;

use app\config\Constant;
use app\models\phoenix\SenderProfile;
use yii\base\Component;

class TextTransformationComponent extends Component
{
    private $vowels         = ['a', 'e', 'i', 'o', 'u', 'y'];
    private $consonants     = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z'];
    private $countReplace   = 3;

    public function addRandomCharacters ($text, SenderProfile $senderProfile)
    {
        $textHash       = md5($text);
        $words          = explode(' ', $text);
        $keyRandomWords = $this->getKeyRandomWords($words);
        foreach ($keyRandomWords as $keyRandomWord) {
            $letter = $this->getRandomLetter($textHash, $keyRandomWord);
            if (empty($letter)) {
                continue;
            }

            $words[$keyRandomWord] = $words[$keyRandomWord] . $letter;

            $this->saveToUsedLetters($letter, $textHash, $keyRandomWord);
        }

        foreach ($words as $key => $word) {
            if (strpos($word, 'http') !== false) {
                $words[$key] = $this->getTransformationLink($word, $senderProfile);
            }
        }

        return implode(' ', $words);
    }

    public function addRandomASCIICodes($text, SenderProfile $senderProfile)
    {
        $textArray = explode(' ', $text);

        $newText = '';
        foreach ($textArray as $word) {
            if (strpos($word, 'http') !== false) {
                $newText .= $this->getTransformationLink($word, $senderProfile) . ' ';
            } else {
                $textLength   = strlen($word);
                $randPosition = rand(0, $textLength);

                $newText .= substr($word, 0, $randPosition) . $this->getRandomChr();
                $newText .= substr($word, $randPosition) . ' ';
            }
        }

        return $newText;
    }

    private function getTransformationLink($url, SenderProfile $senderProfile)
    {
        $config = \Yii::$app->yaml->parse('linkTransformation');
        if ($config['enable']
            && in_array($senderProfile->country, $config['country'])
            && in_array($senderProfile->actionWay, $config['actionWay'])
        ) {
            $urlArray           = parse_url($url);
            $urlArray['scheme'] = implode($this->getRandomChr(), str_split($urlArray['scheme']));
            $url                = $urlArray['scheme'] . ":\t//" . $urlArray['host'] . $urlArray['path'];

            if (!empty($urlArray['query'])) {
                $urlArray['query']  = implode($this->getRandomChr(), str_split($urlArray['query']));
                $url               .= "?" . $urlArray['query'];
            }

            $url                = str_replace('/' , "/\t", $url);
            $url                = str_replace('.' , ".\t", $url);
        }

        return $url;
    }

    private function getRandomChr()
    {
        return "\t";
    }

    private function getKeyRandomWords($words)
    {
        $keyRandomWords = [];
        $words          = array_filter($words, function($item) {
            if (!empty($item)
                && strpos($item, 'http') === false
                && strpos('!@#$%^&*()_+=-`~?/.>,<\'|"{}[]', $item[strlen($item) - 1]) === false
            ) {
                return $item;
            }
        });

        $countWords = count($words);
        if ($countWords < $this->countReplace) {
            $this->countReplace = $countWords;
        }

        $keyWords = array_keys($words);
        for ($i = 0; $i < $this->countReplace; $i++) {
            shuffle($keyWords);
            $keyRandomWords[] = array_shift($keyWords);
        }

        return $keyRandomWords;
    }

    private function saveToUsedLetters($letter, $textHash, $keyRandomWord)
    {
        $redisKey = 'usedLetters' . $keyRandomWord . $textHash;
        $expire   = 3600000;
        $ttl      = \Yii::$app->redis->executeCommand('PTTL', [$redisKey]);

        if ($ttl > 0) {
            $expire = $ttl;
        }

        \Yii::$app->redis->executeCommand('RPUSH', [$redisKey, $letter]);
        \Yii::$app->redis->executeCommand('PEXPIRE', [$redisKey, $expire]);
    }

    private function getRandomLetter($textHash, $keyRandomWord)
    {
        $redisKey    = 'usedLetters' . $keyRandomWord . $textHash;
        $usedLetters = \Yii::$app->redis->executeCommand('LRANGE', [$redisKey, '0', '-1']);
        $vowels      = array_diff($this->vowels, $usedLetters);
        if (!empty($vowels)) {
            return $vowels[array_rand($vowels)];
        }

        $consonants = array_diff($this->consonants, $usedLetters);
        if (!empty($consonants)) {
            return $consonants[array_rand($consonants)];
        }

        return null;
    }
}