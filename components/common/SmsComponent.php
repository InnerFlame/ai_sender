<?php

namespace app\components\common;

use yii\base\Component;

class SmsComponent extends Component
{
    public $wsdl;
    public $title;
    public $login;
    public $password;
    public $recipients = [];

    const MEMCACHE_SMS_ALERTING_KEY = 'smsAlertingStatus';

    public function sendMessage($text)
    {
        if (\Yii::$app->cache->get(self::MEMCACHE_SMS_ALERTING_KEY)) {
           return false;
        }

        $msisdn = implode(',', $this->recipients);
        try {
            $client = new \SoapClient($this->wsdl);

            $auth = [
                'login'     => $this->login,
                'password'  => $this->password,
            ];

            $client->Auth($auth);

            $sms = [
                'sender'        => $this->title,
                'destination'   => $msisdn,
                'text'          => $text,
            ];
            $result = $client->SendSMS($sms);

            return $result->SendSMSResult->ResultArray;
        } catch (\Exception $e) {
            \Yii::warning($e->getMessage());
        }

        return false;
    }

}