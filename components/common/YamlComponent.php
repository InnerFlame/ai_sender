<?php

namespace app\components\common;

use yii\base\Component;
use Symfony\Component\Yaml\Yaml;
use yii\base\ErrorException;
use yii\web\Application as WebApplication;

class YamlComponent extends Component
{
    public $pathOfConfig;

    public function parse($file)
    {
        $memcacheKey = sha1('file_' . $file);

        $result = \Yii::$app->cache->get($memcacheKey);

        if (!$result) {
            $result = $this->getYaml($file);
            \Yii::$app->cache->set($memcacheKey, $result, 3600);
        }

        return $result;
    }

    public function parseByKey($file, $key)
    {
        $result = false;
        $config = $this->parse($file);
        if (array_key_exists($key, $config)) {
            $result = $config[$key];
        }

        return $result;
    }

    public function clearCache($directory = '')
    {
        try {
            $files = new \DirectoryIterator($this->pathOfConfig . $directory);

            foreach ($files as $item) {
                if ($this->isDot($item)) {
                    continue;
                }

                if ($item->isFile()) {
                    $this->clearFileCache($directory . $item->getFilename());
                } else {
                    $this->clearCache($directory . $item->getFilename() . '/');
                }
            }

            if (\Yii::$app instanceof WebApplication) {
                \Yii::$app->getSession()->setFlash('success', 'Clear Cache done');
            }
        } catch (ErrorException $e) {
            if (\Yii::$app instanceof WebApplication) {
                \Yii::$app->getSession()->setFlash('error', $e->getMessage());
            } else {
                throw  $e;
            }
        }
    }

    private function clearFileCache($file)
    {
        list($name, $extension) = explode('.', $file);
        \Yii::$app->cache->delete(sha1('file_' . $name));
    }

    private function getYaml($file)
    {
        return Yaml::parse(file_get_contents($this->pathOfConfig . $file . '.yaml'));
    }

    /**
     * @param \DirectoryIterator $item
     * @return bool
     */
    private function isDot(\DirectoryIterator $item)
    {
        return $item->getFilename() == '..' || $item->getFilename() == '.';
    }
}