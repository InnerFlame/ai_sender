<?php

namespace app\components\common;

use linslin\yii2\curl\Curl;
use yii\base\Component;

class GeoIpComponent extends Component
{
    public function getTimezoneByIp($ip, $proxy = null)
    {
        try {
            $curl     = new Curl();
            $curl->setOption(CURLOPT_CONNECTTIMEOUT, 30)
                ->setOption(CURLOPT_TIMEOUT, 30)
                ->setOption(CURLOPT_SSL_VERIFYHOST, 0)
                ->setOption(CURLOPT_SSL_VERIFYPEER, 0);

            if (!empty($proxy)) {
                $curl->setOption(CURLOPT_PROXY, $proxy);
            }

            $data = $curl->get('http://ip-api.com/json/' . $ip);
            $data = json_decode($data, true);

            if (empty($data)) {
                throw new \Exception('Empty response for getting timezone');
            }

            if (!empty($data['status']) && $data['status'] == 'fail') {
                    throw new \Exception($data['message']);
            }

            $dataTime = new \DateTime();
            $timeZone = new \DateTimeZone($data['timezone']);
            $timeZone = $timeZone->getOffset($dataTime);

            if (!is_int($timeZone)) {
                throw new \Exception('Empty timezone');
            }

            return $timeZone;
        } catch (\Exception $e) {
            \Yii::warning($e->getMessage() . PHP_EOL . $e->getTraceAsString());
            return false;
        }
    }
}