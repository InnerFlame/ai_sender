<?php

namespace app\components\common;

use app\models\phoenix\SenderPhoto;
use app\models\phoenix\SenderPhotoNiche;
use Imagine\Image\Box;
use yii\base\Component;
use yii\db\ActiveRecord;
use yii\imagine\Image;

class SenderPhotoComponent extends Component
{
    public $directory = 'images/senderPhoto/';

    public function getUrl(ActiveRecord $model)
    {
        $photo = SenderPhoto::findOne(['id' => $model->photo]);

        if ($photo && is_file($this->getBasePath() . $photo->name)) {
            return $this->getBaseUrl() . $photo->name;
        }

        return null;
    }

    public function importFromDir(\SplFileInfo $dir, string $niche)
    {
        if (!$dir->isDir()) {
            throw new \Exception((string)$dir . ' is not a dir');
        }
        $handle = opendir($dir->getRealPath());
        if (!$handle) {
            throw new \Exception((string)$dir . ' is not readable');
        }
        while (false !== ($entry = readdir($handle))) {
            if (substr($entry, 0, 1) == '.') {
                continue;
            }
            $file = new \SplFileInfo($dir->getRealPath() . DIRECTORY_SEPARATOR . $entry);
            echo $file->getRealPath() . "\n";
            $this->importPhotoWithRandomBirthDate($file, $niche);
        }
    }

    private function importPhotoWithRandomBirthDate(\SplFileInfo $fileInfo, string $niche)
    {
        if (!$fileInfo->isFile() || !$fileInfo->isReadable()) {
            throw new \Exception((string)$fileInfo . ' is not readable or not exists');
        }
        $fileHash = md5_file($fileInfo->getRealPath());

        $newFileName = $this->getSubDir($fileHash) . substr($fileHash, 4);
        $newFile = new \SplFileInfo(\Yii::$app->basePath . '/web' . SenderPhoto::PHOTO_BASE_PATH . $newFileName);
        if (!is_dir($newFile->getPath())) {
            mkdir($newFile->getPath(), 0777, true);
        }

        rename($fileInfo->getRealPath(), $newFile->getPathname());
        $randomBirthDate = new \DateTime();
        $randomBirthDate->setTimestamp(rand(
            (new \DateTime('19 years ago'))->getTimestamp(),
            (new \DateTime('29 years ago'))->getTimestamp()
        ));

        $senderPhoto = new SenderPhoto();
        $senderPhoto->name = $newFileName;
        $senderPhoto->level = 0;
        $senderPhoto->birthdayYear = $randomBirthDate->format('Y');
        $senderPhoto->birthdayMonth = $randomBirthDate->format('m');
        $senderPhoto->birthdayDay = $randomBirthDate->format('d');
        $senderPhoto->save();
        if ($senderPhoto->id) {
            $senderPhotoNiche = new SenderPhotoNiche();
            $senderPhotoNiche->niche = $niche;
            $senderPhotoNiche->photoId = $senderPhoto->id;
            $senderPhotoNiche->save();
        }
    }

    public function reusedPhoto(SenderPhoto $senderPhoto)
    {
        $fileOld = $this->getBasePath() . $senderPhoto->name;
        if (is_file($fileOld)) {
            $size = getimagesize($fileOld);
            rename($fileOld, $fileOld . '.jpg');
            Image::getImagine()->open($fileOld . '.jpg')->thumbnail(new Box(
                $size[0] - 1,
                $size[1] - 1
            ))->save($fileOld . '.jpg', ['quality' => 90]);
            $hashPhoto = md5_file($fileOld . '.jpg');
            $subDir = $this->getSubDir($hashPhoto);
            if (!is_dir($this->getBasePath() . $subDir)) {
                mkdir($this->getBasePath() . $subDir, 0777, true);
            }
            rename($fileOld . '.jpg', $this->getBasePath() . $subDir . $hashPhoto);

            $senderPhoto->name = $subDir . $hashPhoto;
            $senderPhoto->isUsed = 0;
            if (!$senderPhoto->save()) {
                if (!$senderPhoto->validateAndMarkBadPhoto()) {
                    return false;
                }
                return true;
            }
        }

        return false;
    }

    public function importPhotoToDir(SenderPhoto $senderPhoto)
    {
        $subDir = $this->getSubDir($senderPhoto->name);
        $newFile = $subDir . $senderPhoto->name;
        if (!is_dir($this->getBasePath() . $subDir)) {
            mkdir($this->getBasePath() . $subDir, 0777, true);
        }
        rename($this->getBasePath() . $senderPhoto->name, $this->getBasePath() . $newFile);

        $senderPhoto->name = $newFile;
        $senderPhoto->save();
    }

    public function getSubDir($photoName)
    {
        return substr($photoName, 0, 2) . '/' . substr($photoName, 2, 2) . '/';
    }

    protected function renameFile($fileName)
    {
        return hash('md5', $fileName . microtime() . rand(0000, 9999));
    }

    public function getBasePath()
    {
        return \Yii::$app->getBasePath() . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->directory;
    }

    protected function getBaseUrl()
    {
        return \Yii::$app->getRequest()->getBaseUrl() . DIRECTORY_SEPARATOR . $this->directory;
    }
}
