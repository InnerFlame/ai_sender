<?php

namespace app\components\cleaner;

use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\IncomingMessage;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\RecipientSchema;
use yii\db\Query;

class RecipientDataCleanerComponent
{
    /** @var int $limit */
    private $limit = 300;

    public function clean(string $dateStart, string $dateEnd)
    {
        $recipientIds = $this->getRecipientIds($dateStart, $dateEnd);
        while ($recipientIds) {
            (new Query())->createCommand()
                ->delete(CommunicationSchedule::tableName(), ['recipientId' => $recipientIds])
                ->execute();

            (new Query())->createCommand()
                ->delete(IncomingMessage::tableName(), ['recipientId' => $recipientIds])
                ->execute();

            (new Query())->createCommand()
                ->delete(RecipientSchema::tableName(), ['recipientId' => $recipientIds])
                ->execute();

            (new Query())->createCommand()
                ->delete(RecipientProfile::tableName(), ['id' => $recipientIds])
                ->execute();

            sleep(5);

            $recipientIds = $this->getRecipientIds($dateStart, $dateEnd);
        }
    }

    private function getRecipientIds(string $dateStart, string $dateEnd)
    {
        $query = (new Query())->select('id')
            ->from(RecipientProfile::tableName())
            ->where(['>=', 'createdAt', $dateStart])
            ->andWhere(['<=', 'createdAt', $dateEnd])
            ->limit($this->limit);

        $recipientIds = $query->column();

        return $recipientIds;
    }
}
