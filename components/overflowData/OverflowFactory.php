<?php

namespace app\components\overflowData;

class OverflowFactory
{
    public function getOverflowList(array $overflowList = [], $dateStart, $dateEnd): array
    {
        $overflows = [];
        $builder   = new OverflowBuilder($dateStart, $dateEnd);
        foreach ($overflowList as $overflowName) {
            $overflows[] = $builder->buildOverflow($overflowName);
        }

        return $overflows;
    }
}
