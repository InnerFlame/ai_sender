<?php

namespace app\components\overflowData;

class OverflowData
{
    private $table;
    private $overflow;

    public function __construct(
        Table $table,
        Overflow $overflow
    )
    {
        $this->table    = $table;
        $this->overflow = $overflow;
    }

    public function process($dateStart)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $this->table->create();

            $dateEndScript = date('Y-m-d H:i:s', time() + 3600 * 24);
            while ($dateStart <= $dateEndScript) {
                $dateEnd = date('Y-m-d H:i:s', strtotime($dateStart) + 300);
                echo 'Processing from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL;
                $this->overflow->overflow($dateStart, $dateEnd);
                echo 'Have done process from ' . $dateStart . ' to ' . $dateEnd . PHP_EOL . PHP_EOL;
                $dateStart = date('Y-m-d H:i:s', strtotime($dateStart) + 300);
            }

            $this->table->rename();

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new \Exception($e);
        }
    }

    public function createPartition($from, $to)
    {
        $this->table->createPartition($from, $to);
    }

    public function dropPartition($from, $to)
    {
        $this->table->dropPartition($from, $to);
    }
}