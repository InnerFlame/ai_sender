<?php

namespace app\components\overflowData;

use app\components\overflowData\overflows\AbstractOverflow;

class Overflow
{
    private $overflowFactory;
    private $overflowList;

    public function __construct(
        OverflowFactory $overflowFactory,
        array $overflowList
    )
    {
        $this->overflowFactory = $overflowFactory;
        $this->overflowList    = $overflowList;
    }

    public function overflow($dateStart, $dateEnd)
    {
        $overflows = $this->overflowFactory->getOverflowList($this->overflowList, $dateStart, $dateEnd);

        if (!empty($overflows)) {
            /** @var AbstractOverflow $overflow */
            foreach ($overflows as $overflow) {
                $overflow->overflow();
            }
        }
    }
}