<?php

namespace app\components\overflowData\tables;

abstract class AbstractTable
{
    protected function makeCreatePartition($tableName)
    {
        $partitions = $this->makePartition(14, 14, $tableName);
        if (!empty($partitions)) {
            return sprintf('ALTER TABLE  ' . $tableName . ' PARTITION BY RANGE (UNIX_TIMESTAMP(createdAt))(%s)', implode(',', $partitions));
        }

        return null;
    }

    protected function makeAddPartition($from, $to, $tableName)
    {
        $partitions = $this->makePartition($from, $to, $tableName);
        if (!empty($partitions)) {
            return sprintf('ALTER TABLE  ' . $tableName . ' ADD PARTITION (%s)', implode(',', $partitions));
        }

        return null;
    }

    protected function makeDropPartition($from, $to, $tableName)
    {
        $partitions = [];
        for ($time = strtotime('now -' . $from . ' day'); $time < strtotime('now -'. $to .' day'); $time += 86400) {
            $partitionDate = date('Ymd', $time);
            if ($this->hasPartition($tableName, 'p' . $partitionDate)) {
                $partitions[]  = 'p' . $partitionDate;
            }
        }

        if (!empty($partitions)) {
            return sprintf('ALTER TABLE  ' . $tableName . ' DROP PARTITION %s', implode(',', $partitions));
        }

        return null;
    }

    private function makePartition($from, $to, $tableName)
    {
        $partitions = [];
        for ($time = strtotime('now -' . $from . ' day'); $time < strtotime('now +'. $to .' day'); $time += 86400) {
            $partitionDate = date('Ymd', $time);
            $partitionTime = strtotime(date('Y-m-d 23:59:59', $time));
            if ($this->hasPartition($tableName, 'p' . $partitionDate)) {
                continue;
            }

            $partitions[] = sprintf('PARTITION p%s VALUES LESS THAN (%s) ENGINE = InnoDB', $partitionDate, $partitionTime);
        }

        return $partitions;
    }

    private function hasPartition($tableName, $partitionName)
    {
        $dnName = $this->getDbName();
        $query  = \Yii::$app->db->createCommand('
                SELECT * FROM information_schema.partitions
                WHERE TABLE_SCHEMA=:tableSchema
                AND TABLE_NAME=:tableName
                AND PARTITION_NAME=:partitionName
            ')
            ->bindParam('tableSchema', $dnName)
            ->bindParam('tableName', $tableName)
            ->bindParam('partitionName', $partitionName);

        $result = $query->execute();
        if(empty($result)) {
            return false;
        }

        return true;
    }

    private function getDbName()
    {
        if (preg_match('/dbname=([^;]*)/', \Yii::$app->getDb()->dsn, $match)) {
            return $match[1];
        } else {
            return null;
        }
    }

    abstract public function create();

    abstract public function rename();

    abstract public function createPartition($from, $to);

    abstract public function dropPartition($from, $to);
}