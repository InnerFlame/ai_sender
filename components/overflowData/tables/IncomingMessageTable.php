<?php

namespace app\components\overflowData\tables;


use app\models\phoenix\IncomingMessage;

class IncomingMessageTable extends AbstractTable
{
    public function create()
    {
        if (!\Yii::$app->db->schema->getTableSchema(IncomingMessage::tableName() . 'New')) {
            $createTableSql = '
                CREATE TABLE IF NOT EXISTS `' . IncomingMessage::tableName() . 'New` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `senderId` int(11) NOT NULL,
                    `recipientId` int(11) NOT NULL,
                    `step` int(11) DEFAULT 1,
                    `isUsed` tinyint(1) DEFAULT 0,
                    `text` text,
                    `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    `subject` varchar(50) DEFAULT NULL,
                    `groupId` int(11) DEFAULT 0,
                    `schemaId` int(11) DEFAULT 0,
                    `messageId` varchar(32) DEFAULT NULL,
                    `messageType` varchar(7) DEFAULT NULL,
                    `platformFromSent` varchar(10) NOT NULL DEFAULT "notSet",
                    PRIMARY KEY (`id`,`createdAt`),
                    KEY `senderId` (`senderId`),
                    KEY `recipientId` (`recipientId`),
                    KEY `isUsed` (`isUsed`),
                    KEY `createdAt` (`createdAt`),
                    KEY `getStepAnswers` (`senderId`,`recipientId`),
                    KEY `schemaId` (`schemaId`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ';

            $createTableSql .= $this->makeCreatePartition(IncomingMessage::tableName() . 'New');

            \Yii::$app->db->createCommand($createTableSql)->execute();
        }
    }

    public function createPartition($from, $to)
    {
        $sql = $this->makeAddPartition($from, $to, IncomingMessage::tableName());
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function rename()
    {
        if (\Yii::$app->db->schema->getTableSchema(IncomingMessage::tableName() . 'New')) {
            \Yii::$app->db->createCommand('
                RENAME TABLE `' . IncomingMessage::tableName() . '` to `' . IncomingMessage::tableName() . 'Old`;
                RENAME TABLE `' . IncomingMessage::tableName() . 'New` to `' . IncomingMessage::tableName() . '`;
            ')->execute();
        }
    }

    public function dropPartition($from, $to)
    {
        $sql = $this->makeDropPartition($from, $to, IncomingMessage::tableName());
        \Yii::$app->db->createCommand($sql)->execute();
    }
}