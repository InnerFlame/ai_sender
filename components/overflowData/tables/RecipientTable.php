<?php

namespace app\components\overflowData\tables;

use app\models\phoenix\RecipientProfile;

class RecipientTable extends AbstractTable
{
    public function create()
    {
        if (!\Yii::$app->db->schema->getTableSchema(RecipientProfile::tableName() . 'New')) {
            $createTableSql = '
                CREATE TABLE `' . RecipientProfile::tableName() . '` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `remoteId` varchar(40) NOT NULL,
                    `isPaid` tinyint(1) NOT NULL,
                    `sexuality` int(2) DEFAULT 2,
                    `gender` tinyint(1) DEFAULT 1,
                    `activationTime` timestamp NOT NULL DEFAULT "0000-00-00 00:00:00",
                    `site` int(11) DEFAULT NULL,
                    `country` varchar(3) DEFAULT NULL,
                    `age` int(2) DEFAULT NULL,
                    `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    `online` tinyint(1) DEFAULT 0,
                    `lastOnlineDate` datetime DEFAULT "0000-00-00 00:00:00",
                    `state` varchar(4) DEFAULT NULL,
                    `city` varchar(50) DEFAULT NULL,
                    `citySearch` varchar(50) DEFAULT NULL,
                    `stateSearch` varchar(4) DEFAULT NULL,
                    `isWithPhoto` tinyint(1) DEFAULT 0,
                    `lastUsedPlatform` varchar(10) NOT NULL DEFAULT "notSet",
                    `source` varchar(32) DEFAULT NULL,
                    `locale` varchar(8) DEFAULT NULL,
                    `screenname` varchar(64) DEFAULT NULL,
                    `registrationPlatform` varchar(16) DEFAULT NULL,
                    `photoCount` tinyint(11) DEFAULT 0,
                    `onlineStatusExpireAt` timestamp NOT NULL DEFAULT "0000-00-00 00:00:00",
                    `updatedAt` timestamp NOT NULL DEFAULT "0000-00-00 00:00:00",
                    `birthday` timestamp NOT NULL DEFAULT "0000-00-00 00:00:00",
                    PRIMARY KEY (`id`,`createdAt`),
                    KEY `remoteId` (`remoteId`),
                    KEY `siteCountry` (`site`,`country`),
                    KEY `location` (`country`,`stateSearch`,`citySearch`),
                    KEY `createdAt` (`createdAt`),
                    KEY `activationTime` (`activationTime`),
                    KEY `lastOnlineDate` (`lastOnlineDate`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ';

            $createTableSql .= $this->makeCreatePartition(RecipientProfile::tableName() . 'New');

            \Yii::$app->db->createCommand($createTableSql)->execute();
        }
    }

    public function createPartition($from, $to)
    {
        $sql = $this->makeAddPartition($from, $to, RecipientProfile::tableName());
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function rename()
    {
        if (\Yii::$app->db->schema->getTableSchema(RecipientProfile::tableName() . 'New')) {
            \Yii::$app->db->createCommand('
                RENAME TABLE `' . RecipientProfile::tableName() . '` to `' . RecipientProfile::tableName() . 'Old`;
                RENAME TABLE `' . RecipientProfile::tableName() . 'New` to `' . RecipientProfile::tableName() . '`;
            ')->execute();
        }
    }

    public function dropPartition($from, $to)
    {
        $sql = $this->makeDropPartition($from, $to, RecipientProfile::tableName());
        \Yii::$app->db->createCommand($sql)->execute();
    }
}