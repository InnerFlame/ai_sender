<?php

namespace app\components\overflowData\tables;

use app\models\phoenix\RecipientSchema;

class RecipientSchemaTable extends AbstractTable
{
    public function create()
    {
        if (!\Yii::$app->db->schema->getTableSchema(RecipientSchema::tableName() . 'New')) {
            $createTableSql = '
                CREATE TABLE `' . RecipientSchema::tableName() . 'New` (
                    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                    `recipientId` int(11) NOT NULL,
                    `schemaId` int(11) NOT NULL,
                    `groupId` int(11) DEFAULT NULL,
                    `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    `restartNumber` int(11) NOT NULL,
                    `activatedAt` timestamp NOT NULL DEFAULT "0000-00-00 00:00:00",
                    `startedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    `isProcessed` tinyint(1) DEFAULT 0,
                    PRIMARY KEY (`id`,`createdAt`),
                    KEY `recipientSchema` (`recipientId`,`schemaId`),
                    KEY `createdAt` (`createdAt`),
                    KEY `isProcessed` (`isProcessed`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ';

            $createTableSql .= $this->makeCreatePartition(RecipientSchema::tableName() . 'New');

            \Yii::$app->db->createCommand($createTableSql)->execute();
        }
    }

    public function createPartition($from, $to)
    {
        $sql = $this->makeAddPartition($from, $to, RecipientSchema::tableName());
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function rename()
    {
        if (\Yii::$app->db->schema->getTableSchema(RecipientSchema::tableName() . 'New')) {
            \Yii::$app->db->createCommand('
                RENAME TABLE `' . RecipientSchema::tableName() . '` to `' . RecipientSchema::tableName() . 'Old`;
                RENAME TABLE `' . RecipientSchema::tableName() . 'New` to `' . RecipientSchema::tableName() . '`;
            ')->execute();
        }
    }

    public function dropPartition($from, $to)
    {
        $sql = $this->makeDropPartition($from, $to, RecipientSchema::tableName());
        \Yii::$app->db->createCommand($sql)->execute();
    }
}