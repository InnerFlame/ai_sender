<?php

namespace app\components\overflowData\tables;

use sender\communication\models\CommunicationSchedule;

class CommunicationTable extends AbstractTable
{
    public function create()
    {
        if (!\Yii::$app->db->schema->getTableSchema(CommunicationSchedule::tableName() . 'New')) {
            $createTableSql = '
                CREATE TABLE IF NOT EXISTS `' . CommunicationSchedule::tableName() . 'New` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `senderId` int(11) NOT NULL,
                    `recipientId` int(11) NOT NULL,
                    `messageId` int(11) NOT NULL,
                    `groupId` int(11) DEFAULT NULL,
                    `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    `sentAt` timestamp NOT NULL DEFAULT "0000-00-00 00:00:00",
                    `timeToSend` timestamp NOT NULL DEFAULT "0000-00-00 00:00:00",
                    `step` int(11) DEFAULT 0,
                    `sendStartedAt` timestamp NOT NULL DEFAULT "0000-00-00 00:00:00",
                    `isSenderBanned` tinyint(1) DEFAULT 0,
                    `attemptsCount` int(1) DEFAULT 0,
                    `schemaId` int(11) DEFAULT 0,
                    `restartNumber` tinyint(1) DEFAULT 0,
                    PRIMARY KEY (`id`,`createdAt`),
                    KEY `senderId` (`senderId`),
                    KEY `recipientId` (`recipientId`),
                    KEY `activeCommunication` (`sentAt`,`isSenderBanned`),
                    KEY `createdAt` (`createdAt`),
                    KEY `schemaId` (`schemaId`),
                    KEY `timeToSend` (`timeToSend`),
                    KEY `recipientIdsenderId` (`recipientId`,`senderId`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ';

            $createTableSql .= $this->makeCreatePartition(CommunicationSchedule::tableName() . 'New');

            \Yii::$app->db->createCommand($createTableSql)->execute();
        }
    }

    public function createPartition($from, $to)
    {
        $sql = $this->makeAddPartition($from, $to, CommunicationSchedule::tableName());
        \Yii::$app->db->createCommand($sql)->execute();
    }

    public function rename()
    {
        if (\Yii::$app->db->schema->getTableSchema(CommunicationSchedule::tableName() . 'New')) {
            \Yii::$app->db->createCommand('
                RENAME TABLE `' . CommunicationSchedule::tableName() . '` to `' . CommunicationSchedule::tableName() . 'Old`;
                RENAME TABLE `' . CommunicationSchedule::tableName() . 'New` to `' . CommunicationSchedule::tableName() . '`;
            ')->execute();
        }
    }

    public function dropPartition($from, $to)
    {
        $sql = $this->makeDropPartition($from, $to, CommunicationSchedule::tableName());
        \Yii::$app->db->createCommand($sql)->execute();
    }
}