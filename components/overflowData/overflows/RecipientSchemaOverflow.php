<?php

namespace app\components\overflowData\overflows;


use app\models\phoenix\RecipientSchema;

class RecipientSchemaOverflow extends AbstractOverflow
{
    private $fields = [
        'recipientId',
        'schemaId',
        'groupId',
        'createdAt',
        'activatedAt',
        'startedAt',
        'restartNumber',
        'isProcessed'
    ];

    public function overflow()
    {
        $this->offset     = 0;
        $recipientSchemas = $this->getData(RecipientSchema::tableName(), $this->fields);

        $this->saveData($recipientSchemas, RecipientSchema::tableName(), $this->fields);
    }
}