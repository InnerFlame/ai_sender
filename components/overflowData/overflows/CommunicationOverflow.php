<?php

namespace app\components\overflowData\overflows;

use sender\communication\models\CommunicationSchedule;

class CommunicationOverflow extends AbstractOverflow
{
    private $fields = [
        'senderId',
        'recipientId',
        'messageId',
        'groupId',
        'createdAt',
        'sentAt',
        'timeToSend',
        'step',
        'sendStartedAt',
        'isSenderBanned',
        'attemptsCount',
        'schemaId',
        'restartNumber'
    ];

    public function overflow()
    {
        $this->offset   = 0;
        $communications = $this->getData(CommunicationSchedule::tableName(), $this->fields);

        $this->saveData($communications, CommunicationSchedule::tableName(), $this->fields);
    }
}