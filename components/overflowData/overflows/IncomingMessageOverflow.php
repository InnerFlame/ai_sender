<?php

namespace app\components\overflowData\overflows;


use app\models\phoenix\IncomingMessage;

class IncomingMessageOverflow extends AbstractOverflow
{
    private $fields = [
        'senderId',
        'recipientId',
        'step',
        'isUsed',
        'text',
        'subject',
        'createdAt',
        'messageId',
        'groupId',
        'schemaId',
        'messageType',
        'platformFromSent',
    ];

    public function overflow()
    {
        $this->offset     = 0;
        $incomingMessages = $this->getData(IncomingMessage::tableName(), $this->fields);

        $this->saveData($incomingMessages, IncomingMessage::tableName(), $this->fields);
    }
}