<?php

namespace app\components\overflowData\overflows;

use app\models\phoenix\RecipientProfile;

class RecipientOverflow extends AbstractOverflow
{
    private $fields = [
        'id',
        'remoteId',
        'isPaid',
        'sexuality',
        'gender',
        'activationTime',
        'site',
        'country',
        'age',
        'createdAt',
        'online',
        'lastOnlineDate',
        'state',
        'city',
        'citySearch',
        'stateSearch',
        'isWithPhoto',
        'lastUsedPlatform',
        'source',
        'locale',
        'screenname',
        'registrationPlatform',
        'photoCount',
        'onlineStatusExpireAt',
        'updatedAt',
        'birthday',
    ];

    public function overflow()
    {
        $this->offset     = 0;
        $incomingMessages = $this->getData(RecipientProfile::tableName(), $this->fields);

        $this->saveData($incomingMessages, RecipientProfile::tableName(), $this->fields);
    }
}