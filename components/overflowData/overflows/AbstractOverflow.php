<?php

namespace app\components\overflowData\overflows;

use yii\db\Query;

abstract class AbstractOverflow
{
    protected $dateStart;
    protected $dateEnd;

    protected $limit  = 500;
    protected $offset = 0;

    public function __construct($dateStart, $dateEnd)
    {
        $this->dateStart = $dateStart;
        $this->dateEnd   = $dateEnd;
    }

    protected function getData($table, $fields)
    {
        echo PHP_EOL . $this->offset . '-' . $this->limit . PHP_EOL;
        $query  = (new Query())->select($fields)
            ->from($table)
            ->where(['>', 'createdAt', $this->dateStart])
            ->andWhere(['<=', 'createdAt', $this->dateEnd])
            ->offset($this->offset)
            ->limit($this->limit);

        $data = $query->all();

        return $data;
    }

    protected function saveData($data, $table, $fields)
    {
        while ($data) {
            \Yii::$app->db->createCommand()->batchInsert(
                $table . 'New',
                $fields,
                $data
            )->execute();

            sleep(2);

            $this->offset = $this->offset + $this->limit;
            $data         = $this->getData($table, $fields);
        }
    }

    abstract public function overflow();
}