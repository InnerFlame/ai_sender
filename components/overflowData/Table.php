<?php

namespace app\components\overflowData;

use app\components\overflowData\tables\AbstractTable;

class Table
{
    private $tableFactory;
    private $tableList;

    public function __construct(
        TableFactory $tableFactory,
        array $tableList
    )
    {
        $this->tableFactory = $tableFactory;
        $this->tableList    = $tableList;
    }

    public function create()
    {
        $tables = $this->tableFactory->getTableList($this->tableList);

        if (!empty($tables)) {
            /** @var AbstractTable $table */
            foreach ($tables as $table) {
                $table->create();
            }
        }
    }

    public function rename()
    {
        $tables = $this->tableFactory->getTableList($this->tableList);

        if (!empty($tables)) {
            /** @var AbstractTable $table */
            foreach ($tables as $table) {
                $table->rename();
            }
        }
    }

    public function createPartition($from, $to)
    {
        $tables = $this->tableFactory->getTableList($this->tableList);

        if (!empty($tables)) {
            /** @var AbstractTable $table */
            foreach ($tables as $table) {
                $table->createPartition($from, $to);
            }
        }
    }

    public function dropPartition($from, $to)
    {
        $tables = $this->tableFactory->getTableList($this->tableList);

        if (!empty($tables)) {
            /** @var AbstractTable $table */
            foreach ($tables as $table) {
                $table->dropPartition($from, $to);
            }
        }
    }
}