<?php

namespace app\components\overflowData;

class TableFactory
{
    public function getTableList($tableList)
    {
        $tables = [];
        foreach ($tableList as $table) {
            $table    = $this->getTablePath($table);
            $tables[] = new $table;
        }

        return $tables;
    }

    private function getTablePath($table): string
    {
        return __NAMESPACE__ . '\\tables\\' . ucfirst($table) . 'Table';
    }
}