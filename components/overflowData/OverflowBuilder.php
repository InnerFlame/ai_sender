<?php

namespace app\components\overflowData;

class OverflowBuilder
{
    private $dateStart;
    private $dateEnd;

    public function __construct($dateStart, $dateEnd)
    {
        $this->dateStart = $dateStart;
        $this->dateEnd   = $dateEnd;
    }

    public function buildOverflow($overflowName)
    {
        $action = $this->buildOverflowPath($overflowName);
        return new $action($this->dateStart, $this->dateEnd);
    }

    private function buildOverflowPath($overflowName): string
    {
        return __NAMESPACE__ . '\\overflows\\' . ucfirst($overflowName) . 'Overflow';
    }
}