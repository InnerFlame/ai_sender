<?php

namespace app\components\amqp;

use yii\base\Component;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

abstract class AmqpComponent extends Component
{
    /** @var AMQPStreamConnection */
    private $connection = null;
    private $channelIds = [];

    public $host;
    public $port;
    public $user;
    public $pass;
    public $vhost;

    public function init()
    {
        $this->connection = new AMQPStreamConnection(
            $this->host,
            $this->port,
            $this->user,
            $this->pass,
            $this->vhost
        );
    }

    /**
     * Publish events to queue
     *
     * @param string $queue - queue name
     * @param array $data - array of data
     * @param boolean $passive
     * @param boolean $durable
     * @param boolean $autoDelete
     * @param string $replyTo
     * @return integer - correlation id
     */
    protected function basicPublish($queue, $data, $passive = false, $durable = true, $autoDelete = false, $replyTo = '')
    {
        try {
            $exchange = $queue . '.exchange';

            if (empty($this->channelIds[$queue])) {
                $this->channelIds[$queue] = mt_rand(1, 65535);
            }
            $channelId = $this->channelIds[$queue];
            $channel = $this->connection->channel($channelId);
            $channel->queue_declare($queue, $passive, $durable, false, $autoDelete);
            $channel->exchange_declare($exchange, 'direct', false, true, $autoDelete);
            $channel->queue_bind($queue, $exchange);

            //Publish
            $correlationId = md5(microtime());
            $json = json_encode($data);

            $message = new AMQPMessage(
                $json,
                [
                    'content_type'   => 'text/plain',
                    'delivery_mode'  => AMQPMessage::DELIVERY_MODE_PERSISTENT,
                    'reply_to'       => $replyTo,
                    'correlation_id' => $correlationId,
                ]
            );
            $channel->basic_publish($message, $exchange);

            $this->afterPublish($channel);

            return $correlationId;
        } catch (\PhpAmqpLib\Exception\AMQPProtocolChannelException $e) {
            throw $e;
        } catch (\Exception $e) {
            \Yii::warning($e->getMessage());
        }

        return false;
    }

    public function basicBatchPublish($queue, $dataItems, $passive = false, $durable = true, $autoDelete = false)
    {
        try {
            $exchange = $queue . '.exchange';

            if (empty($this->channelIds[$queue])) {
                $this->channelIds[$queue] = mt_rand(1, 65535);
            }
            $channelId = $this->channelIds[$queue];
            $channel = $this->connection->channel($channelId);
            $channel->queue_declare($queue, $passive, $durable, false, $autoDelete);
            $channel->exchange_declare($exchange, 'direct', false, true, $autoDelete);
            $channel->queue_bind($queue, $exchange);

            foreach ($dataItems as $data) {
                //Publish
                $json = json_encode($data);

                $message = new AMQPMessage(
                    $json,
                    [
                        'content_type' => 'text/plain',
                        'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
                    ]
                );
                $channel->batch_basic_publish($message, $exchange);
            }

            $channel->publish_batch();

            $this->afterPublish($channel);

            return true;
        } catch (\PhpAmqpLib\Exception\AMQPProtocolChannelException $e) {
            throw $e;
        } catch (\Exception $e) {
            \Yii::warning($e->getMessage());
        }

        return false;
    }

    /**
     * @param AMQPMessage $amqpMessage
     */
    protected function getMessageData($amqpMessage)
    {
        if (empty($amqpMessage) || empty($amqpMessage->body)) {
            \Yii::warning('Empty message');
            return false;
        }

        $message = json_decode($amqpMessage->body, true);

        if (empty($message)) {
            \Yii::warning('Empty incoming data');
            return false;
        }

        return $message;
    }

    /**
     * @param AMQPMessage $amqpMessage
     */
    protected function ackMessage($amqpMessage)
    {
        $amqpMessage->delivery_info['channel']->basic_ack($amqpMessage->delivery_info['delivery_tag']);
    }

    /**
     * Consume events from needle queue
     *
     * @param string    $queue
     * @param callable  $consumerCallback
     * @param string    $exchange
     * @param string    $exchangeType
     * @param boolean   $durable
     * @param boolean   $noAck
     * @param boolean   $autoDelete
     * @return null
     */
    protected function basicConsume(
        $queue,
        $consumerCallback,
        $exchange = null,
        $exchangeType = null,
        $durable = true,
        $noAck = false,
        $autoDelete = false
    ) {
        //Consume
        $channel = $this->connection->channel(mt_rand(1, 65535));

        if ($queue == null) {
            list($queue, ,) = $channel->queue_declare($queue, false, $durable, false, $autoDelete);
        } else {
            $channel->queue_declare($queue, false, $durable, false, $autoDelete);
        }

        if (!empty($exchange)) {
            if ($exchange == 'presence.client') $autoDelete = false;
            $channel->exchange_declare($exchange, $exchangeType, false, true, $autoDelete);
            $channel->queue_bind($queue, $exchange);
        }
        $channel->basic_qos(null, 1, null);

        $channel->basic_consume($queue, '', false, $noAck, false, false, $consumerCallback);

        while (count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
    }

    /**
     * close all channel and rabbitMQ connection
     */
    public function closeConnection()
    {
        $this->connection->close();
    }
}