<?php

namespace app\components\amqp;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

class PresenceAmqpComponent extends AmqpComponent
{
    /**
     * Listen to the presence events and process them in callback
     *
     * @param callable $consumerCallback - callback to process events
     * @return null
     */
    public function consumePresenceEvents($consumerCallback)
    {
        $callback = $this->getCallback($consumerCallback);

        $durable    = false;
        $noAck      = true;

        $this->basicConsume(
            'presence.client',
            $callback,
            'presence.client',
            'x-presence',
            $durable,
            $noAck,
            true
        );
    }

    /**
     * @param callable $consumerCallback - callback to process events
     * @return null
     */
    public function consumePresenceEventsForSynchronizer($consumerCallback)
    {
        $callback = $this->getCallback($consumerCallback);

        $durable    = false;
        $noAck      = true;
        $autoDelete = true;

        $this->basicConsume(
            null,
            $callback,
            'presence.client',
            'x-presence',
            $durable,
            $noAck,
            $autoDelete
        );
    }

    /**
     * @param AMQPChannel $channel
     */
    protected function afterPublish($channel)
    {
        //need to close channels to prevent troubles, due to often controllers die;
        $channel->close();
    }

    /**
     * Build callback
     *
     * @param callable $consumerCallback - callback to process events
     * @return callable - callback
     */
    private function getCallback($consumerCallback)
    {
        $callback = function($amqpMessage) use ($consumerCallback) {
            $message = $this->getPresenceEventData($amqpMessage);

            $channel = $amqpMessage->delivery_info['channel'];

            if (empty($message)) {
                return false;
            }

            return call_user_func($consumerCallback, $message, $channel);
        };

        return $callback;
    }

    /**
     * @param AMQPMessage $amqpMessage
     */
    private function getPresenceEventData($amqpMessage)
    {
        if (empty($amqpMessage)) {
            return false;
        }

        $eventData = $amqpMessage->get_properties()['application_headers']->getNativeData();

        if (empty($eventData) || empty($eventData['action'])) {
            \Yii::warning('Empty presence data');
            return false;
        }

        return $eventData;
    }
}