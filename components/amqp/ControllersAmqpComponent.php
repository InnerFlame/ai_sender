<?php

namespace app\components\amqp;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

class ControllersAmqpComponent extends AmqpComponent
{
    /**
     * Publish events to queue
     *
     * @param string $queue - queue name
     * @param array $data - array of data
     * @return integer - correlation id
     */
    public function publishEvent($queue, $data, $replyTo)
    {
        $passive    = true;
        $durable    = false;
        $autoDelete = true;

        return parent::basicPublish($queue, $data, $passive, $durable, $autoDelete, $replyTo);
    }

    /**
     * Listen to the queue and process messages in callback
     *
     * @param null $queue
     * @param callable $consumerCallback - callback to process events
     * @return null
     */
    public function consumeEvents($queue, $consumerCallback)
    {
        $callback = $this->getCallback($consumerCallback);

        $durable    = false;
        $noAck      = true;

        $this->basicConsume(
            $queue,
            $callback,
            $queue . '.exchange',
            'direct',
            $durable,
            $noAck
        );
    }

    /**
     * @param AMQPChannel $channel
     */
    protected function afterPublish($channel)
    {
        //need to close channels to prevent troubles, due to often controllers die;
        $channel->close();
    }

    /**
     * Build callback
     *
     * @param callable $consumerCallback - callback to process events
     * @return callable - callback
     */
    private function getCallback($consumerCallback)
    {
        $callback = function($amqpMessage) use ($consumerCallback) {
            $message = $this->getMessageData($amqpMessage);
            if (empty($message)) {
                return false;
            }

            $correlationId = $this->getCorrelationId($amqpMessage);

            return call_user_func($consumerCallback, $message, $correlationId);
        };

        return $callback;
    }

    /**
     * @param AMQPMessage $amqpMessage
     */
    private function getCorrelationId($amqpMessage)
    {
        $correlationId = null;
        if ($amqpMessage->has('correlation_id')) {
            $correlationId = $amqpMessage->get('correlation_id');
        }

        return $correlationId;
    }
}