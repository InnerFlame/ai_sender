<?php

namespace app\components\amqp;

use PhpAmqpLib\Channel\AMQPChannel;

class InternalAmqpComponent extends AmqpComponent
{
    public $prefix;

    /**
     * Publish events to queue
     *
     * @param string $queue - queue name
     * @param array $data - array of data
     * @return integer - correlation id
     */
    public function publishEvent($queue, $data)
    {
        $queue = $this->prefix . $queue;

        return parent::basicPublish($queue, $data);
    }

    /**
     * Publish multiple events to queue
     *
     * @param string $queue - queue name
     * @param array $data - array of data
     * @return integer - correlation id
     */
    public function batchPublish($queue, $data)
    {
        $queue = $this->prefix . $queue;

        return parent::basicBatchPublish($queue, $data);
    }

    /**
     * @param AMQPChannel $channel
     */
    protected function afterPublish($channel)
    {
        //don`t need to do an action
    }

    /**
     * Listen to the queue and process messages in callback
     *
     * @param null $queue
     * @param callable $consumerCallback - callback to process events
     * @return null
     */
    public function consumeEvents($queue, $consumerCallback)
    {
        $callback = $this->getCallback($consumerCallback);

        $queue = $this->prefix . $queue;

        $this->basicConsume(
            $queue,
            $callback
        );
    }

    /**
     * Build callback
     *
     * @param callable $consumerCallback - callback to process events
     * @return callable - callback
     */
    private function getCallback($consumerCallback)
    {
        $callback = function($amqpMessage) use ($consumerCallback) {
            $this->ackMessage($amqpMessage);
            $message = $this->getMessageData($amqpMessage);

            if (empty($message)) {
                return false;
            }

            return call_user_func($consumerCallback, $message);
        };

        return $callback;
    }
}