<?php

namespace app\components\phoenix\communication;

use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\ProfileMessage;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\StepMessage;
use yii\base\Component;

class MessageComponent extends Component
{
    public function getMessageBody(CommunicationSchedule $communication)
    {
        if ($communication->step) {
            $message = StepMessage::findOne(['id' => $communication->messageId]);
        } else {
            $message = ProfileMessage::findOne(['id' => $communication->messageId]);
        }

        $sender          = SenderProfile::findOne($communication->senderId);
        $transformedText = \Yii::$app->textTransformation->addRandomCharacters($message->body, $sender);
        $language        = $message->language ?? null;

        for($i = 0; $i<=15; $i++) {
            if (\Yii::$app->checkMessage->isWhiteMessageForSend($transformedText, $language)) {
                return $transformedText;
            }

            $transformedText = \Yii::$app->textTransformation->addRandomCharacters($message->body, $sender);
        }

        return $transformedText;
    }
}
