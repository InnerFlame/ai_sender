<?php

namespace app\components\phoenix\communication;

use app\models\phoenix\ExcludedSite;
use app\models\phoenix\RecipientSchema;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\SendRule;
use app\models\phoenix\Schema;
use app\models\phoenix\Group;
use yii\db\Query;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class CommSchedulerComponent extends Component
{
    private $sendingSchedule        = [];
    private $limitInsert            = 50;
    private $maxRestartForNoActive  = 30;

    //TODO: need posibility to process by multiple workers
    public function createSchedule()
    {
        $recipientSchemas = $this->getReadyRecipientSchemas(1000);
        foreach ($recipientSchemas as $recipientSchema) {
            $this->processRecipientSchema($recipientSchema);
            $this->insertPartEntities();
        }

        $this->saveSendingSchedule();
    }

    public function getReadyRecipientSchemas($limit)
    {
        $readySchemas = (new Query())
            ->select('s.*')
            ->from(RecipientSchema::tableName() . ' as s')
            ->innerJoin(RecipientProfile::tableName() . ' as r', 'r.id=s.recipientId')
            ->where([
                's.isProcessed' => false,
                'r.isExcludeByTopic' => 0,
            ])
            ->limit($limit)
            ->all();

        return $readySchemas;
    }

    public function repairMissedRecipientSchema($recipient)
    {
        $recipientSchemas = RecipientSchema::find()
            ->where(['recipientId' => $recipient['id']])
            ->asArray()
            ->all();

        if (empty($recipientSchemas)) {
            $this->addInitialRecipientSchemas($recipient);
        }
    }

    public function addInitialRecipientSchemas($recipient)
    {
        if ($this->isExclude($recipient)) {
            return;
        }

        $recipientProject = RecipientProfile::getProjectFromSiteHash($recipient['site']);

        $query = (new Query())
            ->select('sch.id as schemaId, g.id as groupId, sch.splitName, sch.splitGroup')
            ->from(SendRule::tableName() . ' as r')
            ->innerJoin(Schema::tableName() . ' as sch', 'r.schemaId=sch.id')
            ->innerJoin(Group::tableName() . ' as g', 'sch.id=g.schemaId')
            ->where([
                'r.country'     => $recipient['country'],
                'sch.gender'    => $recipient['gender'],
                'r.isActive'    => 1,
                'g.isInitial'   => 1,
                'sch.isPaid'    => $recipient['isPaid'],
            ])
            ->andWhere(['>=', 'sch.ageTo', $recipient['age']])
            ->andWhere(['<=', 'sch.ageFrom', $recipient['age']]);

        if (!empty($recipientProject)) {
            $query->andWhere([
                'or',
                ['and', 'r.site IS NULL', 'r.project IS NULL'],
                ['r.site' => $recipient['site']],
                ['r.project' => $recipientProject],
            ]);
        } else {
            $query->andWhere([
                'or',
                ['and', 'r.site IS NULL', 'r.project IS NULL'],
                ['r.site' => $recipient['site']],
            ]);
        }

        $groups = $query->all();
        if (empty($groups)) {
            return;
        }

        foreach ($groups as $group) {
            if (!empty($group['splitName'])) {
                $userSplitGroup = \Yii::$container->get('splitSystem')
                    ->getSplitResult($recipient->attributes, $group['splitName']);
                if ($userSplitGroup != $group['splitGroup']) {
                    continue;
                }
            }

            $recipientSchema = new RecipientSchema();
            $recipientSchema->recipientId   = $recipient['id'];
            $recipientSchema->schemaId      = $group['schemaId'];
            $recipientSchema->groupId       = $group['groupId'];
            $recipientSchema->isProcessed   = false;
            $recipientSchema->save();
        }
    }

    private function isExclude($recipient): bool
    {
        $country = $recipient->country;
        $site    = $recipient->site;
        $result  = \Yii::$app->db->cache(function ($db) use ($country, $site) {
            /** @var \yii\db\Connection $db */
            $result = $db->createCommand('
                SELECT id FROM `'. ExcludedSite::tableName() .'`
                WHERE `site`=:recipientSite
                AND (
                    `country`=:recipientCountry
                    OR `country` IS NULL
                )
                limit 1;
            ')
            ->bindParam('recipientSite', $site)
            ->bindParam('recipientCountry', $country)
            ->cache(30)->execute();

            return $result;
        });

        if (empty($result)) {
            return false;
        }

        return true;
    }

    public function setNextGroupForRecipientSchema($recipientId, $schemaId)
    {
        $recipientSchema = RecipientSchema::find()
            ->where([
                'recipientId' => $recipientId,
                'schemaId' => $schemaId,
            ])
            ->asArray()
            ->one();

        if ($recipientSchema['isProcessed'] != true) {
            return false;
        }

        $notSentCommunications = CommunicationSchedule::find()
            ->where([
                'groupId'  => $recipientSchema['groupId'],
                'recipientId' => $recipientSchema['recipientId'],
                'sendStartedAt' => '0000-00-00 00:00:00',
            ])
            ->asArray()
            ->all();

        //don`t update group if current still not sent
        if (!empty($notSentCommunications)) {
            return false;
        }

        $groupId = $this->getNextGroupId($recipientSchema);
        if (!empty($groupId)) {
            \Yii::$app->db->createCommand()->update(
                RecipientSchema::tableName(),
                [
                    'groupId' => $groupId,
                    'isProcessed' => false,
                ],
                [
                    'id' => $recipientSchema['id'],
                ]
            )->execute();
        }
    }

    /**
     * @param $recipientSchema
     */
    private function processRecipientSchema($recipientSchema)
    {
        $group = Group::find()->where(['id' => $recipientSchema['groupId']])->asArray()->one();

        //TODO: remove kostilation after fix
        if (empty($group)) {
            $this->setRecipientSchemaProcessed($recipientSchema);
            return;
        }

        $dateStart  = strtotime($recipientSchema['startedAt']) + $group['startTimeOffset'];
        $dateEnd    = strtotime($recipientSchema['startedAt']) + $group['endTimeOffset'];
        $datePeriod = $dateEnd - $dateStart;
        $datePart   = $datePeriod / $group['messageCount'];
        for ($i = 0; $i < $group['messageCount']; $i++) {
            $timeToSend = date('Y-m-d H:i:s', $dateStart + rand(0, (int)$datePart));
            $dateStart += $datePart;

            $this->pushSchedule([
                $recipientSchema['recipientId'],
                $recipientSchema['groupId'],
                $recipientSchema['schemaId'],
                $timeToSend,
                $recipientSchema['restartNumber'],
            ]);
        }

        $this->setRecipientSchemaProcessed($recipientSchema);
    }

    private function getNextGroupId($recipientSchema)
    {
        $currentGroup = Group::find()->where(['id' => $recipientSchema['groupId']])->asArray()->one();

        $query = (new Query())
            ->select('id')
            ->from(Group::tableName())
            ->where(['schemaId' => $recipientSchema['schemaId']])
            ->andWhere(['>', 'startTimeOffset', $currentGroup['startTimeOffset']])
            ->orderBy('startTimeOffset')
            ->limit(1);

        return $query->scalar();
    }

    /**
     * @param $recipient
     * @return bool
     */
    public function restartReadySchemas($recipient)
    {
        $recipientSchemas = RecipientSchema::find()
            ->where(['recipientId' => $recipient['id']])
            ->asArray()
            ->all();
        if (!$this->canRestart($recipient, $recipientSchemas)) {
            return false;
        }

        foreach ($recipientSchemas as $recipientSchema) {
            $schema = Schema::find()->where(['id' => $recipientSchema['schemaId']])->asArray()->one();
            if ((strtotime($recipientSchema['startedAt']) + $schema['timeToRestart']) < time()
                && $recipientSchema['restartNumber'] < $schema['restartCount']
                && $schema['isPaid'] == $recipient['isPaid']
            ) {
                $initialGroup = $this->getInitialGroupBySchema($recipientSchema['schemaId']);

                \Yii::$app->db->createCommand()->update(
                    RecipientSchema::tableName(),
                    [
                        'startedAt'     => date('Y-m-d H:i:s'),
                        'restartNumber' => $recipientSchema['restartNumber'] + 1,
                        'groupId'       => $initialGroup['id'],
                        'isProcessed'   => false,
                    ],
                    ['id'   => $recipientSchema['id']]
                )->execute();
            }
        }

        return true;
    }

    private function canRestart($recipient, $recipientSchemas)
    {
        if (empty($recipientSchemas)) {
            return false;
        }
        if ($recipient['online'] == 0) {
            return false;
        }
        if (in_array($recipient['lastUsedPlatform'], [RecipientProfile::IOS_APP, RecipientProfile::ANDROID_APP])) {
            return false;
        }
        if ($recipient['activationTime'] == '0000-00-00 00:00:00') {
            $maxRestart = max(ArrayHelper::getColumn($recipientSchemas, 'restartNumber'));
            if ($maxRestart >= $this->maxRestartForNoActive) {
                return false;
            }
        }

        return true;
    }

    private function getInitialGroupBySchema($schemaId)
    {
        $initialGroup = Group::find()
            ->where([
                'schemaId'  => $schemaId,
                'isInitial' => 1,
            ])
            ->asArray()
            ->one();

        return $initialGroup;
    }

    private function setRecipientSchemaProcessed($recipientSchema)
    {
        \Yii::$app->db->createCommand()->update(
            RecipientSchema::tableName(),
            ['isProcessed' => true],
            ['id' => $recipientSchema['id']]
        )->execute();
    }

    /**
     * @throws \yii\db\Exception
     */
    protected function saveSendingSchedule()
    {
        if ($this->sendingSchedule) {
            \Yii::$app->db->createCommand()->batchInsert(
                CommunicationSchedule::tableName(),
                ['recipientId', 'groupId', 'schemaId', 'timeToSend', 'restartNumber'],
                $this->sendingSchedule
            )->execute();

            $this->sendingSchedule = [];
        }
    }

    private function pushSchedule($data)
    {
        $this->sendingSchedule[] = $data;
    }

    private function insertPartEntities()
    {
        if (count($this->sendingSchedule) >= $this->limitInsert) {
            $this->saveSendingSchedule();
        }
    }
}
