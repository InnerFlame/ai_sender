<?php

namespace app\components\phoenix\communication;

use app\config\Constant;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\StepMessage;
use linslin\yii2\curl\Curl;
use sender\logger\components\Log;
use yii\base\Component;

class EvaMessageComponent extends Component
{
    /** @var Log */
    private $logger;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->logger = \Yii::$container->get('logger', Log::EVA_DEFAULT_CONTEXT);
    }

    public function getEvaStep(
        RecipientProfile $recipient,
        SenderProfile $sender,
        array $data = [],
        string $logMessage = ''
    ) {
        if ($recipient->country == 'ITA') {
            $url = Constant::EVA_DOCKER_URL_API;
        } else {
            $url = Constant::EVA_URL_API;
        }

        return $this->getMessage($recipient, $sender, $url, $logMessage, 'stepByStep', $data);
    }

    public function getEvaPlaceholder(
        RecipientProfile $recipient,
        SenderProfile $sender,
        array $data = [],
        string $logMessage = ''
    ) {
        if ($recipient->country == 'ITA') {
            $url = Constant::EVA_DOCKER_URL_PLACEHOLDER_API;
        } else {
            $url = Constant::EVA_URL_PLACEHOLDER_API;
        }

        return $this->getMessage($recipient, $sender, $url, $logMessage, 'placeholder', $data);
    }

    public function getZeroPlaceholder(
        RecipientProfile $recipient,
        SenderProfile $sender,
        array $data = [],
        string $logMessage = ''
    ) {
        $url = Constant::EVA_URL_ZERO_PLACEHOLDER_API;

        return $this->getMessage($recipient, $sender, $url, $logMessage, 'zeroPlaceholder', $data);
    }

    public function getFirstRestartPlaceholder(
        RecipientProfile $recipient,
        SenderProfile $sender,
        array $data = [],
        string $logMessage = ''
    ) {
        $url = Constant::EVA_URL_ZERO_PLACEHOLDER_API;

        return $this->getMessage($recipient, $sender, $url, $logMessage, 'firstRestart', $data);
    }

    private function getMessage(
        RecipientProfile $recipient,
        SenderProfile $sender,
        string $apiUrl,
        string $logMessage,
        string $type,
        array $data = []
    ) {
        $query    = http_build_query($data);
        $curl     = new Curl();
        $dataJson =  $curl->setOption(CURLOPT_POSTFIELDS, $query)
            ->setOption(CURLOPT_CONNECTTIMEOUT, 30)
            ->setOption(CURLOPT_TIMEOUT, 30)
            ->setOption(CURLOPT_SSL_VERIFYHOST, 0)
            ->setOption(CURLOPT_SSL_VERIFYPEER, 0)
            ->post($apiUrl);

        $data = json_decode($dataJson);

        $this->saveToLog($recipient, $sender, $logMessage, $type, $dataJson);

        if (!empty($data->code) && $data->code == 200) {
            if (!empty($data->messages)) {
                return $data;
            }
        }

        return false;
    }

    /**
     * @param object $data
     * @return bool|int
     */
    public function getStepMessage($data)
    {
        if (empty($data->message)) {
            return false;
        }

        $hash        = md5($data->message);
        $stepMessage = StepMessage::findOne(['hash' => $hash]);

        if ($stepMessage) {
            if ($data->hasPlaceholder != $stepMessage->hasPlaceholder) {
                $stepMessage->hasPlaceholder = $data->hasPlaceholder;
                $stepMessage->save();
            }

            return $stepMessage->id;
        }

        $stepMessage                 = new StepMessage();
        $stepMessage->body           = $data->message;
        $stepMessage->hash           = $hash;
        $stepMessage->hasPlaceholder = $data->hasPlaceholder;
        $stepMessage->save();

        return $stepMessage->id;
    }

    public function saveToLog(
        RecipientProfile $recipient,
        SenderProfile $sender,
        string $logMessage,
        string $type,
        string $response
    ) {
        $this->logger->info('Eva message', [
            'evaMessage' => $logMessage,
            'recipientId' => $recipient->id,
            'recipientOriginId' => $recipient->remoteId,
            'country' => $recipient->country,
            'senderId' => $sender->id,
            'senderOriginId' => $sender->originId,
            'site' => $sender->site,
            'type' => $type,
            'response' => $response,
        ]);
    }
}
