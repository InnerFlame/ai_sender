<?php

namespace app\components\phoenix\communication;

use app\models\phoenix\PrepaidRecipients;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\IncomingMessage;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Container;
use yii\base\Component;
use yii\db\Query;

class StepByStepSchedulerComponent extends Component
{
    const QUEUE_SAVE_MESSAGE = 'saveIncomingMessage';

    /** @var \sender\split\components\SplitSystem */
    protected $splitSystem;
    /** @var \app\components\common\IdEncoder */
    protected $idEncoderComponent;

    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->splitSystem        = \Yii::$container->get('splitSystem');
        $this->idEncoderComponent = \Yii::$container->get('idEncoder');
    }

    public function processEvaMessages()
    {
        \Yii::$app->internalAmqp->consumeEvents('incomingMessages', [$this, 'evaMessageHandler']);
    }

    public function evaMessageHandler(array $message)
    {
        $recipient  = RecipientProfile::findOne($message['recipientId']);
        if (empty($recipient)) {
            return false;
        }

        if (!empty($recipient->isExcludeByTopic)) {
            return false;
        }

        $evaAnswer = $this->getEvaData($message);
        if (empty($evaAnswer)) {
            return false;
        }

        $timeToSend = $this->getTimeToSend($message);
        foreach ($evaAnswer->messages as $item) {
            if (!empty($item->isExclude)) {
                $recipient->isExcludeByTopic = 1;
                $recipient->save();
                return false;
            }

            $messageId = \Yii::$app->evaMessage->getStepMessage($item);

            if ($messageId) {
                $sender = SenderProfile::findOne(['id' => $message['senderId']]);

                if (empty($sender)) {
                    continue;
                }

                $scheduleItem                   = new CommunicationSchedule();
                $scheduleItem->senderId         = $message['senderId'];
                $scheduleItem->recipientId      = $message['recipientId'];
                $scheduleItem->step             = $message['step'];
                $scheduleItem->groupId          = $message['groupId'];
                $scheduleItem->schemaId         = $message['schemaId'];
                $scheduleItem->messageId        = $messageId;
                $scheduleItem->timeToSend       = date(
                    'Y-m-d H:i:s',
                    strtotime($timeToSend) + $item->delay
                );
                $scheduleItem->isSenderBanned   = ($sender->bannedAt == '0000-00-00 00:00:00') ? 0 : 1;
                $scheduleItem->save();
            }
        }

        \Yii::$app->db->createCommand()->update(
            IncomingMessage::tableName(),
            ['isUsed' => 1],
            ['id' => $message['id']]
        )->execute();
    }

    private function getTimeToSend(array $message)
    {
        $query = (new Query())
            ->select('max(timeToSend)')
            ->from(CommunicationSchedule::tableName())
            ->where([
                'senderId'    => $message['senderId'],
                'recipientId' => $message['recipientId'],
                'groupId'     => $message['groupId'],
                'schemaId'    => $message['schemaId'],
            ]);

        $date = $query->scalar();
        if (empty($date) || $date < $message['createdAt']) {
            $date = $message['createdAt'];
        }

        return $date;
    }

    public function processIncomingMessages()
    {
        \Yii::$app->internalAmqp->consumeEvents(self::QUEUE_SAVE_MESSAGE, [$this, 'saveStepByStepAnswerHandler']);
    }

    /**
     * save incoming message
     */
    public function saveStepByStepAnswerHandler(array $message)
    {
        if (empty($message['msg']['msgType'])
            || $message['msg']['msgType'] != 'chat'
            || strpos($message['id'], 'phoenix') === false

        ) {
            return false;
        }

        $target = explode('.', $message['id']);

        $senderId = $target[1];
        $message  = $message['msg'];

        $sender = SenderProfile::findOne($senderId);

        if ($sender->userType == SenderProfile::TYPE_SENDER) {
            $recipient = RecipientProfile::findOne(['remoteId' => $message['fromId']]);

            if ($recipient) {
                $communication = CommunicationSchedule::findOne([
                    'senderId'    => $senderId,
                    'recipientId' => $recipient->id,
                    'step'        => 0
                ]);

                $schemaId = 0;
                $groupId  = 0;

                if ($communication) {
                    $schemaId = $communication->schemaId;
                    $groupId  = $communication->groupId;
                }

                if ($recipient->activationTime == '0000-00-00 00:00:00') {
                    $recipient->activationTime = date('Y-m-d H:i:s');
                }

                $recipient->online = 1;
                $recipient->save();

                $this->saveIncomingMessage($recipient, $senderId, $message, $groupId, $schemaId);
            }

            if (\Yii::$container->get('interactionRouter')->isSeparatedInteraction($sender)) {
                Container::updateLastTimeActivity($target[0] . $senderId, $target[0]);
            }
        }
    }

    public function saveIncomingMessage(
        RecipientProfile $recipient,
        int $senderId,
        array $message,
        int $groupId = 0,
        int $schemaId = 0,
        string $messageType = 'online'
    ) {
        $internalMessage                   = new IncomingMessage();
        $internalMessage->senderId         = $senderId;
        $internalMessage->recipientId      = $recipient->id;
        $internalMessage->text             = $message['text'];
        $internalMessage->subject          = $message['subject'];
        $internalMessage->messageId        = $message['messageId'];
        $internalMessage->groupId          = $groupId;
        $internalMessage->schemaId         = $schemaId;
        $internalMessage->messageType      = $messageType;
        $internalMessage->platformFromSent = $recipient->lastUsedPlatform ?? RecipientProfile::NOT_SET;
        $internalMessage->createdAt        = date('Y-m-d H:i:s');

        $data = $this->getData($internalMessage);

        $internalMessage->step = $data['step'];

        if (isset($data['isUsed'])) {
            $internalMessage->isUsed = $data['isUsed'];
        }

        $internalMessage->save();
        $internalMessage->refresh();

        $dataForEve = [
            'id'          => $internalMessage->id,
            'senderId'    => $internalMessage->senderId,
            'recipientId' => $internalMessage->recipientId,
            'text'        => $internalMessage->text,
            'schemaId'    => $internalMessage->schemaId,
            'groupId'     => $internalMessage->groupId,
            'step'        => $internalMessage->step,
            'createdAt'   => $internalMessage->createdAt,
        ];

        \Yii::$app->internalAmqp->publishEvent('incomingMessages', $dataForEve);

        return true;
    }

    private function getEvaData(array $incomingMessage)
    {
        $recipient  = RecipientProfile::findOne($incomingMessage['recipientId']);
        $sender = SenderProfile::findOne($incomingMessage['senderId']);

        if (empty($recipient) || empty($sender)) {
            return false;
        }


        if (in_array($recipient->lastUsedPlatform, [RecipientProfile::ANDROID_APP, RecipientProfile::IOS_APP])) {
            return false;
        }

        $isPrepaid = (bool) PrepaidRecipients::findOne(['recipientId' => $recipient->id]);

        $postData  = [
            'from'             => $recipient->remoteId,
            'to'               => $sender->originId,
            'site'             => \Yii::$app->yaml->parseByKey('site', $recipient->site)['hash'],
            'country'          => $recipient->country,
            'Country'          => $recipient->country,
            'paid'             => $recipient->isPaid,
            'locale'           => $recipient->locale,
            'prepaid'          => $isPrepaid,
            'senderLocale'     => $sender->language,
            'traff_src_from'   => $recipient->source,
            'traff_src_to'     => 'Aff Internal',
            'action_way_to'    => 'external',
            'platform'         => $recipient->lastUsedPlatform,
            'text'             => $incomingMessage['text'],
            'sender_country'   => $sender->country,
            'sender_city'      => $sender->location ?? $sender->city,
            'sender_birthday'  => $sender->birthday,
            'time_offset'      => $sender->timeOffset,
            'multi_profile_id' => $recipient->multiProfileId ?? '',
            'splits'           => $this->splitSystem->getTotalSplitsResult($recipient->attributes),
            'profile_short_id' => $this->idEncoderComponent->getRecipientShortId($recipient),
        ];

        return \Yii::$app->evaMessage->getEvaStep($recipient, $sender, $postData, $incomingMessage['text']);
    }

    /**
     * return data step and isUsed fields
     * @param IncomingMessage $internalMessage
     * @return mixed
     */
    protected function getData(IncomingMessage $internalMessage)
    {
        $messageData = $this->getLastStep($internalMessage->senderId, $internalMessage->recipientId);

        $data['step'] = 1;

        if ($messageData) {
            if ($messageData['isUsed'] == 0) {
                $data['step'] = $messageData['step'];
            } else {
                $data = $this->getDataIfIsUsed($internalMessage, $messageData['step']);
            }
        }

        return $data;
    }

    /**
     * set data step and isUsed fields if this step is used
     * @param IncomingMessage $internalMessage
     * @param $step
     * @return mixed
     */
    protected function getDataIfIsUsed(IncomingMessage $internalMessage, int $step)
    {
        $result = $this->getMessageInSchedule($step, $internalMessage->senderId, $internalMessage->recipientId);

        if ($result) {
            $data['isUsed'] = 1;
            $data['step']   = $step;
        } else {
            $data['step'] = $step + 1;
        }

        return $data;
    }

    protected function getMessageInSchedule(int $step, int $senderId, int $recipientId)
    {
        $query = new Query();

        return $query->select('id')
            ->from(CommunicationSchedule::tableName())
            ->where(['senderId' => $senderId])
            ->andWhere(['recipientId' => $recipientId])
            ->andWhere(['step' => $step])
            ->andWhere(['sentAt' => '0000-00-00 00:00:00'])
            ->scalar();
    }


    protected function getLastStep(int $senderId, int $recipientId)
    {
        $query = new Query();

        return $query->select('max(step) as step, min(isUsed) as isUsed')
            ->from(IncomingMessage::tableName())
            ->where(['senderId'       => $senderId])
            ->andWhere(['recipientId' => $recipientId])
            ->groupBy('senderId, recipientId')
            ->one();
    }
}
