<?php

namespace app\components\phoenix\search;

use app\commands\ZabbixController;
use app\models\phoenix\RecipientProfile;
use sender\monitoring\components\ZabbixMemcachedConnection;

class ApiSearchRpc
{
    const QUEUE = 'rpcApiSearchResponse';

    private $recipientSaver;
    private $recipientChecker;
    private $zabbixMemcachedConnection;

    public function __construct(
        RecipientSaver $recipientSaver,
        RecipientChecker $recipientChecker,
        ZabbixMemcachedConnection $zabbixMemcachedConnection
    )
    {
        $this->recipientSaver   = $recipientSaver;
        $this->recipientChecker = $recipientChecker;
        $this->zabbixMemcachedConnection = $zabbixMemcachedConnection;
    }

    public function processSearchResult()
    {
        \Yii::$app->internalAmqp->consumeEvents(self::QUEUE, [$this, 'searchResultHandler']);
    }

    public function searchResultHandler($message)
    {
        $data       = $message['incomingData']['result'];
        $additional = $message['rpcCallInfo']['additional'];

        if (
            !empty($additional['isLastCall'])
            || (!empty($data['status']) && $data['status'] == 'error')
        ) {
            \Yii::$app->rpcClient->stopRpcContainer($additional['clientName'], $additional['target']);
        }

        if (empty($data['data']['users'])) {
            return true;
        }

        foreach ($data['data']['users'] as $user) {
            $recipient  = RecipientProfile::findOne(['remoteId' => $user['id']]);
            if ($this->recipientChecker->isUserSuitable($user) == false) {
                if ($recipient) {
                    $this->recipientSaver->updateRecipient($recipient, $user);
                }
                continue;
            }

            if ($recipient) {
                $this->recipientSaver->updateRecipient($recipient, $user);

                $recipient->online = (time() - strtotime($user['onlineStatusExpireAt']) < 18*60);
                if ($recipient->online) {
                    \Yii::$app->commScheduler->repairMissedRecipientSchema($recipient);
                    \Yii::$app->commScheduler->restartReadySchemas($recipient);
                }
            } else {
                $recipient = $this->recipientSaver->saveRecipient($user);
                if (!empty($recipient)) {
                    $this->zabbixMemcachedConnection->increment(ZabbixController::ZABBIX_KEY_COUNT_GRAB, 1);

                    \Yii::$app->commScheduler->addInitialRecipientSchemas($recipient);
                }
            }
        }

        return true;
    }

}
