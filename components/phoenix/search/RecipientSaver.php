<?php

namespace app\components\phoenix\search;

use app\components\common\IdEncoder;
use app\models\phoenix\PrepaidRecipients;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\RecipientShortId;

class RecipientSaver
{
    /* @var $idEncoder IdEncoder */
    private $idEncoder;

    public function __construct(IdEncoder $idEncoder)
    {
        $this->idEncoder = $idEncoder;
    }

    public function saveRecipient(array $user)
    {
        try {
            //TODO: remove old fields after migration to new search
            $recipient                       = new RecipientProfile();
            $recipient->remoteId             = $user['id'];
            $recipient->isPaid               = (int) !empty($user['hasAnyFeature']);
            $recipient->gender               = $user['gender'] ?? 1;
            $recipient->sexuality            = $user['sexual_orientation'] ?? 1;
            $recipient->age                  = (!empty($user['birthday'])) ? $this->getAgeFromBirthdaty($user['birthday']) : null;
            $recipient->site                 = $this->getSiteFromHash($user['siteId']);
            $recipient->country              = $user['country'] ?? null;
            $recipient->city                 = $user['city'] ?? null;
            $recipient->online               = (int) (time() - strtotime($user['onlineStatusExpireAt']) < 18*60);
            $recipient->lastOnlineDate       = $user['lastLoginAt'];
            $recipient->isWithPhoto          = !empty($user['photo_count']);
            $recipient->lastUsedPlatform     = $user['registrationPlatform'] ?? RecipientProfile::NOT_SET;
            $recipient->source               = $user['source'] ?? null;
            $recipient->locale               = $this->getLocale($user);
            $recipient->screenname           = $user['screenname'] ?? null;
            $recipient->registrationPlatform = $user['registrationPlatform'] ?? null;
            $recipient->photoCount           = $user['photo_count'] ?? null;
            $recipient->onlineStatusExpireAt = $user['onlineStatusExpireAt'] ?? null;
            $recipient->updatedAt            = $user['updatedAt'] ?? null;
            $recipient->birthday             = $user['birthday'] ?? null;
            $recipient->createdAt            = date('Y-m-d H:i:s');
            $recipient->multiProfileId       = $user['multiProfileId'] ?? null;
            $recipient->niche                = $recipient->getRecipientNiche();
            $recipient->complained           = $user['complained'] ?? 0;
            $recipient->searchable           = $user['searchable'] ?? 1;

            if ($recipient->save()) {
                $this->createShortId($recipient);
                $this->createPrepaidRecord($recipient, $user);
                return $recipient;
            }
        } catch (\yii\db\IntegrityException $e) {
            return false;
        }

        return false;
    }

    public function updateRecipient(RecipientProfile $recipient, array $userData)
    {
        $recipient->isPaid               = (int) !empty($userData['hasAnyFeature']);
        $recipient->online               = (int) (time() - strtotime($userData['onlineStatusExpireAt']) < 18*60);
        $recipient->lastOnlineDate       = $userData['lastLoginAt'];
        $recipient->isWithPhoto          = !empty($userData['photo_count']);
        $recipient->photoCount           = $userData['photo_count'] ?? null;
        $recipient->onlineStatusExpireAt = $userData['onlineStatusExpireAt'] ?? null;
        $recipient->updatedAt            = $userData['updatedAt'] ?? null;
        $recipient->multiProfileId       = $userData['multiProfileId'] ?? null;
        $recipient->niche                = $recipient->getRecipientNiche();
        $recipient->complained           = $userData['complained'] ?? 0;
        $recipient->searchable           = $userData['searchable'] ?? 1;

        if ($recipient->save()) {
            $this->updatePrepaidRecord($recipient, $userData);
            return $recipient;
        }

        return false;
    }

    private function createShortId(RecipientProfile $recipient)
    {
        $recipient->refresh();
        $shortIdModel              = new RecipientShortId();
        $shortIdModel->recipientId = $recipient->id;
        $shortIdModel->shortId     = $this->idEncoder->encode($recipient);
        $shortIdModel->remoteId    = $recipient->remoteId;
        $shortIdModel->country     = $recipient->country;
        $shortIdModel->site        = $recipient->site;
        $shortIdModel->save();
    }

    private function createPrepaidRecord(RecipientProfile $recipient, array $userData)
    {
        if (!empty($userData['riskLevelLow'])) {
            return;
        }

        $recipient->refresh();
        $prepaidRecipient = new PrepaidRecipients();
        $prepaidRecipient->recipientId = $recipient->id;
        $prepaidRecipient->save();
    }

    private function updatePrepaidRecord(RecipientProfile $recipient, array $userData)
    {
        $prepaidRecipient = PrepaidRecipients::findOne(['recipientId' => $recipient->id]);

        if (!empty($userData['riskLevelLow']) && empty($prepaidRecipient)) {
            $prepaidRecipient = new PrepaidRecipients();
            $prepaidRecipient->recipientId = $recipient->id;
            $prepaidRecipient->save();
            return;
        }

        if (empty($userData['riskLevelLow']) && !empty($prepaidRecipient)) {
            $prepaidRecipient->delete();
        }
    }

    private function getLocale($user)
    {
        $locale = null;
        if (is_array($user['locale'])) {
            $locale = array_shift($user['locale']);
        } elseif (is_string($user['locale'])) {
            $locale = $user['locale'];
        }

        return $locale;
    }

    private function getAgeFromBirthdaty($birthday)
    {
        $from = new \DateTime($birthday);
        $to   = new \DateTime();
        return $from->diff($to)->y;
    }

    private function getSiteFromHash($hash)
    {
        $sites = \Yii::$app->yaml->parse('site');
        foreach ($sites as $key => $site) {
            if ($hash == $site['hash']) {
                return $key;
            }
        }

        return null;
    }
}