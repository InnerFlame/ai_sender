<?php

namespace app\components\phoenix\search;

use app\models\phoenix\SearchProfile;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\GrabRule;
use app\models\rpc\Container;
use app\models\rpc\Client;
use app\models\phoenix\SearchClient;
use yii\base\Component;
use app\config\Constant;
use yii\base\Exception;

class SearcherComponent extends Component
{
    protected $limitPerUser = 480;
    protected $limitPerPage = 48;
    protected $pagesLimit   = 5;

    public function resetSearchesCount()
    {
        $conditions = 'countSearch >= :countSearch AND timeStartSearch <= :time';
        $params     = [
            ':countSearch' => $this->limitPerUser,
            ':time' => date('Y-m-d H:i:s', time() - 3600),
        ];

        \Yii::$app->db->createCommand()->update(SearchProfile::tableName(), ['countSearch' => 0], $conditions, $params)->execute();
    }

    public function runSearch()
    {
        $rules = $this->getGrabRules();

        if (empty($rules)) {
            return;
        }

        foreach ($rules as $rule) {
            //To run search by some periods
            $isRuleRecentlyProcessed = \Yii::$app->cache->get('searchRuleProcessed.' . $rule->id);
            if (!empty($isRuleRecentlyProcessed)) {
                continue;
            }

            $this->startSearchProfiles($rule);

            \Yii::$app->cache->set('searchRuleProcessed.' . $rule->id, true, 180);
        }
    }

    public function processSearch($profileId)
    {
        $searchClient = SearchClient::findOne(['profileId' => $profileId]);
        if (empty($searchClient)) {
            throw new Exception('Search client data has been lost');
        }

        $profile    = SenderProfile::findOne($profileId);
        $rule       = GrabRule::findOne($searchClient->ruleId);

        $searchParams = $this->generateSearchParams($rule, $searchClient->location);
        //start search by rule
        for ($page = 0; $page < $this->pagesLimit; $page++) {
            $searchProfile = SearchProfile::findOne(['observerId' => $profile->id]);
            if ($this->isProfileSearchLimitReached($searchProfile)) {
                break;
            }

            $searchParams['offset'] = $page*$this->limitPerPage;

            $this->callSearch($profile, $rule, $searchParams);

            $this->updateSearchProfile($searchProfile);
        }
    }

    protected function callSearch(SenderProfile $profile, GrabRule $rule, array $searchParams)
    {
        $additional = [
            'profileId'       => $profile->id,
            'country'         => $rule->country,
            'city'            => $searchParams['location'],
            'state'           => $rule->state,
            'site'            => $rule->site,
            'clientName'      => Constant::TARGET_PHOENIX_WEB . '.' . $profile->id,
        ];

        \Yii::$app->rpcClient->rpcCall($this->getClientName($profile), 'search', [$searchParams], $additional);
    }

    protected function generateSearchParams(GrabRule $rule, $location)
    {
        $usedValues = \Yii::$app->cache->get('usedSearchParams.' . $rule->id);
        if (empty($usedValues)) {
            $usedValues = [
                'withPhoto' => [],
                'tab'       => [],
                'sortType'  => [],
            ];
        }

        $possibleValues = [
            'withPhoto' => [true, false],
            'tab'       => ['all_members', 'online', 'new_members',],
            'sortType'  => ['photo_quality', 'lastvisit', 'distance', 'age',],
        ];

        if ($rule->country == 'USA') {
            $possibleValues['tab']      = ['online'];
            $possibleValues['sortType'] = ['lastvisit', 'distance'];
        }

        foreach ($possibleValues as $name => $values) {
            $notUsedValues = array_diff($possibleValues[$name], $usedValues[$name]);
            //get list of not used values or get full list and flush cache
            if (!empty($notUsedValues)) {
                $possibleValues[$name] = $notUsedValues;
            } else {
                $usedValues[$name] = [];
            }
        }

        $tabKey                 = array_rand($possibleValues['tab']);
        $tab                    = $possibleValues['tab'][$tabKey];
        $usedValues['tab'][]    = $tab;

        $sortTypeKey                = array_rand($possibleValues['sortType']);
        $sortType                   = $possibleValues['sortType'][$sortTypeKey];
        $usedValues['sortType'][]   = $sortType;

        $withPhotoKey                = array_rand($possibleValues['withPhoto']);
        $withPhoto                   = $possibleValues['withPhoto'][$withPhotoKey];
        $usedValues['withPhoto'][]   = $withPhoto;

        \Yii::$app->cache->set('usedSearchParams.' . $rule->id, $usedValues, 3600);

        $data = [
            'limit'                    => $this->limitPerPage,
            'location'                 => $location,
            'country'                  => $rule->country,
            'gender'                   => 1,
            'ageFrom'                  => $rule->ageFrom,
            'ageTo'                    => $rule->ageTo,
            'checkboxOnline'           => 'on',
            'sexual_orientation'       => ['hetero'],
            'tab'                      => $tab,
            'sortType'                 => $sortType,
        ];

        if ($withPhoto) {
            $data += [
                'searchbar_wphoto'         => 'wphoto',
                'wphoto'                   => [0],
                'photoLevel'               => [0,1,2],
            ];
        }

        return $data;
    }

    protected function getClientName(SenderProfile $profile)
    {
        return 'client.' . Constant::TARGET_PHOENIX_WEB . '.' . $profile->id;
    }

    protected function getLocations(GrabRule $rule)
    {
        $locations = \Yii::$app->yaml->parseByKey('searchLocations', $rule->country);

        if (empty($locations)) {
            $locations = ['any'];
        }

        return $locations;
    }

    protected function makeLocation(GrabRule $rule)
    {
        if ($rule->state) {
            if (empty($rule->city)) {
                return $rule->state;
            }
            return $rule->city . '%2C%20' . $rule->state;
        }

        return 'any';
    }

    protected function isProfileSearchLimitReached(SearchProfile $searchProfile)
    {
        if ($searchProfile->countSearch >= $this->limitPerUser) {
            return true;
        }

        return false;
    }

    protected function updateSearchProfile(SearchProfile $searchProfile)
    {
        if ($searchProfile->countSearch == 0) {
            $searchProfile->timeStartSearch = date('Y-m-d H:i:s');
            $searchProfile->save();
        }

        $searchProfile->updateCounters(['countSearch' => $this->limitPerPage]);
    }

    protected function startSearchProfiles(GrabRule $rule)
    {
        //1. Close previous containers
        $clients = Client::findAll([
            'country'   => $rule->country,
            'site'      => $rule->site,
            'target'    => Constant::TARGET_PHOENIX_WEB,
            'status'    => Client::STATUS_LOGIN,
            'type'      => Client::TYPE_SEARCH
        ]);

        if (!empty($clients) && is_array($clients)) {
            foreach ($clients as $client) {
                \Yii::$app->rpcClient->stopRpcContainer($client->name, $client->target);
            }
        }
        //2. Create new containers
        $locations = $this->getLocations($rule);
        $searchProfiles = $this->getDbSearchProfiles($rule, count($locations));
        if (!empty($searchProfiles) && is_array($searchProfiles)) {
            foreach ($searchProfiles as $searchProfile) {
                if (empty($locations)) {
                    break;
                }

                $locationKey = array_rand($locations);
                $location = $locations[$locationKey];
                unset($locations[$locationKey]);

                if (empty($location)) {
                    break;
                }

                $profile = SenderProfile::findOne($searchProfile->observerId);

                $searchClient = SearchClient::findOne(['profileId' => $profile->id]);
                if (empty($searchClient)) {
                    $searchClient               = new SearchClient();
                    $searchClient->profileId    = $profile->id;
                }
                $searchClient->ruleId   = $rule->id;
                $searchClient->location = $location;
                $searchClient->save();

                $this->startContainer($profile);
            }
        }
    }

    public function startContainer(SenderProfile $profile)
    {
        usleep(500000);
        \Yii::$app->rpcMethods->createRpcContainer($profile, Container::TYPE_SEARCH, Constant::TARGET_PHOENIX_WEB);
        return true;
    }

    public function loginClient(SenderProfile $profile)
    {
        \Yii::$app->rpcMethods->loginRpcClient($profile, Client::TYPE_SEARCH, Constant::TARGET_PHOENIX_WEB);
        return true;
    }

    protected function getDbSearchProfiles(GrabRule $rule, $count)
    {
        $profiles = SearchProfile::findBySql('
            SELECT s.*
            FROM '. SearchProfile::tableName(). ' as s
            JOIN '. SenderProfile::tableName() .' as sp ON s.observerId = sp.id
            WHERE
                sp.country          = :country
                AND sp.photoStatus  = :photoStatus
                AND sp.bannedAt     = :bannedAt
                AND sp.site         = :site
                AND s.countSearch   < :countSearch
            ORDER BY RAND()
            ', [
                ':country'      => $rule->country,
                ':photoStatus'  => SenderProfile::STATUS_APPROVED,
                ':bannedAt'     => '0000-00-00 00:00:00',
                ':site'         => $rule->site,
                'countSearch'   => $this->limitPerUser
            ])
            ->limit($count)
            ->all();

        return $profiles;
    }

    protected function getGrabRules()
    {
        return GrabRule::findAll(['isActive' => 1]);
    }

    public function getStartedSearchContainers()
    {
        return \Yii::$app->redis->executeCommand('LRANGE', [$this->keyStartedSearchContainers, '0', '-1']);
    }
}