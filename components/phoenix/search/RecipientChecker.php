<?php

namespace app\components\phoenix\search;

use app\models\phoenix\UserExclusionsRules;

class RecipientChecker
{
    public function isUserSuitable(array $user)
    {
        /**@var $suitableUserDetails UserExclusionsRules*/
        $suitableUserDetails = UserExclusionsRules::find()->where(['id' => 1, 'isActive' => 1])->one();

        if (!$suitableUserDetails) {
            return true;
        }

        if (empty($user['gender']) || !in_array($user['gender'], $suitableUserDetails->gendersArray)) {
            return false;
        }

        if (empty($user['sexual_orientation']) || !in_array($user['sexual_orientation'],
                $suitableUserDetails->orientationArray)) {
            return false;
        }

        if (empty($user['hasAnyFeature']) || !in_array($user['hasAnyFeature'], $suitableUserDetails->isPaidArray)) {
            return false;
        }

        if (empty($user['registrationPlatform']) || !in_array($user['registrationPlatform'],
                $suitableUserDetails->registrationPlatformsArray)) {
            return false;
        }

        if (empty($user['source']) || in_array($user['source'], $suitableUserDetails->excludedSourcesArray)) {
            return false;
        }

        if (!empty($user['complained']) && ($user['complained'] == 1)) {
            return false;
        }

        if (isset($user['searchable']) && !$user['searchable']) {
            return false;
        }

        return true;
    }
}