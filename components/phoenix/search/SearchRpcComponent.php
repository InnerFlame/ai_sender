<?php

namespace app\components\phoenix\search;

use app\models\phoenix\RecipientProfile;
use yii\base\Component;

class SearchRpcComponent extends Component
{
    const QUEUE = 'rpcSearchResponse';

    protected $unique         = 0;
    protected $all            = 0;
    protected $online         = 0;
    protected $recentlyOnline = 0;

    const WEB_SITE = 'webSite';
    const MOB_SITE = 'mobSite';

    public function processSearchResult()
    {
        \Yii::$app->internalAmqp->consumeEvents(self::QUEUE, [$this, 'searchResultHandler']);
    }

    public function searchResultHandler($message)
    {
        $data       = $message['incomingData']['result'];
        $additional = $message['rpcCallInfo']['additional'];

        if (empty($data['data'])) {
            $error = json_encode($data) . "\n" . date('Y-m-d H:i:s') . "\n\n\n";
            file_put_contents(\Yii::$app->basePath . '/runtime/logs/searchDataError.txt', $error, FILE_APPEND);
            $this->saveToGrabLog($additional);
            return true;
        }

        if (empty($data['data']['users'])) {
            $this->saveToGrabLog($additional);
            return true;
        }

        $this->all = count($data['data']['users']);

        foreach ($data['data']['users'] as $user) {
            $user['id'] = $user['modelId'] ?? $user['id'];
            $online     = $this->getOnlineStatus($user['statuses']) || $this->getRecentlyOnlineStatus($user['statuses']);
            $recipient  = RecipientProfile::findOne(['remoteId' => $user['id']]);

            if ($recipient) {
                $recipient->online = $online;
                $this->updateStatusToRecipient($recipient, $user);
                \Yii::$app->commScheduler->repairMissedRecipientSchema($recipient);
                \Yii::$app->commScheduler->restartReadySchemas($recipient);
            } else {
                $this->unique++;
                $recipient = $this->saveRecipient($user, $online, $additional);

                if (!empty($recipient)) {
                    \Yii::$app->commScheduler->addInitialRecipientSchemas($recipient);
                }
            }
        }

        $this->saveToGrabLog($additional);
        return true;
    }

    private function getPlatform($user)
    {
        if (!empty($user['lastUsedPlatform'])) {
            return $user['lastUsedPlatform'];
        } elseif (!empty($user['registrationPlatform'])) {
            return $user['registrationPlatform'];
        }

        return RecipientProfile::NOT_SET;
    }

    protected function isTopUser($user)
    {
        if (isset($user['marks'])) {
            if (isset($user['marks']['free_communication']) && $user['marks']['free_communication']['enabledForUser'] == 1) {
                return true;
            }

            if (isset($user['marks']['top_in_search']) && $user['marks']['top_in_search']['enabledForUser'] == 1) {
                return true;
            }

            if (isset($user['marks']['invisible_mode']) && $user['marks']['invisible_mode']['exist'] == 1) {
                return true;
            }
        }

        return false;
    }

    protected function updateStatusToRecipient(RecipientProfile $recipient, $user)
    {
        $recipient->lastUsedPlatform = $this->getPlatform($user);
        $recipient->niche            = $recipient->getRecipientNiche();

        if ($recipient->online == true) {
            $recipient->lastOnlineDate = date('Y-m-d H:i:s');
        }

        $recipient->save();
    }

    protected function isSendCommunication($recipient, $online)
    {
        $lastOnlineTime = strtotime($recipient->lastOnlineDate);
        $time           = time() - 3600;

        if ($lastOnlineTime <= $time && $online == 1 && $recipient->online == 0) {
            return true;
        }

        return false;
    }

    protected function saveRecipient($user, $online, $additional)
    {
        try {
            $recipient                   = new RecipientProfile();
            $recipient->remoteId         = $user['id'];
            $recipient->isPaid           = 0;
            $recipient->gender           = $this->getGender($user['gender']);
            $recipient->age              = $user['age'];
            $recipient->site             = $additional['site'];
            $recipient->country          = $additional['country'];
            $recipient->city             = $user['geo']['city'] ?? null;
            $recipient->state            = $additional['state'];
            $recipient->citySearch       = $additional['city'];
            $recipient->stateSearch      = $additional['state'];
            $recipient->online           = $online;
            $recipient->lastOnlineDate   = date('Y-m-d H:i:s');
            $recipient->isWithPhoto      = $this->hasPhoto($user);
            $recipient->lastUsedPlatform = $this->getPlatform($user);
            $recipient->niche            = $recipient->getRecipientNiche();

            if ($recipient->save()) {
                return $recipient;
            }
        } catch (\yii\db\IntegrityException $e) {
            return false;
        }

        return false;
    }

    private function hasPhoto($user)
    {
        if ($user['photos']['count'] == 0) {
            return 0;
        }

        return 1;
    }

    protected function getGender($gender)
    {
        $genders = \Yii::$app->yaml->parse('gender');
        return array_search($gender, $genders);
    }

    protected function getOnlineStatus($status)
    {
        if (!empty($status['onlineStatus'])) {
            $this->online++;
            return 1;
        }

        return 0;
    }

    protected function getRecentlyOnlineStatus($status)
    {
        if (!empty($status['recentlyOnlineStatus'])) {
            $this->recentlyOnline++;
            return true;
        }

        return false;
    }

    /**
     * @param $additional
     * @throws \yii\db\Exception
     * Save to log data about search query
     */
    protected function saveToGrabLog($additional)
    {
        if (!empty($additional['city'])) {
            $location = $additional['city'];
        } elseif (!empty($additional['state'])) {
            $location = $additional['state'];
        } else {
            $location = $additional['country'];
        }

        \Yii::$app->db->createCommand()->insert('grabLog', [
            'senderId'       => $additional['profileId'],
            'all'            => $this->all,
            'unique'         => $this->unique,
            'online'         => $this->online,
            'recentlyOnline' => $this->recentlyOnline,
            'location'       => $location,
        ])->execute();

        $this->all            = 0;
        $this->unique         = 0;
        $this->online         = 0;
        $this->recentlyOnline = 0;
    }
}
