<?php

namespace app\components\phoenix\search;

use app\config\Constant;
use app\models\phoenix\ApiSearchRule;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use app\models\rpc\Container;
use yii\base\Component;
use yii\db\Expression;

class ApiSearchComponent extends Component
{
    public function runSearch()
    {
        $mobApiSearchCountries = ApiSearchRule::getCountries();
        foreach ($mobApiSearchCountries as $country) {
            $query = SenderProfile::find()
                ->where(['country'          => $country])
                ->andWhere(['bannedAt'      => '0000-00-00 00:00:00'])
                ->andWhere(['photoStatus'   => SenderProfile::STATUS_APPROVED])
                ->andWhere(['userType'      => SenderProfile::TYPE_SEARCHER])
                ->orderBy(new Expression('rand()'));
            $profile = $query->one();

            $this->startContainer($profile);
        }
    }

    public function processSearch($profileId)
    {
        $profile = SenderProfile::findOne($profileId);

        $stepInterval = 30;
        $syncTime = time();
        $startTime = \Yii::$app->cache->get('lastSearchSyncTime' . $profile->country);
        if (empty($startTime)) {
            $startTime = $syncTime - 20*$stepInterval;
        }

        $endTime = $startTime + $stepInterval;
        $endTime = ($endTime < $syncTime) ? $endTime : $syncTime;

        do {
            $params = [
                'updatedAt_from'    => (string) $startTime,
                'updatedAt_to'      => (string) $endTime,
                'country'           => $profile->country,
            ];

            $startTime  += $stepInterval;
            $endTime    += $stepInterval;
            $endTime    = ($endTime < $syncTime) ? $endTime : $syncTime;

            $additional = [
                'clientName' => Constant::TARGET_PHOENIX . '.' . $profile->id,
                'target'     => Constant::TARGET_PHOENIX,
                'isLastCall' => ($endTime >= $syncTime),
            ];

            \Yii::$app->rpcClient->rpcCall(
                'client.' . Constant::TARGET_PHOENIX . '.' . $profile->id,
                'searchNewsFeed',
                [$params],
                $additional
            );

        } while ($endTime < $syncTime);

        \Yii::$app->cache->set('lastSearchSyncTime' . $profile->country, $syncTime, 60*$stepInterval);
    }

    public function startContainer(SenderProfile $profile)
    {
        \Yii::$app->rpcMethods->createRpcContainer($profile, Container::TYPE_SEARCH, Constant::TARGET_PHOENIX);
        return true;
    }

    public function loginClient(SenderProfile $profile)
    {
        \Yii::$app->rpcMethods->loginRpcClient($profile, Client::TYPE_SEARCH, Constant::TARGET_PHOENIX);
        return true;
    }
}