<?php

namespace app\components\phoenix;

use app\config\Constant;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Container;
use yii\base\Component;
use yii\db\Expression;
use yii\db\Query;

class ProfileDeactivationComponent extends Component
{
    public function isDeactivated($senderId)
    {
	//temporary. wait for profiles lifecycle solution
	return false;
        $countChat = $this->getAllCommCount($senderId);

        if ($countChat >= mt_rand(80, 200)) {
            SenderProfile::deactivate($senderId);
            return true;
        }

        $countActivity = $this->getActivityDaysCount($senderId);

        if ($countActivity >= mt_rand(15, 20)) {
            SenderProfile::deactivate($senderId);
        }

        $sender    = SenderProfile::findOne($senderId);
        $container = \Yii::$app->rpcClient->getRpcContainer($sender, Constant::TARGET_PHOENIX);

        if (isset($container) && $container->status == Container::STATUS_STARTED) {
            $countChat = $this->getSessionCommCount($senderId, $container->timeSetStatus);

            if ($countChat >= mt_rand(15, 50)) {
                SenderProfile::temporaryDeactivate($senderId);
                return true;
            }
        }

        return false;
    }

    /**
     * @param $senderId
     * @return false|null|string
     * Count chats for all time
     */
    private function getAllCommCount($senderId)
    {
        $query = new Query();

        return $query->select('COUNT(DISTINCT(recipientId))')
            ->from(CommunicationSchedule::tableName())
            ->where(['senderId' => $senderId])
            ->scalar();
    }

    /**
     * @param $senderId
     * @param $timeFrom
     * @return false|null|string
     * Count chats for one session
     */
    private function getSessionCommCount($senderId, $timeFrom)
    {
        $query = new Query();

        return $query->select('COUNT(DISTINCT(recipientId))')
            ->from(CommunicationSchedule::tableName())
            ->where(['senderId' => $senderId])
            ->andWhere(['>', 'sentAt', date('Y-m-d H:i:s', $timeFrom)])
            ->scalar();
    }

    /**
     * @param $senderId
     * @return false|null|string
     * count days with communications
     */
    private function getActivityDaysCount($senderId)
    {
        $query = new Query();

        return $query->select(new Expression("COUNT(DISTINCT(DATE_FORMAT(`createdAt`, '%Y%m%d')))"))
            ->from(CommunicationSchedule::tableName())
            ->where(['senderId' => $senderId])
            ->scalar();
    }
}
