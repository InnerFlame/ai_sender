<?php

namespace app\components\phoenix;

use app\components\phoenix\communication\StepByStepSchedulerComponent;
use app\components\phoenix\search\ApiSearchRpc;
use app\components\phoenix\search\SearchRpcComponent;
use sender\communication\components\commProcessor\CommProcessorRpc;
use sender\interaction\components\InteractionPhotoApprove;
use sender\interaction\components\InteractionPhotoUpload;
use sender\interaction\components\InteractionScamCheck;
use sender\registration\components\RegisterRpc;
use sender\scamCheck\components\ScamCheckRpc;
use yii\base\Component;

class RpcEventsProcessorComponent extends Component
{
    const INTERNAL_MESSAGE = 'internalMessage';
    const INCOMING = 'incoming';

    public function processEvent(array $incomingData, array $rpcCallInfo)
    {
        if (!empty($incomingData['method'])) {
            return $this->processMethod($rpcCallInfo, $incomingData);
        } elseif (!empty($incomingData['error'])) {
            return $this->processError($rpcCallInfo, $incomingData);
        } elseif (!empty($incomingData['result'])) {
            return $this->processResult($rpcCallInfo, $incomingData);
        }

        return true;
    }

    private function processError(array $rpcCallInfo, array $incomingData)
    {
        \Yii::$app->internalAmqp->publishEvent(
            RpcErrorComponent::QUEUE,
            [
                'incomingData' => $incomingData,
                'rpcCallInfo' => $rpcCallInfo,
            ]
        );
        return true;
    }

    private function processResult(array $rpcCallInfo, array $incomingData)
    {
        $params = [
            'rpcCallInfo'  => $rpcCallInfo,
            'incomingData' => $incomingData
        ];

        \Yii::$app->rpcClient->storeRpcLog([
            'clientName' => $this->getClientName($rpcCallInfo),
            'direction'  => self::INCOMING,
            'method'     => $rpcCallInfo['method'],
            'params'     => json_encode($params),
        ]);

        switch ($rpcCallInfo['method']) {
            case 'register':
            case 'photoUpload':
                $queue = RegisterRpc::QUEUE;
                $data  = ['incomingData' => $incomingData, 'rpcCallInfo' => $rpcCallInfo];
                break;
            case 'refreshToken':
            case 'start':
            case 'login':
            case 'fetch':
                $queue = ContainerClientRpcComponent::QUEUE;
                $data  = ['incomingData' => $incomingData, 'rpcCallInfo' => $rpcCallInfo];
                break;
            case 'sendMessage':
                $queue = CommProcessorRpc::QUEUE_SENT_MESSAGE;

                $data  = ['rpcCallInfo' => $rpcCallInfo, 'incomingData' => $incomingData];
                break;
            case 'search':
                $queue = SearchRpcComponent::QUEUE;
                $data  = ['incomingData' => $incomingData, 'rpcCallInfo' => $rpcCallInfo];
                break;
            case 'searchNewsFeed':
                if (!empty($rpcCallInfo['additional']['scamCheck'])) {
                    $queue = ScamCheckRpc::QUEUE;
                } else {
                    $queue = ApiSearchRpc::QUEUE;
                }
                $data  = ['incomingData' => $incomingData, 'rpcCallInfo' => $rpcCallInfo];
                break;
            default:
                return false;
        }

        \Yii::$app->internalAmqp->publishEvent($queue, $data);

        return true;
    }

    private function processMethod(array $rpcCallInfo, array $incomingData)
    {
        \Yii::$app->rpcClient->storeRpcLog([
            'clientName' => $this->getClientName($incomingData),
            'direction'  => self::INCOMING,
            'method'     => $this->getMethod($incomingData),
            'params'     => json_encode($incomingData),
        ]);

        switch ($incomingData['method']) {
            case 'banned':
                $queue = InteractionScamCheck::QUEUE;
                $data = $incomingData;
                break;
            case 'no_photo':
                $queue = InteractionPhotoApprove::QUEUE;
                $data = $incomingData;
                break;
            case 'photoUploaded':
                $queue = InteractionPhotoUpload::QUEUE_UPLOADED;
                $data = $incomingData;
                break;
            case 'photoUploadFiled':
                $queue = InteractionPhotoUpload::QUEUE_ERROR;
                $data = $incomingData;
                break;
            case 'close':
                $queue                         = ContainerClientRpcComponent::QUEUE;
                $data['rpcCallInfo']['method'] = 'close';
                $data['incomingData']          =  $incomingData['params'][0];
                break;
            case 'msg':
                $queue = StepByStepSchedulerComponent::QUEUE_SAVE_MESSAGE;
                $data  = $incomingData['params'][0];
                break;
            case 'start':
                $queue                         = ContainerClientRpcComponent::QUEUE;
                $data['rpcCallInfo']['method'] = 'startMethod';
                $data['incomingData']          =  $incomingData['params'][0];
                break;
            case 'killContainer':
                $queue = ContainerClientRpcComponent::QUEUE;
                $data['rpcCallInfo'] = $rpcCallInfo;
                $data['incomingData'] = $incomingData;
                break;
            default:
                return false;
        }

        \Yii::$app->internalAmqp->publishEvent($queue, $data);

        return true;
    }

    /**
     * @param mixed[] $incomingData
     * @return string|null
     */
    private function getClientName(array $incomingData)
    {
        if (!empty($incomingData['additional']['clientName'])) {
            return $incomingData['additional']['clientName'];
        } elseif (!empty($incomingData['params'][0]['id'])) {
            return $incomingData['params'][0]['id'];
        }

        return null;
    }

    /**
     * @param mixed[] $incomingData
     * @return mixed
     */
    private function getMethod(array $incomingData)
    {
        return !empty($incomingData['method']) ? $incomingData['method'] : $incomingData['error'];
    }
}
