<?php

namespace app\components\phoenix;

use yii\base\Component;

class LocationComponent extends Component
{
    /**
     * @param array $location
     * @return array
     */
    public function getLocation($location)
    {
        $locationData = \Yii::$app->yaml->parseByKey('cityGrab', $location['country']);

        $data = [
            'state' => [],
            'city'  => [],
        ];

        if (!isset($location['state'])) {
            if ($this->isState($locationData)) {
                $data['state'] = $this->makeStateArray($locationData);
            } else {
                $data['city'] = $this->makeCityArray($locationData);
            }
        } else {
            $data['city'] = $this->makeCityArrayForState($locationData, $location['state']);
        }

        return $data;
    }

    protected function isState($location)
    {
        foreach ($location as $item) {
            if (isset($item['stateCode'])) {
                return true;
            }
            return false;
        }
        return false;
    }

    protected function makeStateArray($location)
    {
        $states = [];

        foreach ($location as $item){
            $states[$item['stateCode']] = $item['stateName'];
        }
        return $states;
    }

    protected function makeCityArray($location)
    {
        $cities = [];

        foreach ($location as $item){
            $cities[$item] = $item;
        }
        return $cities;
    }

    protected function makeCityArrayForState($location, $state)
    {
        $cities = [];

        foreach ($location as $item){
            if ($item['stateCode'] == $state){
                foreach ($item['city'] as $city) {
                    $cities[$city] = $city;
                }

                return $cities;
            }
        }
        return [];
    }
}