<?php

namespace app\components\phoenix\report;

use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\DelaySentCommunication;
use app\models\phoenix\PlannedCommunication;
use yii\base\Component;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class DelayStatComponent extends Component
{
    const DELAYED_SENT_LIMIT_STEP_0 = 2000;
    const DELAYED_SENT_LIMIT_STEP_1 = 150;

    const NOT_SENT_LIMIT_STEP_0 = 2000;
    const NOT_SENT_LIMIT_STEP_1 = 150;

    const SCHEDULED_LIMIT_STEP_0 = 700;
    const SCHEDULED_LIMIT_STEP_1 = 30;

    public $smsEnable;
    private $data = [];

    public function getDelayStat($dataFilters)
    {
        $countScheduled  = ArrayHelper::index($this->getCountScheduled($dataFilters), 'date');
        $countDelaySent  = ArrayHelper::index($this->getCountDelaySent($dataFilters), 'date');

        return \Yii::$app->dataFormatter->formatForDelayStat(
            $dataFilters['dateFrom'],
            $dataFilters['dateTo'],
            $dataFilters['group'],
            $countScheduled,
            $countDelaySent
        );
    }

    private function getCountDelaySent($dataFilters)
    {
        $query = (new Query())
            ->from(DelaySentCommunication::tableName())
            ->andWhere(['>=', 'timeToSend', $dataFilters['dateFrom']])
            ->andWhere(['<=', 'timeToSend', $dataFilters['dateTo'] . ' 23:59:59'])
            ->andWhere(['delay' => $dataFilters['delay']])
            ->groupBy('date')
            ->orderBy('date');

        if ($dataFilters['dateFrom'] == $dataFilters['dateTo']) {
            if ($dataFilters['group'] == 'hour') {
                $query->select(new Expression('SUM(countSent) as count, DATE_FORMAT(timeToSend, "%H:00") as date'));
            } else {
                $query->select(new Expression('SUM(countSent) as count, DATE_FORMAT(timeToSend, "%H:%i") as date'));
            }
        } else {
            $query->select(new Expression('SUM(countSent) as count, DATE_FORMAT(timeToSend, "%Y-%m-%d") as date'));
        }

        if ($dataFilters['type'] == 'eva') {
            if ($dataFilters['step'] || $dataFilters['step'] == '0') {
                $query->andWhere(['step' => $dataFilters['step']]);
            }
        } else {
            $query->andWhere(['step' => 0]);
        }

        $result = $query->all();

        return $result;
    }

    private function getCountScheduled($dataFilters)
    {
        $query = (new Query())
            ->from(PlannedCommunication::tableName())
            ->andWhere(['>=', 'timeToSend', $dataFilters['dateFrom']])
            ->andWhere(['<=', 'timeToSend', $dataFilters['dateTo'] . ' 23:59:59'])
            ->groupBy('date')
            ->orderBy('date');

        if ($dataFilters['dateFrom'] == $dataFilters['dateTo']) {
            if ($dataFilters['group'] == 'hour') {
                $query->select(new Expression('SUM(countToSend) as countToSend, SUM(countNotSent) as countNotSent, SUM(countSent) as countSent, DATE_FORMAT(timeToSend, "%H:00") as date'));
            } else {
                $query->select(new Expression('SUM(countToSend) as countToSend, SUM(countNotSent) as countNotSent, SUM(countSent) as countSent, DATE_FORMAT(timeToSend, "%H:%i") as date'));
            }
        } else {
            $query->select(new Expression('SUM(countToSend) as countToSend, SUM(countNotSent) as countNotSent, SUM(countSent) as countSent, DATE_FORMAT(timeToSend, "%Y-%m-%d") as date'));
        }

        if ($dataFilters['type'] == 'eva') {
            if ($dataFilters['step'] || $dataFilters['step'] == '0') {
                $query->andWhere(['step' => $dataFilters['step']]);
            }
        } else {
            $query->andWhere(['step' => 0]);
        }

        $result = $query->all();

        return $result;
    }

    public function collectPlannedCommunication($dateStart, $dateEnd)
    {
        $dataCountInSchedule = $this->getCountInSchedule($dateStart, $dateEnd);
        $dataCountNotSent    = $this->getCountInSchedule($dateStart, $dateEnd, true);
        $dataCountSent       = $this->getCountInSchedule($dateStart, $dateEnd, false, true);

        $dataCountNotSent = ArrayHelper::index(
            $dataCountNotSent,
            'step',
            [
                function ($element) {
                    return $element['date'];
                },
                'step'
            ]
        );

        $dataCountSent = ArrayHelper::index(
            $dataCountSent,
            'step',
            [
                function ($element) {
                    return $element['date'];
                },
                'step'
            ]
        );

        $alerts = [];
        foreach ($dataCountInSchedule as $item) {
            $plannedCommunication = PlannedCommunication::findOne(['step' => $item['step'], 'timeToSend' => $item['date']]);
            $countNotSent         = 0;
            $countSent            = 0;

            if (!empty($dataCountNotSent[$item['date']][$item['step']][$item['step']])
                && $dataCountNotSent[$item['date']][$item['step']][$item['step']]['step'] == $item['step']
            ) {
                $countNotSent = $dataCountNotSent[$item['date']][$item['step']][$item['step']]['count'];
            }

            if (!empty($dataCountSent[$item['date']][$item['step']][$item['step']])
                && $dataCountSent[$item['date']][$item['step']][$item['step']]['step'] == $item['step']
            ) {
                $countSent = $dataCountSent[$item['date']][$item['step']][$item['step']]['count'];
            }

            $alerts[] = $this->checkForScheduledAlerts($item['count'], $item['step'], $item['date']);
            $alerts[] = $this->checkForNotSentAlerts($countNotSent, $item['step'], $item['date']);

            if ($plannedCommunication) {
                $plannedCommunication->countToSend  = $item['count'];
                $plannedCommunication->countNotSent = $countNotSent;
                $plannedCommunication->countSent    = $countSent;
                $plannedCommunication->save();
            } else {
                $this->data[] = [$item['count'], $countNotSent, $countSent, $item['step'], $item['date']];
            }

            $this->savePartDataForPlannedCommunication();
        }

        $this->saveDataForPlannedCommunication();

        $this->sendAlerts($alerts);
    }

    public function collectDelaySentCommunication($dateStart, $dateEnd)
    {
        $delay = 30;

        $alerts = [];
        while($delay <= 180) {
            $countSentWithDelay =  $this->getCountSentWithDelay($dateStart, $dateEnd, $delay);

            foreach ($countSentWithDelay as $item) {
                $delaySentCommunication = DelaySentCommunication::findOne(['step' => $item['step'], 'timeToSend' => $item['date'], 'delay' => $delay]);

                $alert[] = $this->checkForDelayedSentAlerts($item['count'], $item['step'], $item['date']);
                if ($delaySentCommunication) {
                    $delaySentCommunication->countSent = $item['count'];
                    $delaySentCommunication->save();
                } else {
                    $this->data[] = [$item['count'], $delay, $item['step'], $item['date']];
                }

                $this->savePartDataForDelaySentCommunication();
            }

            $delay += 30;
        }

        $this->saveDataForDelaySentCommunication();

        $this->sendAlerts($alerts);
    }

    /**
     * @param $dateStart
     * @param $dateEnd
     * @param $isSent
     * @param $isSend
     * @return array
     */
    private function getCountInSchedule($dateStart, $dateEnd, $isSent = false, $isSend = false)
    {
        $query = (new Query())
            ->select(new Expression('count(id) as count, FROM_UNIXTIME(FLOOR( UNIX_TIMESTAMP(timeToSend)/900 ) * 900) as date, step'))
            ->from(CommunicationSchedule::tableName())
            ->andWhere(['>=', 'timeToSend', $dateStart])
            ->andWhere(['<=', 'timeToSend', $dateEnd])
            ->groupBy('date, step')
            ->orderBy('date');

        if ($isSent) {
            $query->andWhere(['sentAt' => 0]);
        }

        if ($isSend) {
            $query->andWhere('sendStartedAt > "0000:00:00 00:00:00"');
        }

        $result = $query->all();

        return $result;
    }

    private function savePartDataForPlannedCommunication()
    {
        if (count($this->data) >= 50) {
            $this->saveDataForPlannedCommunication();
        }
    }

    private function saveDataForPlannedCommunication()
    {
        if ($this->data) {
            \Yii::$app->db->createCommand()->batchInsert(
                PlannedCommunication::tableName(),
                ['countToSend', 'countNotSent', 'countSent', 'step', 'timeToSend'],
                $this->data
            )->execute();

            $this->data = [];
        }
    }

    /**
     * @param $dateStart
     * @param $dateEnd
     * @param $delay
     * @return array
     */
    private function getCountSentWithDelay($dateStart, $dateEnd, $delay)
    {
        $query = (new Query())
            ->select(new Expression('count(id) as count, FROM_UNIXTIME(FLOOR( UNIX_TIMESTAMP(timeToSend)/900 ) * 900) as date, step'))
            ->from(CommunicationSchedule::tableName())
            ->andWhere(['>=', 'timeToSend', $dateStart])
            ->andWhere(['<=', 'timeToSend', $dateEnd])
            ->andWhere(new Expression('timeToSend<=FROM_UNIXTIME(FLOOR(UNIX_TIMESTAMP(sentAt))-:delay)'), [':delay' => $delay])
            ->groupBy('date, step')
            ->orderBy('date');

        $result = $query->all();

        return $result;
    }

    private function savePartDataForDelaySentCommunication()
    {
        if (count($this->data) >= 50) {
            $this->saveDataForDelaySentCommunication();
        }
    }

    private function saveDataForDelaySentCommunication()
    {
        if ($this->data) {
            \Yii::$app->db->createCommand()->batchInsert(
                DelaySentCommunication::tableName(),
                ['countSent', 'delay', 'step', 'timeToSend'],
                $this->data
            )->execute();

            $this->data = [];
        }
    }

    private function checkForScheduledAlerts($countInSchedule, $step, $date)
    {
        switch($step) {
            case 0:
                $scheduledLimit = self::SCHEDULED_LIMIT_STEP_0;
                break;
            case 1:
                $scheduledLimit = self::SCHEDULED_LIMIT_STEP_1;
                break;
            default:
                return null;
        }

        if ($countInSchedule < $scheduledLimit) {
            return $date . ' Low level of scheduled items on step ' . $step;
        }

        return null;
    }

    private function checkForNotSentAlerts($countNotSent, $step, $date)
    {
        switch($step) {
            case 0:
                $notSentLimit = self::NOT_SENT_LIMIT_STEP_0;
                break;
            case 1:
                $notSentLimit = self::NOT_SENT_LIMIT_STEP_1;
                break;
            default:
                return null;
        }

        if ($countNotSent > $notSentLimit) {
            return $date . ' Too many not sent on step ' . $step;
        }

        return null;
    }

    private function checkForDelayedSentAlerts($countWithDelay, $step, $date)
    {
        switch($step) {
            case 0:
                $delayedSentLimit = self::DELAYED_SENT_LIMIT_STEP_0;
                break;
            case 1:
                $delayedSentLimit = self::DELAYED_SENT_LIMIT_STEP_1;
                break;
            default:
                return null;
        }

        if ($countWithDelay > $delayedSentLimit) {
            return $date . ' Too many delayed on step ' . $step;
        }

        return null;
    }

    private function sendAlerts(array $alerts)
    {
        if ($this->smsEnable == false) {
            return;
        }

        $alerts = array_filter($alerts);

        if (empty($alerts)) {
            return;
        }

        $alerts = implode('. ', $alerts);
        $alerts = substr($alerts, 0, 70);

        \Yii::$app->sms->sendMessage($alerts);
    }
}