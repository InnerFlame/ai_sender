<?php

namespace app\components\phoenix\report;

use app\models\phoenix\SenderBanStat;
use app\models\phoenix\SenderProfile;
use yii\base\Component;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class SenderBanStatComponent extends Component
{
    public function getSenderBanStat($dataFilters)
    {
        $query = (new Query())->from(SenderBanStat::tableName())
            ->where(['>=', 'bannedAt', $dataFilters['dateFrom']])
            ->andWhere(['<=', 'bannedAt', $dataFilters['dateTo'] . ' 23:59:59']);

        if (!empty($dataFilters['country'])) {
            $query->andWhere(['country' => $dataFilters['country']]);
        }

        if ($dataFilters['dateFrom'] == $dataFilters['dateTo']) {
            if ($dataFilters['group'] == 'hour') {
                $query->select(new Expression('SUM(countBanned) as count, DATE_FORMAT(bannedAt, "%H:00") as date'));
            } else {
                $query->select(new Expression('SUM(countBanned) as count, DATE_FORMAT(bannedAt, "%H:%i") as date'));
            }
        } else {
            $query->select(new Expression('SUM(countBanned) as count, DATE_FORMAT(bannedAt, "%Y-%m-%d") as date'));
        }

        $query->groupBy('date');

        $result = ArrayHelper::index($query->all(), 'date');

        return \Yii::$app->dataFormatter->formatRpcErrorLogStat(
            $dataFilters['dateFrom'],
            $dataFilters['dateTo'],
            $dataFilters['group'],
            $result
        );
    }



    public function collectSenderBanStat($dateStart, $dateEnd)
    {
        $countSenderBan = $this->getCountSenderBan($dateStart, $dateEnd);

        foreach ($countSenderBan as $key => $item) {
            $senderBanStat = SenderBanStat::findOne([
                'country'  => $item['country'],
                'bannedAt' => $item['bannedAt']
            ]);

            if ($senderBanStat) {
                $senderBanStat->countBanned = $item['countBanned'];
                $senderBanStat->save();

                unset($countSenderBan[$key]);
            }
        }

        \Yii::$app->db->createCommand()->batchInsert(
            SenderBanStat::tableName(),
            ['countBanned', 'country', 'bannedAt'],
            $countSenderBan
        )->execute();
    }

    private function getCountSenderBan($dateStart, $dateEnd)
    {
        $query = (new Query())->select(new Expression('count(id) as countBanned, country, FROM_UNIXTIME(FLOOR( UNIX_TIMESTAMP(bannedAt)/900 ) * 900) as bannedAt'))
            ->from(SenderProfile::tableName())
            ->where(['>=', 'bannedAt', $dateStart])
            ->andWhere(['<=', 'bannedAt', $dateEnd])
            ->groupBy('country, bannedAt');

        $result = $query->all();

        return $result;
    }
}