<?php

namespace app\components\phoenix\report;


use app\models\phoenix\AnswerRateStat;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\IncomingMessage;
use app\models\phoenix\SenderProfile;
use yii\base\Component;

class AnswersRateStatComponent extends Component
{
    public function aggregateStatData($dateStart, $dateEnd)
    {
        $data        = $this->getCommunicationScheduleData($dateStart, $dateEnd);

        foreach ($data as $record) {
            /**@var $model AnswerRateStat */
            if ($model = $this->findDuplicateRecord($record)){
                $model->initials        = $record['initials'];
                $model->answered        = $record['answered'];
                $model->restartNumber   = $record['restartNumber'];
            } else {
                $model = new AnswerRateStat();
                $model->initials        = $record['initials'];
                $model->answered        = $record['answered'];
                $model->sentAt          = $record['sentAt'];
                $model->groupId         = $record['groupId'];
                $model->schemeId        = $record['schemeId'];
                $model->country         = $record['country'];
                $model->restartNumber   = $record['restartNumber'];
            }

            $model->save();
        }

    }

    /**
     * @param $dateStart
     * @param $dateEnd
     * @return array
     */
    private function getCommunicationScheduleData($dateStart, $dateEnd): array
    {
        $query = \Yii::$app->db->createCommand("
            SELECT
                COUNT(cSchedule.id)                       AS `initials`,
                COUNt(messages.id)                        AS `answered`,
                DATE_FORMAT(cSchedule.sentAt, '%Y-%m-%d') AS `sentAt`,
                cSchedule.groupId                         AS `groupId`,
                cSchedule.schemaId                        AS `schemeId`,
                sender.country                            AS `country`,
                cSchedule.restartNumber                   AS `restartNumber`
            FROM
                ".CommunicationSchedule::tableName()." AS `cSchedule`
            LEFT JOIN ".SenderProfile::tableName()." AS sender
            ON
                cSchedule.senderId = sender.id
            LEFT JOIN ".IncomingMessage::tableName()." AS messages
            ON
                cSchedule.recipientId = messages.recipientId AND cSchedule.senderId = messages.senderId
            WHERE
                cSchedule.sentAt > 0 
                AND 
                cSchedule.step = 0 
                AND 
                (
                    cSchedule.sentAt >= :dateStart
                ) AND (
                    cSchedule.sentAt <= :dateEnd
                )
            GROUP BY
                DATE_FORMAT(cSchedule.sentAt, '%Y-%m-%d'), cSchedule.groupId, cSchedule.schemaId, sender.country, cSchedule.restartNumber
        ", [
            'dateStart' => $dateStart,
            'dateEnd'   => $dateEnd
        ]);

        $data = $query->queryAll();

        return $data;
    }

    /**
     * @param $record
     * @return array|null|\yii\db\ActiveRecord
     */
    private function findDuplicateRecord($record)
    {
        $query = AnswerRateStat::find()
            ->where(['sentAt' => $record['sentAt']])
            ->andWhere(['country' => $record['country']])
            ->andWhere(['groupId' => $record['groupId']])
            ->andWhere(['schemeId' => $record['schemeId']])
            ->andWhere(['restartNumber' => $record['restartNumber']])
        ;

        $duplicate = $query->one();

        return $duplicate;
    }
}