<?php

namespace app\components\phoenix\report;

use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\SentCommunicationLog;
use yii\base\Component;
use yii\base\ErrorException;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class StatCommunicationComponent extends Component
{
    public function getStat($data)
    {
        $query = new Query();

        $query->from(SentCommunicationLog::tableName());

        if ($data['site']) {
            $query->andWhere(['site' => $data['site']]);
        }

        if ($data['country']) {
            $query->andWhere(['country' => $data['country']]);
        }

        if ($data['step'] || $data['step'] == '0') {
            $query->andWhere(['step' => $data['step']]);
        }

        $query->andWhere(['>=', 'dateSent', $data['dateFrom'] . ' 00:00:00']);
        $query->andWhere(['<=', 'dateSent', $data['dateTo'] . ' 23:59:59']);

        if ($data['dateFrom'] == $data['dateTo']) {
            if ($data['group'] == 'hour') {
                $query->select(new Expression('SUM(countComm) as countComm, DATE_FORMAT(dateSent, "%H:00") as date'));
            } else {
                $query->select(new Expression('SUM(countComm) as countComm, DATE_FORMAT(dateSent, "%H:%i") as date'));
            }
        } else {
            $query->select(new Expression('SUM(countComm) as countComm, DATE_FORMAT(dateSent, "%Y-%m-%d") as date'));
        }

        $query->groupBy('date');

        $result = ArrayHelper::index($query->all(), 'date');

        return \Yii::$app->dataFormatter->formatStatCommunication(
            $data['dateFrom'],
            $data['dateTo'],
            $data['group'],
            $result
        );
    }

    public function getStatGroupAllLocation($data)
    {
        $query = (new Query)
            ->select(new Expression('SUM(countComm) as count, DATE_FORMAT(dateSent, "%H:00") as date, site, country'))
            ->from(SentCommunicationLog::tableName())
            ->andWhere(['>=', 'dateSent', $data['date'] . ' 00:00:00'])
            ->andWhere(['<=', 'dateSent', $data['date'] . ' 23:59:59'])
            ->groupBy('date, country, site');

        if ($data['group'] == 'hour') {
            $query->select(new Expression('SUM(countComm) as count, DATE_FORMAT(dateSent, "%H:00") as date, site, country'));
        } else {
            $query->select(new Expression('SUM(countComm) as count, DATE_FORMAT(dateSent, "%H:%i") as date, site, country'));
        }

        $result = $query->all();

        $associativeData = [];
        $data            = [];

        foreach ($result as $item) {
            $associativeData[$item['site']][$item['country']]['date'][]  = $item['date'];
            $associativeData[$item['site']][$item['country']]['count'][] = $item['count'];
            $associativeData[$item['site']][$item['country']]['site']   = \Yii::$app->yaml->parseByKey('site', $item['site'])['name'];
            $associativeData[$item['site']][$item['country']]['country']   = $item['country'];
        }

        foreach ($associativeData as $itemSite) {
            foreach ($itemSite as $item) {
                $data[] = $item;
            }
        }

        return $data;
    }

    public function collectSentCommunication($dateStart, $dateEnd)
    {
        $communications = $this->getSentCommunicationByPeriod($dateStart, $dateEnd);

        if (empty($communications)){
            throw new ErrorException('Communication is empty on date - ' . $dateStart . PHP_EOL);
        }

        foreach ($communications as $key => $communication) {
            $sentCommunicationLog = SentCommunicationLog::findOne([
                'site'     => $communication['site'],
                'country'  => $communication['country'],
                'step'     => $communication['step'],
                'dateSent' => $communication['dateSent'],
            ]);

            if (!empty($sentCommunicationLog)) {
                $sentCommunicationLog->countComm = $communication['countComm'];
                $sentCommunicationLog->save();
                unset($communications[$key]);
            }
        }

        \Yii::$app->db->createCommand()->batchInsert(
            SentCommunicationLog::tableName(),
            ['site', 'step', 'country', 'countComm', 'dateSent'],
            $communications
        )->execute();
    }

    private function getSentCommunicationByPeriod($dateStart, $dateEnd)
    {
        $query = (new Query())
            ->select(new Expression('r.site, c.step, r.country, count(c.id) as countComm, FROM_UNIXTIME(FLOOR( UNIX_TIMESTAMP(c.sentAt)/900 ) * 900) as dateSent'))
            ->from(CommunicationSchedule::tableName() . ' as c')
            ->innerJoin(RecipientProfile::tableName() . ' as r', 'c.recipientId=r.id')
            ->where(['>=', 'c.sentAt', $dateStart])
            ->andWhere(['<=', 'c.sentAt', $dateEnd])
            ->groupBy('r.site, r.country, c.step, dateSent');

        $result = $query->all();

        return $result;
    }
}