<?php

namespace app\components\phoenix\report;

use app\models\phoenix\Group;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\IncomingMessage;
use app\models\phoenix\MatchingErrorLog;
use app\models\phoenix\ProfileMessage;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\StepMessage;
use yii\base\Component;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class TesterToolComponent
 * @package app\components\phoenix\report
 */
class TesterToolComponent extends Component
{
    /**
     * @var $remoteIds array
     * @var $originalIds array
     * @var $responseData array
     */
    private
        $remoteIds      = [],
        $originalIds    = [],
        $responseData   = [];

    /**
     * @param string $id
     * @return array
     */
    public function getData(string $id)
    {
        $this->parseId($id);
        $recipients = $this->getRecipients();

        foreach ($recipients as $recipient) {
            $errors         = [];
            $recipientId    = $recipient['id'];

            $communicationData = $this->getCommunicationDataForRecipient($recipientId);

            foreach ($communicationData as $item) {
                $communicationId = $item['communicationId'];
                $sentAt          = $item['sentAt'];

                if ($sentAt > 0) {
                    break;
                }

                if ($matchingErrorLog = $this->getRecipientCommunicationErrors($communicationId)) {
                    $errors[$communicationId] = $matchingErrorLog;
                }
            }

            $this->setResponseDataItem($recipient, $communicationData, $errors);
        }

        return $this->getResponse();
    }

    /**
     * @param string $communicationId
     * @return static[] | array
     */
    private function getRecipientCommunicationErrors(string $communicationId)
    {
        return MatchingErrorLog::findAll(['communicationId' => $communicationId]);
    }

    /**
     * @param string $recipientId
     * @return array
     */
    private function getCommunicationDataForRecipient(string $recipientId): array
    {
        $communicationData = ArrayHelper::merge(
            $this->getCommunicationsData($recipientId, true),
            $this->getCommunicationsData($recipientId, false)
        );

        ArrayHelper::multisort($communicationData, 'timeToSend', SORT_DESC);

        return $communicationData;
    }

    /**
     * @param string $id
     * @return array
     */
    private function parseId(string $id): array
    {
        if (strpos($id, ',')) {
            $ids = array_map('trim', explode(',', $id));
        } elseif (strpos($id, ' ')) {
            $ids = array_map('trim', explode(' ', $id));
        } else {
            $ids[] = trim($id);
        }

        foreach ($ids as $id) {
            if (is_numeric($id)) {
                $this->setOriginalIds($id);
            } else {
                $this->setRemoteIds($id);
            }
        }

        return $ids;
    }

    /**
     * @return array
     */
    private function getRecipients(): array
    {
        $query = (new Query())
            ->select('*')
            ->from(RecipientProfile::tableName())
            ->where(['remoteId' => $this->getRemoteIds()])
            ->orWhere(['id' => $this->getOriginalIds()]);

        $result = $query->all();

        return $result;
    }

    /**
     * @param string $recipientId
     * @param bool $withSender
     * @return array
     */
    protected function getCommunicationsData(string $recipientId, bool $withSender): array
    {
        $query = (new Query())
            ->select('
                s.id as senderId, s.screenName, s.originId,
                s.site, s.country, g.title,
                c.timeToSend, c.sendStartedAt, c.sentAt,
                c.messageId, c.id as communicationId,
                i.id as incomingMessage, c.restartNumber, c.schemaId
            ')
            ->from(CommunicationSchedule::tableName() . ' as c')
            ->leftJoin(SenderProfile::tableName() . ' as s', 's.id=c.senderId')
            ->leftJoin(Group::tableName() . ' as g', 'g.id=c.groupId')
            ->leftJoin(IncomingMessage::tableName() . ' as i', 'c.recipientId=i.recipientId AND c.senderId=i.senderId AND c.groupId=i.groupId')
            ->where(['c.recipientId' => $recipientId]);

        if ($withSender) {
            $query->andWhere(['>', 'c.senderId', 0])
                ->groupBy('c.groupId, c.senderId');
        } else {
            $query->andWhere(['c.senderId' => 0]);
        }

        $result = $query->all();

        return $result;
    }

    /**
     * @param $recipientId
     * @param $senderId
     * @return array
     */
    public function getHistoryMessages($recipientId, $senderId)
    {
        $sender    = SenderProfile::find()->where(['id' => $senderId])->asArray()->one();
        $recipient = RecipientProfile::find()->where(['id' => $recipientId])->asArray()->one();

        $initiationMessage      = $this->getInitiationMessage($recipientId, $senderId);
        $communicationSchedules = $this->getCommunicationSchedule($recipientId, $senderId);
        $incomingMessages       = $this->getIncomingMessages($recipientId, $senderId);

        if (!empty($initiationMessage)) {
            $history[] = [
                'id'            => $sender['id'],
                'originId'      => $sender['originId'],
                'who'           => 'Woman',
                'sentAt'        => $initiationMessage['sentAt'],
                'sendStartedAt' => $initiationMessage['sendStartedAt'],
                'timeToSend'    => $initiationMessage['timeToSend'],
                'body'          => $initiationMessage['body'],
                'step'          => 0,
                'color'         => '#dff0d8',
            ];
        }

        foreach ($incomingMessages as $key => $incomingMessage) {
            $history[] = [
                'id'            => $recipient['id'],
                'originId'      => $recipient['remoteId'],
                'who'           => 'Man',
                'sentAt'        => $incomingMessage['createdAt'],
                'sendStartedAt' => null,
                'timeToSend'    => null,
                'body'          => $incomingMessage['text'],
                'step'          => $incomingMessage['step'],
                'color'         => '#eff4ff',
            ];
        }

        foreach ($communicationSchedules as $communicationSchedule) {
            $history[] = [
                'id'            => $sender['id'],
                'originId'      => $sender['originId'],
                'who'           => 'Woman',
                'sentAt'        => $communicationSchedule['sentAt'],
                'sendStartedAt' => $communicationSchedule['sendStartedAt'],
                'timeToSend'    => $communicationSchedule['timeToSend'],
                'body'          => $communicationSchedule['body'],
                'step'          => $communicationSchedule['step'],
                'color'         => '#dff0d8',
            ];
        }

        ArrayHelper::multisort($history, ['step', 'who', 'date']);

        return $history;
    }

    /**
     * @param $recipientId
     * @param $senderId
     * @return array|\yii\db\ActiveRecord[]
     */
    private function getIncomingMessages($recipientId, $senderId)
    {
        return IncomingMessage::find()
            ->where([
                'recipientId'   => $recipientId,
                'senderId'      => $senderId
            ])
            ->orderBy('step')
            ->all();
    }

    /**
     * @param $recipientId
     * @param $senderId
     * @return array
     */
    private function getCommunicationSchedule($recipientId, $senderId)
    {
        $query = (new Query())
            ->select('m.body, c.sentAt, c.step, c.timeToSend, c.sendStartedAt')
            ->from(CommunicationSchedule::tableName() . ' as c')
            ->innerJoin(StepMessage::tableName() . ' as m', 'c.messageId=m.id')
            ->where([
                'c.recipientId' => $recipientId,
                'c.senderId'    => $senderId,
            ])
            ->andWhere(['>', 'c.step', 0])
            ->orderBy('c.step');

        $result = $query->all();

        return $result;
    }

    /**
     * @param $recipientId
     * @param $senderId
     * @return array|bool
     */
    private function getInitiationMessage($recipientId, $senderId)
    {
        $query = (new Query())
            ->select('m.body, c.sentAt, c.timeToSend, c.sendStartedAt')
            ->from(CommunicationSchedule::tableName() . ' as c')
            ->innerJoin(ProfileMessage::tableName() . ' as m', 'c.messageId=m.id')
            ->where([
                'c.recipientId' => $recipientId,
                'c.senderId'    => $senderId,
                'c.step'        => 0,
            ]);

        $result = $query->one();

        return $result;
    }

    /**
     * @param array $recipientData
     * @param array $communicationData
     * @param array $errors
     */
    public function setResponseDataItem(array $recipientData, array $communicationData, array $errors)
    {
        $this->responseData[] = [
            'recipient'         => $recipientData,
            'communicationData' => $communicationData,
            'errors'            => $errors
        ];
    }

    /**
     * @return array
     */
    public function getResponse(): array
    {
        return $this->responseData;
    }

    /**
     * @param $value
     */
    public function setRemoteIds($value)
    {
        $this->remoteIds[] = $value;
    }

    /**
     * @return array
     */
    public function getRemoteIds(): array
    {
        return $this->remoteIds;
    }

    /**
     * @param $value
     */
    public function setOriginalIds($value)
    {
        $this->originalIds[] = $value;
    }

    /**
     * @return array
     */
    public function getOriginalIds(): array
    {
        return $this->originalIds;
    }
}
