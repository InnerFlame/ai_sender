<?php

namespace app\components\phoenix\report;

use yii\helpers\ArrayHelper;

class DataFormatterComponent
{
    public function formatForDelayStat($dateFrom, $dateTo, $groupBy, $countScheduled, $countDelaySent)
    {
        $data      = [];
        $dataDates = $this->getDataDates($dateFrom, $dateTo, $groupBy);

        if (empty($dataDates)) {
            return [];
        }

        for ($i = $dataDates['dateStart']; $i < $dataDates['dateEnd']; $i += $dataDates['increment']) {
            $key = date($dataDates['dateFormat'], $i);
            $countToSend  = 0;
            $delay        = 0;
            $countNotSent = 0;
            $countSent    = 0;

            if (!empty($countScheduled[$key])) {
                $countToSend    = $countScheduled[$key]['countToSend'];
                $countNotSent   = $countScheduled[$key]['countNotSent'];
                $countSent = $countScheduled[$key]['countSent'];

                if (!empty($countDelaySent[$key])) {
                    $delay = $countDelaySent[$key]['count'];
                }
            }

            $data[] = [
                'countSchedule'      => $countToSend,
                'countSentWithDelay' => $delay,
                'countNotSent'       => $countNotSent,
                'countSent'          => $countSent,
                'date'               => $key,
            ];
        }

        return $data;
    }

    public function formatCoverageFatal($dateFrom, $dateTo, $data)
    {
        $result    = [];
        $dataDates = $this->getDataDates($dateFrom, $dateTo, 'day');

        if (empty($dataDates)) {
            return [];
        }

        for ($i = $dataDates['dateStart']; $i < $dataDates['dateEnd']; $i += $dataDates['increment']) {
            $key = date($dataDates['dateFormat'], $i);
            $countToSend  = 0;
            $countNotSent = 0;

            if (!empty($data[$key])) {
                $countToSend  = $data[$key]['countGrab'];
                $countNotSent = $data[$key]['countNotSent'];
            }

            $result[] = [
                'countGrab'    => $countToSend,
                'countNotSent' => $countNotSent,
                'date'         => $key,
            ];
        }

        return $result;
    }

    public function formatStatCommunication($dateFrom, $dateTo, $groupBy, $data)
    {
        $result    = [];
        $dataDates = $this->getDataDates($dateFrom, $dateTo, $groupBy);

        if (empty($dataDates)) {
            return [];
        }

        for ($i = $dataDates['dateStart']; $i < $dataDates['dateEnd']; $i += $dataDates['increment']) {
            $key = date($dataDates['dateFormat'], $i);
            $countComm = 0;

            if (!empty($data[$key])) {
                $countComm  = $data[$key]['countComm'];
            }

            $result[] = [
                'countComm'    => $countComm,
                'date'         => $key,
            ];
        }

        return $result;
    }

    public function formatRpcErrorLogStat($dateFrom, $dateTo, $groupBy, $data)
    {
        $result    = [];
        $dataDates = $this->getDataDates($dateFrom, $dateTo, $groupBy);

        if (empty($dataDates)) {
            return [];
        }

        for ($i = $dataDates['dateStart']; $i < $dataDates['dateEnd']; $i += $dataDates['increment']) {
            $key = date($dataDates['dateFormat'], $i);
            $count = 0;

            if (!empty($data[$key])) {
                $count  = $data[$key]['count'];
            }

            $result[] = [
                'count' => $count,
                'date'  => $key,
            ];
        }

        return $result;
    }

    public function formatRegistrationStat($dateFrom, $dateTo, $groupBy, $countInSchedule, $countRegistered)
    {
        $dataDates = $this->getDataDates($dateFrom, $dateTo, $groupBy);
        if (empty($dataDates)) {
            return [];
        }

        for ($i = $dataDates['dateStart']; $i < $dataDates['dateEnd']; $i += $dataDates['increment']) {
            $key           = date($dataDates['dateFormat'], $i);
            $all           = 0;
            $registered    = 0;
            $notRegistered = 0;

            if (!empty($countInSchedule[$key])) {
                if (!empty($countRegistered[$key])) {
                    $registered = $countRegistered[$key]['count'];
                } else {
                    $registered = 0;
                }
                $all           = $countInSchedule[$key]['count'];
                $notRegistered = $all - $registered;
            }

            $result[] = [
                'countInSchedule'    => $all,
                'countRegistered'    => $registered,
                'countNotRegistered' => $notRegistered,
                'date'               => $key,
            ];
        }

        return $result;
    }

    public function formatResendingStat($dateFrom, $dateTo, $groupBy, $data)
    {
        $data      = ArrayHelper::index($data, null, 'entity');
        $dataDates = $this->getDataDates($dateFrom, $dateTo, $groupBy);
        if (empty($dataDates)) {
            return ['date' => []];
        }

        if ($groupBy == 'hour') {
            $dataDates['dateEnd'] += 3600;
        }
        foreach ($data as $key => $item) {
            $dataTmp = ArrayHelper::index($data[$key], 'date');
            for ($i = $dataDates['dateStart']; $i < $dataDates['dateEnd']; $i += $dataDates['increment']) {
                $dateKey = date($dataDates['dateFormat'], $i);
                if (!empty($dataTmp[$dateKey])) {
                    $result['result'][$key][] = (int)$dataTmp[$dateKey]['count'];
                } else {
                    $result['result'][$key][] = 0;
                }
            }
        }

        if (empty($result)) {
            return ['date' => []];
        }

        for ($i = $dataDates['dateStart']; $i < $dataDates['dateEnd']; $i += $dataDates['increment']) {
            $result['date'][] = date($dataDates['dateFormat'], $i);
        }

        return $result;
    }

    private function getDataDates($dateFrom, $dateTo, $groupBy)
    {
        $dateStart = strtotime($dateFrom);
        $dateEnd   = strtotime($dateTo . ' 23:59:59');

        if ($dateFrom == $dateTo) {
            $dateStart = strtotime($dateFrom);
            if ($dateTo == date('Y-m-d')) {
                if ($groupBy == 'hour') {
                    $dateEnd = strtotime(date('Y-m-d H:i:s')) - 3600;
                } elseif ($groupBy == 'day') {
                    $dateEnd = strtotime(date('Y-m-d H:i:s'));
                } else {
                    $dateEnd = strtotime(date('Y-m-d H:i:s')) - 900;
                }
            } elseif ($dateTo > date('Y-m-d')) {
                return [];
            }

            if ($groupBy == 'hour') {
                $increment = 3600;
                $dateFormat = 'H:i';
            } elseif ($groupBy == 'day') {
                $increment  = 86400;
                $dateFormat = 'Y-m-d';
            } else {
                $increment  = 900;
                $dateFormat = 'H:i';
            }
        } else {
            if ($dateTo > date('Y-m-d')) {
                $dateEnd = strtotime(date('Y-m-d 23:59:59'));
            }

            $increment  = 86400;
            $dateFormat = 'Y-m-d';
        }

        return [
            'increment'  => $increment,
            'dateStart'  => $dateStart,
            'dateEnd'    => $dateEnd,
            'dateFormat' => $dateFormat
        ];
    }
}
