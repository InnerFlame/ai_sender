<?php

namespace app\components\phoenix\report;


use app\models\phoenix\RpcErrorLog;
use app\models\rpc\RpcLog;
use yii\base\Component;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class RpcErrorLogComponent extends Component
{
    private $dataToSave = [];

    /**
     * Return stat from aggregated table rpcErrorLog
     * @param $dataFilters
     * @return array
     */
    public function getRpcErrorLogStat($dataFilters)
    {
        $query = (new Query())->from(RpcErrorLog::tableName())
            ->where(['>=', 'errorDate', $dataFilters['dateFrom']])
            ->andWhere(['<=', 'errorDate', $dataFilters['dateTo'] . ' 23:59:59']);

        if (!empty($dataFilters['method'])) {
            $query->andWhere(['method' => $dataFilters['method']]);
        }

        if (!empty($dataFilters['error'])) {
            $query->andWhere(['error' => $dataFilters['error']]);
        }

        if ($dataFilters['dateFrom'] == $dataFilters['dateTo']) {
            if ($dataFilters['group'] == 'hour') {
                $query->select(new Expression('SUM(errorCount) as count, DATE_FORMAT(errorDate, "%H:00") as date'));
            } else {
                $query->select(new Expression('SUM(errorCount) as count, DATE_FORMAT(errorDate, "%H:%i") as date'));
            }
        } else {
            $query->select(new Expression('SUM(errorCount) as count, DATE_FORMAT(errorDate, "%Y-%m-%d") as date'));
        }

        $query->groupBy('date');

        $result = ArrayHelper::index($query->all(), 'date');

        return \Yii::$app->dataFormatter->formatRpcErrorLogStat(
            $dataFilters['dateFrom'],
            $dataFilters['dateTo'],
            $dataFilters['group'],
            $result
        );
    }

    /**
     * Aggregation error rpc from table rpcLog
     * @param $dateStart
     * @param $dateEnd
     */
    public function collectRpcErrorLog($dateStart, $dateEnd)
    {
        $rpcErrors = $this->getRpcErrors($dateStart, $dateEnd);

        foreach ($rpcErrors as $item) {
            $rpcError = RpcErrorLog::findOne([
                'errorDate' => $item['errorDate'],
                'method'    => $item['method'],
                'error'     => $item['error'],
            ]);

            if ($rpcError) {
                $rpcError->errorCount = $item['errorCount'];
                $rpcError->save();
            } else {
                $this->dataToSave[] = [$item['method'], $item['error'], $item['errorCount'], $item['errorDate']];
            }

            $this->savePartDataToRpcErrorLog();
        }

        $this->saveDataToRpcErrorLog();
    }

    /**
     * Delete old data from table rpcErrorLog
     * @throws \yii\db\Exception
     */
    public function deleteOlRpcErrorLog()
    {
        \Yii::$app->db->createCommand()->delete(
            RpcErrorLog::tableName(),
            'errorDate <= :errorDate',
            [
                'errorDate' => date('Y-m-d', time() - 3600 * 24 * 14)
            ]
        )->execute();
    }

    /**
     * return all rpc error from table rpcLog
     * @param $dateStart
     * @param $dateEnd
     * @return array
     */
    private function getRpcErrors($dateStart, $dateEnd)
    {
        $data  = [];
        $query = (new Query())->select(new Expression('params, FROM_UNIXTIME(FLOOR( UNIX_TIMESTAMP(createdAt)/900 ) * 900) as dateError'))
            ->from(RpcLog::tableName())
            ->where(['>=', 'createdAt', $dateStart])
            ->andWhere(['<=', 'createdAt', $dateEnd])
            ->andWhere(['method' => RpcLog::METHOD_ERROR]);

        foreach ($query->batch(100) as $rpcErrors) {
            foreach ($rpcErrors as $rpcError) {
                $params = json_decode($rpcError['params'], true);
                if (empty($params)) {
                    continue;
                }

                $errorType = $this->getError($params['incomingData']['error']);
                $key       = $errorType . $params['rpcCallInfo']['method'] . $rpcError['dateError'];
                if (empty($data[$key])) {
                    $data[$key] = [
                        'errorCount' => 0,
                        'errorDate'  => '',
                        'method'     => '',
                        'error'      => '',
                    ];
                }

                $data[$key]['errorCount'] += 1;
                $data[$key]['errorDate']   = $rpcError['dateError'];
                $data[$key]['method']      = $params['rpcCallInfo']['method'];
                $data[$key]['error']       = $errorType;
            }
        }

        return $data;
    }

    /**
     * return type of error
     * @param $errorText
     * @return int|string
     */
    private function getError($errorText)
    {
        $errors = RpcErrorLog::getErrorsType();

        if (is_array($errorText)) {
            $errorText = json_encode($errorText);
        }

        foreach ($errors as $key => $error) {
            if (stripos($errorText, $error) !== false) {
                return $key;
            }
        }

        $error = $errorText . '-' . date('Y-m-d H:i:s') . PHP_EOL;
        file_put_contents(\Yii::$app->basePath . '/runtime/logs/unknownErrorRpcErrorLog.txt', $error, FILE_APPEND);

        return 'unknownError';
    }

    private function savePartDataToRpcErrorLog()
    {
        if (count($this->dataToSave) >= 50) {
            $this->saveDataToRpcErrorLog();
        }
    }

    private function saveDataToRpcErrorLog()
    {
        if ($this->dataToSave) {
            \Yii::$app->db->createCommand()->batchInsert(
                RpcErrorLog::tableName(),
                ['method', 'error', 'errorCount', 'errorDate'],
                $this->dataToSave
            )->execute();

            $this->dataToSave = [];
        }
    }
}