<?php

namespace app\components\phoenix\report;

use app\models\phoenix\MatchingErrorLog;
use yii\base\Component;
use yii\db\Expression;
use yii\db\Query;

class ResendingStatComponent extends Component
{
    public function getStat($dataFilters)
    {
        if (empty($dataFilters['date'])) {
            $dataFilters['date'] = date('Y-m-d');
        }

        $query = (new Query())
            ->select(new Expression('count(id) as count, entity'))
            ->from(MatchingErrorLog::tableName())
            ->where(['>=', 'createdAt', $dataFilters['dateFrom']])
            ->andWhere(['<=', 'createdAt', $dataFilters['dateTo'] . ' 23:59:59'])
            ->groupBy('date, entity');

        if ($dataFilters['dateFrom'] == $dataFilters['dateTo']) {
            $groupBy = 'hour';
            $query->addSelect(new Expression('DATE_FORMAT(createdAt, "%H:00") as date'));
        } else {
            $groupBy = 'day';
            $query->addSelect(new Expression('DATE_FORMAT(createdAt, "%Y-%m-%d") as date'));
        }

        if (!empty($dataFilters['country'])) {
            $query->andWhere(['country' => $dataFilters['country']]);
        }

        $result = $query->all();

        return \Yii::$app->dataFormatter->formatResendingStat(
            $dataFilters['dateFrom'],
            $dataFilters['dateTo'],
            $groupBy,
            $result
        );
    }
}
