<?php

namespace app\components\phoenix\report;

use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileRegister;
use yii\base\Component;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class RegistrationStatComponent extends Component
{
    private $data = [];

    public function getStat($dataFilters)
    {
        $countInSchedule = ArrayHelper::index($this->getCountInSchedule($dataFilters), 'date');
        $countRegistered = ArrayHelper::index($this->getCountRegistered($dataFilters), 'date');

        return \Yii::$app->dataFormatter->formatRegistrationStat(
            $dataFilters['dateFrom'],
            $dataFilters['dateTo'],
            $dataFilters['group'],
            $countInSchedule,
            $countRegistered
        );
    }

    private function getCountInSchedule($dataFilters)
    {
        $query  = $this->getQuery($dataFilters);
        $result = $query->all();

        return $result;
    }

    private function getCountRegistered($dataFilters)
    {
        $query = $this->getQuery($dataFilters);
        $query->andWhere('sr.registeredAt>"0000-00-00 00:00:00"')
            ->andWhere([
                'photoStatus' => SenderProfile::STATUS_APPROVED,
                'bannedAt'    => 0
            ]);

        $result = $query->all();

        return $result;
    }

    private function getQuery($dataFilters): Query
    {
        $query = (new Query())
            ->from(SenderProfile::tableName() . ' as s')
            ->innerJoin(SenderProfileRegister::tableName() . ' as sr', 's.id=sr.profileId')
            ->where(['>=', 'sr.timeToRegister', $dataFilters['dateFrom']])
            ->andWhere(['<=', 'sr.timeToRegister', $dataFilters['dateTo'] . '23:59:59'])
            ->groupBy('date')
            ->orderBy('date');

        if (!empty($dataFilters['country'])) {
            $query->andWhere(['s.country' => $dataFilters['country']]);
        }

        if ($dataFilters['dateFrom'] == $dataFilters['dateTo']) {
            if ($dataFilters['group'] == 'hour') {
                $query->select(new Expression('count(s.id) as count, DATE_FORMAT(sr.timeToRegister, "%H:00") as date'));
            } else {
                $query->select(new Expression('count(s.id) as count, DATE_FORMAT(sr.timeToRegister, "%Y-%m-%d") as date'));
            }
        } else {
            $query->select(new Expression('count(s.id) as count, DATE_FORMAT(sr.timeToRegister, "%Y-%m-%d") as date'));
        }

        return $query;
    }

}