<?php

namespace app\components\phoenix\report;

use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\UnprocessedRecipient;
use yii\base\Component;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class UnprocessedRecipientStatsComponent extends Component
{
    public function getCoverageFatal($post)
    {
        $query = (new Query())
            ->select('SUM(countGrab) as countGrab, SUM(countNotSent) as countNotSent, grabbedAt as date')
            ->from(UnprocessedRecipient::tableName())
            ->where(['>=', 'grabbedAt', $post['dateFrom']])
            ->andWhere(['<=', 'grabbedAt', $post['dateTo']])
            ->groupBy('date')
            ->orderBy('date');

        if ($post['country']) {
            $query->andWhere(['country' => $post['country']]);
        }

        $result = ArrayHelper::index($query->all(), 'date');

        return \Yii::$app->dataFormatter->formatCoverageFatal(
            $post['dateFrom'],
            $post['dateTo'],
            $result
        );
    }

    public function collectUnprocessedRecipient($dateStart, $dateEnd)
    {
        $dataForUnprocessedRecipient = $this->getDataForUnprocessedRecipient($dateStart, $dateEnd);
        foreach ($dataForUnprocessedRecipient as $item) {
            $unprocessedRecipient = UnprocessedRecipient::findOne([
                'grabbedAt' => $dateStart,
                'country' => $item['country']
            ]);
            if (empty($unprocessedRecipient)) {
                $unprocessedRecipient = new UnprocessedRecipient();
                $unprocessedRecipient->grabbedAt = $dateStart;
                $unprocessedRecipient->country = $item['country'];
            }
            $unprocessedRecipient->countGrab = $item['countGrab'];
            $unprocessedRecipient->countNotSent = $item['countNotSent'];
            $unprocessedRecipient->save();
        }
    }

    private function getDataForUnprocessedRecipient($dateStart, $dateEnd)
    {
        $query = (new Query())
            ->select('count(DISTINCT(id)) as countGrab, country')
            ->from(RecipientProfile::tableName())
            ->where(['>=', 'createdAt', $dateStart])
            ->andWhere(['<=', 'createdAt', $dateEnd])
            ->groupBy('country');

        $grabStats = $query->all();

        $query = (new Query())
            ->select('count(DISTINCT(r.id)) as countNotSent, country')
            ->from(RecipientProfile::tableName() . ' as r')
            ->leftJoin(CommunicationSchedule::tableName() . ' as c', 'r.id=c.recipientId')
            ->where(['>=', 'r.createdAt', $dateStart])
            ->andWhere(['<=', 'r.createdAt', $dateEnd])
            ->andWhere('c.id IS NULL')
            ->groupBy('country');

        $notSentStats = $query->all();
        $notSentStats = ArrayHelper::index($notSentStats, 'country');

        foreach ($grabStats as &$item) {
            $item['countNotSent'] = $notSentStats[$item['country']]['countNotSent'] ?? 0;
        }

        return $grabStats;
    }
}
