<?php

namespace app\components\phoenix;

use app\config\Constant;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use app\models\rpc\Container;
use app\models\rpc\RpcLog;
use yii\base\Component;

class RpcErrorComponent extends Component
{
    const QUEUE = 'rpcErrorResponse';

    /** @var int $delayTime */
    public $delayTime;

    public function processError()
    {
        \Yii::$app->internalAmqp->consumeEvents(self::QUEUE, [$this, 'errorHandler']);
    }

    public function errorHandler(array $message)
    {
        $rpcCallInfo  = $message['rpcCallInfo'];
        $incomingData = $message['incomingData'];

        $this->logError($rpcCallInfo, $incomingData);

        if ($this->isProxyError($incomingData)) {
            \Yii::$container->get('zabbixProxyError')->incrementCountError();
            $this->restartRequest($rpcCallInfo);
            return true;
        }

        if (empty($rpcCallInfo['method'])) {
            return false;
        }

        $params = $rpcCallInfo['params'][0] ?? null;

        switch ($rpcCallInfo['method']) {
            case 'start':
                $this->errorClient($rpcCallInfo, $incomingData);
                break;
            case 'createContainer':
                $this->processStartedState($params, $incomingData);
                break;
            case 'register':
            case 'photoUpload':
                \Yii::$container->get('registerRpcError')->processingError($rpcCallInfo, $incomingData);
                break;
            case 'login':
                $this->loginError($rpcCallInfo, $incomingData);
                break;
            case 'killContainer':
                $this->killContainerError($rpcCallInfo);
                break;
            case 'sendMessage':
                $this->processSendMessageError($rpcCallInfo, $incomingData);
                break;
            default:
                return false;
        }

        return true;
    }

    public function restartRequest(array $rpcCallInfo)
    {
        usleep(500000);

        $key = 'rpcCallRestartsCount.' . $rpcCallInfo['correlationId'];
        $triesCount = \Yii::$app->redis->get($key);
        if (empty($triesCount)) {
            \Yii::$app->redis->setex($key, 60, 1);
        } else {
            \Yii::$app->redis->incr($key);
        }

        if (!empty($triesCount) && $triesCount > 4) {
            return;
        }

        $additional = $rpcCallInfo['additional'];

        \Yii::$app->rpcClient->rpcCall(
            'client.' . $additional['clientName'],
            $rpcCallInfo['method'],
            $rpcCallInfo['params'],
            $additional
        );
    }

    private function loginError(array $rpcCallInfo, array $incomingData)
    {
        if ($incomingData['error'] == 'already logged in') {
            \Yii::$app->rpcClient->setRpcClientLogin(
                $rpcCallInfo['additional']['clientName'],
                $rpcCallInfo['additional']['target']
            );

            switch ($rpcCallInfo['additional']['clientType']) {
                case Client::TYPE_OBSERVE:
                case Client::TYPE_COMMUNICATION:
                    \Yii::$app->rpcClient->rpcCall(
                        'client.' . $rpcCallInfo['additional']['clientName'],
                        'start',
                        [],
                        $rpcCallInfo['additional']
                    );
                    break;
                case Client::TYPE_SEARCH:
                    \Yii::$app->apiSearch->processSearch($rpcCallInfo['additional']['profileId']);
                    break;
            }
        }

        if (is_string($incomingData['error'])) {
            $error = json_decode($incomingData['error'], true);
            if (!empty($error['meta']['code']) && $error['meta']['code'] == 481) {
                \Yii::$app->rpcClient->stopRpcContainer(
                    $rpcCallInfo['additional']['clientName'],
                    $rpcCallInfo['additional']['target']
                );
            } elseif (!empty($error['meta']['code'])
                && $error['meta']['code'] == 302
                && !empty($error['meta']['redirect'][0])
                && $error['meta']['redirect'][0] == '/user/massBlocked/protect/login'
            ) {
                \Yii::$app->rpcClient->stopRpcContainer(
                    $rpcCallInfo['additional']['clientName'],
                    $rpcCallInfo['additional']['target']
                );
                \Yii::$app->rpcClient->freezeClient($rpcCallInfo['additional']['clientName']);
            }
        }
    }

    private function killContainerError(array $rpcCallInfo)
    {
        \Yii::$app->rpcClient->deleteContainerClient($rpcCallInfo['additional']['clientName']);
    }

    /**
     * @param $incomingData
     * @return string|null
     */
    private function getClientName(array $incomingData)
    {
        if (!empty($incomingData['additional']['clientName'])) {
            return $incomingData['additional']['clientName'];
        } elseif (!empty($incomingData['params'][0]['id'])) {
            return $incomingData['params'][0]['id'];
        }

        return null;
    }

    /**
     * @param $rpcCallInfo
     * @param $incomingData
     * @return bool
     */
    private function errorClient(array $rpcCallInfo, array $incomingData)
    {
        switch ($incomingData['error']) {
            case 'already started':
                \Yii::$app->rpcClient->setRpcClientStarted(
                    $rpcCallInfo['additional']['clientName'],
                    $rpcCallInfo['additional']['target']
                );
                break;
            case 'not logged in':
                \Yii::$app->rpcClient->stopRpcContainer(
                    $rpcCallInfo['additional']['clientName'],
                    $rpcCallInfo['additional']['target']
                );
                break;
            default:
                return false;
        }

        return true;
    }

    private function processStartedState(array $rpcCallInfo, array $incomingData)
    {
        if ($incomingData['error'] == 'already started') {
            if (empty($rpcCallInfo)) {
                return false;
            }

            if (empty($rpcCallInfo['id'])) {
                return false;
            }

            //Kostilation to detect client or container already exists
            if (!empty($rpcCallInfo['username'])) {
                if ($incomingData['result'] == 'OFFLINE') {
                    \Yii::$app->rpcClient->setRpcClientStopped($rpcCallInfo['id'], $rpcCallInfo['type']);
                } else {
                    \Yii::$app->rpcClient->setRpcClientStarted($rpcCallInfo['id'], $rpcCallInfo['type']);
                }
            } else {
                list($target, $id) = explode('.', $rpcCallInfo['id']);

                $profile = SenderProfile::findOne($id);

                if (empty($profile)) {
                    return false;
                }

                \Yii::$app->rpcClient->setRpcContainerStarted($profile, $rpcCallInfo['id'], $rpcCallInfo['type']);

                $container = Container::findOne([
                    'name'   => $rpcCallInfo['id'],
                    'target' => $rpcCallInfo['type'],
                ]);

                if (empty($container)) {
                    return false;
                }

                switch ($container->type) {
                    case Container::TYPE_SEARCH:
                        \Yii::$app->searcher->loginClient($profile);
                        break;
                    case Container::TYPE_COMMUNICATION:
                        \Yii::$container->get('commProcessor')->loginClient($profile, $rpcCallInfo['type']);
                        break;
                    default:
                        return true;
                }
            }

            return true;
        }

        if (!empty($incomingData['error']['code']) && $incomingData['error']['code'] == 'ENOMEM') {
            \Yii::$app->cache->set('delayTime', time() + $this->delayTime);

            $error = json_encode($incomingData['error']) . ' - ' . date('Y-m-d H:i:s') . "\n";
            file_put_contents(\Yii::$app->basePath . '/runtime/logs/memoryLogs.txt', $error, FILE_APPEND);

            $containers = Container::findAll([
                'status' => Container::STATUS_STARTED,
                'target' => Constant::TARGET_PHOENIX,
            ]);

            foreach ($containers as $container) {
                \Yii::$app->rpcClient->stopRpcContainer($container->name, Constant::TARGET_PHOENIX);
            }

            sleep(60);

            Container::deleteAll();
            Client::deleteAll();

            return true;
        }

        return true;
    }

    protected function logError(array $rpcCallInfo, array $incomingData)
    {
        $params = [
            'rpcCallInfo'  => $rpcCallInfo,
            'incomingData' => $incomingData
        ];

        \Yii::$app->rpcClient->storeRpcLog([
            'clientName' => $this->getClientName($rpcCallInfo),
            'direction'  => RpcLog::INCOMING,
            'method'     => RpcLog::METHOD_ERROR,
            'params'     => json_encode($params),
        ]);
    }

    /**
     * @param $incomingData
     * @return bool
     */
    private function isProxyError(array $incomingData)
    {
        return (is_string($incomingData['error']) && strpos($incomingData['error'], 'Proxy Error') !== false)
        || (!empty($incomingData['error']['code']) && $incomingData['error']['code'] == 'ECONNRESET');
    }

    private function processSendMessageError(array $rpcCallInfo, array $incomingData)
    {
        if ($incomingData['error'] == 'not started') {
            $this->updateCommunication($rpcCallInfo['additional']['messageId']);
            \Yii::$app->rpcClient->stopRpcContainer($rpcCallInfo['additional']['clientName'], Constant::TARGET_PHOENIX);
        }
    }

    private function updateCommunication(int $messageId)
    {
        $communication                = CommunicationSchedule::findOne($messageId);
        $communication->timeToSend    = date('Y-m-d H:i:s', strtotime($communication->timeToSend) + 45);
        $communication->sendStartedAt = '0000-00-00 00:00:00';
        $communication->save();
    }
}
