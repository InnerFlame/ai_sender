<?php

namespace app\components\phoenix;

use app\config\Constant;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use yii\base\Component;

class ContainerClientRpcComponent extends Component
{
    const QUEUE = 'rpcClientResponse';

    public function processContainerClient()
    {
        \Yii::$app->internalAmqp->consumeEvents(self::QUEUE, [$this, 'containerClientHandler']);
    }

    public function containerClientHandler(array $message)
    {
        switch ($message['rpcCallInfo']['method']) {
            case 'refreshToken':
                $this->processRefreshToken($message['rpcCallInfo'], $message['incomingData']);
                break;
            case 'start':
                $this->processStart($message['rpcCallInfo']);
                break;
            case 'login':
                $this->processSenderLogin($message['rpcCallInfo'], $message['incomingData']);
                break;
            case 'close':
                \Yii::$app->rpcClient->setRpcClientStopped(
                    $message['incomingData']['id'],
                    $message['incomingData']['type']
                );
                break;
            case 'startMethod':
                $this->processStartMethod($message['incomingData']);
                break;
            case 'fetch':
                \Yii::$container->get('fetchRpc')->processRpc($message);
                break;
            case 'killContainer':
                \Yii::$app->rpcClient->deleteContainerClient($message['rpcCallInfo']['additional']['clientName']);
                break;
        }

        return true;
    }

    private function processSenderLogin(array $rpcCallInfo, array $incomingData)
    {
        //TODO: add correct processing
        if (empty($incomingData) || empty($incomingData['result']) || empty($incomingData['result']['data'])) {
            return;
        }

        if ($this->isLoginError($incomingData) && $this->isInternalServerError($incomingData)) {
            \Yii::$app->rpcClient->stopRpcContainer(
                $rpcCallInfo['additional']['clientName'],
                $rpcCallInfo['additional']['target']
            );
            return;
        }

        $senderProfile = SenderProfile::findOne(['id' => $rpcCallInfo['additional']['profileId']]);
        $senderProfile->accessToken     = $incomingData['result']['data']['access_token'];
        $senderProfile->refreshToken    = $incomingData['result']['data']['refresh_token'];
        $senderProfile->save();

        \Yii::$app->rpcClient->setRpcClientLogin(
            $rpcCallInfo['additional']['clientName'],
            $rpcCallInfo['additional']['target']
        );
        \Yii::$app->rpcClient->updateClientRefreshingTime(
            $rpcCallInfo['additional']['clientName'],
            $rpcCallInfo['additional']['target']
        );

        $loginCount = \Yii::$app->redis->get('clientLoginCount.' . $rpcCallInfo['additional']['profileId']);
        if (empty($loginCount)) {
            \Yii::$app->redis->setex('clientLoginCount.' . $rpcCallInfo['additional']['profileId'], 24 * 3600, 1);
        } else {
            \Yii::$app->redis->incr('clientLoginCount.' . $rpcCallInfo['additional']['profileId']);
        }

        switch ($rpcCallInfo['additional']['clientType']) {
            case Client::TYPE_OBSERVE:
            case Client::TYPE_COMMUNICATION:
                \Yii::$app->rpcClient->rpcCall(
                    'client.' . $rpcCallInfo['additional']['clientName'],
                    'start',
                    [],
                    $rpcCallInfo['additional']
                );
                \Yii::$app->rpcMethods->fetchData($senderProfile, $rpcCallInfo['additional']['target']);
                break;
            case Client::TYPE_SEARCH:
                if ($rpcCallInfo['additional']['target'] == Constant::TARGET_PHOENIX) {
                    \Yii::$app->apiSearch->processSearch($rpcCallInfo['additional']['profileId']);
                } else {
                    \Yii::$app->searcher->processSearch($rpcCallInfo['additional']['profileId']);
                }
                break;
            case Client::TYPE_FETCH:
                \Yii::$app->rpcClient->rpcCall(
                    'client.' . $rpcCallInfo['additional']['clientName'],
                    'start',
                    [],
                    $rpcCallInfo['additional']
                );
                break;
        }
    }

    private function processRefreshToken(array $rpcCallInfo, array $incomingData)
    {
        $senderProfile = SenderProfile::findOne(['id' => $rpcCallInfo['additional']['profileId']]);
        //TODO: add correct processing
        if (empty($incomingData) || empty($incomingData['result']) || empty($incomingData['result']['data'])) {
            return;
        }
        $senderProfile->accessToken = $incomingData['result']['data']['access_token'];
        $senderProfile->save();
        \Yii::$app->rpcMethods->fetchData($senderProfile, $rpcCallInfo['additional']['target']);
    }

    private function processStart(array $rpcCallInfo)
    {
        \Yii::$app->rpcClient->setRpcClientStarted(
            $rpcCallInfo['additional']['clientName'],
            $rpcCallInfo['additional']['target']
        );
        SenderProfile::setStatusOnline($rpcCallInfo['additional']['profileId']);

        switch ($rpcCallInfo['additional']['clientType']) {
            case Client::TYPE_FETCH:
                \Yii::$container->get('fetchProcessor')->fetch($rpcCallInfo['additional']);
                break;
        }
    }

    private function processStartMethod(array $params)
    {
        if (!empty($params['start']['details']['advice']) && $params['start']['details']['advice'] == 'reconnect') {
            $this->reconnectClient($params['id']);
            return true;
        }

        if (!empty($params['start']['stage']) && $params['start']['stage'] == 'socket on disconnect') {
            list($target, $id) = explode('.', $params['id']);
            \Yii::$app->rpcClient->stopRpcContainer($target . '.' . $id, $target);
            return true;
        }

        return true;
    }

    private function reconnectClient(string $clientName)
    {
        list($target, $id) = explode('.', $clientName);

        switch ($target) {
            case Constant::TARGET_PHOENIX:
                $sender = SenderProfile::findOne(['id' => $id]);

                if ($sender) {
                    SenderProfile::setStatusOffline($sender->id);

                    $additional = [
                        'target'     => $target,
                        'profileId'  => $sender->id,
                        'clientName' => $clientName,
                    ];

                    \Yii::$app->rpcClient->setRpcClientStopped($clientName, $target);
                    \Yii::$app->rpcClient->rpcCall('client.' . $clientName, 'start', [], $additional);
                }

                break;
        }
    }

    /**
     * @param array $incomingData
     * @return bool
     */
    private function isLoginError(array $incomingData)
    {
        return !empty($incomingData['result']['status']) && $incomingData['result']['status'] == 'error';
    }

    private function isInternalServerError(array $incomingData)
    {
        return !empty($incomingData['result']['meta']['code']) && $incomingData['result']['meta']['code'] == 481;
    }
}
