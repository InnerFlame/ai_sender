<?php

namespace app\components\phoenix\reUpload;

use app\config\Constant;
use app\models\phoenix\ReUploadPhotoScheduler;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use app\models\rpc\Container;
use sender\registration\components\PhotoUpload;
use yii\base\Component;
use yii\db\Expression;
use yii\db\Query;

class PhotoReUploadProcessorComponent extends Component
{
    public function createRpcContainers()
    {
        $senders = $this->getReadySenders();

        if (empty($senders)) {
            return false;
        }

        foreach ($senders as $sender) {
            if (\Yii::$container->get('interactionRouter')->isSeparatedInteraction($sender)) {
                \Yii::$app->rpcMethods->createRpcContainer(
                    $sender,
                    Container::TYPE_RE_UPLOAD_PHOTO,
                    Constant::TARGET_PHOENIX
                );
            }
        }

        return true;
    }

    public function loginClient(SenderProfile $profile)
    {
        \Yii::$app->rpcMethods->loginRpcClient($profile, Client::TYPE_RE_UPLOAD_PHOTO, Constant::TARGET_PHOENIX);
        return true;
    }

    public function processInteractionUploads()
    {
        $senders = $this->getReadySenders();

        if (empty($senders)) {
            return false;
        }

        foreach ($senders as $sender) {
            if (\Yii::$container->get('interactionRouter')->isSeparatedInteraction($sender) == false) {
                /** @var PhotoUpload $photoUpload */
                $photoUpload = \Yii::$container->get('photoUpload');
                $photoUpload->uploadInteractionPhoto($sender);
            }
        }

        return true;
    }

    public function processReUpload()
    {
        $clients = $this->getReadyReUploadClients();

        if (empty($clients)) {
            return false;
        }

        foreach ($clients as $client) {
            $sender    = SenderProfile::findOne($client->profileId);
            $rpcClient = \Yii::$app->rpcClient->getRpcClient($sender, Constant::TARGET_PHOENIX);

            if (empty($rpcClient) || $rpcClient->status != Client::STATUS_LOGIN) {
                continue;
            }

            \Yii::$container->get('photoUpload')->uploadPhoto($sender);

            \Yii::$app->db->createCommand()->update(
                ReUploadPhotoScheduler::tableName(),
                ['startReUpload' => date('Y-m-d H:i:s')],
                [
                    'senderId' => $client->profileId,
                    'startReUpload' => '0000-00-00 00:00:00'
                ]
            )->execute();
        }

        return true;
    }

    protected function getReadySenders(int $limit = 10)
    {
        $communications = $this->getReadyReUploads($limit);
        $senders = [];
        foreach ($communications as $dataToSend) {
            if (empty($senders[$dataToSend['senderId']])) {
                $senders[$dataToSend['senderId']] = SenderProfile::findOne($dataToSend['senderId']);
            }
        }

        return $senders;
    }

    protected function getReadyReUploads(int $limit = 10)
    {
        return (new Query())->select('sch.*')
            ->from(ReUploadPhotoScheduler::tableName() . ' as sch')
            ->innerJoin(SenderProfile::tableName() . ' as s', 's.id=sch.senderId')
            ->where('timeToReUpload < :currentTime', ['currentTime' => date('Y-m-d H:i:s')])
            ->andWhere([
                'startReUpload' => '0000-00-00 00:00:00',
                'bannedAt'      => '0000-00-00 00:00:00',
                'removedAt'     => '0000-00-00 00:00:00',
            ])
            ->groupBy('sch.senderId')
            ->orderBy(new Expression('rand()'))
            ->limit($limit)
            ->all();
    }

    protected function getReadyReUploadClients()
    {
        $clients = Client::find()
            ->where([
                'status'    => Client::STATUS_LOGIN,
                'type'      => Client::TYPE_RE_UPLOAD_PHOTO,
            ])
            ->all();

        return $clients;
    }
}
