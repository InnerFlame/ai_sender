<?php

namespace app\components\phoenix\reUpload;

use app\models\phoenix\ReUploadPhotoScheduler;
use app\models\phoenix\SenderProfile;
use sender\registration\components\generate\PhotoInterface;
use yii\base\Component;
use yii\db\Query;

class PhotoReUploadScheduler extends Component
{
    /** @var int $attemptsToLimit */
    public $attemptsToLimit = 5;

    /** @var  PhotoInterface */
    private $photoGenerator;

    public function __construct(PhotoInterface $photoGenerator)
    {
        $this->photoGenerator = $photoGenerator;
    }

    public function addToSchedule(SenderProfile $sender, int $time = 86400)
    {
        if ($this->isNotProcessed($sender)) {
            if ($this->countReUpload($sender) < $this->attemptsToLimit) {
                if ($sender->photo = $this->photoGenerator->get($sender)) {
                    $sender->changePhotoStatus(SenderProfile::STATUS_TO_REUPLOAD);

                    $reUpload = new ReUploadPhotoScheduler();
                    $reUpload->senderId = $sender->id;
                    $reUpload->timeToReUpload = date('Y-m-d H:i:s', time() + $time);
                    $reUpload->save();
                } else {
                    $sender->changePhotoStatus(SenderProfile::STATUS_NO_PHOTO_FOR_REUPLOAD);
                }
            } else {
                $sender->changePhotoStatus(SenderProfile::STATUS_ATTEMPTS_TO_END_FOR_REUPLOAD);
            }
        }
    }

    private function isNotProcessed(SenderProfile $sender)
    {
        $query = new Query();

        return !$query->select('id')
            ->from(ReUploadPhotoScheduler::tableName())
            ->where(['senderId' => $sender->id])
            ->andWhere(['startReUpload' => '0000-00-00 00:00:00'])
            ->one();
    }

    private function countReUpload(SenderProfile $sender)
    {
        $query = new Query();

        return $query->select('count(id)')
            ->from(ReUploadPhotoScheduler::tableName())
            ->where(['senderId' => $sender->id])
            ->groupBy('senderId')
            ->scalar();
    }
}
