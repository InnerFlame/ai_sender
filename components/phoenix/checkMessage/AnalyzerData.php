<?php

namespace app\components\phoenix\checkMessage;

class AnalyzerData
{
    const SCAM_WORDS_FILE_DATA_CACHE_LIFETIME = 3600;
    const SCAM_WORDS_FILE_DATA_CACHE_KEY      = 'scamWordsCachedData';

    const SCAM_WORDS_FILE_DOES_NOT_EXISTS_CACHE_KEY = 'scamWordsFileDoesNotExists';

    /**
     * @var string[]
     */
    private $punctuationChars;

    /**
     * @var string[]
     */
    private $specialChars;

    /**
     * @var string[]
     */
    private $otherChars;

    /**
     * @var string[]
     */
    private $stringDigits;

    /**
     * @var string[]
     */
    private $emailPatterns;

    /**
     * @var string[]
     */
    private $messengersPatterns;

    /**
     * @var string[]
     */
    private $phoneWordsPatterns;

    /**
     * @var array
     */
    private $forbiddenLettersList;

    /**
     * @var string[]
     */
    private $scamWords;

    /**
     * @var string[]
     */
    private $unicodeEmojis;

    /**
     * AnalyzerData constructor.
     *
     * @param string[] $punctuationChars
     * @param string[] $specialChars
     * @param string[] $otherChars
     * @param string[] $stringDigits
     * @param string[] $emailPatterns
     * @param string[] $messengersPatterns
     * @param string[] $phoneWordsPatterns
     * @param array    $forbiddenLettersList
     * @param string[] $scamWords
     * @param string[] $unicodeEmojis
     */
    public function __construct(
        array $punctuationChars,
        array $specialChars,
        array $otherChars,
        array $stringDigits,
        array $emailPatterns,
        array $messengersPatterns,
        array $phoneWordsPatterns,
        array $forbiddenLettersList,
        array $scamWords,
        array $unicodeEmojis
    ) {
        $this->punctuationChars     = $punctuationChars;
        $this->specialChars         = $specialChars;
        $this->otherChars           = $otherChars;
        $this->stringDigits         = $stringDigits;
        $this->emailPatterns        = $emailPatterns;
        $this->messengersPatterns   = $messengersPatterns;
        $this->phoneWordsPatterns   = $phoneWordsPatterns;
        $this->forbiddenLettersList = $forbiddenLettersList;
        $this->scamWords            = $scamWords;
        $this->unicodeEmojis        = $unicodeEmojis;
    }

    /**
     * @return string[]
     */
    public function getPunctuationChars(): array
    {
        return $this->punctuationChars;
    }

    /**
     * @return string[]
     */
    public function getSpecialChars(): array
    {
        return $this->specialChars;
    }

    /**
     * @return string[]
     */
    public function getOtherChars(): array
    {
        return $this->otherChars;
    }

    /**
     * @return string[]
     */
    public function getStringDigits(): array
    {
        return $this->stringDigits;
    }

    /**
     * @return string[]
     */
    public function getEmailPatterns(): array
    {
        return $this->emailPatterns;
    }

    /**
     * @return string[]
     */
    public function getMessengersPatterns(): array
    {
        return $this->messengersPatterns;
    }

    /**
     * @return string[]
     */
    public function getPhoneWordsPatterns(): array
    {
        return $this->phoneWordsPatterns;
    }

    /**
     * @return array
     */
    public function getForbiddenLettersList(): array
    {
        return $this->forbiddenLettersList;
    }

    /**
     * @return string[]
     */
    public function getScamWords()
    {
        return $this->scamWords;
    }

    /**
     * @return string[]
     */
    public function getUnicodeEmojis(): array
    {
        return $this->unicodeEmojis;
    }
}
