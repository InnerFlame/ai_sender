<?php

namespace app\components\phoenix\checkMessage;

use app\config\Constant;
use app\models\phoenix\ProfileMessage;
use yii\base\Component;
use yii\db\Query;

class CheckMessageComponent extends Component
{
    private $dataToSave = [];

    public function checkMessageForTool($text, array $excludeAnalyzers = [])
    {
        /** @var AnalysisResult[]  $result */
        $result      = $this->checkMessage($text, $excludeAnalyzers);
        $highlighter = new Highlighter('<span style="color:red;">', '</span>');
        $data        = [];

        foreach ($result as $key => $item) {
            if ($item->isSuspicious()) {
                if ($key == 'DifferentEncoding') {
                    $data[] = [
                        'scamWord' => '',
                        'text'     => 'Text contains different encoding',
                        'analyzer' => $key,
                    ];
                } else {
                    foreach ($item->getSuspiciousWords() as $word) {
                        $data[] = [
                            'scamWord' => $word,
                            'text'     => $highlighter->highlight($item->getText(), [$word]),
                            'analyzer' => $key,
                        ];
                    }
                }
            }
        }

        return $data;
    }

    public function checkInitialMessages()
    {
        $highlighter = new Highlighter('<scam>', '</scam>');
        $pathToFile  = \Yii::$app->basePath . '/web/files/blackInitialWords.csv';
        $messages    = (new Query())
            ->select('id, body,language')
            ->from(ProfileMessage::tableName())
            ->all();

        foreach ($messages as $message) {
            $result = $this->checkMessage($message['body'], [], $message['language']);
            $this->makeDataForSave($message['id'], $message['body'], $highlighter, $result);
        }

        $this->saveToCsv($pathToFile);
    }

    public function checkPhrases()
    {
        if (!is_file(\Yii::$app->basePath . '/web/files/phrases.csv')) {
            return false;
        }

        $phrases    = array_map('str_getcsv', file(\Yii::$app->basePath . '/web/files/phrases.csv'));
        $pathToFile = \Yii::$app->basePath . '/web/files/blackPhraseWords.csv';

        $this->checkEvaMessages($phrases, $pathToFile);

        return true;
    }

    public function checkTemplates()
    {
        if (!is_file(\Yii::$app->basePath . '/web/files/templates.csv')) {
            return false;
        }

        $templates  = array_map('str_getcsv', file(\Yii::$app->basePath . '/web/files/templates.csv'));
        $pathToFile = \Yii::$app->basePath . '/web/files/blackTemplateWords.csv';

        $this->checkEvaMessages($templates, $pathToFile);

        return true;
    }

    /**
     * check message before send
     * checks for availability black words after transformation message
     * @param string $message
     * @param null|string $language
     * @return bool
     */
    public function isWhiteMessageForSend(string $message, $language = null)
    {
        $result = $this->checkMessage($message, [], $language);

        foreach ($result as $key => $item) {
            if ($item->isSuspicious() && $key != 'DifferentEncoding') {
                return false;
            }
        }

        return true;
    }

    private function checkEvaMessages($messages, $pathToFile)
    {
        $highlighter = new Highlighter('<scam>', '</scam>');
        foreach ($messages as $key => $message) {
            $result = $this->checkMessage($message[3]);
            $this->makeDataForSave($message[2], $message[3], $highlighter, $result);
        }

        $this->saveToCsv($pathToFile);

        return true;
    }

    private function checkMessage($text, array $excludeAnalyzers = [], $language = null)
    {
        $text            = new Text($text, $language);
        $suspiciousWords = \Yii::$container->get('suspiciousWords');

        return $suspiciousWords->analyze($text, [], $excludeAnalyzers);
    }

    /**
     * @param $id
     * @param $text
     * @param Highlighter $highlighter
     * @param AnalysisResult[] $analysisResult
     */
    private function makeDataForSave($id, $text, Highlighter $highlighter, $analysisResult)
    {
        foreach ($analysisResult as $key => $item) {
            if ($item->isSuspicious()) {
                if ($item->isSuspicious()) {
                    if ($key == 'DifferentEncoding') {
                        $this->dataToSave[] = [
                            $id,
                            $text,
                            '',
                            'Text contains different encoding',
                        ];
                    } else {
                        foreach ($item->getSuspiciousWords() as $word) {
                            $this->dataToSave[] = [
                                $id,
                                $text,
                                $word,
                                $highlighter->highlight($item->getText(), [$word])
                            ];
                        }
                    }
                }
            }
        }
    }

    private function saveToCsv($pathToFile)
    {
        if (empty($this->dataToSave)) {
            return false;
        }

        $file = fopen($pathToFile, 'w');

        foreach($this->dataToSave as $item)
            {
                fputcsv($file, $item);
            }

        fclose($file);

        return true;
    }

    public function getWrongInitialsFromDB(): array
    {
        $messages = (new Query())
            ->select('body as data')
            ->addSelect('language as lang')
            ->from(ProfileMessage::tableName())
            ->all();
        return $this->findWrongMessages($messages);
    }

    public function getWrongPhrasesFromContentTool(): array
    {
        $placeholders = array_column($this->loadDataFromContentTool('placeholders', ['data' => 'name']), 'data');
        $phrases = $this->loadDataFromContentTool('phrases', ['data' => 'phrase_text', 'lang' => 'lang', 'tag' => 'phrase_tag']);
        $phrases = array_filter($phrases, function (array $item) {
            if ($tags = json_decode(@$item['tag'], true)) {
                return in_array('cam models', $tags);
            }
            return true;
        });
        return $this->findWrongMessages(
            $phrases,
            $placeholders
        );
    }

    public function getWrongTemplatesFromContentTool(): array
    {
        return $this->findWrongMessages($this->loadDataFromContentTool('templates', ['data' => 'template_text', 'lang' => 'lang']));
    }

    private function loadDataFromContentTool(string $type, array $columnMap): array
    {
        \Yii::$app->curl->setOption(CURLOPT_SSL_VERIFYPEER, false);
        \Yii::$app->curl->setOption(CURLOPT_SSL_VERIFYHOST, false);
        \Yii::$app->curl->setOption(CURLOPT_RETURNTRANSFER, true);

        $requestData = [
            'type'  => $type,
            'key'   => Constant::CONTENT_API_KEY,
        ];

        $response = \Yii::$app->curl->get(Constant::CONTENT_API_URL . '?' . http_build_query($requestData));
        $data = json_decode(
            $response,
            true
        );
        $result = [];
        foreach ($data as $row) {
            $result[] = $this->rowMap($row, $columnMap);
        }

        return $result;
    }

    private function rowMap(array $row, array $columnMap): array
    {
        $result = [];
        foreach ($columnMap as $dstColumnName => $srcColumnName) {
            $result[$dstColumnName] = @$row[$srcColumnName];
        }
        return $result;
    }

    private function findWrongMessages(array $data, array $placeholders = []): array
    {
        $result = [];
        foreach ($data as $row) {
            /** @var \app\components\phoenix\checkMessage\AnalysisResult[] $checkResults */
            $analyzersToExclude = (!empty($row['lang']) && $row['lang'] != 'en') ? ['DifferentEncoding'] : [];
            $language           = $row['lang'] ?? null;

            $checkResults = $this->checkMessage(
                $this->removePlaceholders($row['data'], $placeholders),
                $analyzersToExclude,
                $language
            );
            foreach ($checkResults as $check => $checkResult) {
                if ($checkResult->isSuspicious()) {
                    $result[] = [
                        'text' => $row['data'],
                        'lang' => $row['lang'],
                    ];
                    break;
                }
            }
        }
        return $result;
    }

    private function removePlaceholders(string $message, array $placeholders): string
    {
        foreach ($placeholders as $placeholder) {
            $message = str_replace($placeholder, ' ', $message);
        }
        return $message;
    }
}
