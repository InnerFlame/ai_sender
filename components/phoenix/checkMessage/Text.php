<?php

namespace app\components\phoenix\checkMessage;

/**
 * Class Text
 * @package  Phoenix\AntiScamm\Components\SuspiciousWords
 */
class Text
{
    /**
     * @var string
     */
    private $originalText;

    /**
     * @var string[]
     */
    private $wordsList;

    /**
     * @var string[]
     */
    private $cleanedWordsList;

    /**
     * @var string
     */
    private $textForDb;

    /**
     * Text constructor.
     *
     * @param string $text
     */
    public function __construct(string $text)
    {
        $this->originalText = trim($text);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getOriginalText();
    }

    /**
     * @return string
     */
    public function getOriginalText(): string
    {
        return $this->originalText;
    }

    /**
     * @return string
     */
    public function getTextForDb(): string
    {
        if ($this->textForDb === null) {
            $this->textForDb = trim(
                json_encode(
                    htmlspecialchars($this->originalText),
                    JSON_UNESCAPED_SLASHES
                ),
                '"'
            );
        }

        return $this->textForDb;
    }

    /**
     * @return string[]
     */
    public function getWordsList(): array
    {
        if ($this->wordsList === null) {
            $this->wordsList = $this->buildWordsList($this->originalText);
        }
        return $this->wordsList;
    }

    /**
     * @return array
     */
    public function getCleanedWordsList(): array
    {
        if ($this->cleanedWordsList === null) {
            $this->cleanedWordsList = $this->buildWordsList($this->originalText, true);
        }

        return $this->cleanedWordsList;
    }

    /**
     * @param bool $stripPunctuation
     *
     * @return int
     */
    public function getWordsCount(bool $stripPunctuation = false): int
    {
        return $stripPunctuation
            ? count($this->getCleanedWordsList())
            : count($this->getWordsList());
    }

    /**
     * @return int
     */
    public function getTextLength(): int
    {
        return mb_strlen($this->getOriginalText());
    }

    /**
     * @param  string $text
     * @param  bool   $stripPunctuation
     * @return array
     */
    private function buildWordsList(string $text, bool $stripPunctuation = false): array
    {
        $text = mb_strtolower($text);
        $text = preg_quote($text, '/');

        if ($stripPunctuation) {
            $text = preg_replace('/[[:punct:]]/', ' ', $text);
        }

        $wordsList = preg_split('/[\s]+/u', $text);
        $wordsList = array_map('trim', $wordsList);

        return array_diff($wordsList, ['', ' ', null, false]);
    }
}
