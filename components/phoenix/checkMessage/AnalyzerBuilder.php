<?php

namespace app\components\phoenix\checkMessage;

use Phoenix\AntiScamm\Components\SuspiciousWords\Analyzers\RealScamKeywordsAnalyzer;

class AnalyzerBuilder
{
    /**
     * @var AnalyzerData
     */
    private $analyzerData;

    /**
     * AnalyzerBuilder constructor.
     *
     * @param AnalyzerData $analyzerData
     */
    public function __construct(AnalyzerData $analyzerData)
    {
        $this->analyzerData = $analyzerData;
    }

    /**
     * @param string $analyzerName
     * @return Analyzer
     */
    public function buildAnalyzer(string $analyzerName): Analyzer
    {
        $builderMethod = $this->createBuilderName($analyzerName);
        if (method_exists($this, $builderMethod)) {
            return $this->{$builderMethod}();
        } else {
            $analyzer = $this->buildAnalyzerPath($analyzerName);
            return new $analyzer($this->analyzerData);
        }
    }

    /**
     * @param  string $analyzerName
     * @return string
     */
    private function createBuilderName(string $analyzerName): string
    {
        return 'build' . ucfirst($analyzerName) . 'Analyzer';
    }

    /**
     * @param  string $analyzerName
     * @return string
     */
    private function buildAnalyzerPath(string $analyzerName): string
    {
        return __NAMESPACE__ . '\\analyzers\\' . ucfirst($analyzerName) . 'Analyzer';
    }
}
