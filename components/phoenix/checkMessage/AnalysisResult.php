<?php

namespace app\components\phoenix\checkMessage;

/**
 * Class AnalysisResult
 * @package  Phoenix\AntiScamm\Components\SuspiciousWords
 */
class AnalysisResult
{
    /**
     * @var Text
     */
    private $text;

    /**
     * @var bool
     */
    private $isSuspicious;

    /**
     * @var array
     */
    private $suspiciousWords;

    /**
     * @var string
     */
    private $pattern;

    /**
     * AnalysisResult constructor.
     *
     * @param Text   $text
     * @param bool   $isSuspicious
     * @param array  $suspiciousWords
     * @param string $pattern
     */
    public function __construct(Text $text, bool $isSuspicious, array $suspiciousWords = [], string $pattern = '')
    {
        $this->text            = $text;
        $this->isSuspicious    = $isSuspicious;
        $this->suspiciousWords = $suspiciousWords;
        $this->pattern         = $pattern;
    }

    /**
     * @return Text
     */
    public function getText(): Text
    {
        return $this->text;
    }

    /**
     * @return boolean
     */
    public function isSuspicious(): bool
    {
        return $this->isSuspicious;
    }

    /**
     * @return array
     */
    public function getSuspiciousWords(): array
    {
        return $this->suspiciousWords;
    }

    /**
     * @return string
     */
    public function getPattern(): string
    {
        return $this->pattern;
    }
}
