<?php

namespace app\components\phoenix\checkMessage;

class AnalyzerFactory
{
    /**
     * @var AnalyzerData
     */
    private $analyzerData;

    /**
     * AnalyzerFactory constructor.
     *
     * @param AnalyzerData $analyzerData
     */
    public function __construct(AnalyzerData $analyzerData)
    {
        $this->analyzerData = $analyzerData;
    }

    /**
     * @param  string[]   $analyzersList
     * @return Analyzer[]
     */
    public function getAnalyzersList(array $analyzersList = []): array
    {
        $builder   = new AnalyzerBuilder($this->analyzerData);

        foreach ($analyzersList as $analyzerName) {
            $analyzers[$analyzerName] = $builder->buildAnalyzer($analyzerName);
        }

        return $analyzers;
    }
}
