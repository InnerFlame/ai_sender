<?php

namespace app\components\phoenix\checkMessage\analyzers;

use app\components\phoenix\checkMessage\AnalysisResult;
use app\components\phoenix\checkMessage\Analyzer;
use app\components\phoenix\checkMessage\AnalyzerData;
use app\components\phoenix\checkMessage\Text;

/**
 * Class ForbiddenLettersAnalyzer
 * @package  Phoenix\AntiScamm\Components\SuspiciousWords\Analyzers
 */
class ForbiddenLettersAnalyzer implements Analyzer
{
    /**
     * @var AnalyzerData
     */
    private $analyzerData;

    /**
     * PunctuationSymbols constructor.
     *
     * @param AnalyzerData $analyzerData
     */
    public function __construct(AnalyzerData $analyzerData)
    {
        $this->analyzerData = $analyzerData;
    }

    /**
     * Analyze text
     *
     * @param Text $text
     *
     * @return AnalysisResult
     */
    public function analyze(Text $text): AnalysisResult
    {
        $isSuspicious = false;
        $suspiciousWords = [];

        foreach ($this->analyzerData->getForbiddenLettersList() as $language => $charsList) {
            foreach ($charsList as $char) {
                if (stripos($text->getOriginalText(), $char) !== false) {
                    $isSuspicious = true;
                    $suspiciousWords[] = $char;
                }
            }
        }

        return new AnalysisResult($text, $isSuspicious, $suspiciousWords);
    }
}
