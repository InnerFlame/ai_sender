<?php

namespace app\components\phoenix\checkMessage\analyzers;

use app\components\phoenix\checkMessage\AnalysisResult;
use app\components\phoenix\checkMessage\Analyzer;
use app\components\phoenix\checkMessage\AnalyzerData;
use app\components\phoenix\checkMessage\Text;


/**
 * Class BlackMessagesAnalyzer
 * @package  Phoenix\AntiScamm\Components\SuspiciousWords\Analyzers
 */
class BlackMessagesAnalyzer implements Analyzer
{
    /**
     * Analyze text
     *
     * @param Text $text
     *
     * @return AnalysisResult
     */
    public function analyze(Text $text): AnalysisResult
    {
        $matches = [];
        $expression = $this->buildExpression();
        $isSuspicious = (bool) preg_match($expression, $text->getOriginalText(), $matches);
        $matches = empty($matches) ? [] : [reset($matches)];

        return new AnalysisResult($text, $isSuspicious, $matches, $expression);
    }

    /**
     * Analyze text
     *
     * @param Text $text
     *
     * @return array
     */
    public function analyzeForEach(Text $text): array
    {
        $results = [];
        $patterns = $this->getPatterns();
        $message = $text->getOriginalText();
        foreach ($patterns as $expression => $properties) {
            $isSuspicious = preg_match(sprintf('/%s/iu', $expression), $message, $matches);

            if ($isSuspicious) {
                $results[$expression] = $matches[0];
            }
        }

        return $results;
    }

    /**
     * Check rules
     *
     * @return array
     */
    public function checkForEach(): array
    {
        $results = [];
        $patterns = $this->getPatterns();
        foreach ($patterns as $expression => $properties) {
            $results[$expression] = [
                'title' => (!empty($properties['title'])) ? $properties['title'] : '',
                'description' => (!empty($properties['description'])) ? $properties['description'] : '',
                'exceptions' => (!empty($properties['exceptions'])) ? $properties['exceptions'] : [],
            ];
            if (!empty($properties['true positive'])) {
                foreach ($properties['true positive'] as $message) {
                    $isSuspicious = preg_match(sprintf('/%s/iu', $expression), $message, $matches);
                    $results[$expression]['true positive'][] = [
                        'message' => $message,
                        'isSuspicious' => $isSuspicious,
                        'matches' => ($isSuspicious) ? $matches[0] : '',
                    ];
                }
            }
            if (!empty($properties['true negative'])) {
                foreach ($properties['true negative'] as $message) {
                    $isSuspicious = preg_match(sprintf('/%s/iu', $expression), $message, $matches);
                    $results[$expression]['true negative'][] = [
                        'message' => $message,
                        'isSuspicious' => $isSuspicious,
                        'matches' => ($isSuspicious) ? $matches[0] : '',
                    ];
                }
            }
        }

        return $results;
    }

    /**
     * Most regexp below you can find here
     * https://jira.togethernetworks.com/confluence/pages/viewpage.action?spaceKey=ANS&title=Regexps+detect+logic
     *
     * @return array
     */
    private function getPatterns(): array
    {
        $patterns = [
            /**===================================================================================
             * CAN I..., DROP YOUR..., LETS START TALK...
             **===================================================================================*/

            /**
             * can i have ur
             * %c+a+n+\si+\s?h+a*v+e+\sy*o*u+r+\s(e|f|g|n|s|c)%ui
             * %c+a+n+\si+\s?h+a*v+e+\sy*o*u+r+\s(?!name)(e|f|g|n|s|c)%ui
             */
            '(c+a+n+\si+\s?h+a*v+e+\sy*o*u+r+\s(?!name)(e|f|g|n|s|c))' => [
                'title' => 'can i have your',
                'description' => 'Detect "can i have your..." + (e|f|g|n|s|c)',
                'exceptions' => [
                    'do not detect "your name"',
                ],
                'true positive' => [
                    'can i have your number',
                    'can i have your email',
                ],
                'true negative' => [
                    'can i have your name',
                ],
            ],

            /**
             * Where can i send
             * /w+h+e+r+e+\sc+a+n+\si+\s((s|x)+e+n+d+|t+e+|s+h+o+w+)/ui
             */
            '(w+h+e+r+e+\sc+a+n+\si+\s((s|x)+e+n+d+|t+e+|s+h+o+w+))' => [
                'title' => 'where can i send',
                'description' => 'Detect "where can i send" in text',
            ],

            /**
             * drop ur
             * %d+r+o+p+\sy*o*u*r+\s(e|n|f|g|y)%ui
             */
            '(d+r+o+p+\sy*o*u*r+\s(e|n|f|g|y))' => [
                'title' => 'drop your',
                'description' => 'Detect "drop your..." + (e|n|f|g|y)',
                'true positive' => [
                    'drop your number',
                ],
            ],

            /**
             * check your
             * %check\sy*o*ua*r\s(e|s|g)%ui
             */
            '(check\sy*o*ua*r\s(e|s|g))' => [
                'title' => 'check your',
                'description' => 'Detect "check your..." + (e|s|g)',
                'true positive' => [
                    'check your email',
                ],
            ],

            /**
             * lets start talk at
             * %l+e+t+s*\s(s+t+a+r+t+\s)?t+a+l+k+\s(a+t+|o+n+)\s(e|f|g|p|s)%ui
             */
            '(l+e+t+s*\s(s+t+a+r+t+\s)?t+a+l+k+\s(a+t+|o+n+)\s(e|f|g|p|s))' => [
                'title' => 'lets start talk at',
                'description' => 'Detect "lets start talk at..." + (e|f|g|p|s)',
                'true positive' => [
                    'lets start talk at phone',
                ],
            ],

            /**
             * my link
             * %my.{0,10}link%i
             */
            '(my.{0,10}link)' => [
                'title' => 'my link',
                'description' => 'Detect "my" + 10 characters + "link"',
            ],

            /**
             * wotz ur (removed)
             * %w\s?(o|a|u|h(a|u|ya)*)+t+(z|\ss|s)*\s(is\s)?\w?(y*u+o*r+|y+o+r*|your?)e?\s(m|e|n|gm|x|po|ph|dig|tel|fa|cell)(?!ame|ean)%ui
             */

            /**
             * what is your mobile/cell/phone
             * %w\s?(o|a|u|h(a|u|ya)*)+t+(z|\ss|s)*\s(is\s)?\w?(y*u+o*r+|y+o+r*|your?)e?\s(m|e|n|gm|x|po|ph|dig|tel|fa|cell)(?!ame|ean)%ui
             */
            '((w[aou]ts?|what|wahts|wh[auo]?tz|wahtz|wht\w[zs])\s?(u|ur|yo|yo?ur?|y[ou]+r)\s?(ce|c\wel|no|pho|mob))' => [
                'title' => 'what is your mobile',
                'description' => 'Detect "what is your mobile/cell/phone" phrases',
                'true positive' => [
                    'what your mobile',
                    'what u cell',
                    'what ur no',
                    'wats yo phone',
                ],
            ],

            /**===================================================================================
             * PHONE, MOBILE, CELL
             **===================================================================================*/

            /**
             * phone
             * %([0-9][\s\-\.\,\(\)\[\]/\*_]{0,}){10,}%ui
             * true positive:
             *   _708_310_6636
             */

            /**
             * phone phrase
             * %(your|ur|phone|cell) number%i
             */
            '((your|ur|phone|cell) number)' => [
                'title' => 'Phone related: number',
                'description' => 'Detect the word "number" related to phone number',
                'true positive' => [
                    'my phone number is',
                ],
            ],

            /**
             * mobail word
             * %m+o+b+a+.?[ijl1¦:\!\|]+(l+)?%
             */
            '(m+o+b+a+.?[ijl1¦:\!\|]+[ijl1¦:\!\|]*)' => [
                'title' => 'Phone related: mobile word',
                'description' => 'Detect the phrase "mobail" with different spellings',
                'true positive' => [
                    'mobail',
                    'mobali',
                ],
            ],

            /**
             * mobile word
             * %m+o+(\w{1,2})?b+[ia]+[ilj]+e+%
             */
            '(m+o+(\w{1,2})?b+[ia]+[ilj]+e+)' => [
                'title' => 'Phone related: mobile word',
                'description' => 'Detect the word "mobile" with different spellings',
            ],

            /**
             * Mobile
             * %m+o+\wb\w{1,3}l+%
             */
            '(m+o+\wb\w{1,3}l+)' => [
                'title' => 'Phone related: mobile word',
                'description' => 'Detect the word "mobile" with different spellings',
            ],

            /**
             * Mobile
             * %[lmi\|j]obile%
             */
            '([lmi\|j]obile)' => [
                'title' => 'Phone related: mobile word',
                'description' => 'Detect the word "mobile" with different spellings',
            ],

            /**
             * Mobile no
             * /mobile\sn(o|a|0)/ui
             * %mobile\sn[ao0u]%
             */
            '(mobile\sn[ao0u])' => [
                'title' => 'Phone related: mobile word',
                'description' => 'Detect the phrase "mobile number" with different spellings',
                'true positive' => [
                    'my mobile number is',
                    'can i have your mobile no',
                ],
            ],

            /**
             * phone word
             * %p+[hf]+[o0.]+n+e+%
             */
            '(p+[hf]+[o0.]+n+e+)' => [
                'title' => 'Phone related: phone word',
                'description' => 'Detect the word "phone" with different spellings',
                'true positive' => [
                    'phone',
                    'pphhoonnee',
                ],
                'true negative' => $this->getNegative('phone'),
            ],

            /**
             * phone word
             * %ph?oh?ne\s?n%ui
             * %(?<!com)ph?oh?ne\s?n%
             */
            '((?<!com)ph?oh?ne\s?n)' => [
                'title' => 'Phone related: phone word',
                'description' => 'Detect the word "phone" with different spellings',
                'exceptions' => [
                    'do not detect "component"',
                ],
                'true positive' => [
                    'give me your ponen numbre',
                ],
                'true negative' => $this->getNegative('phone'),
            ],

            /**
             * cell ph
             * %c+\W*(e|i)*h?l+.?((p|f|n)(h|f|o|n)|u)%ui
             * %c+\W*(e|i)*h?l+.?(p|f|n)(h|f|o|n|u)%
             * %c+\W*[e|i]*h?l+\W*([p|f]+[h|f|o|.]|[n]+[o|u|r|.])%
             */
            '(c+\W*[e|i]*h?l+\W*([p|f]+[h|f|o|.]|[n]+[o|u|r|.]))' => [
                'title' => 'Phone related: cell word',
                'description' => 'Detect the word "cell" + 2 characters sequence',
                'true positive' => [
                    'cell fone',
                    'cell phone',
                    'cell number',
                    'cell n.mber',
                    'cellphone',
                    'c,ell..no',
                ],
                'true negative' => [
                    'Climbing a cliff without any gear',
                ],
            ],

            /**
             * cell no
             * %u+r+\sc+e+l+\sn+o+%ui
             */
            '(u+r+\sc+e+l+\sn+o+)' => [
                'title' => 'Phone related: cell word',
                'description' => 'Detect the phrase "ur cell no"',
            ],

            /**
             * cell numb
             * %c+e+l+\s?[hn]+u+[mb]+(\w{1,2})?r+%
             */
            '(c+e+l+\s?[hn]+u+[mb]+(\w{1,2})?r+)' => [
                'title' => 'Phone related: cell word',
                'description' => 'Detect the phrase "cell number"',
                'true positive' => [
                    'cell number',
                    'cell numeer',
                    'celnumr',
                ],
            ],

            /**
             * your cell
             * %u+r+\sc+e+l+\sn+o+%ui
             */
            '(u|ur|yo|yo?ur?)\s?(c+e+[ilj\|1])' => [
                'title' => 'Phone related: cell word',
                'description' => 'Detect the phrase "ur cell"',
            ],

            /**
             * contact no
             * %c+([uo.0]+)?n+t+(a+)?c+t+[^a-zA-Z0-9]*([pf]+[hfo.0]|[n]+[uro.0])%
             */
            '(c+([uo.0]+)?n+t+(a+)?c+t+[^a-zA-Z0-9]*([pf]+[hfo.0]|[n]+[uro.0]))' => [
                'title' => 'Phone related: contact word',
                'description' => 'Detect the phrase "contact no"',
                'true positive' => [
                    'contact fone',
                    'contact phone',
                    'contact number',
                    'contact n.mber',
                    'contactphone',
                    'cntct..no',
                ],
                'true negative' => [
                    'Contact me wheneva u have some free time',
                    'if u\'re outgoing	and up for a good laugh, pls contact me',
                ],
            ],

            /**===================================================================================
             * NUMBER, DIGITS
             **===================================================================================*/

            /**
             * Phone number in words
             * %((zero|one|two|three|four|five|six|seven|eight|nine)\s?){2,}%ui
             */

            /**
             * Num in text
             * %(^|\s)n+h*(u|a)+h*m+(?!e)...%ui
             */
            '((^|[\W_])n+h*(u|a)+h*m+(?!e|i|ba).{1,3})' => [
                'title' => 'Numbers in text',
                'description' => 'Detect numbers in text',
                'true negative' => $this->getNegative('number'),
            ],

            /**
             * number
             * %(^|\s)n+\w?\W*(u|a|v|o)*\s?[^a-z]*(m|h)+[^a-z]*((b|v)+[^a-z]*)+(a|e|r|i)+[^a-z]*(r|e|y)+%ui
             */
            '((^|[\W_])n+\w?\W*(u|a|v|o)*\s?[^a-z]*(m|h)+[^a-z]*((b|v)+[^a-z]*)+(a|e|r|i)+[^a-z]*(r|e|y)+)' => [
                'title' => 'Phone related: number word',
                'description' => 'Detect the word "number" with different spellings',
                'true positive' => [
                    'my nvmber is',
                    'my numberrr is',
                ],
                'true negative' => $this->getNegative('number'),
            ],

            /**
             * number word
             * %n[\S]*u[\S]*m[\S]*b[\S]*e[/S]*r[/S]*%ui
             */
            '(n[\S]*u[\S]*m[\S]*b[\S]*e[\S]*r[\S]*)' => [
                'title' => 'Phone related: number word',
                'description' => 'Detect "number" character sequence',
                'true negative' => $this->getNegative('number'),
            ],

            /**
             * number word
             * %n+[uao]+(\w)?m+b+(\w{1,3})?[ea]+r+%
             */
            '(n+[uao]+(\w)?m+b+(\w{1,3})?[ea]+r+)' => [
                'title' => 'Phone related: number word',
                'description' => 'Detect the word "number" with different spellings',
                'true negative' => $this->getNegative('number'),
            ],

            /**
             * number word
             * %n+[uoa]+m+b+.[uoa]+r+%
             */
            '(n+[uoa]+m+b+.[uoa]+r+)' => [
                'title' => 'Phone related: number word',
                'description' => 'Detect the word "number" with different spellings',
                'true negative' => $this->getNegative('number'),
            ],

            /**
             * number word
             * %n+(\w{1,2})?u+(\w{1,5})m+b+[iea]+(\w{1,3})?r+%
             */
            '(n+(\w{1,2})?u+(\w{1,5})m+b+[iea]+(\w{1,3})?r+)' => [
                'title' => 'Phone related: number word',
                'description' => 'Detect the word "number" with different spellings',
                'true negative' => $this->getNegative('number'),
            ],

            /**
             * number word
             * %n+(.{1,2})u+(.{1,2})m+(.{1,2})b+(.{1,2})e+(.{1,2})r+(.{1,2})%
             */
            '(n+(.{1,2})u+(.{1,2})m+(.{1,2})b+(.{1,2})e+(.{1,2})r+(.{1,2}))' => [
                'title' => 'Phone related: number word',
                'description' => 'Detect the word "number" with different spellings',
                'true negative' => $this->getNegative('number'),
            ],

            /**
             * number word
             * %n+([uh]+)?(.{1,2})?m+b+(e+)?r+%
             */
            '(n+([uh]+)?(.{1,2})?m+b+(e+)?r+)' => [
                'title' => 'Phone related: number word',
                'description' => 'Detect the word "number" with different spellings',
                'true negative' => $this->getNegative('number'),
            ],

            /**
             * number word
             * %n+[oau]+m+b+\w+r+%
             */
            '(n+[oau]+m+b+\w+r+)' => [
                'title' => 'Phone related: number word',
                'description' => 'Detect the word "number" with different spellings',
                'true negative' => $this->getNegative('number'),
            ],

            /**
             * number word
             * %[nm]+u+\wm+b[iaej1]+r+%
             */
            '([nm]+u+\wm+b[iaej1]+r+)' => [
                'title' => 'Phone related: number word',
                'description' => 'Detect the word "number" with different spellings',
                'true negative' => $this->getNegative('number'),
            ],

            /**
             * phone number count
             * %([0-9][\s\S*]{0,}){9,}%
            '(([0-9][\s\S*]{0,}){9,})' => [
                'title' => 'Phone related: number',
                'description' => 'Detect phone numbers',
            ],
             */

            /**
             * digits
             * %d+(.{1,2})?i+(.{1,2})?g+i+t+s+%
             */
            '(d+(.{1,2})?i+(.{1,2})?g+i+t+s+)' => [
                'title' => 'Phone related: digits',
                'description' => 'Detect "digits" with different spellings',
            ],

            /**
             * digits
             * %d+([lji\!1\|¦:]+)?g+([lji\!1\|¦:]+)?(.{1,2})?t+[sz]+%
             */
            '(d+([lji\!1\|¦:]+)?g+([lji\!1\|¦:]+)?(.{1,2})?t+[sz]+)' => [
                'title' => 'Phone related: digits',
                'description' => 'Detect "digits" with different spellings',
            ],

            /**
             * digits
             * %d+(.{1,2})?i+g+(.{1,3})?i+t+(.{1,3})?s+%
             */
            '(d+(.{1,2})?i+g+(.{1,3})?i+t+(.{1,3})?s+)' => [
                'title' => 'Phone related: digits',
                'description' => 'Detect "digits" with different spellings',
            ],

            /**===================================================================================
             * EMAIL
             **===================================================================================*/

            /**
             * gmail
             * %(g|j|jee)\s?(m|м)+(a|а|e)?(i|l|j|1)?(l|i|1|e)%ui
             * %(g|j|jee)\s?(m|м)+\s?(a|а|e)?\s?(i|l|j|1)\s?(l|i|1|e)%
             * %(g|j|jee)\s?(m|м)+\s?(a|а|e)?\s?(i|l|j|1)\s?(l|i|1|e)([^f,k,v]|$)%
             * %(g|j|jee)\s?(m|м)+\s?(a|а|e)?\s?(i|l|j|1)\s?(l|i|1|e)([^f,g,k,v]|$)%
             */
            '((g|j|jee)\s?(m|м)+\s?(a|а|e)?\s?(i|l|j|1)\s?(l|i|1|e)([^f,g,k,v]|$))' => [
                'title' => 'gmail',
                'description' => 'Detect the word "gmail" with different spellings',
                'exceptions' => [
                    'do not detect "f", "g", "v", "k" at the end of the phrase to skip words like "life", "light", "live", "like"',
                ],
                'true positive' => [
                    'gmail',
                    'gmail.com',
                    'gmaildotcom',
                    'gmail_dot_com',
                    'g m a i l',
                    'jee mail',
                    'gMa11',
                    'Re: canderson9289 at g mail dot com',
                    'm a r i a n k a t e 6 9 7 @ g m a i l . c o m',
                    'this c h r i s t i n a w e s t 8 8 @ g m a i l d o t c o m',
                    'to my email s h y r a s m i t h 0 8 @ g m a i l . c o m got it...',
                    'me JANETHRAWNSLEY AT GMAIL DOT COM',
                    'this c h r i s t i n a w e s t 8 8 @ g m a i l d o t c o m',
                    'send like this -->s b r i n k l y s 2 6 @ g m a i l',
                    'ms a v a n n a h099 at G m a i l. o',
                    'like this s j 8 3 4 4 2 3 @ g m a i l',
                    'goodguy_gmail.com',
                ],
                'true negative' => $this->getNegative('email'),
            ],

            /**
             * gmail
             * %g+i+.{1,2}m+a+i+(l+)?%
             */
            '(g+i+.{1,2}m+a+i+(l+)?)' => [
                'title' => 'email',
                'description' => 'Detect the word "gmail" with different spellings',
                'true positive' => [
                    'gi-mail',
                ],
                'true negative' => $this->getNegative('email'),
            ],

            /**
             * gmail
             * %g+[ijl]?\w?m+\wa+\wl+%
             */
            '(g+[ijl]?\w?m+\wa+\wl+)' => [
                'title' => 'email',
                'description' => 'Detect the word "gmail" with different spellings',
                'true negative' => $this->getNegative('email'),
            ],

            /**
             * gmail underscore
             * %(g|j|jee)[ /\\\.\-\*_]*(m|м)[ /\\\.\-\*_]*(a|а|e)[ /\\\.\-\*_]*(i|l|1)[ /\\\.\-\*_]*(l|i|1)%ui
             * %(g|j|jee)[ /\\\.\-\*_]*(m|м)[ /\\\.\-\*_]*(a|а|e)[ /\\\.\-\*_]*(i|l|1)[ /\\\.\-\*_]*(l|i|1)([^f,v]|$)%
             * %(g|j|jee)[ /\\\.\-\*_]*(m|м)[ /\\\.\-\*_]*(a|а|e)[ /\\\.\-\*_]*(i|l|1)[ /\\\.\-\*_]*(l|i|1)([^f,g,k,v]|$)%
             */
            '((^|[\W_]|at|dog)(g+|je*)(?<sym>[ \/\\\\.\-\*_]*)[mм]\g<sym>[a|а|e]\g<sym>[il1]\g<sym>[li1]([^fgkv,]|$))' => [
                'title' => 'gmail underscore',
                'description' => 'Detect the word "gmail" with different spellings',
                'exceptions' => [
                    'do not detect "f", "g", "v", "k" at the end of the phrase to skip words like "life", "light", "live", "like"',
                ],
                'true positive' => [
                    'gmail',
                    'gmail.com',
                    'gmaildotcom',
                    'gmail_dot_com',
                    'g m a i l',
                    'jee mail',
                    'gMa11',
                    'Re: canderson9289 at g mail dot com',
                    'm a r i a n k a t e 6 9 7 @ g m a i l . c o m',
                    'this c h r i s t i n a w e s t 8 8 @ g m a i l d o t c o m',
                    'to my email s h y r a s m i t h 0 8 @ g m a i l . c o m got it...',
                    'me JANETHRAWNSLEY AT GMAIL DOT COM',
                    'this c h r i s t i n a w e s t 8 8 @ g m a i l d o t c o m',
                    'send like this -->s b r i n k l y s 2 6 @ g m a i l',
                    'ms a v a n n a h099 at G m a i l. o',
                    'like this s j 8 3 4 4 2 3 @ g m a i l',
                    'gigoman_dog_G_M_eil_com',
                    'gigowumen_@_G_M_eil_com',
                    'goodguy_gmail.com',
                    'g_ma-i_l',
                ],
                'true negative' => $this->getNegative('email'),
            ],

            /**
             * email
             * %e?.?m[ /\\\.\-\*]*a[ /\\\.\-\*]*(i|1)[ /\\\.\-\*]*(l|i|1)%ui
             * %[eg@]+.?m[ \/\\.\-\*]*a[ \/\\.\-\*]*(i|1)[ \/\\.\-\*]*(l|i|1)%
             */

            /**
             * email underscore
             * %(^|\s)(e|a)+.?m+(a|e|i)(i|y|l)(l|i|1)%ui
             * %(^|\s)(?!ameli)(e|a)+.?m+(a|e|i)(i|y|l)(l|i|1)%
             * %(^|\s)(?!ameli|amali)(e|a)+.?m+(a|e|i)(i|y|l)(l|i|1)%
             */

            /**
             * email
             * %(^|\s)e+(i|l|y)*\s?m*h?n*\s?(a|i)+\s?(e|i)*\s?(l|i)+(\s|$)%ui
             * %(^|\s)e+(i|l|y)*\s?m*h?n*(?!\sall\s)\s?(a|i)+\s?(e|i)*\s?(l|i)+(\s|$)%
             */

            /**
             * e-address
             * %(e|i|l)+m*.?a+d+r+e+(s|z)+%ui
             */
            '((e|i|l)+m*.?a+d+r+e+(s|z)+)' => [
                'title' => 'email address',
                'description' => 'Detect the phrase "email address" with different spellings',
                'true positive' => [
                    'email address',
                    'e-addres',
                ],
                'true negative' => $this->getNegative('email'),
            ],

            /**
             * e-address
             * %e+(m+)?a+(.{1,2})?d+(.{1,2})?r+e+[sz]+%
             */
            '(e+(m+)?a+(.{1,2})?d+(.{1,2})?r+e+[sz]+)' => [
                'title' => 'email',
                'description' => 'Detect the phrase "email address" with different spellings',
                'true positive' => [
                    'eadress',
                    'emadrez',
                ],
                'true negative' => $this->getNegative('email'),
            ],

            /**
             * e-address
             * %e+m+a+d+(r+)?(e+)?\w{1,2}[sz]+%
             */
            '(e+m+a+d+(r+)?(e+)?\w{1,2}[sz]+)' => [
                'title' => 'email',
                'description' => 'Detect the phrase "email address" with different spellings',
                'true positive' => [
                    'emadrez',
                ],
                'true negative' => $this->getNegative('email'),
            ],

            /**
             * Email special
             * %(^|\s)e+.?(m|n)+(a|i)+\s?(i|e|l|!)\s?(l|i|\||!)%ui
             */

            /**
             * Email
             * %\se+.?m+.?a+.?i+[lij\|]+%
             */

            /**
             * Email
             * %e+r+n+a+i+%
             */
            '(e+r+n+a+i+)' => [
                'title' => 'email',
                'description' => 'Detect the word "email" with letters "rn" instead "m"',
                'true positive' => [
                    'ernail',
                ],
                'true negative' => $this->getNegative('email'),
            ],

            /**
             * Email
             * %[^frhu]e+[l]?m+a+([ij\|:¦\!]+)?[l\!1\|¦:]+%
             */

            /**
             * Email
             * %e+(.{1,2})?m+a+i+(.{1,2})?[l\!1\|¦:]+%
             */

            /**
             * Email
             * %^e+(.{1,2})?m+(.{1,2})?[aey]+[l\!1\|¦:]+e?%
             */

            /**
             * Email
             * %m+e+([ij\|:¦\!]+)?y+l+%
             */

            /**
             * Email
             * %[ef]+\s?.{1,2}m+a+[ij\|:¦\!]+[l\!1\|¦:]+%
             */

            /**
             * Email
             * %[^bdfhnptvzr]e+[^bdfhnptvz](\w{1,2})?m+(\w{1,2})?[ae]+i+l+%
             */

            /**
             * Email
             * %e+m+[ea]+y+[lij\|:¦\!]+%
             */

            /**
             * Email
             * %e+(?![pt])\wm+\wa+\wl%
             */

            /**
             * Email
             * %e+(?![pt])\wm+\wa+\wl%
             */

            /**
             * Email
             * %e+-?[\/(]v[\\)]a+[ij1l]+%
             */
            '(e+-?[\/(|i]v[\\\)|i]a+[ij1l]+)' => [
                'title' => 'email',
                'description' => 'Detect the word "email" with something like "/v\" instead "m"',
                'true positive' => [
                    'e/V\ail',
                    'e(V)ail',
                    'e|V|ail',
                    'eIVIail',
                ],
                'true negative' => $this->getNegative('email'),
            ],

            /**
             * Email
             * %e+-?[\/\\\.\-\*_]+a+[1ijl]+%
             */
            '((^|[\W_])e+[\/\\\.\-\*_]+a+[1ijl]+)' => [
                'title' => 'email',
                'description' => 'Detect the word "email" with some characters like "*", "." instead "m"',
                'true positive' => [
                    'e*ail',
                ],
                'true negative' => $this->getNegative('email'),
            ],

            /**
             * E-message
             * %e+.?m+e+s+a+g+e%
             */
            '((^|[\W_])e+.?m+e+s+a+g+e)' => [
                'title' => 'email',
                'description' => 'Detect the word "e-message" with different spellings',
                'true positive' => [
                    'emesage',
                    'e-mesage',
                ],
            ],


            /**
             * @
             * %e+.?m+e+s+a+g+e%
             */
            '(at\ssymbol)' => [
                'title' => '@ symb',
                'description' => 'Detect the phrase "at symbol"',
            ],

            /**
             * email in word
             * %(^|\s)(e|i)+[\s]?(m|w|u|h|i)+[\S]?(a|e|i|o|y)+\s?(a|i|e|l|j)+(l|i|e)+%ui
             * %(^|\s)(e|i)+[\s]?(?!will|while)(m|w|u|h|i)+[\S]?(a|e|i|o|y)+\s?(a|i|e|l|j)+(l|i|e)+%ui
             */

            /**===================================================================================
             * HOTMAIL, etc...
             **===================================================================================*/

            /**
             * hotmail
             * %(h|н).?(o|0|о|a|а|u)?.?(t|т)\S?(m|м).?(a|а).?(i|і|1).?(l|i|1)%ui
             * %(h|н).?(o|0|о|a|а|u)?.?(t|т)\s?(m|м).?(a|а).?(i|і|1).?(l|i|1)%
             */
            '((h|н).?(o|0|о|a|а|u)?.?(t|т).?(m|м).?(a|а).?(i|і|1).?(l|i|1))' => [
                'title' => 'hotmail',
                'description' => 'Detect the word "hotmail" with different spellings',
                'true positive' => [
                    'hotmail',
                    'h o t m a i l',
                    'h o tm a 1 l',
                    'blowzhotmailcouk',
                    'whenever steamymeggers80 at h o tm a i l c o m and we',
                    'searching for a naughty match in my area a l l a n _ r u l z _ 05 at h o tm a i l c o',
                    'h0_t_mai_l',
                ],
                'true negative' => $this->getNegative('email'),
            ],

            /**
             * rocketmail
             * %r.?(o|0).?(c|с).?(k|к).?(e|е).?(t|т).?(m|м).?(a|а).?(i|1).?(i|l)%ui
             */
            '(r.?(o|0).?(c|с).?(k|к).?(e|е).?(t|т).?(m|м).?(a|а).?(i|1).?(i|l))' => [
                'title' => 'rocketmail',
                'description' => 'Detect the word "rocketmail" with different spellings',
                'true positive' => [
                    'rocketmail',
                    'rocket mail',
                    'r_0C_ket_ma_i_I',
                ],
                'true negative' => $this->getNegative('email'),
            ],

            /**===================================================================================
             * YAHOO
             **===================================================================================*/

            /**
             * yahoo short
             * %y[^a-z]*(a[^a-z]*)+h[^a-z]*(0|o|u)%ui
             * %y[^a-z]*(a[^a-z]*)+h[^a-z]*(0|o|u)+\b%ui
             */
            '(y[^a-z]*(a[^a-z]*)+h[^a-z]*(0|o|u)+\b)' => [
                'title' => 'yahoo short',
                'description' => 'Detect the word "yahoo" with different spellings',
                'true positive' => [
                    'yahoo',
                ],
                'true negative' => $this->getNegative('yahoo'),
            ],

            /**
             * yahoo
             * %y.?ah[^o\s]?o.?%ui
             * %y[^a-zA-Z0-9]*a([^a-zA-Z0-9]+h[^a-zA-Z1-9]+|h)\S?[o|0|u](\W+|$)%
             * %y+[^a-zA-Z0-9]*a+([^a-zA-Z0-9]+h+[^a-zA-Z1-9]+|h+)\S?[o|0|u]+(\W+|$)%
             */
            '(y+[^a-zA-Z0-9]*a+([^a-zA-Z0-9]+h+[^a-zA-Z1-9]+|h+)\S?[o|0|u]+(\W+|$))' => [
                'title' => 'yahoo',
                'description' => 'Detect the word "yahoo" with different spellings',
                'true positive' => [
                    'yahoo',
                    'yah)o',
                    'yyaahh00',
                    'me@yahoo.com',
                    'iloveniedy underscore 05 att yahoo dott com',
                    'ichellewhite1918 GMAIL•c0m oR hotsexy_10mitch YAh0O•c0m',
                    'my email is caseyxbrown79 at y a h o o dot c o m',
                    'it says that account doesnt exist. samsonsolomon760@yahoo',
                    'Email Me: martinezjosie10 -A-t- -Y-a-h-o-o- -D-o-t- C-o-m-)',
                    'martinezjosie10 /@/ y/a/h/o/o / ./ / /c/o/m/.',
                    'jessieseekinglove @ y a h o o . c a',
                    'and my yA hO0 is s m o o t h 19 s a n y a at y a h O 0 . c 0 m',
                    'g r a c i x 12// at// y a h oo// dot// c om……',
                    'email l/a/s/s/e/y/r/e/be/c/c/a at y/a/h/o/o/ d/o/t c/om',
                    'get hookup on ya/h/o/ add me',
                    'y_a_h00',
                ],
                'true negative' => $this->getNegative('yahoo'),
            ],

            /**===================================================================================
             * OUTLOOK
             **===================================================================================*/

            /**
             * outlook
             * %[o0]+u+t+l+[o0]+[o0]+k+%
             */
            '([o0]+u+t+[l|]+[o0]+[o0]+k+)' => [
                'title' => 'outlook',
                'description' => 'Detect the word "outlook" with different spellings',
                'true positive' => [
                    'outlook',
                    'outlook',
                    '0ut|ook',
                    'outl00k',
                ],
            ],

            /**
             * outlook new
             * %(o|0)[^a-z]*u[^a-z]*t[^a-z]*(l|\\|)[^a-z]*(o|0)[^a-z]*(o|0)[^a-z]*k%i
             * %(o|0)[^a-z]*u[^a-z]*t[^a-z]*(l|\\|)[^a-z]*(o|0)[^a-z]*(o|0)[^a-z]*k+\b%
             */
            '((o|0)[^a-z]*u[^a-z]*t[^a-z]*(l|\\|)[^a-z]*(o|0)[^a-z]*(o|0)[^a-z]*k+\b)' => [
                'title' => 'outlook',
                'description' => 'Detect the word "outlook" with different spellings',
                'true positive' => [
                    'like this j a m i e w o l f e 1 @ o u t l o o k c o m',
                    'outlook',
                    'o u t l o o k',
                    '0 u t | o o k',
                    'outl00k',
                    'out look',
                ],
                'true negative' => [
                    'he could care less about looking at your dick',
                ],
            ],

            /**===================================================================================
             * FACEBOOK
             **===================================================================================*/

            /**
             * facebook1 underscore
             * %([ \.,_]|^)fb([ \.,_]|$)%i
             */
            '(([ \.,_]|^)fb([ \.,_]|$))' => [
                'title' => 'facebook underscore',
                'description' => 'Detect the sequence "fb"',
                'true positive' => [
                    'im on fb babe',
                    'underscored_fb_type',
                    'fb',
                    'word fb',
                    'fb word',
                    'word fb word',
                ],
                'true negative' => $this->getNegative('facebook'),
            ],

            /**
             * fyesbuuk
             * %f+a*i*y*e*(s|c)+e*\s?(b|v)(o|u)%ui
             * %f+a*i*y*e*(s|c)+e*\s?(b|v)(o|u)([^r]|$)%
             */
            '(f+a*i*y*e*(s|c)+e*\s?(b|v)(o|0|u)([^rt]|$))' => [
                'title' => 'facebook',
                'description' => 'Detect the word "facebook" with different spellings',
                'true positive' => [
                    'facebook',
                    'facebuk',
                    'faceb0k',
                    'face book',
                    'face bu',
                    'face boo|<',
                ],
                'true negative' => $this->getNegative('facebook'),
            ],

            /**
             * fbook
             * %(f|p|fh|ph|fc|fce|fcee)[^a-z]*((b|bh)[^a-z]*)+((0|o|u|\(\.\)|\(\)|♥)[^a-z]*)+k%ui
             * %(f|fh|ph|fc|fce|fcee)[^a-z]*((b|bh)[^a-z]*)+((0|o|u|\(\.\)|\(\)|♥)[^a-z]*)+k%
             * %(([^o]|\b)f|fh|ph|fc|fce|fcee)[^a-z]*((b|bh)[^a-z]*)+((0|o|u|\(\.\)|\(\)|♥)[^a-z]*)+k%
             * %(([^o]|\b)f|fh|ph|fc|fce|fcee|ace)([^a-z]*|\W*(p[li1]u?s)\W*|\W*(and)\W*)((b|bh)[^a-z]*)+((0|o|u|\(\.\)|\(\)|♥)[^a-z]*)+k%
             */
            '((([^o]|\b)f|fh|ph|fc|fce|fcee|ace)([^a-z]*|\W*(p[li1]u?s)\W*|\W*(and)\W*)((b|bh)[^a-z]*)+((0|o|u|\(\.\)|\(\)|♥)[^a-z]*)+k)' => [
                'title' => 'facebook',
                'description' => 'Detect the word "fbook" with different spellings, incl variants like "f plus/and book"',
                'exceptions' => [
                    'do not detect "o" before "f", for example "what kind of books?"',
                ],
                'true positive' => [
                    'fbook',
                    'fb00k',
                    'f book',
                    'f-book',
                    'ace >>> book',
                    'f pls book',
                    'ace and book',
                ],
                'true negative' => $this->getNegative('facebook'),
            ],

            /**
             * facebook1
             * %\bf\W?b\W?\b%ui
             * %\bf[_\W]*b[_\W]*\b%
             */
            '(\bf[_\W]*b[_\W]*\b)' => [
                'title' => 'facebook',
                'description' => 'Detect the sequence "fb" with different spellings',
                'true positive' => [
                    'fb',
                    'f.b.',
                    'f/b',
                    'f__b__',
                    'F::B',
                ],
                'true negative' => $this->getNegative('facebook'),
            ],

            /**
             * facebook2
             * %(f|p|fh|ph|fp)(a|ay)+\w?(c|s)+(e|a)*((b|bh))+(o|u)+%ui
             */

            /**
             * face bo
             * %f.?(e|a|\s)+(s|c|y)+(e|s)+(d|z)*.?(b|v)+(o|u)+%ui
             * %f.?(e|a|_|\s)+(s|c|y)+(e|s)+(d|z)*.?(b|v)+(o|u)+[k\s\p{P}]%
             * %f.?(e|a|_)+.?(s|c|y)+.?(e|s)+.?(d|z)*.?(b|v)+.?(o|u)+[k\s\p{P}]%
             * %f.?(e|a|i|_)+.?(s|c|z|y)+.?(e|s)+.?(d|z)*.?(b|d|v)+.?(o|0|u|\.)+(c)*[k\s\p{P}]%
             */
            '(f.?(e|a|i|_)+.?(s|c|z|y)+.?(e|s)+.?(d|z)*.?(b|d|v)+.?(o|0|u|\.)+(c)*[k\s\p{P}])' => [
                'title' => 'facebook',
                'description' => 'Detect the word "facebook" with different spellings',
                'true positive' => [
                    'on facebook',
                    'on f a c e b o o k',
                    'my face book',
                    'f_a__ce_boo__k',
                    'fiaeciedook',
                    'feazebook',
                    'feiaaazzeebook',
                    'find me on faaceb .. k mine er FLARE NILSEN',
                    'fazzzieebock',
                ],
                'true negative' => $this->getNegative('facebook'),
            ],

            /**===================================================================================
             * SKYPE
             **===================================================================================*/

            /**
             * facebook3
             * %f*\S*a\S*c\S*e\S*b\S*o\S*k*%
             */
            '(f*\S*a\S*c\S*e\S*b\S*o\S*k*)' => [
                'title' => 'facebook',
                'description' => 'Detect letters "a+c+e+b+o" in one word',
                'true positive' => [
                    'acebook',
                    'faceboo',
                    'on facebook',
                    'f_a_c_e_b_o_o_k',
                    'FaaaxxCcceexxBoooxxxKK',
                    'faiceiboxok',
                ],
                'true negative' => $this->getNegative('facebook'),
            ],

            /**
             * facebook4
             * %f\S*([aecsxz])\S*([aecsxz])\S*([aecsxz])\S*[bd]\S*[ou0]\S*k%
             */
            '(f\S*([aecsxz])\S*([aecsxz])\S*([aecsxz])\S*[bd]\S*[ou0]\S*k)' => [
                'title' => 'facebook',
                'description' => 'Detect all words starting with "f" and ending with "k", min 7 characters length',
                'true positive' => [
                    'f_a_c_e_b_o_o_k',
                    'FaaaxxCcceexxBoooxxxKK',
                    'faiceiboxok',
                    'Fehhyseboock',
                    'Feyhhseboock',
                    'faxeaexboock',
                    'fazeaeaxboock',
                    'fcceeaaboook',
                    'fceaeccccccccbooock',
                    'feacxboock',
                    'feicce.booiuuuck',
                    'feiiccee.booiuuuck',
                ],
                'true negative' => $this->getNegative('facebook'),
            ],

            /**
             * skype underscore
             * %(s|\$)[ /\\\.\-\*_]*k[ /\\\.\-\*_]*(y|/|4)[ /\\\.\-\*_]*p%ui
             */
            '((s|\$)[ \/\\\.\-\*_]*k[ \/\\\.\-\*_]*(y|\/|4)[ \/\\\.\-\*_]*p)' => [
                'title' => 'skype underscore',
                'description' => 'Detect the word "skype" with different spellings',
                'true positive' => [
                    'skype',
                    'SkYPe',
                    '$kype',
                    'sk/pe',
                    'hi there how are you care to chat add me s *k *y *p *3* > dianne jones69',
                    'add me $ *k *4 *p *3* > dianne jones69',
                    'nine sk/pe pink',
                    'sk_y_pe',
                    's___k_y_p',
                ],
                'true negative' => $this->getNegative('skype'),
            ],

            /**
             * skype
             * %\b(sk|sky)(.*(p[li1]u?s).*|.*(and).*)(ype|pe)\b%
             *
             * Детектит skype с модификациями
             */
            '(\b(sk|sky)(.*(p[li1]u?s).*|.*(and).*)(ype|pe)\b)' => [
                'title' => 'facebook',
                'description' => 'Detect the word "skype" variants like "sk plus/and ype"',
                'true positive' => [
                    'sk and ype',
                    'sky plus pe',
                    'SKq and qYPE',
                    'sky--plus--pe',
                ],
                'true negative' => $this->getNegative('skype'),
            ],

            /**
             * skype me
             * %(\s|^)s+k+\S*\sme%ui
             * %(\s|^)s+k+\S*\sme(e|\W+|$)%
             */
            '((\s|^)s+k+\S*\sme(e|\W+|$))' => [
                'title' => 'skype me',
                'description' => 'Detect the phrase "skype me" with different spellings',
                'exceptions' => [
                    'do not detect any letters [a-z] after "me" except "e" to skip words like "men"',
                ],
                'true positive' => [
                    'skype me',
                    'sk me',
                ],
                'true negative' => $this->getNegative('skype'),
            ],

            /**
             * skype in word
             * %(s|x|z)x?p?(q|k|i|h|l)+x?h?r?l?e?\s?(v|y|i|a)\S*p%ui
             * %(s|x|z)x?p?(q|k|i)+x?h?r?l?e?\s?(v|y|i|a)\S*p%
             */

            /**
             * skype short
             * %(s|\$)+k+(y|/|4)+p+%i
             */

            /**
             * skype dupe
             * %s+k+p+e+%i
             */

            /**
             * skype
             * %(?<![a-z])(s|\$|z|5)+[^a-z]*((k|c|<|l<)+[^a-z]*)+((a|i|h)[^a-z]*)*((((y|/|4|i|Ӱ|7|3)+[^a-z]*)+((i|h|k)[^a-z]*)*(p|þ|₱)+)|(((p|þ|₱)[^a-z]*)+((e|y|/|4|Ӱ|7|3)+[^a-z]*)+))(?(?<!skip)|e)%ui
             * %(?<![a-z])(s|\$|z|5)+[^a-z]*((k|c|<|l<)+[^a-z]*)+((a|i|h)[^a-z]*)*((((y|/|4|i|Ӱ|7|3|v)+[^a-z]*)+((i|h|k)[^a-z]*)*(p|þ|₱)+)|(((p|þ|₱)[^a-z]*)+((e|y|/|4|Ӱ|7|3)+[^a-z]*)+))(?(?<!skip)|e)%
             * %(?<![a-z])(s|\$|w|z|5)+[^a-z]*((k|c|<|l<)+[^a-z]*)+((a|i|h|l)[^a-z]*)*((((y|\/|4|i|j|Ӱ|7|3|v)+[^a-z]*)+((i|h|k)[^a-z]*)*(p|þ|₱)+)|(((p|þ|₱)[^a-z]*)+((e|y|\/|4|Ӱ|7|3)+[^a-z]*)+))(?(?<!skip)|e)%
             */
            '((?<![a-z])(s|\$|w|z|5)+[^a-z]*((k|c|<|l<)+[^a-z]*)+((a|i|h|l)[^a-z]*)*((((y|\/|4|i|j|Ӱ|7|3|v)+[^a-z]*)+((i|h|k)[^a-z]*)*(p|þ|₱)+)|(((p|þ|₱)[^a-z]*)+((e|y|\/|4|Ӱ|7|3)+[^a-z]*)+))(?(?<!skip)|e))' => [
                'title' => 'skype',
                'description' => 'Detect the word "skype" with different spellings',
                'true positive' => [
                    'skype',
                    'SkYPe',
                    'skvpe',
                    'skipe',
                    'sklpe',
                    '$kype',
                    'sk/pe',
                    'skpe',
                    'skyp',
                    '$kyp',
                    's k y p e',
                    's.k.y.p.e',
                    's--k--y--p--e',
                    'hi there how are you care to chat add me s *k *y *p *3* > dianne jones69',
                    'add me $ *k *4 *p *3* > dianne jones69',
                    'nine sk/pe pink',
                    'sk_y_pe',
                    's___k_y_p',
                    's.l<,yp\'e',
                    's-kjyp\'e',
                    'wkype',
                    'skpye',
                ],
                'true negative' => $this->getNegative('skype'),
            ],

            /**===================================================================================
             * POPULAR MESSENGERS AND DOMAINS
             **===================================================================================*/

            /**
             * messengers patterns Viber
             * %(v+i+b+e+r)%
             */
            '(v+i+b+e+r)' => [
                'title' => 'Messengers patterns: Viber',
                'description' => 'Detect the word "Viber"',
                'true positive' => [
                    'viber',
                ],
            ],

            /**
             * messengers patterns WhatsApp
             * %(w+h+a+t+s+a+p+p+)%
             */
            '(w+h+a+t+s+a+p+p+)' => [
                'title' => 'Messengers patterns: WhatsApp',
                'description' => 'Detect the word "WhatsApp"',
                'true positive' => [
                    'whatsapp',
                ],
            ],

            /**
             * messengers patterns Hangouts
             * %(h+a+n+g+[o0]+u+t+(s+)?)%
             */
            '(h+a+n+g+[o0]+u+t+(s+)?)' => [
                'title' => 'Messengers patterns: Hangouts',
                'description' => 'Detect the word "Hangouts"',
                'true positive' => [
                    'hangouts',
                    'hangout',
                ],
                'true negative' => [
                    'hang out',
                ],
            ],

            /**
             * messengers patterns Snapchat
             * %(s+n+a+p+c+h+a+t+)%
             */
            '(s+n+a+p+c+h+a+t+)' => [
                'title' => 'Messengers patterns: Snapchat',
                'description' => 'Detect the word "Snapchat"',
                'true positive' => [
                    'snapchat',
                ],
            ],

            /**
             * messengers patterns Twitter
             * %(t+w+i+t+t+e+r+)%
             */
            '(t+w+i+t+t+e+r+)' => [
                'title' => 'Messengers patterns: Twitter',
                'description' => 'Detect the word "Twitter"',
                'true positive' => [
                    'twitter',
                ],
            ],

            /**
             * messengers patterns Telegram
             * %(t+e+l+e+g+r+a+m+)%
             */
            '(t+e+l+e+g+r+a+m+)' => [
                'title' => 'Messengers patterns: Telegram',
                'description' => 'Detect the word "Telegram"',
                'true positive' => [
                    'telegram',
                ],
            ],

            /**
             * domain patterns Yandex
             * %(t+e+l+e+g+r+a+m+)%
             */
            '(y+a+n+d+e+x+)' => [
                'title' => 'yandex',
                'description' => 'Detect the word "yandex"',
                'true positive' => [
                    'yandex',
                ],
            ],

            /**
             * domain patterns iCloud
             * %(t+e+l+e+g+r+a+m+)%
             */
            '(i+c+l+o+u+d+)' => [
                'title' => 'icloud',
                'description' => 'Detect the word "icloud"',
                'true positive' => [
                    'icloud',
                ],
            ],

            /**
             * domain patterns Verizon
             * %v+e+r+i+z+[o0]+n+%
             */
            '(v+e+r+i+z+[o0]+n+)' => [
                'title' => 'verizon',
                'description' => 'Detect the word "verizon"',
            ],

            /**
             * domain patterns EarthLink
             * %e+a+r+t+h+l+i+n+k+%
             */
            '(e+a+r+t+h+l+i+n+k+)' => [
                'title' => 'earthlink',
                'description' => 'Detect the word "earthlink"',
            ],

            /**
             * domain patterns Google
             * %g+[o0]+[o0]+g+l+e+%
             * %g+[^a-zA-Z0-9]*[o0]+[^a-np-zA-NP-Z1-9]*g+[^a-zA-Z0-9]*l+[^a-zA-Z0-9]*e+%
             */
            '(g+[^a-zA-Z0-9]*[o0]+[^a-np-zA-NP-Z1-9]*g+[^a-zA-Z0-9]*l+[^a-zA-Z0-9]*e+)' => [
                'title' => 'google',
                'description' => 'Detect the word "google"',
                'true positive' => [
                    'google',
                    'g00gle',
                    'gogle',
                    'g o o g l e',
                    'g̶o̶o̶g̶l̶e̶h̶a̶n̶g̶o̶u̶t̶',
                ],
            ],

            /**
             * domain patterns BellSouth
             * %b+e+l+l+s+[o0]+u+t+h+%
             */
            '(b+e+l+l+s+[o0]+u+t+h+)' => [
                'title' => 'bellsouth',
                'description' => 'Detect the word "bellsouth"',
            ],

            /**
             * domain patterns Pobox
             * %p+[o0]+b+[o0]+x+%
             */
            '(p+[o0]+b+[o0]+x+)' => [
                'title' => 'pobox',
                'description' => 'Detect the word "pobox"',
            ],

            /**
             * domain patterns Prodigy
             * %p+r+o+d+i+g+y+%
             */
            '(p+r+o+d+i+g+y+)' => [
                'title' => 'prodigy',
                'description' => 'Detect the word "prodigy"',
            ],

            /**
             * domain patterns T-Online
             * %t+\-+[o0]+n+l+i+n+e+%
             */
            '(t+\-+[o0]+n+l+i+n+e+)' => [
                'title' => 't-online',
                'description' => 'Detect the word "t-online"',
            ],

            /**
             * domain patterns epost
             * %e+\w{1,2}p+o+s+t+%
             */
            '(e+\w{1,2}p+o+s+t+)' => [
                'title' => 'epost',
                'description' => 'Detect the word "epost"',
            ],

            /**===================================================================================
             * OTHER RULES
             **===================================================================================*/

            /**
             * dot com
             * %(\.|(d)[ /\\\.\-\*]*(o|0|о)[ /\\\.\-\*]*(t))[ /\\\.\-\*]*(c|с)[ /\\\.\-\*]*(o|0|u|o)*[ /\\\.\-\*]*(m|м)%ui
             */

            /**
             * dotcom underscore
             * %(\.|(d)[ /\\\.\-\*_]*(o|0|о)[ /\\\.\-\*_]*(t))[ /\\\.\-\*_]*(c|с)[ /\\\.\-\*_]*(o|0|u|o)*[ /\\\.\-\*_]*(m|м)%ui
             */

            /**
             * sms
             * %([ \-\.\,]|^)sms([ \-\.\,]|$)%ui
             * true positive:
             *   send me sms
             *   sms me
             *   send sms to my number
             * true negative:
             *   orgasms
             */

            /**
             * Uppercase characters (removed)
             * %[a-z][^\n\s\.\!\?'",][A-Z]%ui
             */

            /**
             * Punctuation characters (removed)
             * %([;\)\}\+`\]\^\|]\w|('\S*')|([/\?!\*][^/\?!\*\s:]\S*){2,}|(\w"\w)|\w[\(\{\[\|])%ui
             */

            /**
             * three spaces (removed)
             * %\s[^iu]\s[^yr]\s[^md]\s[^m]%ui
             */
        ];

        return $patterns;
    }

    /**
     * Get negative examples for the regexp patterns above
     *
     * @param string $class
     * @return array
     */
    private function getNegative($class): array
    {
        $patterns = [
            'email' => [
                'ee',
                'mm',
                'a big mistake',
                'asking me',
                'i cant i left them all at home xx',
                'i will gonna wear em all for you',
                'i wanna feel your rock hard dick touching me lightly',
                'if you wanna chat, mail me',
                'just drop a Mail to say hi!',
                'keep fucking me like that and I am going to cum so hard',
                'letting me live my own life',
                'my name is Ameli',
                'my name is Amalia',
                'yu seem a little frustrated lol',
            ],
            'facebook' => [
                'fab',
                'fbcd',
                'omfb',
                'face on book',
                'fuckin\'book',
                'interface burned',
                'Aww thanks, but tbh I\'m not much of a book person sorry. But you could tell me whats your favourite authour is?',
                'Do you want to lick my pussy? tell me how do you want to fuck me? ok...see',
                'hello are you willing to vote my hot sexy link baby is free baby you like baby',
                'mm,till i cum on ur face babe,,',
                'mm,yep,,of course babe,,u can bury ur face babe',
                'Nah I\'m naturally brown I don\'t need the sun. I\'m more of a read book, suck a dick, pass it over and get on with life kinda girl',
                'right, then why are you here, if you want there, you cant show face but a dick is okay?:)',
                'usually they are we all end up booking hotels',
                'what do you mean of that book a car?',
                'what kind of books? i love outdoor fun',
                'yes please babe.. cum in my face babe..',
                'You still don\'t have your moms face burned in your head yet',
            ],
            'number' => [
                'namba',
            ],
            'phone' => [
                'There are some many more components to a real relationship',
            ],
            'skype' => [
                'sky',
                'ship',
                'skip',
                'slip',
                'Are you sexy please take your shirt',
                'estoy preparada para recibirlo todo',
                'I guess it\'s a bit forward. We need more time to get to know each other better. Don\'t you think so? My pleasure to meet ya.',
                'hum! je veux que tu sois hyper cochon avec moi mon amour',
                'light skin men',
                'love put you over my knee and smack ypur bott for being such',
                'me gusta aquí. No estoy preparada conectarte hasta ahora no doy mi info a desconosidos, no quiero ser perseguida de maniaco',
                'Mmm lick my pussy please',
                'perdona...pero no estoy preparada conectar contigo :)',
                'peut etre que toi meme tu nee s\'est pas t\'y prendre',
                'right now i am thinking more fun so i just join here i love rubbing my pussy pretty horny',
                'si aceptas, por favor',
                'Soy periodista y me gusta estar en el centro de atención.',
                'tes lèvres me donnes des envies hyper cochonnes ahaah',
                'That would be fun. I respect privacy, so when you\'re comfortable...you can send one. What\'s your name? Are you the \'straight, no chaser\' type or \'something sweet on the rocks\' type?',
                'The food I like usually depends on my mood! Right now, Chinese food sounds delicious! My personal favorite dish - grilled prawns with tropical fruit salsa! mmm delicious...',
                'This is my personal perfume to seduce you naughty boy',
                'wood luv to fuck ypuwood luv to fuck ypu',
                'yes inside to my pussy please',
                'you are on hands type man ? that is good',
            ],
            'yahoo' => [
                'nahooy',
                'ya 3 hour',
                'ya.. How are you',
                'Yah our',
                'Yah ur are',
                'i have a kid, daughter... in Cuyahoga Falls',
            ],
        ];

        return $patterns[$class];
    }

    private function buildExpression(): string
    {
        $patterns = $this->getPatterns();

        return sprintf('/%s/iu', implode('|', array_keys($patterns)));
    }
}
