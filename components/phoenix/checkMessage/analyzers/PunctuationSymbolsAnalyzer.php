<?php

namespace app\components\phoenix\checkMessage\analyzers;

use app\components\phoenix\checkMessage\AnalysisResult;
use app\components\phoenix\checkMessage\Analyzer;
use app\components\phoenix\checkMessage\AnalyzerData;
use app\components\phoenix\checkMessage\Text;

class PunctuationSymbolsAnalyzer implements Analyzer
{
    const MATCH_COUNT = 4;

    /**
     * @var AnalyzerData
     */
    private $analyzerData;

    /**
     * PunctuationSymbols constructor.
     *
     * @param AnalyzerData $analyzerData
     */
    public function __construct(AnalyzerData $analyzerData)
    {
        $this->analyzerData = $analyzerData;
    }

    /**
     * Analyze text
     *
     * @param Text $text
     *
     * @return AnalysisResult
     */
    public function analyze(Text $text): AnalysisResult
    {
        $isSuspicious = false;
        $suspiciousWords = [];
        $expression = $this->buildExpression();

        foreach ($text->getWordsList() as $string) {
            $matches = [];
            if ($this->findMatches($expression, $string, $matches) >= self::MATCH_COUNT) {
                $isSuspicious = true;
                foreach ($matches as $match) {
                    $suspiciousWords[] = $match[0];
                }
            }
        }

        return new AnalysisResult($text, $isSuspicious, $suspiciousWords, $expression);
    }

    /**
     * Find matches in text
     *
     * @param  string $expression   - Regular expression string
     * @param  string $subject      - Text to searching matches
     * @param  array  $matchedWords - Matches
     *
     * @return int                  - Returns count of matches
     */
    private function findMatches(string $expression, string $subject, array &$matchedWords = []): int
    {
        return preg_match_all($expression, $subject, $matchedWords, PREG_SET_ORDER);
    }

    /**
     * Build regular expression
     *
     * @return string
     */
    private function buildExpression(): string
    {
        $punctuationList = array_map('preg_quote', $this->analyzerData->getPunctuationChars());
        $expression = sprintf('/\w[%s]/', implode('', $punctuationList));

        return $expression;
    }
}
