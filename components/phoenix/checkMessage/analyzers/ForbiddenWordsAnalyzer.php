<?php

namespace app\components\phoenix\checkMessage\analyzers;

use app\components\phoenix\checkMessage\AnalysisResult;
use app\components\phoenix\checkMessage\Analyzer;
use app\components\phoenix\checkMessage\Text;

/**
 * Class ForbiddenWordsAnalyzer
 * @package  Phoenix\AntiScamm\Components\SuspiciousWords\Analyzers
 */
class ForbiddenWordsAnalyzer implements Analyzer
{
    /**
     * @var string[]
     */
    private $forbiddenWordsList;

    /**
     * PunctuationSymbols constructor.
     *
     * @param string[] $forbiddenWordsList
     */
    public function __construct(array $forbiddenWordsList)
    {
        $this->forbiddenWordsList = $forbiddenWordsList;
    }

    /**
     * Analyze text
     *
     * @param Text $text
     *
     * @return AnalysisResult
     */
    public function analyze(Text $text): AnalysisResult
    {
        $isSuspicious = false;
        $suspiciousWords = [];

        if (!empty($this->forbiddenWordsList)) {
            $originalText = $text->getOriginalText();
            $replacedText = str_ireplace($this->forbiddenWordsList, '', $originalText);
            $isSuspicious = $replacedText !== $originalText;

            if ($isSuspicious) {
                $suspiciousWords = $this->findDifferences($originalText, $replacedText);
            }
        }

        return new AnalysisResult($text, $isSuspicious, $suspiciousWords);
    }

    /**
     * @param string $originalText
     * @param string $replacedText
     *
     * @return array
     */
    private function findDifferences(string $originalText, string $replacedText): array
    {
        $differences = [];
        $originalTextLength = mb_strlen($originalText);
        $auxCollectedString = '';
        $auxIsWordMismatch = false;
        $originalTextWordIterator = 0;
        $replacedTextWordIterator = 0;

        while ($originalTextWordIterator <= $originalTextLength) {
            if (isset($originalText{$originalTextWordIterator})
                && (!isset($replacedText[$replacedTextWordIterator])
                    || $replacedText[$replacedTextWordIterator] !== $originalText[$originalTextWordIterator]
                )
            ) {
                $auxCollectedString .= $originalText{$originalTextWordIterator};
                $auxIsWordMismatch = true;
            } else {
                if ($auxIsWordMismatch) {
                    $differences[] = trim($auxCollectedString);
                    $auxCollectedString = '';
                    $auxIsWordMismatch = false;
                }

                ++$replacedTextWordIterator;
            }

            ++$originalTextWordIterator;
        }

        return $differences;
    }
}
