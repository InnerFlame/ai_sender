<?php
namespace app\components\phoenix\checkMessage\analyzers;

use app\components\phoenix\checkMessage\Analyzer;

/**
 * Class RealScamKeywordsAnalyzer
 * @package  Phoenix\AntiScamm\Components\SuspiciousWords\Analyzers
 */
class RealScamKeywordsAnalyzer extends ForbiddenWordsAnalyzer implements Analyzer
{
    // It does the same as the ForbiddenWordsAnalyzer, but has a different list of words and reason
}
