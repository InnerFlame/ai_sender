<?php

namespace app\components\phoenix\checkMessage\analyzers;

use app\components\phoenix\checkMessage\AnalysisResult;
use app\components\phoenix\checkMessage\Analyzer;
use app\components\phoenix\checkMessage\AnalyzerData;
use app\components\phoenix\checkMessage\Text;

/**
 * Class DigitsAnalyzer
 * @package  Phoenix\AntiScamm\Components\SuspiciousWords\Analyzers
 */
class DigitsAnalyzer implements Analyzer
{
    const DIGITS_COUNT = 3;

    /**
     * @var AnalyzerData
     */
    private $analyzerData;

    /**
     * PunctuationSymbols constructor.
     *
     * @param AnalyzerData $analyzerData
     */
    public function __construct(AnalyzerData $analyzerData)
    {
        $this->analyzerData = $analyzerData;
    }

    /**
     * Analyze text
     *
     * @param Text $text
     *
     * @return AnalysisResult
     */
    public function analyze(Text $text): AnalysisResult
    {
        $isSuspicious = false;
        $suspiciousWords = [];
        $pattern = '';

        $digits = $this->findDigits($text);
        $stringDigits = $this->findStringDigits($text);

        if ($digits['isSuspicious']) {
            $isSuspicious = true;
            $pattern = $digits['pattern'];
            $suspiciousWords = $digits['matches'];
        } elseif ($stringDigits['isSuspicious']) {
            $isSuspicious = true;
            $pattern = $stringDigits['pattern'];
            $suspiciousWords = $stringDigits['matches'];
        }

        return new AnalysisResult($text, $isSuspicious, $suspiciousWords, $pattern);
    }

    /**
     * @param  Text $text
     * @return array
     */
    private function findDigits(Text $text): array
    {
        $pattern = sprintf(
            '/(.?+[\d]{2,}.?+){%s,}|(\d.{1,3}){%s,}|(\w+[\d]\w+){%s,}/',
            self::DIGITS_COUNT,
            self::DIGITS_COUNT,
            self::DIGITS_COUNT
        );
        $isSuspicious = (bool) preg_match(
            $pattern,
            $text->getOriginalText(),
            $matches
        );

        return [
            'isSuspicious' => $isSuspicious,
            'pattern' => $pattern,
            'matches' => empty($matches[0]) ? [] : [$matches[0]],
        ];
    }

    /**
     * @param  Text $text
     * @return array
     */
    private function findStringDigits(Text $text): array
    {
        $matches = [];
        $digitsPattern = '/\b' . implode('|', $this->analyzerData->getStringDigits()) . '\b/i';
        $matchCount = preg_match_all(
            $digitsPattern,
            strtolower($text->getOriginalText()),
            $matches
        );
        $suspiciousDigits = reset($matches);
        $uniqueDigits = array_unique($suspiciousDigits);

        return [
            'isSuspicious' => $matchCount >= self::DIGITS_COUNT && count($uniqueDigits) > 1,
            'pattern' => $digitsPattern,
            'matches' => $suspiciousDigits,
        ];
    }
}
