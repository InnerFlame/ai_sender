<?php

namespace app\components\phoenix\checkMessage\analyzers;

use app\models\phoenix\phrase\PhraseCategory;
use app\models\phoenix\phrase\PhraseCensor;
use app\models\phoenix\phrase\PhraseTags;

/**
 * Class CensorPhraseAnalyzer
 */
class CensorPhraseAnalyzer
{
    /**
     * @param string $text
     * @return bool
     */
    public static function isWrongPhraseEmail(string $text): bool
    {
        return self::isWrongPhrase($text, 'email');
    }

    /**
     * @param string $text
     * @return bool
     */
    public static function isWrongPhraseScreenName(string $text): bool
    {
        return self::isWrongPhrase($text, 'screenname');
    }

    /**
     * @param string $text
     * @param string $keyword
     * @return bool
     */
    private static function isWrongPhrase(string $text, string $keyword = ''): bool
    {
        try {
            $checkingCfg = \Yii::$app->yaml->parseByKey('censorPhrase', $keyword);
            $searchAttributes = [];
            if (isset($checkingCfg['tag'])) {
                $tags = is_array($checkingCfg['tag']) ? $checkingCfg['tag'] : [$checkingCfg['tag']];
                $tagsId = array_keys(PhraseTags::find()->where(['IN', 'tag', $tags])->indexBy('id')->all());
                $searchAttributes['tagId'] = $tagsId;
            }

            if (isset($checkingCfg['phrasesCategory'])) {
                $categories = is_array($checkingCfg['phrasesCategory']) ? $checkingCfg['phrasesCategory'] : [$checkingCfg['phrasesCategory']];
                $categoriesId = array_keys(PhraseCategory::find()->where(['IN', 'name', $categories])->indexBy('id')->all());
                $searchAttributes['categoryId'] = $categoriesId;
            }

            $forbiddenWords = PhraseCensor::find()->where($searchAttributes)->all();

            if (!empty($forbiddenWords)) {
                foreach ($forbiddenWords as $wordRow) {
                    if (stripos($text, $wordRow['text']) !== false) {
                        return true;
                    }
                }
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}
