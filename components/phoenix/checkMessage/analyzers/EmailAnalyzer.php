<?php

namespace app\components\phoenix\checkMessage\analyzers;

use app\components\phoenix\checkMessage\AnalysisResult;
use app\components\phoenix\checkMessage\Analyzer;
use app\components\phoenix\checkMessage\AnalyzerData;
use app\components\phoenix\checkMessage\Text;

class EmailAnalyzer implements Analyzer
{
    /**
     * @var AnalyzerData
     */
    private $analyzerData;

    /**
     * PunctuationSymbols constructor.
     *
     * @param AnalyzerData $analyzerData
     */
    public function __construct(AnalyzerData $analyzerData)
    {
        $this->analyzerData = $analyzerData;
    }

    /**
     * Analyze text
     *
     * @param Text $text
     *
     * @return AnalysisResult
     */
    public function analyze(Text $text): AnalysisResult
    {
        $matches = [];
        $expression = $this->buildExpression();
        $isSuspicious = (bool) preg_match($expression, $text->getOriginalText(), $matches)
            || (bool) preg_match($expression, $this->cleanText($text->getOriginalText()), $matches);
        $matches = empty($matches) ? [] : [reset($matches)];

        return new AnalysisResult($text, $isSuspicious, $matches, $expression);
    }

    /**
     * @return string
     */
    private function buildExpression(): string
    {
        $patterns = $this->analyzerData->getEmailPatterns();

        return sprintf('/%s/i', implode('|', $patterns));
    }

    /**
     * @param  string $text
     * @return string
     */
    private function cleanText(string $text): string
    {
        return preg_replace('/[\s\W\d]/', '', $text);
    }
}
