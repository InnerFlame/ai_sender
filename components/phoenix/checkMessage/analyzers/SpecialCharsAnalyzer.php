<?php

namespace app\components\phoenix\checkMessage\analyzers;

use app\components\phoenix\checkMessage\AnalysisResult;
use app\components\phoenix\checkMessage\Analyzer;
use app\components\phoenix\checkMessage\AnalyzerData;
use app\components\phoenix\checkMessage\Text;

class SpecialCharsAnalyzer implements Analyzer
{
    const CHARS_COUNT = 1;

    /**
     * @var AnalyzerData
     */
    private $analyzerData;

    /**
     * PunctuationSymbols constructor.
     *
     * @param AnalyzerData $analyzerData
     */
    public function __construct(AnalyzerData $analyzerData)
    {
        $this->analyzerData = $analyzerData;
    }

    /**
     * Analyze text
     *
     * @param Text $text
     *
     * @return AnalysisResult
     */
    public function analyze(Text $text): AnalysisResult
    {
        $suspiciousWords = [];
        $counter = 0;
        foreach ($this->analyzerData->getSpecialChars() as $char) {
            if (strpos($text->getOriginalText(), $char) !== false) {
                $suspiciousWords[] = $char;
                ++$counter;
            }
        }

        $isSuspicious = $counter >= self::CHARS_COUNT;

        return new AnalysisResult($text, $isSuspicious, $suspiciousWords);
    }
}
