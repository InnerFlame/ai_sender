<?php

namespace app\components\phoenix\checkMessage\analyzers;

use app\components\phoenix\checkMessage\AnalysisResult;
use app\components\phoenix\checkMessage\Analyzer;
use app\components\phoenix\checkMessage\AnalyzerData;
use app\components\phoenix\checkMessage\Text;

/**
 * Class DifferentEncodingAnalyzer
 * @package  Phoenix\AntiScamm\Components\SuspiciousWords\Analyzers
 */
class DifferentEncodingAnalyzer implements Analyzer
{
    /**
     * @var AnalyzerData
     */
    private $analyzerData;

    /**
     * PunctuationSymbols constructor.
     *
     * @param AnalyzerData $analyzerData
     */
    public function __construct(AnalyzerData $analyzerData)
    {
        $this->analyzerData = $analyzerData;
    }

    /**
     * Analyze text
     *
     * @param Text $text
     *
     * @return AnalysisResult
     */
    public function analyze(Text $text): AnalysisResult
    {
        $isSuspicious = false;

        if ($text->getWordsCount() > 1) {
            $cleanedText = $this->cleanText($text->getOriginalText());
            $wordsEncoding = $this->detectTextEncoding($cleanedText);
            $isSuspicious = $this->makeDecision($wordsEncoding);
        }

        return new AnalysisResult($text, $isSuspicious);
    }


    /**
     * Make a decision on the basis of words encoding
     *
     * @param array $wordsEncodingRate
     * Array
     * (
     *   [ASCII] => 65
     *   [UTF-8] => 10
     *   [0]     => 25
     * )
     *
     * @return bool
     */
    private function makeDecision(array $wordsEncodingRate): bool
    {
        if (empty($wordsEncodingRate)) {
            return false;
        }

        if (count($wordsEncodingRate) > 1) {
            return true;
        }

        return false;
    }

    /**
     * Returns array of percents
     *
     * @param  string $text
     *
     * @return array
     */
    private function detectTextEncoding(string $text): array
    {
        if (empty($text)) {
            return [];
        }

        $textEncoding = [];
        $textLength = strlen($text);

        for ($iterationCount = 0; $iterationCount < $textLength; $iterationCount++) {
            $symbolToCheck = $text{$iterationCount};
            if (!empty($symbolToCheck)) {
                $encoding = mb_detect_encoding($symbolToCheck);
                $textEncoding[$encoding] = isset($textEncoding[$encoding]) ? $textEncoding[$encoding] + 1 : 1;
            }
        }

        foreach ($textEncoding as &$symbolsCount) {
            $symbolsCount = ceil(($symbolsCount / $textLength) * 100);
        }

        arsort($textEncoding); // Sorting from biggest to smaller percent values

        return $textEncoding;
    }

    /**
     * @param  string $text
     *
     * @return string
     */
    private function cleanText(string $text): string
    {
        $text = trim(preg_replace('/[0-9\r\n\s\x{fe0f}]/u', '', $text));
        $text = str_replace($this->getIgnoredCharsList(), '', $text);

        return $text;
    }

    /**
     * Get ignored chars
     *
     * @return array
     */
    private function getIgnoredCharsList(): array
    {
        return array_merge(
            $this->analyzerData->getPunctuationChars(),
            $this->analyzerData->getSpecialChars(),
            $this->analyzerData->getOtherChars(),
            $this->analyzerData->getUnicodeEmojis()
        );
    }
}
