<?php

namespace app\components\phoenix\checkMessage\analyzers;

use app\components\phoenix\checkMessage\AnalysisResult;
use app\components\phoenix\checkMessage\Analyzer;
use app\components\phoenix\checkMessage\Text;

/**
 * Class TextLengthAnalyzer
 * @package  Phoenix\AntiScamm\Components\SuspiciousWords\Analyzers
 */
class TextLengthAnalyzer implements Analyzer
{
    const MAX_LENGTH = 400;

    /**
     * Analyze text
     *
     * @param Text $text
     *
     * @return AnalysisResult
     */
    public function analyze(Text $text): AnalysisResult
    {
        $isSuspicious = $text->getTextLength() > self::MAX_LENGTH;
        $message      = 'length of message ' . $text->getTextLength();

        return new AnalysisResult($text, $isSuspicious, [$message]);
    }
}
