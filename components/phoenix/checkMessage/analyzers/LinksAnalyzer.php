<?php
namespace app\components\phoenix\checkMessage\analyzers;

use app\components\phoenix\checkMessage\AnalysisResult;
use app\components\phoenix\checkMessage\Analyzer;
use app\components\phoenix\checkMessage\Text;

/**
 * Class LinksAnalyzer
 * @package  Phoenix\AntiScamm\Components\SuspiciousWords\Analyzers
 */
class LinksAnalyzer implements Analyzer
{
    /**
     * Analyze text
     *
     * @param Text $text
     *
     * @return AnalysisResult
     */
    public function analyze(Text $text): AnalysisResult
    {
        $isSuspicious = false;
        $url = '';

        foreach ($text->getWordsList() as $token) {
            $url = filter_var(filter_var($token, FILTER_SANITIZE_URL), FILTER_VALIDATE_URL);
            if ($url !== false) {
                $isSuspicious = true;
                break;
            }
        }

        return new AnalysisResult(
            $text,
            $isSuspicious,
            empty($url) ? [] : [$url]
        );
    }
}
