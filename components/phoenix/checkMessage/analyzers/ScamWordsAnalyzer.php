<?php

namespace app\components\phoenix\checkMessage\analyzers;

use app\components\phoenix\checkMessage\AnalysisResult;
use app\components\phoenix\checkMessage\Analyzer;
use app\components\phoenix\checkMessage\AnalyzerData;
use app\components\phoenix\checkMessage\Text;

class ScamWordsAnalyzer implements Analyzer
{
    /**
     * @var AnalyzerData
     */
    private $analyzerData;

    /**
     * @param AnalyzerData $analyzerData
     */
    public function __construct(AnalyzerData $analyzerData)
    {
        $this->analyzerData = $analyzerData;
    }

    /**
     * Analyze text
     *
     * @param Text $text
     *
     * @return AnalysisResult
     */
    public function analyze(Text $text): AnalysisResult
    {
        $suspiciousWords = [];

        foreach ($this->analyzerData->getScamWords() as $word) {
            if (empty($word[0])) {
                continue;
            }

            $position = stripos($text, $word[0]);

            if ($position !== false) {
                $suspiciousWords[] = $word[0];
            }
        }

        $isSuspicious = !empty($suspiciousWords);

        return new AnalysisResult($text, $isSuspicious, $suspiciousWords);
    }
}
