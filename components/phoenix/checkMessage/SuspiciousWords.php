<?php

namespace app\components\phoenix\checkMessage;

class SuspiciousWords
{
    /**
     * @var AnalyzerFactory
     */
    private $analyzerFactory;

    /**
     * @var array
     */
    private $analyzersList = [
        'PunctuationSymbols',
        'SpecialChars',
        'Email',
        'Messengers',
        'TextLength',
        'PhoneWords',
        'Digits',
        'BlackMessages',
        'ForbiddenLetters',
        'ScamWords',
        'DifferentEncoding',
        'Links',
    ];

    /**
     * SuspiciousWords constructor.
     *
     * @param AnalyzerFactory $analyzerFactory
     */
    public function __construct(AnalyzerFactory $analyzerFactory)
    {
        $this->analyzerFactory  = $analyzerFactory;
    }

    /**
     * Analyze the text
     *
     * @param  Text      $text          - Text to analyze
     * @param  string[]  $analyzersList - List of analyzers
     * @param  string[] $analyzersExludeList - List of analyzers to exclude
     *
     * @return AnalysisResult[]
     */
    public function analyze(Text $text, array $analyzersList = [], array $analyzersExludeList = []): array
    {
        if (empty($analyzersList)) {
            $analyzersList = $this->analyzersList;
        }

        $analyzers = $this->analyzerFactory->getAnalyzersList(array_diff($analyzersList, $analyzersExludeList));
        $result    = [];

        if (!empty($analyzers)) {
            foreach ($analyzers as $name => $analyzer) {
                $result[$name] = $analyzer->analyze($text);
            }
        }

        return $result;
    }
}
