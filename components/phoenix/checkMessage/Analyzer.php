<?php

namespace app\components\phoenix\checkMessage;

interface Analyzer
{
    /**
     * Analyze text
     *
     * @param Text $text
     *
     * @return AnalysisResult
     */
    public function analyze(Text $text): AnalysisResult;
}
