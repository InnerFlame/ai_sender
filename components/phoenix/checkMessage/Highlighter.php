<?php

namespace app\components\phoenix\checkMessage;

class Highlighter
{
    /**
     * @var string
     */
    private $before;

    /**
     * @var string
     */
    private $after;

    /**
     * Hightlighter constructor.
     *
     * @param string $before
     * @param string $after
     */
    public function __construct(string $before, string $after)
    {
        $this->before = $before;
        $this->after = $after;
    }

    /**
     * Highlight text
     * Important: case insensitive
     *
     * @param string $text
     * @param array  $symbols
     *
     * @return string
     */
    public function highlight(string $text, array $symbols): string
    {
        $symbols = array_unique($symbols);
        foreach ($symbols as $symbol) {
            $escapedSymbolForRegexp = str_replace('/', '\/', preg_quote($symbol, '\\'));
            $replacedText = preg_replace(
                '/(' . $escapedSymbolForRegexp . ')/i',
                $this->before . '${1}' . $this->after,
                $text
            );
            $text = $replacedText === null ? $text : $replacedText;
        }

        return $text;
    }
}
