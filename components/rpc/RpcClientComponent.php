<?php

namespace app\components\rpc;

use app\config\Constant;
use app\models\phoenix\PresenceEventLog;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use app\models\rpc\Container;
use app\models\rpc\RpcLog;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class RpcClientComponent extends Component
{
    /** @var string */
    private $rpcConsumeQueue = 'logic';

    /** @var int */
    private $rpcInfoTtl = 360;

    /** @var int */
    public $limitContainersComm;
    /** @var int */
    public $limitContainersRegister;
    /** @var int */
    public $limitContainersObserve;
    /** @var int */
    public $limitContainersSearch;
    /** @var int */
    public $limitContainersRemove;
    /** @var bool */
    public $enableLogs;

    protected function startRpcContainer(SenderProfile $profile, string $target, string $containerType = null)
    {
        $containerName = $target . '.' . $profile['id'];

        $container = Container::findOne(['name' => $containerName, 'target' => $target]);

        if (!empty($container) && $container->status == Container::STATUS_STARTED) {
            return $container;
        }

        static::callContainerInitialisation($containerName, $profile, $target, $containerType);

        return false;
    }

    public function stopRpcContainer(string $containerName, string $target = 'phoenix', string $reason = '')
    {
        $data = new \stdClass();
        $data->id       = $containerName;
        $data->type     = $target;
        $data->reason   = $reason;

        $additional = [
            'target'     => $target,
            'clientName' => $containerName,
        ];

        //send close request
        $this->rpcCall('super', 'killContainer', [$data], $additional);
    }

    /**
     * Call container initialisation on remote side
     *
     * @param string $containerName
     * @param SenderProfile $profile
     * @param string $target
     * @param string|null $type - type of container
     * @return null
     */
    protected function callContainerInitialisation(
        string $containerName,
        SenderProfile $profile,
        string $target,
        string $type = null
    ) {
        //set wait status to prevent new container initialisations for next 30 seconds
        $this->setContainerStatus(
            $containerName,
            ['status' => Container::STATUS_WAIT, 'type' => $type],
            $target,
            $profile
        );

        $data = new \stdClass();
        $data->id = $containerName;
        $data->type = $target;

        $additional = [
            'target'     => $target,
            'clientName' => $containerName,
        ];

        //send initialisation request
        $this->rpcCall('super', 'createContainer', [$data], $additional);
    }

    /**
     * Mark container as started for next hour
     *
     * @param SenderProfile $profile
     * @param string $name
     * @param string $target - target for container
     * @return null
     */
    public function setRpcContainerStarted(SenderProfile $profile, string $name, string $target)
    {
        $this->setContainerStatus($name, ['status' => Container::STATUS_STARTED], $target, $profile);
    }

    /**
     * Mark container as stopped
     *
     * @param string $name
     * @param string $target - target for container
     * @return null
     */
    public function setRpcContainerStopped(string $name, string $target)
    {
        if ($this->setContainerStatus($name, ['status' => Container::STATUS_STOPPED], $target, null, true)) {
            $this->setRpcClientStopped($name, $target);
        }
    }

    /**
     * Delete container and client
     * @param string $name
     * @return null
     */
    public function deleteContainerClient(string $name)
    {
        Container::deleteAll(['name' => $name]);
        Client::deleteAll(['name' => $name]);
    }

    /**
     * Get started container from cache
     *
     * @param SenderProfile $profile - params for client
     * @param string $target - target for container
     * @return string
     */
    public function getRpcContainer(SenderProfile $profile, string $target)
    {
        $containerName = $target . '.' . $profile['id'];

        $container = Container::findOne(['name' => $containerName, 'target' => $target]);

        return $container;
    }

    /**
     * Create record of client in redis
     * Set status wait
     *
     * @param SenderProfile $profile
     * @param $clientType
     * @param $target
     */
    public function setRpcClientWait(SenderProfile $profile, string $clientType, string $target)
    {
        $clientName = $target . '.' . $profile['id'];
        $data       = [
            'status' => Client::STATUS_WAIT,
            'type'   => $clientType,
        ];

        $this->setClientStatus($clientName, $data, $target, $profile);
    }

    /**
     * Mark client as login
     *
     * @param string $name
     * @param string $target
     */
    public function setRpcClientLogin(string $name, string $target)
    {
        $this->setClientStatus($name, ['status' => Client::STATUS_LOGIN], $target);
    }

    /**
     * Mark client as started for next hour
     *
     * @param string $name
     * @param string $target - target for client
     * @return null
     */
    public function setRpcClientStarted(string $name, string $target)
    {
        $this->setClientStatus($name, ['status' => Client::STATUS_STARTED], $target);
    }

    /**
     * Mark client as started for next hour
     *
     * @param string $name
     * @param string $target - target for client
     * @return null
     */
    public function setRpcClientStopped(string $name, string $target)
    {
        $this->setClientStatus($name, ['status' => Client::STATUS_STOPPED], $target, null, true);
    }

    /**
     * Get started client from cache
     *
     * @param SenderProfile $profile - params for client
     * @param string $target - target for client
     * @return Client
     */
    public function getRpcClient(SenderProfile $profile, string $target = 'phoenix')
    {
        $client = Client::findOne(['name' => $target . '.' . $profile['id'], 'target' => $target]);

        return $client;
    }

    /**
     * Make RPC call
     *
     * @param string $client - remote client identifier
     * @param string $method - method to call
     * @param array  $params - params to send
     * @param array $additional - additional information to save in cache
     * @return null
     */
    public function rpcCall(string $client, string $method, array $params, array $additional = [])
    {
        try {
            $correlationId = \Yii::$app->controllersAmqp->publishEvent(
                $client,
                $data = [
                    'method' => $method,
                    'params' => $params,
                ],
                $this->rpcConsumeQueue
            );

            $this->saveRpcCallToCache($correlationId, $data, $additional);

            $this->storeRpcLog([
                'clientName' => $client,
                'direction'  => RpcLog::OUTGOING,
                'method'     => $method,
                'params'     => json_encode($params),
            ]);
        } catch (\PhpAmqpLib\Exception\AMQPProtocolChannelException $e) {
            $this->processingAmqpException($e, $client);
        } catch (\PhpAmqpLib\Exception\AMQPChannelException $e) {
            $this->processingAmqpException($e, $client);
        }
    }

    private function processingAmqpException(\Throwable $e, string $client)
    {
        \Yii::warning($e->getMessage() . PHP_EOL . $e->getTraceAsString());
        if ($client != 'super') {
            list($type, $target, $id) = explode('.', $client);
            $this->stopRpcContainer($target . '.' . $id, $target, $e->getMessage());
        }
    }

    /**
     * Store info about RPC call into cache to correlate with rpc events
     *
     * @param string $correlationId
     * @param array $data - RPC call info
     * @param array $additional - additional information
     * @return null
     */
    protected function saveRpcCallToCache(string $correlationId, array $data, array $additional = [])
    {
        //store info about rpc call into cache for 1 minute to correlate with rpc events
        $data['correlationId'] = $correlationId;
        $data['additional'] = $additional;
        \Yii::$app->cache->set($correlationId, json_encode($data), $this->rpcInfoTtl);
    }

    /**
     * Try to get RPC call info from cache
     * @param string|null $correlationId
     * @return array
     */
    public function getRpcCallInfo(?string $correlationId)
    {
        if (is_null($correlationId)) {
            return [];
        }
        $data = \Yii::$app->cache->get($correlationId);

        if (!empty($data)) {
            return json_decode($data, true);
        }

        return [];
    }

    /**
     * Store RPC requests and events into log
     *
     * @param array $logData
     * @return null
     */
    public function storeRpcLog(array $logData)
    {
        if ($this->enableLogs == false) {
            return;
        }

        $logData['clientName'] = str_replace('client.', '', $logData['clientName']);

        $log = new RpcLog();
        $log->clientName    = $logData['clientName'];
        $log->direction     = $logData['direction'];
        $log->method        = $logData['method'];
        $log->params        = substr($logData['params'], 0, 1500);
        $log->createdAt     = date('Y-m-d H:i:s');
        $log->save();
    }

    /**
     * Store presence events into log
     *
     * @param array $logData
     * @return null
     */
    public function storePresenceEventLog(array $logData)
    {
        if ($this->enableLogs == false) {
            return;
        }

        $log = new PresenceEventLog();
        $log->clientName    = $logData['clientName'];
        $log->action        = $logData['action'];
        $log->createdAt     = date('Y-m-d H:i:s');
        $log->save();
    }

    protected function setClientStatus(
        string $name,
        array $data,
        string $target,
        SenderProfile $profile = null,
        bool $stopped = false
    ) {
        $client = Client::findOne(['name' => $name, 'target' => $target]);

        if ($stopped && empty($client)) {
            return false;
        }

        if (empty($client)) {
            $profileId = $profile['id'] ?? explode('.', $name)[1];
            $sender    = SenderProfile::findOne($profileId);

            $client          = new Client();
            $client->profileId = $sender->id;
            $client->name      = $name;
            $client->target    = $target;
            $client->country   = $sender->country;
            $client->site      = $sender->site;
        }

        $client->status = $data['status'] ?? null;
        $client->timeSetStatus   = $data['time'] ?? time();

        if (isset($data['type'])) {
            $client->type = $data['type'];
        }

        if ($client->save()) {
            return true;
        }

        return false;
    }

    protected function setContainerStatus(
        string $name,
        array $data,
        string $target,
        SenderProfile $profile = null,
        bool $stopped = false
    ) {
        $container = Container::findOne(['name' => $name, 'target' => $target]);

        if ($stopped && empty($container)) {
            return false;
        }

        if (empty($container)) {
            $profileId = $profile['id'] ?? explode('.', $name)[1];
            $sender    = SenderProfile::findOne($profileId);

            $container            = new Container();
            $container->profileId = $sender->id;
            $container->name      = $name;
            $container->target    = $target;
            $container->country   = $sender->country;
            $container->site      = $sender->site;
        }

        $container->status        = $data['status'] ?? null;
        $container->timeSetStatus = time();

        if ($data['status'] != Container::STATUS_STOPPED) {
            $container->lastTimeActivity = time();
        }

        if (isset($data['type'])) {
            $container->type = $data['type'];
        }

        if ($container->save()) {
            return true;
        }

        return false;
    }

    public function isContainersLimitReached(string $type, SenderProfile $profile)
    {
        $limitContainer = 0;
        $filters        = [];

        switch ($type) {
            case Container::TYPE_REGISTRATION:
            case Container::TYPE_RE_UPLOAD_PHOTO:
                $limitContainer = $this->limitContainersRegister;
                break;
            case Container::TYPE_OBSERVE:
                $limitContainer = $this->limitContainersObserve;
                break;
            case Container::TYPE_SEARCH:
                $limitContainer = $this->limitContainersSearch;
                break;
            case Container::TYPE_COMMUNICATION:
                $limitContainer = $this->limitContainersComm;
                break;
            case Container::TYPE_FETCH:
                $limitContainer = $this->limitContainersRemove;
                break;
        }

        $countStarted = Container::getStartedCount($type, $filters);

        if ($countStarted >= $limitContainer) {
            return true;
        }

        return false;
    }

    /**
     * Start container for RPC client on remote side
     *
     * @param SenderProfile  $profile - params for container
     * @param string|null $containerType - type of container for calculation of started containers
     * @return string - client identifier
     */
    public function phoenixStartRpcContainer(SenderProfile $profile, string $containerType = null)
    {
        if (time() > \Yii::$app->cache->get('delayTime')) {
            $ids    = Constant::PHOENIX_GO_IDS;
            $target = Constant::TARGET_PHOENIX;
            if (in_array($profile['id'], $ids)) {
                $target = Constant::TARGET_PHOENIX_GO;
            }
            return $this->startRpcContainer($profile, $target, $containerType);
        }

        return false;
    }

    /**
     * Start container for RPC client on remote side
     *
     * @param SenderProfile  $profile - params for container
     * @param string|null $containerType - type of container for calculation of started containers
     * @return string - client identifier
     */
    public function phoenixStartWebRpcContainer(SenderProfile $profile, string $containerType = null)
    {
        if (time() > \Yii::$app->cache->get('delayTime')) {
            return $this->startRpcContainer($profile, Constant::TARGET_PHOENIX_WEB, $containerType);
        }

        return false;
    }

    /**
     * Refresh expired RPC clients to hold online status
     */
    public function refreshRpcClients()
    {
        $clients = Client::find()
            ->where([
                'target'    => [Constant::TARGET_PHOENIX, Constant::TARGET_PHOENIX_GO],
                'status'    => Client::STATUS_STARTED,
                'type'      => Client::TYPE_COMMUNICATION,
            ])
            ->all();

        $clients = array_filter($clients, function ($client) {
            return $client->refreshingTime < (time() - 1200);
        });
        $clients = ArrayHelper::index($clients, 'profileId');

        $profiles = SenderProfile::findAll(array_keys($clients));

        foreach ($profiles as $profile) {
            $client = Client::findOne(['profileId' => $profile['id']]);
            \Yii::$app->rpcMethods->refreshToken($profile, $clients[$profile['id']]['target']);
        }
    }

    public function updateClientRefreshingTime(string $name, string $target)
    {
        $client = Client::findOne(['name' => $name, 'target' => $target]);

        if ($client) {
            $client->refreshingTime = time();
            $client->save();
        }
    }

    public function isClientFrozen(SenderProfile $profile, string $target)
    {
        \Yii::$app->redis->zremrangebyscore('frozenClients', 0, time() - 3600);

        if (\Yii::$app->redis->zscore('frozenClients', $target . '.' . $profile->id)) {
            return true;
        }

        return false;
    }

    public function freezeClient(string $clientName)
    {
        \Yii::$app->redis->zadd('frozenClients', time(), $clientName);
    }
}
