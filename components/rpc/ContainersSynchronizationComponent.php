<?php

namespace app\components\rpc;

use yii\base\Component;
use yii\helpers\ArrayHelper;
use app\models\rpc\Container;

class ContainersSynchronizationComponent extends Component
{
    private static $timeEnd;

    private $keyStartedContainersFromOutside = 'startedContainersFromOutside';

    public function processContainersSynchronization()
    {
        \Yii::$app->presenceAmqp->consumePresenceEventsForSynchronizer([$this, 'containersHandler']);

        $this->stopContainers();
    }

    public function containersHandler($message, $channel)
    {
        if (empty(self::$timeEnd)) {
            self::$timeEnd = time() + 10;
        }

        if ($message['action'] == 'bind') {
            \Yii::$app->redis->executeCommand('RPUSH', [$this->keyStartedContainersFromOutside, $message['key']]);
        }

        if (time() >= self::$timeEnd) {
            $channel->callbacks = [];
            self::$timeEnd  = null;
        }

        return true;
    }

    private function stopContainers()
    {
        $mustBeStartedCommunications  = ArrayHelper::getColumn(\Yii::$container->get('communicationContainerManager')->getAllStartedClients(), 'name', false);
        $mustBeStartedOtherTypes      = $this->getStartedOtherTypes();
        $mustBeStartedContainers      = array_merge($mustBeStartedCommunications, $mustBeStartedOtherTypes);
        $startedContainersFromOutside = \Yii::$app->redis->executeCommand('LRANGE', [$this->keyStartedContainersFromOutside, '0', '-1']);

        foreach ($startedContainersFromOutside as $item) {
            if (!in_array($item, $mustBeStartedContainers)) {
                list($target, $id) = explode('.', $item);
                \Yii::$app->rpcClient->stopRpcContainer($item, $target);
            }
        }

        $shouldNotBeStartedContainers = Container::find()
            ->where(['status' => Container::STATUS_STARTED])
            ->andWhere(['in', 'type', [Container::TYPE_COMMUNICATION, Container::TYPE_SEARCH]])
            ->andWhere(['not in', 'name', $mustBeStartedContainers])
            ->all();

        foreach ($shouldNotBeStartedContainers as $item) {
            \Yii::$app->rpcClient->deleteContainerClient($item['name']);
        }

        \Yii::$app->redis->executeCommand('DEL',  [$this->keyStartedContainersFromOutside]);
    }

    private function getStartedOtherTypes()
    {
        $containers = Container::find()
            ->where(['status' => [Container::STATUS_STARTED, Container::STATUS_WAIT]])
            ->andWhere(['not in','type', Container::TYPE_COMMUNICATION])
            ->all();

        return ArrayHelper::getColumn($containers, 'name', false);
    }
}