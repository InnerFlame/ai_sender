<?php

namespace app\components\rpc;

use app\config\Constant;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Container;

use yii\base\Component;

/**
 * Component to process remote RPC client`s statuses
 */
class PresenceEventsProcessorComponent extends Component
{
    public function processEvents()
    {
        \Yii::$app->presenceAmqp->consumePresenceEvents([$this, 'messageHandler']);
    }

    public function messageHandler(array $incomingEventData)
    {
        \Yii::$app->rpcClient->storePresenceEventLog([
            'clientName'    => 'container.' . $incomingEventData['key'],
            'action'        => $incomingEventData['action'],
        ]);

        list($target, $profileId) = explode('.', $incomingEventData['key']);

        switch ($incomingEventData['action']) {
            case 'bind':
                $this->setContainerStarted($incomingEventData, $target, $profileId);
                break;
            case 'unbind':
                $this->setContainerStopped($incomingEventData, $target, $profileId);
                break;
        }

        return true;
    }

    private function setContainerStarted(array $incomingEventData, string $target, int $profileId)
    {
        $profile = SenderProfile::findOne($profileId);

        if (empty($profile)) {
            return  false;
        }

        \Yii::$app->rpcClient->setRpcContainerStarted($profile, $incomingEventData['key'], $target);

        $container = Container::findOne([
            'name'   => $incomingEventData['key'],
            'target' => $target,
        ]);

        if (empty($container)) {
            return true;
        }

        if (empty($container->type)) {
            \Yii::$app->rpcClient->stopRpcContainer($container->target . '.' . $container->profileId);
            return true;
        }

        $profile = SenderProfile::findOne($container->profileId);

        if (empty($profile)) {
            return true;
        }

        switch ($container->type) {
            case Container::TYPE_SEARCH:
                if ($container->target == Constant::TARGET_PHOENIX) {
                    \Yii::$app->apiSearch->loginClient($profile);
                } else {
                    \Yii::$app->searcher->loginClient($profile);
                }
                break;
            case Container::TYPE_COMMUNICATION:
                \Yii::$container->get('commProcessor')->loginClient($profile, $target);
                break;
            case Container::TYPE_RE_UPLOAD_PHOTO:
                \Yii::$app->reUploadProcessor->loginClient($profile);
                break;
            case Container::TYPE_FETCH:
                \Yii::$container->get('fetchProcessor')->loginClient($profile);
                break;
            default:
                return true;
        }

        return true;
    }

    private function setContainerStopped(array $incomingEventData, string $target, int $profileId)
    {
        SenderProfile::setStatusOffline($profileId);

        $container = Container::findOne([
            'name'   => $incomingEventData['key'],
            'target' => $target,
        ]);

        if (empty($container)) {
            return false;
        }

        \Yii::$app->rpcClient->deleteContainerClient($incomingEventData['key']);

        return true;
    }
}
