<?php

namespace app\components\rpc;

use app\config\Constant;
use app\models\phoenix\SenderProfile;
use app\models\rpc\Client;
use app\models\rpc\Container;
use yii\base\Component;
use yii\db\ActiveRecord;

class RpcMethodsComponent extends Component
{
    /**
     * Login client for RPC on remote side
     *
     * @param ActiveRecord $profile
     * @param $clientType
     * @param $target
     * @return bool|null|static
     */
    public function loginRpcClient(ActiveRecord $profile, $clientType, $target = 'phoenix')
    {
        $rpcContainer = \Yii::$app->rpcClient->getRpcContainer($profile, $target);
        $rpcClient    = \Yii::$app->rpcClient->getRpcClient($profile, $target);

        //can`t create client if no container or it`s status still not defined
        if (empty ($rpcContainer) || $rpcContainer->status != Container::STATUS_STARTED) {
            if (!empty($rpcClient)) {
                $rpcClient->delete();
            }
            
            return false;
        }

        if (empty($rpcClient) || $rpcClient->status != Client::STATUS_STARTED) {
            //create client only if it is not existed
            if (time() > \Yii::$app->cache->get('delayTime')) {
                $clientData = $this->getClientData($profile, $target, $clientType);
                $client     = Client::findOne(['name' => $clientData['id'], 'target' => $target]);

                if (empty($client)) {
                    $this->callClientInitialisation($profile, $clientData, $target, $clientType);
                    return false;
                }

                if ($client->status == Client::STATUS_STARTED) {
                    //client has been completely initialized
                    return $client;
                }

                if ($client->status == Client::STATUS_STOPPED || $this->isWaitingTimeHasPassed($client)) {
                    //initialisation request may be already sent, still no response with status. Don`t need to send another one
                    $this->callClientInitialisation($profile, $clientData, $target, $clientType);
                }
            }
        }

        return false;
    }

    /**
     * Creation container
     * @param ActiveRecord $profile
     * @param $containerType
     * @param $target
     * @return bool
     */
    public function createRpcContainer(ActiveRecord $profile, $containerType, $target = 'phoenix')
    {
        if (\Yii::$app->rpcClient->isContainersLimitReached($containerType, $profile)) {
            return false;
        }

        if (\Yii::$app->rpcClient->isClientFrozen($profile, $target)) {
            return false;
        }

        $rpcContainer = \Yii::$app->rpcClient->getRpcContainer($profile, $target);

        if (empty($rpcContainer) || $this->isWaitingTimeHasPassed($rpcContainer) || $rpcContainer->status == Container::STATUS_STOPPED) {
            switch ($target) {
                case Constant::TARGET_PHOENIX:
                case Constant::TARGET_PHOENIX_GO:
                    \Yii::$app->rpcClient->phoenixStartRpcContainer($profile, $containerType);
                    break;
                case Constant::TARGET_PHOENIX_WEB:
                    \Yii::$app->rpcClient->phoenixStartWebRpcContainer($profile, $containerType);
                    break;
            }
        }

        return true;
    }

    /**
     * Refresh profile state on remote side
     * @param $profile
     * @param $target
     */
    public function refreshToken(SenderProfile $profile, $target)
    {
        $data = [
            'key' => $profile->refreshToken,
        ];

        $additional = [
            'target'     => $target,
            'profileId'  => $profile->id,
            'clientName' => $target . '.' . $profile->id,
        ];

        \Yii::$app->rpcClient->rpcCall($this->getClientName($profile, $target), 'refreshToken', [$data], $additional);
        \Yii::$app->rpcClient->updateClientRefreshingTime($target . '.' . $profile->id, $target);
    }

    /**
     * Fetch some profile data
     * @param $profile
     * @param $target
     */
    public function fetchData(SenderProfile $profile, $target)
    {
        $data = [
            'id'            => '',
            'user_status'   => '',
        ];

        $additional = [
            'target'     => $target,
            'profileId'  => $profile->id,
            'clientName' => $target . '.' . $profile->id,
        ];

        \Yii::$app->rpcClient->rpcCall($this->getClientName($profile, $target), 'fetch', [$data], $additional);
    }

    /**
     * Get client name by profile and target
     * @param $profile
     * @param $target
     */
    private function getClientName(SenderProfile $profile, $target)
    {
        return 'client.' . $target . '.' . $profile->id;
    }

    /**
     * @param $rpc
     * @return bool
     */
    private function isWaitingTimeHasPassed($rpc)
    {
        return $rpc->status == Container::STATUS_WAIT && (time() - $rpc->timeSetStatus) >= 30;
    }

    /**
     * Call client initialisation on remote side
     * @param $profile
     * @param $clientData
     * @param $target
     * @param $clientType
     */
    protected function callClientInitialisation($profile, $clientData, $target, $clientType)
    {
        $loginCount = \Yii::$app->redis->get('clientLoginCount.' . $profile->id);

        switch ($target) {
            case Constant::TARGET_PHOENIX:
                $loginLimit = 49;
                break;
            case Constant::TARGET_PHOENIX_WEB:
                $loginLimit = 35;
                break;
            default:
                $loginLimit = 49;
        }

        if ($loginCount >= $loginLimit) {
            \Yii::warning(date('Y-m-d H:i:s') . 'Login limit reached ' . $target . ' ' . $profile->id);
            return false;
        }

        //set wait status to prevent new client initialisations for next 10 seconds
        \Yii::$app->rpcClient->setRpcClientWait($profile, $clientType, $target);

        $additional = [
            'target'     => $target,
            'profileId'  => $profile->id,
            'clientName' => $clientData['id'],
            'clientType' => $clientType,
        ];

        \Yii::$app->rpcClient->rpcCall($this->getClientName($profile, $target), 'login', [$clientData], $additional);
    }

    /**
     * Client data for send to login
     *
     * @param $profile
     * @param $target
     * @return array
     */
    protected function getClientData(SenderProfile $profile, $target, $clientType)
    {
        $yaml          = \Yii::$app->yaml;
        $language      = $profile->language;
        $country       = $profile->country;

        switch ($target) {
            case Constant::TARGET_PHOENIX:
            case Constant::TARGET_PHOENIX_GO:
                $userAgent     = $yaml->parseByKey('userAgentMob', $profile->userAgent);
                $userAgent     = str_replace(['$(language)', '$(country)'], [$language, $country], $userAgent);
                break;
            case Constant::TARGET_PHOENIX_WEB:
                $userAgent     = $yaml->parseByKey('userAgentWeb', $profile->userAgent);
                break;
        }

        $data = [
            'id'                       => $target . '.' . $profile->id,
            'host'                     => $yaml->parseByKey('site', $profile->site)['name'],
            'email'                    => $profile->email,
            'password'                 => $profile->password,
            'birthDate'                => $profile->birthday,
            'deviceIdHex'              => $profile->deviceIdHex,
            'internalDeviceIdentifier' => $profile->internalDeviceIdentifier,
            'gtmClientId'              => $profile->gtmClientId,
            'location'                 => $profile->location,
            'userAgent'                => $userAgent,
            'locale'                   => [
                'language' => $language,
                'country'  => $country,
            ],
            'app'                      => [
                'bundle'  => $yaml->parseByKey('bundles', $profile->site),
                'version' => $yaml->parseByKey('versions', $profile->version),
                'marker'  => $profile->marker,
            ],
            'proxy'                    => \Yii::$app->proxyManager->getProxy($profile, Constant::TARGET_PHOENIX, $clientType),
            'type'                     => $target,
        ];


        if (!empty($profile->ip) && filter_var($profile->ip, FILTER_VALIDATE_IP)) {
            $data['ip'] = $profile->ip;
        }

        return $data;
    }
}