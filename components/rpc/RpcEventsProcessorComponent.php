<?php

namespace app\components\rpc;

use yii\base\Component;

/**
 * Component to process RPC events
 */
class RpcEventsProcessorComponent extends Component
{
    /** @var string */
    private $rpcConsumeQueue = 'logic';

    public function processEvents()
    {
        \Yii::$app->controllersAmqp->consumeEvents($this->rpcConsumeQueue, [$this, 'messageHandler']);
    }

    public function messageHandler(array $message, ?string $correlationId)
    {
        $rpcCallInfo  = \Yii::$app->rpcClient->getRpcCallInfo($correlationId);

        if (empty($message)) {
            return false;
        }

        $clientName = null;

        if (!empty($message['method'])) {
            if (in_array($message['method'], ['banned', 'no_photo']) && !empty($message['profileId'])) {
                $clientName = $message['profileId'];
            } else {
                $clientName = $message['params'][0]['id'];
            }
        } elseif (isset($rpcCallInfo['additional']['clientName'])) {
            $clientName =  $rpcCallInfo['additional']['clientName'];
        }

        if (empty($clientName)) {
            return false;
        }

        \Yii::$app->phoenixRpcEventsProcessor->processEvent($message, $rpcCallInfo);

        return false;
    }
}
