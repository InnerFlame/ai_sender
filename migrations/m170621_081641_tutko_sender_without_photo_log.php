<?php

use yii\db\Migration;
use app\models\phoenix\SenderWithoutPhotoLog;

class m170621_081641_tutko_sender_without_photo_log extends Migration
{
    public function up()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `' . SenderWithoutPhotoLog::tableName() . '` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `senderId` INT(11) NOT NULL,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `senderId` (`senderId`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
    }

    public function down()
    {
        $this->execute('DROP TABLE IF EXISTS `' . SenderWithoutPhotoLog::tableName() . '`');
    }
}
