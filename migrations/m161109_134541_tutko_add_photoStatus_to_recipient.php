<?php

use yii\db\Migration;
use app\models\phoenix\RecipientProfile;

class m161109_134541_tutko_add_photoStatus_to_recipient extends Migration
{
    public function up()
    {
        $this->addColumn(RecipientProfile::tableName(), 'isWithPhoto', $this->boolean()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(RecipientProfile::tableName(), 'isWithPhoto');
    }
}
