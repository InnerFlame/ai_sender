<?php

use yii\db\Migration;

class m171004_135128_martchenko_add_splits_to_scheme extends Migration
{
    public function safeUp()
    {
        $this->addColumn('schema', 'splitName', $this->string()->defaultValue(null));
        $this->addColumn('schema', 'splitGroup', $this->integer()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('schema', 'splitName');
        $this->dropColumn('schema', 'splitGroup');
    }
}
