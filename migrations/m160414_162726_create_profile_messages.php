<?php

use yii\db\Migration;
use yii\db\Schema;

class m160414_162726_create_profile_messages extends Migration
{
    public function up()
    {
        $this->createTable('profileMessages', [
            'id' => $this->primaryKey(),
            'subject' => Schema::TYPE_STRING,
            'body' => Schema::TYPE_TEXT,
            'language' => Schema::TYPE_STRING,
            'country' => Schema::TYPE_STRING,
            'gender' => Schema::TYPE_STRING,
            'sexuality' => Schema::TYPE_STRING,
            'paid' => Schema::TYPE_BOOLEAN,
        ]);
    }

    public function down()
    {
        $this->dropTable('profileMessages');
    }
}
