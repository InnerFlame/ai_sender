<?php

use app\models\phoenix\IncorrectMessage;
use yii\db\Migration;

class m170707_115655_kuleshov_content_tool_language extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            IncorrectMessage::tableName(),
            'lang',
            $this->string(3)->null()
        );
    }

    public function safeDown()
    {
        $this->dropColumn(
            IncorrectMessage::tableName(),
            'lang'
        );
    }
}
