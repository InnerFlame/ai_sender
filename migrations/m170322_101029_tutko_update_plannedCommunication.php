<?php

use yii\db\Migration;
use app\models\phoenix\PlannedCommunication;

class m170322_101029_tutko_update_plannedCommunication extends Migration
{
    public function up()
    {
        $this->addColumn(PlannedCommunication::tableName(), 'countSent', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(PlannedCommunication::tableName(), 'countSent');
    }
}
