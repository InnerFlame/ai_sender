<?php

use yii\db\Migration;
use app\models\phoenix\Group;

class m161129_133335_tutko_add_flag_isInitioal_to_group extends Migration
{
    public function up()
    {
        $this->addColumn(Group::tableName(), 'isInitial', $this->boolean()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(Group::tableName(), 'isInitial');
    }

}
