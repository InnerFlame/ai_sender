<?php

use yii\db\Migration;
use app\models\phoenix\CommunicationContainer;

class m170426_081248_tutko_delete_site_from_communication_container extends Migration
{
    public function up()
    {
        $this->dropColumn(CommunicationContainer::tableName(), 'site');
    }

    public function down()
    {
        $this->addColumn(CommunicationContainer::tableName(), 'site', $this->integer()->defaultValue(1));
    }

}
