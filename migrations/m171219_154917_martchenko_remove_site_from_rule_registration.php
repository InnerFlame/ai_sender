<?php

use yii\db\Migration;

class m171219_154917_martchenko_remove_site_from_rule_registration extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('rulesRegistration', 'site');
    }

    public function safeDown()
    {
        $this->addColumn('rulesRegistration', 'site', $this->integer()->notNull());
    }
}
