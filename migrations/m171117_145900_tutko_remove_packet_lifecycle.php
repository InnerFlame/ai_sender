<?php

use yii\db\Migration;

class m171117_145900_tutko_remove_packet_lifecycle extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `checkBanScheduler` DROP COLUMN `state`');
        $this->execute('ALTER TABLE `communicationContainer` DROP COLUMN `lifeCycle`');
        $this->execute("DROP TABLE IF EXISTS `launchedContainersInfo`");

    }

    public function safeDown()
    {
        $this->execute('ALTER TABLE `checkBanScheduler` ADD COLUMN `state` VARCHAR (20)');
        $this->execute('ALTER TABLE `communicationContainer` ADD COLUMN `lifeCycle` VARCHAR (20) DEFAULT "flexible"');
        $this->execute("
            CREATE TABLE IF NOT EXISTS `launchedContainersInfo`(
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `country` varchar(3) DEFAULT NULL,
                `data` text,
                PRIMARY KEY (`id`),
                KEY `country` (`country`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
    }
}
