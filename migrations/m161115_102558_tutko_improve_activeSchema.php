<?php

use yii\db\Migration;
use app\models\phoenix\RecipientSchema;

class m161115_102558_tutko_improve_activeSchema extends Migration
{
    public function up()
    {
        $this->renameColumn(RecipientSchema::tableName(), 'createdAt', 'restartedAt');
    }

    public function down()
    {
        $this->renameColumn(RecipientSchema::tableName(), 'restartedAt', 'createdAt');
    }
}
