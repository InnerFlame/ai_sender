<?php

use yii\db\Migration;

class m171107_164534_tutko_create_senderRemoveLog_table extends Migration
{
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `senderRemoveLog` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `removedSenderId` INT(11) NOT NULL,
                `createdSenderId` INT(11) NOT NULL,
                `timeToRemove` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `removedAt` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `timeToCheck` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `checkedAt` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `status` TEXT,
                `niche` VARCHAR (20),
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `removedSenderId` (`removedSenderId`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        ');
    }

    public function safeDown()
    {
        $this->execute('DROP TABLE IF EXISTS `senderRemoveLog`');
    }

}
