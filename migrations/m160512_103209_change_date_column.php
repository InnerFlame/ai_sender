<?php

use sender\registration\models\RulesRegistration;
use app\models\phoenix\Group;
use yii\db\Migration;


class m160512_103209_change_date_column extends Migration
{
    public function up()
    {
        $this->renameColumn(RulesRegistration::tableName(), 'startTimeOffset', 'dateFrom');
        $this->renameColumn(RulesRegistration::tableName(), 'endTimeOffset', 'dateTo');

        $this->renameColumn(Group::tableName(), 'dateStart', 'startTimeOffset');
        $this->renameColumn(Group::tableName(), 'dateEnd', 'endTimeOffset');
    }

    public function down()
    {
        $this->renameColumn(RulesRegistration::tableName(), 'dateFrom', 'startTimeOffset');
        $this->renameColumn(RulesRegistration::tableName(), 'dateTo', 'endTimeOffset');

        $this->renameColumn(Group::tableName(), 'startTimeOffset', 'dateStart');
        $this->renameColumn(Group::tableName(), 'endTimeOffset', 'dateEnd');
    }
}
