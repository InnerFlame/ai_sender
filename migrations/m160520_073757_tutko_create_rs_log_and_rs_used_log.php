<?php

use yii\db\Migration;
use app\models\phoenix\ {
    ProcessedActivities,
    ActivityScheduler
};

class m160520_073757_tutko_create_rs_log_and_rs_used_log extends Migration
{
    public function up()
    {
        $this->createTable(ActivityScheduler::tableName(), [
            'id'           => $this->primaryKey(),
            'recipientId'  => $this->integer(),
            'senderId'     => $this->integer(),
            'activityType' => $this->integer(),
            'createdAt'    => $this->timestamp(),
            'timeToSend'   => $this->timestamp()->defaultValue(false),
            'sentAt'       => $this->timestamp()->defaultValue(false),
        ]);

        $this->createTable(ProcessedActivities::tableName(), [
            'schemaId'    => $this->integer(),
            'recipientId' => $this->integer(),
            'groupId'     => $this->integer(),
            'activityId'  => $this->integer(),
        ]);

        $this->createIndex('schemaRecipientActivity', ProcessedActivities::tableName(), 'schemaId, recipientId, groupId, activityId', true);
    }

    public function down()
    {
        $this->dropTable(ActivityScheduler::tableName());

        $this->dropTable(ProcessedActivities::tableName());
    }
}
