<?php

use yii\db\Migration;
use app\models\common\User;

class m160413_153247_create_user_table extends Migration
{
    public function up()
    {
        $this->createTable(User::tableName(), [
            'id' => $this->primaryKey(),
            'email' => $this->string(40)->notNull(),
            'password' => $this->string(40)->notNull(),
            'authKey' => $this->string(40),
            'accessToken' => $this->string(40),
            'role' => 'ENUM ("' . User::SUPER_ADMIN . '", "' . User::ADMIN . '") NOT NULL DEFAULT "' . User::ADMIN . '" '
        ]);
    }

    public function down()
    {
        $this->dropTable(User::tableName());
    }
}
