<?php

use yii\db\Migration;
use app\models\phoenix\ProfileMessage;
use yii\db\Schema;

class m160427_132144_messageProfile_update extends Migration
{
    public function up()
    {
        $this->dropTable('profileMessages');
        $this->createTable(ProfileMessage::tableName(),
            [
                'id'        => $this->primaryKey(),
                'subject'   => $this->string(255)->defaultValue(''),
                'body'      => $this->text()->notNull(),
                'language'  => $this->string(5),
                'country'   => $this->string(2)->notNull(),
                'gender'    => $this->integer()->notNull(),
                'sexuality' => $this->integer()->notNull(),
                'sendMode'  => $this->integer(1),
                'project'   => $this->integer()->notNull(),
                'isPaid'      => $this->boolean()->defaultValue(false),
            ]
            );

        $this->createIndex('language', ProfileMessage::tableName(), 'language');
        $this->createIndex('country', ProfileMessage::tableName(), 'country');
        $this->createIndex('gender', ProfileMessage::tableName(), 'gender');
        $this->createIndex('sexuality', ProfileMessage::tableName(), 'sexuality');
    }

    public function down()
    {
        $this->createTable('profileMessages', [
            'id'        => $this->primaryKey(),
            'subject'   => Schema::TYPE_STRING,
            'body'      => Schema::TYPE_TEXT,
            'language'  => Schema::TYPE_STRING,
            'country'   => Schema::TYPE_STRING,
            'gender'    => Schema::TYPE_STRING,
            'sexuality' => Schema::TYPE_STRING,
            'paid'      => Schema::TYPE_BOOLEAN,
        ]);

       $this->dropTable(ProfileMessage::tableName());
    }
}
