<?php

use app\models\phoenix\IncorrectMessage;
use yii\db\Migration;

class m170613_125915_incorrect_messages extends Migration
{
    public function up()
    {
        $this->createTable(
            IncorrectMessage::tableName(),
            [
                'id' => $this->primaryKey(),
                'message' => $this->text()->notNull(),
                'type' => "ENUM('phrase','template','initial') NOT NULL",
                'timestamp' => $this->timestamp()->notNull()->defaultExpression('now()'),
            ]);
        $this->createIndex(
            IncorrectMessage::tableName() . '_type_idx',
            IncorrectMessage::tableName(),
            'type'
        );
    }

    public function down()
    {
        $this->dropIndex(IncorrectMessage::tableName() . '_type_idx', IncorrectMessage::tableName());
        $this->dropTable(IncorrectMessage::tableName());
    }
}
