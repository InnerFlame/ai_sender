<?php

use yii\db\Migration;

class m161206_125910_tutko_create_unprocessedRecipient_table extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `unprocessedRecipient` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `countGrab` INT(11) NOT NULL,
                `countNotSent` INT(11) NOT NULL,
                `grabbedAt` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `grabbedAt` (`grabbedAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `unprocessedRecipient`");
    }
}
