<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;

class m160727_084615_tutko_create_monitoring_photo_table extends Migration
{
    const TABLE_PHOTO_MONITOR_SCHEDULER = 'photoMonitoringScheduler';

    public function up()
    {
        $this->execute("
            CREATE TABLE  IF NOT EXISTS `" . self::TABLE_PHOTO_MONITOR_SCHEDULER . "` (
              `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
              `senderId` INT(11) NOT NULL,
              `observerId` INT(11) NOT NULL,
              `timeToMonitoring` TIMESTAMP  NOT NULL DEFAULT '0000-00-00 00:00:00',
              `startMonitoring`  TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
              `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`)
            );
        ");

        $this->createIndex('photoStatus', SenderProfile::tableName(), 'photoStatus');

        $this->alterColumn(SenderProfile::tableName(), 'photoStatus', $this->string(10));
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `" . self::TABLE_PHOTO_MONITOR_SCHEDULER . "`");

        $this->dropIndex('photoStatus', SenderProfile::tableName());

        $this->alterColumn(SenderProfile::tableName(), 'photoStatus', $this->string(8));
    }
}
