<?php

use yii\db\Migration;

class m160705_130346_tutko_change_struct_sender extends Migration
{
    public function up()
    {
        $this->dropColumn(\app\models\phoenix\SenderProfileRegister::tableName(), 'isActive');
    }

    public function down()
    {
        $this->addColumn(\app\models\phoenix\SenderProfileRegister::tableName(), 'isActive', $this->boolean());
    }

}
