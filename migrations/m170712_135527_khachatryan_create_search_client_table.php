<?php

use yii\db\Migration;

/**
 * Class m170712_135527_khachatryan_create_search_client_table
 */
class m170712_135527_khachatryan_create_search_client_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('searchClient', [
            'id' => $this->primaryKey(),
            'profileId' => $this->integer(),
            'ruleId' => $this->integer(),
            'location' => $this->string('20'),
            'createdAt' => $this->timestamp()
        ]);

        $this->createIndex('profileId', 'searchClient', 'profileId');
        $this->createIndex('createdAt', 'searchClient', 'createdAt');
    }

    public function safeDown()
    {
        $this->dropTable('searchClient');
    }
}
