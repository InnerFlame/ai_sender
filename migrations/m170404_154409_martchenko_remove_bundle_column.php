<?php

use yii\db\Migration;
use \app\models\phoenix\SenderProfile;

class m170404_154409_martchenko_remove_bundle_column extends Migration
{
    public function up()
    {
        $this->dropColumn(SenderProfile::tableName(), 'bundle');
    }

    public function down()
    {
        $this->addColumn(SenderProfile::tableName(), 'bundle', $this->integer());
    }
}
