<?php

use yii\db\Migration;
use sender\communication\models\CommunicationSchedule;

class m160530_101500_tutko_change_column_communicationSchedule extends Migration
{
    public function up()
    {
        $this->renameColumn(CommunicationSchedule::tableName(), 'sendedAt', 'sentAt');
    }

    public function down()
    {
        $this->renameColumn(CommunicationSchedule::tableName(), 'sentAt', 'sendedAt');
    }

}
