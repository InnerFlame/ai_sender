<?php

use yii\db\Migration;
use app\models\phoenix\CommunicationContainer;

class m170406_075313_tutko_add_logic_type_to_comm_containers extends Migration
{
    public function up()
    {
        $this->addColumn(CommunicationContainer::tableName(), 'lifeCycle', $this->string(15)->defaultValue('packet'));

        $this->execute('
            CREATE TABLE IF NOT EXISTS `lifeCycleLog` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `senderId` INT(11) NOT NULL,
                `extra` text,
                `addedToInitialAt` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `addedToAnswerAt` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `deletedFromPullAt` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `senderId` (`senderId`)
            )
        ');
    }

    public function down()
    {
        $this->dropColumn(CommunicationContainer::tableName(), 'lifeCycle');

        $this->execute('DROP TABLE IF EXISTS `lifeCycleLog`');
    }
}
