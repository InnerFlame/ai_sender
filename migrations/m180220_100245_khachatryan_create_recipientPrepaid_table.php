<?php

use yii\db\Migration;

class m180220_100245_khachatryan_create_recipientPrepaid_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('prepaidRecipients', [
            'recipientId' => $this->primaryKey()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('prepaidRecipients');
    }
}
