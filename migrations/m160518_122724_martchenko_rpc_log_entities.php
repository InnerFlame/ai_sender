<?php

use yii\db\Migration;

class m160518_122724_martchenko_rpc_log_entities extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `rpcLog` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `clientName` varchar(32) DEFAULT NULL,
                `direction` enum('incoming', 'outgoing') NOT NULL,
                `method` varchar(32) NOT NULL,
                `params` text DEFAULT NULL,
                `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                 PRIMARY KEY (`id`),
                 KEY `clientName` (`clientName`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table to store rpc log by clients'
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `presenceEventLog` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `clientName` varchar(32) DEFAULT NULL,
                `action` varchar(32) NOT NULL,
                `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                 PRIMARY KEY (`id`),
                 KEY `clientName` (`clientName`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table to store presence events log by clients'
        ");
    }

    public function down()
    {
       $this->execute("DROP TABLE IF EXISTS `rpcLog`");
       $this->execute("DROP TABLE IF EXISTS `presenceEventLog`");
    }
}
