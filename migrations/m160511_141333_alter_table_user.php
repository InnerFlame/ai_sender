<?php

use yii\db\Migration;
use app\models\common\User;

class m160511_141333_alter_table_user extends Migration
{
    public function up()
    {
        $this->addColumn(User::tableName(), 'createdAt', $this->timestamp());
    }

    public function down()
    {
        $this->dropColumn(User::tableName(), 'createdAt');
    }

}
