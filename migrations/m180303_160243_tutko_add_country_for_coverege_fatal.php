<?php

use yii\db\Migration;
use app\models\phoenix\UnprocessedRecipient;

/**
 * Class m180303_160243_tutko_add_country_for_coverege_fatal
 */
class m180303_160243_tutko_add_country_for_coverege_fatal extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('TRUNCATE TABLE `' . UnprocessedRecipient::tableName() . '`');
        $this->execute('
            ALTER TABLE `' . UnprocessedRecipient::tableName() . '`
            ADD COLUMN `country` VARCHAR(3) NOT NULL
        ');
        $this->execute('
            ALTER TABLE `' . UnprocessedRecipient::tableName() . '`
            CHANGE COLUMN `grabbedAt` `grabbedAt` DATE NOT NULL
        ');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute('TRUNCATE TABLE `' . UnprocessedRecipient::tableName() . '`');
        $this->execute('
            ALTER TABLE `' . UnprocessedRecipient::tableName() . '`
            DROP COLUMN `country`
        ');
        $this->execute('
            ALTER TABLE `' . UnprocessedRecipient::tableName() . '`
            CHANGE COLUMN `grabbedAt`
            `grabbedAt`  TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00"
        ');
    }
}
