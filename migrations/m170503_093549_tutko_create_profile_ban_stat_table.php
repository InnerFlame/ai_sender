<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderBanStat;

class m170503_093549_tutko_create_profile_ban_stat_table extends Migration
{
    public function up()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `' . SenderBanStat::tableName() . '` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `countBanned` INT(11) NOT NULL,
                `country` varchar(5) NOT NULL,
                `bannedAt` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `countryBannedAt` (`country`,`bannedAt`),
                KEY `bannedAt` (`bannedAt`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ');

        $this->execute('ALTER TABLE `' . SenderProfile::tableName() . '` CHANGE `createdAt` `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;');
    }

    public function down()
    {
        $this->execute('DROP TABLE IF EXISTS `' . SenderBanStat::tableName() . '`');
    }
}
