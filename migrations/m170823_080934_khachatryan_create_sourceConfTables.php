<?php

use yii\db\Migration;
use yii\db\Expression;
use app\models\phoenix\SendExclusionsRules;
use app\models\phoenix\UserExclusionsRules;

class m170823_080934_khachatryan_create_sourceConfTables extends Migration
{
    public function safeUp()
    {
        $this->createTable(SendExclusionsRules::tableName(), [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()->notNull(),
            'regDateFrom' => $this->date()->defaultValue(NULL),
            'regDateTo'   => $this->date()->defaultValue(NULL),
            'countries'   => $this->text()->defaultValue(NULL),
            'sitesIds'    => $this->text()->defaultValue(NULL),
            'sources'     => $this->text()->defaultValue(NULL),
            'isActive'    => $this->boolean()->notNull()->defaultValue(false),
            'updatedAt'   => $this->timestamp()->defaultValue(NULL),
            'createdAt'   => $this->timestamp()->defaultValue(new Expression('NOW()')),
        ]);

        $this->createIndex('createdAt', SendExclusionsRules::tableName(), 'createdAt');

        $this->createTable(UserExclusionsRules::tableName(), [
            'id'                    => $this->primaryKey(),
            'genders'               => $this->string()->defaultValue(NULL),
            'orientation'           => $this->string()->defaultValue(NULL),
            'isPaid'                => $this->string()->defaultValue(NULL),
            'searchable'            => $this->string()->defaultValue(NULL),
            'registrationPlatforms' => $this->text()->defaultValue(NULL),
            'excludedSources'       => $this->text()->defaultValue(NULL),
            'isActive'              => $this->boolean()->notNull()->defaultValue(false),
            'updatedAt'             => $this->timestamp()->defaultValue(NULL),
        ]);

        $this->sendExclusionInitialData();
        $this->userExclusionInitialData();
    }

    public function safeDown()
    {
        $this->dropTable(UserExclusionsRules::tableName());
        $this->dropTable(SendExclusionsRules::tableName());
    }

    private function sendExclusionInitialData()
    {
        $data = [];
        $confData = \Yii::$app->yaml->parse('sendExclusionDetails');

        $i = 0;
        foreach ($confData as $name => $datum) {
            $data[$i]['name'] = $name;
            $data[$i]['regDateFrom'] = $datum['regDateFrom'] ?? NULL;
            $data[$i]['regDateTo'] = $datum['regDateTo'] ?? NULL;
            $data[$i]['countries'] = $this->fieldEncode('country');
            $data[$i]['sitesIds'] = $this->fieldEncode('siteId');
            $data[$i]['sources'] = $this->fieldEncode('source');
            $data[$i]['isActive'] = 1;
            ++$i;
        }

        $fields = ['name', 'regDateFrom', 'regDateTo', 'countries', 'sitesIds', 'sources', 'isActive'];

        Yii::$app->db->createCommand()->batchInsert('sendExclusionsRules', $fields, $data)->execute();
    }

    private function userExclusionInitialData()
    {
        $confData = \Yii::$app->yaml->parse('suitableUserDetails');

        $initialRecord = new UserExclusionsRules();
        $initialRecord->gendersArray = $confData['gender'];
        $initialRecord->orientationArray = $confData['orientation'];
        $initialRecord->isPaidArray = $confData['isPaid'];
        $initialRecord->searchableArray = $confData['searchable'];
        $initialRecord->registrationPlatformsArray = $confData['registrationPlatform'];
        $initialRecord->excludedSourcesArray = $confData['excludedSources'];
        $initialRecord->isActive = 1;

        $initialRecord->save();
    }

    /**
     * @param $fieldName
     * @return string
     */
    private function fieldEncode($fieldName): ?string
    {
        return (isset($datum[$fieldName]) && !empty($datum[$fieldName])) ? json_encode($datum[$fieldName]) : NULL;
    }
}
