<?php

use yii\db\Migration;
use app\models\phoenix\SendRule;

class m160420_095553_create_apply_table extends Migration
{
    public function up()
    {
        $this->createTable(SendRule::tableName(), [
            'id' => $this->primaryKey(),
            'schemaId' => $this->integer(),
            'site' => $this->integer(),
            'country' => $this->string(2),
            'status' => $this->integer(3)
        ]);

        $this->createIndex('schemaId', SendRule::tableName(), 'schemaId');
        $this->createIndex('uniqueItems', SendRule::tableName(), 'schemaId, site, country', true);
    }

    public function down()
    {
        $this->dropTable(SendRule::tableName());
    }
}
