<?php

use yii\db\Migration;
use sender\communication\models\CommunicationSchedule;

class m161104_130608_martchenko_rename_models extends Migration
{
    public function up()
    {
        if (\Yii::$app->db->schema->getTableSchema('phoenixCommunicationSchedule')) {
            $this->renameTable('phoenixCommunicationSchedule', CommunicationSchedule::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixIncomingMessage')) {
            $this->renameTable('phoenixIncomingMessage', \app\models\phoenix\IncomingMessage::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixMatchingErrorLog')) {
            $this->renameTable('phoenixMatchingErrorLog', \app\models\phoenix\MatchingErrorLog::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixOfflineMessageSchedule')) {
            $this->renameTable('phoenixOfflineMessageSchedule', \app\models\phoenix\OfflineMessageSchedule::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixPhotoMonitoringScheduler')) {
            $this->renameTable('phoenixPhotoMonitoringScheduler', \app\models\phoenix\PhotoMonitoringScheduler::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixActivity')) {
            $this->renameTable('phoenixActivity', \app\models\phoenix\Activity::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixActivityScheduler')) {
            $this->renameTable('phoenixActivityScheduler', \app\models\phoenix\ActivityScheduler::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixGrabRule')) {
            $this->renameTable('phoenixGrabRule', \app\models\phoenix\GrabRule::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixPresenceEventLog')) {
            $this->renameTable('phoenixPresenceEventLog', \app\models\phoenix\PresenceEventLog::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixProcessedActivities')) {
            $this->renameTable('phoenixProcessedActivities', \app\models\phoenix\ProcessedActivities::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixProfileMessage')) {
            $this->renameTable('phoenixProfileMessage', \app\models\phoenix\ProfileMessage::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixRecipientProfile')) {
            $this->renameTable('phoenixRecipientProfile', \app\models\phoenix\RecipientProfile::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixReUploadPhotoScheduler')) {
            $this->renameTable('phoenixReUploadPhotoScheduler', \app\models\phoenix\ReUploadPhotoScheduler::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixRulesRegistration')) {
            $this->renameTable('phoenixRulesRegistration', \sender\registration\models\RulesRegistration::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixSchema')) {
            $this->renameTable('phoenixSchema', \app\models\phoenix\Schema::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixSearchProfile')) {
            $this->renameTable('phoenixSearchProfile', \app\models\phoenix\SearchProfile::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixSenderPhoto')) {
            $this->renameTable('phoenixSenderPhoto', \app\models\phoenix\SenderPhoto::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixSenderProfile')) {
            $this->renameTable('phoenixSenderProfile', \app\models\phoenix\SenderProfile::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixSenderProfileRegister')) {
            $this->renameTable('phoenixSenderProfileRegister', \app\models\phoenix\SenderProfileRegister::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixSendRule')) {
            $this->renameTable('phoenixSendRule', \app\models\phoenix\SendRule::tableName());
        }
        if (\Yii::$app->db->schema->getTableSchema('phoenixStepMessage')) {
            $this->renameTable('phoenixStepMessage', \app\models\phoenix\StepMessage::tableName());
        }
    }

    public function down()
    {
        echo "m161104_130608_martchenko_rename_models cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
