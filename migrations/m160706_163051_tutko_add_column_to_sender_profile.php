<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;

class m160706_163051_tutko_add_column_to_sender_profile extends Migration
{
    public function up()
    {
        $this->addColumn(SenderProfile::tableName(), 'state', $this->string(2));
        $this->addColumn(SenderProfile::tableName(), 'city', $this->string(25));
    }

    public function down()
    {
        $this->dropColumn(SenderProfile::tableName(), 'state');
        $this->dropColumn(SenderProfile::tableName(), 'city');
    }
}
