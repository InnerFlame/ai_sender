<?php

use yii\db\Migration;
use app\models\phoenix\ActivityScheduler;

class m160721_062957_tutko_create_index extends Migration
{
    public function up()
    {
        $this->createIndex('recipientId', ActivityScheduler::tableName(), 'recipientId');
    }

    public function down()
    {
        $this->dropIndex('recipientId', ActivityScheduler::tableName());
    }
}
