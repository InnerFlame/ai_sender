<?php

use yii\db\Migration;

class m161207_124905_tutko_cteate_tables_for_aggrigation_delay_stat extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `plannedCommunication` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `countToSend` INT(11) NOT NULL,
                `countNotSent` INT(11) NOT NULL,
                `step` INT(11) NOT NULL,
                `timeToSend` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `step` (`step`),
                KEY `timeToSend` (`timeToSend`),
                KEY `stepTimeToSend` (`step`, `timeToSend`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `delaySentCommunication` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `countSent` INT(11) NOT NULL,
                `delay` INT(11) NOT NULL,
                `step` INT(11) NOT NULL,
                `timeToSend` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `step` (`step`),
                KEY `delay` (`delay`),
                KEY `timeToSend` (`timeToSend`),
                KEY `stepTimeToSendDelay` (`step`, `timeToSend`, `delay`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `plannedCommunication`");

        $this->execute("DROP TABLE IF EXISTS `delaySentCommunication`");
    }
}
