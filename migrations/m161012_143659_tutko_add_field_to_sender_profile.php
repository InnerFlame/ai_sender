<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\OfflineMessageSchedule;
use app\models\phoenix\IncomingMessage;

class m161012_143659_tutko_add_field_to_sender_profile extends Migration
{
    public function up()
    {
        $this->addColumn(SenderProfile::tableName(), 'lastTimeOnline', 'TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00"');
        $this->addColumn(SenderProfile::tableName(), 'online', $this->boolean()->defaultValue(0));

        $this->execute("
            CREATE TABLE IF NOT EXISTS `" . OfflineMessageSchedule::tableName() . "`(
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `profileId` INT(11) NOT NULL,
                `startedCheck` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `profileId` (`profileId`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `phoenixNotExistsRecipient` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `recipientId` VARCHAR(32) NOT NULL,
                `senderId` VARCHAR(32) NOT NULL,
                `timeSent` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `message` TEXT,
                `cretedAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");

        $this->addColumn(IncomingMessage::tableName(), 'messageId', $this->string(32));
    }

    public function down()
    {
        $this->dropColumn(SenderProfile::tableName(), 'lastTimeOnline');
        $this->dropColumn(SenderProfile::tableName(), 'online');

        $this->execute("DROP TABLE IF EXISTS `" . OfflineMessageSchedule::tableName() . "`");

        $this->execute("DROP TABLE IF EXISTS `phoenixNotExistsRecipient`");

        $this->dropColumn(IncomingMessage::tableName(), 'messageId');
    }
}
