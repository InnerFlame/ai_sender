<?php

use yii\db\Migration;
use app\models\phoenix\Group;

class m170116_095523_tutko_change_struct_for_activity extends Migration
{
    public function up()
    {
        $this->execute('DROP TABLE IF EXISTS `activity`');

        $this->addColumn(Group::tableName(), 'percentWink', 'TINYINT(1) DEFAULT 0');
        $this->addColumn(Group::tableName(), 'percentAddToFriend', 'TINYINT(1) DEFAULT 0');
        $this->addColumn(Group::tableName(), 'percentBrowse', 'TINYINT(1) DEFAULT 0');
        $this->addColumn(Group::tableName(), 'typeSendActivity', $this->string(15 )->defaultValue(Group::TYPE_SEND_ACTIVITY_ALL));
    }

    public function down()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `activity` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `type` INT(11) NOT NULL,
                `percent` INT(11) NOT NULL DEFAULT 0,
                `schemaId` INT(11) NOT NULL,
                PRIMARY KEY (`id`),
                KEY `schemaId` (`schemaId`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");

        $this->dropColumn(Group::tableName(), 'percentWink');
        $this->dropColumn(Group::tableName(), 'percentAddToFriend');
        $this->dropColumn(Group::tableName(), 'percentBrowse');
        $this->dropColumn(Group::tableName(), 'typeSendActivity');
    }
}
