<?php

use yii\db\Migration;
use app\models\phoenix\ProfileMessage;
use app\models\phoenix\Group;
use app\models\phoenix\SenderProfileRegister;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SendRule;
use app\models\phoenix\Schema;

class m160428_143728_tutko_change_struct_tables extends Migration
{
    public function up()
    {
        $this->addColumn(ProfileMessage::tableName(), 'profileType', $this->integer());

        $this->addColumn(RecipientProfile::tableName(), 'site', $this->integer());
        $this->addColumn(RecipientProfile::tableName(), 'country', $this->string(2));
        $this->createIndex('siteCountry', RecipientProfile::tableName(), 'site, country');

        $this->createIndex('siteCountry', SenderProfile::tableName(), 'site, country');

        $this->alterColumn(Group::tableName(), 'dateStart', $this->integer());
        $this->alterColumn(Group::tableName(), 'dateEnd', $this->integer());
        $this->renameColumn(Group::tableName(), 'profileTypeId', 'profileType');
        $this->renameColumn(Group::tableName(), 'messageTypeId', 'messageType');

        $this->addColumn(SenderProfileRegister::tableName(), 'isActive', $this->boolean());

        $this->renameColumn(SendRule::tableName(), 'status', 'isActive');

        $this->renameColumn(Schema::tableName(), 'paid', 'isPaid');

        $this->addColumn('communicationLog', 'sendedAt', 'TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00"');
    }

    public function down()
    {
        $this->alterColumn(Group::tableName(), 'dateStart', $this->dateTime());
        $this->alterColumn(Group::tableName(), 'dateEnd', $this->dateTime());
        $this->renameColumn(Group::tableName(), 'profileType', 'profileTypeId');
        $this->renameColumn(Group::tableName(), 'messageType', 'messageTypeId');

        $this->dropColumn(ProfileMessage::tableName(), 'profileType');

        $this->dropColumn(RecipientProfile::tableName(), 'site');
        $this->dropColumn(RecipientProfile::tableName(), 'country');
        $this->dropIndex('siteCountry', RecipientProfile::tableName());

        $this->dropIndex('siteCountry', SenderProfile::tableName());

        $this->dropColumn(SenderProfileRegister::tableName(), 'isActive');

        $this->renameColumn(SendRule::tableName(), 'isActive', 'status');

        $this->renameColumn(Schema::tableName(), 'isPaid', 'paid');

        $this->dropColumn('communicationLog', 'sendedAt');
    }
}
