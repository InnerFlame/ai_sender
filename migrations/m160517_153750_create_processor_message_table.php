<?php

use yii\db\Migration;
use app\models\phoenix\IncomingMessage;


class m160517_153750_create_processor_message_table extends Migration
{
    public function up()
    {
        $this->createTable(IncomingMessage::tableName(),[
            'id'              => $this->primaryKey(),
            'siteId'          => $this->integer()->notNull(),
            'senderId'        => $this->integer()->notNull(),
            'recipientId'     => $this->integer()->notNull(),
            'step'            => $this->integer()->defaultValue(1),
            'isUsed'          => $this->boolean()->defaultValue(0),
            'text'            => $this->text(),
            'createdAt'       => $this->timestamp(),
        ]);

        $this->createIndex('getStepAnswers', IncomingMessage::tableName(), 'siteId, senderId, recipientId');
        $this->createIndex('senderId', IncomingMessage::tableName(), 'senderId');
        $this->createIndex('recipientId', IncomingMessage::tableName(), 'recipientId');
        $this->createIndex('isUsed', IncomingMessage::tableName(), 'isUsed');
        $this->createIndex('createdAt', IncomingMessage::tableName(), 'createdAt');
    }

    public function down()
    {
        $this->dropTable(IncomingMessage::tableName());
    }
}
