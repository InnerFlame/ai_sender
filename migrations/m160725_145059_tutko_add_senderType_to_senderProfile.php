<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;

class m160725_145059_tutko_add_senderType_to_senderProfile extends Migration
{
    public function up()
    {
        $this->addColumn(SenderProfile::tableName(), 'userType', $this->string(8)->defaultValue(SenderProfile::TYPE_SENDER));
    }

    public function down()
    {
        $this->dropColumn(SenderProfile::tableName(), 'userType');
    }
}
