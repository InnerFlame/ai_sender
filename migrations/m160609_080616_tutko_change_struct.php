<?php

use yii\db\Migration;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\ {
    SenderProfileRegister,
    SenderProfile,
    IncomingMessage
};

class m160609_080616_tutko_change_struct extends Migration
{
    public function up()
    {
        $this->dropColumn(IncomingMessage::tableName(), 'siteId');
        $this->dropIndex('getStepAnswers', IncomingMessage::tableName());
        $this->createIndex('getStepAnswers', IncomingMessage::tableName(), 'senderId, recipientId');

        $this->addColumn(SenderProfile::tableName(), 'originId', $this->string(32));
        $this->addColumn(SenderProfile::tableName(), 'accessToken', $this->string(32));
        $this->addColumn(SenderProfile::tableName(), 'refreshToken', $this->string(32));
        $this->addColumn(SenderProfile::tableName(), 'bannedAt', 'TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00"');
        $this->createIndex('originId', SenderProfile::tableName(), 'originId');

        $this->addColumn(SenderProfileRegister::tableName(), 'errorAt', 'TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00"');
        $this->addColumn(SenderProfileRegister::tableName(), 'errorText', 'text');

        $this->addColumn(CommunicationSchedule::tableName(), 'sendStartedAt', 'TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00"');
        $this->addColumn(CommunicationSchedule::tableName(), 'isSenderBanned', $this->boolean()->defaultValue(0));

        $this->addColumn(IncomingMessage::tableName(), 'subject', $this->string(50));

        $this->alterColumn(CommunicationSchedule::tableName(), 'step', 'INT(11) NULL DEFAULT 0');
    }

    public function down()
    {
        $this->addColumn(IncomingMessage::tableName(), 'siteId', $this->integer());
        $this->dropIndex('getStepAnswers', IncomingMessage::tableName());
        $this->createIndex('getStepAnswers', IncomingMessage::tableName(), 'siteId, senderId, recipientId');

        $this->dropIndex('originId', SenderProfile::tableName());
        $this->dropColumn(SenderProfile::tableName(), 'originId');
        $this->dropColumn(SenderProfile::tableName(), 'accessToken');
        $this->dropColumn(SenderProfile::tableName(), 'refreshToken');
        $this->dropColumn(SenderProfile::tableName(), 'bannedAt');

        $this->dropColumn(SenderProfileRegister::tableName(), 'errorAt');
        $this->dropColumn(SenderProfileRegister::tableName(), 'errorText');

        $this->dropColumn(CommunicationSchedule::tableName(), 'sendStartedAt');

        $this->dropColumn(IncomingMessage::tableName(), 'subject');

        $this->alterColumn(CommunicationSchedule::tableName(), 'step', 'INT(11) NULL DEFAULT 1');
        $this->dropColumn(CommunicationSchedule::tableName(), 'isSenderBanned');
    }
}
