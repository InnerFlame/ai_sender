<?php

use yii\db\Migration;
use app\models\phoenix\RecipientProfile;

class m170807_135159_tutko_add_field_to_recipient extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `' . RecipientProfile::tableName() . '` ADD COLUMN `isExcludeByTopic` BOOLEAN NOT NULL DEFAULT FALSE;');
    }

    public function safeDown()
    {
        $this->execute('ALTER TABLE `' . RecipientProfile::tableName() . '` DROP COLUMN `isExcludeByTopic`');
    }
}
