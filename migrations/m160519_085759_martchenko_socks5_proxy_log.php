<?php

use yii\db\Migration;

class m160519_085759_martchenko_socks5_proxy_log extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `socks5UsageLog` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `clientName` varchar(32) NOT NULL,
                `proxy` varchar(32) NOT NULL,
                `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                 PRIMARY KEY (`id`),
                 KEY `clientName` (`clientName`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table to store proxy usages log'
        ");
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `socks5UsageLog`");
    }
}
