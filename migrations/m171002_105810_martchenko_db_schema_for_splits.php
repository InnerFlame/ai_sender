<?php

use yii\db\Migration;

class m171002_105810_martchenko_db_schema_for_splits extends Migration
{
    public function up()
    {
        $this->createTable(
            'split',
            [
                'id'                    => $this->primaryKey(),
                'name'                  => $this->string()->notNull(),
                'createdAt'             => $this->timestamp()->notNull(),
                'isActive'              => $this->boolean()->notNull()->defaultValue(false),
                'startedAt'             => $this->timestamp()->notNull(),
                'finishedAt'            => $this->timestamp()->notNull(),
                'percentage'            => $this->string(64)->notNull(),
                'regDateFrom'           => $this->timestamp()->defaultValue(null),
                'regDateTo'             => $this->timestamp()->defaultValue(null),
                'country'               => $this->string()->defaultValue(null),
                'gender'                => $this->string(64)->defaultValue(null),
                'sexuality'             => $this->string(64)->defaultValue(null),
                'isPaid'                => $this->string(64)->defaultValue(null),
                'registrationPlatform'  => $this->string(64)->defaultValue(null),
                'site'                  => $this->text()->defaultValue(null),
                'source'                => $this->text()->defaultValue(null),
                'description'           => $this->text()->defaultValue(null),
            ]
        );
        $this->createIndex('name', 'split', 'name', true);
    }

    public function down()
    {
        $this->dropTable('split');
    }
}
