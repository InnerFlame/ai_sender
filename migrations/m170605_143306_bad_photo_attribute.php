<?php

use app\models\phoenix\SenderPhoto;
use yii\db\Migration;

class m170605_143306_bad_photo_attribute extends Migration
{
    public function up()
    {
        $this->addColumn(SenderPhoto::tableName(), 'isBad', $this->boolean()->notNull()->defaultValue(0));
        $this->addColumn(SenderPhoto::tableName(), 'badPhotoReason', $this->string(64));
    }

    public function down()
    {
        $this->dropColumn(SenderPhoto::tableName(), 'isBad');
        $this->dropColumn(SenderPhoto::tableName(), 'badPhotoReason');
    }
}
