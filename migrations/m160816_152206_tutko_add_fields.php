<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\RecipientProfile;

class m160816_152206_tutko_add_fields extends Migration
{
    const TABLE_PHOTO_MONITOR_SCHEDULER = 'photoMonitoringScheduler';

    public function up()
    {
        $this->addColumn(SenderProfile::tableName(), 'location', $this->string(50));

        $this->dropIndex('country', RecipientProfile::tableName());

        $this->renameColumn(self::TABLE_PHOTO_MONITOR_SCHEDULER, 'endMonitoringAt', 'monitoringFinishedAt');

        $this->alterColumn(SenderProfile::tableName(), 'gtmClientId', $this->string(36));
        $this->alterColumn(SenderProfile::tableName(), 'marker', $this->string(32));
    }

    public function down()
    {
        $this->dropColumn(SenderProfile::tableName(), 'location');

        $this->createIndex('country', RecipientProfile::tableName(), 'country');

        $this->renameColumn(self::TABLE_PHOTO_MONITOR_SCHEDULER, 'monitoringFinishedAt', 'endMonitoringAt');

        $this->alterColumn(SenderProfile::tableName(), 'gtmClientId', $this->string(255));
        $this->alterColumn(SenderProfile::tableName(), 'marker', $this->string(255));
    }
}
