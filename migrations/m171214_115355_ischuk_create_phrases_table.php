<?php

use yii\db\Migration;
use yii\db\Expression;

/**
 * Class m171214_115355_ischuk_create_phrases_table
 */
class m171214_115355_ischuk_create_phrases_table extends Migration
{
    private $tablePhraseCensor = 'phraseCensor';
    private $tablePhraseCategory = 'phraseCategory';
    private $tablePhraseTags = 'phraseTags';

    private $idxCategoryId = 'idx-phraseCensor-categoryId';
    private $idxTagId = 'idx-phraseCensor-tagId';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = NULL;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8';
        }

        $this->createTable($this->tablePhraseCensor, [
            'id'         => $this->primaryKey(),
            'text'       => $this->string(30),
            'categoryId' => $this->integer(11)->notNull()->defaultValue(0),
            'tagId'      => $this->integer(11)->notNull()->defaultValue(0),
            'staffId'    => $this->smallInteger(5)->notNull()->defaultValue(0),
            'createdAt'  => $this->timestamp()->defaultValue(new Expression('NOW()')),
        ], $tableOptions);

        $this->createTable($this->tablePhraseCategory, [
            'id'   => $this->primaryKey(),
            'name' => $this->string(30),
        ], $tableOptions);

        $this->createTable($this->tablePhraseTags, [
            'id'  => $this->primaryKey(),
            'tag' => $this->string(30),
        ], $tableOptions);

        $this->createIndex($this->idxCategoryId, $this->tablePhraseCensor, 'categoryId');
        $this->createIndex($this->idxTagId, $this->tablePhraseCensor, 'tagId');

        $this->batchCategory();
        $this->batchTag();
        $this->batchCensor();
    }

    private function batchCategory()
    {
        $data = $this->getData('phraseCategory');
        $this->batchInsert($this->tablePhraseCategory, ['id', 'name'], $data);
    }

    private function batchTag()
    {
        $data = $this->getData('phraseTag');
        $this->batchInsert($this->tablePhraseTags, ['id', 'tag'], $data);
    }

    private function batchCensor()
    {
        $data = $this->getData('phraseCensor');
        foreach ($data as $num => &$item) {
            $item[5] = trim(str_replace(' |', '', $item[5]));
        }
        $this->batchInsert($this->tablePhraseCensor, ['id', 'text', 'categoryId', 'tagId', 'staffId', 'createdAt'], $data);
    }

    private function getData($file)
    {
        $data = [];
        $rows = explode("\n", @file_get_contents(__DIR__ . '/data/' . $file . '.txt'));

        foreach ($rows as $numRow => $row) {
            if ($file === 'phraseCensor') {
                $cols = explode("| ", $row);
                array_shift($cols);
            } else {
                $cols = explode("|", $row);
                array_shift($cols);
                array_pop($cols);
            }
            $cols = array_map('trim', $cols);
            if (!$cols || $numRow == 1) {
                continue;
            }
            $data[] = $cols;
        }

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex($this->idxTagId, $this->tablePhraseCensor);
        $this->dropIndex($this->idxCategoryId, $this->tablePhraseCensor);

        $this->dropTable($this->tablePhraseTags);
        $this->dropTable($this->tablePhraseCategory);
        $this->dropTable($this->tablePhraseCensor);
    }
}

