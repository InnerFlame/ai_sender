<?php

use yii\db\Migration;

class m170615_100457_martchenko_use_db_for_containers_and_clients extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `container` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `profileId` INT(11) NOT NULL,
                `name` VARCHAR (64) NOT NULL,
                `status` VARCHAR (32) NOT NULL,
                `target` VARCHAR (32) NOT NULL,
                `type` VARCHAR (32) NOT NULL,
                `country` VARCHAR (3) NOT NULL,
                `site` INT(11) NOT NULL,
                `timeSetStatus` INT(11) DEFAULT '0',
                `lastTimeActivity` INT(11) DEFAULT '0',
                PRIMARY KEY (`id`),
                KEY `profileId` (`profileId`),
                KEY `name` (`name`),
                KEY `status_type` (`status`, `type`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `client` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `profileId` INT(11) NOT NULL,
                `name` VARCHAR (64) NOT NULL,
                `status` VARCHAR (32) NOT NULL,
                `target` VARCHAR (32) NOT NULL,
                `type` VARCHAR (32) NOT NULL,
                `country` VARCHAR (3) NOT NULL,
                `site` INT(11) NOT NULL,
                `timeSetStatus` INT(11) DEFAULT '0',
                `refreshingTime` INT(11) DEFAULT '0',
                PRIMARY KEY (`id`),
                KEY `profileId` (`profileId`),
                KEY `name` (`name`),
                KEY `status_type` (`status`, `type`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
    }

    public function safeDown()
    {
        $this->execute('DROP TABLE IF EXISTS `container`');
        $this->execute('DROP TABLE IF EXISTS `client`');
    }
}
