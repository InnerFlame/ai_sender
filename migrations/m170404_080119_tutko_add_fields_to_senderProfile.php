<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;
use app\models\rpc\LifeCycleContainersInfo;

class m170404_080119_tutko_add_fields_to_senderProfile extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `" . LifeCycleContainersInfo::tableName() . "`(
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `country` varchar(3) DEFAULT NULL,
                `data` text,
                PRIMARY KEY (`id`),
                KEY `country` (`country`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");

        $this->addColumn(SenderProfile::tableName(), 'canBeInPullAt', 'TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00"');
        $this->addColumn(SenderProfile::tableName(), 'liveTime', 'TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00"');
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `" . LifeCycleContainersInfo::tableName() . "`");

        $this->dropColumn(SenderProfile::tableName(), 'canBeInPullAt');
        $this->dropColumn(SenderProfile::tableName(), 'liveTime');
    }
}
