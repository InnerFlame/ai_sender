<?php

use yii\db\Migration;
use app\models\phoenix\CheckBanScheduler;

class m170208_154737_tutko_create_table_for_check_user_scamer_status extends Migration
{
    public function up()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `' . CheckBanScheduler::tableName() . '` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `senderId` INT(11) NOT NULL,
                `recipientId` INT(11) NOT NULL,
                `attemptsCount` INT(11) NOT NULL DEFAULT 0,
                `state` VARCHAR(20),
                `timeToSend` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `sendStartedAt` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `sentAt` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `senderId` (`senderId`),
                KEY `timeToSend` (`timeToSend`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ');

    }

    public function down()
    {
        $this->execute('DROP TABLE IF EXISTS `' . CheckBanScheduler::tableName() . '`');
    }
}
