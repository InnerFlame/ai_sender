<?php

use yii\db\Migration;

class m171103_085752_tutko_add_scheme_to_exludes_rules extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `sendExclusionsRules` ADD COLUMN schemaIds TEXT NULL DEFAULT NULL');
        $this->execute('ALTER TABLE `sendExclusionsRules` ADD COLUMN platforms TEXT NULL DEFAULT NULL');
    }

    public function safeDown()
    {
        $this->execute('ALTER TABLE `sendExclusionsRules` DROP COLUMN schemaIds');
        $this->execute('ALTER TABLE `sendExclusionsRules` DROP COLUMN platforms');
    }

}
