<?php

use yii\db\Migration;
use app\models\phoenix\StepMessage;

class m160812_125816_tutko_change_struct_step_by_step extends Migration
{
    public function up()
    {
        $this->dropColumn(StepMessage::tableName(), 'step');
        $this->addColumn(StepMessage::tableName(), 'hash', $this->string(32));
        $this->createIndex('hash', StepMessage::tableName(), 'hash', true);
    }

    public function down()
    {
        $this->dropIndex('hash', StepMessage::tableName());
        $this->dropColumn(StepMessage::tableName(), 'hash');
        $this->addColumn(StepMessage::tableName(), 'step', $this->integer(11));
    }
}
