<?php

use yii\db\Migration;

class m180220_102754_martchenko_addProxyIps extends Migration
{
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `proxyIp` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `ipStart` int(11) unsigned NOT NULL DEFAULT \'0\',
              `ipEnd` int(11) unsigned NOT NULL DEFAULT \'0\',
              `anonimity` varchar(16) DEFAULT NULL,
              `type` varchar(16) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `ipEnd_ipStart` (`ipEnd`,`ipStart`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ');

        $this->execute('
              DROP TABLE IF EXISTS `usedGeoIp`;
        ');
    }

    public function safeDown()
    {
        $this->execute('
              DROP TABLE IF EXISTS `proxyIp`;
        ');

        $this->execute('
            CREATE TABLE IF NOT EXISTS `usedGeoIp` (
                `idGeoIp` INT(11) unsigned NOT NULL,
                KEY `idGeoIp` (`idGeoIp`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ');
    }
}