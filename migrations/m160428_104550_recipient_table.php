<?php

use yii\db\Migration;
use app\models\phoenix\RecipientProfile;

class m160428_104550_recipient_table extends Migration
{
    public function up()
    {
        $this->createTable(RecipientProfile::tableName(), [
            'id' => $this->primaryKey(),
            'remoteId' => $this->string(40)->notNull(),
            'remoteType' => $this->integer()->notNull(),
            'isPaid' => $this->boolean()->notNull(),
            'sexuality' => $this->integer()->notNull(),
            'gender' => $this->integer()->notNull(),
            'birthday' => $this->date()->notNull(),
            'activationTime' => $this->timestamp()->defaultValue(0),
        ]);
    }

    public function down()
    {
        $this->dropTable(RecipientProfile::tableName());
    }
}
