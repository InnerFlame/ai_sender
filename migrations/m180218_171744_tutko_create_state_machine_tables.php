<?php

use yii\db\Migration;

/**
 * Class m180218_171744_tutko_create_state_machine_tables
 */
class m180218_171744_tutko_create_state_machine_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `stateMachine` (
                `id` INT (11) unsigned NOT NULL AUTO_INCREMENT,
                `model` VARCHAR (255) NOT NULL,
                `modelPk` INT (11) NOT NULL,
                `smName` VARCHAR (255) NOT NULL,
                `fromState` VARCHAR (255) DEFAULT NULL,
                `toState` VARCHAR (255) DEFAULT NULL,
                `status` VARCHAR (255) NOT NULL,
                `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `modelModelPkSmName` (`model`, `modelPk`, `smName`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

        $this->execute('
            CREATE TABLE IF NOT EXISTS `stateMachineTransition` (
                `id` INT (11) unsigned NOT NULL AUTO_INCREMENT,
                `stateMachineId` INT (11) NOT NULL,
                `transition` VARCHAR (255) NOT NULL,
                `status` VARCHAR (255) NOT NULL,
                `countRemainingAttempts` INT (11) NOT NULL,
                `timeStart` timestamp NOT NULL DEFAULT "0000-00-00 00:00:00",
                `timeRepeat` timestamp NOT NULL DEFAULT "0000-00-00 00:00:00",
                `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `stateMachineId` (`stateMachineId`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute('DROP TABLE IF EXISTS `stateMachine`;');

        $this->execute('DROP TABLE IF EXISTS `stateMachineTransition`;');
    }
}
