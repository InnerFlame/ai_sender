<?php

use yii\db\Migration;
use yii\db\Expression;
use app\models\phoenix\NichesCommTroublesLog;

/**
 * Class m170912_082532_khachatryan_create_nichesLog_table
 */
class m170912_082532_khachatryan_create_nichesLog_table extends Migration
{
    public function safeUp()
    {
        $this->createTable(NichesCommTroublesLog::tableName(), [
            'id'            => $this->primaryKey(),
            'niche'         => $this->string(30)->notNull(),
            'country'       => $this->string(3)->notNull(),
            'profileType'   => $this->smallInteger()->notNull()->comment('Sender(1) / Recipient(2)'),
            'message'       => $this->string(),
            'createdAt'     => $this->timestamp()->defaultValue(new Expression('NOW()')),
        ]);

        $this->createIndex('createdAt', NichesCommTroublesLog::tableName(), 'createdAt');
        $this->createIndex('searchIndex', NichesCommTroublesLog::tableName(), ['country', 'profileType']);

    }

    public function safeDown()
    {
        $this->dropTable(NichesCommTroublesLog::tableName());
    }
}
