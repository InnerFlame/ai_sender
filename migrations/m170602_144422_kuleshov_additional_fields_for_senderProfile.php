<?php

use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileAttributes;
use yii\db\Migration;

class m170602_144422_kuleshov_additional_fields_for_senderProfile extends Migration
{
    public function up()
    {
        $this->createTable(
            SenderProfileAttributes::tableName(),
            [
                'id' => $this->integer()->unique()->notNull(),
                'photoLevel' => $this->smallInteger()->notNull()->defaultValue(0),
                'height' => $this->smallInteger()->notNull()->defaultValue(0),
                'weight' => $this->smallInteger()->notNull()->defaultValue(0),
                'build' => $this->smallInteger()->notNull()->defaultValue(0),
                'race' => $this->smallInteger()->notNull()->defaultValue(0),
                'eyeColor' => $this->smallInteger()->notNull()->defaultValue(0),
                'hairColor' => $this->smallInteger()->notNull()->defaultValue(0),
                'familyStatus' => $this->smallInteger()->notNull()->defaultValue(0),
                'glasses' => $this->boolean()->notNull()->defaultValue(0),
                'religion' => $this->smallInteger()->notNull()->defaultValue(0),
            ]
        );
        $this->addForeignKey(
            'fk_sender_profile_attributes_sender_profile',
            SenderProfileAttributes::tableName(),
            'id',
            SenderProfile::tableName(),
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk_sender_profile_attributes_sender_profile', SenderProfileAttributes::tableName());
        $this->dropTable(SenderProfileAttributes::tableName());
    }
}
