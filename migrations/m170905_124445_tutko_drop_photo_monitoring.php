<?php

use yii\db\Migration;

class m170905_124445_tutko_drop_photo_monitoring extends Migration
{
    public function safeUp()
    {
        $this->execute('DROP TABLE IF EXISTS `photoMonitoringScheduler`');
    }

    public function safeDown()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `photoMonitoringScheduler` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `senderId` int(11) NOT NULL,
                `observerId` int(11) NOT NULL,
                `timeToMonitoring` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                `startMonitoring` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                `monitoringFinishedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `timeToMonitoring` (`timeToMonitoring`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1235817 DEFAULT CHARSET=utf8
      ");
    }
}
