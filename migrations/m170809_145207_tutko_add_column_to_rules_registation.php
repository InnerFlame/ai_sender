<?php

use yii\db\Migration;
use sender\registration\models\RulesRegistration;

class m170809_145207_tutko_add_column_to_rules_registation extends Migration
{
    public function safeUp()
    {
        $this->execute('
            ALTER TABLE `' . RulesRegistration::tableName() . '` ADD COLUMN `niche` VARCHAR(25);
            ALTER TABLE `' . RulesRegistration::tableName() . '` CHANGE `isActive` `isActive` TINYINT(1) NULL DEFAULT 0;
        ');
    }

    public function safeDown()
    {
        $this->execute('ALTER TABLE `' . RulesRegistration::tableName() . '` DROP COLUMN `niche`');
    }
}
