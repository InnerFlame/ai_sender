<?php

use yii\db\Migration;
use app\models\phoenix\Group;
use app\models\phoenix\Schema;

class m160414_110659_create_tables extends Migration
{
    public function up()
    {
        $this->createTable('site', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'callbackUrl' => $this->string(),
            'token' => $this->string(32),
        ]);

        $this->createTable('profileType', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);

        $this->createTable('messageType', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);

        $this->createTable(Group::tableName(), [
            'id' => $this->primaryKey(),
            'schemaId' => $this->integer(),
            'siteId' => $this->integer(),
            'profileTypeId' => $this->integer(),
            'messageTypeId' => $this->integer(),
            'messageCount' => $this->integer(),
            'title' => $this->string(),
            'dateStart' => $this->timestamp(),
            'dateEnd' => $this->timestamp(),
            'period' => $this->integer()
        ]);

        $this->createTable(Schema::tableName(), [
            'id' => $this->primaryKey(),
            'siteId' => $this->integer()->notNull(),
            'title' => $this->string(100)->notNull(),
            'description' => $this->string(255),
            'paid' => "ENUM('yes', 'no') NOT NULL DEFAULT 'no' ",
            'gender' => "ENUM('female', 'male') NOT NULL DEFAULT 'male' ",
            'sexuality' => "ENUM('gey', 'bee', 'hetero') NOT NULL DEFAULT 'hetero' ",
            'ageFrom' => $this->integer(3)->notNull(),
            'ageOn' => $this->integer(3)->notNull(),
        ]);

        $this->createIndex('schemaIdK', Group::tableName(), 'schemaId');
        $this->createIndex('siteIdK', Group::tableName(), 'siteId');
        $this->createIndex('profileTypeIdK', Group::tableName(), 'profileTypeId');
        $this->createIndex('messageTypeIdK', Group::tableName(), 'messageTypeId');

        $this->createIndex('siteIdK', Schema::tableName(), 'siteId');
    }

    public function down()
    {
        $this->dropTable(Schema::tableName());

        $this->dropTable(Group::tableName());

        $this->dropTable('profileType');

        $this->dropTable('messageType');
    }
}
