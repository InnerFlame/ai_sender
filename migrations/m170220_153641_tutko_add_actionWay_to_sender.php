<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderEmail;

class m170220_153641_tutko_add_actionWay_to_sender extends Migration
{
    public function up()
    {
        $this->addColumn(SenderProfile::tableName(), 'actionWay', $this->string(20)->defaultValue(null));

        SenderProfile::updateAll(['actionWay' => SenderProfile::ACTION_WAY_EXTERNAL]);

        $this->execute('
            CREATE TABLE IF NOT EXISTS `' . SenderEmail::tableName() . '`(
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `email` VARCHAR(255) NOT NULL,
                `domain` VARCHAR(15) NOT NULL,
                `isUsed` INT(11) NOT NULL DEFAULT 0,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `isUsed` (`isUsed`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ');
    }

    public function down()
    {
        $this->dropColumn(SenderProfile::tableName(), 'actionWay');

        $this->execute('DROP TABLE IF EXISTS `' . SenderEmail::tableName() . '`');
    }
}
