<?php

use yii\db\Migration;

/**
 * Class m180906_132441_sarychev_remove_logs_tables
 */
class m180906_132441_sarychev_remove_logs_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("DROP TABLE IF EXISTS `evaLog`");
        $this->execute("DROP TABLE IF EXISTS `lifeCycleLog`");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `evaLog` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `senderId` INT(11) NOT NULL,
                `recipientId` INT(11) NOT NULL,
                `site` INT(11) NOT NULL,
                `country` VARCHAR(3) NOT NULL,
                `message` TEXT,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                'response' text,
                'responseTime' TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                PRIMARY KEY (`id`),
                KEY `recipientId` (`recipientId`),
                KEY `senderId` (`senderId`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
        $this->execute('
            CREATE TABLE IF NOT EXISTS `lifeCycleLog` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `senderId` INT(11) NOT NULL,
                `extra` text,
                `addedToInitialAt` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `addedToAnswerAt` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `deletedFromPullAt` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `senderId` (`senderId`)
            )
        ');
    }
}
