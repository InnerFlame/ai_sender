<?php

use sender\registration\models\RulesRegistration;
use app\models\phoenix\Schema;
use app\models\phoenix\Group;
use yii\db\Migration;

class m160505_130003_rename_table_and_column extends Migration
{
    public function up()
    {
        $this->renameColumn(Schema::tableName(), 'ageOn', 'ageTo');

        $this->renameColumn(RulesRegistration::tableName(), 'dateFrom', 'startTimeOffset');
        $this->renameColumn(RulesRegistration::tableName(), 'dateTo', 'endTimeOffset');

        $this->renameColumn(Group::tableName(), 'messageType', 'sendMode');
    }

    public function down()
    {
        $this->renameColumn(Schema::tableName(), 'ageTo', 'ageOn');

        $this->renameColumn(RulesRegistration::tableName(), 'startTimeOffset', 'dateFrom');
        $this->renameColumn(RulesRegistration::tableName(), 'endTimeOffset', 'dateTo');

        $this->renameColumn(Group::tableName(), 'sendMode', 'messageType');
    }

}
