<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileRegister;
use sender\communication\models\CommunicationSchedule;

class m160704_071713_tutko_add_keys_to_sender_profile extends Migration
{
    public function up()
    {
        $this->createIndex('bannedAt', SenderProfile::tableName(), 'bannedAt');
        $this->createIndex('activeCommunication', CommunicationSchedule::tableName(), 'sentAt, isSenderBanned');
        $this->createIndex('queueToRegistration', SenderProfileRegister::tableName(), 'registeredAt');
    }

    public function down()
    {
        $this->dropIndex('bannedAt', SenderProfile::tableName());
        $this->dropIndex('activeCommunication', CommunicationSchedule::tableName());
        $this->dropIndex('queueToRegistration', SenderProfileRegister::tableName());
    }
}
