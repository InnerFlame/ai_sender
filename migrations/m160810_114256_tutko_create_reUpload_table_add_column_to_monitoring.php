<?php

use yii\db\Migration;
use app\models\phoenix\ReUploadPhotoScheduler;
use app\models\phoenix\SenderProfile;

class m160810_114256_tutko_create_reUpload_table_add_column_to_monitoring extends Migration
{
    const TABLE_PHOTO_MONITOR_SCHEDULER = 'photoMonitoringScheduler';

    public function up()
    {
        $this->addColumn(self::TABLE_PHOTO_MONITOR_SCHEDULER, 'endMonitoringAt', " TIMESTAMP  NOT NULL DEFAULT '0000-00-00 00:00:00'");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `" . ReUploadPhotoScheduler::tableName() . "`(
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `senderId` INT(11) NOT NULL,
                `timeToReUpload` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `startReUpload`TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `senderId` (`senderId`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");

        $this->addColumn(SenderProfile::tableName(), 'photoUploadedAt', "TIMESTAMP  NOT NULL DEFAULT '0000-00-00 00:00:00'");
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_PHOTO_MONITOR_SCHEDULER, 'endMonitoringAt');

        $this->execute("DROP TABLE IF EXISTS `" . ReUploadPhotoScheduler::tableName() . "`");

        $this->dropColumn(SenderProfile::tableName(), 'photoUploadedAt');
    }
}
