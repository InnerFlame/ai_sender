<?php

use yii\db\Migration;

class m161114_132646_tutko_log_fail_eva_answers extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `evaLog` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `senderId` INT(11) NOT NULL,
                `recipientId` INT(11) NOT NULL,
                `site` INT(11) NOT NULL,
                `country` VARCHAR(3) NOT NULL,
                `message` TEXT,
                `error` VARCHAR(50),
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `recipientId` (`recipientId`),
                KEY `senderId` (`senderId`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `evaLog`");
    }
}
