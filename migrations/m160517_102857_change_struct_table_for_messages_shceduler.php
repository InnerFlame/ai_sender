<?php

use yii\db\Migration;
use sender\communication\models\CommunicationSchedule;

class m160517_102857_change_struct_table_for_messages_shceduler extends Migration
{
    public function up()
    {
        $this->alterColumn(CommunicationSchedule::tableName(), 'groupId', $this->integer()->defaultValue(NULL));

        $this->addColumn(CommunicationSchedule::tableName(), 'communicationType', $this->integer()->defaultValue(0));
        $this->addColumn(CommunicationSchedule::tableName(), 'step', $this->integer()->defaultValue(NULL));
        $this->createIndex('communicationType', CommunicationSchedule::tableName(), 'communicationType');
    }

    public function down()
    {
        $this->alterColumn(CommunicationSchedule::tableName(), 'groupId', $this->integer()->notNull());

        $this->dropColumn(CommunicationSchedule::tableName(), 'communicationType');
    }

}
