<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;

class m170210_160436_tutko_add_field_to_senderProfile extends Migration
{
    public function up()
    {
        $this->addColumn(SenderProfile::tableName(), 'ip', $this->string(20)->defaultValue(null));
        $this->createIndex('ip', SenderProfile::tableName(), 'ip');
    }

    public function down()
    {
        $this->dropColumn(SenderProfile::tableName(), 'ip');
    }
}
