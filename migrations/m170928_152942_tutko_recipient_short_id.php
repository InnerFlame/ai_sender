<?php

use yii\db\Migration;

class m170928_152942_tutko_recipient_short_id extends Migration
{
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `recipientShortId` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `recipientId` INT(11) NOT NULL,
                `shortId` VARCHAR (10) NOT NULL,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `recipientIdShortId` (`recipientId`, `shortId`),
                KEY `ShortId` (`shortId`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
    }

    public function safeDown()
    {
        $this->execute('DROP TABLE IF EXISTS `recipientShortId`');
    }
}
