<?php

use yii\db\Migration;

class m170921_145938_tutko_create_table_resipient_placeholder_initial_log extends Migration
{
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `recipientPlaceholderInitialLog` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `recipientId` INT(11) NOT NULL,
                `countSent` INT(11) DEFAULT 0,
                `lastRestart` INT(11) DEFAULT 0,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `recipientId` (`recipientId`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
    }

    public function safeDown()
    {
        $this->execute('DROP TABLE IF EXISTS `recipientPlaceholderInitialLog`');
    }
}
