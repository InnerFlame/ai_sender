<?php

use yii\db\Migration;
use app\models\phoenix\CheckBanScheduler;

class m170316_140903_tutko_add_information_to_checkBan_table extends Migration
{
    public function up()
    {
        $this->addColumn(CheckBanScheduler::tableName(), 'information', $this->string());
    }

    public function down()
    {
        $this->dropColumn(CheckBanScheduler::tableName(), 'information');
    }
}
