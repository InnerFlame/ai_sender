<?php

use yii\db\Migration;

/**
 * Class m180306_102942_tutko_add_originId_to_eva_log
 */
class m180306_102942_tutko_add_originId_to_eva_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('TRUNCATE TABLE `evaLog`');
        $this->execute('
            ALTER TABLE `evaLog`
            ADD COLUMN `senderOriginId` VARCHAR(32) NOT NULL
        ');
        $this->execute('
            ALTER TABLE `evaLog`
            ADD COLUMN `recipientOriginId` VARCHAR(32) NOT NULL
        ');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute('TRUNCATE TABLE `' . 'evaLog' . '`');
        $this->execute('
            ALTER TABLE `evaLog`
            DROP COLUMN `senderOriginId`
        ');
        $this->execute('
            ALTER TABLE `evaLog`
            DROP COLUMN `recipientOriginId`
        ');
    }

}
