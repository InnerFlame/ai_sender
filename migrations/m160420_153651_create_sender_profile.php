<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileRegister;

class m160420_153651_create_sender_profile extends Migration
{
    public function up()
    {
        $this->createTable(SenderProfile::tableName(), [
            'id' => $this->primaryKey(),
            'firstName' => $this->string(50)->notNull(),
            'lastName' => $this->string(50)->notNull(),
            'screenName' => $this->string(50)->notNull(),
            'birthday' => $this->date()->notNull(),
            'site' => $this->integer()->notNull(),
            'profileType' => $this->integer()->notNull(),
            'country' => $this->string(2)->notNull(),
            'sexuality' => $this->integer()->notNull(),
            'gender' => $this->integer()->notNull(),
            'description' => $this->text(),
            'photo' => $this->string()
        ]);

        $this->createTable(SenderProfileRegister::tableName(), [
            'id' => $this->primaryKey(),
            'profileId' => $this->integer(),
            'dateRegistration' => $this->timestamp(),
            'dateRegistrationSite' => $this->timestamp(),
            'statusRegistration' => $this->boolean()
        ]);

        $this->createIndex('profileId', SenderProfileRegister::tableName(), 'profileId');
    }

    public function down()
    {
        $this->dropTable(SenderProfile::tableName());
    }
}
