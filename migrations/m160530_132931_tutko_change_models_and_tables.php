<?php

use yii\db\Migration;
use app\models\phoenix\{
    SenderProfile,
    SenderProfileRegister,
    PresenceEventLog
};

class m160530_132931_tutko_change_models_and_tables extends Migration
{
    public function up()
    {
        $this->addColumn(SenderProfile::tableName(), 'internalDeviceIdentifier', $this->string(32));
        $this->addColumn(SenderProfile::tableName(), 'deviceIdHex', $this->string(48));
        $this->addColumn(SenderProfile::tableName(), 'createdAt', $this->timestamp());
        $this->addColumn(SenderProfile::tableName(), 'gtmClientId', $this->string());
        $this->addColumn(SenderProfile::tableName(), 'userAgent', $this->integer());
        $this->addColumn(SenderProfile::tableName(), 'password', $this->string(12));
        $this->addColumn(SenderProfile::tableName(), 'language', $this->string(5));
        $this->addColumn(SenderProfile::tableName(), 'version', $this->integer());
        $this->addColumn(SenderProfile::tableName(), 'bundle', $this->integer());
        $this->addColumn(SenderProfile::tableName(), 'marker', $this->string());

        $this->addColumn(SenderProfileRegister::tableName(), 'registerStartedAt', 'TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00"');
    }

    public function down()
    {
        $this->dropColumn(SenderProfile::tableName(), 'internalDeviceIdentifier');
        $this->dropColumn(SenderProfile::tableName(), 'deviceIdHex');
        $this->dropColumn(SenderProfile::tableName(), 'gtmClientId');
        $this->dropColumn(SenderProfile::tableName(), 'userAgent');
        $this->dropColumn(SenderProfile::tableName(), 'createdAt');
        $this->dropColumn(SenderProfile::tableName(), 'language');
        $this->dropColumn(SenderProfile::tableName(), 'password');
        $this->dropColumn(SenderProfile::tableName(), 'version');
        $this->dropColumn(SenderProfile::tableName(), 'bundle');
        $this->dropColumn(SenderProfile::tableName(), 'marker');

        $this->dropColumn(SenderProfileRegister::tableName(), 'registerStartedAt');
    }
}

