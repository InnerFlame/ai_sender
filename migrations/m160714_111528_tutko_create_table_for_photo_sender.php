<?php

use yii\db\Migration;
use app\models\phoenix\SenderPhoto;
use app\models\phoenix\SenderProfile;

class m160714_111528_tutko_create_table_for_photo_sender extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE  IF NOT EXISTS `" . SenderPhoto::tableName() . "` (
              `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
              `name` VARCHAR (45) DEFAULT NULL,
              `ageFrom` INT (2) NOT NULL DEFAULT 18,
              `ageTo` INT (2) NOT NULL DEFAULT 55,
              `isUsed` TINYINT(1) NOT NULL DEFAULT '0',
              `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`),
              KEY `isUsed` (`isUsed`)
            );
        ");

        $this->alterColumn(SenderProfile::tableName(), 'photo', $this->integer());
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS " . SenderPhoto::tableName());

        $this->alterColumn(SenderProfile::tableName(), 'photo', $this->string());
    }
}
