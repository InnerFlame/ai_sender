<?php

use yii\db\Migration;
use app\models\phoenix\RecipientProfileData;
use app\models\phoenix\RecipientProfile;

/**
 * Class m170712_082959_khachatryan_drop_table_recipientProfileData
 */
class m170712_082959_khachatryan_drop_table_recipientProfileData extends Migration
{
    /** @var int Set batch size if need */
    private $batchSize = 150;

    /**
     * Add multiProfileId column with index to recipientProfile,
     * if recipientProfileData table exists transfer multiIds to recipientProfile and drop it
     */
    public function safeUp()
    {
        $this->addColumn(RecipientProfile::tableName(), 'multiProfileId', $this->string(32));
        $this->createIndex('multiProfileId', RecipientProfile::tableName(), 'multiProfileId');

        $tableSchema = Yii::$app->db->schema->getTableSchema(RecipientProfileData::tableName());
        if (!is_null($tableSchema)) {
            $this->transferMultiProfileIdsToRecipientProfileTable();
            $this->dropTable(RecipientProfileData::tableName());
        }
    }

    /**
     * Restore all changes:
     * - creates recipientProfileData table
     * - transfer all multiIds back to it
     */
    public function safeDown()
    {
        $this->createRecipientProfileDataTable();
        $this->transferMultiProfileIdsBackToRecipientProfileDataTable();
        $this->dropColumn(RecipientProfile::tableName(), 'multiProfileId');
    }


    /**
     * Transfer data from recipientProfileData to recipientProfile
     */
    private function transferMultiProfileIdsToRecipientProfileTable()
    {
        $recipientProfileModelsQuery = RecipientProfile::find()->with('recipientProfileData');

        foreach ($recipientProfileModelsQuery->batch($this->batchSize) as $recipientProfileModels) {
            foreach ($recipientProfileModels as $recipientProfileModel) {
                /** @var $recipientProfileModel RecipientProfile */
                $multiId = !is_null($recipientProfileModel->recipientProfileData)
                    ? $recipientProfileModel->recipientProfileData->multiProfileId : null;
                $recipientProfileModel->multiProfileId = $multiId;
                $recipientProfileModel->save();
            }
        }
    }

    /**
     * Transfer data from recipientProfile to recipientProfileData
     */
    private function transferMultiProfileIdsBackToRecipientProfileDataTable()
    {
        $recipientProfileModelsQuery = RecipientProfile::find()->with('recipientProfileData');

        foreach ($recipientProfileModelsQuery->batch($this->batchSize) as $recipientProfileModels) {
            foreach ($recipientProfileModels as $recipientProfileModel) {
                /** @var $recipientProfileModel RecipientProfile */
                $recipientProfileData                 = new RecipientProfileData();
                $recipientProfileData->recipientId    = $recipientProfileModel->id;
                $recipientProfileData->multiProfileId = $recipientProfileModel->multiProfileId;
                $recipientProfileData->save();
            }
        }
    }

    /**
     * Create table like /migrations/m170323_100106_tutko_add_multiprofileId_to_recipient.php
     */
    private function createRecipientProfileDataTable()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `' . RecipientProfileData::tableName() . '`(
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `recipientId` INT(11) unsigned NOT NULL,
                `multiProfileId` VARCHAR(32) DEFAULT NULL,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `recipientId` (`recipientId`),
                KEY `multiProfileId` (`multiProfileId`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ');
    }
}
