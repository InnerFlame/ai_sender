<?php

use yii\db\Migration;
use app\models\phoenix\SearchProfile;
use app\models\phoenix\RecipientProfile;

class m160804_083553_tutko_change_table extends Migration
{
    public function up()
    {
        $this->renameColumn(SearchProfile::tableName(), 'profileId', 'observerId');

        $this->renameColumn('phoenixSearchScheduler', 'profileId', 'observerId');
        $this->renameColumn('phoenixSearchScheduler', 'searchStartedAt', 'startMonitoring');

        $this->addColumn(RecipientProfile::tableName(), 'online', $this->boolean()->defaultValue(0));
        $this->addColumn(RecipientProfile::tableName(), 'lastOnlineDate', $this->dateTime()->defaultValue('0000-00-00 00:00:00'));
        $this->addColumn(RecipientProfile::tableName(), 'state', $this->string(4));
        $this->addColumn(RecipientProfile::tableName(), 'city', $this->string(20));
        $this->addColumn(RecipientProfile::tableName(), 'citySearch', $this->string(20));
        $this->addColumn(RecipientProfile::tableName(), 'stateSearch', $this->string(4));
        $this->dropColumn(RecipientProfile::tableName(), 'lastVisitStatus');
        $this->dropColumn(RecipientProfile::tableName(), 'lastUsedPlatform');
        $this->dropColumn(RecipientProfile::tableName(), 'registrationPlatform');

        $this->createIndex('country', RecipientProfile::tableName(), 'country');
        $this->createIndex('location', RecipientProfile::tableName(), 'country,stateSearch,citySearch');
        $this->createIndex('online', RecipientProfile::tableName(), 'online');
    }

    public function down()
    {
        $this->renameColumn(SearchProfile::tableName(), 'observerId', 'profileId');

        $this->renameColumn('phoenixSearchScheduler', 'observerId', 'profileId');
        $this->renameColumn('phoenixSearchScheduler', 'startMonitoring', 'searchStartedAt');

        $this->dropIndex('country', RecipientProfile::tableName());
        $this->dropIndex('location', RecipientProfile::tableName());
        $this->dropIndex('online', RecipientProfile::tableName());

        $this->dropColumn(RecipientProfile::tableName(), 'online');
        $this->dropColumn(RecipientProfile::tableName(), 'lastOnlineDate');
        $this->dropColumn(RecipientProfile::tableName(), 'state');
        $this->dropColumn(RecipientProfile::tableName(), 'city');
        $this->dropColumn(RecipientProfile::tableName(), 'citySearch');
        $this->dropColumn(RecipientProfile::tableName(), 'stateSearch');
        $this->addColumn(RecipientProfile::tableName(), 'lastVisitStatus', $this->string());
        $this->addColumn(RecipientProfile::tableName(), 'lastUsedPlatform', $this->string());
        $this->addColumn(RecipientProfile::tableName(), 'registrationPlatform', $this->string());
    }
}
