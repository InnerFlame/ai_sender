<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;

class m170607_093951_tutko_add_index_to_ip extends Migration
{
    public function up()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `usedGeoIp` (
                `idGeoIp` INT(11) unsigned NOT NULL,
                KEY `idGeoIp` (`idGeoIp`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ');
    }

    public function down()
    {
        $this->execute('DROP TABLE IF EXISTS `usedGeoIp`');
    }

}
