<?php

use yii\db\Migration;
use sender\communication\models\CommunicationSchedule;

class m160916_100305_tutko_add_miss_to_comm extends Migration
{
    public function up()
    {
        $this->addColumn(CommunicationSchedule::tableName(), 'attemptsCount', $this->integer(1)->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(CommunicationSchedule::tableName(), 'attemptsCount');
    }

}
