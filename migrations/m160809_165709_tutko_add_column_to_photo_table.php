<?php

use yii\db\Migration;
use app\models\phoenix\SenderPhoto;

class m160809_165709_tutko_add_column_to_photo_table extends Migration
{
    public function up()
    {
        $this->dropColumn(SenderPhoto::tableName(), 'ageFrom');
        $this->dropColumn(SenderPhoto::tableName(), 'ageTo');
        $this->addColumn(SenderPhoto::tableName(), 'level', $this->integer(1)->defaultValue(0));
        $this->addColumn(SenderPhoto::tableName(), 'birthdayYear', $this->integer(4));
        $this->addColumn(SenderPhoto::tableName(), 'birthdayMonth', $this->integer(2));
        $this->addColumn(SenderPhoto::tableName(), 'birthdayDay', $this->integer(2));
        $this->createIndex('birthdayYear', SenderPhoto::tableName(), 'birthdayYear');
        $this->createIndex('name', SenderPhoto::tableName(), 'name', true);
    }

    public function down()
    {
        $this->addColumn(SenderPhoto::tableName(), 'ageFrom', $this->integer(2)->defaultValue(18));
        $this->addColumn(SenderPhoto::tableName(), 'ageTo', $this->integer(2)->defaultValue(55));
        $this->dropColumn(SenderPhoto::tableName(), 'level');
        $this->dropColumn(SenderPhoto::tableName(), 'birthdayYear');
        $this->dropColumn(SenderPhoto::tableName(), 'birthdayMonth');
        $this->dropColumn(SenderPhoto::tableName(), 'birthdayDay');
        $this->dropIndex('birthdayYear', SenderPhoto::tableName());
        $this->dropIndex('name', SenderPhoto::tableName());
    }
}
