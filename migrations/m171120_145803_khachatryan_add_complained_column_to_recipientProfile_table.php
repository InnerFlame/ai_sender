<?php

use yii\db\Migration;

/**
 * Class m171120_145803_khachatryan_add_complained_column_to_recipientProfile_table
 */
class m171120_145803_khachatryan_add_complained_column_to_recipientProfile_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('recipientProfile', 'complained', $this->boolean()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('recipientProfile', 'complained');
    }
}
