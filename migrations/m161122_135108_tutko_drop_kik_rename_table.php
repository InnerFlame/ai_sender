<?php

use yii\db\Migration;
use app\models\phoenix\RecipientSchema;
use app\models\phoenix\Group;

class m161122_135108_tutko_drop_kik_rename_table extends Migration
{
    public function up()
    {


        $this->execute("DROP TABLE IF EXISTS `kikCommunicationSchedule`");
        $this->execute("DROP TABLE IF EXISTS `kikIncomingMessage`");
        $this->execute("DROP TABLE IF EXISTS `kikMatchingErrorLog`");
        $this->execute("DROP TABLE IF EXISTS `kikMessage`");
        $this->execute("DROP TABLE IF EXISTS `kikMessageLog`");
        $this->execute("DROP TABLE IF EXISTS `kikRecipient`");
        $this->execute("DROP TABLE IF EXISTS `kikSender`");
        $this->execute("DROP TABLE IF EXISTS `kikSendRule`");

        if (\Yii::$app->db->schema->getTableSchema('phoenixActiveSchema')) {
            $this->execute("RENAME TABLE `phoenixActiveSchema` TO `" . RecipientSchema::tableName() . "`");
        }

        if (\Yii::$app->db->schema->getTableSchema('phoenixGrabLog')) {
            $this->execute("RENAME TABLE `phoenixGrabLog` TO `grabLog`");
        }

        if (\Yii::$app->db->schema->getTableSchema('phoenixGroup')) {
            $this->execute("RENAME TABLE `phoenixGroup` TO `" . Group::tableName() . "`");
        }
    }


    public function down()
    {
        if (\Yii::$app->db->schema->getTableSchema(RecipientSchema::tableName())) {
            $this->execute("RENAME TABLE `" . RecipientSchema::tableName() . "` TO `phoenixActiveSchema`");
        }

        if (\Yii::$app->db->schema->getTableSchema(Group::tableName())) {
            $this->execute("RENAME TABLE `" . Group::tableName() . "` TO `phoenixGroup`");
        }
    }
}
