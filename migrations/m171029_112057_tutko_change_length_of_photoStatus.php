<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;

class m171029_112057_tutko_change_length_of_photoStatus extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `' . SenderProfile::tableName() . '` CHANGE `photoStatus` `photoStatus` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;');
    }

    public function safeDown()
    {
        $this->execute('ALTER TABLE `' . SenderProfile::tableName() . '` CHANGE `photoStatus` `photoStatus` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;');
    }

}
