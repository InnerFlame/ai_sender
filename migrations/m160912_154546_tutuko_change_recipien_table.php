<?php

use yii\db\Migration;
use app\models\phoenix\RecipientProfile;

class m160912_154546_tutuko_change_recipien_table extends Migration
{
    public function up()
    {
        $this->alterColumn(RecipientProfile::tableName(), 'city', $this->string(50));
        $this->alterColumn(RecipientProfile::tableName(), 'citySearch', $this->string(50));
    }

    public function down()
    {
        $this->alterColumn(RecipientProfile::tableName(), 'city', $this->string(50));
        $this->alterColumn(RecipientProfile::tableName(), 'citySearch', $this->string(50));
    }
}
