<?php

use yii\db\Migration;
use app\models\phoenix\Schema;

class m160728_120653_tutko_add_restartType_to_shema extends Migration
{
    public function up()
    {
        $this->addColumn(Schema::tableName(), 'restartType', $this->integer(2)->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(Schema::tableName(), 'restartType');
    }
}
