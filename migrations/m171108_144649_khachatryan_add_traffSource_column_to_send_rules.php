<?php

use yii\db\Migration;

/**
 * Class m171108_144649_khachatryan_add_traffSource_column_to_send_rules
 */
class m171108_144649_khachatryan_add_traffSource_column_to_send_rules extends Migration
{
    public function safeUp()
    {
        $this->createTable('sendRuleToTraffSource', [
            'id'          => $this->primaryKey(),
            'sendRuleId'  => $this->integer(),
            'traffSource' => $this->string(30)
        ]);

        $this->createIndex('sendRuleIdIndex', 'sendRuleToTraffSource', 'sendRuleId');
    }

    public function safeDown()
    {
        $this->dropTable('sendRuleToTraffSource');
    }
}
