<?php

use yii\db\Migration;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\ProfileMessage;
use sender\registration\models\RulesRegistration;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SendRule;

class m160629_071303_tutko_change_recipient_profile_table extends Migration
{
    public function up()
    {
        $this->dropColumn(RecipientProfile::tableName(), 'remoteType');
        $this->dropColumn(RecipientProfile::tableName(), 'birthday');
        $this->alterColumn(RecipientProfile::tableName(), 'country', $this->string(3));
        $this->alterColumn(RecipientProfile::tableName(), 'gender', $this->boolean()->defaultValue(1));
        $this->dropColumn(RecipientProfile::tableName(), 'geo');
        $this->alterColumn(RecipientProfile::tableName(), 'sexuality', $this->integer(2)->defaultValue(2));
        $this->addColumn(RecipientProfile::tableName(), 'createdAt', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');

        $this->execute("
             CREATE TABLE IF NOT EXISTS `phoenixGrabLog` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `senderId` varchar(32) DEFAULT NULL,
                  `all` int(11) DEFAULT NULL,
                  `unique` int(11) DEFAULT NULL,
                  `online` int(11) DEFAULT NULL,
                  `recentlyOnline` int(11) DEFAULT NULL,
                  `location` varchar(10) NOT NULL,
                  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id`),
                  KEY `accountId` (`senderId`)
             ) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COMMENT='Logs for count grab users'
        ");

        $this->alterColumn(ProfileMessage::tableName(), 'country', $this->string(3));
        $this->alterColumn(RulesRegistration::tableName(), 'country', $this->string(3));
        $this->alterColumn(SenderProfile::tableName(), 'country', $this->string(3));
        $this->alterColumn(SendRule::tableName(), 'country', $this->string(3));
    }

    public function down()
    {
        $this->addColumn(RecipientProfile::tableName(), 'remoteType', $this->integer()->notNull());
        $this->addColumn(RecipientProfile::tableName(), 'birthday', $this->date()->notNull());
        $this->alterColumn(RecipientProfile::tableName(), 'country', $this->string(2));
        $this->addColumn(RecipientProfile::tableName(), 'geo', $this->string());
        $this->alterColumn(RecipientProfile::tableName(), 'gender', $this->integer());
        $this->alterColumn(RecipientProfile::tableName(), 'sexuality', $this->integer()->defaultValue(1));
        $this->dropColumn(RecipientProfile::tableName(), 'createdAt');

        $this->execute("DROP TABLE IF EXISTS `phoenixGrabLog`");

        $this->alterColumn(ProfileMessage::tableName(), 'country', $this->string(2));
        $this->alterColumn(RulesRegistration::tableName(), 'country', $this->string(2));
        $this->alterColumn(SenderProfile::tableName(), 'country', $this->string(2));
        $this->alterColumn(SendRule::tableName(), 'country', $this->string(2));
    }
}
