<?php

use yii\db\Migration;
use app\models\phoenix\MatchingErrorLog;

class m160502_083525_create_communication__empty_log extends Migration
{
    public function up()
    {
        $this->createTable(MatchingErrorLog::tableName(), [
            'id' => $this->primaryKey(),
            'recipientId' => $this->integer(),
            'groupId' => $this->integer(),
            'entity' => $this->string(),
            'createdAt' => $this->timestamp()
        ]);
    }

    public function down()
    {
        $this->dropTable(MatchingErrorLog::tableName());
    }
}
