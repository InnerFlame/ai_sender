<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfileRegister;

class m160428_122352_create_communication_log_table extends Migration
{
    public function up()
    {
        $this->createTable('communicationLog', [
            'id' => $this->primaryKey(),
            'senderId' => $this->integer()->notNull(),
            'recipientId' => $this->integer()->notNull(),
            'messageId' => $this->integer()->notNull(),
            'groupId' => $this->integer()->notNull(),
            'createdAt' => $this->timestamp()->notNull(),
        ]);

        $this->createIndex('senderId', 'communicationLog', 'senderId');
        $this->createIndex('recipientId', 'communicationLog', 'recipientId');

        $this->alterColumn(SenderProfileRegister::tableName(), 'timeToRegister', 'TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00"');
    }

    public function down()
    {
        $this->dropTable('communicationLog');
    }
}
