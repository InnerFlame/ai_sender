<?php

use yii\db\Migration;
use app\models\phoenix\StepMessage;

class m170118_152356_tutko_add_field_to_stepMessage extends Migration
{
    public function up()
    {
        $this->addColumn(StepMessage::tableName(), 'hasPlaceholder', $this->boolean()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(StepMessage::tableName(), 'hasPlaceholder');
    }
}
