<?php

use yii\db\Migration;
use sender\registration\models\RulesRegistration;

class m160423_112939_create_rule_profile_sender_registration extends Migration
{
    public function up()
    {
        $this->createTable(RulesRegistration::tableName(), [
            'id' => $this->primaryKey(),
            'site' => $this->integer()->notNull(),
            'country' => $this->string(2)->notNull(),
            'profileType' => $this->integer()->notNull(),
            'sexuality' => $this->integer()->notNull(),
            'gender' => $this->integer()->notNull(),
            'dateRegistration' => $this->timestamp()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable(RulesRegistration::tableName());
    }
}
