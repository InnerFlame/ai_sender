<?php

use yii\db\Migration;
use app\models\phoenix\RecipientSchema;

class m161229_123905_martchenko_modify_recipient_schema extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `'. RecipientSchema::tableName() .'` ADD COLUMN `isProcessed` TINYINT(1) DEFAULT 0');
        $this->execute('ALTER TABLE `'. RecipientSchema::tableName() .'` ADD KEY `isProcessed` (`isProcessed`)');
    }

    public function down()
    {
        $this->execute('ALTER TABLE `'. RecipientSchema::tableName() .'` DROP COLUMN `isProcessed`');
    }
}
