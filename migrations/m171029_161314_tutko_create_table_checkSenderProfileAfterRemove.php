<?php

use yii\db\Migration;

class m171029_161314_tutko_create_table_checkSenderProfileAfterRemove extends Migration
{
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `senderRemoveCheckSchedule` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `senderId` INT(11) NOT NULL,
                `timeToCheck` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `startedAt` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `senderId` (`senderId`),
                KEY `timeToCheck` (`timeToCheck`, `startedAt`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
    }

    public function safeDown()
    {
        $this->execute('DROP TABLE IF EXISTS `senderRemoveCheckSchedule`');
    }
}
