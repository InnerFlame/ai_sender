<?php

use yii\db\Migration;

class m170315_135600_tutko_eva_log_request_respons extends Migration
{
    public function up()
    {
        if (\Yii::$app->db->schema->getTableSchema('failEvaAnswerLog')) {
            $this->renameTable('failEvaAnswerLog', 'evaLog');
        }

        $this->dropColumn('evaLog', 'error');
        $this->addColumn('evaLog', 'type', $this->string(20));
        $this->addColumn('evaLog', 'response', $this->text());
        $this->addColumn('evaLog', 'responseTime', $this->timestamp()->defaultValue('0000-00-00 00:00:00'));
    }

    public function down()
    {
        $this->dropColumn('evaLog', 'type');
        $this->dropColumn('evaLog', 'response');
        $this->dropColumn('evaLog', 'responseTime');
        $this->addColumn('evaLog', 'error', $this->string(50));

        if (\Yii::$app->db->schema->getTableSchema('evaLog')) {
            $this->renameTable('evaLog', 'failEvaAnswerLog');
        }
    }

}
