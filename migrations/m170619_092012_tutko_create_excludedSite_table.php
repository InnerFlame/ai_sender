<?php

use yii\db\Migration;

class m170619_092012_tutko_create_excludedSite_table extends Migration
{
    public function up()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `excludedSite` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `country` VARCHAR (3) DEFAULT NULL,
                `site` INT(11) NOT NULL,
                `isActive` tinyint,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `siteCountry` (`site`, `country`),
                KEY `isActive` (`isActive`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            INSERT INTO `excludedSite`(`site`,`isActive`) VALUES (415,1),(416,1),(417,1),(940,1),(941,1),(942,1),(946,1),(943,1),(947,1);

        ');
    }

    public function down()
    {
        $this->execute('DROP TABLE IF EXISTS `excludedSite`');
    }
}
