<?php

use yii\db\Migration;
use app\models\phoenix\RecipientProfile;

class m170128_135851_martchenko_api_search_changes extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `" . RecipientProfile::tableName() . "`
                ADD COLUMN `source` varchar(32) DEFAULT NULL,
                ADD COLUMN `locale` varchar(8) DEFAULT NULL,
                ADD COLUMN `screenname` varchar(64) DEFAULT NULL,
                ADD COLUMN `registrationPlatform` varchar(16) DEFAULT NULL,
                ADD COLUMN `photoCount` tinyint(11) DEFAULT '0',
                ADD COLUMN `onlineStatusExpireAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                ADD COLUMN `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                ADD COLUMN `birthday` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
        ");
    }

    public function down()
    {
        $this->execute("
            ALTER TABLE `" . RecipientProfile::tableName() . "`
                DROP COLUMN `source`,
                DROP COLUMN `locale`,
                DROP COLUMN `screenname`,
                DROP COLUMN `registrationPlatform`,
                DROP COLUMN `photoCount`,
                DROP COLUMN `onlineStatusExpireAt`,
                DROP COLUMN `updatedAt`,
                DROP COLUMN `birthday`
        ");
    }
}
