<?php

use yii\db\Migration;
use app\models\phoenix\MatchingErrorLog;

class m161212_153432_tutko_change_error_match extends Migration
{
    public function up()
    {
        $this->truncateTable(MatchingErrorLog::tableName());

        $this->dropColumn(MatchingErrorLog::tableName(), 'recipientId');
        $this->dropColumn(MatchingErrorLog::tableName(), 'groupId');
        $this->addColumn(MatchingErrorLog::tableName(), 'communicationId', $this->integer());
        $this->createIndex('communicationId', MatchingErrorLog::tableName(), 'communicationId');
    }

    public function down()
    {
        $this->dropIndex('communicationId', MatchingErrorLog::tableName());
        $this->addColumn(MatchingErrorLog::tableName(), 'recipientId', $this->integer());
        $this->addColumn(MatchingErrorLog::tableName(), 'groupId', $this->integer());
    }

}
