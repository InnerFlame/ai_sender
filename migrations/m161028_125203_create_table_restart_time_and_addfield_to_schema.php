<?php

use yii\db\Migration;
use app\models\phoenix\Schema;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\RecipientSchema;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\IncomingMessage;

class m161028_125203_create_table_restart_time_and_addfield_to_schema extends Migration
{
    public function up()
    {
        $this->dropColumn(Schema::tableName(), 'restartType');
        $this->addColumn(Schema::tableName(), 'timeToRestart', $this->integer()->defaultValue(0));
        $this->addColumn(Schema::tableName(), 'restartCount', $this->integer()->defaultValue(0));

        $this->execute('DROP TABLE IF EXISTS `phoenixActivationSchema`');

        $this->execute("
            CREATE TABLE IF NOT EXISTS `" . RecipientSchema::tableName() . "` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `recipientId` INT(11) NOT NULL,
                `schemaId` INT(11) NOT NULL,
                `restartNumber` INT(11) NOT NULL,
                `activationTime` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `recipientSchema` (`recipientId`,`schemaId`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");

        $this->addColumn(CommunicationSchedule::tableName(), 'restartNumber', $this->boolean()->defaultValue(0));

        $data = \Yii::$app->db->createCommand("
          SELECT r.id,r.activationTime,i.schemaId
          FROM `" . RecipientProfile::tableName() . "` as r INNER JOIN `" . IncomingMessage::tableName() . "` as i on r.id=i.recipientId
          WHERE r.activationTime<>0
            AND i.schemaId<>0
          group by r.id
        ")->queryAll();

        $dataToSaving = [];

        foreach ($data as $item) {
            $dataToSaving[] = [$item['id'], $item['schemaId'], 0, $item['activationTime']];

            if (count($dataToSaving) >= 100) {
                \Yii::$app->db->createCommand()
                    ->batchInsert(RecipientSchema::tableName(), ['recipientId', 'schemaId', 'restartNumber', 'activationTime'], $dataToSaving)
                    ->execute();

                $dataToSaving = [];
            }
        }

        \Yii::$app->db->createCommand()
            ->batchInsert(RecipientSchema::tableName(), ['recipientId', 'schemaId', 'restartNumber', 'activationTime'], $dataToSaving)
            ->execute();
    }

    public function down()
    {
        $this->addColumn(Schema::tableName(), 'restartType', $this->integer(2)->defaultValue(0));
        $this->dropColumn(Schema::tableName(), 'timeToRestart');
        $this->dropColumn(Schema::tableName(), 'restartCount');

        $this->execute('DROP TABLE IF EXISTS `' . RecipientSchema::tableName() . '`');

        $this->dropColumn(CommunicationSchedule::tableName(), 'restartNumber');
    }
}
