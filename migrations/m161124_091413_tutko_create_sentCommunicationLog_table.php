<?php

use yii\db\Migration;
use app\models\phoenix\SentCommunicationLog;

class m161124_091413_tutko_create_sentCommunicationLog_table extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `" . SentCommunicationLog::tableName() . "` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `site` INT(11) NOT NULL,
                `step` INT (11) NOT NULL,
                `country` VARCHAR(3) NOT NULL,
                `countComm` INT(11) NOT NULL,
                `dateSent`  TIMESTAMP NOT NULL DEFAULT '0000-00-00',
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `dateSent` (`dateSent`),
                KEY `siteCountryDateSent` (`site`, `country`, `dateSent`)
            )
        ");
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `" . SentCommunicationLog::tableName() . "`");
    }
}
