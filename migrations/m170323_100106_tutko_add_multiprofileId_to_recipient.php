<?php

use yii\db\Migration;
use app\models\phoenix\RecipientProfileData;

class m170323_100106_tutko_add_multiprofileId_to_recipient extends Migration
{
    public function up()
    {
        //TODO: deprecated table
        /*
        $this->execute('
            CREATE TABLE IF NOT EXISTS `' . RecipientProfileData::tableName() . '`(
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `recipientId` INT(11) unsigned NOT NULL,
                `multiProfileId` VARCHAR(40) DEFAULT NULL,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `recipientId` (`recipientId`),
                KEY `multiProfileId` (`multiProfileId`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ');
        */
    }

    public function down()
    {
        return true;
        //$this->execute('DROP TABLE IF EXISTS `' . RecipientProfileData::tableName() . '`');
    }
}
