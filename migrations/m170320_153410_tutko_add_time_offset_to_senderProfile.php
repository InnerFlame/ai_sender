<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;

class m170320_153410_tutko_add_time_offset_to_senderProfile extends Migration
{
    public function up()
    {
        $this->addColumn(SenderProfile::tableName(), 'timeOffset', $this->string(10));
    }

    public function down()
    {
        $this->dropColumn(SenderProfile::tableName(), 'timeOffset');
    }
}
