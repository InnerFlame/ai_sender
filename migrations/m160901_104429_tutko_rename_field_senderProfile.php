<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;

class m160901_104429_tutko_rename_field_senderProfile extends Migration
{
    public function up()
    {
        $this->renameColumn(SenderProfile::tableName(), 'temporaryDeactivatedAt', 'pausedAt');
    }

    public function down()
    {
        $this->renameColumn(SenderProfile::tableName(), 'pausedAt', 'temporaryDeactivatedAt');
    }
}
