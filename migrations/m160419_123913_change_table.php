<?php

use yii\db\Migration;
use app\models\phoenix\Schema;
use app\models\phoenix\Group;
use app\models\common\User;

class m160419_123913_change_table extends Migration
{
    public function up()
    {
        if (Yii::$app->db->schema->getTableSchema('profileType')) {
            $this->dropTable('profileType');
        }

        if (Yii::$app->db->schema->getTableSchema('messageType')) {
            $this->dropTable('messageType');
        }

        if (Yii::$app->db->schema->getTableSchema('site')) {
            $this->dropTable('site');
        }

        $this->alterColumn(User::tableName(), 'role', 'integer');

        $this->addColumn(Schema::tableName(), 'createdAt', 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
        $this->alterColumn(Schema::tableName(), 'paid', 'boolean');
        $this->alterColumn(Schema::tableName(), 'gender', 'integer');
        $this->alterColumn(Schema::tableName(), 'sexuality', 'integer');
        $this->dropColumn(Schema::tableName(), 'siteId');


        $this->alterColumn(Group::tableName(), 'dateStart', 'TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00"');
        $this->addColumn(Group::tableName(), 'createdAt', 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
        $this->dropColumn(Group::tableName(), 'siteId');
    }

    public function down()
    {
        $this->dropColumn(Schema::tableName(), 'createdAt');

        $this->dropColumn(Group::tableName(), 'createdAt');

        $this->alterColumn(Schema::tableName(), 'paid', "ENUM('yes', 'no') NOT NULL DEFAULT 'no' ");
        $this->alterColumn(Schema::tableName(), 'gender', "ENUM('female', 'male') NOT NULL DEFAULT 'male' ");
        $this->alterColumn(Schema::tableName(), 'sexuality', "ENUM('gey', 'bee', 'hetero') NOT NULL DEFAULT 'hetero' ");

        $this->alterColumn(User::tableName(), 'role', 'ENUM ("admin", "superAdmin") NOT NULL DEFAULT "admin" ');
    }
}
