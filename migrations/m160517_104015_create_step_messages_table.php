
<?php

use yii\db\Migration;
use app\models\phoenix\StepMessage;

/**
 * Handles the creation for table `step_messages_table`.
 */
class m160517_104015_create_step_messages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(StepMessage::tableName(), [
            'id'        => $this->primaryKey(),
            'body'      => $this->text(),
            'step'      => $this->integer(),
            'createdAt' => $this->timestamp(),
        ]);

        $this->createIndex('step', StepMessage::tableName(), 'step');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(StepMessage::tableName());
    }
}
