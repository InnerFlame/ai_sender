<?php

use yii\db\Migration;
use app\models\phoenix\GrabRule;

class m160914_135203_tutko_add_field_to_grab_rule extends Migration
{
    public function up()
    {
        $this->addColumn(GrabRule::tableName(), 'site', $this->integer());
        $this->addColumn(GrabRule::tableName(), 'sendRuleId', $this->integer());

        $this->createIndex('sendRuleId', GrabRule::tableName(), 'sendRuleId');
    }

    public function down()
    {
        $this->dropIndex('sendRuleId', GrabRule::tableName());

        $this->dropColumn(GrabRule::tableName(), 'site');
        $this->dropColumn(GrabRule::tableName(), 'sendRuleId');
    }
}
