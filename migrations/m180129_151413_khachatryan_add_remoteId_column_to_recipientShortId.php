<?php

use yii\db\Migration;

class m180129_151413_khachatryan_add_remoteId_column_to_recipientShortId extends Migration
{
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `recipientShortId_tmp` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `recipientId` INT(11) NOT NULL,
                `shortId` VARCHAR (10) NOT NULL,
                `remoteId` VARCHAR(40) DEFAULT NULL,
                `country` VARCHAR(3) DEFAULT NULL,
                `site` INTEGER DEFAULT NULL,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `recipientIdShortId` (`recipientId`, `shortId`),
                KEY `ShortId` (`shortId`),
                KEY `filterIndex` (`country`, `site`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
    }

    public function safeDown()
    {
        $this->execute('
              DROP TABLE IF EXISTS `recipientShortId_tmp`;
        ');
    }
}
