<?php

use yii\db\Migration;
use app\models\phoenix\CorrectLocation;

class m170913_133130_tutko_create_table_correctLocation extends Migration
{
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `' . CorrectLocation::tableName() . '` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `correctName` VARCHAR(255) NOT NULL,
                `inCorrectName` VARCHAR (255) DEFAULT NULL,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `inCorrectNameCorrectName` (`inCorrectName`, `correctName`),
                KEY `correctName` (`correctName`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
    }

    public function safeDown()
    {
        $this->execute('DROP TABLE IF EXISTS `' . CorrectLocation::tableName() . '`');
    }
}
