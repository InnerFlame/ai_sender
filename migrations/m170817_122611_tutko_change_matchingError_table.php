<?php

use yii\db\Migration;

class m170817_122611_tutko_change_matchingError_table extends Migration
{
    public function safeUp()
    {
        $this->execute('
            TRUNCATE TABLE `matchingErrorLog`;
            ALTER TABLE `matchingErrorLog` ADD INDEX createdAt (`createdAt`);
            ALTER TABLE `matchingErrorLog` ADD `country` VARCHAR(3) NULL DEFAULT NULL;
            ALTER TABLE `matchingErrorLog` ADD INDEX countryCreatedAt (`country`,`createdAt`);
        ');
    }

    public function safeDown()
    {
        $this->execute('
            ALTER TABLE `matchingErrorLog` DROP INDEX createdAt;
            ALTER TABLE `matchingErrorLog` DROP COLUMN country;
        ');
    }
}
