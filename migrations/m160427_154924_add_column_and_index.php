<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;

class m160427_154924_add_column_and_index extends Migration
{
    public function up()
    {
        $this->createIndex('screenName', SenderProfile::tableName(), 'screenName');
    }

    public function down()
    {
        $this->dropIndex('screenName', SenderProfile::tableName());
    }
}
