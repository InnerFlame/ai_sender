<?php

use yii\db\Migration;

class m171113_075436_tutko_improve_regRule extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `rulesRegistration` ADD COLUMN `ageFrom` INT(1) DEFAULT 18;');
        $this->execute('ALTER TABLE `rulesRegistration` ADD COLUMN `ageTo` INT(11) DEFAULT 99;');
        $this->execute('ALTER TABLE `rulesRegistration` ADD COLUMN `language` VARCHAR (5);');
        $this->execute('ALTER TABLE `rulesRegistration` ADD COLUMN `userType` VARCHAR (10);');
        $this->execute('ALTER TABLE `rulesRegistration` ADD COLUMN `countCreated` INT(11) DEFAULT 0;');
        $this->execute('ALTER TABLE `rulesRegistration` ADD COLUMN `hasDone` TINYINT(1) DEFAULT 0;');
        $this->execute('ALTER TABLE `rulesRegistration` DROP COLUMN `dateTo`');
        $this->execute('ALTER TABLE `rulesRegistration` DROP COLUMN `dateFrom`');
        $this->execute('ALTER TABLE `senderProfileRegister` ADD COLUMN `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;');
    }

    public function safeDown()
    {
        $this->execute('ALTER TABLE `rulesRegistration` DROP COLUMN `ageFrom`');
        $this->execute('ALTER TABLE `rulesRegistration` DROP COLUMN `ageTo`');
        $this->execute('ALTER TABLE `rulesRegistration` DROP COLUMN `language`');
        $this->execute('ALTER TABLE `rulesRegistration` DROP COLUMN `userType`');
        $this->execute('ALTER TABLE `rulesRegistration` DROP COLUMN `countCreated`');
        $this->execute('ALTER TABLE `rulesRegistration` DROP COLUMN `hasDone`');
        $this->execute('ALTER TABLE `rulesRegistration` ADD COLUMN `dateTo` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00";');
        $this->execute('ALTER TABLE `rulesRegistration` ADD COLUMN `dateFrom` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00";');
        $this->execute('ALTER TABLE `senderProfileRegister` DROP COLUMN `createdAt`');
    }
}
