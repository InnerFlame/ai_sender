<?php

use yii\db\Migration;
use app\models\phoenix\RecipientSchema;

class m161220_123326_martchenko_recipient_group extends Migration
{
    public function up()
    {
        if (\Yii::$app->db->schema->getTableSchema('activeSchema')) {
            $this->execute("RENAME TABLE `activeSchema` TO `" . RecipientSchema::tableName() . "`");
        }

        $this->execute('ALTER TABLE `'. RecipientSchema::tableName() .'` ADD COLUMN `groupId` INT(11) DEFAULT NULL AFTER `schemaId`');
        $this->execute('ALTER TABLE `'. RecipientSchema::tableName() .'` ADD COLUMN `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `groupId`');
        $this->execute("ALTER TABLE `". RecipientSchema::tableName() ."` CHANGE COLUMN `activationTime` `activatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'");
        $this->execute('ALTER TABLE `'. RecipientSchema::tableName() .'` CHANGE COLUMN `restartedAt` `startedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP');
    }

    public function down()
    {
        $this->execute('ALTER TABLE `'. RecipientSchema::tableName() .'` DROP COLUMN `groupId`');
        $this->execute('ALTER TABLE `'. RecipientSchema::tableName() .'` DROP COLUMN `createdAt`');
        $this->execute("ALTER TABLE `". RecipientSchema::tableName() ."` CHANGE COLUMN `activatedAt` `activationTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'");
        $this->execute('ALTER TABLE `'. RecipientSchema::tableName() .'` CHANGE COLUMN `startedAt` `restartedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP');

        if (\Yii::$app->db->schema->getTableSchema(RecipientSchema::tableName())) {
            $this->execute("RENAME TABLE `" . RecipientSchema::tableName() . "` TO `activeSchema`");
        }
    }
}
