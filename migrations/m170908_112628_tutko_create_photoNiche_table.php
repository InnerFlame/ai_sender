<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderPhoto;
use yii\db\Query;
use app\models\phoenix\SenderProfileAttributes;
use app\models\phoenix\SenderPhotoNiche;
use sender\registration\models\RulesRegistration;
use app\models\phoenix\ProfileMessage;
use app\models\phoenix\Group;
use app\models\phoenix\SenderProfileRemoveSchedule;

class m170908_112628_tutko_create_photoNiche_table extends Migration
{
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `' . SenderPhotoNiche::tableName() . '` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `photoId` INT(11) NOT NULL,
                `niche` VARCHAR (20) DEFAULT NULL,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `photoId` (`photoId`),
                KEY `niche` (`niche`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            ALTER TABLE `' . SenderProfile::tableName() . '` DROP COLUMN `profileType`;
            ALTER TABLE `' . RulesRegistration::tableName() . '` DROP COLUMN `profileType`;
            ALTER TABLE `' . ProfileMessage::tableName() . '` DROP COLUMN `profileType`;
            ALTER TABLE `' . Group::tableName() . '` DROP COLUMN `profileType`;
            ALTER TABLE `' . SenderProfile::tableName() . '` ADD INDEX `birthday` (`birthday`);
            ALTER TABLE `' . SenderPhoto::tableName() . '` ADD INDEX `IsBadisUsedBirthdayYear` (`isBad`, `isUsed`, `birthdayYear`);
            ALTER TABLE `' . SenderProfileRemoveSchedule::tableName() . '` ADD COLUMN `niche` VARCHAR (20) DEFAULT NULL;
        ');

        $data  = [];
        $query = (new \yii\db\Query())
            ->select('id, isUsed')
            ->from(SenderPhoto::tableName());

        foreach ($query->batch(1000) as $result) {
            foreach ($result as $item) {
                if ($item['isUsed'] == 0) {
                    $data[] = [$item['id'], 'general'];
                } else {
                    $dataTmp     = [];
                    $senderNiche = \Yii::$app->yaml->parse('senderNiche');
                    foreach ($senderNiche['params'] as $niche => $params) {
                        $query = (new Query())
                            ->select('s.id')
                            ->from(SenderProfile::tableName() . ' as s')
                            ->innerJoin(SenderProfileAttributes::tableName() . ' as sa', 's.id=sa.id')
                            ->where(['photo' => $item['id']]);

                        foreach ($params as $column => $param) {
                            foreach ($param as $instruction => $value) {
                                $query->andWhere([$instruction, $column, $value]);
                            }
                        }

                        $result = $query->one();
                        if (!empty($result)) {
                            $dataTmp[] = [$item['id'], $niche];
                        }
                    }

                    if (empty($dataTmp)) {
                        $data[] = [$item['id'], 'general'];
                    } else {
                        $data    = array_merge($data, $dataTmp);
                    }
                }

                if (count($data) >= 200) {
                    \Yii::$app->db->createCommand()->batchInsert('senderPhotoNiche', ['photoId', 'niche'], $data)->execute();
                    $data = [];
                }
            }
        }

        if (empty($data)) {
            \Yii::$app->db->createCommand()->batchInsert('senderPhotoNiche', ['photoId', 'niche'], $data)->execute();
        }
    }

    public function safeDown()
    {
        $this->execute('
            DROP TABLE IF EXISTS `' . SenderPhotoNiche::tableName() . '`;
            ALTER TABLE `' . SenderProfile::tableName() . '` ADD COLUMN `profileType` INT(11);
            ALTER TABLE `' . RulesRegistration::tableName() . '` ADD COLUMN `profileType`  INT(11);
            ALTER TABLE `' . ProfileMessage::tableName() . '` ADD COLUMN `profileType` INT(11);
            ALTER TABLE `' . Group::tableName() . '` ADD COLUMN `profileType` INT(11);
            ALTER TABLE `' . SenderProfile::tableName() . '` DROP INDEX `birthday`;
            ALTER TABLE `' . SenderProfile::tableName() . '` DROP INDEX `IsBadisUsedBirthdayYear`;
            ALTER TABLE `' . SenderProfileRemoveSchedule::tableName() . '` DROP COLUMN `niche`;
        ');
    }
}
