<?php

use yii\db\Migration;
use app\models\phoenix\IncomingMessage;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\ActivationSchema;

class m161007_115612_tutko_add_schemaId_and_create_table_for_activation_schem extends Migration
{
    public function up()
    {
        $this->addColumn(IncomingMessage::tableName(), 'groupId', $this->integer()->defaultValue(0));
        $this->addColumn(IncomingMessage::tableName(), 'schemaId', $this->integer()->defaultValue(0));
        $this->createIndex('schemaId', IncomingMessage::tableName(), 'schemaId');

        $this->addColumn(CommunicationSchedule::tableName(), 'schemaId', $this->integer()->defaultValue(0));
        $this->createIndex('schemaId', CommunicationSchedule::tableName(), 'schemaId');
    }

    public function down()
    {
        $this->dropColumn(IncomingMessage::tableName(), 'groupId');
        $this->dropColumn(IncomingMessage::tableName(), 'schemaId');

        $this->dropColumn(CommunicationSchedule::tableName(), 'schemaId');
    }
}
