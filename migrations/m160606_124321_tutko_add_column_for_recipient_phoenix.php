<?php

use yii\db\Migration;
use app\models\phoenix\RecipientProfile;

class m160606_124321_tutko_add_column_for_recipient_phoenix extends Migration
{
    public function up()
    {
        //is remoteType column remove ?
        //is birthday column remove ?

        $this->addColumn(RecipientProfile::tableName(), 'age', $this->integer(2));
        $this->addColumn(RecipientProfile::tableName(), 'geo', $this->string());
        $this->addColumn(RecipientProfile::tableName(), 'registrationPlatform', $this->string());
        $this->addColumn(RecipientProfile::tableName(), 'lastUsedPlatform', $this->string());
        $this->addColumn(RecipientProfile::tableName(), 'lastVisitStatus', $this->string());

        $this->createIndex('remoteId', RecipientProfile::tableName(), 'remoteId', true);
    }

    public function down()
    {
        $this->dropColumn(RecipientProfile::tableName(), 'age');
        $this->dropColumn(RecipientProfile::tableName(), 'geo');
        $this->dropColumn(RecipientProfile::tableName(), 'registrationPlatform');
        $this->dropColumn(RecipientProfile::tableName(), 'lastUsedPlatform');
        $this->dropColumn(RecipientProfile::tableName(), 'lastVisitStatus');
    }
}
