<?php

use yii\db\Migration;
use app\models\phoenix\ApiSearchRule;
use app\models\phoenix\CommunicationContainer;

class m170324_090331_tutko_create_country_config_in_db extends Migration
{
    public function up()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `' . ApiSearchRule::tableName() . '` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `country` VARCHAR(3) NOT NULL,
                `isActive` tinyint,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            )
        ');

        $this->execute('
            CREATE TABLE IF NOT EXISTS `' . CommunicationContainer::tableName() . '` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `country` VARCHAR(3) NOT NULL,
                `site` INT(11) NOT NULL,
                `countStarted` INT(11) NOT NULL DEFAULT 0,
                `isActive` tinyint,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            )
        ');

        $countries = ['AUS', 'GBR', 'CAN'];
        foreach ($countries as $country) {
            $apiSearchRule           = new ApiSearchRule();
            $apiSearchRule->country  = $country;
            $apiSearchRule->isActive = 1;
            $apiSearchRule->save();

            $communicationContainer               = new CommunicationContainer();
            $communicationContainer->country      = $country;
            $communicationContainer->site         = 1;
            $communicationContainer->countStarted = 60;
            $communicationContainer->isActive     = 1;
            $communicationContainer->save();
        }

    }

    public function down()
    {
        $this->execute('DROP TABLE IF EXISTS `' . ApiSearchRule::tableName() . '`');
        $this->execute('DROP TABLE IF EXISTS `' . CommunicationContainer::tableName() . '`');
    }
}
