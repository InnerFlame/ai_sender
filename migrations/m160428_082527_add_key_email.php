<?php

use yii\db\Migration;
use \app\models\phoenix\SenderProfile;

class m160428_082527_add_key_email extends Migration
{
    public function up()
    {
        $this->createIndex('email', SenderProfile::tableName(), 'email');
    }

    public function down()
    {
        $this->dropIndex('email', SenderProfile::tableName());
    }
}


