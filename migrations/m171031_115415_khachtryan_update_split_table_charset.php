<?php

use yii\db\Migration;

class m171031_115415_khachtryan_update_split_table_charset extends Migration
{
    public function safeUp()
    {
        Yii::$app->db
            ->createCommand("ALTER TABLE split CONVERT TO CHARACTER SET utf8;")
            ->execute();
    }

    public function safeDown()
    {
        Yii::$app->db
            ->createCommand("ALTER TABLE split CONVERT TO CHARACTER SET latin1;")
            ->execute();
    }
}
