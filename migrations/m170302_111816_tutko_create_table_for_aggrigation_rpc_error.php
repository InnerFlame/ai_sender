<?php

use yii\db\Migration;
use app\models\phoenix\RpcErrorLog;

class m170302_111816_tutko_create_table_for_aggrigation_rpc_error extends Migration
{
    public function up()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `' . RpcErrorLog::tableName() . '` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `method` VARCHAR(32) NOT NULL,
                `error` VARCHAR(32) NOT NULL,
                `errorCount` INT(11) NOT NULL DEFAULT 0,
                `errorDate` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `errorDate` (`errorDate`),
                KEY `errorErrorDate` (`error`, `errorDate`),
                KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ');
    }

    public function down()
    {
        $this->execute('DROP TABLE IF EXISTS `' . RpcErrorLog::tableName() . '`');
    }
}
