<?php

use yii\db\Migration;
use app\models\phoenix\PresenceEventLog;

class m161012_073004_tutko_drop_search_scheduler extends Migration
{
    public function up()
    {
        $this->execute('DROP TABLE IF EXISTS `phoenixSearchScheduler`');
    }

    public function down()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `phoenixSearchScheduler` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `profileId` INT(11) NOT NULL,
                `ruleId` INT(11) NOT NULL,
                `offset` INT(4) NOT NULL,
                `timeToSearch` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `searchStartedAt` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `searchedAt` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `createdAt`  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `profileId` (`profileId`),
                KEY `ruleId` (`ruleId`)
            )
        ");
    }
}
