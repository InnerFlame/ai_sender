<?php

use yii\db\Migration;

class m171110_154619_kuleshov_zabbix_timestamps extends Migration
{
    public function safeUp()
    {
        $this->createTable(
            'zabbixTimestamp',
            [
                'key' => $this->string()->notNull()->unique(),
                'lastTimestamp' => $this->integer()->notNull(),
            ]
        );
    }

    public function safeDown()
    {
        $this->dropTable('zabbixTimestamp');
    }
}
