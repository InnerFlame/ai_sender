<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfileRegister;

class m160718_093157_tutko_remove_error_text extends Migration
{
    public function up()
    {
        $this->dropColumn(SenderProfileRegister::tableName(), 'errorText');
    }

    public function down()
    {
        $this->addColumn(SenderProfileRegister::tableName(), 'errorText', $this->text());
    }
}
