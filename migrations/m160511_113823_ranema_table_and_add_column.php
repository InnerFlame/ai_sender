<?php

use yii\db\Migration;
use sender\communication\models\CommunicationSchedule;

class m160511_113823_ranema_table_and_add_column extends Migration
{
    public function up()
    {
        $this->renameTable('communicationLog', CommunicationSchedule::tableName());

        $this->addColumn(CommunicationSchedule::tableName(), 'timeToSend', $this->timestamp());
    }

    public function down()
    {
        $this->renameTable(CommunicationSchedule::tableName(), 'communicationLog');

        $this->dropColumn(CommunicationSchedule::tableName(), 'timeToSend');
    }
}
