<?php

use yii\db\Migration;
use sender\registration\models\RulesRegistration;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileRegister;


class m160426_142011_martchenko_db_and_models_improvements extends Migration
{
    public function up()
    {
        $this->renameColumn(RulesRegistration::tableName(), 'dateRegistration', 'dateFrom');
        $this->addColumn(RulesRegistration::tableName(), 'dateTo', $this->timestamp()->notNull());
        $this->addColumn(RulesRegistration::tableName(), 'isActive', $this->boolean());
        $this->addColumn(RulesRegistration::tableName(), 'profilesCount', $this->integer()->notNull());

        $this->alterColumn(SenderProfile::tableName(), 'screenName', $this->string(50));
        $this->addColumn(SenderProfile::tableName(), 'email', $this->string(50));

        $this->dropColumn(SenderProfileRegister::tableName(), 'statusRegistration');
        $this->addColumn(SenderProfileRegister::tableName(), 'ruleId', $this->integer());
        $this->renameColumn(SenderProfileRegister::tableName(), 'dateRegistration', 'timeToRegister');
        $this->renameColumn(SenderProfileRegister::tableName(), 'dateRegistrationSite', 'registeredAt');
    }

    public function down()
    {
        $this->renameColumn(RulesRegistration::tableName(), 'dateFrom', 'dateRegistration');
        $this->dropColumn(RulesRegistration::tableName(), 'dateTo');
        $this->dropColumn(RulesRegistration::tableName(), 'isActive');
        $this->dropColumn(RulesRegistration::tableName(), 'profilesCount');

        $this->alterColumn(SenderProfile::tableName(), 'screenName', $this->string(50)->notNull());
        $this->dropColumn(SenderProfile::tableName(), 'email');

        $this->addColumn(SenderProfileRegister::tableName(), 'statusRegistration', $this->boolean());
        $this->dropColumn(SenderProfileRegister::tableName(), 'ruleId');
        $this->renameColumn(SenderProfileRegister::tableName(), 'timeToRegister', 'dateRegistration');
        $this->renameColumn(SenderProfileRegister::tableName(), 'registeredAt', 'dateRegistrationSite');
    }
}
