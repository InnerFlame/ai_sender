<?php

use yii\db\Migration;

class m161206_105952_martchenko_containers_info_backup extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `launchedContainersInfo`(
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `country` varchar(3) DEFAULT NULL,
                `data` text,
                PRIMARY KEY (`id`),
                KEY `country` (`country`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `launchedContainersInfo`");
    }
}
