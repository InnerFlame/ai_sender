<?php

use yii\db\Migration;
use app\models\phoenix\SearchProfile;
use app\models\phoenix\GrabRule;

class m160803_123855_tutko_create_tables_for_search extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `" . GrabRule::tableName() . "` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `country` VARCHAR(3) NOT NULL,
                `city` VARCHAR(50) DEFAULT NULL,
                `state` VARCHAR(2) DEFAULT NULL,
                `ageFrom` INT(2) NOT NULL DEFAULT 18,
                `ageTo` INT(2) NOT NULL DEFAULT 35,
                `isActive` TINYINT(1) DEFAULT 0,
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table for grab rules'
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `" . SearchProfile::tableName() . "` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `profileId` INT(11) NOT NULL,
                `countSearch` INT(3) NOT NULL DEFAULT 0,
                `timeStartSearch` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `profileId` (`profileId`)
            )
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `phoenixSearchScheduler` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `profileId` INT(11) NOT NULL,
                `ruleId` INT(11) NOT NULL,
                `offset` INT(4) NOT NULL,
                `timeToSearch` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `searchStartedAt` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `searchedAt` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `createdAt`  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `profileId` (`profileId`),
                KEY `ruleId` (`ruleId`)
            )
        ");
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `" . GrabRule::tableName() . "`");

        $this->execute("DROP TABLE IF EXISTS `" . SearchProfile::tableName() . "`");

        $this->execute("DROP TABLE IF EXISTS `phoenixSearchScheduler`");
    }
}
