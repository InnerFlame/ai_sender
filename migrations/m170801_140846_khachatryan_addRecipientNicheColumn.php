<?php

use yii\db\Migration;
use app\models\phoenix\RecipientProfile;

/**
 * Class m170801_140846_khachatryan_addRecipientNicheColumn
 */
class m170801_140846_khachatryan_addRecipientNicheColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn(RecipientProfile::tableName(), 'niche', $this->string(30)->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn(RecipientProfile::tableName(), 'niche');
    }
}
