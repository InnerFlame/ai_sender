<?php

use yii\db\Migration;

class m171206_154429_tutko_creata_fetch_schedule_tabale extends Migration
{
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `senderFetchSchedule` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `senderId` int(11) NOT NULL,
              `timeToFetch` timestamp NOT NULL DEFAULT "0000-00-00 00:00:00",
              `startedAt` timestamp NOT NULL DEFAULT "0000-00-00 00:00:00",
              `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`),
              KEY `senderId` (`senderId`),
              KEY `timeToFetch` (`timeToFetch`,`startedAt`),
              KEY `createdAt` (`createdAt`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ');
    }

    public function safeDown()
    {
        $this->execute('DROP TABLE IF EXISTS `senderRemoveCheckSchedule`');
    }
}
