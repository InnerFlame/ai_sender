<?php

use yii\db\Migration;
use app\models\phoenix\RecipientProfile;
use app\models\phoenix\IncomingMessage;

class m161125_114139_tutko_add_field_to_recipient_profile extends Migration
{
    public function up()
    {
        $this->addColumn(RecipientProfile::tableName(), 'lastUsedPlatform', $this->string(10)->notNull()->defaultValue(RecipientProfile::NOT_SET));

        $this->addColumn(IncomingMessage::tableName(), 'platformFromSent', $this->string(10)->notNull()->defaultValue(RecipientProfile::NOT_SET));
    }

    public function down()
    {
        $this->dropColumn(RecipientProfile::tableName(), 'lastUsedPlatform');

        $this->dropColumn(IncomingMessage::tableName(), 'platformFromSent');
    }
}
