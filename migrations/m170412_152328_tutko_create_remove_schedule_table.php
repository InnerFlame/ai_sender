<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\SenderProfileRemoveSchedule;

class m170412_152328_tutko_create_remove_schedule_table extends Migration
{
    public function up()
    {
        $this->addColumn(SenderProfile::tableName(), 'removedAt', 'TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00"');

        $this->execute('
            CREATE TABLE IF NOT EXISTS `' . SenderProfileRemoveSchedule::tableName() . '` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `senderId` INT(11) NOT NULL,
                `timeToRemove` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `startedRemoveAt` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `removedAt` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00",
                `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                KEY `senderId` (`senderId`),
                KEY `timeToRemove` (`timeToRemove`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ');
    }

    public function down()
    {
        $this->dropColumn(SenderProfile::tableName(), 'removedAt');

        $this->execute('DROP TABLE IF EXISTS `' . SenderProfileRemoveSchedule::tableName() . '`');
    }
}