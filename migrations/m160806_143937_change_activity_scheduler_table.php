<?php

use yii\db\Migration;
use app\models\phoenix\ActivityScheduler;

class m160806_143937_change_activity_scheduler_table extends Migration
{
    public function up()
    {
        $this->alterColumn(ActivityScheduler::tableName(), 'recipientId', $this->string(40));
        $this->addColumn(ActivityScheduler::tableName(), 'sendStartedAt', $this->timestamp()->defaultValue('0000-00-00 00:00:00'));
        $this->addColumn(ActivityScheduler::tableName(), 'isSenderBanned', $this->boolean()->defaultValue(0));

    }

    public function down()
    {
        $this->alterColumn(ActivityScheduler::tableName(), 'recipientId', $this->integer(11));
        $this->dropColumn(ActivityScheduler::tableName(), 'sendStartedAt');
        $this->dropColumn(ActivityScheduler::tableName(), 'isSenderBanned');
    }

}
