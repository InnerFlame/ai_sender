<?php

use yii\db\Migration;
use app\models\phoenix\SendRule;

class m170711_081142_khachatryan_alter_table_sendRule extends Migration
{
    public function safeUp()
    {
        $this->addColumn(SendRule::tableName(), 'project', $this->string(15));
        $this->dropIndex('uniqueItems', SendRule::tableName());
        $this->createIndex('uniqueItems', SendRule::tableName(), ['schemaId', 'site', 'project', 'country'], true);
    }

    public function safeDown()
    {
        $this->dropColumn(SendRule::tableName(), 'project');
        $this->dropIndex('uniqueItems', SendRule::tableName());
        $this->createIndex('uniqueItems', SendRule::tableName(), ['schemaId', 'site', 'country'], true);
    }
}
