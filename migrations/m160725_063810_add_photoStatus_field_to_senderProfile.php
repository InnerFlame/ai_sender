<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;

class m160725_063810_add_photoStatus_field_to_senderProfile extends Migration
{
    public function up()
    {
        $this->addColumn(SenderProfile::tableName(), 'photoStatus', $this->string(8));
    }

    public function down()
    {
        $this->dropColumn(SenderProfile::tableName(), 'photoStatus');
    }
}
