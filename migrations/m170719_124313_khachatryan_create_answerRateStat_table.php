<?php

use yii\db\Migration;

/**
 * Class m170719_124313_khachatryan_create_answerRateStat_table
 */
class m170719_124313_khachatryan_create_answerRateStat_table extends Migration
{
    public function safeUp()
    {
        $tableExists = Yii::$app->db->schema->getTableSchema('answerRateStat');
        if(is_null($tableExists)) {
            $this->createTable('answerRateStat', [
                'id'        => $this->primaryKey(),
                'initials'  => $this->integer(),
                'answered'  => $this->integer(),
                'sentAt'    => $this->timestamp()->defaultValue(NULL),
                'groupId'   => $this->integer(),
                'schemeId'  => $this->integer(),
                'country'   => $this->char(5),
                'restartNumber' => $this->integer()
            ]);

            $this->createIndex('sentAt', 'answerRateStat', ['sentAt', 'country']);
        }
    }

    public function safeDown()
    {
        $tableExists = Yii::$app->db->schema->getTableSchema('answerRateStat');
        if(!is_null($tableExists)) {
            $this->dropTable('answerRateStat');
        }
    }
}
