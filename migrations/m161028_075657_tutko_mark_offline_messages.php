<?php

use yii\db\Migration;
use sender\communication\models\CommunicationSchedule;
use app\models\phoenix\IncomingMessage;

class m161028_075657_tutko_mark_offline_messages extends Migration
{
    public function up()
    {
        $this->dropColumn(CommunicationSchedule::tableName(), 'communicationType');

        $this->execute("DROP TABLE IF EXISTS `phoenixNotExistsRecipient`");

        $this->addColumn(IncomingMessage::tableName(), 'messageType', $this->string(7));
    }

    public function down()
    {
        $this->addColumn(CommunicationSchedule::tableName(), 'communicationType', $this->integer()->defaultValue(0));

        $this->execute("
            CREATE TABLE IF NOT EXISTS `phoenixNotExistsRecipient` (
                `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
                `recipientId` VARCHAR(32) NOT NULL,
                `senderId` VARCHAR(32) NOT NULL,
                `timeSent` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
                `message` TEXT,
                `cretedAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");

        $this->dropColumn(IncomingMessage::tableName(), 'messageType');
    }
}
