<?php

use yii\db\Migration;
use app\models\phoenix\SenderProfile;
use app\models\phoenix\ActivationSenderScheduler;

class m160830_103447_tutko_update_senderProfile extends Migration
{
    public function up()
    {
        $this->addColumn(SenderProfile::tableName(), 'deactivatedAt', "TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00'");
        $this->addColumn(SenderProfile::tableName(), 'temporaryDeactivatedAt', "TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00'");

    }

    public function down()
    {
        $this->dropColumn(SenderProfile::tableName(), 'deactivatedAt');
        $this->dropColumn(SenderProfile::tableName(), 'temporaryDeactivatedAt');
    }

}
